
/****************************************
***** For more information contact ******
*********** kkhatti@gmail.com ***********
****************************************/

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#DADADA';

	//Developed by Kamlesh - Dilse
	config.toolbar_Custom_mini =
	[
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'Undo', 'Redo' ] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize' ] }	
		
	];

	//Developed by Kamlesh - Dilse
	config.toolbar_Custom_medium =
	[
	
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', 'Preview', 'Print' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'Undo', 'Redo' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },		
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'BGColor' ] },
		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule'] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }

		
	];	

/*
	var xbasepath = webRoot+'js';
	
	config.filebrowserBrowseUrl = xbasepath+'/kcfinder/browse.php?opener=ckeditor&type=files';
	config.filebrowserImageBrowseUrl = xbasepath+'/kcfinder/browse.php?opener=ckeditor&type=images';
	config.filebrowserFlashBrowseUrl = xbasepath+'/kcfinder/browse.php?opener=ckeditor&type=flash';
	config.filebrowserUploadUrl = xbasepath+'/kcfinder/upload.php?opener=ckeditor&type=files';
	config.filebrowserImageUploadUrl = xbasepath+'/kcfinder/upload.php?opener=ckeditor&type=images';
	config.filebrowserFlashUploadUrl = xbasepath+'/kcfinder/upload.php?opener=ckeditor&type=flash';
	config.allowedContent = true;
	config.ignoreEmptyParagraph = false;
	config.extraAllowedContent = 'ul ol li';
*/

};
