<?php

class CustomerModel extends Model
{

	public $db; //database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}
    
	/*getting all customer details */
	public function get_customer_data($moddata){
		 $get_url     = API_URL.'categoryitems/get_customer_data/';
		 $json          = $this->receive_data($get_url,$moddata);
		 return json_decode($json);	
	}
	
	function get_customers_details($apiUrl, $data ){
	    $url     = $apiUrl.'customer/get_customers_details/';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function users_filter($apiUrl, $data ){
		$url     = $apiUrl.'customer/users_filter/';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	public function process_user_data($data){
		$self  = $this;
		array_walk($data,function(&$value,$key) use($self){
			$value = get_object_vars($value);
		});
		return $data;
	}
	
	/*---Added -----*/
	public function savingCustomerInfo($post){
		$get_url     = API_URL.'customer/savingCustomerInfo/';	
		$json          = $this->receive_data($get_url, $post);
		$json = json_decode($json);
	}
	
	public function getCustomerNotes($post){
		$get_url     = API_URL.'customer/getCustomerNotes/';	
		$json          = $this->receive_data($get_url, $post);
		$json = json_decode($json);
		return $json->Data;		
	}
	
	public function addCustomerNote($post){
		$get_url     = API_URL.'customer/addCustomerNote/';	
		$json          = $this->receive_data($get_url, $post);
		
		$data = $this->getCustomerNotes($post);
		return $data;
	}
	
	public function removeCustomerNote($post){
		$get_url    = API_URL.'customer/removeCustomerNote/';	
		$json       = $this->receive_data($get_url, $post);
		$json       = json_decode($json);
		return $json->Data;		
	}
	
	public function get_user_review($post){
		$get_url       = API_URL.'customer/get_user_reviews/';
		$json          = $this->receive_data($get_url,$post);
		$json          = json_decode($json);
		
		if(isset($json->Data[0]))
			return get_object_vars($json->Data[0]);	
		else
			return array();	
		
	}
	
	public function get_all_review_contents(){
		$get_url       = API_URL.'customer/get_all_review_contents/';
		$json          = $this->receive_data($get_url,array());
		$json          = json_decode($json);
		return $this->convert_array($json->Data);
	
	}
	
	public function update_customer($data){
		$get_url       = API_URL.'customer/update_customer/';
		$json          = $this->receive_data($get_url,$data);
		$json          = json_decode($json);
		return $json;
	}
	
	public function convert_array($data){
		array_walk($data,function(&$val,$key){ 
			$val = get_object_vars($val);
		});
	return 	$data;
	}
	
	public function orderhistory($post){
		$url     = API_URL.'myaccount/orderhistory';
		$json    = $this->receive_data($url,$post);
		$finalData = json_decode($json);
		return $finalData->Data;            
	}
	
	
	public function listSavedAddress($uid){
		$get_url     = API_URL.'myaccount/listSavedAddress/';
		$json          = $this->receive_data($get_url,array("uid"=>$uid));
		$finalData = @json_decode($json);
		return $finalData->Data;
	
	}
	
	public function savedBilling($post){
		$get_url     = API_URL.'myaccount/savedBilling/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;
	}
	
	public function savingUserAddress($data){
		$get_url     = API_URL.'myaccount/saveUserAddress/';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function removeAddress($data){
		$get_url     = API_URL.'myaccount/removeAddress/';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function editAddress($data){
		$get_url     = API_URL.'myaccount/editAddress/';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function get_Billinginfo($data,$print=false){
		$get_url       = API_URL.'cart/getBillinginfo/';
		$json          = $this->receive_data($get_url,$data);
		if($print == true){ echo $json; } else { return @json_decode($json); }
	}
	
	public function cardAddEditEvent($post){
		$get_url     = API_URL.'myaccount/cardAddEditEvent/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;	
	}

	public function cardRemoveEvent($post){
		$get_url     = API_URL.'myaccount/cardRemoveEvent/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;	
	}
	
	public function getSavedMeals($post){
		$get_url     = API_URL.'myaccount/getSavedMeals/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;	
	}
	
	public function createnewUser($post){
		$get_url     = API_URL.'myaccount/createAccount/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;
	}
	
	public function addUserReview($post){
		$get_url     = API_URL.'myaccount/addUserReview/';
		echo $json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		if( isset($json->Data) ) return $json->Data;
	}
	
	public function  get_user_review_messages($post){
		$get_url     = API_URL.'myaccount/get_user_review_messages/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		$data = $json->Data;
		array_walk($data,function(&$val,$key){
		$val = get_object_vars($val);
		});
		return $data;
	}
	public function reorder($data){
		$uid = $data['user_id'];
		$delUrl        = API_URL.'customer/delete_all_orders/';
		$this->receive_data($delUrl,array("user_id"=>$uid));
		$get_url       = API_URL.'myaccount/myOrderReorder/';
		//exit;
		return $json   = $this->receive_data($get_url,$data);
	
	}
	
}