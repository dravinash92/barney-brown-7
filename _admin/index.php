<?php

  date_default_timezone_set('Asia/Kolkata');
  //Load Directory Conf
  include_once('app/conf/conf.cms.php');   
  //Load DB
  //include_once(DIR_CONF.'conf.my_db.php'); 
  //include_once(DIR_DB.'object.my_db.php');
//error_reporting(E_ALL ^ E_NOTICE);
//   error_reporting(0);
// ini_set('display_errors',0);
  //Require Template Engine
  require_once(DIR_SMARTY.'libs/SmartyBC.class.php');
  
  include_once(DIR_BASE.'engine.php');
  include_once(DIR_BASE.'base.php');
  include_once(DIR_BASE.'model.php');
  //include_once(DIR_BASE.'mpdf/mpdf.php');
  
  foreach (glob("app/controller/*.php") as $filename)
  {
    include_once $filename;
  }
  $inspire = new Base;
  $inspire->postdata = $_POST;
  $inspire->init();
