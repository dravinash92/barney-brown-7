<?php

class AccountsModel extends Model
{
    
    public $db; 
    //database connection object
    
    public function __construct()
    {
        $this->db = parent::__construct();
    }
    
    function get_standard_catgory_items($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/get_standard_catgory_items';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    function insert($apiUrl, $data)
    {
        
        $url  = $apiUrl . 'categoryitems/add_standard_catgory_items';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    
    function insert_standard_products($apiUrl, $data)
    {
        
        $url  = $apiUrl . 'categoryitems/add_standard_catgory_products';
        $json = $this->receive_data($url, $data);
        print_r($json);
        exit;
        return $json;
    }
    
    
    function get_standard_catgory_items_list($apiUrl, $data)
    {
        
        $url  = $apiUrl . 'categoryitems/get_standard_catgory_items_list';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    function get_standard_product_items($apiUrl, $data)
    {
        
        $url  = $apiUrl . 'categoryitems/get_standard_product_items';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    function update_standard_category_products($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/update_standard_category_products';
        $json = $this->receive_data($url, $data);
        
        return $json;
    }
    
    function delete_standard_category_products($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/delete_standard_category_products';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    function update_standard_category_priority($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/update_standard_category_priority';
        $json = $this->receive_data($url, $data);
        return $json;
        
    }
    
    function get_all_pickups_items($apiUrl, $data)
    {
        
        $url       = $apiUrl . 'categoryitems/get_all_pickups_items';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    function process_user_data($data)
    {
        $self = $this;
        array_walk($data, function(&$value, $key) use ($self)
        {
            $value = get_object_vars($value);
            $array = get_object_vars(json_decode($value['sandwich_data']));
            array_walk($array, function(&$val, $key)
            {
                $val = get_object_vars($val);
            });
            $value['sandwich_data'] = $array;
            $value['user_name']     = $self->get_user($value['uid']);
            $value['formated_date'] = date('m/d/y', strtotime($value['date_of_creation']));
        });
        return $data;
    }
    
    
    function get_all_order_status($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/get_all_order_status';
        $json = $this->receive_data($url, $data);
        
        $finalData = json_decode($json);
        
        return $finalData->Data;
    }
    
    function update_order_status($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/update_order_status';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    
    function get_all_delivery_items($apiUrl, $data)
    {
        $url       = $apiUrl . 'categoryitems/get_all_delivery_items';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    
    function get_search_delivery_items($apiUrl, $data)
    {
        
        $url  = $apiUrl . 'categoryitems/get_search_delivery_items';
        $json = $this->receive_data($url, $data);
        
        $finalData = json_decode($json);
        return $finalData->Data;
        
    }
    
    function get_admin_user_data($apiUrl, $data)
    {
        $data = $data == '' ? array() : $data; 
        $url       = $apiUrl . 'categoryitems/get_admin_user_data';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
        
    }

    function get_delivery_user_data($apiUrl, $data)
    {
         $query1  = "SELECT d.*,(SELECT COUNT(*)  FROM orders o WHERE o.delivery_assigned_to = d.uid AND o.order_status=7 AND o.order_delivered >=  DATE_SUB(NOW(), INTERVAL 30 DAY)) as deliveries , (SELECT SUM(total) FROM orders o WHERE o.delivery_assigned_to = d.uid AND o.order_status=7 AND o.order_delivered >=  DATE_SUB(NOW(), INTERVAL 30 DAY)) as totals FROM delivery_user d";

        $run_query1 = $this->db->Query($query1);
        $result = $this->db->FetchAllArray($run_query1);
        
        foreach ($result as $key => $account) {
            $query2 = "SELECT p.store_name FROM pickup_stores p WHERE p.id IN(".$account['store_id'].")";
            $run_qry = $this->db->Query($query2);
            $result2 = $this->db->FetchAllArray($run_qry);
            $store_name = array();
            $store_name = array_column($result2, 'store_name');
            //$result[$key]['store_names'] = array_combine(explode(',', $account['store_id']), $store_name);
            $result[$key]['store_names'] = implode(', ',$store_name);
        }
         return $result;  
    }
    
    function get_admin_user_category($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/get_admin_user_category';
        $json = $this->receive_data($url, $data);
        
        $finalData = json_decode($json);
        return $finalData->Data;
        
    }
    
    function add_new_admin_user($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/add_new_admin_user';
        $json = $this->receive_data($url, $data);
        return $json;
    }

    function add_new_delivery_user($apiUrl, $data)
    {
        $query = "INSERT INTO `delivery_user` (name,user_name, password, store_id, live) values ('" . $data['name'] . "','" . $data['user_name'] . "','" .  md5($data['password']). "','" .$data['store_id'] . "','" . $data['live'] . "') ";
        return $this->db->Query($query);
    }
    function update_delivery_user($apiUrl, $data)
    {
        $query = "UPDATE `delivery_user` SET `name` = '" . $data['name'] . "',`user_name` = '" . $data['user_name'] . "',`password` = '" . md5($data['password']) . "',`store_id` = '" . $data['store_id'] . "' WHERE uid = ".$data['uid'];
        return $this->db->Query($query);
    }
    
    function get_admin_user($apiUrl, $data)
    {
        
        $url       = $apiUrl . 'categoryitems/get_admin_user';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }

    function get_delivery_user($apiUrl, $data)
    {
        // $url       = $apiUrl . 'categoryitems/get_delivery_user';
        // $json      = $this->receive_data($url, $data);
        // $finalData = json_decode($json);
        // return $finalData->Data;
        $query1  = "SELECT * FROM delivery_user WHERE uid = ".$data['id'];

        $run_query1 = $this->db->Query($query1);
        $result = $this->db->FetchAllArray($run_query1);
         return $result;  
    }
    
    
    function update_admin_user($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/update_admin_user';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    function delete_admin_user($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/delete_admin_user';
        $json = $this->receive_data($url, $data);
        return $json;
    }

    function delete_delivery_user($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/delete_delivery_user';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    function get_store_accounts_data($apiUrl, $data)
    {
        $data = $data == '' ? array() : $data; 
        $url       = $apiUrl . 'categoryitems/get_store_accounts_data';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    
    function add_new_store_account($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/add_new_store_account';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    function get_store_account($apiUrl, $data)
    {
        $url       = $apiUrl . 'categoryitems/get_store_account';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    
    function update_store_account($apiUrl, $data)
    {
        $url = $apiUrl . 'categoryitems/update_store_account';
        echo $json = $this->receive_data($url, $data);
        return $json;
    }
    
    
    function delete_store_account($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/delete_store_account';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    
    function add_webpage_banner_data($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/add_webpage_banner_data';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    
    
    function get_web_homepage_data($apiUrl, $data)
    {
        $url       = $apiUrl . 'categoryitems/get_web_homepage_data';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    
    
    function update_web_home_page_priority($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/update_web_home_page_priority';
        $json = $this->receive_data($url, $data);
        return $json;
        
    }
    
    function get_web_homepage_banner($apiUrl, $data)
    {
        
        $url       = $apiUrl . 'categoryitems/get_web_homepage_banner';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    
    
    function update_webpage_banner_data($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/update_webpage_banner_data';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    
    
    function update_webpage_data($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/update_webpage_data';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    
    
    function get_webpages_data($apiUrl, $data)
    {
        $url       = $apiUrl . 'categoryitems/get_webpages_data';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    
    function get_webpagedata_data($apiUrl, $data)
    {
        $url       = $apiUrl . 'categoryitems/get_webpagedata_data';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    
    
    function delete_home_page_banner($apiUrl, $data)
    {
        $url  = $apiUrl . 'categoryitems/delete_home_page_banner';
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    function get_sandwich_category_items($apiUrl, $data)
    {
        $url       = $apiUrl . 'categoryitems/get_sandwich_custom_category_items';
        $json      = $this->receive_data($url, $data);
        $finalData = json_decode($json);
        return $finalData->Data;
    }
    
    function storeUsernameExists($post)
    {
        $get_url = API_URL . 'myaccount/storeUsernameExists/';
        $json    = $this->receive_data($get_url, $post);
        return json_decode($json);
    }
    
    function cmsUsernameExists($post)
    {
        $get_url = API_URL . 'myaccount/cmsUsernameExists/';
        $json    = $this->receive_data($get_url, $post);
        $json    = json_decode($json);
        return $json->Data;
    }
    
    function saveSocialLinks($post)
    {
        $get_url = API_URL . 'myaccount/saveSocialLinks/';
        $json    = $this->receive_data($get_url, $post);
    }
    
    function getSocialLinks($post)
    {
        $get_url = API_URL . 'myaccount/getSocialLinks/';
        $json    = $this->receive_data($get_url, $post);
        $json    = json_decode($json);
        if (isset($json->Data))
            return $json->Data;
    }
    
    function updateStoreOrder($post)
    {
        $get_url = API_URL . 'myaccount/updateStoreOrder/';
        $json    = $this->receive_data($get_url, $post);
        $json    = json_decode($json);
        return $json->Data;
    }
    
    function updateUsersLive($post)
    {
        $get_url = API_URL . 'myaccount/updateUsersLive/';
        $json    = $this->receive_data($get_url, $post);
        $json    = json_decode($json);
        return $json->Data;
    }

    function updateDeliveryUsersLive($post)
    {
        $get_url = API_URL . 'myaccount/updateDeliveryUsersLive/';
        $json    = $this->receive_data($get_url, $post);
        $json    = json_decode($json);
        return $json->Data;
    }
    
    public function getAllzipcodes($post)
    {
        $get_url = API_URL . 'myaccount/getAllzipcodes/';
        return $json = $this->receive_data($get_url, array());
    }
    
    function deleteZipcode()
    {
        $get_url = API_URL . 'myaccount/deleteZipcode/';
        return $json = $this->receive_data($get_url, array(
            'zipId' => $_POST['zipid']
        ));
    }

    public function getPickupStores()
    {
        $get_url   = API_URL . 'cart/listPickupAddress/';
        $json      = $this->receive_data($get_url, array());
        $finalData = @json_decode($json);
        return $finalData->Data;
    }
    public function get_delivery_user_by_date($post)
    {
        $from_date  = date("Y-m-d", strtotime($post['from_date']));
        $to_date  = date("Y-m-d", strtotime($post['to_date']));

        if (isset($post['from_time']))
                $from_date .= " " . $post['from_time'] . ":00";
            else
                $from_date .= " 00:00:00";

        if (isset($post['to_time']))
                $to_date .= " " . $post['to_time'] . ":00";
            else
                $to_date .= " 00:00:00";


        $query1  = "SELECT d.*,(SELECT COUNT(*)  FROM orders o WHERE o.delivery_assigned_to = d.uid AND o.order_status=7 AND (o.order_delivered BETWEEN '".$from_date."' AND '".$to_date."')) as deliveries , (SELECT SUM(total) FROM orders o WHERE o.delivery_assigned_to = d.uid AND o.order_status=7 AND (o.order_delivered BETWEEN '".$from_date."' AND '".$to_date."')) as totals FROM delivery_user d";

        $run_query1 = $this->db->Query($query1);
        $result = $this->db->FetchAllArray($run_query1);
        
        foreach ($result as $key => $account) {
            $query2 = "SELECT p.store_name FROM pickup_stores p WHERE p.id IN(".$account['store_id'].")";
            $run_qry = $this->db->Query($query2);
            $result2 = $this->db->FetchAllArray($run_qry);
            $store_name = array();
            $store_name = array_column($result2, 'store_name');
            //$result[$key]['store_names'] = array_combine(explode(',', $account['store_id']), $store_name);
            $result[$key]['store_names'] = implode(', ',$store_name);
        }
         return $result; 
    }
}