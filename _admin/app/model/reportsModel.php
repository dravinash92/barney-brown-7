<?php 
class ReportsModel extends Model
{

	public $db; 
	//database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}	
	
	public function getPickupStores(){ 
		$get_url     = API_URL.'cart/listPickupAddress/';	
		$json          = $this->receive_data($get_url,array());
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function getOrders($data=array()){		
		$get_url     = API_URL.'reports/getOrders';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;	    
	}	
	
	public function getDiscountsList(){
		$get_url     = API_URL.'reports/getDiscountsList';	
		$json          = $this->receive_data($get_url,array());
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function getSystemUsers(){
		$get_url     = API_URL.'reports/getSystemUsers';	
		$json          = $this->receive_data($get_url,array());
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function doTotalSalesFilterReports($data){
		$get_url     = API_URL.'reports/doTotalSalesFilterReports';	
		$json          = $this->receive_data($get_url,$data);
		
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function getSandwichItems($data=array()){
		$get_url     = API_URL.'reports/sandwichitems';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getStandaredItems($data=array()){
		$get_url     = API_URL.'reports/standareditems';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getStandaredItemsQty($data=array()){
		$get_url     = API_URL.'reports/standareditemsqty';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getCustomItems($data=array()){
		$get_url     = API_URL.'reports/customitems';
		$json          = $this->receive_data($get_url,$data); 
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function doTotalItemsFilterReports($data){
		$get_url     = API_URL.'reports/doTotalItemsFilterReports';
		$json          = $this->receive_data($get_url,$data);
		
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function doTotalCustomItemsFilterReports($data){
		$get_url     = API_URL.'reports/doTotalCustomItemsFilterReports';
		 $json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	public function getOrderedItems(){		
		$get_url     = API_URL.'reports/getOrderedItems';
		$json          = $this->receive_data($get_url,array());		
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getHourlyOrderReport($post)
	{		
		$get_url     = API_URL.'reports/getHourlyOrderReport';
		$json          = $this->receive_data($get_url,$post);	
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getOrderDetails($post){
		$get_url     = API_URL.'reports/getOrderDetails';
		$json          = $this->receive_data($get_url,$post);	
		$finalData = @json_decode($json);
		return $finalData->Data;	
	}
	
	public function checkReportPin($post){
		$get_url     = API_URL.'reports/checkReportPin';
		$json          = $this->receive_data($get_url,$post);	
		$finalData = @json_decode($json);
		return $finalData->Data;	
	}
	
	public function getStandardCategory($post=array()){
		$get_url     = API_URL.'reports/getStandardCategory';
		$json          = $this->receive_data($get_url,$post);	
		$finalData = @json_decode($json);
		return $finalData->Data;	
	}
	
	public function getCateringCategory($post=array()){
		$get_url     = API_URL.'reports/getCateringCategory';
		$json          = $this->receive_data($get_url,$post);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getTotalSaleCount($data){
		$get_url     = API_URL.'reports/getTotalSaleCount';
		 $json          = $this->receive_data($get_url,array('ids'=>$data));
		 $finalData = @json_decode($json);
		 return $finalData->Data;
	}
	public function get_user_delivery_details($del_uid)
	{
		$qry = "SELECT o.order_id, o.order_number as OrderNumber, concat(DATE_FORMAT(o.order_date, '%m/%d/%y'), ' ' ,DATE_FORMAT(o.order_date,'%h:%i %p ')) as Placement, concat(DATE_FORMAT(o.order_delivered, '%m/%d/%y'), ' ' ,DATE_FORMAT(o.order_delivered,'%h:%i %p ')) as Fulfillment,(SELECT CONCAT(u.first_name,' ', u.last_name) FROM users u WHERE o.user_id = u.uid)as Name , o.sub_total as SubTot, o.tax, o.delivery_fee as Fee, o.tip, o.total as Value, o.off_amount as Dscnt, (o.total - o.off_amount) as PaidAmt FROM orders o WHERE o.delivery_assigned_to = ".$del_uid." AND o.order_delivered >=  DATE_SUB(NOW(), INTERVAL 30 DAY) ORDER BY o.order_delivered DESC LIMIT 30" ;
		$run_qry = $this->db->Query($qry);
       return $this->db->FetchAllArray($run_qry);
	}
	public function get_user_delivery_details_byDate($post)
	{
		$from_date  = date("Y-m-d", strtotime($post['from_date']));
        $to_date  = date("Y-m-d", strtotime($post['to_date']));

        if (isset($post['from_time']))
                $from_date .= " " . $post['from_time'] . ":00";
            else
                $from_date .= " 00:00:00";

        if (isset($post['to_time']))
                $to_date .= " " . $post['to_time'] . ":00";
            else
                $to_date .= " 00:00:00";

		$qry = "SELECT o.order_id, o.order_number as OrderNumber, concat(DATE_FORMAT(o.order_date, '%m/%d/%y'), ' ' ,DATE_FORMAT(o.order_date,'%h:%i %p ')) as Placement, concat(DATE_FORMAT(o.order_delivered, '%m/%d/%y'), ' ' ,DATE_FORMAT(o.order_delivered,'%h:%i %p ')) as Fulfillment,(SELECT CONCAT(u.first_name,' ', u.last_name) FROM users u WHERE o.user_id = u.uid) as Name , o.sub_total as SubTot, o.tax, o.delivery_fee as Fee, o.tip, o.total as Value, o.off_amount as Dscnt, (o.total - o.off_amount) as PaidAmt FROM orders o WHERE o.delivery_assigned_to = ".$post['uid']." AND (o.order_delivered BETWEEN '".$from_date."' AND '".$to_date."') ORDER BY o.order_delivered DESC LIMIT 30";
		
		$run_qry = $this->db->Query($qry);
       	return $this->db->FetchAllArray($run_qry);
	}

	function get_delivery_user($uid)
    {
        $query  = "SELECT * FROM delivery_user WHERE uid = ".$uid;
         return $this->db->FetchArrayRow($this->db->Query($query));
    }
}
