<?php

class DiscountsModel extends Model
{

	public $db; 
	//database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}
    
    function get_discount_list($apiUrl, $data ){
			$url     = $apiUrl.'discounts/get_discount_list';
			$json    = $this->receive_data($url,$data);
			return $json;
	}	
	
	function add_discount($apiUrl, $data ){
		$url     = $apiUrl.'discounts/add_discount';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_discount_row($apiUrl, $data ){
		
		$url     = $apiUrl.'discounts/get_discount_row';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	function update_discount($apiUrl, $data ){
	
		$url     = $apiUrl.'discounts/update_discount';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function delete_items($apiUrl, $data ){
		$url     = $apiUrl.'discounts/delete_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function update_is_live($apiUrl, $data ){
		$url     = $apiUrl.'discounts/update_is_live';
		$json    = $this->receive_data($url, $data);
		return $json;
	}
}