<?php

class SandwichModel extends Model
{
    public $db; 
    //database connection object
    
    public function __construct()
    {
        $this->db = parent::__construct();
    }

    public function get_all_sandwich_data($param)
    {
         $get_url = API_URL . 'sandwich/get_admin_gallery_data/';
        $json    = $this->receive_data($get_url, $param);
        return json_decode($json);
    }
    
    public function process_user_data($data)
    {
        $self = $this;
        array_walk($data, function(&$value, $key) use ($self)
        {
            $value = get_object_vars($value);
        });
        return $data;
    }
    
    public function get_purchase($id)
    {
        $get_url = API_URL . 'sandwich/get_purchase/';
        $json    = $this->receive_data($get_url, array(
            'id' => $id
        ));
        $json    = json_decode($json);
        return $json->Data[0]->count;
    }
    
    public function get_user($id)
    {
        $get_url = API_URL . 'user/get/';
        $json    = $this->receive_data($get_url, array(
            'user_id' => $id
        ));
        $data    = json_decode($json);
        #print_r( $da )
        return array(
            'name' => $data->Data[0]->first_name . ' ' . $data->Data[0]->last_name,
            'email' => $data->Data[0]->username
        );
    }
    
    function _pre_process_user_data($data)
    {
        $self = $this;
        array_walk($data, function(&$value, $key) use ($self)
        {
            $value = get_object_vars($value);
            
            $Sanwichdata = json_decode($value['sandwich_data']);
            if (isset($Sanwichdata))
                $array = get_object_vars($Sanwichdata);
            else
                $array = array();
            array_walk($array, function(&$val, $key)
            {
                $val = get_object_vars($val);
                
                
                $val['item_qty'] = get_object_vars($val['item_qty']);
            });
            $value['sandwich_data_json'] = $value['sandwich_data'];
            $value['sandwich_data']      = $array;
            $value['purchase']           = $self->get_purchase($value['id']);
            $usrinfo                     = $self->get_user($value['uid']);
            $value['user_name']          = $usrinfo['name'];
            $value['user_email']         = $usrinfo['email'];
            $value['formated_date']      = date('m/d/y', strtotime($value['date_of_creation']));
        });
        return $data;
    }
    
    
    function option_data($id)
    {
        $url = API_URL . 'sandwich/get_item_options/';
        return $data = $this->receive_data($url, array(
            'id' => $id
        ));
    }
    
    public function input_compatible_data($data)
    {
        $self = $this;
        
        array_walk($data, function(&$value, $key) use ($self)
        {
            $value = get_object_vars($value);
            if ($value['options_id']) {
                $optn_id = explode(':', $value['options_id']);
                array_walk($optn_id, function(&$val, $key) use ($self)
                {
                    $val     = json_decode($self->option_data($val));
                    $fin_val = $val->Data;
                    array_walk($fin_val, function(&$final, $key)
                    {
                        $final = get_object_vars($final);
                    });
                    $val = $fin_val[0];
                });
                $value['options_id'] = $optn_id;
            }
        });
        
        return $data;
    }
    
    public function load_data($id)
    {
        $url   = API_URL . 'sandwich/get_item/';
        $data  = $this->receive_data($url, array(
            'item_id' => $id
        ));
        $final = json_decode($data);
        return $final;
    }
    
    public function set_public()
    {
        $get_url = API_URL . 'sandwich/update_gallery_public_state/';
        $json    = $this->receive_data($get_url, $_POST);
    }
    
    function delete_gallery()
    {
        $get_url = API_URL . 'sandwich/delete_gallery_item/';
        echo $json = $this->receive_data($get_url, $_POST);
        
    }
    
    public function input_user_sanwich_data($data)
    {
        
        if ($data['id'] && $data['uid'] && ($data['name'] == 0)) {
            $url = API_URL . 'sandwich/update_sandwich_data/';
        } else if ($data['name'] == 1) {
            $url = API_URL . 'sandwich/add_sandwich_data/';
        }
        
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    public function toggle_flag()
    {
        $params  = array(
            'id' => $_POST['id'],
            'state' => $_POST['status']
        );
        $get_url = API_URL . 'sandwich/toggle_flag/';
        return $json = $this->receive_data($get_url, $params);
    }
    function addLike($data)
    {
        $url  = API_URL . 'sandwich/addAdminLike/';
        $json = $this->receive_data($url, array(
            'id' => $data['id'],
            'count' => $data['count']
        ));
        return $json;
    }
    function addPuchase($data)
    {
        $url  = API_URL . 'sandwich/addPuchase/';
        $json = $this->receive_data($url, array(
            'id' => $data['id'],
            'count' => $data['count']
        ));
        return $json;
    }
    
    public function addSandwichToSOTD($data = array())
    {
        $url  = API_URL . 'sandwich/addSandwichToSOTD/';
        $json = $this->receive_data($url, $data);
        $json = json_decode($json);
        return $json->Data;
        
    }
    
    public function getSandwichOfTheDay($data = array())
    {
        $url  = API_URL . 'sandwich/getSandwichOfTheDay/';
        $json = $this->receive_data($url, $data);
        $json = json_decode($json);
        return $json->Data;
        
    }
    
    public function removeSandwichToSOTD($data = array())
    {
        $url  = API_URL . 'sandwich/removeSandwichToSOTD/';
        $json = $this->receive_data($url, $data);
        $json = json_decode($json);
        return $json->Data;
        
    }
    
    public function addSandwichToTRND($data = array())
    {
        $url  = API_URL . 'sandwich/addSandwichToTRND/';
        $json = $this->receive_data($url, $data);
        $json = json_decode($json);
        return $json->Data;
    }
	
    public function setSandwichPriority($data = array())
    {
		/*Please don't change given below code without full understanding / flow*/
       // $url  = API_URL . 'sandwich/setSandwichPriority/';		
        $url  = API_URL . 'sandwich/addSandwichToTRND/';		
        $json = $this->receive_data($url, $data);		
        $json = json_decode($json); 
        return $json->Data;
    }
    
    public function getSandwichOfTrending($data = array())
    {
        $url  = API_URL . 'sandwich/getSandwichOfTrending/';
        $json = $this->receive_data($url, $data);
        $json = json_decode($json);
        return $json->Data;
    }
    
    public function removeSandwichToTRND($data = array())
    {
        $url  = API_URL . 'sandwich/removeSandwichToTRND/';
        $json = $this->receive_data($url, $data);
        $json = json_decode($json);
        return $json->Data;
    }
    
    public function setTrndLive($data = array())
    {
        $url = API_URL . 'sandwich/setTrndLive/';
        echo $json = $this->receive_data($url, $data);
        $json = json_decode($json);
        return $json->Data;
    }
    
    public function getAllproductsExtras()
    {
        
        $get_url = API_URL . 'categoryitems/getAllproductsExtras/';
        $json    = $this->receive_data($get_url, array());
        echo $json;
    }
    
    public function get_gallery_data_count($data = array()){
    	$get_url     = API_URL.'sandwich/get_gallery_data_count/';
    	$json          = $this->receive_data($get_url,$data);
    	$finalData = @json_decode($json);
    	return $finalData->Data;
    }
    public function filter_seacrh_ajax_more($data){
    	
    	 $sandwich = $this->receive_data(API_URL . 'sandwich/filter_seacrh_ajax_more_admin/', array('limit'=>$data['limit'],'start'=>$data['start']));
    	 $sandwich = json_decode($sandwich);
    	return $sandwich;
    }
    function getSandwichCategoryItems()
    {
    
    	$url       = API_URL.'sandwich/getSandwichCategoryItems/';
    	$json      = $this->receive_data($url,array('uid'=>@$_SESSION['uid']));
    	$data      = json_decode($json);
    
    
    	return  $data;
    }
    
    function getSandwichCategories()
    {
    
    	$url       = API_URL.'sandwich/getSandwichCategories/';
    	$json      = $this->receive_data($url,array('uid'=>@$_SESSION['uid']));
    	$data      = json_decode($json);
    
    
    	return  $data;
    }
    public function filters($filter){
 
        if( isset( $filter->sort_id ) && $filter->sort_id > 0 ){
            $params= array( 'sort_id'=>$filter->sort_id );
        }
        else{
            $params= array( );
        }

    	$get_url  = API_URL . 'sandwich/getAllsandwichDetailsSearch/';
    	$sandwich = $this->receive_data( $get_url,$params );

    	$data = json_decode($sandwich);
    	$data = @$data->Data;
    	$_self = $this;
    	array_walk($data,function(&$value,$key) use ($_self) {
    		if($value->sandwich_data){ $vals = $value->sandwich_data;
    		$vals = json_decode($vals);
    		$vals = get_object_vars($vals);
    		$value->sandwich_data =  $_self->arraYprocessfilter($vals);
    		}
    	});
    	
    		return $data;
    }
    public function arraYprocessfilter($v){
    	array_walk($v,function(&$v,$k){
    		$v = $v->item_name;
    	});
    		return $v;
    }
    public function filter_seacrh_ajax_admin($ids,$sort,$limit,$start =0){
    	$ids = implode(",",$ids);

    	$sandwich = $this->receive_data(API_URL . 'sandwich/filter_seacrh_ajax_admin/', array('id'=>$ids,'sortBy'=>$sort,'limit'=>$limit, 'start' => $start));
        //print $sandwich;die;
    	$sandwich = json_decode($sandwich);
    	return $sandwich;
    }
    function get_searchItem_count($data)
    {
        $url       = API_URL.'sandwich/get_searchItem_count/';
        $json      = $this->receive_data($url,$data);
        $result      = json_decode($json);
        return  $result->Data;
        
    }
    function sandwich_filter($data)
    {
        $url       = API_URL.'sandwich/sandwich_filter/';
        $json      = $this->receive_data($url,$data);
        $result      = json_decode($json);
        return  $result->Data;
    }
    function sandwich_filter_count($data)
    {
        $url       = API_URL.'sandwich/sandwich_filter_count/';
        $json      = $this->receive_data($url,$data);
        $result      = json_decode($json);
        return  $result->Data;
    }

    function more_sandwich_filter($data)
    {
        $url       = API_URL.'sandwich/more_sandwich_filter/';
        $json      = $this->receive_data($url,$data);
        $result      = json_decode($json);
        return  $result->Data;
    }
    public function gallery_load_data($post)
    {
        $url       = API_URL.'sandwich/gallery_load_data/';
        $json      = $this->receive_data($url,$post);
        $result      = json_decode($json);
        return  $result->Data;
    }
}