<?php

class MenuModel extends Model
{

	public $db; 
	//database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}
	

	function get_standard_catgory_items($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_standard_catgory_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_catering_catgory_items($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_catering_catgory_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

	function insert($apiUrl,$data){
	
		$url     = $apiUrl.'categoryitems/add_standard_catgory_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function insert_standard_products($apiUrl,$data){
	$url     = $apiUrl.'categoryitems/add_standard_catgory_products';
	$json    = $this->receive_data($url,$data);
	return $json;
	}
	
	
	function get_standard_catgory_items_list($apiUrl, $data ){

		$url     = $apiUrl.'categoryitems/get_standard_catgory_items_list';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

	function get_standard_product_items($apiUrl, $data ){

		$url     = $apiUrl.'categoryitems/get_standard_product_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

	function update_standard_category_products($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/update_standard_category_products';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

	function delete_standard_category_products($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_standard_category_products';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function update_standard_category_priority($apiUrl, $data){
		$url     = $apiUrl.'categoryitems/update_standard_category_priority';
		$json    = $this->receive_data($url,$data);
	
		return $json;
		
	}
	
	function get_all_pickups_items($apiUrl, $data ){
		ini_set('display_errors','1');
		$url     = $apiUrl.'categoryitems/get_all_pickups_items';
		$json    = $this->receive_data($url,$data);
		
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	function  process_user_data($data){
		$self  = $this;
		array_walk($data,function(&$value,$key) use($self){
			$value = get_object_vars($value);
			$array = get_object_vars(json_decode($value['sandwich_data']));
			array_walk($array,function(&$val,$key){
				$val = get_object_vars($val);
			});
			$value['sandwich_data'] = $array;
			$value['user_name']  = $self->get_user($value['uid']);
			$value['formated_date'] = date('m/d/y',strtotime($value['date_of_creation']));
		});
		return $data;
	}
	
	
	function get_all_order_status($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_all_order_status';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function update_order_status($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/update_order_status';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function get_all_delivery_items($apiUrl, $data ){
		ini_set('display_errors','1');
		$url     = $apiUrl.'categoryitems/get_all_delivery_items';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function get_search_delivery_items($apiUrl,$data ){
		
		$url     = $apiUrl.'categoryitems/get_search_delivery_items';
		$json    = $this->receive_data($url,$data);
		
		$finalData = json_decode($json);
		return $finalData->Data;
		
	}
	
	function get_admin_user_data($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/get_admin_user_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
		
	}
	
	function get_admin_user_category($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/get_admin_user_category';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	
	}
	
	function add_new_admin_user($apiUrl,$data){
			$url     = $apiUrl.'categoryitems/add_new_admin_user';
			$json    = $this->receive_data($url,$data);
			return $json;		
	}
	
	function get_admin_user($apiUrl, $data){

		$url     = $apiUrl.'categoryitems/get_admin_user';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}	
	
	
	function update_admin_user($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/update_admin_user';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function delete_admin_user($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_admin_user';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_store_accounts_data($apiUrl, $data){
	
		$url     = $apiUrl.'categoryitems/get_store_accounts_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function add_new_store_account($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/add_new_store_account';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_store_account($apiUrl, $data){
	
		$url     = $apiUrl.'categoryitems/get_store_account';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function update_store_account($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/update_store_account';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function delete_store_account($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_store_account';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function add_webpage_banner_data($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/add_webpage_banner_data';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	
	function get_web_homepage_data($apiUrl, $data){	
		$url     = $apiUrl.'categoryitems/get_web_homepage_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	
	function update_web_home_page_priority($apiUrl, $data){
		$url     = $apiUrl.'categoryitems/update_web_home_page_priority';
		$json    = $this->receive_data($url,$data);
		return $json;
	
	}
	
	function get_web_homepage_banner($apiUrl, $data){
	
		$url     = $apiUrl.'categoryitems/get_web_homepage_banner';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	
	function update_webpage_banner_data($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/update_webpage_banner_data';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	
	function update_webpage_data($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/update_webpage_data';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	
	function get_webpages_data($apiUrl, $data){		
		$url     = $apiUrl.'categoryitems/get_webpages_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function get_webpagedata_data($apiUrl, $data){
		$url     = $apiUrl.'categoryitems/get_webpagedata_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	
	function delete_home_page_banner($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_home_page_banner';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_sandwich_custom_category_items($apiUrl, $data){
		$url     = $apiUrl.'categoryitems/get_sandwich_custom_category_items';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function get_custom_catgory_items($apiUrl, $data ){
			$url     = $apiUrl.'categoryitems/get_custom_catgory_items';
			$json    = $this->receive_data($url,$data);
			return $json;
	}
	
	function get_custom_catgory_items_list($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_custom_catgory_items_list';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_sandwich_category_items($apiUrl, $data ){
		
		$url     = $apiUrl.'categoryitems/get_sandwich_category_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function update_sandwich_category($apiUrl, $data ){
		 $url     = $apiUrl.'categoryitems/update_sandwich_category_items';
		 $json    = $this->receive_data($url,$data); 
		 return $json;
	}
	
	function delete_sandwich_category_items($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_sandwich_category_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

	function delete_category_items($data)
	{
		$q1  = "SELECT * FROM `sandwich_category_items` WHERE `id` = '" . $data['id'] . "';";
		$r1 = $this->db->Query($q1);
		$res = $this->db->FetchArrayRow($r1);
		$path = $_SERVER['DOCUMENT_ROOT']. 'upload/';
		if (!empty(@$res['item_image']) && file_exists($path. @$res['item_image'])) unlink($path . @$res['item_image']);
        if (!empty(@$res['item_image_sliced']) && file_exists($path. @$res['item_image_sliced'])) unlink($path. @$res['item_image_sliced']);
        if (!empty(@$res['image_long']) && file_exists($path. @$res['image_long'])) unlink($path. @$res['image_long']);
        if (!empty(@$res['image_round']) && file_exists($path. @$res['image_round'])) unlink($path. @$res['image_round']);
        if (!empty(@$res['image_trapezoid']) && file_exists($path. @$res['image_trapezoid'])) unlink($path . @$res['image_trapezoid']);
		
		$optionImgs = get_object_vars(json_decode($res['option_images']));
        foreach ($optionImgs as $key => $value) 
        {
            $optionImgs[$key] = get_object_vars($value);
        }
        foreach ($optionImgs as $key => $value) 
        {
        	foreach ($value as $key)
        	{
        		 
        		$filePath =  $_SERVER['DOCUMENT_ROOT']. 'upload/'. @$key;
				unlink($filePath);
        		
        	}
        	
        }
		$query      = "DELETE FROM `sandwich_category_items` WHERE `id` = '" . $data['id'] . "';";
		return $this->db->Query($query);
	}
	
	function add_catgory_items($apiUrl,$data){
		
			$url     = $apiUrl.'categoryitems/add_catgory_items';
			$json    = $this->receive_data($url,$data);
			return $json;
	}	
	
	function get_individual_item_count(){
		    $url     = API_URL.'categoryitems/get_individual_item_count/';
			 $json    = $this->receive_data($url,array()); 
			return $json;
	}
	
	function process_data($data){
		if(is_array($data)){
			array_walk($data,function(&$val,$key){
				$val = get_object_vars($val);
			});
		 return $data;	
		}
	}
	
	function updateStandardCategoryPriority($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/updateSandwichCategoryItems';
		$json    = $this->receive_data($url,$data);
		return $json;
		
	}
	function deleteStandardCategory($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/deleteStandardCategory';
		$json    = $this->receive_data($url,$data);
		return $json;
		
	}
	
	function get_standard_product_extraData($apiUrl,$data){
		$url   = $apiUrl.'categoryitems/get_standard_product_extraData';
		$json  = $this->receive_data($url,$data);
		$data  = json_decode($json); 
		return $this->process_extraData($data->Data);
 	}
 	
 	
 	function edit_options($id){
 		
 		$url = API_URL.'menu/edit_options/';
 		$json  = $this->receive_data($url,array('id'=>$id));
 		$data  = json_decode($json);
 		return $data->Data;
 	}
 	
 	function  deleteOption($id){
 	$url   = API_URL.'menu/deleteOption/';
    $json  = $this->receive_data($url,array('id'=>$id));
 	}
 	
 	function addoptions($post){ 
 		$url = API_URL.'menu/addoptions/';
 		$json  = $this->receive_data($url,$post);
 	}
 	
 	function updateoptionstatus()
 	{
 		$id = $_POST['hid'];
 		foreach($id as $id){
 			$idstring = 'live'.$id;
 			if(isset($_POST[$idstring])) { $_POST['live'][] = 1; unset($_POST[$idstring]); } 
 			else  $_POST['live'][] = 0;
 		}
 		
        $url   = API_URL.'menu/updateoptionstatus/';
 	    $json  = $this->receive_data($url,$_POST);
 		  
 	}
 	
 	
 	function process_extraData($data){
 		array_walk($data,function(&$val,$key){
 			$val = get_object_vars($val);
 			$extra = $val['extra'];
 			array_walk($extra,function(&$values,$keys){
 				$values = get_object_vars($values);
 			});
 				$val['extra'] = $extra;
 		});
 		return $data;
 	}
 	
 	function fill_unique_slugs(){
 		$url  = API_URL.'menu/fill_unique_slugs';
 	}
 	
 	
}