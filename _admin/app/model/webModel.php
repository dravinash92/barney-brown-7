<?php

class WebModel extends Model
{

	public $db; 
	//database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}

	function get_standard_catgory_items($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_standard_catgory_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

	function insert($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/add_standard_catgory_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function insert_standard_products($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/add_standard_catgory_products';
		$json    = $this->receive_data($url,$data);
		print_r($json);exit;
		return $json;
	}
	
	
	function get_standard_catgory_items_list($apiUrl, $data ){

		$url     = $apiUrl.'categoryitems/get_standard_catgory_items_list';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

	function get_standard_product_items($apiUrl, $data ){

		$url     = $apiUrl.'categoryitems/get_standard_product_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

	function update_standard_category_products($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/update_standard_category_products';
		$json    = $this->receive_data($url,$data);
		
		return $json;
	}

	function delete_standard_category_products($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_standard_category_products';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function update_standard_category_priority($apiUrl, $data){
		
		$url     = $apiUrl.'categoryitems/update_standard_category_priority';
		$json    = $this->receive_data($url,$data);
		return $json;
		
	}
	
	function get_all_pickups_items($apiUrl, $data ){
		ini_set('display_errors','1');
		$url     = $apiUrl.'categoryitems/get_all_pickups_items';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	function  process_user_data($data){
		$self  = $this;
		array_walk($data,function(&$value,$key) use($self){
			$value = get_object_vars($value);
			$array = get_object_vars(json_decode($value['sandwich_data']));
			array_walk($array,function(&$val,$key){
				$val = get_object_vars($val);
			});
			$value['sandwich_data'] = $array;
			$value['user_name']  = $self->get_user($value['uid']);
			$value['formated_date'] = date('m/d/y',strtotime($value['date_of_creation']));
		});
		return $data;
	}
	
	
	function get_all_order_status($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_all_order_status';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function update_order_status($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/update_order_status';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function get_all_delivery_items($apiUrl, $data ){
		ini_set('display_errors','1');
		$url     = $apiUrl.'categoryitems/get_all_delivery_items';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function get_search_delivery_items($apiUrl,$data ){
		
		$url     = $apiUrl.'categoryitems/get_search_delivery_items';
		$json    = $this->receive_data($url,$data);
		
		$finalData = json_decode($json);
		return $finalData->Data;
		
	}
	
	function get_admin_user_data($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/get_admin_user_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
		
	}
	
	function get_admin_user_category($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/get_admin_user_category';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	
	}
	
	function add_new_admin_user($apiUrl,$data){
			$url     = $apiUrl.'categoryitems/add_new_admin_user';
			$json    = $this->receive_data($url,$data);
			return $json;		
	}
	
	function get_admin_user($apiUrl, $data){

		$url     = $apiUrl.'categoryitems/get_admin_user';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}	
	
	
	function update_admin_user($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/update_admin_user';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function delete_admin_user($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_admin_user';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_store_accounts_data($apiUrl, $data){
	
		$url     = $apiUrl.'categoryitems/get_store_accounts_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function add_new_store_account($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/add_new_store_account';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_store_account($apiUrl, $data){
	
		$url     = $apiUrl.'categoryitems/get_store_account';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function update_store_account($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/update_store_account';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function delete_store_account($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_store_account';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	function add_webpage_banner_data($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/add_webpage_banner_data';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	
	function get_web_homepage_data($apiUrl, $data){	
		$url     = $apiUrl.'categoryitems/get_web_homepage_data';
		$json    = $this->receive_data($url,$data);		
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	
	function update_web_home_page_priority($apiUrl, $data){
	
		$url     = $apiUrl.'categoryitems/update_web_home_page_priority';
	    $json    = $this->receive_data($url,$data);		
		return $json;
	
	}
	
	function get_web_homepage_banner($apiUrl, $data){
	
		$url     = $apiUrl.'categoryitems/get_web_homepage_banner';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	
	function update_webpage_banner_data($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/update_webpage_banner_data';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	
	
	function update_webpage_data($apiUrl,$data){
		$url     = $apiUrl.'categoryitems/update_webpage_data';
		$json    = $this->receive_data($url,$data);		
		return $json;
	}
	
	
	
	function get_webpages_data($apiUrl, $data){		
		$url     = $apiUrl.'categoryitems/get_webpages_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	function get_webpagedata_data($apiUrl, $data){
		$url     = $apiUrl.'categoryitems/get_webpagedata_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	
	function delete_home_page_banner($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/delete_home_page_banner';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_sandwich_category_items($apiUrl, $data){
		$url     = $apiUrl.'categoryitems/get_sandwich_custom_category_items';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
}