<?php


class CreateSandwichModel extends Model
{
    
    public $db; 
    //database connection object
    
    public function __construct()
    {
        $this->db = parent::__construct();
    }
    
    public function load_data($id)
    {
        $url   = API_URL . 'sandwich/get_item/';
        $data  = $this->receive_data($url, array(
            'item_id' => $id
        ));
        $final = json_decode($data);
        return $this->convert_array($final->Data);
    }
    
    public function input_user_sanwich_data($data)
    {
        
        if ($data['id'] && $data['uid'] && ($data['tempname'] == 0)) {
            // update
            $url = API_URL . 'sandwich/update_sandwich_data/';
        } else if ($data['tempname'] == 1) {
            // edit
            $url = API_URL . 'sandwich/add_sandwich_data/';
        }
        
        $json = $this->receive_data($url, $data);
        return $json;
    }
    
    public function filter_words()
    {
        $word    = $_POST['word'];
        $get_url = API_URL . 'sandwich/filterWord/';
        $json    = $this->receive_data($get_url, array(
            'word' => $word
        ));
        $data    = json_encode($json);
        return $data;
    }
    
    
    public function get_user_sanwich_data($id, $uid, $pub = false)
    {
        $get_url = API_URL . 'sandwich/get_sandwich_data/';
        if ($pub == true) {
            $datArr = array(
                'id' => $id,
                'uid' => $uid,
                'is_pub' => 1
            );
        } else {
            $datArr = array(
                'id' => $id,
                'uid' => $uid
            );
        }
        $json = $this->receive_data($get_url, $datArr);
        return $json;
    }
    
    
    
    function option_data($id)
    {
        $url = API_URL . 'sandwich/get_item_options/';
        return $data = $this->receive_data($url, array(
            'id' => $id
        ));
    }
    
    function process_user_data($data)
    {
        array_walk($data, function(&$value, $key)
        {
            $value = get_object_vars($value);
            $array = get_object_vars(json_decode($value['sandwich_data']));
            array_walk($array, function(&$val, $key)
            {
                $val = get_object_vars($val);
            });
            $value['sandwich_data'] = $array;
        });
        return $data;
    }
    
    public function convert_array($array)
    {
        $_this = $this;
        array_walk($array, function(&$value, $key) use ($_this)
        {
            $value = get_object_vars($value);
            if ($value['options_id']) {
                $optn_id = explode(':', $value['options_id']);
                array_walk($optn_id, function(&$val, $key) use ($_this)
                {
                    $val     = json_decode($_this->option_data($val));
                    $fin_val = $val->Data;
                    array_walk($fin_val, function(&$final, $key)
                    {
                        $final = get_object_vars($final);
                    });
                    $val = $fin_val[0];
                });
                $value['options_id'] = $optn_id;
            }
            if($value['option_images']){
                $value['option_images'] = json_decode($value['option_images'], true);
            }
        });
        return $array;
    } 
    
    public function get_random_name()
    {
        $url  = API_URL . 'sandwich/get_random_name/';
        $data = $this->receive_data($url, array());
        return $data;
    }
    
    
    public function get_all_images()
    {
        $final = array();
        
        $url  = API_URL . 'sandwich/get_all_images/';
        $data = $this->receive_data($url, array());
        $data = json_decode($data);
        $data = $data->Data;
        
        foreach ($data as $data) {
            
            $im1 = @getimagesize(ADMIN_URL . 'upload/' . $data->item_image);
            $im2 = @getimagesize(ADMIN_URL . 'upload/' . $data->image_round);
            $im3 = @getimagesize(ADMIN_URL . 'upload/' . $data->image_long);
            $im4 = @getimagesize(ADMIN_URL . 'upload/' . $data->image_trapezoid);
            $im5 = @getimagesize(ADMIN_URL . 'upload/' . $data->item_image_sliced);
            
            if (isset($im1['mime']))
                $final[] = $data->item_image;
            if (isset($im2['mime']))
                $final[] = $data->image_round;
            if (isset($im3['mime']))
                $final[] = $data->image_long;
            if (isset($im4['mime']))
                $final[] = $data->image_trapezoid;
            if (isset($im5['mime']))
                $final[] = $data->item_image_sliced;
        }
        
        return $final;
    }
    
    public function get_user_firstName($id)
    {
        $get_url = API_URL . 'user/get/';
        $json    = $this->receive_data($get_url, array(
            'user_id' => $id
        ));
        $data    = json_decode($json);
        if (count($data->Data) > 0)
            return $name = $data->Data[0]->first_name;
        else
            return '';
    }
    
    public function get_category_items_data($ids)
    {
        $url  = API_URL . 'sandwich/get_category_items_data/';
        $data = $this->receive_data($url, array(
            'ids' => $ids
        ));
        return $data;
    }
}