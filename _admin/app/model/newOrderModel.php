<?php

class NewOrderModel extends Model
{

	public $db; 
	//database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}
	
	public function get_customer_data($data){
		$get_url     = API_URL.'customer/getCustomerData/';
		$json          = $this->receive_data($get_url,$data);
		return json_decode($json);
	
	}
	
	public function getCustomerNotes($post){
		$get_url     = API_URL.'customer/getCustomerNotes/';	
		$json          = $this->receive_data($get_url, $post);
		$json = json_decode($json);
		return $json->Data;		
	}
	
	public function getUserDetails($uid){
		$data['user_id'] = $uid;
		$get_url     = API_URL.'user/get/';
		$json          = $this->receive_data($get_url,$data);
		$json = json_decode($json);
		return $json->Data[0];
	
	}
	
	public function getUserOrdersInfo($uid){
		$data['user_id'] = $uid;
		$get_url     = API_URL.'cart/getUserOrdersInfo/';
		$json          = $this->receive_data($get_url,$data);
		$json = json_decode($json);
		if(isset($json->Data[0]))
			return $json->Data[0];
		else
			return 0;
	
	}
	
	public function getUserAddresses($uid){
		$data['uid'] = $uid;
		$get_url     = API_URL.'myaccount/listSavedAddress/';
		$json          = $this->receive_data($get_url,$data);
		$json = json_decode($json);
		return $json->Data;
	}
	
	public function gallery_load_data(){		
		$get_url     = API_URL.'sandwich/get_gallery_data_admin/';	
		$json          = $this->receive_data($get_url,array());
		$json = json_decode($json);
		return $json->Data;
	}
        
        public function gallery_load_customer_data($user_id){		
		$get_url     = API_URL.'sandwich/get_gallery_data_admin/';	
		$json          = $this->receive_data($get_url,array('uid' => $user_id));
		$json = json_decode($json);
		return $json->Data;
	}
	
	function  process_user_data($data){
		$self  = $this;
		array_walk($data,function(&$value,$key) use($self){
			//$value = get_object_vars($value);
			$value = get_object_vars((object)$value);
			$array = get_object_vars(json_decode($value['sandwich_data']));
			array_walk($array,function(&$val,$key){
				$val = get_object_vars($val);
			});

			$bread     = $array['BREAD']['item_name'][0];
			$protein   = $self->getCombinedString($array['PROTEIN']['item_qty']); 
			$cheese    = $self->getCombinedString($array['CHEESE']['item_qty']);  
			$topping   = $self->getCombinedString($array['TOPPINGS']['item_qty']); 
			$condiment = $self->getCombinedString($array['CONDIMENTS']['item_qty']); 

			// item ids 
			$bread_id     = $array['BREAD']['item_id'][0];
			$protein_id   = implode(', ',$array['PROTEIN']['item_id']); 
			$cheese_id    = implode(', ',$array['CHEESE']['item_id']);  
			$topping_id   = implode(', ',$array['TOPPINGS']['item_id']); 
			$condiment_id = implode(', ',$array['CONDIMENTS']['item_id']);
			    
		    $desc = $bread.", ".$protein.", ".$cheese.", ".$topping.", ".$condiment; 
			$descData  = explode(', ',$desc);
			$descData  = array_filter($descData);
		    $desc      = implode(', ',$descData);

		    $desc_id = $bread_id.", ".$protein_id.", ".$cheese_id.", ".$topping_id.", ".$condiment_id;
		    $descDataId  = explode(', ',$desc_id);
			$descDataId  = array_filter($descDataId);
		    $desc_id      = implode(', ',$descDataId);

		    if(!isset($data['data_from'])){
				$brd = $bread;
				$total = $this->get_sandwich_current_price_menu($brd,$desc,$desc_id);
				$value['current_price'] = $total;
			}

			$value['sandwich_name_trimmed'] = $self->word_processor($value['sandwich_name']);
			//$value['like_id']       = $like_id;
			$value['sandwich_data'] = $array;
			$value['sandwich_desc'] = $desc;
			$value['sandwich_desc_id'] = $desc_id;
			
			$value['user_name']     = $self->get_user($value['uid']);
			$value['formated_date'] = date('m/d/y',strtotime($value['date_of_creation']));
		});
		return $data;
	}
	/* newly added functions */
	public function get_sandwich_current_price_menu($bread,$desc,$desc_id)
	{
		$params      = array('bread'=>$bread,'desc'=>$desc,'desc_id'=>$desc_id);
		$get_url     = API_URL.'sandwich/get_sandwich_current_price_menu/';
		 $json    = $this->receive_data($get_url,$params);
		$finalData = json_decode($json,true);
        return $finalData['Data'];
	}

	public function getCombinedString($data)
	{
		$arr = array();
		
		if(is_object($data)){
			$data = get_object_vars($data);
		} else $data = array();
		
		foreach ($data as $key => $data) {
			$arr[] = $key . " (" . $data[1] . ")";
		}
		return implode(', ', $arr);
	}

	function word_processor($word)
	{
		  
		$string = wordwrap($word,17,"<br>");
	    $count  = substr_count($string, '<br>');
	    
	    if($count > 1){
	    	
	    	$pieces = explode('<br>',$string);
	    	$finalString  =  $pieces[0]."<br>".$pieces[1]."...";
	    	
	    } else{
	    	
	    	$finalString  = $string;
	    }
	    
		return $finalString;
  		  
	}

	public function get_sandwich_current_price($pid, $bread,$desc,$desc_id)
	{
		$params      = array('id'=> $pid, 'bread'=>$bread,'desc'=>$desc,'desc_id'=>$desc_id);
		$get_url     = API_URL.'sandwich/get_sandwich_current_price/';
		$json    = $this->receive_data($get_url,$params);
		$finalData = json_decode($json,true);
        return $finalData['Data'];
	}

	public function updateToastmenu($data){
		$params      = array('item_id'=>$data['item_id'],'toast'=>$data['toast']);
		$get_url     = API_URL.'sandwich/updateToastmenu/';
		  $json    = $this->receive_data($get_url,$params);
		return  $json;
	}
	/* end */
	
	public function get_user($id){
		
		$get_url     = API_URL.'user/get/';
		$json        = $this->receive_data($get_url,array("user_id"=>$id));
		
		$data        = json_decode($json);
		return $name   = $data->Data[0]->first_name.' '.$data->Data[0]->last_name;
	}
	
	public function getStandardCategories(){
		
		$get_url     = API_URL.'categoryitems/getStandardCategories/';
		$json        = $this->receive_data($get_url,array());
		$data        = json_decode($json);
		return $data->Data;
	}
	
	public function getStandardCategoryProducts($id){
		$data['standard_category_id']	= $id;	
		$get_url     = API_URL.'categoryitems/getStandardCategoryProducts/';
		$json        = $this->receive_data($get_url,$data);
		$data        = json_decode($json);
		return $data->Data;
	}
	
	public function editAddress($data){
		$get_url     = API_URL.'myaccount/editAddress/';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function savingUserAddress($data){
		$get_url     = API_URL.'myaccount/saveUserAddress/';	
		echo $json          = $this->receive_data($get_url,$data);
		exit;
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function cartItems($data){
		$get_url     = API_URL.'cart/cartItems/';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function addItemToCart($post){
						
		$get_url     = API_URL.'cart/addItemToCart/';	
		$json          = $this->receive_data($get_url,$post);	
		//echo "<pre>";print_r($json);exit;	
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function updateItemToCart($post){
		$get_url     = API_URL.'cart/updateItemToCart/';	
		$json          = $this->receive_data($get_url,$post);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function removeCartItem($post){
		$get_url     = API_URL.'cart/removeCartItem/';	
		$json          = $this->receive_data($get_url,$post);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	function  processSandwiches($data){
		$self  = $this;
		array_walk($data,function(&$value,$key) use($self){
			$value = get_object_vars($value);
			$array = get_object_vars(json_decode($value['sandwich_data']));
			array_walk($array,function(&$val,$key){
				$val = get_object_vars($val);
			});
			$format_data = $array;
			
			$sx = '';
			foreach($array as $fval){				
				$str = $fval['item_name'];
				foreach($str as $st){
					$sx .= $st.' ';
				}				
			}
			
			$sx = trim($sx);
			$format_data = str_replace(' ', ',',$sx);
			$value['data_string'] =  $format_data;
			$value['formated_date'] = date('m/d/y',strtotime($value['date_of_creation']));
		});
		return $data;
	}
	
	public function getPickupStores(){
		$get_url     = API_URL.'cart/listPickupAddress/';	
		$json          = $this->receive_data($get_url,array());
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function save_billing_info($data){
		$get_url       = API_URL.'cart/savebillinginfo/';
		$json          = $this->receive_data($get_url,$data);
		$finalData     = @json_decode($json);
		echo $json;
	}
	
	public function get_Billinginfo($data,$print=false){
		$get_url       = API_URL.'cart/getBillinginfo/';
		$json          = $this->receive_data($get_url,$data);
		if($print == true){ echo $json; } else { return @json_decode($json); }
	}
	
	public function confirm_cart($post){
		$get_url     = API_URL.'sandwich/insert_Admin_checkout_placeorder/';
		return $json        = $this->receive_data($get_url,$post);
	}
	
	public function geDiscountDetails($data){
		$get_url     = API_URL.'cart/geDiscountDetails/';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		
		if(isset($finalData->Data) && $finalData->Data->status != "error" )
			return $finalData->Data;
		else
			return array();
		
	}
	
	public function processCustomerdata($data){
		$self  = $this;
		array_walk($data,function(&$value,$key) use($self){
			$value = get_object_vars($value);
		});
		return $data;
	}
	
	public function getOrdersList($data){
		$get_url     = API_URL.'cart/getOrdersList/';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}	
	
	public function createAccount($data){
		$get_url     = API_URL.'myaccount/createAccount/';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	function get_catering_catgory_items($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_catering_catgory_items';
		$json    = $this->receive_data($url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	function get_standard_catgory_items_list($apiUrl, $data ){

		$url     = $apiUrl.'categoryitems/get_standard_catgory_items_list_front';
		$json    = $this->receive_data($url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}	
	
		
	public function getStoreFomZip(){
		if(isset($_POST['zip'])) $zip = $_POST['zip']; else $zip = 0;
		$data = $this->receive_data(API_URL.'myaccount/getStoreFomZip/',array('zip'=>$zip));
		$data = json_decode($data);
		return $data->Data;
	}
	
	public function getCurrentStoreTimeSlot($data){
	
		$get_url     = API_URL.'cart/getCurrentStoreTimeSlot/';
		$json          = $this->receive_data($get_url,$data);
		$json = json_decode($json);
		if(isset($json->Data)) return $json->Data; else return array();
	
	}
	
}
