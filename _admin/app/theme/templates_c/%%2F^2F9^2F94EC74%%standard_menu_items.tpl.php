<?php /* Smarty version 2.6.25, created on 2020-10-20 00:46:40
         compiled from standard_menu_items.tpl */ ?>
<div class="container">

      <div class="standard-menu"><div class="condiment">
        <h1>STANDARD MENU ITEMS</h1>
        <form name="newbread" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/addStandardCategoryProducts" enctype="multipart/form-data">        
     <ul>
        <li>
          <label>Product Name</label>
          <input style="width:250px;" type="text" name="product_name" value="">
        </li>
 
        <li>
          <label>Description</label>
          <textarea style="width:250px;" name="description"></textarea>
        </li>

        <li>
          <label>Product Image</label>
          <input style="width:250px;" type="file"  name="product_image">
        </li>
       
        <li>
          <label>Price</label>
          <input type="text" value="" name="product_price" class="price-menu">
        </li>
          
        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="taxable" name="taxable" class="price-menu" <?php if ($this->_tpl_vars['data']['taxable'] == 1): ?> checked <?php endif; ?> >
          <label for="taxable" style="width:45px;">Taxable</label>

        </li>  
        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="allow_spcl_instruction" name="allow_spcl_instruction" class="price-menu" <?php if ($this->_tpl_vars['data']['allow_spcl_instruction'] == 1): ?> checked <?php endif; ?> >
          <label for="allow_spcl_instruction" style="width:150px;">Allow Special Instructions</label>
        </li>

        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="add_modifier" name="add_modifier" class="price-menu" <?php if ($this->_tpl_vars['data']['add_modifier'] == 1): ?> checked <?php endif; ?> >
          <label for="add_modifier" style="width:100px;">Add Modifier</label>
        </li>  
      
      </ul>
      <ul class="item-description">
        <div class="description-wrap-item">
          <div class="description-left">

            <div class="description-main">
              <label>Description</label>
              <input type="text" name="modifier_desc" class="descriptor" placeholder="Add a Protein" value="">
            </div>
             <div class="radio-box">
        <div class="description-radio_options">
<p class="radio-label-itm">Optional or Required</p>
          <div class="input">
            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_isoptional" id="optional" value="yes">
             <label for="optional">Optional</label>
            </div>

            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_isoptional" id="required" value="no">
             <label for="required">Required</label>
            </div>
          </div>
      </div>

      <div class="description-radio_options">
        <p class="radio-label-itm">Amount to Choose</p>
          <div class="input">
            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_is_single" id="limited" value="yes">
             <label for="limited">Choose One</label>
            </div>

            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_is_single" id="unlimited" value="no">
             <label for="unlimited">Choose Unlimited</label>
            </div>
          </div>
      </div>
      </div>
          </div> 

          <div class="description-right">
         <div class="description-right-wrap">
            <div class="description-main add-more-opt">
              <label>Options</label>
              <input type="text" name="option[]" class="option_price" id="add_remove" placeholder="" value="">
            </div>
            <div class="description-main description-small-feild add-more-prc">
              <label>Price</label>
              <input type="text" name="price[]" class="option_price" id="add_remove" placeholder="" value="">
            </div>
            <div class="remove-btn add-more-rem">
              <label></label>
              <p class="rem_add_more">remove</p>             
            </div>
          </div>
            <div class="desc-additm">
            <p class="add_more">add item</p>
          </div>
          </div>

        </div>
        </ul>
      <input type="hidden" name="hidden_id" value="<?php echo $this->_tpl_vars['id']; ?>
">
       <div class="button">
         <input type="submit" class="save save_standard_product" style="cursor: pointer;" value="SAVE">
        <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu" class="cancel">CANCEL</a>
    </div>
   </div></div> 
 </form>
    </div>