<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:37:37
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\add_to_cart_shopping_details.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c8411b4dfe8_13844810',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2496b96f47b96e5dc77c5451e5c62f5bb99a07f7' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\add_to_cart_shopping_details.tpl',
      1 => 1600697397,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c8411b4dfe8_13844810 (Smarty_Internal_Template $_smarty_tpl) {
?><h3>SHOPPING CART</h3>
        <form  id="cartdetails">
         <table class="shopping-cart-table-new">
			<tbody>
				<tr>
				 <th>ORDER DETAILS</th>
				 <th>&nbsp;</th>
				 <th>QTY</th>
				 <th>TOTAL</th>
				 <th>&nbsp;</th>
				</tr>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwiches']->value, 'sandwich', false, 'myId');
$_smarty_tpl->tpl_vars['sandwich']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['myId']->value => $_smarty_tpl->tpl_vars['sandwich']->value) {
$_smarty_tpl->tpl_vars['sandwich']->do_else = false;
?>
				<tr>
				 <td colspan="2">
					 <p><?php echo $_smarty_tpl->tpl_vars['sandwich']->value['sandwich_name'];?>
_________________________________</p>
					<p><?php echo $_smarty_tpl->tpl_vars['sandwich']->value['data_string'];?>
</p>
				 </td>
				 <td>
					<span class="arrow-holder">
				   <a href="#" class="left arrow-left"></a>
				   <input type="text" class="qty" name="qty[]" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['item_qty'];?>
">
				   <a href="#" class="arrow-right"></a>
				   <input type="hidden" name="data_id" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['id'];?>
" />
				   <input type="hidden" name="data_type" value="user_sandwich" />
				   <input type="hidden" name="data_order_id" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['order_item_id'];?>
" />
				 </span>
				 </td>
				   <td>$<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['item_price']*$_smarty_tpl->tpl_vars['sandwich']->value['item_qty'];?>
</td>
				 <td><a href="javascript:void(0)" class="remove remove_cart_item">REMOVE</a></td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product', false, 'myId');
$_smarty_tpl->tpl_vars['product']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['myId']->value => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->do_else = false;
?>
				<tr>
				 <td colspan="2"><p><?php echo $_smarty_tpl->tpl_vars['product']->value->product_name;?>
 ______________________________<?php if ($_smarty_tpl->tpl_vars['product']->value->extra_name) {?><br/> <?php echo $_smarty_tpl->tpl_vars['product']->value->extra_name;
}?></p></td>
				 <td>
					<span class="arrow-holder">
				   <a href="#" class="arrow-left"></a>
				   <input type="text" class="qty" name="qty[]" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->item_qty;?>
">
				   <a href="#" class="arrow-right"></a>
				   <input type="hidden" name="data_id" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
" />
				   <input type="hidden" name="data_order_id" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->order_item_id;?>
" />
				   <input type="hidden" name="data_type" value="product" />
				 </span>
				 </td>
				 <td>$<?php echo $_smarty_tpl->tpl_vars['product']->value->item_price;?>
</td>
				 <td><a href="javascript:void(0)" class="remove remove_cart_item">REMOVE</a></td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">SUBTOTAL</td>
            <td id='addToCart_Subtotal'>$<?php echo $_smarty_tpl->tpl_vars['subtotal']->value;?>
 </td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">TIP</td>
            <td colspan="2">
              <select name="tips_selected" class="select-tip">
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 3) {?> selected <?php }?> value="3">$3.00</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 3.50) {?> selected <?php }?> value="3.50">$3.50</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 4) {?> selected <?php }?> value="4">$4.00</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 4.50) {?> selected <?php }?> value="4.50">$4.50</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 5) {?> selected <?php }?> value="5">$5.00</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 5.50) {?> selected <?php }?> value="5">$5.50</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 6) {?> selected <?php }?> value="6">$6.00</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 6.50) {?> selected <?php }?> value="6.50">$6.50</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 7) {?> selected <?php }?> value="7">$7.00</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 7.50) {?> selected <?php }?> value="7.50">$7.50</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 8) {?> selected <?php }?> value="8">$8.00</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 8.50) {?> selected <?php }?> value="8.50">$8.50</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 9) {?> selected <?php }?> value="9">$9.00</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 9) {?> selected <?php }?> value="9.50">$9.50</option>
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 10) {?> selected <?php }?> value="10">$10.00</option>                
                <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == 10.50) {?> selected <?php }?> value="10.50">$10.50</option>                
              </select>
            </td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TAX</h4></td>
            <td><h4 id='taxvalue'>$<?php echo $_smarty_tpl->tpl_vars['tax']->value;?>
</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL VALUE</h4></td>
            <td><h4>$<?php echo $_smarty_tpl->tpl_vars['total_price']->value;?>
</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">DISCOUNT:  <?php echo $_smarty_tpl->tpl_vars['off_type']->value;?>
 OFF SPECIAL</td>
            <td>-$<?php echo $_smarty_tpl->tpl_vars['off_value']->value;?>
</td>
            <td>&nbsp;</td>
           </tr>
		   
		   <?php if ($_smarty_tpl->tpl_vars['manual_discount']->value) {?>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">MANUAL DISCOUNT: </td>
            <td>-$<?php echo $_smarty_tpl->tpl_vars['manual_discount']->value;?>
</td>
            <td>&nbsp;</td>
           </tr>
		   <?php }?>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">DISCOUNT / GIFT CARD</p>
            	<span class="text-box-holder">
              	<input name="" type="text" class="text-box coupon_code">
                <a href="javascript:void(0)" class="shopping-cart-select apply_coupon">Apply</a>
              </span>
            </td>
           </tr>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">MANUAL DISCOUNT</p>
            	<span class="text-box-holder">
              	<input name="manual_discount_val" type="text" class="text-box manual_discount" value="">
                <a href="javascript:void(0)" class="shopping-cart-select apply_manual_discount">Apply</a>              
              </span>
            </td>
           </tr>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL OWED</h4></td>
            <td><h4>$<?php echo $_smarty_tpl->tpl_vars['grand_total']->value;?>
</h4></td>
            <td>&nbsp;</td>
           </tr>
         </tbody>
        </table>
		
		<input	type="hidden" name="order_string" value="<?php echo $_smarty_tpl->tpl_vars['order_string']->value;?>
" />
		<input	type="hidden" name="grand_total" value="<?php echo $_smarty_tpl->tpl_vars['grand_total']->value;?>
" />
		<input	type="hidden" name="sub_total" value="<?php echo $_smarty_tpl->tpl_vars['total_price']->value;?>
" />
		<input	type="hidden" name="coupon_id" value="<?php echo $_smarty_tpl->tpl_vars['coupon_id']->value;?>
" />
		<input	type="hidden" name="coupon_code" value="<?php echo $_smarty_tpl->tpl_vars['coupon_code']->value;?>
" />
		<input	type="hidden" name="manual_discount" value="<?php echo $_smarty_tpl->tpl_vars['manual_discount']->value;?>
" />
		<input	type="hidden" name="tips" value="<?php echo $_smarty_tpl->tpl_vars['tips']->value;?>
" />
		<input	type="hidden" name="off_type" value="<?php echo $_smarty_tpl->tpl_vars['off_type']->value;?>
" />
		<input	type="hidden" name="off_amount" value="<?php echo $_smarty_tpl->tpl_vars['off_value']->value;?>
" />
		
		</form>			
    
<?php }
}
