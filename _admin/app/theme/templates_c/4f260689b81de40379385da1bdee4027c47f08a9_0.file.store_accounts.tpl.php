<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:36:58
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\store_accounts.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c83ea166a56_58029265',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4f260689b81de40379385da1bdee4027c47f08a9' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\store_accounts.tpl',
      1 => 1600125368,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c83ea166a56_58029265 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <div class="store-accounts">
       <h1>STORE ACCOUNTS</h1>
       <form id="updateStoreOrder" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/updateStoreOrder" method="post">
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="9%">&nbsp;</th>
          <th colspan="1">&nbsp;</th>
          <th colspan="3">STORE</th>
        </tr>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['store_accounts']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
        <tr>
          <td width="9%" align="center">
          <span class="multi-left">
               <input type="checkbox" id="checkbox-<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" class="input-checkbox" name="live[<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
]" value="1" <?php if ($_smarty_tpl->tpl_vars['v']->value->live == 1) {?> checked <?php }?>>
             <label for="checkbox-<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" class="multisel-ckeck"></label>
            </span>
          </td>
          <td width="16%"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->ordered;?>
" class="field" name="ordered[<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
]"></td>
          <td width="64%"><?php echo $_smarty_tpl->tpl_vars['v']->value->store_name;?>
</td>
          <td width="4%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/editStoreAccount/<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
">edit</a></td>
          <td width="7%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/deleteStoreAccount/<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
">delete</a></td>
        </tr>
     <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </tbody></table>
      
       <a class="update" href="#" onclick="document.getElementById('updateStoreOrder').submit()">update</a>
      <a class="add-user" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/newStoreUser/">ADD STORE</a>
      </form>  
        
    </div>
  </div>
<?php }
}
