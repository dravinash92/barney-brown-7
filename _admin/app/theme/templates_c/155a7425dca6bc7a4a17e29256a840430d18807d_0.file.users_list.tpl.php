<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:37:03
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\users_list.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c83ef597456_87034240',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '155a7425dca6bc7a4a17e29256a840430d18807d' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\users_list.tpl',
      1 => 1600125372,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c83ef597456_87034240 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <div class="admin-user-account">
       <h1>USER ACCOUNTS</h1>
       <form id="updateUsersLive" method="post" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/updateUsersLive">
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="7%">&nbsp;</th>
          <th colspan="1">USER</th>
          <th colspan="3">ACCOUNT TYPE</th>
        </tr>
      
       <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['admin_users']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
        <tr>
          <td width="7%" align="center">
              <span class="multi-left">
               <input type="checkbox" id="checkbox-<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
" class="input-checkbox" name="live[<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
]" value="1" <?php if ($_smarty_tpl->tpl_vars['v']->value->live == 1) {?> checked <?php }?>>
               <label for="checkbox-<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
" class="multisel-ckeck"></label>
              </span>
          </td>
          <td width="43%"><?php echo $_smarty_tpl->tpl_vars['v']->value->first_name;?>
  <?php echo $_smarty_tpl->tpl_vars['v']->value->last_name;?>
</td>
          <td width="33%"><?php echo $_smarty_tpl->tpl_vars['v']->value->cat_name;?>
</td>
          <td width="5%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/editAdminUser/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
">edit</a></td>
          <td width="12%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/deleteAdminUser/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
">delete</a></td>
        </tr>
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

      </tbody></table>
      <a class="update" href="#" onclick="document.getElementById('updateUsersLive').submit()">update</a>
      <a class="add-user" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/newUser/">ADD NEW USER</a>
      </form>
    </div>
  </div>
<?php }
}
