<?php /* Smarty version 2.6.25, created on 2020-10-07 00:44:40
         compiled from hourly_report.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', 'hourly_report.tpl', 59, false),array('modifier', 'string_format', 'hourly_report.tpl', 60, false),array('function', 'math', 'hourly_report.tpl', 73, false),)), $this); ?>
<div class="container">
    <div class="hourly-report">
      
      <div class="heading">
        <h1>HOURLY REPORT</h1>
        
        <div class="date">
          <p>
            <span>Date</span>
            <input id="datehourreport" type="text" value="<?php echo $this->_tpl_vars['calendarDate']; ?>
"><a class="date-icon" href="#"><img width="24" height="23" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/calendar.png"></a>
          </p>
        </div>
      </div>
      
      <div class="store-wrapper">
           <div class="right">
              <span>Store Location</span>
              <select class="store" name="store">  <option value="">Select Store</option>
              <?php $_from = $this->_tpl_vars['pickupstores']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j'] => $this->_tpl_vars['stores']):
?>
               <option value="<?php echo $this->_tpl_vars['stores']->id; ?>
" id="store<?php echo $this->_tpl_vars['stores']->id; ?>
"><?php echo $this->_tpl_vars['stores']->store_name; ?>
</option>               
               <?php endforeach; endif; unset($_from); ?>
              </select>
           </div>
           
           <div class="left"> 
             <ul>
              <li>
              <span>To</span>
              <select name="tohour" id="tohour">
               <?php echo $this->_tpl_vars['toTimes']; ?>
               
               </select>
             </li>
             <li>
              <span>From</span>
               <select  name="fromhour" id="fromhour">
                <?php echo $this->_tpl_vars['fromTimes']; ?>

                </select>
             </li>
             </ul>
           </div>
           
      </div>
      <div class="reporttable">
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="62%">TIME</th>
          <th width="7%">PICK-UP</th>
          <th width="7%">DELIVERY</th>
          <th width="7%"> TOTAL</th>
          <th width="8%">SALES</th>
           <th width="9%">%SALES</th>
        </tr>
        
         <tr class="heading-color">
          <td></td>
          <td class="center"><?php echo $this->_tpl_vars['total_pickups']; ?>
</td>
          <td class="center"><?php echo $this->_tpl_vars['total_delivery']; ?>
</td>
          <td class="center"><?php echo ((is_array($_tmp=$this->_tpl_vars['total'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
          <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['total_sales'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
          <td>100%</td>
        </tr>
      <?php $_from = $this->_tpl_vars['orderedItems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i'] => $this->_tpl_vars['orderedItem']):
?>
      
        <tr <?php if ($this->_tpl_vars['i']%2 == 0): ?> class="even" <?php endif; ?>>
          <td><?php echo $this->_tpl_vars['orderedItem']->time; ?>
</td>
          <td class="center"><?php echo $this->_tpl_vars['orderedItem']->pickups; ?>
</td>
          <td class="center"><?php echo $this->_tpl_vars['orderedItem']->delivery; ?>
</td>
          <td class="center"><?php echo $this->_tpl_vars['orderedItem']->total->total_order; ?>
</td>
          <td>$<?php echo $this->_tpl_vars['orderedItem']->total->total_sales; ?>
</td>
          <td> 
			<?php if ($this->_tpl_vars['orderedItem']->total->total_order > 0): ?>
				<?php echo smarty_function_math(array('equation' => "(( x / y ) * z )",'x' => $this->_tpl_vars['orderedItem']->total->total_sales,'y' => $this->_tpl_vars['total_sales'],'z' => 100,'format' => "%.2f"), $this);?>
%
          
			<?php endif; ?>          
          </td>
        </tr>
				
    
      <?php endforeach; endif; unset($_from); ?>
        </tbody></table>
        </div>
      <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="62%">TIME</th>
          <th width="7%">PICK-UP</th>
          <th width="7%">DELIVERY</th>
          <th width="7%"> TOTAL</th>
          <th width="8%">SALES</th>
           <th width="9%">%SALES</th>
        </tr>
        
         <tr class="heading-color">
          <td></td>
          <td class="center">445</td>
          <td class="center">550</td>
          <td class="center">985</td>
          <td>$9850.00</td>
          <td>100.000%</td>
        </tr>
        
        <tr>
          <td>12:00AM - 12:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		 <tr class="even">
          <td>1:00AM - 1:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>2:00AM - 2:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
        
		 <tr class="even">
          <td>3:00AM - 3:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>4:00AM - 4:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>5:00AM - 5:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>6:00AM - 6:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>7:00AM - 7:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>8:00AM - 8:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>9:00AM - 9:59AM</td>
          <td class="center">5</td>
          <td class="center">15</td>
          <td class="center">20</td>
          <td>$200.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>10:00AM - 10:59AM</td>
          <td class="center">15</td>
          <td class="center">25</td>
          <td class="center">40</td>
          <td>$400.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>11:00AM - 11:59AM</td>
          <td class="center">45</td>
          <td class="center">55</td>
          <td class="center">100</td>
          <td>$1000.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>12:00PM - 12:59PM</td>
          <td class="center">85</td>
          <td class="center">105</td>
          <td class="center">190</td>
          <td>$1900.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>1:00PM - 1:59PM</td>
          <td class="center">90</td>
          <td class="center">120</td>
          <td class="center">210</td>
          <td>$2100.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>2:00PM - 2:59PM</td>
          <td class="center">70</td>
          <td class="center">100</td>
          <td class="center">100</td>
          <td>$1000.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>3:00PM - 3:59PM</td>
          <td class="center">30</td>
          <td class="center">50</td>
          <td class="center">80</td>
          <td>$800.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>4:00PM - 4:59PM</td>
          <td class="center">20</td>
          <td class="center">30</td>
          <td class="center">50</td>
          <td>$500.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>5:00PM - 5:59PM</td>
          <td class="center">10</td>
          <td class="center">15</td>
          <td class="center">25</td>
          <td>$250.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>6:00PM - 6:59PM</td>
          <td class="center">25</td>
          <td class="center">25</td>
          <td class="center">50</td>
          <td>$500.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>7:00PM - 7:59PM</td>
          <td class="center">30</td>
          <td class="center">40</td>
          <td class="center">70</td>
          <td>$700.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>8:00PM - 8:59PM</td>
          <td class="center">15</td>
          <td class="center">20</td>
          <td class="center">35</td>
          <td>$350.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>9:00PM - 9:59PM</td>
          <td class="center">5</td>
          <td class="center">10</td>
          <td class="center">15</td>
          <td>$150.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>10:00PM - 10:59PM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>11:00PM - 11:59PM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
	
		
      </tbody></table>-->
    </div>
  </div>