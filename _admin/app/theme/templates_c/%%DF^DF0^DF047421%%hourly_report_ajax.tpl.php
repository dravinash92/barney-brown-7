<?php /* Smarty version 2.6.25, created on 2018-01-19 06:08:43
         compiled from hourly_report_ajax.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', 'hourly_report_ajax.tpl', 16, false),array('modifier', 'string_format', 'hourly_report_ajax.tpl', 17, false),array('function', 'math', 'hourly_report_ajax.tpl', 29, false),)), $this); ?>
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="62%">TIME</th>
          <th width="7%">PICK-UP</th>
          <th width="7%">DELIVERY</th>
          <th width="7%"> TOTAL</th>
          <th width="8%">SALES</th>
           <th width="9%">%SALES</th>
        </tr>
        
         <tr class="heading-color">
          <td></td>
          <td class="center"><?php echo $this->_tpl_vars['total_pickups']; ?>
</td>
          <td class="center"><?php echo $this->_tpl_vars['total_delivery']; ?>
</td>
          <td class="center"><?php echo ((is_array($_tmp=$this->_tpl_vars['total'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
          <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['total_sales'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
          <td>100%</td>
        </tr>
      <?php $_from = $this->_tpl_vars['orderedItems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i'] => $this->_tpl_vars['orderedItem']):
?>
      
        <tr <?php if ($this->_tpl_vars['i']%2 == 0): ?> class="even" <?php endif; ?>>
          <td><?php echo $this->_tpl_vars['orderedItem']->time; ?>
</td>
          <td class="center"><?php echo $this->_tpl_vars['orderedItem']->pickups; ?>
</td>
          <td class="center"><?php echo $this->_tpl_vars['orderedItem']->delivery; ?>
</td>
          <td class="center"><?php echo $this->_tpl_vars['orderedItem']->total->total_order; ?>
</td>
          <td>$<?php echo $this->_tpl_vars['orderedItem']->total->total_sales; ?>
</td>
          <td>  <?php if ($this->_tpl_vars['orderedItem']->total->total_order > 0): ?>
           <?php echo smarty_function_math(array('equation' => "(( x / y ) * z )",'x' => $this->_tpl_vars['orderedItem']->total->total_sales,'y' => $this->_tpl_vars['total_sales'],'z' => 100,'format' => "%.2f"), $this);?>
%          
			<?php endif; ?>
			</td>
        </tr>
				
    
      <?php endforeach; endif; unset($_from); ?>
      </tbody></table>
      