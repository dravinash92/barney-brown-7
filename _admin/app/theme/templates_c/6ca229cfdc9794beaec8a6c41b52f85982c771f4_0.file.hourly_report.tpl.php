<?php
/* Smarty version 3.1.39, created on 2021-03-31 03:40:56
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\hourly_report.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_60642788d676a3_87642907',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6ca229cfdc9794beaec8a6c41b52f85982c771f4' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\hourly_report.tpl',
      1 => 1600125376,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60642788d676a3_87642907 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_admin\\app\\smarty\\libs\\plugins\\function.math.php','function'=>'smarty_function_math',),));
?>
<div class="container">
    <div class="hourly-report">
      
      <div class="heading">
        <h1>HOURLY REPORT</h1>
        
        <div class="date">
          <p>
            <span>Date</span>
            <input id="datehourreport" type="text" value="<?php echo $_smarty_tpl->tpl_vars['calendarDate']->value;?>
"><a class="date-icon" href="#"><img width="24" height="23" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/calendar.png"></a>
          </p>
        </div>
      </div>
      
      <div class="store-wrapper">
           <div class="right">
              <span>Store Location</span>
              <select class="store" name="store">  <option value="">Select Store</option>
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickupstores']->value, 'stores', false, 'j');
$_smarty_tpl->tpl_vars['stores']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['stores']->value) {
$_smarty_tpl->tpl_vars['stores']->do_else = false;
?>
               <option value="<?php echo $_smarty_tpl->tpl_vars['stores']->value->id;?>
" id="store<?php echo $_smarty_tpl->tpl_vars['stores']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['stores']->value->store_name;?>
</option>               
               <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </select>
           </div>
           
           <div class="left"> 
             <ul>
              <li>
              <span>To</span>
              <select name="tohour" id="tohour">
               <?php echo $_smarty_tpl->tpl_vars['toTimes']->value;?>
               
               </select>
             </li>
             <li>
              <span>From</span>
               <select  name="fromhour" id="fromhour">
                <?php echo $_smarty_tpl->tpl_vars['fromTimes']->value;?>

                </select>
             </li>
             </ul>
           </div>
           
      </div>
      <div class="reporttable">
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="62%">TIME</th>
          <th width="7%">PICK-UP</th>
          <th width="7%">DELIVERY</th>
          <th width="7%"> TOTAL</th>
          <th width="8%">SALES</th>
           <th width="9%">%SALES</th>
        </tr>
        
         <tr class="heading-color">
          <td></td>
          <td class="center"><?php echo $_smarty_tpl->tpl_vars['total_pickups']->value;?>
</td>
          <td class="center"><?php echo $_smarty_tpl->tpl_vars['total_delivery']->value;?>
</td>
          <td class="center"><?php echo number_format($_smarty_tpl->tpl_vars['total']->value);?>
</td>
          <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['total_sales']->value);?>
</td>
          <td>100%</td>
        </tr>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orderedItems']->value, 'orderedItem', false, 'i');
$_smarty_tpl->tpl_vars['orderedItem']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['orderedItem']->value) {
$_smarty_tpl->tpl_vars['orderedItem']->do_else = false;
?>
      
        <tr <?php if ($_smarty_tpl->tpl_vars['i']->value%2 == 0) {?> class="even" <?php }?>>
          <td><?php echo $_smarty_tpl->tpl_vars['orderedItem']->value->time;?>
</td>
          <td class="center"><?php echo $_smarty_tpl->tpl_vars['orderedItem']->value->pickups;?>
</td>
          <td class="center"><?php echo $_smarty_tpl->tpl_vars['orderedItem']->value->delivery;?>
</td>
          <td class="center"><?php echo $_smarty_tpl->tpl_vars['orderedItem']->value->total->total_order;?>
</td>
          <td>$<?php echo $_smarty_tpl->tpl_vars['orderedItem']->value->total->total_sales;?>
</td>
          <td> 
			<?php if ($_smarty_tpl->tpl_vars['orderedItem']->value->total->total_order > 0) {?>
				<?php echo smarty_function_math(array('equation'=>"(( x / y ) * z )",'x'=>$_smarty_tpl->tpl_vars['orderedItem']->value->total->total_sales,'y'=>$_smarty_tpl->tpl_vars['total_sales']->value,'z'=>100,'format'=>"%.2f"),$_smarty_tpl);?>
%
          
			<?php }?>          
          </td>
        </tr>
				
    
      <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody></table>
        </div>
      <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="62%">TIME</th>
          <th width="7%">PICK-UP</th>
          <th width="7%">DELIVERY</th>
          <th width="7%"> TOTAL</th>
          <th width="8%">SALES</th>
           <th width="9%">%SALES</th>
        </tr>
        
         <tr class="heading-color">
          <td></td>
          <td class="center">445</td>
          <td class="center">550</td>
          <td class="center">985</td>
          <td>$9850.00</td>
          <td>100.000%</td>
        </tr>
        
        <tr>
          <td>12:00AM - 12:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		 <tr class="even">
          <td>1:00AM - 1:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>2:00AM - 2:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
        
		 <tr class="even">
          <td>3:00AM - 3:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>4:00AM - 4:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>5:00AM - 5:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>6:00AM - 6:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>7:00AM - 7:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>8:00AM - 8:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>9:00AM - 9:59AM</td>
          <td class="center">5</td>
          <td class="center">15</td>
          <td class="center">20</td>
          <td>$200.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>10:00AM - 10:59AM</td>
          <td class="center">15</td>
          <td class="center">25</td>
          <td class="center">40</td>
          <td>$400.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>11:00AM - 11:59AM</td>
          <td class="center">45</td>
          <td class="center">55</td>
          <td class="center">100</td>
          <td>$1000.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>12:00PM - 12:59PM</td>
          <td class="center">85</td>
          <td class="center">105</td>
          <td class="center">190</td>
          <td>$1900.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>1:00PM - 1:59PM</td>
          <td class="center">90</td>
          <td class="center">120</td>
          <td class="center">210</td>
          <td>$2100.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>2:00PM - 2:59PM</td>
          <td class="center">70</td>
          <td class="center">100</td>
          <td class="center">100</td>
          <td>$1000.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>3:00PM - 3:59PM</td>
          <td class="center">30</td>
          <td class="center">50</td>
          <td class="center">80</td>
          <td>$800.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>4:00PM - 4:59PM</td>
          <td class="center">20</td>
          <td class="center">30</td>
          <td class="center">50</td>
          <td>$500.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>5:00PM - 5:59PM</td>
          <td class="center">10</td>
          <td class="center">15</td>
          <td class="center">25</td>
          <td>$250.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>6:00PM - 6:59PM</td>
          <td class="center">25</td>
          <td class="center">25</td>
          <td class="center">50</td>
          <td>$500.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>7:00PM - 7:59PM</td>
          <td class="center">30</td>
          <td class="center">40</td>
          <td class="center">70</td>
          <td>$700.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>8:00PM - 8:59PM</td>
          <td class="center">15</td>
          <td class="center">20</td>
          <td class="center">35</td>
          <td>$350.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>9:00PM - 9:59PM</td>
          <td class="center">5</td>
          <td class="center">10</td>
          <td class="center">15</td>
          <td>$150.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>10:00PM - 10:59PM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>11:00PM - 11:59PM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
	
		
      </tbody></table>-->
    </div>
  </div>
<?php }
}
