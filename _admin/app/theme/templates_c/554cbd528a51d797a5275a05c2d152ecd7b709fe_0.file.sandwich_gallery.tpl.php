<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:28:12
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\sandwich_gallery.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c81dc861344_09729806',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '554cbd528a51d797a5275a05c2d152ecd7b709fe' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\sandwich_gallery.tpl',
      1 => 1616675279,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c81dc861344_09729806 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
     <div class="sandwich-gallery">
       <div class="heading">
         <h1>SANDWICH GALLERY</h1>
         
         <div class="search">
           <p>Search</p>
           <input type="text" name="search_ingallery" > <a class="search-button" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/index/"><img width="26" height="26" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/link.png"> </a>
           <input type="hidden" id="searchTerm" value="<?php echo $_smarty_tpl->tpl_vars['serch_term']->value;?>
">
         </div>
       </div>
       
       <div class="leftside-bar">
          <h2>Sort By</h2>
          
           <div class="bgforSelect">
                <select id="" name="" class="wid-input sortby">
                   <option value="1" <?php if ($_smarty_tpl->tpl_vars['sort_id']->value == 1) {?> selected="selected" <?php }?>>MOST RECENT</option>
                   <option value="2" <?php if ($_smarty_tpl->tpl_vars['sort_id']->value == 2) {?> selected="selected" <?php }?>>MOST LIKED</option>
                   <option value="3" <?php if ($_smarty_tpl->tpl_vars['sort_id']->value == 3) {?> selected="selected" <?php }?>>MOST POPULAR</option>
                </select>
              </div>
          
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category', false, 'i');
$_smarty_tpl->tpl_vars['category']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->do_else = false;
?>
	            
	            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['category']->value, 'data', false, 'k');
$_smarty_tpl->tpl_vars['data']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->do_else = false;
?>
	              <?php if ($_smarty_tpl->tpl_vars['data']->value->category_identifier) {?>
         			<div class="items">
           				<h3><?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
</h3>
            				<ul>
            				<?php if ($_smarty_tpl->tpl_vars['data']->value->category_name != "bread") {?>
	              <li>
	              <span class="multi-left">
                  <input type="checkbox"  value="null" name="check" id="check_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
_00" class="input-checkbox">
                  <label for="check_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
_00"  class="multisel-ckeck"></label></span>
                  <span style="width: 98px;" class="radio-text">No <?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
</span>
                  </li>   
                 <?php }?>  
                 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryItems']->value, 'categoryItem', false, 'j');
$_smarty_tpl->tpl_vars['categoryItem']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['categoryItem']->value) {
$_smarty_tpl->tpl_vars['categoryItem']->do_else = false;
?>
		            
		            	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryItem']->value, 'dataitems', false, 'j');
$_smarty_tpl->tpl_vars['dataitems']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['dataitems']->value) {
$_smarty_tpl->tpl_vars['dataitems']->do_else = false;
?>
                     <?php if ($_smarty_tpl->tpl_vars['data']->value->id == $_smarty_tpl->tpl_vars['dataitems']->value->category_id) {?>
                <li>
                 <span class="multi-left">
                  <input type="checkbox"  value="check1" name="check" id="check_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
" class="input-checkbox">
                  <label for="check_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
"></label></span>
                  <span style="width: 98px;" class="radio-text"><?php echo $_smarty_tpl->tpl_vars['dataitems']->value->item_name;?>
</span>
                </li>
                <?php }?>
              
                  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	                 
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             </ul>
             </div>
              <?php }?>  
               <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	                 
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>         

         
          
       </div>
       
       <div class="right-side">
       <input name="total_item_count" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['total_item_count']->value;?>
" />
        <input name="sort_type" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['sort_id']->value;?>
" />
       <table width="100%" cellspacing="0" cellpadding="0" border="0" class="adminGalleryList">
        <tbody><tr class="static">
          <th width="10%" height="31">SOTD</th>
          <th width="10%" height="31">TRND</th>
          <th width="10%" height="31">PUBLIC</th>
          <th width="38%">CUSTOM SANDWICHES</th>
          <th width="7%">LIKES</th>
          <th width="11%">PURCHASES</th>
          <th width="10%">PRICE</th>
          <th width="12%">FLAGGED</th>
          <th width="6%">&nbsp;</th>
          <th width="6%">&nbsp;</th>
        </tr>
        
        
        <?php if (count($_smarty_tpl->tpl_vars['GALLARY_DATA']->value) > 0) {?>
        
        <?php
$__section_sandwitch_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['GALLARY_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_0_start = min(0, $__section_sandwitch_0_loop);
$__section_sandwitch_0_total = min(($__section_sandwitch_0_loop - $__section_sandwitch_0_start), $__section_sandwitch_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_0_total !== 0) {
for ($__section_sandwitch_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_0_start; $__section_sandwitch_0_iteration <= $__section_sandwitch_0_total; $__section_sandwitch_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>
     
     
     
              <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>
              
              <?php 
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
              ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
              if($c){
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }}
              ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
              ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              ?>
      
      
                  <?php if (is_array($_SESSION['orders']['item_id'])) {?> 
                   <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_SESSION['orders']['item_id']));?>
                   <?php } else { ?>
                   <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
                   <?php }?>
        
        <tr <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null) % 2 != 0) {?> class="even" <?php }?> data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0];?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" >
		
		<td width="10%"><div class="plus <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sotd'] == 1) {?> white <?php } else { ?> black <?php }?> sotd" data-sandwich="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">+</div></td>	
		
		<td width="10%"><div class="plus <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['trnd'] == 1) {?> white <?php } else { ?> black <?php }?> trnd" data-sandwich="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">+</div></td>	
        
        <td width="10%" align="center">
              <span class="multi-left">
               <input <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['is_public'] == 1) {?> checked="checked" <?php }?> type="checkbox" name="" class="input-checkbox" id="checkbox-<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null);?>
">
               <label class="multisel-ckeck" for="checkbox-<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null);?>
"></label>
              </span>
          </td>
          <td><?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
</td>
          <td><input name="sandwich_like" id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_count'];?>
" type="text" class="sandwich_like" style="width:80px;" /></td>
          <td><input name="sandwich_purchase" id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['purchase_count'];?>
" type="text" class="sandwich_purchase" style="width:80px;" /></td>
          <td>$ <?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_price'];?>
</td>
          <td><a href="#" class="flag"><?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['flag'] == 1) {?><img width="15" height="20" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/flag.png">unflag<?php }?></a></td>
          <td width="6%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/edit/<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">edit</a></td>
          <td width="6%"><a href="#" class="remove_gallery_item">remove</a></td>
        </tr>
        
        <?php
}
}
?>
        
        <?php }?>
	</tbody></table>
<div class="loadMoreGallery" style="text-align:center"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/star.png" alt="Loading" /> </div>
       </div>
       </div>
       
     </div>
  </div>
<?php }
}
