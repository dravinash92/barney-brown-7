<?php
/* Smarty version 3.1.39, created on 2021-03-25 09:04:05
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\pickups.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c8a454323a3_38119076',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e008e9d087c1b84ccf03ac6b3cdf0b78c81ced99' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\pickups.tpl',
      1 => 1616677442,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c8a454323a3_38119076 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <div class="deliveries">
       <h1>PICK-UPS</h1>
       <div class="top-deliveries-wrapper">
       		<div class="left">
          		<table class="top-deliveries-table">
								<tbody><tr>
                  <td>NEW</td>
                 <td class="count_new"><?php echo $_smarty_tpl->tpl_vars['count_new']->value;?>
</td>
                </tr>
                <tr>
                  <td>PRC</td>
                  <td class="count_prc"><?php echo $_smarty_tpl->tpl_vars['count_prc']->value;?>
</td>
                </tr>
                <tr>
                  <td>OUT</td>
                  <td class="count_bag"><?php echo $_smarty_tpl->tpl_vars['count_bag']->value;?>
</td>
                </tr>
                <tr>
                  <td>DEL</td>
                  <td class="count_pick"><?php echo $_smarty_tpl->tpl_vars['count_pick']->value;?>
</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td class="total_pickUps" ><?php echo $_smarty_tpl->tpl_vars['total_pickUps']->value;?>
</td>
                </tr>
              </tbody></table>

          </div>
          <div class="right">
            <form action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
pickups/searchPickupsData" name="delivery_search" method="POST">
          	<div class="text-box-holder">
            	<label>Store Location</label>
              <select name="store_id" id="store_id" class="select">
				<option value="">Select Store</option>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickupstores']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['store_id'] == $_smarty_tpl->tpl_vars['v']->value->id) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value->store_name;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->address1;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->address2;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->zip;?>
</option>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
              </select>
          	</div>
            <div class="text-box-holder">
            	<label>Status</label>
              <select name="delivery_status" id="1" class="select">
              <option value="">--Select--</option>
              <option value="0">New</option>
              	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
                <option value="<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
" <?php if ($_smarty_tpl->tpl_vars['del_status']->value == $_smarty_tpl->tpl_vars['b']->value->id) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['b']->value->label;?>
</option>
              <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>    
              </select>
          	</div>
            <div class="text-box-holder2">
            	<label>Date</label>
              <input name="search_date" id="search_pickup_date" type="text" class="text-box" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
">
              <a href="#" class="date-icon"></a>
          	</div>
          	 <input type="submit" name="submit" value="SEARCH" style="margin-top: 15px;margin-left: 26px;">
             </form>
            
          </div>
       </div>
        <?php if ($_smarty_tpl->tpl_vars['total_pickUps']->value != 0) {?> 
       <div class="deliveries-table-detail-wrapper">
       	 <h2>NEW</h2>
         <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
         
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addressList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 0) {?>
            <?php if ($_smarty_tpl->tpl_vars['v']->value->timediff > 45) {?>  
            <tr style="background: #cef2cc;">
            <?php } elseif (($_smarty_tpl->tpl_vars['v']->value->timediff <= 45) && ($_smarty_tpl->tpl_vars['v']->value->timediff > 30)) {?>
            <tr style="background:#fdf5e0;">
            <?php } elseif ($_smarty_tpl->tpl_vars['v']->value->timediff <= 30) {?>
            <tr style="background:#fedfe4;">
            <?php }?>
            <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

            	<h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
            	<p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
              <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDet">View order details</a></p>
            </td>
            <td>
            	<p>	<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->store_name)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->store_name;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->phone)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
<br><?php }?>              
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address1)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address2)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->zip)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>    
				</p>
				<p>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->tab, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
					<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
 
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>			
              </p>
            </td>
            <td>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
					<p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </td>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
             <?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'pickup_cancel') {?>
            
             <td><a  onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;
if ($_smarty_tpl->tpl_vars['b']->value->code == 'pickup_prc') {?>,<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
 <?php }?>)" href="javascript:void(0)">	<img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt=""></td>
             <?php }?>
           <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'pickup_cancel') {?>
           <td>	<a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
)" href="javascript:void(0)">Cancel</a></td>
           <?php }?>
	        
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </tr>
         <?php }?> 
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          
   
          
        </tbody></table>
				<h2>PROCESSED</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
          
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
           <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addressList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == '1') {?>
          
          <tr>
             <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

            	<h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
            	<p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
              <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDet">View order details</a></p>
            </td>
           
              <td>
				<p>	<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->store_name)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->store_name;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->phone)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
<br><?php }?>              
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address1)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address2)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->zip)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>              
				</p>
				<p>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->tab, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
					<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
 
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>			
              </p>
            </td>
            <td>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
					<p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
            </td>
             
          
           <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
           			<?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 1 && $_smarty_tpl->tpl_vars['b']->value->id == 1) {?>
           			  <td><a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,0)" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
           			<?php }?>
           		   <?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'pickup_cancel' && $_smarty_tpl->tpl_vars['b']->value->code != 'pickup_prc') {?>
             <td><a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
)" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt=""></td>
             <?php }?>
           <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'pickup_cancel') {?>
           <td>	<a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
)" href="javascript:void(0)">Cancel</a></td>
           <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </tr>
           <?php }?> 
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody></table>
        <h2>BAGGED / READY</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addressList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 2) {?>
          
          <tr>
             <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

            	<h3><?php echo $_smarty_tpl->tpl_vars['v']->value->time;?>
</h3>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
              <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDet">View order details</a></p>
            </td>
           
              <td>
				<p>	<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->store_name)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->store_name;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->phone)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
<br><?php }?>              
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address1)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address2)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->zip)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>              
				</p>
				<p>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->tab, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
					<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
 
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>			
              </p>
            </td>
            <td>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
					<p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
     
            </td>
         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
           			<?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 2 && $_smarty_tpl->tpl_vars['b']->value->id == 2) {?>
           			  <td><a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,0)" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
           			   <td><a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,1)" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
           			<?php }?>
           		<?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'pickup_cancel' && $_smarty_tpl->tpl_vars['b']->value->code != 'pickup_prc' && $_smarty_tpl->tpl_vars['b']->value->code != 'pickup_bag') {?>
             <td><a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
)" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt=""></td>
             <?php }?>
           <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'pickup_cancel') {?>
           <td>	<a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
)" href="javascript:void(0)">Cancel</a></td>
           <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </tr>
           <?php }?> 
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          
        
          
        </tbody></table>
        <h2>PICKED UP</h2>
        <table class="deliveries-new-table border-bottom">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
        
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
          </tr>
          <tr>
           <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addressList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 3) {?>
          
          <tr>
             <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

            	<h3><?php echo $_smarty_tpl->tpl_vars['v']->value->time;?>
</h3>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
              <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDet">View order details</a></p>
            </td>
           
            <td>
				<p>	<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->store_name)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->store_name;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->phone)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
<br><?php }?>              
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address1)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address2)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
					<?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->zip)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>              
				</p>
				<p>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->tab, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
					<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
 
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>			
              </p>
            </td>
            <td>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
					<p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </td>
          
          
            <td><a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,0)" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,1)" href="javascript:void(0)""><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="managePickupData(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,2)" href="javascript:void(0)""><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
              
          </tr>
             <?php }?> 
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         
          
        </tbody></table>
       </div>
       <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['total_pickUps']->value == 0) {?>
      <div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div>
      <?php }?>
    </div>
  </div><?php }
}
