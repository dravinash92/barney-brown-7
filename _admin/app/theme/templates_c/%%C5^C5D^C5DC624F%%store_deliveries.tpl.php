<?php /* Smarty version 2.6.25, created on 2020-09-25 01:53:16
         compiled from store_deliveries.tpl */ ?>
<div class="container">
    <div class="deliveries">
       <h1>DELIVERIES</h1>
       
       <div class="top-deliveries-wrapper">
          <div class="left">
              <table class="top-deliveries-table">
                <tbody><tr>
                  <td>NEW</td>
                  <td><?php echo $this->_tpl_vars['count_new']; ?>
</td>
                </tr>
                <tr>
                  <td>PRC</td>
                  <td><?php echo $this->_tpl_vars['count_prc']; ?>
</td>
                </tr>
                <tr>
                  <td>OUT</td>
                  <td><?php echo $this->_tpl_vars['count_bag']; ?>
</td>
                </tr>
                <tr>
                  <td>DEL</td>
                  <td><?php echo $this->_tpl_vars['count_pick']; ?>
</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td><?php echo $this->_tpl_vars['total_delivery']; ?>
</td>
                </tr>
              </tbody></table>
          </div>
          
          <div class="right">
          <form action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
storedeliveries/searchDeliveryData" name="delivery_search" method="POST">
            <div class="text-box-holder">
              <label>Store Location</label>
              <select name="store_id" id="store_id" class="select" disabled>
               <option value=" ">--Select--</option>
                 <?php $_from = $this->_tpl_vars['stores']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['store']):
?>
          <option <?php if ($this->_tpl_vars['store']->id == $this->_tpl_vars['storeId']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['store']->id; ?>
"><?php echo $this->_tpl_vars['store']->store_name; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
              </select>
            </div>
            <div class="text-box-holder">
              <label>Status</label>
              <select name="delivery_status" id="1" class="select">
              <option value="">--Select--</option>
              <option value="0">New</option>
                <?php $_from = $this->_tpl_vars['getOrderSatusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
                <option value="<?php echo $this->_tpl_vars['b']->id; ?>
" <?php if ($this->_tpl_vars['del_status'] == $this->_tpl_vars['b']->id): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['b']->label; ?>
</option>
              <?php endforeach; endif; unset($_from); ?>    
              </select>
            </div>
            <div class="text-box-holder2">
              <label>Date</label>
              <input id="search_delivery_date" name="search_date" type="text"  placeholder="<?php echo $this->_tpl_vars['date']; ?>
" value="<?php echo $this->_tpl_vars['date']; ?>
" class="text-box">
              <a href="#" class="date-icon"></a>
            </div>
             <input type="submit" name="submit" value="SEARCH" style="margin-top: 15px;margin-left: 26px;">
             </form>
          </div>
         
       </div>
      <?php if ($this->_tpl_vars['total_delivery'] != 0): ?> 
       <div class="deliveries-table-detail-wrapper">
         <h2>NEW</h2>
         <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
          
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
            <?php $_from = $this->_tpl_vars['DeliveryDataList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
          <?php if ($this->_tpl_vars['v']->order_status == 0): ?>
          <?php if (( $this->_tpl_vars['v']->timediff > 45 ) && ( $this->_tpl_vars['v']->time == 1 )): ?>  
          <tr style="background: #cef2cc;">
            <?php elseif (( $this->_tpl_vars['v']->timediff <= 45 ) && ( $this->_tpl_vars['v']->timediff > 30 ) && ( $this->_tpl_vars['v']->time == 1 )): ?>
            <tr style="background:#fffcb5;">
              <?php elseif (( $this->_tpl_vars['v']->timediff <= 30 ) && ( $this->_tpl_vars['v']->time == 1 )): ?>
              <tr style="background:#fedfe4;">
                <?php else: ?>
                <tr>
                <?php endif; ?>
            <td><?php echo $this->_tpl_vars['v']->name; ?>

              <h3><?php echo $this->_tpl_vars['v']->timeformat; ?>
</h3>
              <p><?php echo $this->_tpl_vars['v']->dayname; ?>
</p>
              <p><?php echo $this->_tpl_vars['v']->date; ?>
</p>
        <?php if ($this->_tpl_vars['v']->application): ?><p>From <?php echo $this->_tpl_vars['v']->application; ?>
</p><?php endif; ?>
              <p><a href="#" id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p><?php echo $this->_tpl_vars['v']->address->name; ?>
<br>
              <?php echo $this->_tpl_vars['v']->address->phone; ?>
<br>              
          <?php if ($this->_tpl_vars['v']->address->address1 != ""): ?><?php echo $this->_tpl_vars['v']->address->address1; ?>
</br><?php endif; ?>
          <?php if ($this->_tpl_vars['v']->address->address2 != ""): ?><?php echo $this->_tpl_vars['v']->address->address2; ?>
</br><?php endif; ?>
          <?php if ($this->_tpl_vars['v']->address->zip != ""): ?><?php echo $this->_tpl_vars['v']->address->zip; ?>
</br><?php endif; ?>
              
              </p>
            </td>
            <td>
           
            <?php $_from = $this->_tpl_vars['v']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
        <p>(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 </p>
        <?php if (isset ( $this->_tpl_vars['item']->modifiers ) && $this->_tpl_vars['item']->modifiers != '' && $this->_tpl_vars['item']->modifiers != '0'): ?>
        <div class="item-modifier">Modifiers: <?php echo $this->_tpl_vars['item']->modifiers; ?>
</div><?php endif; ?>
        <?php if (isset ( $this->_tpl_vars['item']->spl_instructions ) && $this->_tpl_vars['item']->spl_instructions != ''): ?>
        <div class="item-modifier">Special Instructions: <?php echo $this->_tpl_vars['item']->spl_instructions; ?>
</div><?php endif; ?>
      <?php endforeach; endif; unset($_from); ?>
             
            </td>
        
           <?php $_from = $this->_tpl_vars['getOrderSatusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
                 <?php if ($this->_tpl_vars['b']->code != 'Delivery_Cancel'): ?>
                 <td><a id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDetStore" onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
<?php if ($this->_tpl_vars['b']->id == 5): ?>,<?php echo $this->_tpl_vars['v']->order_number; ?>
<?php endif; ?>)" href="javascript:void(0)" ><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box1.png" alt="">
                 
                 </td>
                 <?php endif; ?>
               <?php if ($this->_tpl_vars['b']->code == 'Delivery_Cancel'): ?>
               <td></td>
               <?php endif; ?>
              
            <?php endforeach; endif; unset($_from); ?>
         
          </tr>  
         <?php endif; ?>
       <?php endforeach; endif; unset($_from); ?>
          
          
        </tbody></table>
        <h2>PROCESSED</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
       
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
          <?php $_from = $this->_tpl_vars['DeliveryDataList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
          <?php if ($this->_tpl_vars['v']->order_status == 5): ?>
           <tr>
            <td><?php echo $this->_tpl_vars['v']->name; ?>

              <h3><?php echo $this->_tpl_vars['v']->timeformat; ?>
</h3>
              <p><?php echo $this->_tpl_vars['v']->dayname; ?>
</p>
              <p><?php echo $this->_tpl_vars['v']->date; ?>
</p>
              <p><a href="#" id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p><?php echo $this->_tpl_vars['v']->address->name; ?>
<br>
              <?php echo $this->_tpl_vars['v']->address->phone; ?>
<br>              
          <?php if ($this->_tpl_vars['v']->address->address1 != ""): ?><?php echo $this->_tpl_vars['v']->address->address1; ?>
</br><?php endif; ?>
          <?php if ($this->_tpl_vars['v']->address->address2 != ""): ?><?php echo $this->_tpl_vars['v']->address->address2; ?>
</br><?php endif; ?>
          <?php if ($this->_tpl_vars['v']->address->zip != ""): ?><?php echo $this->_tpl_vars['v']->address->zip; ?>
</br><?php endif; ?>
              
              </p>
            </td>
            <td>
           
            <?php $_from = $this->_tpl_vars['v']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
        <p>(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 </p>
        <?php if (isset ( $this->_tpl_vars['item']->modifiers ) && $this->_tpl_vars['item']->modifiers != '' && $this->_tpl_vars['item']->modifiers != '0'): ?>
          <div class="item-modifier">Modifiers: <?php echo $this->_tpl_vars['item']->modifiers; ?>
</div>
        <?php endif; ?>
        <?php if (isset ( $this->_tpl_vars['item']->spl_instructions ) && $this->_tpl_vars['item']->spl_instructions != ''): ?>
          <div class="item-modifier">Special Instructions: <?php echo $this->_tpl_vars['item']->spl_instructions; ?>
</div>
        <?php endif; ?>
      <?php endforeach; endif; unset($_from); ?>
             
            </td>
        
        
             <?php $_from = $this->_tpl_vars['getOrderSatusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
                <?php if ($this->_tpl_vars['v']->order_status == 5 && $this->_tpl_vars['b']->id == 5): ?>
                <td><a onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,0)" href="javascript:void(0)" ><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
              <?php endif; ?>
              <?php if ($this->_tpl_vars['b']->code != 'Delivery_Cancel' && $this->_tpl_vars['b']->code != 'Delivery_Pro'): ?>
                <td style="position: relative;"><a onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
,'',<?php echo $this->_tpl_vars['v']->order_status; ?>
)" href="javascript:void(0)"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box1.png" alt=""></a>
                  <div class="assign-store-order" id="storedelivery_user_assign_<?php echo $this->_tpl_vars['v']->order_id; ?>
" style="display: none;">
                  <i class="close-btn" id="close-storeassign-user_<?php echo $this->_tpl_vars['v']->order_id; ?>
">close</i>
                  <div class="assign-title">
                    <h1>ASSIGN ORDER</h1>
                  </div>
                  <div class="asign-order-select">
                    <select class="storedeliveryUserOptions_<?php echo $this->_tpl_vars['v']->order_id; ?>
"> 
                      <option value="0">SELECT</option>
                    </select>
                  </div>
                  <div class="assign-submit">
                    <button class="assign_storedel_user" data-orderid= "<?php echo $this->_tpl_vars['v']->order_id; ?>
">Assign</button>
                  </div>
                </div>
                </td>
              <?php endif; ?>
             <?php if ($this->_tpl_vars['b']->code == 'Delivery_Cancel'): ?>
                 <td></td>
             <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
       
          </tr>  
         <?php endif; ?>
       <?php endforeach; endif; unset($_from); ?>
         
          
        </tbody></table>
        <h2>OUT FOR DELIVERY</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>   
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
            <?php $_from = $this->_tpl_vars['DeliveryDataList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
          <?php if ($this->_tpl_vars['v']->order_status == 6): ?>
          <tr>
            <td><?php echo $this->_tpl_vars['v']->name; ?>

              <h3><?php echo $this->_tpl_vars['v']->timeformat; ?>
</h3>
              <p><?php echo $this->_tpl_vars['v']->dayname; ?>
</p>
              <p><?php echo $this->_tpl_vars['v']->date; ?>
</p>
              <p><a href="#" id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p><?php echo $this->_tpl_vars['v']->address->name; ?>
<br>
              <?php echo $this->_tpl_vars['v']->address->phone; ?>
<br>              
          <?php if ($this->_tpl_vars['v']->address->address1 != ""): ?><?php echo $this->_tpl_vars['v']->address->address1; ?>
</br><?php endif; ?>
          <?php if ($this->_tpl_vars['v']->address->address2 != ""): ?><?php echo $this->_tpl_vars['v']->address->address2; ?>
</br><?php endif; ?>
          <?php if ($this->_tpl_vars['v']->address->zip != ""): ?><?php echo $this->_tpl_vars['v']->address->zip; ?>
</br><?php endif; ?>
              
              </p>
            </td>
            <td>
           
            <?php $_from = $this->_tpl_vars['v']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
        <p>(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 </p>
        <?php if (isset ( $this->_tpl_vars['item']->modifiers ) && $this->_tpl_vars['item']->modifiers != '' && $this->_tpl_vars['item']->modifiers != '0'): ?>
          <div class="item-modifier">Modifiers: <?php echo $this->_tpl_vars['item']->modifiers; ?>
</div>
        <?php endif; ?>
        <?php if (isset ( $this->_tpl_vars['item']->spl_instructions ) && $this->_tpl_vars['item']->spl_instructions != ''): ?>
          <div class="item-modifier">Special Instructions: <?php echo $this->_tpl_vars['item']->spl_instructions; ?>
</div>
        <?php endif; ?>
      <?php endforeach; endif; unset($_from); ?>
             
            </td>
        
           <?php $_from = $this->_tpl_vars['getOrderSatusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
              <?php if ($this->_tpl_vars['v']->order_status == 6 && $this->_tpl_vars['b']->id == 6): ?>
                  <td><a onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,5)" href="javascript:void(0)" ><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a>
                  </td>
                   <td><a onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,5)" href="javascript:void(0)" ><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a>
                   </td>
                <?php endif; ?>
              <?php if ($this->_tpl_vars['b']->code != 'Delivery_Cancel' && $this->_tpl_vars['b']->code != 'Delivery_Pro' && $this->_tpl_vars['b']->code != 'Delivery_out'): ?>
             <td><a onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
)" href="javascript:void(0)" ><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box1.png" alt=""></td>
             <?php endif; ?>
           <?php if ($this->_tpl_vars['b']->code == 'Delivery_Cancel'): ?>
           <td> </td>
           <?php endif; ?>
              
            <?php endforeach; endif; unset($_from); ?>
         
          </tr>
          <tr class="no-border-up">
  <td></td>
  <td></td>
  <td></td>
<td><b class="bold">Assigned: </b></td>
  <?php if ($this->_tpl_vars['v']->delivery_assigned_to != 0): ?>
  <td><?php echo $this->_tpl_vars['v']->delivery_uname; ?>
</td>
  <td><?php echo $this->_tpl_vars['v']->delivery_assigned_time; ?>
</td>
  <?php else: ?>
  <td></td>
  <td></td>
  <?php endif; ?>
  <td></td>


</tr>  
         <?php endif; ?>
       <?php endforeach; endif; unset($_from); ?>
        
          
        </tbody></table>
        <h2>DELIVERED</h2>
        <table class="deliveries-new-table border-bottom">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
           
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
         <?php $_from = $this->_tpl_vars['DeliverySortDataList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
          <?php if ($this->_tpl_vars['v']->order_status == 7): ?>
           <tr>
            <td><?php echo $this->_tpl_vars['v']->name; ?>

              <h3><?php echo $this->_tpl_vars['v']->timeformat; ?>
</h3>
              <p><?php echo $this->_tpl_vars['v']->dayname; ?>
</p>
              <p><?php echo $this->_tpl_vars['v']->date; ?>
</p>
              <p><a href="#" id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p><?php echo $this->_tpl_vars['v']->address->name; ?>
<br>
              <?php echo $this->_tpl_vars['v']->address->phone; ?>
<br>              
          <?php if ($this->_tpl_vars['v']->address->address1 != ""): ?><?php echo $this->_tpl_vars['v']->address->address1; ?>
</br><?php endif; ?>
          <?php if ($this->_tpl_vars['v']->address->address2 != ""): ?><?php echo $this->_tpl_vars['v']->address->address2; ?>
</br><?php endif; ?>
          <?php if ($this->_tpl_vars['v']->address->zip != ""): ?><?php echo $this->_tpl_vars['v']->address->zip; ?>
</br><?php endif; ?>
              
              </p>
            </td>
            <td>
           
            <?php $_from = $this->_tpl_vars['v']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
        <p>(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 </p>
        <?php if (isset ( $this->_tpl_vars['item']->modifiers ) && $this->_tpl_vars['item']->modifiers != '' && $this->_tpl_vars['item']->modifiers != '0'): ?>
          <div class="item-modifier">Modifiers: <?php echo $this->_tpl_vars['item']->modifiers; ?>
</div>
        <?php endif; ?>
        <?php if (isset ( $this->_tpl_vars['item']->spl_instructions ) && $this->_tpl_vars['item']->spl_instructions != ''): ?>
          <div class="item-modifier">Special Instructions: <?php echo $this->_tpl_vars['item']->spl_instructions; ?>
</div>
        <?php endif; ?>
      <?php endforeach; endif; unset($_from); ?>
             
            </td>
        
        <td><a onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,6)" href="javascript:void(0)" ><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,6)" href="javascript:void(0)" ><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="manageStoreDelivereis(<?php echo $this->_tpl_vars['v']->order_id; ?>
,6)" href="javascript:void(0)"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
            <td></td>
          </tr>  
          <tr class="no-border-up">
  <td></td>
  <td></td>
  <td></td>
<td><b class="bold">Delivered: </b></td>
 <?php if ($this->_tpl_vars['v']->delivery_assigned_to != 0): ?>
  <td><?php echo $this->_tpl_vars['v']->delivery_uname; ?>
</td>
  <td><?php echo $this->_tpl_vars['v']->order_delivered; ?>
</td>
  <?php else: ?>
  <td></td>
  <td></td>
  <?php endif; ?>
  <td></td>


</tr>
         <?php endif; ?>
       <?php endforeach; endif; unset($_from); ?>
        
          
        </tbody></table>
       </div>
       <?php endif; ?>
      <?php if ($this->_tpl_vars['total_delivery'] == 0): ?><div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div><?php endif; ?>
    </div>
  </div>