<?php /* Smarty version 2.6.25, created on 2018-04-10 13:29:19
         compiled from edit_sandwich.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'array_keys', 'edit_sandwich.tpl', 24, false),array('modifier', 'array_values', 'edit_sandwich.tpl', 30, false),array('modifier', 'count', 'edit_sandwich.tpl', 61, false),array('modifier', 'in_array', 'edit_sandwich.tpl', 104, false),)), $this); ?>

<div class="container">
    <div class="sandwich">
      <h1>EDIT SANDWICH</h1>
      
     
      
      <p> ID#: 348650<br>
        User: <?php echo $this->_tpl_vars['EDIT_DATA'][0]['user_name']; ?>
 (<?php echo $this->_tpl_vars['EDIT_DATA'][0]['user_email']; ?>
)<br>
        Date Created: 
      
        
        06/15/2013<br>
        Likes: 100<br>
        Purchases: 5 <br>
       
       <?php echo $this->_tpl_vars['EDIT_DATA'][0]['description']; ?>

       
       <?php $this->assign('protien_itemid', $this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['PROTEIN']['item_id']); ?>
       <?php $this->assign('cheese_itemid', $this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['CHEESE']['item_id']); ?>
       <?php $this->assign('topping_itemid', $this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['TOPPINGS']['item_id']); ?>
       <?php $this->assign('cond_itemid', $this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['CONDIMENTS']['item_id']); ?>
       
       <?php $this->assign('protien_item_qty_key', array_keys($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['PROTEIN']['item_qty'])); ?>
       <?php $this->assign('cheese_item_qty_key', array_keys($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['CHEESE']['item_qty'])); ?>
       <?php $this->assign('topping_item_qty_key', array_keys($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['TOPPINGS']['item_qty'])); ?>
       <?php $this->assign('cond_item_qty_key', array_keys($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['CONDIMENTS']['item_qty'])); ?>
       
   
       <?php $this->assign('protien_item_qty_value', array_values($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['PROTEIN']['item_qty'])); ?>
       <?php $this->assign('cheese_item_qty_value', array_values($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['CHEESE']['item_qty'])); ?>
       <?php $this->assign('topping_item_qty_value', array_values($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['TOPPINGS']['item_qty'])); ?>
       <?php $this->assign('cond_item_qty_value', array_values($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['CONDIMENTS']['item_qty'])); ?>
 
       <input type="hidden" value="<?php echo $this->_tpl_vars['EDIT_DATA'][0]['sandwich_name']; ?>
" name='sandwich_name' />  
       <input type="hidden" value="<?php echo $this->_tpl_vars['EDIT_DATA'][0]['sandwich_price']; ?>
" name='sandwich_price' />       
       <input type="hidden" value="<?php echo $this->_tpl_vars['EDIT_DATA'][0]['menu_is_active']; ?>
" name='sandwich_menustate' />
       <input type="hidden" value="<?php echo $this->_tpl_vars['EDIT_DATA'][0]['id']; ?>
" name='sandwich_id' />      
       <input type="hidden" value="<?php echo $this->_tpl_vars['EDIT_DATA'][0]['uid']; ?>
" name='sandwich_uid' />      
               
       <script>admin_ha.common.json_obj = JSON.parse('<?php echo $this->_tpl_vars['EDIT_DATA'][0]['sandwich_data_json']; ?>
');</script>   

      </p>
      <ul>
        <li>
          <label>Name of Sandwich</label>
          <input type="text" name="sandwichname" value=" <?php echo $this->_tpl_vars['EDIT_DATA'][0]['sandwich_name']; ?>
">
        </li>
        <li>
          <label>Description</label>
          <textarea name="description" id="description"><?php echo $this->_tpl_vars['EDIT_DATA'][0]['description']; ?>
</textarea>
        </li>
        <li> <span class="multi-left">
          <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="public" <?php if ($this->_tpl_vars['EDIT_DATA'][0]['is_public'] == 1): ?> checked="checked" <?php endif; ?> value="">
          <label for="checkbox-2-1" class="multisel-ckeck"></label>
          </span> <span>Public</span> </li>
      </ul>
      <div class="custom-sandwich">
        <h2>CUSTOM SANDWICH</h2>
        
        <?php if (count($this->_tpl_vars['BREAD_DATA']) > 0): ?>
        <div class="bread">
          <h3>BREAD</h3>
          <ul>
          
            <?php unset($this->_sections['bread']);
$this->_sections['bread']['name'] = 'bread';
$this->_sections['bread']['start'] = (int)0;
$this->_sections['bread']['loop'] = is_array($_loop=count($this->_tpl_vars['BREAD_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['bread']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['bread']['show'] = true;
$this->_sections['bread']['max'] = $this->_sections['bread']['loop'];
if ($this->_sections['bread']['start'] < 0)
    $this->_sections['bread']['start'] = max($this->_sections['bread']['step'] > 0 ? 0 : -1, $this->_sections['bread']['loop'] + $this->_sections['bread']['start']);
else
    $this->_sections['bread']['start'] = min($this->_sections['bread']['start'], $this->_sections['bread']['step'] > 0 ? $this->_sections['bread']['loop'] : $this->_sections['bread']['loop']-1);
if ($this->_sections['bread']['show']) {
    $this->_sections['bread']['total'] = min(ceil(($this->_sections['bread']['step'] > 0 ? $this->_sections['bread']['loop'] - $this->_sections['bread']['start'] : $this->_sections['bread']['start']+1)/abs($this->_sections['bread']['step'])), $this->_sections['bread']['max']);
    if ($this->_sections['bread']['total'] == 0)
        $this->_sections['bread']['show'] = false;
} else
    $this->_sections['bread']['total'] = 0;
if ($this->_sections['bread']['show']):

            for ($this->_sections['bread']['index'] = $this->_sections['bread']['start'], $this->_sections['bread']['iteration'] = 1;
                 $this->_sections['bread']['iteration'] <= $this->_sections['bread']['total'];
                 $this->_sections['bread']['index'] += $this->_sections['bread']['step'], $this->_sections['bread']['iteration']++):
$this->_sections['bread']['rownum'] = $this->_sections['bread']['iteration'];
$this->_sections['bread']['index_prev'] = $this->_sections['bread']['index'] - $this->_sections['bread']['step'];
$this->_sections['bread']['index_next'] = $this->_sections['bread']['index'] + $this->_sections['bread']['step'];
$this->_sections['bread']['first']      = ($this->_sections['bread']['iteration'] == 1);
$this->_sections['bread']['last']       = ($this->_sections['bread']['iteration'] == $this->_sections['bread']['total']);
?>
            
          
            
            <li>
              <div class="radio-section">
                <input 
                
                data-id = "<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['bread']['index']]['id']; ?>
"
                data-item_price = "<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['bread']['index']]['item_price']; ?>
"
                 data-item_name = "<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['bread']['index']]['item_name']; ?>
"
                 data-category = "BREAD"
                <?php if ($this->_tpl_vars['EDIT_DATA'][0]['sandwich_data']['BREAD']['item_id'][0] == $this->_tpl_vars['BREAD_DATA'][$this->_sections['bread']['index']]['id']): ?>   checked="checked" <?php endif; ?>
                
                type="radio" value="Redirect" class="input-radio" name="Redirect" id="1-<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['bread']['index']]['id']; ?>
">
                <label for="1-<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['bread']['index']]['id']; ?>
"></label>
              </div>
              <span class="radio-text"> <?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['bread']['index']]['item_name']; ?>
 </span> </li>
            <?php endfor; endif; ?>
          </ul>
        </div>
        <?php endif; ?>
        
        
        <?php if (count($this->_tpl_vars['PROTEIN_DATA']) > 0): ?>
        <div class="meats">
          <h3>Meats</h3>
          <ul>
           <?php unset($this->_sections['protein']);
$this->_sections['protein']['name'] = 'protein';
$this->_sections['protein']['start'] = (int)0;
$this->_sections['protein']['loop'] = is_array($_loop=count($this->_tpl_vars['PROTEIN_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['protein']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['protein']['show'] = true;
$this->_sections['protein']['max'] = $this->_sections['protein']['loop'];
if ($this->_sections['protein']['start'] < 0)
    $this->_sections['protein']['start'] = max($this->_sections['protein']['step'] > 0 ? 0 : -1, $this->_sections['protein']['loop'] + $this->_sections['protein']['start']);
else
    $this->_sections['protein']['start'] = min($this->_sections['protein']['start'], $this->_sections['protein']['step'] > 0 ? $this->_sections['protein']['loop'] : $this->_sections['protein']['loop']-1);
if ($this->_sections['protein']['show']) {
    $this->_sections['protein']['total'] = min(ceil(($this->_sections['protein']['step'] > 0 ? $this->_sections['protein']['loop'] - $this->_sections['protein']['start'] : $this->_sections['protein']['start']+1)/abs($this->_sections['protein']['step'])), $this->_sections['protein']['max']);
    if ($this->_sections['protein']['total'] == 0)
        $this->_sections['protein']['show'] = false;
} else
    $this->_sections['protein']['total'] = 0;
if ($this->_sections['protein']['show']):

            for ($this->_sections['protein']['index'] = $this->_sections['protein']['start'], $this->_sections['protein']['iteration'] = 1;
                 $this->_sections['protein']['iteration'] <= $this->_sections['protein']['total'];
                 $this->_sections['protein']['index'] += $this->_sections['protein']['step'], $this->_sections['protein']['iteration']++):
$this->_sections['protein']['rownum'] = $this->_sections['protein']['iteration'];
$this->_sections['protein']['index_prev'] = $this->_sections['protein']['index'] - $this->_sections['protein']['step'];
$this->_sections['protein']['index_next'] = $this->_sections['protein']['index'] + $this->_sections['protein']['step'];
$this->_sections['protein']['first']      = ($this->_sections['protein']['iteration'] == 1);
$this->_sections['protein']['last']       = ($this->_sections['protein']['iteration'] == $this->_sections['protein']['total']);
?>
           <?php $this->assign('prot_name', $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['item_name']); ?>
             <li><span class="multi-left">
              <input 
              
              data-id = "<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['id']; ?>
"
              data-item_price = "<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['item_price']; ?>
"
              data-item_name = "<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['item_name']; ?>
"
              data-image = "<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['item_image']; ?>
"
               data-category = "PROTEIN"
               <?php if (((is_array($_tmp=$this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['protien_itemid']) : in_array($_tmp, $this->_tpl_vars['protien_itemid'])) == 1): ?>
                checked="checked" 
                <?php endif; ?>
              type="checkbox" id="2-<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['id']; ?>
" class="input-checkbox" name="" value="<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['item_name']; ?>
">
              <label for="2-<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['id']; ?>
" class="multisel-ckeck"></label>
              </span> <span class="radio-text"><?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['item_name']; ?>
</span> 
              
                 <?php if ($this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'] != 0): ?>
                 
              <select>
                  <?php unset($this->_sections['protein_ops']);
$this->_sections['protein_ops']['name'] = 'protein_ops';
$this->_sections['protein_ops']['start'] = (int)0;
$this->_sections['protein_ops']['loop'] = is_array($_loop=count($this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['protein_ops']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['protein_ops']['show'] = true;
$this->_sections['protein_ops']['max'] = $this->_sections['protein_ops']['loop'];
if ($this->_sections['protein_ops']['start'] < 0)
    $this->_sections['protein_ops']['start'] = max($this->_sections['protein_ops']['step'] > 0 ? 0 : -1, $this->_sections['protein_ops']['loop'] + $this->_sections['protein_ops']['start']);
else
    $this->_sections['protein_ops']['start'] = min($this->_sections['protein_ops']['start'], $this->_sections['protein_ops']['step'] > 0 ? $this->_sections['protein_ops']['loop'] : $this->_sections['protein_ops']['loop']-1);
if ($this->_sections['protein_ops']['show']) {
    $this->_sections['protein_ops']['total'] = min(ceil(($this->_sections['protein_ops']['step'] > 0 ? $this->_sections['protein_ops']['loop'] - $this->_sections['protein_ops']['start'] : $this->_sections['protein_ops']['start']+1)/abs($this->_sections['protein_ops']['step'])), $this->_sections['protein_ops']['max']);
    if ($this->_sections['protein_ops']['total'] == 0)
        $this->_sections['protein_ops']['show'] = false;
} else
    $this->_sections['protein_ops']['total'] = 0;
if ($this->_sections['protein_ops']['show']):

            for ($this->_sections['protein_ops']['index'] = $this->_sections['protein_ops']['start'], $this->_sections['protein_ops']['iteration'] = 1;
                 $this->_sections['protein_ops']['iteration'] <= $this->_sections['protein_ops']['total'];
                 $this->_sections['protein_ops']['index'] += $this->_sections['protein_ops']['step'], $this->_sections['protein_ops']['iteration']++):
$this->_sections['protein_ops']['rownum'] = $this->_sections['protein_ops']['iteration'];
$this->_sections['protein_ops']['index_prev'] = $this->_sections['protein_ops']['index'] - $this->_sections['protein_ops']['step'];
$this->_sections['protein_ops']['index_next'] = $this->_sections['protein_ops']['index'] + $this->_sections['protein_ops']['step'];
$this->_sections['protein_ops']['first']      = ($this->_sections['protein_ops']['iteration'] == 1);
$this->_sections['protein_ops']['last']       = ($this->_sections['protein_ops']['iteration'] == $this->_sections['protein_ops']['total']);
?>
                 
                 
                    
                  <option 
                  
                  <?php if ($this->_tpl_vars['protien_item_qty_value']['1']['0'] == $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'][$this->_sections['protein_ops']['index']]['option_name']): ?>
                  selected
                  <?php endif; ?>
                  data-id="<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'][$this->_sections['protein_ops']['index']]['id']; ?>
" 
                  data-unit="<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'][$this->_sections['protein_ops']['index']]['option_unit']; ?>
" 
                  data-mult="<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'][$this->_sections['protein_ops']['index']]['price_mult']; ?>
" 
                  data-name="<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'][$this->_sections['protein_ops']['index']]['option_name']; ?>
" 
                  value="<?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'][$this->_sections['protein_ops']['index']]['option_name']; ?>
"><?php echo $this->_tpl_vars['PROTEIN_DATA'][$this->_sections['protein']['index']]['options_id'][$this->_sections['protein_ops']['index']]['option_name']; ?>
</option>
                  <?php endfor; endif; ?>
                 </select> 
                 <?php endif; ?>
              
              </li>
           <?php endfor; endif; ?>
          </ul>
        </div>
        <?php endif; ?>
        
        
         <?php if (count($this->_tpl_vars['CHEESE_DATA']) > 0): ?>
        
        <div class="cheese">
          <h3>CHEESES</h3>
          <ul>
            
            
            <?php unset($this->_sections['cheese']);
$this->_sections['cheese']['name'] = 'cheese';
$this->_sections['cheese']['start'] = (int)0;
$this->_sections['cheese']['loop'] = is_array($_loop=count($this->_tpl_vars['CHEESE_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cheese']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['cheese']['show'] = true;
$this->_sections['cheese']['max'] = $this->_sections['cheese']['loop'];
if ($this->_sections['cheese']['start'] < 0)
    $this->_sections['cheese']['start'] = max($this->_sections['cheese']['step'] > 0 ? 0 : -1, $this->_sections['cheese']['loop'] + $this->_sections['cheese']['start']);
else
    $this->_sections['cheese']['start'] = min($this->_sections['cheese']['start'], $this->_sections['cheese']['step'] > 0 ? $this->_sections['cheese']['loop'] : $this->_sections['cheese']['loop']-1);
if ($this->_sections['cheese']['show']) {
    $this->_sections['cheese']['total'] = min(ceil(($this->_sections['cheese']['step'] > 0 ? $this->_sections['cheese']['loop'] - $this->_sections['cheese']['start'] : $this->_sections['cheese']['start']+1)/abs($this->_sections['cheese']['step'])), $this->_sections['cheese']['max']);
    if ($this->_sections['cheese']['total'] == 0)
        $this->_sections['cheese']['show'] = false;
} else
    $this->_sections['cheese']['total'] = 0;
if ($this->_sections['cheese']['show']):

            for ($this->_sections['cheese']['index'] = $this->_sections['cheese']['start'], $this->_sections['cheese']['iteration'] = 1;
                 $this->_sections['cheese']['iteration'] <= $this->_sections['cheese']['total'];
                 $this->_sections['cheese']['index'] += $this->_sections['cheese']['step'], $this->_sections['cheese']['iteration']++):
$this->_sections['cheese']['rownum'] = $this->_sections['cheese']['iteration'];
$this->_sections['cheese']['index_prev'] = $this->_sections['cheese']['index'] - $this->_sections['cheese']['step'];
$this->_sections['cheese']['index_next'] = $this->_sections['cheese']['index'] + $this->_sections['cheese']['step'];
$this->_sections['cheese']['first']      = ($this->_sections['cheese']['iteration'] == 1);
$this->_sections['cheese']['last']       = ($this->_sections['cheese']['iteration'] == $this->_sections['cheese']['total']);
?>
           
             <li><span class="multi-left">
              <input 
              
              data-id = "<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['id']; ?>
"
              data-item_price = "<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['item_price']; ?>
"
              data-item_name = "<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['item_name']; ?>
"
              data-image = "<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['item_image']; ?>
"
              data-category = "CHEESE"
                <?php if (((is_array($_tmp=$this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['cheese_itemid']) : in_array($_tmp, $this->_tpl_vars['cheese_itemid'])) == 1): ?>
                checked="checked" 
                <?php endif; ?>
              
              type="checkbox" id="3-<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['id']; ?>
" class="input-checkbox" name="" value="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['item_name']; ?>
">
              <label for="3-<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['id']; ?>
" class="multisel-ckeck"></label>
              </span> <span class="radio-text"><?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['item_name']; ?>
</span> 
              
                 <?php if ($this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['options_id'] != 0): ?>
                 <select>
                  <?php unset($this->_sections['cheese_ops']);
$this->_sections['cheese_ops']['name'] = 'cheese_ops';
$this->_sections['cheese_ops']['start'] = (int)0;
$this->_sections['cheese_ops']['loop'] = is_array($_loop=count($this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['options_id'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cheese_ops']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['cheese_ops']['show'] = true;
$this->_sections['cheese_ops']['max'] = $this->_sections['cheese_ops']['loop'];
if ($this->_sections['cheese_ops']['start'] < 0)
    $this->_sections['cheese_ops']['start'] = max($this->_sections['cheese_ops']['step'] > 0 ? 0 : -1, $this->_sections['cheese_ops']['loop'] + $this->_sections['cheese_ops']['start']);
else
    $this->_sections['cheese_ops']['start'] = min($this->_sections['cheese_ops']['start'], $this->_sections['cheese_ops']['step'] > 0 ? $this->_sections['cheese_ops']['loop'] : $this->_sections['cheese_ops']['loop']-1);
if ($this->_sections['cheese_ops']['show']) {
    $this->_sections['cheese_ops']['total'] = min(ceil(($this->_sections['cheese_ops']['step'] > 0 ? $this->_sections['cheese_ops']['loop'] - $this->_sections['cheese_ops']['start'] : $this->_sections['cheese_ops']['start']+1)/abs($this->_sections['cheese_ops']['step'])), $this->_sections['cheese_ops']['max']);
    if ($this->_sections['cheese_ops']['total'] == 0)
        $this->_sections['cheese_ops']['show'] = false;
} else
    $this->_sections['cheese_ops']['total'] = 0;
if ($this->_sections['cheese_ops']['show']):

            for ($this->_sections['cheese_ops']['index'] = $this->_sections['cheese_ops']['start'], $this->_sections['cheese_ops']['iteration'] = 1;
                 $this->_sections['cheese_ops']['iteration'] <= $this->_sections['cheese_ops']['total'];
                 $this->_sections['cheese_ops']['index'] += $this->_sections['cheese_ops']['step'], $this->_sections['cheese_ops']['iteration']++):
$this->_sections['cheese_ops']['rownum'] = $this->_sections['cheese_ops']['iteration'];
$this->_sections['cheese_ops']['index_prev'] = $this->_sections['cheese_ops']['index'] - $this->_sections['cheese_ops']['step'];
$this->_sections['cheese_ops']['index_next'] = $this->_sections['cheese_ops']['index'] + $this->_sections['cheese_ops']['step'];
$this->_sections['cheese_ops']['first']      = ($this->_sections['cheese_ops']['iteration'] == 1);
$this->_sections['cheese_ops']['last']       = ($this->_sections['cheese_ops']['iteration'] == $this->_sections['cheese_ops']['total']);
?>
                  <option 
                  data-id="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['options_id'][$this->_sections['cheese_ops']['index']]['id']; ?>
" 
                  data-unit="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['options_id'][$this->_sections['cheese_ops']['index']]['option_unit']; ?>
" 
                  data-mult="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['options_id'][$this->_sections['cheese_ops']['index']]['price_mult']; ?>
" 
                  data-name="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['options_id'][$this->_sections['cheese_ops']['index']]['option_name']; ?>
" 
                  value="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['options_id'][$this->_sections['cheese_ops']['index']]['option_name']; ?>
"><?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['cheese']['index']]['options_id'][$this->_sections['cheese_ops']['index']]['option_name']; ?>
</option>
                  <?php endfor; endif; ?>
                 </select>
                 <?php endif; ?>
              
              </li>
           <?php endfor; endif; ?>
            
            
          </ul>
        </div>
        
         <?php endif; ?>
        
         <?php if (count($this->_tpl_vars['TOPPING_DATA']) > 0): ?>
        <div class="cheese">
          <h3>STANDARD TOPPINGS</h3>
          <ul>
          
          
            <?php unset($this->_sections['topping']);
$this->_sections['topping']['name'] = 'topping';
$this->_sections['topping']['start'] = (int)0;
$this->_sections['topping']['loop'] = is_array($_loop=count($this->_tpl_vars['TOPPING_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['topping']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['topping']['show'] = true;
$this->_sections['topping']['max'] = $this->_sections['topping']['loop'];
if ($this->_sections['topping']['start'] < 0)
    $this->_sections['topping']['start'] = max($this->_sections['topping']['step'] > 0 ? 0 : -1, $this->_sections['topping']['loop'] + $this->_sections['topping']['start']);
else
    $this->_sections['topping']['start'] = min($this->_sections['topping']['start'], $this->_sections['topping']['step'] > 0 ? $this->_sections['topping']['loop'] : $this->_sections['topping']['loop']-1);
if ($this->_sections['topping']['show']) {
    $this->_sections['topping']['total'] = min(ceil(($this->_sections['topping']['step'] > 0 ? $this->_sections['topping']['loop'] - $this->_sections['topping']['start'] : $this->_sections['topping']['start']+1)/abs($this->_sections['topping']['step'])), $this->_sections['topping']['max']);
    if ($this->_sections['topping']['total'] == 0)
        $this->_sections['topping']['show'] = false;
} else
    $this->_sections['topping']['total'] = 0;
if ($this->_sections['topping']['show']):

            for ($this->_sections['topping']['index'] = $this->_sections['topping']['start'], $this->_sections['topping']['iteration'] = 1;
                 $this->_sections['topping']['iteration'] <= $this->_sections['topping']['total'];
                 $this->_sections['topping']['index'] += $this->_sections['topping']['step'], $this->_sections['topping']['iteration']++):
$this->_sections['topping']['rownum'] = $this->_sections['topping']['iteration'];
$this->_sections['topping']['index_prev'] = $this->_sections['topping']['index'] - $this->_sections['topping']['step'];
$this->_sections['topping']['index_next'] = $this->_sections['topping']['index'] + $this->_sections['topping']['step'];
$this->_sections['topping']['first']      = ($this->_sections['topping']['iteration'] == 1);
$this->_sections['topping']['last']       = ($this->_sections['topping']['iteration'] == $this->_sections['topping']['total']);
?>
           
             <li><span class="multi-left">
              <input 
              
              data-id = "<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['id']; ?>
"
              data-item_price = "<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['item_price']; ?>
"
              data-item_name = "<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['item_name']; ?>
"
                data-image = "<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['item_image']; ?>
"
              data-category = "TOPPINGS"
               <?php if (((is_array($_tmp=$this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['topping_itemid']) : in_array($_tmp, $this->_tpl_vars['topping_itemid'])) == 1): ?>
                checked="checked" 
                <?php endif; ?>
              
              type="checkbox" id="4-<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['id']; ?>
" class="input-checkbox" name="" value="<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['item_name']; ?>
">
              <label for="4-<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['id']; ?>
" class="multisel-ckeck"></label>
              </span> <span class="radio-text"><?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['item_name']; ?>
</span> 
              
                 <?php if ($this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['options_id'] != 0): ?>
                 <select>
                  <?php unset($this->_sections['topping_ops']);
$this->_sections['topping_ops']['name'] = 'topping_ops';
$this->_sections['topping_ops']['start'] = (int)0;
$this->_sections['topping_ops']['loop'] = is_array($_loop=count($this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['options_id'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['topping_ops']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['topping_ops']['show'] = true;
$this->_sections['topping_ops']['max'] = $this->_sections['topping_ops']['loop'];
if ($this->_sections['topping_ops']['start'] < 0)
    $this->_sections['topping_ops']['start'] = max($this->_sections['topping_ops']['step'] > 0 ? 0 : -1, $this->_sections['topping_ops']['loop'] + $this->_sections['topping_ops']['start']);
else
    $this->_sections['topping_ops']['start'] = min($this->_sections['topping_ops']['start'], $this->_sections['topping_ops']['step'] > 0 ? $this->_sections['topping_ops']['loop'] : $this->_sections['topping_ops']['loop']-1);
if ($this->_sections['topping_ops']['show']) {
    $this->_sections['topping_ops']['total'] = min(ceil(($this->_sections['topping_ops']['step'] > 0 ? $this->_sections['topping_ops']['loop'] - $this->_sections['topping_ops']['start'] : $this->_sections['topping_ops']['start']+1)/abs($this->_sections['topping_ops']['step'])), $this->_sections['topping_ops']['max']);
    if ($this->_sections['topping_ops']['total'] == 0)
        $this->_sections['topping_ops']['show'] = false;
} else
    $this->_sections['topping_ops']['total'] = 0;
if ($this->_sections['topping_ops']['show']):

            for ($this->_sections['topping_ops']['index'] = $this->_sections['topping_ops']['start'], $this->_sections['topping_ops']['iteration'] = 1;
                 $this->_sections['topping_ops']['iteration'] <= $this->_sections['topping_ops']['total'];
                 $this->_sections['topping_ops']['index'] += $this->_sections['topping_ops']['step'], $this->_sections['topping_ops']['iteration']++):
$this->_sections['topping_ops']['rownum'] = $this->_sections['topping_ops']['iteration'];
$this->_sections['topping_ops']['index_prev'] = $this->_sections['topping_ops']['index'] - $this->_sections['topping_ops']['step'];
$this->_sections['topping_ops']['index_next'] = $this->_sections['topping_ops']['index'] + $this->_sections['topping_ops']['step'];
$this->_sections['topping_ops']['first']      = ($this->_sections['topping_ops']['iteration'] == 1);
$this->_sections['topping_ops']['last']       = ($this->_sections['topping_ops']['iteration'] == $this->_sections['topping_ops']['total']);
?>
                  <option 
                  data-id="<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['options_id'][$this->_sections['topping_ops']['index']]['id']; ?>
" 
                  data-unit="<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['options_id'][$this->_sections['topping_ops']['index']]['option_unit']; ?>
" 
                  data-mult="<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['options_id'][$this->_sections['topping_ops']['index']]['price_mult']; ?>
" 
                  data-name="<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['options_id'][$this->_sections['topping_ops']['index']]['option_name']; ?>
" 
                  value="<?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['options_id'][$this->_sections['topping_ops']['index']]['option_name']; ?>
"><?php echo $this->_tpl_vars['TOPPING_DATA'][$this->_sections['topping']['index']]['options_id'][$this->_sections['topping_ops']['index']]['option_name']; ?>
</option>
                  <?php endfor; endif; ?>
                 </select>
                 <?php endif; ?>
              
              </li>
           <?php endfor; endif; ?>
            
          </ul>
        </div>
          <?php endif; ?>
        
        <?php if (count($this->_tpl_vars['CONDIMENTS_DATA']) > 0): ?>
        <div class="cheese">
          <h3>CONDIMENTS</h3>
          <ul>
          
          
            <?php unset($this->_sections['condiments']);
$this->_sections['condiments']['name'] = 'condiments';
$this->_sections['condiments']['start'] = (int)0;
$this->_sections['condiments']['loop'] = is_array($_loop=count($this->_tpl_vars['CONDIMENTS_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['condiments']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['condiments']['show'] = true;
$this->_sections['condiments']['max'] = $this->_sections['condiments']['loop'];
if ($this->_sections['condiments']['start'] < 0)
    $this->_sections['condiments']['start'] = max($this->_sections['condiments']['step'] > 0 ? 0 : -1, $this->_sections['condiments']['loop'] + $this->_sections['condiments']['start']);
else
    $this->_sections['condiments']['start'] = min($this->_sections['condiments']['start'], $this->_sections['condiments']['step'] > 0 ? $this->_sections['condiments']['loop'] : $this->_sections['condiments']['loop']-1);
if ($this->_sections['condiments']['show']) {
    $this->_sections['condiments']['total'] = min(ceil(($this->_sections['condiments']['step'] > 0 ? $this->_sections['condiments']['loop'] - $this->_sections['condiments']['start'] : $this->_sections['condiments']['start']+1)/abs($this->_sections['condiments']['step'])), $this->_sections['condiments']['max']);
    if ($this->_sections['condiments']['total'] == 0)
        $this->_sections['condiments']['show'] = false;
} else
    $this->_sections['condiments']['total'] = 0;
if ($this->_sections['condiments']['show']):

            for ($this->_sections['condiments']['index'] = $this->_sections['condiments']['start'], $this->_sections['condiments']['iteration'] = 1;
                 $this->_sections['condiments']['iteration'] <= $this->_sections['condiments']['total'];
                 $this->_sections['condiments']['index'] += $this->_sections['condiments']['step'], $this->_sections['condiments']['iteration']++):
$this->_sections['condiments']['rownum'] = $this->_sections['condiments']['iteration'];
$this->_sections['condiments']['index_prev'] = $this->_sections['condiments']['index'] - $this->_sections['condiments']['step'];
$this->_sections['condiments']['index_next'] = $this->_sections['condiments']['index'] + $this->_sections['condiments']['step'];
$this->_sections['condiments']['first']      = ($this->_sections['condiments']['iteration'] == 1);
$this->_sections['condiments']['last']       = ($this->_sections['condiments']['iteration'] == $this->_sections['condiments']['total']);
?>
           
             <li><span class="multi-left">
              <input 
              
              data-id = "<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['id']; ?>
"
              data-item_price = "<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['item_price']; ?>
"
              data-item_name = "<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['item_name']; ?>
"
               data-image = "<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['item_image']; ?>
"
               data-category = "CONDIMENTS"
                <?php if (((is_array($_tmp=$this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['cond_itemid']) : in_array($_tmp, $this->_tpl_vars['cond_itemid'])) == 1): ?>
                checked="checked" 
                <?php endif; ?>
              
              type="checkbox" id="5-<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['id']; ?>
" class="input-checkbox" name="" value="<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['item_name']; ?>
">
              <label for="5-<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['id']; ?>
" class="multisel-ckeck"></label>
              </span> <span class="radio-text"><?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['item_name']; ?>
</span> 
              
                 <?php if ($this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['options_id'] != 0): ?>
                 <select>
                  <?php unset($this->_sections['condiments_ops']);
$this->_sections['condiments_ops']['name'] = 'condiments_ops';
$this->_sections['condiments_ops']['start'] = (int)0;
$this->_sections['condiments_ops']['loop'] = is_array($_loop=count($this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['options_id'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['condiments_ops']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['condiments_ops']['show'] = true;
$this->_sections['condiments_ops']['max'] = $this->_sections['condiments_ops']['loop'];
if ($this->_sections['condiments_ops']['start'] < 0)
    $this->_sections['condiments_ops']['start'] = max($this->_sections['condiments_ops']['step'] > 0 ? 0 : -1, $this->_sections['condiments_ops']['loop'] + $this->_sections['condiments_ops']['start']);
else
    $this->_sections['condiments_ops']['start'] = min($this->_sections['condiments_ops']['start'], $this->_sections['condiments_ops']['step'] > 0 ? $this->_sections['condiments_ops']['loop'] : $this->_sections['condiments_ops']['loop']-1);
if ($this->_sections['condiments_ops']['show']) {
    $this->_sections['condiments_ops']['total'] = min(ceil(($this->_sections['condiments_ops']['step'] > 0 ? $this->_sections['condiments_ops']['loop'] - $this->_sections['condiments_ops']['start'] : $this->_sections['condiments_ops']['start']+1)/abs($this->_sections['condiments_ops']['step'])), $this->_sections['condiments_ops']['max']);
    if ($this->_sections['condiments_ops']['total'] == 0)
        $this->_sections['condiments_ops']['show'] = false;
} else
    $this->_sections['condiments_ops']['total'] = 0;
if ($this->_sections['condiments_ops']['show']):

            for ($this->_sections['condiments_ops']['index'] = $this->_sections['condiments_ops']['start'], $this->_sections['condiments_ops']['iteration'] = 1;
                 $this->_sections['condiments_ops']['iteration'] <= $this->_sections['condiments_ops']['total'];
                 $this->_sections['condiments_ops']['index'] += $this->_sections['condiments_ops']['step'], $this->_sections['condiments_ops']['iteration']++):
$this->_sections['condiments_ops']['rownum'] = $this->_sections['condiments_ops']['iteration'];
$this->_sections['condiments_ops']['index_prev'] = $this->_sections['condiments_ops']['index'] - $this->_sections['condiments_ops']['step'];
$this->_sections['condiments_ops']['index_next'] = $this->_sections['condiments_ops']['index'] + $this->_sections['condiments_ops']['step'];
$this->_sections['condiments_ops']['first']      = ($this->_sections['condiments_ops']['iteration'] == 1);
$this->_sections['condiments_ops']['last']       = ($this->_sections['condiments_ops']['iteration'] == $this->_sections['condiments_ops']['total']);
?>
                  <option 
                  data-id="<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['options_id'][$this->_sections['condiments_ops']['index']]['id']; ?>
" 
                  data-unit="<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['options_id'][$this->_sections['condiments_ops']['index']]['option_unit']; ?>
" 
                  data-mult="<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['options_id'][$this->_sections['condiments_ops']['index']]['price_mult']; ?>
" 
                  data-name="<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['options_id'][$this->_sections['condiments_ops']['index']]['option_name']; ?>
" 
                  value="<?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['options_id'][$this->_sections['condiments_ops']['index']]['option_name']; ?>
"><?php echo $this->_tpl_vars['CONDIMENTS_DATA'][$this->_sections['condiments']['index']]['options_id'][$this->_sections['condiments_ops']['index']]['option_name']; ?>
</option>
                  <?php endfor; endif; ?>
                 </select>
                 <?php endif; ?>
              
              </li>
           <?php endfor; endif; ?>
            
          </ul>
        </div>
        <?php endif; ?>
        
      </div>
      <div class="button"> <a class="save gallery_save" href="#">SAVE</a> <a class="cancel" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich">CANCEL</a> </div>
    </div>
  </div>