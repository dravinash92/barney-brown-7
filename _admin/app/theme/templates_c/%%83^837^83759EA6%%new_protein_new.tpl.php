<?php /* Smarty version 2.6.25, created on 2019-11-20 04:50:08
         compiled from new_protein_new.tpl */ ?>
<div class="container">
  <div class="condiment">
    <h1>NEW PROTEIN</h1>

    <form name="newprotein" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/addCategoryItems" enctype="multipart/form-data">


      <div class="edit-item--input">
        <label>Name of Protein</label>
        <input type="text" name="item_name" value="" placeholder="Prociutto">
      </div>

      <div class="edit-item--input">
        <label>Price</label>
        <input type="text" name="item_price" value="" placeholder="Price">
      </div>
      <div class="texa-prem_wrap">
        <div class="texa-prem forms">
          <label>Taxable</label>
          <!--   <input type="checkbox" value="1" name="premium" class="price-menu"  > -->
          <label for="cbk1">
            <input type="checkbox" id="cbk1" value="1" name="taxable">
            <span class="cbx">
              <svg width="12px" height="11px" viewBox="0 0 12 11">
                <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
              </svg>
            </span>

          </label>

        </div> 
          <input type="hidden" value="2" name="category_id">
        <div class="texa-prem forms">
          <label>Premium</label>
          <label for="cbk2">
            <input type="checkbox" id="cbk2" value="1" name="premium">
            <span class="cbx">
              <svg width="12px" height="11px" viewBox="0 0 12 11">
                <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
              </svg>
            </span>

          </label>
        </div>
      </div>

      <?php $_from = $this->_tpl_vars['option_data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>

      <div class="forms file-choose-pro">
        <label for="cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
">
          <input type="checkbox" id="cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" name="select_sandwich_option[]" value="<?php echo $this->_tpl_vars['b']['id']; ?>
"  onclick="showUploadImage(this)" >
          <span class="cbx">
            <svg width="12px" height="11px" viewBox="0 0 12 11">
              <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
            </svg>
          </span>
        </label>
        <label for="cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" class="multisel-ckeck"><?php echo $this->_tpl_vars['b']['option_display_name']; ?>
</label>
      </div>

      <div class="slect-image--wrap" id="upload-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" style="display: none;">
        <div>
          <label>Long Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="lImg-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" name="lImg-<?php echo $this->_tpl_vars['b']['id']; ?>
" />
            </label>
            <span id="lImgName-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
">No file selected</span>
          </p>
        </div>

        <div>

          <label>Round Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="rImg-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" name="rImg-<?php echo $this->_tpl_vars['b']['id']; ?>
"/>
            </label>
            <span id="rImgName-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
">No file selected</span>
          </p>
        </div>
        <div>
          <label>Trapezoid Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="tImg-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" name="tImg-<?php echo $this->_tpl_vars['b']['id']; ?>
"/>
            </label>
            <span id="tImgName-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
">No file selected</span>
          </p>
        </div>
      </div>
      <?php endforeach; endif; unset($_from); ?>  


      <div class="button">
       <input type="submit" class="save save_custom_item" value="SAVE">
       <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/custom" class="cancel">CANCEL</a>
     </div>
   </form>

 </div>
</div>