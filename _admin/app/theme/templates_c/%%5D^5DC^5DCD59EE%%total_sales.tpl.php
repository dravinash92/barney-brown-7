<?php /* Smarty version 2.6.25, created on 2020-09-30 00:22:48
         compiled from total_sales.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'total_sales.tpl', 158, false),array('modifier', 'string_format', 'total_sales.tpl', 225, false),)), $this); ?>
<div class="container">
    <div class="item-report">
      <h1>TOTAL SALES</h1>
      <div class="filter_wrap">
      <form name="totals_sales_report_filter">
      <div class="store-location">
         <label>Name of Stores</label>
         <select name="pickup_store">
           <option value="">Select Store</option>
			<?php $_from = $this->_tpl_vars['pickupstores']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<option value="<?php echo $this->_tpl_vars['v']->id; ?>
"><?php echo $this->_tpl_vars['v']->store_name; ?>
, <?php echo $this->_tpl_vars['v']->address1; ?>
, <?php echo $this->_tpl_vars['v']->address2; ?>
, <?php echo $this->_tpl_vars['v']->zip; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
         </select>
        <ul>
          <li><label>Order Type</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="order_type[]" value="true">
            <label for="checkbox-2-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Delivery</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-5" class="input-checkbox" name="order_type[]" value="false">
            <label for="checkbox-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Pick-Up</span>
          </li>
        </ul>
        <ul>
          <li><label>Order Status</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-3-1" class="input-checkbox" name="order_status[]" value="0,0">
            <label for="checkbox-3-1" class="multisel-ckeck"></label>
            </span> <span class="radio-text">New</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-2" class="input-checkbox" name="order_status[]" value="1,5">
            <label for="checkbox-3-2" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Processed</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-3" class="input-checkbox" name="order_status[]" value="2,6">
            <label for="checkbox-3-3" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Ready</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-4" class="input-checkbox" name="order_status[]" value="3,7">
            <label for="checkbox-3-4" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Out</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-5" class="input-checkbox" name="order_status[]" value="4,8">
            <label for="checkbox-3-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Received</span>
          </li>
        </ul>
      </div>
      
      <div class="date-time-settings">
         <label>Date/Time Settings</label>
         <ul>
           <li>
            <div class="radio-left">
             <div class="radio-section">
                <input type="radio" value="order_date"  class="input-radio" name="order_date_filter" id="1">
                <label for="1"></label>
              </div>
              <span class="radio-text">Order Placement</span>
             </div>
             <div class="radio-right">
              <div class="radio-section">
                  <input type="radio" value="date" checked class="input-radio" name="order_date_filter" id="2">
                <label for="2"></label>
                </div>
                <span class="radio-text">Order Fulfillment</span>
              </div>
           </li>
           <li>
             <label>From</label>
          <input type="text" class="expiration" placeholder="Select date" name="from_date" value="<?php echo $this->_tpl_vars['from_date']; ?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/calendar.png"></a>
            <select name="from_time" name="from_time">
				<?php echo $this->_tpl_vars['fromTimes']; ?>

             </select>
           </li>
            <li>
             <label>To</label>
          <input type="text" class="expiration" placeholder="Select date" name="to_date" value="<?php echo $this->_tpl_vars['to_date']; ?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/calendar.png"></a>
            <select class="to_time" name="to_time">
				<?php echo $this->_tpl_vars['toTimes']; ?>
				
             </select>
           </li>
         </ul>
      </div>
      
      <div class="type-purchase-report">
        <ul>
          <li><label>Type of Purchase</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-4-5" class="input-checkbox" name="type_purchase[]" value="0">
            <label for="checkbox-4-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Website</span> </li>
            
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-8" class="input-checkbox" name="type_purchase[]" value="1">
            <label for="checkbox-3-8" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Phone Order</span>
          </li>
          
        </ul>
        
      </div>
      
      <div class="type-purchase-discount">
        <ul>
          <li><label>Discounts</label></li>
          <li>
             <select name="discount">
              <option value="">Select Discount</option>
				<?php $_from = $this->_tpl_vars['discounts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<option value="<?php echo $this->_tpl_vars['v']->id; ?>
"><?php echo $this->_tpl_vars['v']->name; ?>
, <?php echo $this->_tpl_vars['v']->code; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
             </select>
           </li>
           <li><label>Refund Types</label></li>
          <li>
             <select name="refund_types">
              <option value="">Select Refund Types</option>
              <option value="">Refund Types</option>
             </select>
           </li>
           
          <li><label>System User</label></li>
          <li>
             <select name="system_user">
              <option value="">Select User</option>
				<?php $_from = $this->_tpl_vars['systemusers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
					<option value="<?php echo $this->_tpl_vars['v']->uid; ?>
"><?php echo $this->_tpl_vars['v']->first_name; ?>
 <?php echo $this->_tpl_vars['v']->last_name; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
             </select>
           </li>
           
           <li><label>Search</label></li>
           <li>
             <input type="text" name="search_text" >
             <a href="javascript:void(0)" class="do_total_sales_filter"><img width="26" height="26" alt="" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/link.png"></a>
          </li>          
        </ul>
         
      </div>
      </form>
		</div>
		<div class="reports_table">
			<?php if (count($this->_tpl_vars['orders']) > 0): ?>
			<?php 	
				$items_total= $tax_total = $sub_total  = $delivery_fee_total = $tip_total = $total = $dscnt_total = $gft_total = $refund = $paid = 0;
			 ?>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody>
			<tr>
			  <th>ORDER #</th>
			  <th>DATE/TIME</th>
			  <th>CUSTOMER NAME</th>
			  <!-- <th>ITEMS</th> -->
			  <th>SUBTOT</th>
			  <th>TAX</th>
        <th>DELIVERY FEE</th>
        <th>TIP</th> 
			  <th>TOTAL VALUE</th>
			  <th>DSCNT</th>
			  <th>GFT CRD</th>
			  <th>REFUND</th>
			  <th>TOTAL PAID</th>
			</tr>
			
			<?php $_from = $this->_tpl_vars['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<?php $this->assign('item_total', $this->_tpl_vars['v']->item_total-$this->_tpl_vars['v']->total_tax); ?>				
				<?php $this->assign('tax', $this->_tpl_vars['v']->total_tax); ?>
				<?php $this->assign('sub', $this->_tpl_vars['v']->sub_total); ?>
        <?php $this->assign('delivery_fee', $this->_tpl_vars['v']->delivery_fee); ?>
				<?php $this->assign('tip', $this->_tpl_vars['v']->tip); ?>
				<?php $this->assign('calc_total', $this->_tpl_vars['v']->calc_total); ?>
				<?php $this->assign('off_amount', $this->_tpl_vars['v']->off_amount); ?>
				<?php $this->assign('total', $this->_tpl_vars['v']->total); ?>
				<?php 
					$items_total += $this->get_template_vars('item_total');
					$tax_total += $this->get_template_vars('tax');
					$sub_total += $this->get_template_vars('sub');
          $delivery_fee_total += $this->get_template_vars('delivery_fee');
					$tip_total += $this->get_template_vars('tip');
					$calc_total += $this->get_template_vars('calc_total');
					$dscnt_total += $this->get_template_vars('off_amount');
					$paid += $this->get_template_vars('total');
				 ?>			
			<?php endforeach; endif; unset($_from); ?>
			
			<tr class="subheader">
			  <td colspan="3">TOTAL</td>
			  <!-- <td>$<?php echo sprintf('%0.2f',$items_total) ?></td> -->
			  <td>$<?php echo sprintf('%0.2f',$sub_total) ?></td>
			  <td>$<?php echo sprintf('%0.2f',$tax_total) ?></td>
        <td>$<?php echo sprintf('%0.2f',$delivery_fee_total) ?></td>
			  <td>$<?php echo sprintf('%0.2f',$tip_total) ?></td>
			  <td>$<?php echo sprintf('%0.2f',$calc_total+$tax_total) ?></td>
			  <td>-$<?php echo sprintf('%0.2f',$dscnt_total) ?></td>
			  <td>-$0.00</td>
			  <td>-$0.00</td>
			  <td>$<?php echo sprintf('%0.2f',$calc_total+$tax_total-$dscnt_total) ?></td>
			</tr>			
			
			
			<?php $_from = $this->_tpl_vars['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<?php if (!(!(1 & $this->_tpl_vars['k']))): ?> 
				<tr class="">
				<?php else: ?>	
				<tr class="even">
				<?php endif; ?>	
				  <td><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
reports/showOrderDetails/<?php echo $this->_tpl_vars['v']->order_id; ?>
"><?php echo $this->_tpl_vars['v']->order_number; ?>
</a></td>
				  <td><?php echo $this->_tpl_vars['v']->o_date; ?>
</td>
				  <td><?php echo $this->_tpl_vars['v']->first_name; ?>
 <?php echo $this->_tpl_vars['v']->last_name; ?>
</td>
				  <!-- <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->amount_wt)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td> -->
				  <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->sub_total)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				  <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->total_tax)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
          <td>$<?php echo $this->_tpl_vars['v']->delivery_fee; ?>
</td>
				  <td>$<?php echo $this->_tpl_vars['v']->tip; ?>
</td>
				  <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->calc_total+$this->_tpl_vars['v']->total_tax)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				  <td>-$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->off_amount)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				  <td>-$0.00</td>
				  <td>-$0.00</td>
				  <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->total)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				</tr>				
			<?php endforeach; endif; unset($_from); ?>					
			
			</tbody>
		</table>	
		<?php else: ?>
			<h3>No orders found.</h3>	
		<?php endif; ?>	
		</div>	
		
    </div>
  </div>