<?php /* Smarty version 2.6.25, created on 2020-09-15 06:35:30
         compiled from sandwich-bread.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'sandwich-bread.tpl', 10, false),)), $this); ?>
  <h2>Choose Your Bread</h2>
              <h3>CHOOSE FROM OUR SELECTION OF NEW YORK'S AND NEW JERSEY'S FINEST.</h3>
              <div class="main-category-list">
              
         
              
                <ul>             
              

                <?php unset($this->_sections['options']);
$this->_sections['options']['name'] = 'options';
$this->_sections['options']['start'] = (int)0;
$this->_sections['options']['loop'] = is_array($_loop=count($this->_tpl_vars['BREAD_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['options']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['options']['show'] = true;
$this->_sections['options']['max'] = $this->_sections['options']['loop'];
if ($this->_sections['options']['start'] < 0)
    $this->_sections['options']['start'] = max($this->_sections['options']['step'] > 0 ? 0 : -1, $this->_sections['options']['loop'] + $this->_sections['options']['start']);
else
    $this->_sections['options']['start'] = min($this->_sections['options']['start'], $this->_sections['options']['step'] > 0 ? $this->_sections['options']['loop'] : $this->_sections['options']['loop']-1);
if ($this->_sections['options']['show']) {
    $this->_sections['options']['total'] = min(ceil(($this->_sections['options']['step'] > 0 ? $this->_sections['options']['loop'] - $this->_sections['options']['start'] : $this->_sections['options']['start']+1)/abs($this->_sections['options']['step'])), $this->_sections['options']['max']);
    if ($this->_sections['options']['total'] == 0)
        $this->_sections['options']['show'] = false;
} else
    $this->_sections['options']['total'] = 0;
if ($this->_sections['options']['show']):

            for ($this->_sections['options']['index'] = $this->_sections['options']['start'], $this->_sections['options']['iteration'] = 1;
                 $this->_sections['options']['iteration'] <= $this->_sections['options']['total'];
                 $this->_sections['options']['index'] += $this->_sections['options']['step'], $this->_sections['options']['iteration']++):
$this->_sections['options']['rownum'] = $this->_sections['options']['iteration'];
$this->_sections['options']['index_prev'] = $this->_sections['options']['index'] - $this->_sections['options']['step'];
$this->_sections['options']['index_next'] = $this->_sections['options']['index'] + $this->_sections['options']['step'];
$this->_sections['options']['first']      = ($this->_sections['options']['iteration'] == 1);
$this->_sections['options']['last']       = ($this->_sections['options']['iteration'] == $this->_sections['options']['total']);
?>
                
                  <li>
                    <input data-shape="<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['options']['index']]['bread_shape']; ?>
" data-bread_type="<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['options']['index']]['bread_type']; ?>
" data-item_image_sliced="<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['options']['index']]['item_image_sliced']; ?>
" data-type="replace" data-id="<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['options']['index']]['id']; ?>
" data-itemname="<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['options']['index']]['item_name']; ?>
" data-price="<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['options']['index']]['item_price']; ?>
" data-image="<?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['options']['index']]['item_image']; ?>
" type="radio" name="radiog_lite" id="radio<?php echo $this->_sections['options']['index']+1; ?>
" class="css-checkbox" />
                    <label for="radio<?php echo $this->_sections['options']['index']+1; ?>
" class="css-label"><?php echo $this->_tpl_vars['BREAD_DATA'][$this->_sections['options']['index']]['item_name']; ?>
</label>
                  </li>
                <?php endfor; endif; ?>                  
                </ul>
              </div>