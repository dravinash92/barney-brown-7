<?php /* Smarty version 2.6.25, created on 2020-09-28 03:32:41
         compiled from add_to_cart.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'add_to_cart.tpl', 79, false),array('modifier', 'is_array', 'add_to_cart.tpl', 136, false),array('modifier', 'in_array', 'add_to_cart.tpl', 137, false),array('modifier', 'json_encode', 'add_to_cart.tpl', 184, false),array('modifier', 'substr', 'add_to_cart.tpl', 228, false),array('modifier', 'date_format', 'add_to_cart.tpl', 446, false),)), $this); ?>
<div class="container">
    <div class="place-order">
       <h1>PLACE ORDER</h1>
       
       <div class="place-order-coustomer-info">
       		<div class="address-details">
           <h3>CUSTOMER INFO</h3>
           <p>Name: <span><?php echo $this->_tpl_vars['user']->first_name; ?>
 <?php echo $this->_tpl_vars['user']->last_name; ?>
</span> <input type="text" class="text-box edit_customer_info" name="first_name" placeholder="First Name" style="display:none;" > <input type="text" class="text-box edit_customer_info" name="last_name" placeholder="Last Name"style="display:none;"><br>
              Email: <span><?php echo $this->_tpl_vars['user']->username; ?>
</span>
               <input type="text" class="text-box edit_customer_info" name="username"  value="" style="display:none;"><br/>
              Phone: <span><?php echo $this->_tpl_vars['user']->phone; ?>
</span>
              <input type="text" class="text-box edit_customer_info" name="phone" placeholder="000-000-0000" value="<?php echo $this->_tpl_vars['user']->phone; ?>
" style="display:none;"><br/>
              Total Orders: <?php echo $this->_tpl_vars['user']->total_orders; ?>
<br>
              Total Sales: $<?php echo $this->_tpl_vars['user']->total_sum; ?>
</p>
           <a href="javascript:void(0)" class="edit-info edit_info" >Edit Info</a>
           <a href="javascript:void(0)" class="edit-info save_info" style="display:none;">Save Info</a>
           <input type="hidden" name="user_id" class="user_id" value="<?php echo $this->_tpl_vars['user']->uid; ?>
" />
         </div>
         
         <div class="customer-notes">
         	<h3>CUSTOMER NOTES</h3>
			<div class="customer-notes-list">
				<?php $_from = $this->_tpl_vars['notes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<div class="note">
					<p><span class="note_date"><?php echo $this->_tpl_vars['v']->date; ?>
</span><span class="address"><?php echo $this->_tpl_vars['v']->notes; ?>
</span></p>
					<br/>
					<a href="javascript:void(0)" data-id="<?php echo $this->_tpl_vars['v']->id; ?>
" class="remove remove_customer_note">Remove</a>
         		</div>				
				<?php endforeach; endif; unset($_from); ?>
         	</div>
         	<div class="add-customer-note" style="display:none;">
				<textarea col="20" rows="3" placeholder="Add new note"></textarea>	
				<br/>
				<a href="javascript:void(0)" class="btn-brown add_customer_note">Add</a>   <a href="javascript:void(0)" class="btn-brown cancel_customer_note">Cancel</a>
         	</div>	
            <a href="javascript:void(0)" class="add-new-note">add new note</a>
         </div>
         
         <div class="date-time-order">
         		<h3>DATE/TIME OF ORDER</h3>
            <div class="radio-holder">
            	<input type="radio" class="css-checkbox now_or_specific" id="radio1" name="radio_time" value="now">
              <label class="css-label time_type_label" for="radio1">Now</label>
              <input type="radio" class="css-checkbox now_or_specific" id="radio2" name="radio_time" value="specific">
              <label class="css-label time_type_label" for="radio2">Specific Date/Time</label>
            </div>
            
            <div class="text-box-holder">
            	<label>Date</label>
              <input type="text" class="text-box" name="">
              <a class="date-icon" href="#"></a>
            </div>
            <div class="text-box-holder">
            	<label>Time</label>
				<?php echo $this->_tpl_vars['times']; ?>

            </div>
         </div>
         
         <div class="pickup-deliver-order">
         	<div class="radio-holder">
				<input type="radio" class="css-checkbox address_type" id="radio3" name="radio_address_type" value="delivery">
				<label class="css-label address_type_label" for="radio3">DELIVERY</label>
				<input type="radio" class="css-checkbox address_type" id="radio4" name="radio_address_type" value="pickup">
				<label class="css-label address_type_label" for="radio4">PICK-UP</label>
            </div>
			<div class="text-box-holder address-container">
           
			</div>
          
         </div>
         
       </div>
       
       <h2>SANDWICHES</h2>
       <ul class="new-order-sandwichilist">
         <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['user']->uid; ?>
" class="big-buttons-sandwich"><span>+</span>CREATE A SANDWICH</a></li>
         <li><a href="javascript:void(0)" class="big-buttons-sandwich choose-from-gallery"><span>+</span>SANDWICH GALLERY</a></li>
         
         <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['GALLARY_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
            
            <?php $this->assign('sandwich_id', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']); ?>
            <?php $this->assign('sandwich_uid', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']); ?>
            <?php $this->assign('user_uid', $this->_tpl_vars['user']->uid); ?>
            <?php $this->assign('menu_is_active', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_is_active']); ?>
             
            <?php 
				
			if((in_array( $this->get_template_vars('sandwich_id') , $this->get_template_vars('temp_sandwiches') ) || $this->get_template_vars('sandwich_uid') == $this->get_template_vars('user_uid') ) && $this->get_template_vars('menu_is_active') == 1 ){
				
             ?> 
             
              <?php $this->assign('prot_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
      
      
                  <?php if (is_array($this->_supers['session']['orders']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'], $this->_supers['session']['orders']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
              
              
                <li <?php if ($this->_tpl_vars['items_id'] == 1): ?> rel="ADDED TO CART" <?php else: ?> rel="ADD TO CART" <?php endif; ?> id="toastID_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-formatdate="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]; ?>
"  data-date="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-flag="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['flag']; ?>
" data-name="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
"  data-sandwich_desc="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
" data-sandwich_desc_id ="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_desc_id']; ?>
"  data-menuadds="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_add_count']; ?>
" data-userid ="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
" data-toast="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_toast']; ?>
" data-formatdate="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-bread="<?php echo $this->_tpl_vars['bread_name']; ?>
"  data-date="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['current_price']; ?>
" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-type="user_sandwich" data-likeid="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['like_id']; ?>
" data-likecount="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['like_count']; ?>
"> 
                <input type="hidden" name="image" id="sandImg_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png">
                
                <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['by_admin'] == 1): ?> 
                  <?php $this->assign('url', $this->_tpl_vars['SITE_URL']); ?>
                <?php else: ?>
                  <?php $this->assign('url', $this->_tpl_vars['CMS_URL']); ?>
                <?php endif; ?>
                
                
                <span class="sandwichilist-holder">
					        <img data-href="<?php echo $this->_tpl_vars['url']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" width="156" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
/thumbnails/sandwich_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png" alt="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
">
					        <h3><?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
</h3>
				          <h4>$<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['current_price']; ?>
</h4>
					        
					        <a href="javascript:void(0)" data-type="user_sandwich" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" class="add-cart view-sandwich" >ADD TO CART</a>
				        </span>
                
                </li>
                
            <?php 
			}
             ?>    
                
            <?php endfor; endif; ?>
         
       </ul>
       
		<?php $_from = $this->_tpl_vars['stdCategoryItems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['outer'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['outer']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['category']):
        $this->_foreach['outer']['iteration']++;
?>
			<h2><?php echo $this->_tpl_vars['category']->standard_cat_name; ?>
</h2> 
			 <ul class="sides-list">      
			<?php $_from = $this->_tpl_vars['category']->categoryProducts; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
				<li>
				   <h3><?php echo $this->_tpl_vars['item']->product_name; ?>
________________</h3>
				   <h4>$<?php echo $this->_tpl_vars['item']->product_price; ?>
</h4>
				   <span class="arrow-holder">
					 <a href="#" data-event="noupdate" class="arrow-left"></a>
					 <input name="quantity" type="text" class="qty" value="1">
					 <a href="#" data-event="noupdate" class="arrow-right"></a>
				   </span>
				   <!--<a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
neworder/cartConfirm/" data-type="product" data-id="<?php echo $this->_tpl_vars['item']->id; ?>
" class="add-cart">ADD TO CART</a>-->
				   <a href="javascript:void(0)" data-type="product" data-id="<?php echo $this->_tpl_vars['item']->id; ?>
" class="add-cart <?php if ($this->_tpl_vars['item']->allow_spcl_instruction == 1 || $this->_tpl_vars['item']->add_modifier == 1): ?> salad-listing <?php else: ?> add-to-cart <?php endif; ?>" data-sandwich="product" data-sandwich_id="<?php echo $this->_tpl_vars['item']->id; ?>
" data-uid="<?php echo $this->_tpl_vars['uid']; ?>
" data-product_name="<?php echo $this->_tpl_vars['item']->product_name; ?>
" data-description="<?php echo $this->_tpl_vars['item']->description; ?>
" data-product_image="<?php echo $this->_tpl_vars['item']->product_image; ?>
" data-product_price="<?php echo $this->_tpl_vars['item']->product_price; ?>
" data-standard_category_id="<?php echo $this->_tpl_vars['item']->standard_category_id; ?>
" data-uid=<?php echo $this->_tpl_vars['uid']; ?>
 data-product="product" data-spcl_instr="<?php echo $this->_tpl_vars['item']->allow_spcl_instruction; ?>
" data-add_modifier="<?php echo $this->_tpl_vars['item']->add_modifier; ?>
" data-modifier_desc="<?php echo $this->_tpl_vars['item']->modifier_desc; ?>
" data-modifier_isoptional="<?php echo $this->_tpl_vars['item']->modifier_isoptional; ?>
" data-modifier_is_single="<?php echo $this->_tpl_vars['item']->modifier_is_single; ?>
" data-modifier_options='<?php echo json_encode($this->_tpl_vars['modifier_options'][$this->_tpl_vars['key']]); ?>
'>ADD TO CART</a>
				</li> 
			<?php endforeach; endif; unset($_from); ?>
			</ul>
		<?php endforeach; endif; unset($_from); ?>
       
		<?php $_from = $this->_tpl_vars['cateringdataList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j'] => $this->_tpl_vars['list']):
?>
			   
			<?php if (count($this->_tpl_vars['list']['data']) > 0): ?>
			<h2><?php echo $this->_tpl_vars['list']['cat_name']; ?>
</h2> 
			 <ul class="sides-list">  
			<?php $_from = $this->_tpl_vars['list']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['data']):
?>
				<li>
				   <h3><?php echo $this->_tpl_vars['data']->product_name; ?>
________________</h3>
				   <h4>$<?php echo $this->_tpl_vars['data']->product_price; ?>
</h4>
				   <span class="arrow-holder">
					 <a href="#" data-event="noupdate" class="arrow-left"></a>
					 <input name="quantity" type="text" class="qty" value="1">
					 <a href="#" data-event="noupdate" class="arrow-right"></a>
				   </span>
				   <!--<a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
neworder/cartConfirm/" data-type="product" data-id="<?php echo $this->_tpl_vars['item']->id; ?>
" class="add-cart">ADD TO CART</a>-->
				   <a href="javascript:void(0)" data-type="product" data-id="<?php echo $this->_tpl_vars['data']->id; ?>
" class="add-cart <?php if ($this->_tpl_vars['data']->allow_spcl_instruction == 1 || $this->_tpl_vars['data']->add_modifier == 1): ?> salad-listing <?php else: ?> add-to-cart <?php endif; ?>" data-sandwich="product" data-sandwich_id="<?php echo $this->_tpl_vars['data']->id; ?>
" data-uid="<?php echo $this->_tpl_vars['uid']; ?>
" data-product_name="<?php echo $this->_tpl_vars['data']->product_name; ?>
" data-description="<?php echo $this->_tpl_vars['data']->description; ?>
" data-product_image="<?php echo $this->_tpl_vars['data']->product_image; ?>
" data-product_price="<?php echo $this->_tpl_vars['data']->product_price; ?>
" data-standard_category_id="<?php echo $this->_tpl_vars['data']->standard_category_id; ?>
" data-uid=<?php echo $this->_tpl_vars['uid']; ?>
 data-product="product" data-spcl_instr="<?php echo $this->_tpl_vars['data']->allow_spcl_instruction; ?>
" data-add_modifier="<?php echo $this->_tpl_vars['data']->add_modifier; ?>
" data-modifier_desc="<?php echo $this->_tpl_vars['data']->modifier_desc; ?>
" data-modifier_isoptional="<?php echo $this->_tpl_vars['data']->modifier_isoptional; ?>
" data-modifier_is_single="<?php echo $this->_tpl_vars['data']->modifier_is_single; ?>
" data-modifier_options='<?php echo $this->_tpl_vars['data']->modifier_options; ?>
'>ADD TO CART</a>
				</li> 
			<?php endforeach; endif; unset($_from); ?>
			</ul>	
			<?php endif; ?>
			
		<?php endforeach; endif; unset($_from); ?>
		
		
		<div class="shopping-cart">
			<?php echo $this->_tpl_vars['shopitems']; ?>

		</div>
       
       <div class="billing-information-holder">
			<h3>BILLING INFORMATION</h3>
       	
			<div class="billing-card-holder">
			  <div class="text-box-holder">
				<select name="" class="billing-select">
					<option value="">Select Card</option>	
					<option value="no">No Charge</option>	
					<option value="0">Add New</option>
					<?php $_from = $this->_tpl_vars['billingInfo']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['myId'] => $this->_tpl_vars['billings']):
?>
						<option value="<?php echo $this->_tpl_vars['billings']->id; ?>
"><?php echo $this->_tpl_vars['billings']->card_type; ?>
 card number ending with <?php echo ((is_array($_tmp=$this->_tpl_vars['billings']->card_number)) ? $this->_run_mod_handler('substr', true, $_tmp, -4) : substr($_tmp, -4)); ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			  </div>
			  <!--<a href="javascript:void(0)" class="edit-bill">add new billing</a>-->
			  <!--<a href="javascript:void(0)" class="edit-bill">edit billing</a>-->
			</div>
			<br/>
			 <a href="javascript:void(0)" style="display:none;" class="change-billing">CHANGE</a>
			<div class="billing-info-show" style="display:none; color:#000;"></div>
			<input type="hidden" name="billing_card" value="" />
          <a href="javascript:void(0)" class="place-order">PLACE ORDER</a>
       </div>
       
    </div>
  </div>

<div class="popup-wrapper" id="add-new-address">
  <div class="add-new-address-inner"> <a href="#" class="close-button"></a>
	<form class="add_address" method="post" >
    <div class="title-holder">
      <h1>ADD NEW ADDRESS</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Recipient Name</p>
        <input name="recipient" type="text" class="text-box-control" placeholder="Enter Recipient Name">
        </span> <span class="text-box-holder">
        <p>Company</p>
        <input name="company" type="text" class="text-box-control" placeholder="Enter Company Name">
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Street Address 1</p>
        <input name="address1" type="text" class="text-box-control" >
        </span> <span class="text-box-holder">
        <p>Street Address 2</p>
        <input name="address2" type="text" class="text-box-control" >
        </span> <span class="text-box-holder1">
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" >
        </span> <span class="text-box-holder">
        <p>Phone Number</p>
        <input name="phone1" type="text" class="text-box-phone" >
        <input name="phone2" type="text" class="text-box-phone" >
        <input name="phone3" type="text" class="text-box-phone margin-none" >
        </span> <span class="text-box-holder1">
        <p>Ext.</p>
        <input name="extn" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <h2>New York, NY</h2>
        <span class="text-box-zip-holder1">
        <p>Zip Code</p>
        <input name="zip" type="text" class="text-box-zip" >
        </span> </span>
        <h3><?php echo $this->_tpl_vars['title_text']; ?>
 currently delivers in Midtown Manhattan from 23rd Street to 59th Street and from 3rd Avenue to 8th Avenue.</h3>
      </li>
      <li> <span class="delivery-instructions">
        <p>Delivery Instructions</p>
        <input name="delivery_instructions" type="text" class="text-box-delivery" >
        </span> </li>
      <li> <a href="javaScript:void(0)" class="add-address save_address">ADD ADDRESS</a> </li>
    </ul>
    <input type="hidden" name="uid" value="<?php echo $this->_tpl_vars['user']->uid; ?>
" />
    </form>
  </div>
</div>
<div class="popup-wrapper" id="edit-address">
  <div class="add-new-address-inner">
	
  </div>
</div>

<div class="popup-wrapper" id="sandwich-gallery">
  <div class="add-new-address-inner">
	  <a href="#" class="close-button"></a>
	<div class="row">
                  
         <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['GALLARY_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
            
            <?php $this->assign('sandwich_id', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']); ?>
            
              <?php $this->assign('prot_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
      
      
                  <?php if (is_array($this->_supers['session']['orders']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'], $this->_supers['session']['orders']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
                   
                   
                     
                <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['by_admin'] == 1): ?> 
                  <?php $this->assign('url', $this->_tpl_vars['SITE_URL']); ?>
                <?php else: ?>
                  <?php $this->assign('url', $this->_tpl_vars['CMS_URL']); ?>
                <?php endif; ?>
              
              
                <div class="width33" <?php if ($this->_tpl_vars['items_id'] == 1): ?> rel="ADDED TO CART" <?php else: ?> rel="ADD TO CART" <?php endif; ?> data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-formatdate="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]; ?>
"  data-date="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
"   > 
                
                
                <span class="sandwichilist-holder">
					<img data-href="<?php echo $this->_tpl_vars['url']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" width="156" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png" alt="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
">
					<h3><?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
</h3>
					<h4>$<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
</h4>
					<!--<a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
neworder/cartConfirm/" class="add-to-cart">ADD TO CART</a>-->
					<a href="javascript:void(0)" data-type="user_sandwich" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" class="button add-to-cart" >SELECT</a>
				</span>
                
                </div>
                
                
            <?php endfor; endif; ?>
         
       </div>
  </div>
</div>

<!--credit card popup-->
<!--Popup Start-->
<div class="popup-wrapper" id="credit-card-details">
  <div class="add-new-address-inner"> <a href="javascript:void(0)" class="close-button">Close</a>
    <div class="title-holder">
      <h1>ADD NEW CREDIT CARD</h1>
    </div>
     <form id="billForm">
    <ul class="from-holder">
   
      <li> <!-- <span class="text-box-holder">
        <p>Credit Card Nick Name</p>
        <input name="cardNickname" type="text" class="text-box-control" placeholder="">
        </span>  --><span class="text-box-holder">
        <p>Card Number</p>
        <input name="cardNo" type="text" class="text-box-control" placeholder="">
        </span> <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="cardCvv" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
      <p>Credit Card</p>
        <span class="credit-card">
        <select name="cardType"  >
          
           <option value="visa">Visa</option>
          <option value="MasterCard">MasterCard</option>
          <option value="Discover">Discover</option>
          <option value="American Express">American Express</option>
        
        </select>
        </span> <br><br><br>
        <p>Expiration Date</p>
        <span class="month">
        <select name="cardMonth"  >
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="03">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <select name="cardYear">
         
          
          
<?php $this->assign('currentyear', ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y"))); ?>
<?php $this->assign('numyears', 50); ?>
<?php $this->assign('totalyears', $this->_tpl_vars['currentyear']+$this->_tpl_vars['numyears']); ?>
<?php unset($this->_sections['loopyers']);
$this->_sections['loopyers']['name'] = 'loopyers';
$this->_sections['loopyers']['start'] = (int)$this->_tpl_vars['currentyear'];
$this->_sections['loopyers']['loop'] = is_array($_loop=$this->_tpl_vars['totalyears']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['loopyers']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['loopyers']['show'] = true;
$this->_sections['loopyers']['max'] = $this->_sections['loopyers']['loop'];
if ($this->_sections['loopyers']['start'] < 0)
    $this->_sections['loopyers']['start'] = max($this->_sections['loopyers']['step'] > 0 ? 0 : -1, $this->_sections['loopyers']['loop'] + $this->_sections['loopyers']['start']);
else
    $this->_sections['loopyers']['start'] = min($this->_sections['loopyers']['start'], $this->_sections['loopyers']['step'] > 0 ? $this->_sections['loopyers']['loop'] : $this->_sections['loopyers']['loop']-1);
if ($this->_sections['loopyers']['show']) {
    $this->_sections['loopyers']['total'] = min(ceil(($this->_sections['loopyers']['step'] > 0 ? $this->_sections['loopyers']['loop'] - $this->_sections['loopyers']['start'] : $this->_sections['loopyers']['start']+1)/abs($this->_sections['loopyers']['step'])), $this->_sections['loopyers']['max']);
    if ($this->_sections['loopyers']['total'] == 0)
        $this->_sections['loopyers']['show'] = false;
} else
    $this->_sections['loopyers']['total'] = 0;
if ($this->_sections['loopyers']['show']):

            for ($this->_sections['loopyers']['index'] = $this->_sections['loopyers']['start'], $this->_sections['loopyers']['iteration'] = 1;
                 $this->_sections['loopyers']['iteration'] <= $this->_sections['loopyers']['total'];
                 $this->_sections['loopyers']['index'] += $this->_sections['loopyers']['step'], $this->_sections['loopyers']['iteration']++):
$this->_sections['loopyers']['rownum'] = $this->_sections['loopyers']['iteration'];
$this->_sections['loopyers']['index_prev'] = $this->_sections['loopyers']['index'] - $this->_sections['loopyers']['step'];
$this->_sections['loopyers']['index_next'] = $this->_sections['loopyers']['index'] + $this->_sections['loopyers']['step'];
$this->_sections['loopyers']['first']      = ($this->_sections['loopyers']['iteration'] == 1);
$this->_sections['loopyers']['last']       = ($this->_sections['loopyers']['iteration'] == $this->_sections['loopyers']['total']);
?>
<option><?php echo $this->_sections['loopyers']['index']; ?>
</option>
<?php endfor; endif; ?>
         
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="cardZip" type="text" class="text-box-billing-zip" >
        </span> </li>
         <!--<li> <span class="text-box-holder">
        <p>Street Address </p>
        <input name="address1" type="text" class="text-box-control required" required >
        </span> 
	<span class="text-box-holder1"> 
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control required" required>
        </span> </li>-->
      <li> <span class="checkbox-save-bill">
        <input  type="checkbox" id="save_billing"  name="save_billing" value="save_billing"/>
        <label for="save_billing">Save Billing Info <span>(Verisign Encryption)</span></label>
        </span> </li>
      <li> <span class="card-holder-margin">
        <h3>One or two sentences about site security, encryption methods, storage of data meeting Y and Z standards (to let people feel secure about storing their credit card info).</h3>
        <a href="javascript:void(0)" class="credit-card-button">ADD</a> </span> </li>
    </ul>
    </form>
  </div>
  
  <form id="_payment_form" action="paymentpage/" method="post" style="display:none">
  <input type = "hidden" name="_card_type" value="" />
  <input type = "hidden" name="_card_number" value="" />
  <input type = "hidden" name="_card_cvv" value="" />
  <input type = "hidden" name="_card_zip" value="" />
  <input type = "hidden" name="_card_name" value="" />
  <input type = "hidden" name="_expiry_month" value="" />
  <input type = "hidden" name="_expiry_year" value="" />
  </form>
  
</div>
<?php echo '

'; ?>


<!--credit card popup-->