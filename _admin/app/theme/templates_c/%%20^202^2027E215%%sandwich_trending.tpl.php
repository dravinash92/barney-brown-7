<?php /* Smarty version 2.6.25, created on 2018-10-01 06:53:54
         compiled from sandwich_trending.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'sandwich_trending.tpl', 7, false),array('function', 'cycle', 'sandwich_trending.tpl', 21, false),)), $this); ?>
<div class="container">
	<div class="sandwich-gallery">
		<div class="heading">
         <h1>TRENDING SANDWICHES</h1>
         <a class="add_sotd_button">ADD NEW</a>
        </div>
        <?php if (count($this->_tpl_vars['sandwiches']) > 0): ?>
        <table class="listing sandwich-trending">
			<tbody>
				<tr class="static">
				  <th width="10%"> LIVE  </th>		
				  <th width="20%" height="31">Priority</th>				  
				  <th width="25%" height="31">CUSTOM SANDWICHES</th>
				  <th width="15%" height="31">USER</th>
				  <th width="10%">PRICE</th>				  
				  <th width="10%">        </th>	
				  <th width="10%">        </th>					  
				</tr>
				
				<?php $_from = $this->_tpl_vars['sandwiches']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['sandwich']):
?>
				<tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
">
					<td width="10%" align="center"> 
					  <span class="multi-left">
					   <input type="checkbox" name="" class="do-trnd input-checkbox" id="checkbox-<?php echo $this->_tpl_vars['k']; ?>
" data-id="<?php echo $this->_tpl_vars['sandwich']->id; ?>
" <?php if ($this->_tpl_vars['sandwich']->trnd_live == 1): ?> checked <?php endif; ?>>
					   <label class="multisel-ckeck" for="checkbox-<?php echo $this->_tpl_vars['k']; ?>
"></label>
					  </span>
					</td>	
					<td  width="20%"><input class="do-trnd input priority_class" id="<?php echo $this->_tpl_vars['sandwich']->id; ?>
" style="max-width: 40%;" type="number" min="1" name="priority" value="<?php echo $this->_tpl_vars['sandwich']->priority; ?>
" /></td>	
					<td><?php echo $this->_tpl_vars['sandwich']->sandwich_name; ?>
</td>					
					<td><?php echo $this->_tpl_vars['sandwich']->first_name; ?>
 <?php echo $this->_tpl_vars['sandwich']->last_name; ?>
</td>
					<td>$<?php echo $this->_tpl_vars['sandwich']->sandwich_price; ?>
</td>
					<td> <a href="#" class="remove_trnd" data-sandwich="<?php echo $this->_tpl_vars['sandwich']->id; ?>
">Remove</a></td>
					<td> <a href="javascript:;" class="set_priority" data-priority="<?php echo $this->_tpl_vars['sandwich']->id; ?>
" data-sandwich="<?php echo $this->_tpl_vars['sandwich']->id; ?>
">Update</a></td>
				</tr>
				<?php endforeach; endif; unset($_from); ?>				
			</tbody>	
        </table>
        <?php else: ?>
			
			<p> No Sandwiches in this section.</p>
			
        <?php endif; ?>
    </div>     
</div>