<?php
/* Smarty version 3.1.39, created on 2021-03-25 09:53:42
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c95e61fbe99_73289564',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bece5264c9f3864317663cca07dc118e3a25c10d' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\header.tpl',
      1 => 1616680339,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c95e61fbe99_73289564 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head> 
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo $_smarty_tpl->tpl_vars['Title']->value;?>
</title>
<link href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/screen.css" rel="stylesheet"/>
<link href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/print.css" rel="stylesheet"/>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/jquery-ui.css"/>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/common.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.mCustomScrollbar.concat.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
ckeditor/ckeditor.js"><?php echo '</script'; ?>
>




<?php echo '<script'; ?>
>
	var SITE_URL = "<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
";
	var CMS_URL = "<?php echo $_smarty_tpl->tpl_vars['CMS_URL']->value;?>
";
	var API_URL  = "<?php echo $_smarty_tpl->tpl_vars['API_URL']->value;?>
";
	var SESSION_ID = "<?php echo $_smarty_tpl->tpl_vars['TO_USER_ID']->value;?>
";
	var DATA_ID = "<?php echo $_smarty_tpl->tpl_vars['DATA_ID']->value;?>
";
	var BASE_FARE = "<?php echo $_smarty_tpl->tpl_vars['BASE_FARE']->value;?>
";
	 
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/sandwichcreator.js"><?php echo '</script'; ?>
>

</head>
<body>


<div class="popupOrderDetailsBg">&nbsp;</div>

<div class="popupOrderDetails">

<div class="clear"></div>


<div class="popup_order_detail" >

<p  class="closeOrd">X</p>
<div id="popOut" >
  
  
  <p class="loader">
  <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/optionload.gif" />
  </p>
  </div>
  
 <!-- end -->
  </div>  

<div class="clear"></div>

</div>

<div id="outer-wrapper">

<div class="container">
     <!--Header Starting-->
     <div class="header">
        <div class="logo">
           <a href="#"><img width="225" height="28" alt="<?php echo $_smarty_tpl->tpl_vars['title_text']->value;?>
" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo.png"></a>
        </div>  
        
        <div class="user-login">
         
             <?php  if( isset($_SESSION['admin_uid']) ) {  ?>
           <p><?php  echo $_SESSION['uname'] ?></p>
           <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
login/signout/">logout</a>
          <?php  } ?>
        </div>
        
     </div>
  </div>


<div class="navigation">
    <div class="container">
   <?php  if( isset($_SESSION['admin_uid']) ) {  ?>
      <nav>
        <ul>
		<?php if ($_smarty_tpl->tpl_vars['User_Type']->value != 'store') {?>	
	    <li class="first-child"><a  <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'neworder') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/">NEW ORDER</a></li>
	  
	    
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'deliveries') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
deliveries/">DELIVERIES</a></li>
		
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'pickup') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
pickups/">PICK-UPS</a></li>
		<?php } else { ?>
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'storedeliveries') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
storedeliveries/">DELIVERIES</a></li>
		
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'storepickups') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
storepickups/">PICK-UPS</a></li>
		
		<?php }?>
		
		
		  
		<?php if ($_smarty_tpl->tpl_vars['User_Type']->value != 'store') {?>	
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'customers') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
customer/">CUSTOMERS</a></li>
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'menu') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/">MENU ITEMS</a></li>
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'sandwich') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/">SANDWICH GALLERY</a></li>
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'discounts') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
discounts/">DISCOUNTS </a></li>
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'web') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
web/">WEB PAGES</a></li>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['User_Type']->value != 'store') {?>	
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'reports') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
reports/">REPORTS</a></li>
		<?php } else { ?>
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'reports') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
storereports">REPORTS</a></li>
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'storemanage') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
storemanage/">MANAGE STORE</a></li>	
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['User_Type']->value != 'store') {?>	
		<li><a <?php if ($_smarty_tpl->tpl_vars['ACTIVE_MAIN_MENU_TEXT']->value == 'admin') {?> class="active" <?php }?> href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/">ADMIN</a></li>
		<?php }?>
   
        </ul>
      </nav>
<?php  } ?>
    </div>
  </div>
  
<?php if ($_smarty_tpl->tpl_vars['SEC_NAV_OPS']->value['show'] == true) {?> 

<div class="secondary-navigation">
    <div class="container">
      <nav>
        <ul>
<?php
$__section_nav_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['SEC_NAV_OPS']->value['text'])) ? count($_loop) : max(0, (int) $_loop));
$__section_nav_0_start = min(0, $__section_nav_0_loop);
$__section_nav_0_total = min(($__section_nav_0_loop - $__section_nav_0_start), $__section_nav_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_nav'] = new Smarty_Variable(array());
if ($__section_nav_0_total !== 0) {
for ($__section_nav_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_nav']->value['index'] = $__section_nav_0_start; $__section_nav_0_iteration <= $__section_nav_0_total; $__section_nav_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_nav']->value['index']++){
?>

<li class="first-child"><a <?php if ($_smarty_tpl->tpl_vars['SEC_NAV_OPS']->value['active_text'] == $_smarty_tpl->tpl_vars['SEC_NAV_OPS']->value['text'][(isset($_smarty_tpl->tpl_vars['__smarty_section_nav']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_nav']->value['index'] : null)]) {?> class="active" <?php }?>  href="<?php echo $_smarty_tpl->tpl_vars['SEC_NAV_OPS']->value['url'][(isset($_smarty_tpl->tpl_vars['__smarty_section_nav']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_nav']->value['index'] : null)];?>
"> <?php echo $_smarty_tpl->tpl_vars['SEC_NAV_OPS']->value['text'][(isset($_smarty_tpl->tpl_vars['__smarty_section_nav']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_nav']->value['index'] : null)];?>
 </a></li>
<?php
}
}
?>
         
       </ul>
      </nav>
    </div>
  </div> 
  
<?php }?>


<?php  if( isset($_SESSION['success']) ) {?>
<div style="width: 930px;height: auto;margin: 0 auto;clear:both">
<br><p style="color:red;margin-top:10px;"><?php echo $_smarty_tpl->tpl_vars['success']->value;?>
</div></div>
<?php  unset($_SESSION['success']); } ?>

<?php }
}
