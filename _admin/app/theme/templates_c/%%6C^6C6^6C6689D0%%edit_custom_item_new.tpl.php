<?php /* Smarty version 2.6.25, created on 2020-09-16 00:04:49
         compiled from edit_custom_item_new.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'explode', 'edit_custom_item_new.tpl', 50, false),array('modifier', 'in_array', 'edit_custom_item_new.tpl', 57, false),)), $this); ?>
<div class="container">
  <div class="condiment">
    <h1>EDIT ITEM</h1>

    <form name="newprotein" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/updateItems" enctype="multipart/form-data">

      <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
">
      
      <input type="hidden" name="old_image" value="<?php echo $this->_tpl_vars['data']['item_image']; ?>
">
      <input type="hidden" name="old_image_sliced" value="<?php echo $this->_tpl_vars['data']['item_image_sliced']; ?>
">

      <div class="edit-item--input">
        <label>Name of Protein</label>
        <input type="text" name="item_name" value="<?php echo $this->_tpl_vars['data']['item_name']; ?>
">
      </div>

      <div class="edit-item--input">
        <label>Price</label>
        <input type="text" name="item_price" value="<?php echo $this->_tpl_vars['data']['item_price']; ?>
">
      </div>
      <div class="texa-prem_wrap">
        <div class="texa-prem forms">
          <label>Taxable</label>
          <!--   <input type="checkbox" value="1" name="premium" class="price-menu"  > -->
          <label for="cbk1">
            <input type="checkbox" id="cbk1" value="1" name="taxable" <?php if ($this->_tpl_vars['data']['taxable'] == 1): ?> checked <?php endif; ?>>
            <span class="cbx">
              <svg width="12px" height="11px" viewBox="0 0 12 11">
                <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
              </svg>
            </span>

          </label>

        </div> 
          <input type="hidden" value="2" name="category_id">
        <div class="texa-prem forms">
          <label>Premium</label>
          <label for="cbk2">
            <input type="checkbox" id="cbk2" value="1" name="premium" <?php if ($this->_tpl_vars['data']['premium'] == 1): ?> checked <?php endif; ?>>
            <span class="cbx">
              <svg width="12px" height="11px" viewBox="0 0 12 11">
                <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
              </svg>
            </span>

          </label>
        </div>
      </div>
      <?php $this->assign('optnarray', ((is_array($_tmp=":")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['data']['options_id']) : explode($_tmp, $this->_tpl_vars['data']['options_id']))); ?>
      <?php $_from = $this->_tpl_vars['sandwichCatagoryData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>



      <div class="forms file-choose-pro">
        <label for="cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
">
          <input type="checkbox" id="cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" name="select_sandwich_option[]" value="<?php echo $this->_tpl_vars['b']['id']; ?>
"  onclick="showUploadImage(this)" <?php if (((is_array($_tmp=$this->_tpl_vars['b']['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['optnarray']) : in_array($_tmp, $this->_tpl_vars['optnarray'])) == 1): ?> checked <?php endif; ?> >
          <span class="cbx">
            <svg width="12px" height="11px" viewBox="0 0 12 11">
              <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
            </svg>
          </span>
        </label>
        <label for="cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" class="multisel-ckeck"><?php echo $this->_tpl_vars['b']['option_display_name']; ?>
</label>
      </div>

      <div class="slect-image--wrap" id="upload-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" style="display: none;">
        <div>
          <label>Long Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="lImg-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" name="lImg-<?php echo $this->_tpl_vars['b']['id']; ?>
" />
             
            </label>
            <span id="lImgName-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
">No file selected</span>
            <?php $_from = $this->_tpl_vars['optionImgs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ky'] => $this->_tpl_vars['op']):
?>
            <?php if ($this->_tpl_vars['ky'] == $this->_tpl_vars['b']['id'] && $this->_tpl_vars['op']['lImg'] != ""): ?>
            <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/<?php echo $this->_tpl_vars['op']['lImg']; ?>
">
            <!-- <input type="hidden" name="<?php echo $this->_tpl_vars['b']['id']; ?>
-old-lImg-cbk" value="<?php echo $this->_tpl_vars['op']['lImg']; ?>
"> -->
            <input type="hidden" name="oldOptionImgs[<?php echo $this->_tpl_vars['op']['lImg']; ?>
]" value="<?php echo $this->_tpl_vars['ky']; ?>
">
            <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
          </p>
        </div>

        <div>

          <label>Round Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="rImg-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" name="rImg-<?php echo $this->_tpl_vars['b']['id']; ?>
" value="" />
              
            </label>
            <span id="rImgName-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
">No file selected</span>
            <?php $_from = $this->_tpl_vars['optionImgs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ky'] => $this->_tpl_vars['op']):
?>
            <?php if ($this->_tpl_vars['ky'] == $this->_tpl_vars['b']['id'] && $this->_tpl_vars['op']['rImg'] != ""): ?>
            <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/<?php echo $this->_tpl_vars['op']['rImg']; ?>
">
            <!-- <input type="hidden" name="<?php echo $this->_tpl_vars['b']['id']; ?>
-old-rImg-cbk" value="<?php echo $this->_tpl_vars['op']['rImg']; ?>
"> -->
            <input type="hidden" name="oldOptionImgs[<?php echo $this->_tpl_vars['op']['rImg']; ?>
]" value="<?php echo $this->_tpl_vars['ky']; ?>
">
            <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
          </p>
        </div>
        <div>
          <label>Trapezoid Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="tImg-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
" name="tImg-<?php echo $this->_tpl_vars['b']['id']; ?>
"/>

            </label>
            <span id="tImgName-cbk-<?php echo $this->_tpl_vars['b']['id']; ?>
">No file selected</span>

            <?php $_from = $this->_tpl_vars['optionImgs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ky'] => $this->_tpl_vars['op']):
?>
            <?php if ($this->_tpl_vars['ky'] == $this->_tpl_vars['b']['id'] && $this->_tpl_vars['op']['tImg'] != ""): ?>
            <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/<?php echo $this->_tpl_vars['op']['tImg']; ?>
">
            <!-- <input type="hidden" name="<?php echo $this->_tpl_vars['b']['id']; ?>
-old-tImg-cbk" value="<?php echo $this->_tpl_vars['op']['tImg']; ?>
"> -->
            <input type="hidden" name="oldOptionImgs[<?php echo $this->_tpl_vars['op']['tImg']; ?>
]" value="<?php echo $this->_tpl_vars['ky']; ?>
">
            <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>

          </p>
        </div>
      </div>
      <?php endforeach; endif; unset($_from); ?>  

<input type="hidden" value="<?php echo $this->_tpl_vars['data']['item_price']; ?>
" name="old_price">
      <div class="button">
       <input type="submit" class="save save_custom_item" value="SAVE">
       <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/custom" class="cancel">CANCEL</a>
     </div>
   </form>

 </div>
</div>