<?php /* Smarty version 2.6.25, created on 2018-08-14 05:56:34
         compiled from sandwich_of_the_day.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'sandwich_of_the_day.tpl', 7, false),array('modifier', 'date_format', 'sandwich_of_the_day.tpl', 20, false),array('function', 'cycle', 'sandwich_of_the_day.tpl', 19, false),)), $this); ?>
<div class="container">
	<div class="sandwich-gallery">
		<div class="heading">
         <h1>SANDWICH OF THE DAY</h1>
         <a class="add_sotd_button">ADD NEW</a>
        </div>
        <?php if (count($this->_tpl_vars['sandwiches']) > 0): ?>
        <table class="listing">
			<tbody>
				<tr class="static">
				  <th width="25%" height="31">DATE SCHEDULED</th>
				  <th width="25%" height="31">CUSTOM SANDWICHES</th>
				  <th width="20%" height="31">USER</th>
				  <th width="15%">PRICE</th>				  
				  <th width="15%">        </th>				  
				</tr>
				
				<?php $_from = $this->_tpl_vars['sandwiches']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['sandwich']):
?>
				<tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
">
					<td><input name="sotd_date" id="sotd_date<?php echo $this->_tpl_vars['k']; ?>
" type="text" placeholder="<?php echo ((is_array($_tmp=$this->_tpl_vars['sandwich']->sotd_date)) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp)); ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sandwich']->sotd_date)) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp)); ?>
" class="text-box" readonly />
					<a class="calendar" href="#"><img class="calendarimg" width="24" height="23" alt="calendar" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/calendar_new.png"></a></td>
					
					<td><?php echo $this->_tpl_vars['sandwich']->sandwich_name; ?>
</td>
					
					<td><?php echo $this->_tpl_vars['sandwich']->first_name; ?>
 <?php echo $this->_tpl_vars['sandwich']->last_name; ?>
</td>
					<td>$<?php echo $this->_tpl_vars['sandwich']->sandwich_price; ?>
</td>
					<td> <a href="#" class="remove_sotd" data-sandwich="<?php echo $this->_tpl_vars['sandwich']->id; ?>
">Remove</a></td>
				</tr>
				<?php endforeach; endif; unset($_from); ?>				
			</tbody>	
        </table>
        <?php else: ?>
			
			<p> No Sandwiches in this section.</p>
			
        <?php endif; ?>
    </div>     
</div>