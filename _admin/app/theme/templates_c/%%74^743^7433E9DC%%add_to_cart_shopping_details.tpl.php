<?php /* Smarty version 2.6.25, created on 2020-09-21 10:10:18
         compiled from add_to_cart_shopping_details.tpl */ ?>
<h3>SHOPPING CART</h3>
        <form  id="cartdetails">
         <table class="shopping-cart-table-new">
			<tbody>
				<tr>
				 <th>ORDER DETAILS</th>
				 <th>&nbsp;</th>
				 <th>QTY</th>
				 <th>TOTAL</th>
				 <th>&nbsp;</th>
				</tr>
				<?php $_from = $this->_tpl_vars['sandwiches']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['myId'] => $this->_tpl_vars['sandwich']):
?>
				<tr>
				 <td colspan="2">
					 <p><?php echo $this->_tpl_vars['sandwich']['sandwich_name']; ?>
_________________________________</p>
					<p><?php echo $this->_tpl_vars['sandwich']['data_string']; ?>
</p>
				 </td>
				 <td>
					<span class="arrow-holder">
				   <a href="#" class="left arrow-left"></a>
				   <input type="text" class="qty" name="qty[]" value="<?php echo $this->_tpl_vars['sandwich']['item_qty']; ?>
">
				   <a href="#" class="arrow-right"></a>
				   <input type="hidden" name="data_id" value="<?php echo $this->_tpl_vars['sandwich']['id']; ?>
" />
				   <input type="hidden" name="data_type" value="user_sandwich" />
				   <input type="hidden" name="data_order_id" value="<?php echo $this->_tpl_vars['sandwich']['order_item_id']; ?>
" />
				 </span>
				 </td>
				   <td>$<?php echo $this->_tpl_vars['sandwich']['item_price']*$this->_tpl_vars['sandwich']['item_qty']; ?>
</td>
				 <td><a href="javascript:void(0)" class="remove remove_cart_item">REMOVE</a></td>
				</tr>
				<?php endforeach; endif; unset($_from); ?>
				
				<?php $_from = $this->_tpl_vars['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['myId'] => $this->_tpl_vars['product']):
?>
				<tr>
				 <td colspan="2"><p><?php echo $this->_tpl_vars['product']->product_name; ?>
 ______________________________<?php if ($this->_tpl_vars['product']->extra_name): ?><br/> <?php echo $this->_tpl_vars['product']->extra_name; ?>
<?php endif; ?></p></td>
				 <td>
					<span class="arrow-holder">
				   <a href="#" class="arrow-left"></a>
				   <input type="text" class="qty" name="qty[]" value="<?php echo $this->_tpl_vars['product']->item_qty; ?>
">
				   <a href="#" class="arrow-right"></a>
				   <input type="hidden" name="data_id" value="<?php echo $this->_tpl_vars['product']->id; ?>
" />
				   <input type="hidden" name="data_order_id" value="<?php echo $this->_tpl_vars['product']->order_item_id; ?>
" />
				   <input type="hidden" name="data_type" value="product" />
				 </span>
				 </td>
				 <td>$<?php echo $this->_tpl_vars['product']->item_price; ?>
</td>
				 <td><a href="javascript:void(0)" class="remove remove_cart_item">REMOVE</a></td>
				</tr>
				<?php endforeach; endif; unset($_from); ?>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">SUBTOTAL</td>
            <td id='addToCart_Subtotal'>$<?php echo $this->_tpl_vars['subtotal']; ?>
 </td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">TIP</td>
            <td colspan="2">
              <select name="tips_selected" class="select-tip">
                <option <?php if ($this->_tpl_vars['tips'] == 3): ?> selected <?php endif; ?> value="3">$3.00</option>
                <option <?php if ($this->_tpl_vars['tips'] == 3.50): ?> selected <?php endif; ?> value="3.50">$3.50</option>
                <option <?php if ($this->_tpl_vars['tips'] == 4): ?> selected <?php endif; ?> value="4">$4.00</option>
                <option <?php if ($this->_tpl_vars['tips'] == 4.50): ?> selected <?php endif; ?> value="4.50">$4.50</option>
                <option <?php if ($this->_tpl_vars['tips'] == 5): ?> selected <?php endif; ?> value="5">$5.00</option>
                <option <?php if ($this->_tpl_vars['tips'] == 5.50): ?> selected <?php endif; ?> value="5">$5.50</option>
                <option <?php if ($this->_tpl_vars['tips'] == 6): ?> selected <?php endif; ?> value="6">$6.00</option>
                <option <?php if ($this->_tpl_vars['tips'] == 6.50): ?> selected <?php endif; ?> value="6.50">$6.50</option>
                <option <?php if ($this->_tpl_vars['tips'] == 7): ?> selected <?php endif; ?> value="7">$7.00</option>
                <option <?php if ($this->_tpl_vars['tips'] == 7.50): ?> selected <?php endif; ?> value="7.50">$7.50</option>
                <option <?php if ($this->_tpl_vars['tips'] == 8): ?> selected <?php endif; ?> value="8">$8.00</option>
                <option <?php if ($this->_tpl_vars['tips'] == 8.50): ?> selected <?php endif; ?> value="8.50">$8.50</option>
                <option <?php if ($this->_tpl_vars['tips'] == 9): ?> selected <?php endif; ?> value="9">$9.00</option>
                <option <?php if ($this->_tpl_vars['tips'] == 9): ?> selected <?php endif; ?> value="9.50">$9.50</option>
                <option <?php if ($this->_tpl_vars['tips'] == 10): ?> selected <?php endif; ?> value="10">$10.00</option>                
                <option <?php if ($this->_tpl_vars['tips'] == 10.50): ?> selected <?php endif; ?> value="10.50">$10.50</option>                
              </select>
            </td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TAX</h4></td>
            <td><h4 id='taxvalue'>$<?php echo $this->_tpl_vars['tax']; ?>
</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL VALUE</h4></td>
            <td><h4>$<?php echo $this->_tpl_vars['total_price']; ?>
</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">DISCOUNT:  <?php echo $this->_tpl_vars['off_type']; ?>
 OFF SPECIAL</td>
            <td>-$<?php echo $this->_tpl_vars['off_value']; ?>
</td>
            <td>&nbsp;</td>
           </tr>
		   
		   <?php if ($this->_tpl_vars['manual_discount']): ?>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">MANUAL DISCOUNT: </td>
            <td>-$<?php echo $this->_tpl_vars['manual_discount']; ?>
</td>
            <td>&nbsp;</td>
           </tr>
		   <?php endif; ?>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">DISCOUNT / GIFT CARD</p>
            	<span class="text-box-holder">
              	<input name="" type="text" class="text-box coupon_code">
                <a href="javascript:void(0)" class="shopping-cart-select apply_coupon">Apply</a>
              </span>
            </td>
           </tr>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">MANUAL DISCOUNT</p>
            	<span class="text-box-holder">
              	<input name="manual_discount_val" type="text" class="text-box manual_discount" value="">
                <a href="javascript:void(0)" class="shopping-cart-select apply_manual_discount">Apply</a>              
              </span>
            </td>
           </tr>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL OWED</h4></td>
            <td><h4>$<?php echo $this->_tpl_vars['grand_total']; ?>
</h4></td>
            <td>&nbsp;</td>
           </tr>
         </tbody>
        </table>
		
		<input	type="hidden" name="order_string" value="<?php echo $this->_tpl_vars['order_string']; ?>
" />
		<input	type="hidden" name="grand_total" value="<?php echo $this->_tpl_vars['grand_total']; ?>
" />
		<input	type="hidden" name="sub_total" value="<?php echo $this->_tpl_vars['total_price']; ?>
" />
		<input	type="hidden" name="coupon_id" value="<?php echo $this->_tpl_vars['coupon_id']; ?>
" />
		<input	type="hidden" name="coupon_code" value="<?php echo $this->_tpl_vars['coupon_code']; ?>
" />
		<input	type="hidden" name="manual_discount" value="<?php echo $this->_tpl_vars['manual_discount']; ?>
" />
		<input	type="hidden" name="tips" value="<?php echo $this->_tpl_vars['tips']; ?>
" />
		<input	type="hidden" name="off_type" value="<?php echo $this->_tpl_vars['off_type']; ?>
" />
		<input	type="hidden" name="off_amount" value="<?php echo $this->_tpl_vars['off_value']; ?>
" />
		
		</form>			
    