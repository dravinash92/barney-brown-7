<?php /* Smarty version 2.6.25, created on 2020-10-14 00:44:13
         compiled from order_detail_popup.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip', 'order_detail_popup.tpl', 13, false),array('modifier', 'count_characters', 'order_detail_popup.tpl', 16, false),array('modifier', 'truncate', 'order_detail_popup.tpl', 113, false),array('modifier', 'replace', 'order_detail_popup.tpl', 114, false),array('modifier', 'date_format', 'order_detail_popup.tpl', 116, false),array('modifier', 'explode', 'order_detail_popup.tpl', 180, false),array('modifier', 'cat', 'order_detail_popup.tpl', 192, false),)), $this); ?>

<table cellpadding="0" cellspacing="0" border="0" style="width:100%" class="print">
<tr>
<td style="width:50%">



<div class="order_details_left">
    <h1>  <?php if ($this->_tpl_vars['is_delivery'] == 1): ?>  DELIVERY <?php else: ?> PICKUP <?php endif; ?> #<?php echo $this->_tpl_vars['orderid']; ?>
<br />TRANSACTION ID #<?php echo $this->_tpl_vars['authorizenet_transaction_id']; ?>
<br /><?php echo $this->_tpl_vars['date']; ?>
 <?php if ($this->_tpl_vars['time'] == 1): ?> -  (SPECIFIC) <?php endif; ?> </h1>
     <div class="address" style="display:none;">    
      <p>  
      
       <?php if (((is_array($_tmp=$this->_tpl_vars['address']->name)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?>  <?php echo $this->_tpl_vars['address']->name; ?>
 <br /> <?php endif; ?>
       <?php if (((is_array($_tmp=$this->_tpl_vars['address']->company)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?>  <?php echo $this->_tpl_vars['address']->company; ?>
<br /> <?php endif; ?>
       <?php if (((is_array($_tmp=$this->_tpl_vars['address']->addline1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?>  <?php echo $this->_tpl_vars['address']->addline1; ?>
, <?php endif; ?> <?php if (((is_array($_tmp=$this->_tpl_vars['address']->street)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?> <?php echo $this->_tpl_vars['address']->street; ?>
, <?php endif; ?> <?php if (((is_array($_tmp=$this->_tpl_vars['address']->addline1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)) || ((is_array($_tmp=$this->_tpl_vars['address']->street)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><br /><?php endif; ?>
          <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline2)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?>  <?php echo ((is_array($_tmp=$this->_tpl_vars['address']->addline2)) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)); ?>
,  <br /><?php endif; ?>
       New York, NY <?php echo $this->_tpl_vars['address']->zip; ?>
<br />
       <?php if ($this->_tpl_vars['is_delivery'] == 1): ?>  <?php if ($this->_tpl_vars['address']->cross_streets): ?>  (<?php echo $this->_tpl_vars['address']->cross_streets; ?>
)<br /> <?php endif; ?>
       <?php endif; ?>
       <?php echo $this->_tpl_vars['address']->phone; ?>

       
      </p>
    </div>
    
    <?php if ($this->_tpl_vars['is_delivery'] == 1 && $this->_tpl_vars['delivery_instructions']): ?> <div class="instructions">
       <p>INSTRUCTIONS: <?php echo $this->_tpl_vars['delivery_instructions']; ?>
</p>  
    </div>
    <?php endif; ?>
     
    <?php unset($this->_sections['orderitems']);
$this->_sections['orderitems']['name'] = 'orderitems';
$this->_sections['orderitems']['loop'] = is_array($_loop=$this->_tpl_vars['order_item_detail']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['orderitems']['show'] = true;
$this->_sections['orderitems']['max'] = $this->_sections['orderitems']['loop'];
$this->_sections['orderitems']['step'] = 1;
$this->_sections['orderitems']['start'] = $this->_sections['orderitems']['step'] > 0 ? 0 : $this->_sections['orderitems']['loop']-1;
if ($this->_sections['orderitems']['show']) {
    $this->_sections['orderitems']['total'] = $this->_sections['orderitems']['loop'];
    if ($this->_sections['orderitems']['total'] == 0)
        $this->_sections['orderitems']['show'] = false;
} else
    $this->_sections['orderitems']['total'] = 0;
if ($this->_sections['orderitems']['show']):

            for ($this->_sections['orderitems']['index'] = $this->_sections['orderitems']['start'], $this->_sections['orderitems']['iteration'] = 1;
                 $this->_sections['orderitems']['iteration'] <= $this->_sections['orderitems']['total'];
                 $this->_sections['orderitems']['index'] += $this->_sections['orderitems']['step'], $this->_sections['orderitems']['iteration']++):
$this->_sections['orderitems']['rownum'] = $this->_sections['orderitems']['iteration'];
$this->_sections['orderitems']['index_prev'] = $this->_sections['orderitems']['index'] - $this->_sections['orderitems']['step'];
$this->_sections['orderitems']['index_next'] = $this->_sections['orderitems']['index'] + $this->_sections['orderitems']['step'];
$this->_sections['orderitems']['first']      = ($this->_sections['orderitems']['iteration'] == 1);
$this->_sections['orderitems']['last']       = ($this->_sections['orderitems']['iteration'] == $this->_sections['orderitems']['total']);
?>
    
    
      
      <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->type == 'user_sandwich'): ?> 
      
    <div class="order_details_checkbox-wrapper">
      <div class="checkbox"><input type="checkbox"  name="sqrchk<?php echo $this->_sections['orderitems']['index']; ?>
" id="sqrchk<?php echo $this->_sections['orderitems']['index']; ?>
"  class="printchksqure"/>
      <label for="sqrchk<?php echo $this->_sections['orderitems']['index']; ?>
"  class="printchksqure_label"></label></div> 
      <div class="product"> 
       <div class="quantity"> <?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->qty; ?>
</div>  
       <div class="ingredient"><div class="order-item-name"><?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_name; ?>
</div>
       <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_bread): ?><span class="ingredient_span">Bread:</span>
	   <label> <?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_bread; ?>
</label> <br /> <?php endif; ?> 
       <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_condiments): ?><span class="ingredient_span">Condiments:</span> <label><?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_condiments; ?>
</label> <br /> <?php endif; ?>
       <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_protein): ?><span class="ingredient_span">Protein:</span> <label><?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_protein; ?>
 </label>  <br />  <?php endif; ?> 
       <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_cheese): ?><span class="ingredient_span">Cheese:</span> <label> <?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_cheese; ?>
 </label>  <br />  <?php endif; ?>
       <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_topping): ?><span class="ingredient_span">Toppings:</span> <label> <?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->sandwich_topping; ?>
 </label><br/> <?php endif; ?> 
       <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->toasted == 1): ?> 
          <b>Toasted</b>
       <?php endif; ?>
       
       </div>
      </div>
    </div>
    
    <?php else: ?>
    
     <div class="order_details_checkbox-wrapper">
      <div class="checkbox"><input type="checkbox"  name="sqrchk<?php echo $this->_sections['orderitems']['index']; ?>
" id="sqrchk<?php echo $this->_sections['orderitems']['index']; ?>
"  class="printchksqure"/>
      <label for="sqrchk<?php echo $this->_sections['orderitems']['index']; ?>
"  class="printchksqure_label"></label>
      </div> 
      <div class="product"> 
       <div class="product-quantity"><?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->qty; ?>
 </div>  
       <div class="product-ingredient"><?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orderitems']['index']]->product_name; ?>

       </div>
      </div>
    </div>
    
    <?php endif; ?>
  

  
    
    <?php endfor; endif; ?>

  </div>

</td>
<td style="width:38%">
  <div class="order_details_right">
   
    

    
  <?php if ($this->_tpl_vars['is_delivery'] == 1 && $this->_tpl_vars['delivery_instructions']): ?> <!--   <div class="instructions">
       <p>INSTRUCTIONS: <?php echo $this->_tpl_vars['delivery_instructions']; ?>
</p>  
    </div>-->
    <?php endif; ?>
       
       
           
       
    <div class="ingredient-wrapper">
       
       <div class="ingredient <?php if ($this->_tpl_vars['is_delivery'] == 1): ?>  delivery_address <?php else: ?> pickup_address <?php endif; ?>">
		
			<div class="wrap_item">
				<div class="cell">
				   <h3><?php if ($this->_tpl_vars['address']->name): ?>  <?php echo $this->_tpl_vars['address']->name; ?>
 <?php endif; ?> </h3>
				   <h1 class="ordCount"><?php echo $this->_tpl_vars['count_order']; ?>
</h1>
				   
				   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->company)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <p><?php echo $this->_tpl_vars['address']->company; ?>
</p> <?php endif; ?>
				   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <p> <?php echo $this->_tpl_vars['address']->addline1; ?>
, <?php endif; ?> <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->street)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <?php echo $this->_tpl_vars['address']->street; ?>
, <?php endif; ?> <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) || ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->street)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?></p><?php endif; ?>
				   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline2)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?><p><?php echo $this->_tpl_vars['address']->addline2; ?>
,  </p><?php endif; ?>
				   <p>New York, NY <?php echo $this->_tpl_vars['address']->zip; ?>
</p>
				   <?php if ($this->_tpl_vars['is_delivery'] == 1): ?>  <?php if ($this->_tpl_vars['address']->cross_streets): ?><p>(<?php echo $this->_tpl_vars['address']->cross_streets; ?>
)</p> <?php endif; ?>
				   <?php endif; ?>
				   <p><?php echo $this->_tpl_vars['address']->phone; ?>
</p>
				  
				
				   <?php if ($this->_tpl_vars['is_delivery'] == 1 && ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['delivery_instructions'])) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?>    
				   <!--<div class="instructions">
				   <p>INSTRUCTIONS: <?php echo ((is_array($_tmp=$this->_tpl_vars['delivery_instructions'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 60, "...", true) : smarty_modifier_truncate($_tmp, 60, "...", true)); ?>
</p> </div>-->  <?php endif; ?>
				   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->sandwich_details)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?><p><?php echo ((is_array($_tmp=$this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->sandwich_details)) ? $this->_run_mod_handler('replace', true, $_tmp, ', , ', ', ') : smarty_modifier_replace($_tmp, ', , ', ', ')); ?>
 </p><?php endif; ?>
				   
				   <p> <strong>Order Placed: </strong> <?php echo ((is_array($_tmp=$this->_tpl_vars['order_date_time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%D - %H:%M") : smarty_modifier_date_format($_tmp, "%D - %H:%M")); ?>
 </p>
				</div>   
			</div>
      	</div>
		<!-- Asked to display Address box twice -->
		<div class="ingredient address_page <?php if ($this->_tpl_vars['is_delivery'] == 1): ?>  delivery_address <?php else: ?> pickup_address <?php endif; ?>">
		
			<div class="wrap_item">
				<div class="cell">
				   <h3><?php if ($this->_tpl_vars['address']->name): ?>  <?php echo $this->_tpl_vars['address']->name; ?>
 <?php endif; ?> </h3>
				   <h1 class="ordCount"><?php echo $this->_tpl_vars['count_order']; ?>
</h1>
				   
				   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->company)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <p><?php echo $this->_tpl_vars['address']->company; ?>
</p> <?php endif; ?>
				   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <p> <?php echo $this->_tpl_vars['address']->addline1; ?>
, <?php endif; ?> <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->street)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <?php echo $this->_tpl_vars['address']->street; ?>
, <?php endif; ?> <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) || ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->street)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?></p><?php endif; ?>
				   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline2)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?><p><?php echo $this->_tpl_vars['address']->addline2; ?>
,  </p><?php endif; ?>
				   <p>New York, NY <?php echo $this->_tpl_vars['address']->zip; ?>
</p>
				   <?php if ($this->_tpl_vars['is_delivery'] == 1): ?>  <?php if ($this->_tpl_vars['address']->cross_streets): ?><p>(<?php echo $this->_tpl_vars['address']->cross_streets; ?>
)</p> <?php endif; ?>
				   <?php endif; ?>
				   <p><?php echo $this->_tpl_vars['address']->phone; ?>
</p>
				
				   <?php if ($this->_tpl_vars['is_delivery'] == 1 && ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['delivery_instructions'])) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?>    
				   <!--<div class="instructions">
				   <p>INSTRUCTIONS: <?php echo ((is_array($_tmp=$this->_tpl_vars['delivery_instructions'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 60, "...", true) : smarty_modifier_truncate($_tmp, 60, "...", true)); ?>
</p> </div>-->  <?php endif; ?>
				   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->sandwich_details)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?><p><?php echo ((is_array($_tmp=$this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->sandwich_details)) ? $this->_run_mod_handler('replace', true, $_tmp, ', , ', ', ') : smarty_modifier_replace($_tmp, ', , ', ', ')); ?>
 </p><?php endif; ?>
				   
				    <p> <strong>Order Placed: </strong> <?php echo ((is_array($_tmp=$this->_tpl_vars['order_date_time'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%D - %H:%M") : smarty_modifier_date_format($_tmp, "%D - %H:%M")); ?>
 </p>
				</div>   
			</div>
      	</div>
		<!-- Asked to display Address box twice -->
		
         <?php $this->assign('foocount', '0'); ?>  
         <?php $this->assign('barcount', '0'); ?>
         <?php $this->assign('sidebarcount', '0'); ?>
         <?php $this->assign('boxes', '9'); ?>
		 <?php $this->assign('fcount', 1); ?>
		 <?php $this->assign('pageCount', 1); ?>
		 <?php $this->assign('gtrcount', 22); ?>
         
        <?php unset($this->_sections['orderitemsdet']);
$this->_sections['orderitemsdet']['name'] = 'orderitemsdet';
$this->_sections['orderitemsdet']['loop'] = is_array($_loop=$this->_tpl_vars['order_item_detail']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['orderitemsdet']['show'] = true;
$this->_sections['orderitemsdet']['max'] = $this->_sections['orderitemsdet']['loop'];
$this->_sections['orderitemsdet']['step'] = 1;
$this->_sections['orderitemsdet']['start'] = $this->_sections['orderitemsdet']['step'] > 0 ? 0 : $this->_sections['orderitemsdet']['loop']-1;
if ($this->_sections['orderitemsdet']['show']) {
    $this->_sections['orderitemsdet']['total'] = $this->_sections['orderitemsdet']['loop'];
    if ($this->_sections['orderitemsdet']['total'] == 0)
        $this->_sections['orderitemsdet']['show'] = false;
} else
    $this->_sections['orderitemsdet']['total'] = 0;
if ($this->_sections['orderitemsdet']['show']):

            for ($this->_sections['orderitemsdet']['index'] = $this->_sections['orderitemsdet']['start'], $this->_sections['orderitemsdet']['iteration'] = 1;
                 $this->_sections['orderitemsdet']['iteration'] <= $this->_sections['orderitemsdet']['total'];
                 $this->_sections['orderitemsdet']['index'] += $this->_sections['orderitemsdet']['step'], $this->_sections['orderitemsdet']['iteration']++):
$this->_sections['orderitemsdet']['rownum'] = $this->_sections['orderitemsdet']['iteration'];
$this->_sections['orderitemsdet']['index_prev'] = $this->_sections['orderitemsdet']['index'] - $this->_sections['orderitemsdet']['step'];
$this->_sections['orderitemsdet']['index_next'] = $this->_sections['orderitemsdet']['index'] + $this->_sections['orderitemsdet']['step'];
$this->_sections['orderitemsdet']['first']      = ($this->_sections['orderitemsdet']['iteration'] == 1);
$this->_sections['orderitemsdet']['last']       = ($this->_sections['orderitemsdet']['iteration'] == $this->_sections['orderitemsdet']['total']);
?> 
         <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->type != 'product'): ?>
                
                    
        <?php unset($this->_sections['foo']);
$this->_sections['foo']['name'] = 'foo';
$this->_sections['foo']['start'] = (int)0;
$this->_sections['foo']['loop'] = is_array($_loop=$this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->qty) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['foo']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['foo']['show'] = true;
$this->_sections['foo']['max'] = $this->_sections['foo']['loop'];
if ($this->_sections['foo']['start'] < 0)
    $this->_sections['foo']['start'] = max($this->_sections['foo']['step'] > 0 ? 0 : -1, $this->_sections['foo']['loop'] + $this->_sections['foo']['start']);
else
    $this->_sections['foo']['start'] = min($this->_sections['foo']['start'], $this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] : $this->_sections['foo']['loop']-1);
if ($this->_sections['foo']['show']) {
    $this->_sections['foo']['total'] = min(ceil(($this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] - $this->_sections['foo']['start'] : $this->_sections['foo']['start']+1)/abs($this->_sections['foo']['step'])), $this->_sections['foo']['max']);
    if ($this->_sections['foo']['total'] == 0)
        $this->_sections['foo']['show'] = false;
} else
    $this->_sections['foo']['total'] = 0;
if ($this->_sections['foo']['show']):

            for ($this->_sections['foo']['index'] = $this->_sections['foo']['start'], $this->_sections['foo']['iteration'] = 1;
                 $this->_sections['foo']['iteration'] <= $this->_sections['foo']['total'];
                 $this->_sections['foo']['index'] += $this->_sections['foo']['step'], $this->_sections['foo']['iteration']++):
$this->_sections['foo']['rownum'] = $this->_sections['foo']['iteration'];
$this->_sections['foo']['index_prev'] = $this->_sections['foo']['index'] - $this->_sections['foo']['step'];
$this->_sections['foo']['index_next'] = $this->_sections['foo']['index'] + $this->_sections['foo']['step'];
$this->_sections['foo']['first']      = ($this->_sections['foo']['iteration'] == 1);
$this->_sections['foo']['last']       = ($this->_sections['foo']['iteration'] == $this->_sections['foo']['total']);
?>  

		       <?php if ($this->_tpl_vars['fcount'] == 10): ?>	
           <?php $this->assign('pageCount', $this->_tpl_vars['pageCount']+1); ?>  		 
          <?php endif; ?> 
		  <?php if ($this->_tpl_vars['fcount'] % 11 == 0): ?>	 
		    <?php $this->assign('pageCount', $this->_tpl_vars['pageCount']+1); ?>  
		  <?php endif; ?>
		  
     
         <div class="ingredient">
			<div class="wrap_item">
				<div class="cell">
			
			 <h3> <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->type == 'product'): ?>
			 <?php echo ((is_array($_tmp=$this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->product_name)) ? $this->_run_mod_handler('truncate', true, $_tmp, 35, "...", true) : smarty_modifier_truncate($_tmp, 35, "...", true)); ?>

			 <?php else: ?>
			 <?php echo ((is_array($_tmp=$this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->sandwich_name)) ? $this->_run_mod_handler('truncate', true, $_tmp, 35, "...", true) : smarty_modifier_truncate($_tmp, 35, "...", true)); ?>

			 <?php endif; ?>  
			  
			 </h3>
			 <?php $this->assign('sandwichDetails', ((is_array($_tmp=",")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->sandwich_details) : explode($_tmp, $this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->sandwich_details))); ?>
			 
			 <?php $this->assign('sandwichDet', ''); ?>
			 <?php $_from = $this->_tpl_vars['sandwichDetails']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['det'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['det']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['foo']):
        $this->_foreach['det']['iteration']++;
?>
			 
			  <?php if (($this->_foreach['det']['iteration']-1) == 0): ?>    
			      <?php $this->assign('sep', ""); ?>  
			       <?php else: ?>
			       <?php $this->assign('sep', ", "); ?>  
			    <?php endif; ?>  
			 
			  <?php if ($this->_tpl_vars['foo'] != ' '): ?>
			    <?php $this->assign('sandwichDet', ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['sandwichDet'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['sep']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['sep'])))) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['foo']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['foo']))); ?>  
			  <?php endif; ?>
             <?php endforeach; endif; unset($_from); ?>
			  
			 
			 
			 <p class="normal_desc"><?php echo ((is_array($_tmp=$this->_tpl_vars['sandwichDet'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 115, "...", true) : smarty_modifier_truncate($_tmp, 115, "...", true)); ?>
</p>
			 <p class="print_desc"><?php echo ((is_array($_tmp=$this->_tpl_vars['sandwichDet'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 140, "...", true) : smarty_modifier_truncate($_tmp, 140, "...", true)); ?>
</p>
			
			 <?php if ($this->_tpl_vars['order_item_detail'][$this->_sections['orderitemsdet']['index']]->toasted): ?> 
			  
			 <b>Toasted</b>
			 <?php endif; ?>
			 
			 </div>   
			</div>
         </div>
        <?php $this->assign('barcount', $this->_tpl_vars['barcount']+1); ?>
        
        <?php if ($this->_tpl_vars['sidebarcount'] > 9): ?>
			 <?php $this->assign('y', 10); ?>			  
			 <?php $this->assign('boxes', 10); ?>			  			 
		<?php else: ?>
			<?php $this->assign('y', 9); ?>				
		<?php endif; ?>	
        
        <?php if ($this->_tpl_vars['boxes'] == 9): ?>
        	<?php if ($this->_tpl_vars['boxescount'] == 8): ?> 
		 
				<?php $this->assign('boxescount', 0); ?>
			<?php endif; ?> 
		<?php endif; ?>
		
		<?php if ($this->_tpl_vars['boxes'] == 10): ?>
        	<?php if ($this->_tpl_vars['boxescount'] == 13): ?> 
				 
				<?php $this->assign('boxescount', 0); ?>
			<?php endif; ?> 
		<?php endif; ?>	
      
	  
     		
	   <?php if ($this->_tpl_vars['fcount'] == 10): ?>	
		    <!--<div class="page-break1"> &nbsp; &nbsp; &nbsp; &nbsp;</div>-->
		   
		<?php else: ?>  
		
		<?php if ($this->_tpl_vars['count_order'] > 21): ?>
	       <?php if ($this->_tpl_vars['fcount'] == $this->_tpl_vars['gtrcount']): ?>
		    <!--<div class="page-break2"> &nbsp; &nbsp; &nbsp; &nbsp;</div> -->
			<?php $this->assign('gtrcount', $this->_tpl_vars['gtrcount']+12); ?>
			<?php endif; ?>
		<?php endif; ?>
		<?php endif; ?>
         
         <?php $this->assign('sidebarcount', $this->_tpl_vars['sidebarcount']+1); ?>
         <?php $this->assign('boxescount', $this->_tpl_vars['boxescount']+1); ?>
		  <?php $this->assign('fcount', $this->_tpl_vars['fcount']+1); ?>
		  
		  
			<!--<?php if ($this->_tpl_vars['fcount'] % 10 == 0): ?>
			<?php $this->assign('fcount', $this->_tpl_vars['fcount']+1); ?>
				
				<div class="ingredient address_page <?php if ($this->_tpl_vars['is_delivery'] == 1): ?>  delivery_address <?php else: ?> pickup_address <?php endif; ?>">
		
					<div class="wrap_item">
						<div class="cell">
						   <h3><?php if ($this->_tpl_vars['address']->name): ?>  <?php echo $this->_tpl_vars['address']->name; ?>
 <?php endif; ?> </h3>
						   <h1 class="ordCount"><?php echo $this->_tpl_vars['count_order']; ?>
</h1>
						   
						   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->company)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <p><?php echo $this->_tpl_vars['address']->company; ?>
</p> <?php endif; ?>
						   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <p> <?php echo $this->_tpl_vars['address']->addline1; ?>
, <?php endif; ?> <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->street)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?> <?php echo $this->_tpl_vars['address']->street; ?>
, <?php endif; ?> <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) || ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->street)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?></p><?php endif; ?>
						   <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['address']->addline2)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp)))) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp))): ?><p><?php echo $this->_tpl_vars['address']->addline2; ?>
,  </p><?php endif; ?>
						   <p>New York, NY <?php echo $this->_tpl_vars['address']->zip; ?>
</p>
						   <?php if ($this->_tpl_vars['is_delivery'] == 1): ?>  <?php if ($this->_tpl_vars['address']->cross_streets): ?><p>(<?php echo $this->_tpl_vars['address']->cross_streets; ?>
)</p> <?php endif; ?>
						   <?php endif; ?>
						   <p><?php echo $this->_tpl_vars['address']->phone; ?>
</p>
						  						
						  
						</div>   
					</div>
				</div>
			
			<?php endif; ?>-->
         
         <?php endfor; endif; ?>
         <?php endif; ?>
             
              
          
         <?php endfor; endif; ?>
        
     
  
   
         
    
    </div>
      
  </div></td>
</tr>
</table>

  

  
  
  