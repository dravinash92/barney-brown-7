<?php /* Smarty version 2.6.25, created on 2019-11-07 08:51:41
         compiled from edit_delivery_user.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'edit_delivery_user.tpl', 6, false),array('modifier', 'in_array', 'edit_delivery_user.tpl', 25, false),)), $this); ?>
 <div class="container">
    <div class="user-account">
       <h1>EDIT DELIVERY ACCOUNT</h1>
    
		   <form action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/updateDeliveryUser" method="POST">
		   	 <?php unset($this->_sections['deliveryUser']);
$this->_sections['deliveryUser']['name'] = 'deliveryUser';
$this->_sections['deliveryUser']['start'] = (int)0;
$this->_sections['deliveryUser']['loop'] = is_array($_loop=count($this->_tpl_vars['delivery_user'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['deliveryUser']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['deliveryUser']['show'] = true;
$this->_sections['deliveryUser']['max'] = $this->_sections['deliveryUser']['loop'];
if ($this->_sections['deliveryUser']['start'] < 0)
    $this->_sections['deliveryUser']['start'] = max($this->_sections['deliveryUser']['step'] > 0 ? 0 : -1, $this->_sections['deliveryUser']['loop'] + $this->_sections['deliveryUser']['start']);
else
    $this->_sections['deliveryUser']['start'] = min($this->_sections['deliveryUser']['start'], $this->_sections['deliveryUser']['step'] > 0 ? $this->_sections['deliveryUser']['loop'] : $this->_sections['deliveryUser']['loop']-1);
if ($this->_sections['deliveryUser']['show']) {
    $this->_sections['deliveryUser']['total'] = min(ceil(($this->_sections['deliveryUser']['step'] > 0 ? $this->_sections['deliveryUser']['loop'] - $this->_sections['deliveryUser']['start'] : $this->_sections['deliveryUser']['start']+1)/abs($this->_sections['deliveryUser']['step'])), $this->_sections['deliveryUser']['max']);
    if ($this->_sections['deliveryUser']['total'] == 0)
        $this->_sections['deliveryUser']['show'] = false;
} else
    $this->_sections['deliveryUser']['total'] = 0;
if ($this->_sections['deliveryUser']['show']):

            for ($this->_sections['deliveryUser']['index'] = $this->_sections['deliveryUser']['start'], $this->_sections['deliveryUser']['iteration'] = 1;
                 $this->_sections['deliveryUser']['iteration'] <= $this->_sections['deliveryUser']['total'];
                 $this->_sections['deliveryUser']['index'] += $this->_sections['deliveryUser']['step'], $this->_sections['deliveryUser']['iteration']++):
$this->_sections['deliveryUser']['rownum'] = $this->_sections['deliveryUser']['iteration'];
$this->_sections['deliveryUser']['index_prev'] = $this->_sections['deliveryUser']['index'] - $this->_sections['deliveryUser']['step'];
$this->_sections['deliveryUser']['index_next'] = $this->_sections['deliveryUser']['index'] + $this->_sections['deliveryUser']['step'];
$this->_sections['deliveryUser']['first']      = ($this->_sections['deliveryUser']['iteration'] == 1);
$this->_sections['deliveryUser']['last']       = ($this->_sections['deliveryUser']['iteration'] == $this->_sections['deliveryUser']['total']);
?>
		         <div class="item-report">
		           <label>Name</label>
		          <input name="delivery_name" type="text" value="<?php echo $this->_tpl_vars['delivery_user'][$this->_sections['deliveryUser']['index']]['name']; ?>
">
		          <input type="hidden" name="hidden_uid" value="<?php echo $this->_tpl_vars['delivery_user'][$this->_sections['deliveryUser']['index']]['uid']; ?>
">
		          <label>Username</label>
		          <input name="delivery_user_uname" type="text" value="<?php echo $this->_tpl_vars['delivery_user'][$this->_sections['deliveryUser']['index']]['user_name']; ?>
">
		          <label>Password</label>
		          <input name="delivery_user_pwd" type="password">
		          <label>Confirm Password</label>
		          <input name="delivery_user_cpwd" type="password" value="">
		         <!--  <label>Assigned Stores</label> -->
		          <div class="store-location store-location-new">
		          	<ul>
			          <li><label>Assigned Stores</label></li><?php $this->assign('count', 1); ?>
			              <?php unset($this->_sections['options']);
$this->_sections['options']['name'] = 'options';
$this->_sections['options']['start'] = (int)0;
$this->_sections['options']['loop'] = is_array($_loop=count($this->_tpl_vars['pickupstores'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['options']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['options']['show'] = true;
$this->_sections['options']['max'] = $this->_sections['options']['loop'];
if ($this->_sections['options']['start'] < 0)
    $this->_sections['options']['start'] = max($this->_sections['options']['step'] > 0 ? 0 : -1, $this->_sections['options']['loop'] + $this->_sections['options']['start']);
else
    $this->_sections['options']['start'] = min($this->_sections['options']['start'], $this->_sections['options']['step'] > 0 ? $this->_sections['options']['loop'] : $this->_sections['options']['loop']-1);
if ($this->_sections['options']['show']) {
    $this->_sections['options']['total'] = min(ceil(($this->_sections['options']['step'] > 0 ? $this->_sections['options']['loop'] - $this->_sections['options']['start'] : $this->_sections['options']['start']+1)/abs($this->_sections['options']['step'])), $this->_sections['options']['max']);
    if ($this->_sections['options']['total'] == 0)
        $this->_sections['options']['show'] = false;
} else
    $this->_sections['options']['total'] = 0;
if ($this->_sections['options']['show']):

            for ($this->_sections['options']['index'] = $this->_sections['options']['start'], $this->_sections['options']['iteration'] = 1;
                 $this->_sections['options']['iteration'] <= $this->_sections['options']['total'];
                 $this->_sections['options']['index'] += $this->_sections['options']['step'], $this->_sections['options']['iteration']++):
$this->_sections['options']['rownum'] = $this->_sections['options']['iteration'];
$this->_sections['options']['index_prev'] = $this->_sections['options']['index'] - $this->_sections['options']['step'];
$this->_sections['options']['index_next'] = $this->_sections['options']['index'] + $this->_sections['options']['step'];
$this->_sections['options']['first']      = ($this->_sections['options']['iteration'] == 1);
$this->_sections['options']['last']       = ($this->_sections['options']['iteration'] == $this->_sections['options']['total']);
?>
			             
				          <li>
					           <span class="multi-left">
					           <input type="checkbox" id="checkbox-2-<?php echo $this->_tpl_vars['count']; ?>
" class="input-checkbox" name="store_id[]" value="<?php echo $this->_tpl_vars['pickupstores'][$this->_sections['options']['index']]['id']; ?>
" <?php if (((is_array($_tmp=$this->_tpl_vars['pickupstores'][$this->_sections['options']['index']]['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['sid_ary']) : in_array($_tmp, $this->_tpl_vars['sid_ary']))): ?> checked <?php endif; ?>>
					            <label for="checkbox-2-<?php echo $this->_tpl_vars['count']; ?>
" class="multisel-ckeck"></label>
					            </span> <span class="radio-text"><?php echo $this->_tpl_vars['pickupstores'][$this->_sections['options']['index']]['store_name']; ?>
</span> 
					       </li>
					       <?php $this->assign('count', $this->_tpl_vars['count']+1); ?>
				          <?php endfor; endif; ?>
       				 </ul>
		          </div>
		         	<br/><br/><br/>
		          <input type="submit" name="submit" value="UPDATE USER">
		           </div>
		           <?php endfor; endif; ?>
		       </form>
      
    </div>
  </div>

