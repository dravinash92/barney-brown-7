<?php /* Smarty version 2.6.25, created on 2020-09-30 06:54:57
         compiled from edit_store_account_data.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'edit_store_account_data.tpl', 72, false),array('modifier', 'count', 'edit_store_account_data.tpl', 415, false),)), $this); ?>
<div class="container">
    <div class="user-account">
       <h1>EDIT STORE ACCOUNT</h1>
       <form name="maincontents" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/updateStoreAccount" method="POST">
	   <div class="column column-1">
        <?php if ($this->_tpl_vars['isStore'] == 0): ?>
          <label>Store Name</label>
          <input type="text" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->store_name; ?>
" name="store_name">
          <label>Store Address Line One</label>
          <input type="text" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->address1; ?>
" name="address1">
          <label>Store Address Line Two</label>
          <input type="text" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->address2; ?>
" name="address2">
          <label>Store Zipcode</label>
          <input type="text" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->zip; ?>
" name="zip">
          <label>Username</label>
          <input type="text" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->username; ?>
" name="username" disabled>
          <label>Password</label>
          <input type="password" name="password">
          <label>Confirm Password</label>
          <input type="password" value="" name="con_password">
          <label>Reports Pin</label>
          <input type="text" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->report_pin; ?>
" name="report_pin">
     <span class="multi-left">
               <input type="checkbox" id="checkbox-2-0" class="input-checkbox" name="ip_checked" value="" <?php if ($this->_tpl_vars['store_accounts_data']->pickup_stores->restrict_ip != ""): ?> checked <?php endif; ?>>
          <label for="checkbox-2-0" class="multisel-ckeck"></label>
         </span><label class="restrict">Restrict IP Access to:</label>
          <input type="text" name="restrict_ip" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->restrict_ip; ?>
">
          
                   
           <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->id; ?>
" />
          
        
          <button type="button" class="save_store_account">SAVE STORE</button>
          <a class="cancel" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts">CANCEL</a>
		
		</div>
		
	    <!--    
      	 <div class="pick-up"  style="margin-left: 312px; width: 368px; height: 623px; margin-top: -578px;"> -->
        <?php else: ?>
          <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->id; ?>
" />
        <input type="hidden" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->store_name; ?>
" name="store_name">
        <input type="hidden" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->address1; ?>
" name="address1">
        <input type="hidden" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->address2; ?>
" name="address2">
        <input type="hidden" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->zip; ?>
" name="zip">
        <input type="hidden" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->username; ?>
" name="username" disabled>
        
         <!-- <div class="pick-up" style=" width: 368px; " > -->
        <?php endif; ?>
		<div class="column column-2">
		<div class="pick-up" style=" width: 368px; " >
         <h3>Pick-Up / Delivery Windows</h3>
          <ul>
           <li class="radio"></li>
           <li class="week">&nbsp;</li>
           <li class="from">From</li>
           <li class="to">To</li>
         </ul>
         
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="day0" value="Sunday" <?php if ($this->_tpl_vars['store_timings_day'][0] == 'Sunday' && $this->_tpl_vars['store_active'][0] == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-2-1" class="multisel-ckeck"></label>
              </span>
           </li>
      
           <li class="week">Sunday</li>
           <li class="from"> 
             <select name="day_open0">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][0] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
			</select>

           </li>
           <li class="to">
                        <select name="day_close0">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][0] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
          <input type="checkbox" <?php if ($this->_tpl_vars['pickup_active'][0] == 1): ?> checked <?php endif; ?> id="checkbox-0-pickup" class="input-checkbox" name="checkbox-0-pickup" value="1">
                <label for="checkbox-0-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['delivery_active'][0] == 1): ?> checked <?php endif; ?> id="checkbox-0-delivery" class="input-checkbox" name="checkbox-0-delivery" value="1">
                <label for="checkbox-0-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
          </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-2" class="input-checkbox" name="day1" value="Monday" <?php if ($this->_tpl_vars['store_timings_day'][1] == 'Monday' && $this->_tpl_vars['store_active'][1] == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-2-2" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Monday</li>
           <li class="from"> 
             <select name="day_open1">
                <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][1] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected  <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
             </select>
           </li>
           <li class="to">
             <select name="day_close1">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][1] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
          
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['pickup_active'][1] == 1): ?> checked <?php endif; ?> id="checkbox-1-pickup" class="input-checkbox" name="checkbox-1-pickup" value="1">
                <label for="checkbox-1-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['delivery_active'][1] == 1): ?> checked <?php endif; ?> id="checkbox-1-delivery" class="input-checkbox" name="checkbox-1-delivery" value="1">
                <label for="checkbox-1-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-3" class="input-checkbox" name="day2" value="Tuesday" <?php if ($this->_tpl_vars['store_timings_day'][2] == 'Tuesday' && $this->_tpl_vars['store_active'][2] == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-2-3" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Tuesday</li>
           <li class="from"> 
             <select name="day_open2">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][2] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close2">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][2] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['pickup_active'][2] == 1): ?> checked <?php endif; ?> id="checkbox-2-pickup" class="input-checkbox" name="checkbox-2-pickup" value="1">
                <label for="checkbox-2-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['delivery_active'][2] == 1): ?> checked <?php endif; ?> id="checkbox-2-delivery" class="input-checkbox" name="checkbox-2-delivery" value="1">
                <label for="checkbox-2-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-4" class="input-checkbox" name="day3" value="Wednesday" <?php if ($this->_tpl_vars['store_timings_day'][3] == 'Wednesday' && $this->_tpl_vars['store_active'][3] == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-2-4" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Wednesday</li>
           <li class="from"> 
             <select name="day_open3">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][3] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close3">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][3] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['pickup_active'][3] == 1): ?> checked <?php endif; ?> id="checkbox-3-pickup" class="input-checkbox" name="checkbox-3-pickup" value="1">
                <label for="checkbox-3-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['delivery_active'][3] == 1): ?> checked <?php endif; ?> id="checkbox-3-delivery" class="input-checkbox" name="checkbox-3-delivery" value="1">
                <label for="checkbox-3-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="day4" value="Thursday" <?php if ($this->_tpl_vars['store_timings_day'][4] == 'Thursday' && $this->_tpl_vars['store_active'][4] == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-2-5" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Thursday</li>
           <li class="from"> 
             <select name="day_open4">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][4] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close4">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][4] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
          
           <li class="deactdelvery" >
               <span class="multi-left" >
                  <input type="checkbox" <?php if ($this->_tpl_vars['pickup_active'][4] == 1): ?> checked <?php endif; ?> id="checkbox-4-pickup" class="input-checkbox" name="checkbox-4-pickup" value="1">
                <label for="checkbox-4-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['delivery_active'][4] == 1): ?> checked <?php endif; ?> id="checkbox-4-delivery" class="input-checkbox" name="checkbox-4-delivery" value="1">
                <label for="checkbox-4-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-6" class="input-checkbox" name="day5" value="Friday" <?php if ($this->_tpl_vars['store_timings_day'][5] == 'Friday' && $this->_tpl_vars['store_active'][5] == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-2-6" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Friday</li>
           <li class="from"> 
             <select name="day_open5">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][5] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close5">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][5] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
          
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['pickup_active'][5] == 1): ?> checked <?php endif; ?> id="checkbox-5-pickup" class="input-checkbox" name="checkbox-5-pickup" value="1">
                <label for="checkbox-5-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['delivery_active'][5] == 1): ?> checked <?php endif; ?> id="checkbox-5-delivery" class="input-checkbox" name="checkbox-5-delivery" value="1">
                <label for="checkbox-5-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-7" class="input-checkbox" name="day6" value="Saturday" <?php if ($this->_tpl_vars['store_timings_day'][6] == 'Saturday' && $this->_tpl_vars['store_active'][6] == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-2-7" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Saturday</li>
           <li class="from"> 
             <select name="day_open6">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][6] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close6">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][6] == ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M'))): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['pickup_active'][6] == 1): ?> checked <?php endif; ?> id="checkbox-6-pickup" class="input-checkbox" name="checkbox-6-pickup" value="1">
                <label for="checkbox-6-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($this->_tpl_vars['delivery_active'][6] == 1): ?> checked <?php endif; ?> id="checkbox-6-delivery" class="input-checkbox" name="checkbox-6-delivery" value="1">
                <label for="checkbox-6-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
            
         </ul>
         
         <!--
         <div class="deactivate">
            <ul>
              <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-8" class="input-checkbox" name="" value="">
                <label for="checkbox-2-8" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
              </li>
              <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-9" class="input-checkbox" name="" value="">
                <label for="checkbox-2-9" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
              </li>
            </ul>
         
         </div>
         -->
           <?php if ($this->_tpl_vars['isStore']): ?>
                 <button type="button" class="save_store_account">SAVE STORE</button>
          <a class="cancel" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts">CANCEL</a>
          <?php endif; ?>
       </div>
	   <div class="pick-up" style=" width: 368px; " >
         <h3>CLOSE SPECIFIC DAY</h3>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="specificDay" class="input-checkbox" name="specificday_active" value="1"  <?php if ($this->_tpl_vars['store_accounts_data']->pickup_stores->specificday_active == 1): ?>  checked <?php endif; ?>>
               <label for="specificDay" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Close Specific Day</li>
          </ul>
           <div style="clear:both;"></div>
          <ul>
           <li class="deactdelvery text-box-holder2" >
               <span class="multi-left">
                  <input name="specificday" id="search_delivery_date" type="text" class="text-box" name="specificDay" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->specificday; ?>
">
                  <a href="#" class="date-icon"></a>
               </span>
            </li>
         </ul>
         <div style="clear:both;"></div>
         <ul style="margin-top: 20px;">
           <li class="deactdelvery" >
           <label  class="multisel-ckeck">Message</label>
               <span class="multi-left">
                  <textarea rows="" cols="" id="specificday_message" name="specificday_message"><?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->specificday_message; ?>
</textarea>
               </span>
            </li>
         </ul>
         </div>
	   </div>
	   
	   <div class="column column-3">
	   
       <?php if ($this->_tpl_vars['isStore'] == 0): ?>
       <div class="zipcode-wrapper ten">
         <h3>ALLOWED ZIP CODES($10 MIN)</h3> 
           <div class="zip_wrap">
         <ul class="heading">
           <li class="zip-code">Zip Code</li>
           <li class="abbr">Abbr.</li>
           <li class="abbr">Del. Fee</li>
         </ul>
        <?php $_from = $this->_tpl_vars['store_accounts_data']->store_zipcodes; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
         <?php if (( $this->_tpl_vars['v']->min_order == 10 || $this->_tpl_vars['v']->min_order == 0 )): ?>
			<ul>
			   <li class="zip-code"><input type="text"  value="<?php echo $this->_tpl_vars['v']->zipcode; ?>
" name="zipcode<?php echo $this->_tpl_vars['k']+1; ?>
">
			    <input name="zipCodeId" type="hidden" value="<?php echo $this->_tpl_vars['v']->id; ?>
" />
			   </li>

			   <li class="abbr"><input  maxlength="3" type="text" value="<?php echo $this->_tpl_vars['v']->abbreviation; ?>
" name="abbreviation<?php echo $this->_tpl_vars['k']+1; ?>
">
			    <input type="hidden" name="minAmount<?php echo $this->_tpl_vars['k']+1; ?>
" class="minAmount" value="<?php echo $this->_tpl_vars['v']->min_order; ?>
"></li>
          <li class="delivery-fee"><input type="text" value="<?php echo $this->_tpl_vars['v']->delivery_fee; ?>
" name="deliveryfee<?php echo $this->_tpl_vars['k']+1; ?>
"></li>

			    <li class="zipItemrmv"><a class="removeZip" >Remove</a></li>
			</ul>
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</div>
		<input type="hidden" name="zipcount" id="zipcount" value="<?php echo count($this->_tpl_vars['store_accounts_data']->store_zipcodes); ?>
" />		
       
        <div style="clear:both"><a class="add zip_add" data-minamount="10" href="javascript:void(0)" style="">add</a> </div>
         
       </div>
     <div class="zipcode-wrapper hundred" >
         <h3>ALLOWED ZIP CODES($100 MIN)</h3> 
           <div class="zip_wrap">
         <ul class="heading">
           <li class="zip-code">Zip Code</li>
           <li class="abbr">Abbr.</li>
           <li class="abbr">Del. Fee</li>
         </ul>
        <?php $_from = $this->_tpl_vars['store_accounts_data']->store_zipcodes; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
        <?php if ($this->_tpl_vars['v']->min_order == 100): ?>
			<ul>
			   <li class="zip-code"><input type="text"  value="<?php echo $this->_tpl_vars['v']->zipcode; ?>
" name="zipcode<?php echo $this->_tpl_vars['k']+1; ?>
">
			    <input name="zipCodeId" type="hidden" value="<?php echo $this->_tpl_vars['v']->id; ?>
" />
			   </li>
			   <li class="abbr"><input type="text" maxlength="3" value="<?php echo $this->_tpl_vars['v']->abbreviation; ?>
" name="abbreviation<?php echo $this->_tpl_vars['k']+1; ?>
">
			   <input type="hidden" name="minAmount<?php echo $this->_tpl_vars['k']+1; ?>
" class="minAmount" value="<?php echo $this->_tpl_vars['v']->min_order; ?>
"></li>
         <li class="delivery-fee"><input type="text" value="<?php echo $this->_tpl_vars['v']->delivery_fee; ?>
" name="deliveryfee<?php echo $this->_tpl_vars['k']+1; ?>
"></li>
			    <li class="zipItemrmv"><a class="removeZip" >Remove</a></li>
			</ul>
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</div>
		<input type="hidden" name="zipcount" id="zipcounthundred" value="<?php echo count($this->_tpl_vars['store_accounts_data']->store_zipcodes); ?>
" />	
        <div style="clear:both"><a class="add zip_add_hundred" data-minamount="100" href="javascript:void(0)" style="">add</a> </div>
         
       </div>     
         <?php endif; ?>
		 
		</div> 
       </form>
    </div>
  </div>
 