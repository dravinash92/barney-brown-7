<?php
/* Smarty version 3.1.39, created on 2021-03-31 04:17:06
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\sandwich-bread.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_60643002688d42_59498402',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e57d733c608569e456d31cddaaf7f99ef904b6f4' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\sandwich-bread.tpl',
      1 => 1600125374,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60643002688d42_59498402 (Smarty_Internal_Template $_smarty_tpl) {
?>  <h2>Choose Your Bread</h2>
              <h3>CHOOSE FROM OUR SELECTION OF NEW YORK'S AND NEW JERSEY'S FINEST.</h3>
              <div class="main-category-list">
              
         
              
                <ul>             
              

                <?php
$__section_options_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['BREAD_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_options_0_start = min(0, $__section_options_0_loop);
$__section_options_0_total = min(($__section_options_0_loop - $__section_options_0_start), $__section_options_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_options'] = new Smarty_Variable(array());
if ($__section_options_0_total !== 0) {
for ($__section_options_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] = $__section_options_0_start; $__section_options_0_iteration <= $__section_options_0_total; $__section_options_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']++){
?>
                
                  <li>
                    <input data-shape="<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['bread_shape'];?>
" data-bread_type="<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['bread_type'];?>
" data-item_image_sliced="<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['item_image_sliced'];?>
" data-type="replace" data-id="<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['id'];?>
" data-itemname="<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['item_name'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['item_price'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['item_image'];?>
" type="radio" name="radiog_lite" id="radio<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)+1;?>
" class="css-checkbox" />
                    <label for="radio<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)+1;?>
" class="css-label"><?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['item_name'];?>
</label>
                  </li>
                <?php
}
}
?>                  
                </ul>
              </div>
<?php }
}
