<?php /* Smarty version 2.6.25, created on 2020-09-15 07:47:03
         compiled from menu_items.tpl */ ?>
<div class="container">
      <div class="store-accounts">
        <h1>MENU ITEMS</h1>
             
        <h2>CUSTOM</h2>       
        
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody><tr>
           <th width="89%">SANDWICHES</th>
           <th width="11%"># OPTIONS</th>
         </tr>
         
         <tr>
           <td>Breads</td>
           <td class="align-right"><?php echo $this->_tpl_vars['counts']['BREAD']; ?>
</td>
         </tr>
         
         <tr class="even">
           <td>Meats</td>
           <td class="align-right"><?php echo $this->_tpl_vars['counts']['PROTEIN']; ?>
</td>
         </tr>
		 
		  <tr>
           <td>Cheeses</td>
           <td class="align-right"><?php echo $this->_tpl_vars['counts']['CHEESE']; ?>
</td>
         </tr>
		 
		 <tr class="even">
           <td>Toppings</td>
           <td class="align-right"><?php echo $this->_tpl_vars['counts']['TOPPINGS']; ?>
</td>
         </tr>
		 
		  <tr>
           <td>Condiments</td>
           <td class="align-right"><?php echo $this->_tpl_vars['counts']['CONDIMENTS']; ?>
</td>
         </tr>
         
       </tbody></table>
       <a class="edit" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/custom">EDIT</a>
       <a class="edit" style="margin-left:10px;" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/edit_options">EDIT OPTIONS</a>
        <h2>STANDARD</h2>       
        <a class="category" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/newStandardCategory">NEW CATEGORY</a>
        <p>
          <span>View </span>
          <select>
                 <option value="">Current</option>
                 <option value=""></option>
          </select> 
        </p>
       
       <?php $_from = $this->_tpl_vars['dataList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j'] => $this->_tpl_vars['list']):
?>

      <div class="store-accounts">
      
   <form name="myForm" method="post" id="myForm" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/updateStandardCategoryPriority" > 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th width="10%" height="33">LIVE</th>
          <th width="16%">PRTY</th>
          <th width="52%"><?php echo $this->_tpl_vars['list']['name']; ?>
</th>
		   <th width="9%">PRICE</th>
		    <th width="4%"></th>
		    <th width="9%"><a style="color: #deb885;text-decoration:underline;" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/deleteStandardCategory/?id=<?php echo $this->_tpl_vars['list']['id']; ?>
" onclick="return confirm('Are you sure you want to delete?')" class="deletecategory">Delete</a></th>

        </tr>
       
<?php $_from = $this->_tpl_vars['list']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['data']):
?>
     
        <tr class="<?php if ($this->_tpl_vars['k']%2 == 0): ?>even<?php endif; ?>">
          <td width="10%" align="center">
          <span class="multi-left">
         
               <input type="checkbox" <?php if ($this->_tpl_vars['data']['live']): ?> checked="checked" <?php endif; ?> name="livecheckbox<?php echo $this->_tpl_vars['data']['id']; ?>
" class="input-checkbox" id="checkbox-<?php echo $this->_tpl_vars['j']; ?>
-<?php echo $this->_tpl_vars['k']; ?>
">
             <label class="multisel-ckeck" for="checkbox-<?php echo $this->_tpl_vars['j']; ?>
-<?php echo $this->_tpl_vars['k']; ?>
"></label>
            </span>
          </td>
          <td width="16%"><input type="text" class="field" name="priority[]" value="<?php echo $this->_tpl_vars['data']['priority']; ?>
"></td>
          <td width="52%"><a href="#"><?php echo $this->_tpl_vars['data']['product_name']; ?>
</a></td>
          <td width="9%"><a href="#"><?php echo '$'; ?>
<?php echo $this->_tpl_vars['data']['product_price']; ?>
</a></td>
          <td width="4%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/editStandardProducts/<?php echo $this->_tpl_vars['data']['id']; ?>
">edit</a></td>
          <td width="9%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/deleteStandardCategoryProducts/<?php echo $this->_tpl_vars['data']['id']; ?>
" onclick="return confirm('Are you sure you want to delete?')">delete</a></td>
        </tr>
  <input type="hidden" name="hidden_standard_ids[]" value="<?php echo $this->_tpl_vars['data']['id']; ?>
">

<?php endforeach; endif; unset($_from); ?>
      </tbody></table>  
     

       <a href="#" onclick="document.getElementById('myForm').submit();" class="update update_priority">update</a>
      
      <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/newStandardProducts/<?php echo $this->_tpl_vars['list']['id']; ?>
" class="add-user">ADD NEW ITEM</a>
   </form>       
    </div>

<?php endforeach; endif; unset($_from); ?>



<!------------------------------------------Catering items loop--------------------------------------------------->
        <h2>CATERING</h2>       
        <a class="category" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/newStandardCategory">NEW CATEGORY</a>
        <p>
          <span>View </span>
          <select>
                 <option value="">Current</option>
                 <option value=""></option>
          </select> 
        </p>
       
       <?php $_from = $this->_tpl_vars['cateringdataList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j'] => $this->_tpl_vars['list']):
?>

      <div class="store-accounts">
      
   <form name="myFormCatering" method="post" id="myForm" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/updateStandardCategoryPriority" > 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th width="10%" height="33">LIVE</th>
          <th width="16%">PRTY</th>
          <th width="52%"><?php echo $this->_tpl_vars['list']['name']; ?>
</th>
		   <th width="9%">PRICE</th>
		    <th width="4%"></th>
		    <th width="9%"><a style="color: #deb885;text-decoration:underline;" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/deleteStandardCategory/?id=<?php echo $this->_tpl_vars['list']['id']; ?>
" onclick="return confirm('Are you sure you want to delete?')" class="deletecategory">Delete</a></th>

        </tr>
       
<?php $_from = $this->_tpl_vars['list']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['data']):
?>
     
        <tr class="<?php if ($this->_tpl_vars['k']%2 == 0): ?>even<?php endif; ?>">
          <td width="10%" align="center">
          <span class="multi-left">
         
               <input type="checkbox" <?php if ($this->_tpl_vars['data']['live']): ?> checked="checked" <?php endif; ?> name="livecheckbox<?php echo $this->_tpl_vars['data']['id']; ?>
" class="input-checkbox" id="checkboxcat-<?php echo $this->_tpl_vars['j']; ?>
-<?php echo $this->_tpl_vars['k']; ?>
">
             <label class="multisel-ckeck" for="checkboxcat-<?php echo $this->_tpl_vars['j']; ?>
-<?php echo $this->_tpl_vars['k']; ?>
"></label>
            </span>
          </td>
          <td width="16%"><input type="text" class="field" name="priority[]" value="<?php echo $this->_tpl_vars['data']['priority']; ?>
"></td>
          <td width="52%"><a href="#"><?php echo $this->_tpl_vars['data']['product_name']; ?>
</a></td>
          <td width="9%"><a href="#"><?php echo '$'; ?>
<?php echo $this->_tpl_vars['data']['product_price']; ?>
</a></td>
          <td width="4%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/editStandardProducts/<?php echo $this->_tpl_vars['data']['id']; ?>
">edit</a></td>
          <td width="9%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/deleteStandardCategoryProducts/<?php echo $this->_tpl_vars['data']['id']; ?>
" onclick="return confirm('Are you sure you want to delete?')">delete</a></td>
        </tr>
  <input type="hidden" name="hidden_standard_ids[]" value="<?php echo $this->_tpl_vars['data']['id']; ?>
">

<?php endforeach; endif; unset($_from); ?>
      </tbody></table>  
     

       <a href="#" onclick="document.getElementById('myFormCatering').submit();" class="update update_priority">update</a>
      
      <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/newStandardProducts/<?php echo $this->_tpl_vars['list']['id']; ?>
" class="add-user">ADD NEW ITEM</a>
   </form>       
    </div>

<?php endforeach; endif; unset($_from); ?>
   
  </div>
  
</div>  