<?php /* Smarty version 2.6.25, created on 2020-09-15 06:35:05
         compiled from customer_profile.tpl */ ?>

<div class="container">
      <div class="customer-profile">
        <h1><?php echo $this->_tpl_vars['udata']['first_name']; ?>
  <?php echo $this->_tpl_vars['udata']['last_name']; ?>
</h1>
        <a class="create-new-order" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
neworder/addtocart/<?php echo $this->_tpl_vars['UID']; ?>
" >Create new Order</a>
        <nav>
          <ul class="admin_customer_tabs">
            <li><a href="#" class="active">PROFILE</a></li>
            <li><a href="#">ORDER HISTORY</a></li>
          <!--  <li><a href="#">SAVED MEALS</a></li>-->
           <li><a href="#">SAVED ADDRESSES</a></li> 
            <li><a href="#">SAVED BILLING</a></li>
            <!--<li><a href="#">MEAL REMINDER</a></li>-->
          </ul>
        </nav>
        
     <div class="profile-wrapper">        
        <input type="hidden" name="hiduid" value="<?php echo $this->_tpl_vars['UID']; ?>
" />
      <div class="ajxwrp"> 
		<h3>Profile</h3>
        <div class="account">
          <h4>ACCOUNT INFO</h4>
         
          <ul>
            <li>
               <label>Name: <span><?php echo $this->_tpl_vars['udata']['first_name']; ?>
  <?php echo $this->_tpl_vars['udata']['last_name']; ?>
</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
            <li>
               <label> Email Address: <a href="mailto:<?php echo $this->_tpl_vars['udata']['username']; ?>
"><span><?php echo $this->_tpl_vars['udata']['username']; ?>
<span></a></label>
               <a href="#" class="edit">EDIT</a>
            </li>
             <li>
               <label>Password: <span>xxxxxxx</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
            <!--
             <li>
               <label>Phone: <span><?php echo $this->_tpl_vars['udata']['phone']; ?>
</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
             <li>
               <label>Company: <span><?php echo $this->_tpl_vars['udata']['company']; ?>
</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>-->
          </ul>
        </div>
        <!--
        <div class="tell-us">
          <h4>TELL US ABOUT YOUR EXPERIENCE</h4>
           <form id="user_review">
          <ul>
            <li>
               <label>How did you hear about us?  </label>
               <div class="bgforSelect">
                <select class="wid-input" name="how_know">
                  <option value="">Choose One</option>
                  
                  
                  <?php unset($this->_sections['users_review_content']);
$this->_sections['users_review_content']['name'] = 'users_review_content';
$this->_sections['users_review_content']['loop'] = is_array($_loop=$this->_tpl_vars['user_review_contents']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['users_review_content']['show'] = true;
$this->_sections['users_review_content']['max'] = $this->_sections['users_review_content']['loop'];
$this->_sections['users_review_content']['step'] = 1;
$this->_sections['users_review_content']['start'] = $this->_sections['users_review_content']['step'] > 0 ? 0 : $this->_sections['users_review_content']['loop']-1;
if ($this->_sections['users_review_content']['show']) {
    $this->_sections['users_review_content']['total'] = $this->_sections['users_review_content']['loop'];
    if ($this->_sections['users_review_content']['total'] == 0)
        $this->_sections['users_review_content']['show'] = false;
} else
    $this->_sections['users_review_content']['total'] = 0;
if ($this->_sections['users_review_content']['show']):

            for ($this->_sections['users_review_content']['index'] = $this->_sections['users_review_content']['start'], $this->_sections['users_review_content']['iteration'] = 1;
                 $this->_sections['users_review_content']['iteration'] <= $this->_sections['users_review_content']['total'];
                 $this->_sections['users_review_content']['index'] += $this->_sections['users_review_content']['step'], $this->_sections['users_review_content']['iteration']++):
$this->_sections['users_review_content']['rownum'] = $this->_sections['users_review_content']['iteration'];
$this->_sections['users_review_content']['index_prev'] = $this->_sections['users_review_content']['index'] - $this->_sections['users_review_content']['step'];
$this->_sections['users_review_content']['index_next'] = $this->_sections['users_review_content']['index'] + $this->_sections['users_review_content']['step'];
$this->_sections['users_review_content']['first']      = ($this->_sections['users_review_content']['iteration'] == 1);
$this->_sections['users_review_content']['last']       = ($this->_sections['users_review_content']['iteration'] == $this->_sections['users_review_content']['total']);
?> 
                   <option value="<?php echo $this->_tpl_vars['user_review_contents'][$this->_sections['users_review_content']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['user_review']['how_know'] == $this->_tpl_vars['user_review_contents'][$this->_sections['users_review_content']['index']]['id']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['user_review_contents'][$this->_sections['users_review_content']['index']]['review_text']; ?>
</option>
                   <?php endfor; endif; ?>
                   </select>
              </div>
            </li>
           <li>
               <label>How would you rate your ordering experience?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_order" >
                  <option value="">Choose One</option>

                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 1): ?> selected="selected" <?php endif; ?>  value="1">1</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 2): ?> selected="selected" <?php endif; ?>  value="2">2</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 3): ?> selected="selected" <?php endif; ?>  value="3">3</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 4): ?> selected="selected" <?php endif; ?>  value="4">4</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 5): ?> selected="selected" <?php endif; ?>  value="5">5</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 6): ?> selected="selected" <?php endif; ?>  value="6">6</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 7): ?> selected="selected" <?php endif; ?>  value="7">7</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 8): ?> selected="selected" <?php endif; ?>  value="8">8</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 9): ?> selected="selected" <?php endif; ?>  value="9">9</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_order'] == 10): ?> selected="selected" <?php endif; ?>  value="10">10</option>
                
                </select>
              </div>
            </li>
             <li>
               <label>How would you rate your customer service experience?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_customer_service" >
                  <option value="">Choose One</option>
                
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 1): ?> selected="selected" <?php endif; ?>  value="1">1</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 2): ?> selected="selected" <?php endif; ?>  value="2">2</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 3): ?> selected="selected" <?php endif; ?>  value="3">3</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 4): ?> selected="selected" <?php endif; ?>  value="4">4</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 5): ?> selected="selected" <?php endif; ?>  value="5">5</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 6): ?> selected="selected" <?php endif; ?>  value="6">6</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 7): ?> selected="selected" <?php endif; ?>  value="7">7</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 8): ?> selected="selected" <?php endif; ?>  value="8">8</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 9): ?> selected="selected" <?php endif; ?>  value="9">9</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_customer_service'] == 10): ?> selected="selected" <?php endif; ?>  value="10">10</option>
                
                
                </select>
              </div>
            </li>
            
            <li>
               <label>How would you rate the quality of your food?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_food" >
                  <option value="">Choose One</option>
                 
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 1): ?> selected="selected" <?php endif; ?>  value="1">1</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 2): ?> selected="selected" <?php endif; ?>  value="2">2</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 3): ?> selected="selected" <?php endif; ?>  value="3">3</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 4): ?> selected="selected" <?php endif; ?>  value="4">4</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 5): ?> selected="selected" <?php endif; ?>  value="5">5</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 6): ?> selected="selected" <?php endif; ?>  value="6">6</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 7): ?> selected="selected" <?php endif; ?>  value="7">7</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 8): ?> selected="selected" <?php endif; ?>  value="8">8</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 9): ?> selected="selected" <?php endif; ?>  value="9">9</option>
                <option <?php if ($this->_tpl_vars['user_review']['rating_food'] == 10): ?> selected="selected" <?php endif; ?>  value="10">10</option>
                
                
                </select>
              </div>
            </li>
            <li>
				<label>What can we do to be better?</label>
				<?php unset($this->_sections['reviewmessages']);
$this->_sections['reviewmessages']['name'] = 'reviewmessages';
$this->_sections['reviewmessages']['loop'] = is_array($_loop=$this->_tpl_vars['user_review_messages']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['reviewmessages']['show'] = true;
$this->_sections['reviewmessages']['max'] = $this->_sections['reviewmessages']['loop'];
$this->_sections['reviewmessages']['step'] = 1;
$this->_sections['reviewmessages']['start'] = $this->_sections['reviewmessages']['step'] > 0 ? 0 : $this->_sections['reviewmessages']['loop']-1;
if ($this->_sections['reviewmessages']['show']) {
    $this->_sections['reviewmessages']['total'] = $this->_sections['reviewmessages']['loop'];
    if ($this->_sections['reviewmessages']['total'] == 0)
        $this->_sections['reviewmessages']['show'] = false;
} else
    $this->_sections['reviewmessages']['total'] = 0;
if ($this->_sections['reviewmessages']['show']):

            for ($this->_sections['reviewmessages']['index'] = $this->_sections['reviewmessages']['start'], $this->_sections['reviewmessages']['iteration'] = 1;
                 $this->_sections['reviewmessages']['iteration'] <= $this->_sections['reviewmessages']['total'];
                 $this->_sections['reviewmessages']['index'] += $this->_sections['reviewmessages']['step'], $this->_sections['reviewmessages']['iteration']++):
$this->_sections['reviewmessages']['rownum'] = $this->_sections['reviewmessages']['iteration'];
$this->_sections['reviewmessages']['index_prev'] = $this->_sections['reviewmessages']['index'] - $this->_sections['reviewmessages']['step'];
$this->_sections['reviewmessages']['index_next'] = $this->_sections['reviewmessages']['index'] + $this->_sections['reviewmessages']['step'];
$this->_sections['reviewmessages']['first']      = ($this->_sections['reviewmessages']['iteration'] == 1);
$this->_sections['reviewmessages']['last']       = ($this->_sections['reviewmessages']['iteration'] == $this->_sections['reviewmessages']['total']);
?>
				<p><?php echo $this->_tpl_vars['user_review_messages'][$this->_sections['reviewmessages']['index']]['message']; ?>
</p> <br>
				<?php endfor; endif; ?>
				</li>
            <li>
				<input type="hidden" name="uid" value="<?php echo $this->_tpl_vars['udata']['uid']; ?>
" />
				<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user_review']['id']; ?>
" />
				<input type="button" class="submit savereview" name="savereview"  value="Submit" />
            </li>
          </ul>
          </form>
        </div>
        -->
         </div>         
     
     </div>
    
    </div>
     
  </div>