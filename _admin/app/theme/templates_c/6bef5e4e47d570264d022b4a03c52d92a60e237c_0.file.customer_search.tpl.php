<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:37:28
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\customer_search.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c8408175b10_34421216',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6bef5e4e47d570264d022b4a03c52d92a60e237c' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\customer_search.tpl',
      1 => 1601445545,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c8408175b10_34421216 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
      <div class="customers">
      
       <h1>CUSTOMERS</h1>
       
       <form class="new-order-form" id="new-order-form" action="" method="post">
       	<div class="text-box-holder">
          <label>First Name</label>
          <input name="customername" id="customernameinput" type="text" class="main-text-box common-text-width">
          <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
customer/index/" id="customername" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Last Name</label>
          <input name="lastname" type="text" id="lastnameinput" class="main-text-box common-text-width">
          <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
customer/index/" id="lastname" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Phone Number</label>
          <input name="ph1" type="text" class="main-text-box phone-text-width">
          <input name="ph2" type="text" class="main-text-box phone-text-width">
          <input name="ph3" type="text" class="main-text-box phone-text-width">
          <input name="ph4" type="text" class="main-text-box phone-text-width">
        <!--<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
customer/index/" class="arrow" id="phonesearch">&nbsp;</a>-->
        </div>
        <div class="text-box-holder">
          <label>Email Address</label>
          <input name="customeremail" type="text" id="customeremailinput" class="main-text-box common-text-width">
          <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
customer/index/" id="customeremail" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Company (Optional)</label>
          <input name="company" type="text" class="main-text-box common-text-width">
          <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
customer/index/" id="customercompany" class="arrow">&nbsp;</a>
        </div>
         <div class="text-box-holder">
       <input type="submit" class="new-account createaccount" value="New Account"/>
        </div>
            <div class="text-box-holder"> <span class="rerror_msg"></span> </div>
       </form>
       
       <!--<a href="javascript:void(0);"  class="new-account createaccount">New Account</a>-->
                    <input name="total_item_count" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['total_item_count']->value;?>
" />
		<input type="hidden" name="search_condition" value="<?php echo $_smarty_tpl->tpl_vars['search_condition']->value;?>
" class="search_condition" />
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminCustomerList">
          <tbody><tr>
            <th width="22%">CUSTOMER NAME</th>
            <th width="32%">EMAIL</th>
            <th width="18%">PHONE</th>
            <th width="15%">ORDERS</th>
            <th width="13%">SALES TOTAL</th>
          </tr>
          <?php
$__section_users_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['users_data']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_users_0_total = $__section_users_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_users'] = new Smarty_Variable(array());
if ($__section_users_0_total !== 0) {
for ($__section_users_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] = 0; $__section_users_0_iteration <= $__section_users_0_total; $__section_users_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']++){
?> 
   		 <tr>
            <td><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
customer/profile/<?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['uid'];?>
"><?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['last_name'];?>
</a></td>
            <td><?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['username'];?>
</td>
            <td></td>
            <td><?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['sandwich_count'];?>
</td>
            <td>$<?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['total_price'];?>
</td>
          </tr>
		<?php
}
}
?>  
		  
       </tbody></table>
<div class="loadMoreCustomer" style="display:none;text-align:center"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/star.png" alt="Loading" /> </div>

    </div>
     
  </div>
  
<style>
.register {
    border: 1px solid red !important;
}

.rerror_msg {
    color: red;
    font-size: 12px;
}
.loadMoreCustomer {
    float: left;
    display: block;
    width: 100%;
    margin: 20px auto 0;
}


</style>
<?php echo '<script'; ?>
>
$(document).ready(function(){
    $(window).scroll(function(){
if($(window).scrollTop() == $(document).height() - $(window).height() && $(".search_condition").val()==""){

                setTimeout(function() {
                    $('.loadMoreCustomer').show();
        $current_count = $('.adminCustomerList tr').length;
		//alert($current_count);
        $total_items = $('input[name="total_item_count"]').val();
        

        if (parseInt($current_count) < parseInt($total_items)) {
            $.ajax({
                type: "POST",
                data: {
                	"count": $current_count,
                   
                },
                url: SITE_URL + 'customer/loadmoreCustomer',
                success: function(e) {
                    $('.adminCustomerList tr:last').after(e);
					$('.loadMoreCustomer').hide();
                }
            });
        } else {
            $('.loadMoreCustomer').hide();
        }

                }, 300);
            }    });
});
<?php echo '</script'; ?>
>
  
<?php }
}
