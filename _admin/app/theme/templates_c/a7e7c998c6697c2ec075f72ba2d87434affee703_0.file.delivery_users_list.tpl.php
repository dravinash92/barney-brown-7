<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:37:04
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\delivery_users_list.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c83f07e4e30_94954051',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a7e7c998c6697c2ec075f72ba2d87434affee703' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\delivery_users_list.tpl',
      1 => 1600125378,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c83f07e4e30_94954051 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <div class="admin-user-account showdel-head">
       <h1>DELIVERY ACCOUNTS</h1>
    <form class="showdel-form" >
      <!-- <form method="post" class="showdel-form" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/deliveriesByDate"> -->
            <div class="input-wrap_date">
        <div class="input-main">
        <input type="hidden" name="uid" value="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
">
             <label>From</label>
          <input type="text" class="expiration" placeholder="Select date" name="from_date" id="from_date" value="<?php echo $_smarty_tpl->tpl_vars['from_date']->value;?>
"> 

          <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/calendar.png"></a>
          

            <select name="from_time" id="from_time">
        <?php echo $_smarty_tpl->tpl_vars['fromTimes']->value;?>

             </select>
           </div>
           <div>
             <label>To</label>
          <input type="text" class="expiration" placeholder="Select date" name="to_date" id="to_date" value="<?php echo $_smarty_tpl->tpl_vars['to_date']->value;?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/calendar.png"></a>
            <select class="to_time" id="to_time">
        <?php echo $_smarty_tpl->tpl_vars['toTimes']->value;?>
        
             </select>
           </div>
             <button id="searchDelUser" type="button"><img width="26" height="26" alt="" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/link.png"></button>
             </div>
           </form>
      <div id="ajaxList"></div>  
      <div id="delUserList"> 
       <form id="updateDeliveryUsersLive" method="post" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/updateDeliveryUsersLive">
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="7%">&nbsp;</th>
          <th>USER</th>
          <th>ASSIGNED STORES</th>
          <th># DELIVERIES</th>
          <th> ORDER TOTALS</th>
          <th colspan="2"></th>
        </tr>
      <?php $_smarty_tpl->_assignInScope('key', 1);?>
        <?php
$__section_options_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['delivery_users']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_options_0_start = min(0, $__section_options_0_loop);
$__section_options_0_total = min(($__section_options_0_loop - $__section_options_0_start), $__section_options_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_options'] = new Smarty_Variable(array());
if ($__section_options_0_total !== 0) {
for ($__section_options_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] = $__section_options_0_start; $__section_options_0_iteration <= $__section_options_0_total; $__section_options_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']++){
?>
        <tr>
          <td width="7%" align="center">
              <span class="multi-left">
               <input type="checkbox" id="checkbox-<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['uid'];?>
" class="input-checkbox" name="live[<?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['uid'];?>
]" value="1" <?php if ($_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['live'] == 1) {?> checked <?php }?>>
               <label for="checkbox-<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['uid'];?>
" class="multisel-ckeck"></label>
              </span>
          </td>
          <td width="15%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
reports/showDeliveriesDetails/<?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['uid'];?>
"><?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['name'];?>
</a></td>
          <td width="18%"><?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['store_names'];?>
</td>
          <td width="10%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
reports/showDeliveriesDetails/<?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['uid'];?>
"><?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['deliveries'];?>
</a></td>

          <td width="10%"><?php if ($_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['totals'] != '') {?> $ <?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['totals'];?>
 <?php } else { ?>$ 0.00<?php }?></td>
          
          <td width="5%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/editDeliveryUser/<?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['uid'];?>
">edit</a></td>
          <td width="5%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/deleteDeliveryUser/<?php echo $_smarty_tpl->tpl_vars['delivery_users']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['uid'];?>
">delete</a></td>
        </tr>
         <?php $_smarty_tpl->_assignInScope('key', $_smarty_tpl->tpl_vars['key']->value+1);?>
       <?php
}
}
?>

      </tbody></table>
      <a class="update" href="#" onclick="document.getElementById('updateDeliveryUsersLive').submit()">update</a>
      <a class="add-user" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/delivery_users/">ADD NEW USER</a>
      </form></div> 

    </div>
  </div>
<?php }
}
