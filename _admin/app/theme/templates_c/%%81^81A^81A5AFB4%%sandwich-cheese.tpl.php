<?php /* Smarty version 2.6.25, created on 2020-09-16 03:13:49
         compiled from sandwich-cheese.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'sandwich-cheese.tpl', 9, false),)), $this); ?>

  <h2> Choose Your Cheese </h2>
  <h3><!--  INCLUDES ONE PORTION OF CHEESE. <br> $1.00 PER EXTRA CHEESE. --></h3>
  <div class="protein-select-wrapper scroll-bar " style="position: relative; overflow: visible;">
  
  <ul>
                

                <?php unset($this->_sections['options']);
$this->_sections['options']['name'] = 'options';
$this->_sections['options']['start'] = (int)0;
$this->_sections['options']['loop'] = is_array($_loop=count($this->_tpl_vars['CHEESE_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['options']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['options']['show'] = true;
$this->_sections['options']['max'] = $this->_sections['options']['loop'];
if ($this->_sections['options']['start'] < 0)
    $this->_sections['options']['start'] = max($this->_sections['options']['step'] > 0 ? 0 : -1, $this->_sections['options']['loop'] + $this->_sections['options']['start']);
else
    $this->_sections['options']['start'] = min($this->_sections['options']['start'], $this->_sections['options']['step'] > 0 ? $this->_sections['options']['loop'] : $this->_sections['options']['loop']-1);
if ($this->_sections['options']['show']) {
    $this->_sections['options']['total'] = min(ceil(($this->_sections['options']['step'] > 0 ? $this->_sections['options']['loop'] - $this->_sections['options']['start'] : $this->_sections['options']['start']+1)/abs($this->_sections['options']['step'])), $this->_sections['options']['max']);
    if ($this->_sections['options']['total'] == 0)
        $this->_sections['options']['show'] = false;
} else
    $this->_sections['options']['total'] = 0;
if ($this->_sections['options']['show']):

            for ($this->_sections['options']['index'] = $this->_sections['options']['start'], $this->_sections['options']['iteration'] = 1;
                 $this->_sections['options']['iteration'] <= $this->_sections['options']['total'];
                 $this->_sections['options']['index'] += $this->_sections['options']['step'], $this->_sections['options']['iteration']++):
$this->_sections['options']['rownum'] = $this->_sections['options']['iteration'];
$this->_sections['options']['index_prev'] = $this->_sections['options']['index'] - $this->_sections['options']['step'];
$this->_sections['options']['index_next'] = $this->_sections['options']['index'] + $this->_sections['options']['step'];
$this->_sections['options']['first']      = ($this->_sections['options']['iteration'] == 1);
$this->_sections['options']['last']       = ($this->_sections['options']['iteration'] == $this->_sections['options']['total']);
?>
                
                
            
                
                  <li>
                    <input data-priority = "<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['image_priority']; ?>
" data-image_trapezoid="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['image_trapezoid']; ?>
" data-image_round="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['image_round']; ?>
" data-image_long="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['image_long']; ?>
" data-empty="false" data-id="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['id']; ?>
"  data-price="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['item_price']; ?>
"   data-itemname="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['item_name']; ?>
" type="checkbox" value="" name="check" id="cheese-check<?php echo $this->_sections['options']['index']+1; ?>
">
                    <label <?php if ($this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['premium']): ?> class="premium-icon" <?php endif; ?>  for="cheese-check<?php echo $this->_sections['options']['index']+1; ?>
"><?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['item_name']; ?>
</label>
                  
                    <?php if ($this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['options_id'] != 0): ?>
                    
                    <div class="sub-value"> <a href="#" class="left"></a>
                    <span class="text-box">
                    
                  
                     
                    <?php unset($this->_sections['subval']);
$this->_sections['subval']['name'] = 'subval';
$this->_sections['subval']['start'] = (int)0;
$this->_sections['subval']['loop'] = is_array($_loop=count($this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['options_id'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['subval']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['subval']['show'] = true;
$this->_sections['subval']['max'] = $this->_sections['subval']['loop'];
if ($this->_sections['subval']['start'] < 0)
    $this->_sections['subval']['start'] = max($this->_sections['subval']['step'] > 0 ? 0 : -1, $this->_sections['subval']['loop'] + $this->_sections['subval']['start']);
else
    $this->_sections['subval']['start'] = min($this->_sections['subval']['start'], $this->_sections['subval']['step'] > 0 ? $this->_sections['subval']['loop'] : $this->_sections['subval']['loop']-1);
if ($this->_sections['subval']['show']) {
    $this->_sections['subval']['total'] = min(ceil(($this->_sections['subval']['step'] > 0 ? $this->_sections['subval']['loop'] - $this->_sections['subval']['start'] : $this->_sections['subval']['start']+1)/abs($this->_sections['subval']['step'])), $this->_sections['subval']['max']);
    if ($this->_sections['subval']['total'] == 0)
        $this->_sections['subval']['show'] = false;
} else
    $this->_sections['subval']['total'] = 0;
if ($this->_sections['subval']['show']):

            for ($this->_sections['subval']['index'] = $this->_sections['subval']['start'], $this->_sections['subval']['iteration'] = 1;
                 $this->_sections['subval']['iteration'] <= $this->_sections['subval']['total'];
                 $this->_sections['subval']['index'] += $this->_sections['subval']['step'], $this->_sections['subval']['iteration']++):
$this->_sections['subval']['rownum'] = $this->_sections['subval']['iteration'];
$this->_sections['subval']['index_prev'] = $this->_sections['subval']['index'] - $this->_sections['subval']['step'];
$this->_sections['subval']['index_next'] = $this->_sections['subval']['index'] + $this->_sections['subval']['step'];
$this->_sections['subval']['first']      = ($this->_sections['subval']['iteration'] == 1);
$this->_sections['subval']['last']       = ($this->_sections['subval']['iteration'] == $this->_sections['subval']['total']);
?>

                    <?php $this->assign('option_id', $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['options_id'][$this->_sections['subval']['index']]['id']); ?>
                    
                    <?php $this->assign('display', 'none'); ?>
                    <?php $this->assign('current', 'false'); ?>
                    
                    <?php if ($this->_sections['subval']['index'] == 0): ?> 
                    
                    <?php $this->assign('display', 'block'); ?>   
                    <?php $this->assign('current', 'true'); ?>
                    
                    <?php endif; ?>
                  
                    <input rel="slide" readonly style="display:<?php echo $this->_tpl_vars['display']; ?>
" data-unit="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['options_id'][$this->_sections['subval']['index']]['option_unit']; ?>
" data-current="<?php echo $this->_tpl_vars['current']; ?>
" data-optionid="<?php echo $this->_tpl_vars['option_id']; ?>
" data-image="" data-image_trapezoid="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['option_images'][$this->_tpl_vars['option_id']]['tImg']; ?>
" data-image_round="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['option_images'][$this->_tpl_vars['option_id']]['rImg']; ?>
" data-image_long="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['option_images'][$this->_tpl_vars['option_id']]['lImg']; ?>
" data-price_mul="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['options_id'][$this->_sections['subval']['index']]['price_mult']; ?>
"  type="text" name="" class="text-box" value="<?php echo $this->_tpl_vars['CHEESE_DATA'][$this->_sections['options']['index']]['options_id'][$this->_sections['subval']['index']]['option_name']; ?>
"/>
                    
                    <?php endfor; endif; ?>
                     
                    </span>
            
                   <a href="#" class="right right-hover"></a> 
           </div> 
                    
                    <?php endif; ?>
                  
                  </li>
                  
                 <?php endfor; endif; ?>
                  
                  

                </ul>
              </div>