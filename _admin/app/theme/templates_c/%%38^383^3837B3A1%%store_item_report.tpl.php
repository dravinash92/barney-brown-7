<?php /* Smarty version 2.6.25, created on 2018-07-23 04:03:36
         compiled from store_item_report.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'store_item_report.tpl', 158, false),array('function', 'math', 'store_item_report.tpl', 191, false),array('modifier', 'string_format', 'store_item_report.tpl', 180, false),)), $this); ?>
<div class="container">
    <div class="item-report">
      <h1>ITEM REPORT</h1>
      
      <div class="filter_wrap">
      <form name="items_sales_report_filter">
      <div class="store-location">
         <label>Store Location</label>
        <select name="pickup_store" disabled>
           <option value="">Select Store</option>
			<?php $_from = $this->_tpl_vars['pickupstores']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<option <?php if ($this->_tpl_vars['storeId'] == $this->_tpl_vars['v']->id): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['v']->id; ?>
"><?php echo $this->_tpl_vars['v']->store_name; ?>
, <?php echo $this->_tpl_vars['v']->address1; ?>
, <?php echo $this->_tpl_vars['v']->address2; ?>
, <?php echo $this->_tpl_vars['v']->zip; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
         </select>
        <ul>
          <li><label>Order Type</label></li>
          <li>
           <span class="multi-left">
          <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="order_type[]" value="1">
            <label for="checkbox-2-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Delivery</span> </li>
          <li>
            <span class="multi-left">
             <input type="checkbox" id="checkbox-5" class="input-checkbox" name="order_type[]" value="0">
            <label for="checkbox-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Pick-Up</span>
          </li>
        </ul>
        <ul>
          <li><label>Order Status</label></li>
           <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-3-1" class="input-checkbox" name="order_status[]" value="0">
            <label for="checkbox-3-1" class="multisel-ckeck"></label>
            </span> <span class="radio-text">New</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-2" class="input-checkbox" name="order_status[]" value="1">
            <label for="checkbox-3-2" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Processed</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-3" class="input-checkbox" name="order_status[]" value="2">
            <label for="checkbox-3-3" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Ready</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-4" class="input-checkbox" name="order_status[]" value="6">
            <label for="checkbox-3-4" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Out</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-5" class="input-checkbox" name="order_status[]" value="7">
            <label for="checkbox-3-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Received</span>
          </li>
        </ul>
      </div>
      
       <div class="date-time-settings">
         <label>Date/Time Settings</label>
         <ul>
           <li>
            <div class="radio-left">
             <div class="radio-section">
                <input type="radio" value="order_date" checked class="input-radio" name="order_date_filter" id="1">
                <label for="1"></label>
              </div>
              <span class="radio-text">Order Placement</span>
             </div>
             <div class="radio-right">
              <div class="radio-section">
                  <input type="radio" value="order_delivered" class="input-radio" name="order_date_filter" id="2">
                <label for="2"></label>
                </div>
                <span class="radio-text">Order Fulfillment</span>
              </div>
           </li>
            <li>
             <label>From</label>
          <input type="text" class="expiration" placeholder="Select date" name="from_date" value="<?php echo $this->_tpl_vars['from_date']; ?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/calendar.png"></a>
            <select name="from_time" name="from_time">
				<option value="">Select Time</option>
				<option value="9:00">9:00 AM</option>
                <option value="13:00">1:00 PM</option>
                <option value="16:00">4:00 PM</option>
                <option value="20:00">8:00 PM</option>
             </select>
           </li>
            <li>
             <label>To</label>
          <input type="text" class="expiration" placeholder="Select date" name="to_date" value="<?php echo $this->_tpl_vars['to_date']; ?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/calendar.png"></a>
            <select class="to_time" name="to_time">
				<option value="">Select Time</option>
				<option value="9:00">9:00 AM</option>
                <option value="13:00">1:00 PM</option>
                <option value="16:00">4:00 PM</option>
                <option value="20:00">8:00 PM</option>
             </select>
           </li>
         </ul>
      </div>
      
      <div class="type-purchase">
        <ul>
          <li><label>Type of Purchase</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-4-5" class="input-checkbox" name="type_purchase[]" value="0">
            <label for="checkbox-4-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Website</span> </li>
            
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-8" class="input-checkbox" name="type_purchase[]" value="1">
            <label for="checkbox-3-8" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Phone Order</span>
          </li>
          <li></li>
          <li>
             <label>System User</label>
            <select name="system_user">
              <option value="">Select User</option>
				<?php $_from = $this->_tpl_vars['systemusers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
					<option value="<?php echo $this->_tpl_vars['v']->uid; ?>
"><?php echo $this->_tpl_vars['v']->first_name; ?>
 <?php echo $this->_tpl_vars['v']->last_name; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
             </select>
             <a href="javascript:void(0)" class="do_store_item_report_filter"><img width="26" height="26" alt="" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/link.png"></a>
          </li>           
        </ul>        
      </div>
      </form>
      </div>
      <div class="reports_table">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="sandwiches">
        <tbody><tr>
          <th width="65%">CUSTOM SANDWICHES</th>
           <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        <tr class="subheader">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><span class="hide_totalqty"></span></td>
          <td>$<span class="hide_subtottal"></span></td>
          <td>100%</td>
        </tr>
        <tr class="items">
          <td colspan="5">BREADS</td>
        </tr>
		<?php $_from = $this->_tpl_vars['sandwichitems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
		<?php if ($this->_tpl_vars['v']->category_id == 1): ?>
		 <tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
 datarow">
          <td><?php echo $this->_tpl_vars['v']->item_name; ?>
</td>
           <td>$<?php echo $this->_tpl_vars['v']->item_price; ?>

           <?php $this->assign('bprice', $this->_tpl_vars['v']->item_price); ?>
           </td>
          <td>
     
          <?php $_from = $this->_tpl_vars['breadsanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['bk'] => $this->_tpl_vars['bv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['bv']['itemname']): ?>
          <?php $this->assign('bqty', $this->_tpl_vars['bv']['qty']); ?>
           <?php echo $this->_tpl_vars['bqty']; ?>

           <?php $this->assign('sum_qty_bread', $this->_tpl_vars['sum_qty_bread']+$this->_tpl_vars['bqty']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
          
         
          
          </td>
          <td>
         <?php $_from = $this->_tpl_vars['breadsanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['bk'] => $this->_tpl_vars['bv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['bv']['itemname']): ?>
          <?php $this->assign('breads_subtotal', $this->_tpl_vars['bqty']*$this->_tpl_vars['bprice']); ?>
          $<?php echo ((is_array($_tmp=$this->_tpl_vars['breads_subtotal'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

          <?php $this->assign('sum_all_breads', $this->_tpl_vars['sum_all_breads']+$this->_tpl_vars['breads_subtotal']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
          </td>
		   <td>
		   
		 <?php $_from = $this->_tpl_vars['breadsanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['bk'] => $this->_tpl_vars['bv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['bv']['itemname']): ?>
          <?php $this->assign('bqty', $this->_tpl_vars['bv']['qty']); ?>
         
              <?php echo smarty_function_math(array('equation' => "(( x / y ) * z )",'x' => $this->_tpl_vars['bqty'],'y' => $this->_tpl_vars['salesCount'],'z' => 100,'format' => "%.2f"), $this);?>
%
           <?php $this->assign('sum_qty_bread', $this->_tpl_vars['sum_qty_bread']+$this->_tpl_vars['bqty']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
		   
		   </td>
        </tr>
        <?php endif; ?>
		 <?php endforeach; endif; unset($_from); ?>
		        
         <tr class="items">
          <td colspan="5">PROTEINS</td>
        </tr>
		<?php $_from = $this->_tpl_vars['sandwichitems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
		<?php if ($this->_tpl_vars['v']->category_id == 2): ?>
		 <tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
 datarow">
          <td><?php echo $this->_tpl_vars['v']->item_name; ?>

          </td>
           <td>$<?php echo $this->_tpl_vars['v']->item_price; ?>

           <?php $this->assign('pprice', $this->_tpl_vars['v']->item_price); ?>
           </td>
          <td>
          <?php $_from = $this->_tpl_vars['proteinsanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pk'] => $this->_tpl_vars['pv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['pv']['itemname']): ?>
          <?php $this->assign('pqty', $this->_tpl_vars['pv']['qty']); ?>
          <?php echo $this->_tpl_vars['pqty']; ?>

          <?php $this->assign('sum_qty_protein', $this->_tpl_vars['sum_qty_protein']+$this->_tpl_vars['pqty']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
         </td>
          <td>
          <?php $_from = $this->_tpl_vars['proteinsanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pk'] => $this->_tpl_vars['pv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['pv']['itemname']): ?>
          <?php $this->assign('protein_subtotal', $this->_tpl_vars['pqty']*$this->_tpl_vars['pprice']); ?>
          $<?php echo ((is_array($_tmp=$this->_tpl_vars['protein_subtotal'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

          <?php $this->assign('sum_all_protein', $this->_tpl_vars['sum_all_protein']+$this->_tpl_vars['protein_subtotal']); ?>
           <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
          </td>
		   <td></td>
        </tr>
		<?php endif; ?>
		 <?php endforeach; endif; unset($_from); ?>
				
		<tr class="items">
          <td colspan="5">CHEESES</td>
        </tr>
		<?php $_from = $this->_tpl_vars['sandwichitems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
		<?php if ($this->_tpl_vars['v']->category_id == 3): ?>
		 <tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
 datarow">
          <td><?php echo $this->_tpl_vars['v']->item_name; ?>
</td>
           <td>$<?php echo $this->_tpl_vars['v']->item_price; ?>

           <?php $this->assign('cprice', $this->_tpl_vars['v']->item_price); ?>
           </td>
          <td><?php $_from = $this->_tpl_vars['cheesesanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ck'] => $this->_tpl_vars['cv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['cv']['itemname']): ?>
           <?php $this->assign('cqty', $this->_tpl_vars['cv']['qty']); ?>
          <?php echo $this->_tpl_vars['cqty']; ?>

           <?php $this->assign('sum_qty_cheese', $this->_tpl_vars['sum_qty_cheese']+$this->_tpl_vars['cqty']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?></td>
          <td>
          <?php $_from = $this->_tpl_vars['cheesesanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ck'] => $this->_tpl_vars['cv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['cv']['itemname']): ?>
          <?php $this->assign('cheese_subtotal', $this->_tpl_vars['cqty']*$this->_tpl_vars['cprice']); ?>
          $<?php echo ((is_array($_tmp=$this->_tpl_vars['cheese_subtotal'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

          <?php $this->assign('sum_all_cheese', $this->_tpl_vars['sum_all_cheese']+$this->_tpl_vars['cheese_subtotal']); ?>
           <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
          </td>
		   <td></td>
        </tr>
		<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
				
		<tr class="items">
          <td colspan="5">TOPPINGS</td>
        </tr>
		<?php $_from = $this->_tpl_vars['sandwichitems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
		<?php if ($this->_tpl_vars['v']->category_id == 4): ?>
		 <tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
 datarow">
          <td><?php echo $this->_tpl_vars['v']->item_name; ?>
</td>
           <td>$<?php echo $this->_tpl_vars['v']->item_price; ?>

           <?php $this->assign('tprice', $this->_tpl_vars['v']->item_price); ?>
           </td>
          <td><?php $_from = $this->_tpl_vars['toppingssanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tk'] => $this->_tpl_vars['tv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['tv']['itemname']): ?>
          <?php $this->assign('tqty', $this->_tpl_vars['tv']['qty']); ?>
          <?php echo $this->_tpl_vars['tqty']; ?>

          <?php $this->assign('sum_qty_toppings', $this->_tpl_vars['sum_qty_toppings']+$this->_tpl_vars['tqty']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?></td>
          <td>
          <?php $_from = $this->_tpl_vars['toppingssanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tk'] => $this->_tpl_vars['tv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['tv']['itemname']): ?>
          <?php $this->assign('toppings_subtotal', $this->_tpl_vars['tqty']*$this->_tpl_vars['tprice']); ?>
          $<?php echo ((is_array($_tmp=$this->_tpl_vars['toppings_subtotal'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

          <?php $this->assign('sum_all_toppings', $this->_tpl_vars['sum_all_toppings']+$this->_tpl_vars['toppings_subtotal']); ?>
           <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
          </td>
		   <td>
		   
		   <?php $_from = $this->_tpl_vars['toppingssanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tk'] => $this->_tpl_vars['tv']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['tv']['itemname']): ?>
          <?php $this->assign('tqty', $this->_tpl_vars['tv']['qty']); ?>
          <?php echo smarty_function_math(array('equation' => "(( x / y ) * z )",'x' => $this->_tpl_vars['tqty'],'y' => $this->_tpl_vars['salesCount'],'z' => 100,'format' => "%.2f"), $this);?>
%
          <?php $this->assign('sum_qty_toppings', $this->_tpl_vars['sum_qty_toppings']+$this->_tpl_vars['tqty']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
		   
		   </td>
        </tr>
		<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr class="items">
          <td colspan="5">CONDIMENTS</td>
        </tr>
		<?php $_from = $this->_tpl_vars['sandwichitems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
		<?php if ($this->_tpl_vars['v']->category_id == 5): ?>
		 <tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
 datarow">
          <td><?php echo $this->_tpl_vars['v']->item_name; ?>
</td>
           <td>$<?php echo $this->_tpl_vars['v']->item_price; ?>

           <?php $this->assign('coprice', $this->_tpl_vars['v']->item_price); ?>
           </td>
          <td><?php $_from = $this->_tpl_vars['condisanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cok'] => $this->_tpl_vars['cov']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['cov']['itemname']): ?>
          <?php $this->assign('coqty', $this->_tpl_vars['cov']['qty']); ?>
          <?php echo $this->_tpl_vars['coqty']; ?>

           <?php $this->assign('sum_qty_condi', $this->_tpl_vars['sum_qty_condi']+$this->_tpl_vars['coqty']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?></td>
          <td>
          <?php $_from = $this->_tpl_vars['condisanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cok'] => $this->_tpl_vars['cov']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['cov']['itemname']): ?>
          <?php $this->assign('condi_subtotal', $this->_tpl_vars['coqty']*$this->_tpl_vars['coprice']); ?>
          $<?php echo ((is_array($_tmp=$this->_tpl_vars['condi_subtotal'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

           <?php $this->assign('sum_all_condi', $this->_tpl_vars['sum_all_condi']+$this->_tpl_vars['condi_subtotal']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?></td>
		   <td><?php $_from = $this->_tpl_vars['condisanddata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cok'] => $this->_tpl_vars['cov']):
?>
          <?php if ($this->_tpl_vars['v']->item_name == $this->_tpl_vars['cov']['itemname']): ?>
          <?php $this->assign('coqty', $this->_tpl_vars['cov']['qty']); ?>
           <?php echo smarty_function_math(array('equation' => "(( x / y ) * z )",'x' => $this->_tpl_vars['coqty'],'y' => $this->_tpl_vars['salesCount'],'z' => 100,'format' => "%.2f"), $this);?>
%
           <?php $this->assign('sum_qty_condi', $this->_tpl_vars['sum_qty_condi']+$this->_tpl_vars['coqty']); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?></td>
        </tr>
		<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		<?php $this->assign('tottal_subtottal', $this->_tpl_vars['sum_all_condi']+$this->_tpl_vars['sum_all_breads']+$this->_tpl_vars['sum_all_protein']+$this->_tpl_vars['sum_all_cheese']+$this->_tpl_vars['sum_all_toppings']); ?>
		<?php $this->assign('tottal_qty', $this->_tpl_vars['sum_qty_bread']+$this->_tpl_vars['sum_qty_protein']+$this->_tpl_vars['sum_qty_cheese']+$this->_tpl_vars['sum_qty_toppings']+$this->_tpl_vars['sum_qty_condi']); ?>
    	
      </tbody></table>
	  
	   <table width="100%" cellspacing="0" cellpadding="0" border="0" class="products">
        <tbody><tr>
          <th width="65%">STANDARD ITEMS</th>
          <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        
        <?php $_from = $this->_tpl_vars['standardCatagory']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sc'] => $this->_tpl_vars['sci']):
?>
        <tr class="subheader <?php echo $this->_tpl_vars['sc']; ?>
">
          <td><?php echo $this->_tpl_vars['sci']->category_identifier; ?>
</td>
          <td></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-item-<?php echo $this->_tpl_vars['sc']; ?>
"></span></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-<?php echo $this->_tpl_vars['sc']; ?>
"></span></td>
          <td></td>
        </tr>
        <?php $this->assign('sum_all_salades', 0); ?>
        <?php $this->assign('sum_all_salades_count', 0); ?>
        
		<?php $_from = $this->_tpl_vars['standareditems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
		<?php if ($this->_tpl_vars['v']->standard_category_id == $this->_tpl_vars['sci']->id): ?>
		<tr class="<?php echo smarty_function_cycle(array('values' => "odd,even"), $this);?>
 datarow st-cat-<?php echo $this->_tpl_vars['sc']; ?>
">
          <td><?php echo $this->_tpl_vars['v']->product_name; ?>
</td>
          <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->product_price)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

          <?php $this->assign('price', $this->_tpl_vars['v']->product_price); ?>
          </td>
          <td><?php $_from = $this->_tpl_vars['standareditemsqty']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sk'] => $this->_tpl_vars['sv']):
?>
          <?php if ($this->_tpl_vars['v']->id == $this->_tpl_vars['sv']->item_id): ?>
          <?php echo $this->_tpl_vars['sv']->qty; ?>

          <?php $this->assign('qty', $this->_tpl_vars['sv']->qty); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?></td>
          <td>
          <?php $_from = $this->_tpl_vars['standareditemsqty']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sk'] => $this->_tpl_vars['sv']):
?>
          <?php if ($this->_tpl_vars['v']->id == $this->_tpl_vars['sv']->item_id): ?>
          <?php $this->assign('salades_subtotal', $this->_tpl_vars['qty']*$this->_tpl_vars['price']); ?>
          $<?php echo ((is_array($_tmp=$this->_tpl_vars['salades_subtotal'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

           <?php $this->assign('sum_all_salades', $this->_tpl_vars['sum_all_salades']+$this->_tpl_vars['salades_subtotal']); ?>
           <?php $this->assign('sum_all_salades_count', $this->_tpl_vars['sum_all_salades_count']+$this->_tpl_vars['qty']); ?>
           <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
           </td>
		   <td> 
		   
		   <?php $_from = $this->_tpl_vars['standareditemsqty']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sk'] => $this->_tpl_vars['sv']):
?>
          <?php if ($this->_tpl_vars['v']->id == $this->_tpl_vars['sv']->item_id): ?>
          <?php $this->assign('stpq', $this->_tpl_vars['sv']->qty); ?>
          <?php echo smarty_function_math(array('equation' => "(( x / y ) * z )",'x' => $this->_tpl_vars['stpq'],'y' => $this->_tpl_vars['salesCount'],'z' => 100,'format' => "%.2f"), $this);?>
%
          <?php $this->assign('qty', $this->_tpl_vars['sv']->qty); ?>
          <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
		   
		   </td>
        </tr>
		<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		
		<?php $this->assign('tottal_subtottal', $this->_tpl_vars['tottal_subtottal']+$this->_tpl_vars['sum_all_salades']); ?>
		<?php $this->assign('tottal_qty', $this->_tpl_vars['tottal_qty']+$this->_tpl_vars['sum_all_salades_count']); ?>
		
		<input type="hidden" id="st-cat-<?php echo $this->_tpl_vars['sc']; ?>
"  value="<?php echo ((is_array($_tmp=$this->_tpl_vars['sum_all_salades'])) ? $this->_run_mod_handler('string_format', true, $_tmp, '%.2f') : smarty_modifier_string_format($_tmp, '%.2f')); ?>
"/>
		<input type="hidden" id="st-cat-item-<?php echo $this->_tpl_vars['sc']; ?>
" value="<?php echo $this->_tpl_vars['sum_all_salades_count']; ?>
"/>
        	
        <?php endforeach; endif; unset($_from); ?>	
		
	   </tbody>
	   </table>
	   </div>
		 <span class="tottal_subtottal"><?php echo ((is_array($_tmp=$this->_tpl_vars['tottal_subtottal'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</span>
    	 <span class="tottal_qty"><?php echo $this->_tpl_vars['tottal_qty']; ?>
</span>
    </div>
  </div>