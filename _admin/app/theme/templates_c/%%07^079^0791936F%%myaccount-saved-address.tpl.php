<?php /* Smarty version 2.6.25, created on 2020-09-21 00:39:54
         compiled from myaccount-saved-address.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'myaccount-saved-address.tpl', 7, false),array('modifier', 'truncate', 'myaccount-saved-address.tpl', 19, false),)), $this); ?>
		<div class="saved-address-wrapper">
          <ul class="saved-address-inner">
            <li> <span class="add-new-address-main"> <a href="#" class="change change-address" ><span>+</span><br />
              ADD NEW<br />
              ADDRESS</a> </span>
            </li>
            <?php if (count($this->_tpl_vars['addresses']) > 0): ?>
         
            <?php $_from = $this->_tpl_vars['addresses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<li> <span class="address-content">
				  <h3><?php echo $this->_tpl_vars['v']->name; ?>
 </h3>
				  <p>
					<?php echo $this->_tpl_vars['v']->company; ?>
<br />
					<?php echo $this->_tpl_vars['v']->address1; ?>
, <?php echo $this->_tpl_vars['v']->address2; ?>
<br />
					<?php if ($this->_tpl_vars['v']->street != ""): ?> <?php echo $this->_tpl_vars['v']->street; ?>
 <?php endif; ?>					
					<?php if ($this->_tpl_vars['v']->cross_streets != ""): ?>, (<?php echo $this->_tpl_vars['v']->cross_streets; ?>
) <br /> <?php endif; ?>
					New York, <?php echo $this->_tpl_vars['v']->zip; ?>
<br />
					<?php echo $this->_tpl_vars['v']->phone; ?>
</p>
				  <?php if ($this->_tpl_vars['v']->delivery_instructions != ""): ?><p>DELIVERY INSTRUCTIONS:<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->delivery_instructions)) ? $this->_run_mod_handler('truncate', true, $_tmp, 35, "...") : smarty_modifier_truncate($_tmp, 35, "...")); ?>
</p><?php endif; ?><br/>
					<?php if ($this->_tpl_vars['v']->extn != ""): ?><p>Ring buzzer <?php echo $this->_tpl_vars['v']->extn; ?>
</p><?php endif; ?>
				  </span> 
				<span class="button-holder-address">
					<a data-target="<?php echo $this->_tpl_vars['v']->address_id; ?>
" class="edit edit_address">EDIT</a>
					<a data-target="<?php echo $this->_tpl_vars['v']->address_id; ?>
" class="remove remove_address">remove</a>
				</span>
				</li>
            <?php endforeach; endif; unset($_from); ?>
            <?php endif; ?>
          </ul>
        </div>
      </div>
  </div>
  
 