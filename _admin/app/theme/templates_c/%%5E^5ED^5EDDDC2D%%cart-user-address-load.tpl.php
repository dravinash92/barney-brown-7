<?php /* Smarty version 2.6.25, created on 2020-09-25 01:02:59
         compiled from cart-user-address-load.tpl */ ?>
 <label>User Addresses</label>
    <select name="user_address" class="select user_address">
        <option value="">Select Address</option>
		<?php $_from = $this->_tpl_vars['addresses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
			<option value="<?php echo $this->_tpl_vars['v']->address_id; ?>
"><?php echo $this->_tpl_vars['v']->name; ?>
,<?php if ($this->_tpl_vars['v']->address1 != ""): ?> <?php echo $this->_tpl_vars['v']->address1; ?>
,<?php endif; ?> <?php echo $this->_tpl_vars['v']->cross_streets; ?>
, <?php echo $this->_tpl_vars['v']->zip; ?>
</option>
        <?php endforeach; endif; unset($_from); ?>
    </select>
    <input type="hidden" name="address_type"  value="delivery" />     
    <a href="javascript:void(0)" class="add-new-address add-address">add new address</a>
	<a href="javascript:void(0)" class="add-new-address edit-address">edit addresses</a>