<?php
/* Smarty version 3.1.39, created on 2021-03-31 04:17:06
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\sandwich-landing.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_606430026f5477_47519894',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2e128797cd814fae86866e1e1a3d35b169de0f64' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\sandwich-landing.tpl',
      1 => 1600125378,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606430026f5477_47519894 (Smarty_Internal_Template $_smarty_tpl) {
?>  <div class="landing-page"  >
               <h2>MAKE ONE OF YOUR GO-TO SANDWICHES, OR TRY SOMETHING COMPLETELY NEW! </h2> 
              <!--  <p>Sandwiches include your choice of one of our fine artisinal breads, unlimited toppings, unlimited condiments, plus any of the add-ons listed below. Start creating your sandwich by selecting one of the breads on the right!</p> -->
               <div class="price">
                 <ul>
                   <li>STARTING AT  <span class="starting">$<?php echo $_smarty_tpl->tpl_vars['BASE_FARE']->value;?>
.00</span></li>
                   <li>PROTEINS: <span>+ $2.00</span></li>
                   <li>CHEESES: <span>+ $1.00</span></li>
                   <li>PREMIUMS: <span>+ $1.00</span></li>
                 </ul>
               </div>
            </div>
<?php }
}
