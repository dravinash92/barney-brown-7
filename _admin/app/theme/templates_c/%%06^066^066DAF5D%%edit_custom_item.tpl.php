<?php /* Smarty version 2.6.25, created on 2020-09-16 00:04:38
         compiled from edit_custom_item.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'explode', 'edit_custom_item.tpl', 116, false),array('modifier', 'in_array', 'edit_custom_item.tpl', 121, false),)), $this); ?>

<div class="container">
    <div class="condiment">
      <h1>EDIT ITEM </h1>
      <form name="edititems" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/updateItems" enctype="multipart/form-data">
          <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
">
          <input type="hidden" name="old_image" value="<?php echo $this->_tpl_vars['data']['item_image']; ?>
">
          <input type="hidden" name="old_image_sliced" value="<?php echo $this->_tpl_vars['data']['item_image_sliced']; ?>
">
          <input type="hidden" name="old_image_long" value="<?php echo $this->_tpl_vars['data']['image_long']; ?>
">
          <input type="hidden" name="old_image_round" value="<?php echo $this->_tpl_vars['data']['image_round']; ?>
">
          <input type="hidden" name="old_image_trapezoid" value="<?php echo $this->_tpl_vars['data']['image_trapezoid']; ?>
">
      <ul>
        <li>
          <label>Name of Protein</label>
          <input type="text" name="item_name" value="<?php echo $this->_tpl_vars['data']['item_name']; ?>
">
        </li>
        
        <li>
          <label>Price</label>
          <input type="text" name="item_price" value="<?php echo $this->_tpl_vars['data']['item_price']; ?>
">
        </li>
        
        <!--  <li>
          <label>Description (Optional)</label>
          <div gramm="gramm" contenteditable="" style="border: 1px solid transparent; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; box-sizing: content-box; display: block; height: 96px; width: 293.375px; padding: 3px 5px; margin: 0px 0px 8px; position: absolute; color: transparent; white-space: pre-wrap; overflow: hidden; z-index: 0; text-align: left; -webkit-transform: translate(0px, 70px); transform: translate(0px, 70px); background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);" id="b5a5d32d-b1b2-ed53-22fe-f66acdeb77bf" gramm_id="b5a5d32d-b1b2-ed53-22fe-f66acdeb77bf" gr_new_editor="true"><span style="line-height: normal; white-space: pre-wrap; text-align: start; clear: none; box-sizing: border-box; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; display: inline-block; width: 100%; font-family: monospace; font-size: 13px; font-weight: normal; font-style: normal; letter-spacing: normal; text-shadow: none; color: transparent; height: 102px;"></span><br></div><textarea gramm="" txt_gramm_id="4d6efe49-ae3f-c5c5-c33a-461e19cdb53a" gramm_id="c82bf7d2-5c61-2cda-8e5c-815df6eeb63f" spellcheck="false" gr_new_editor="true" style="white-space: pre-wrap; z-index: auto; position: relative; line-height: normal; font-size: 13px; -webkit-transition: none; transition: none; height: 96px; overflow: auto; background: transparent !important;"></textarea><div class="gr-btn-freemium gr-shown" style="display: none; z-index: 2; margin-left: 487px; margin-top: 364px; opacity: 1;">	<div class="gr-btn-status" title="Protected by Grammarly"></div></div>
        </li> -->
      <?php if ($this->_tpl_vars['data']['category_id'] == 1): ?>
		 <li>
          <label>Bread Type</label>
          <select  name="bread_type">
          	<option <?php if ($this->_tpl_vars['data']['bread_type'] == 0): ?> selected="selected" <?php endif; ?> value="0">None</option>
          	<option <?php if ($this->_tpl_vars['data']['bread_type'] == 1): ?> selected="selected" <?php endif; ?> value="1">3 foot</option>
          	<option <?php if ($this->_tpl_vars['data']['bread_type'] == 2): ?> selected="selected" <?php endif; ?>value="2">6 foot</option>
          	
          </select>
        </li>
         <li>
          <label>Bread Shape</label>
          <select  name="bread_shape">
          	<option <?php if ($this->_tpl_vars['data']['bread_shape'] == 0): ?> selected="selected" <?php endif; ?> value="0">Long</option>
          	<option <?php if ($this->_tpl_vars['data']['bread_shape'] == 1): ?> selected="selected" <?php endif; ?> value="1">Round</option>
          	<option <?php if ($this->_tpl_vars['data']['bread_shape'] == 2): ?> selected="selected" <?php endif; ?>value="2">Trapezoid</option>
          	
          </select>
        </li>
	 <?php endif; ?> 
	   <?php if ($this->_tpl_vars['data']['category_id'] == 1): ?>
        <li>
         <label>Image</label>
          <label>
           <?php if ($this->_tpl_vars['data']['item_image'] != ""): ?>
          	 <?php $this->assign('uploadpath', "upload/".($this->_tpl_vars['data']['item_image'])); ?>
           
      		 <?php if (file_exists ( $this->_tpl_vars['uploadpath'] )): ?>
      		 		<img  style=" margin-top: -28px;"  src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/<?php echo $this->_tpl_vars['data']['item_image']; ?>
" height="50px" width="50px">  
			 <?php endif; ?>
			 <?php endif; ?> 
          	 <input type="file" class="choose" name="item_image" value="<?php echo $this->_tpl_vars['data']['item_image']; ?>
"> 
          </label>
      
        </li> <?php endif; ?>
          <?php if ($this->_tpl_vars['data']['category_id'] == 1): ?>
        <li>
         <label>Image Sliced</label>
         <label> 
          <?php if ($this->_tpl_vars['data']['item_image_sliced'] != ""): ?>
	          <?php $this->assign('uploadpathslice', "upload/".($this->_tpl_vars['data']['item_image_sliced'])); ?>
	      		 <?php if (file_exists ( $this->_tpl_vars['uploadpathslice'] )): ?>
	         	<img  style=" margin-top: -28px;" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/<?php echo $this->_tpl_vars['data']['item_image_sliced']; ?>
" height="50px" width="50px"> 
				 <?php endif; ?>
			<?php endif; ?>
         	<input type="file" class="choose" name="item_image_sliced" value="<?php echo $this->_tpl_vars['data']['item_image_sliced']; ?>
"> 
         </label>
        </li>
       
        <?php endif; ?>
          <?php if ($this->_tpl_vars['data']['category_id'] != 1): ?>
        <li>
         <label>Long Image </label>
          <label> 
           <?php if ($this->_tpl_vars['data']['image_long'] != ""): ?>
	           	<?php $this->assign('uploadpathlong', "upload/".($this->_tpl_vars['data']['image_long'])); ?>
	      		 <?php if (file_exists ( $this->_tpl_vars['uploadpathlong'] )): ?>
	          		<img  style=" margin-top: -28px;" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/<?php echo $this->_tpl_vars['data']['image_long']; ?>
" height="50px" width="50px"> 
				 <?php endif; ?>
			<?php endif; ?>
          	<input type="file" class="choose" name="image_long" value="<?php echo $this->_tpl_vars['data']['image_long']; ?>
"> 
          </label>
        </li>
        <li>
         <label>Round Image </label>
          <label> 
           <?php if ($this->_tpl_vars['data']['image_round'] != ""): ?>
	           	<?php $this->assign('uploadpathlong', "upload/".($this->_tpl_vars['data']['image_round'])); ?>
	      		 <?php if (file_exists ( $this->_tpl_vars['uploadpathlong'] )): ?>
          			<img  style=" margin-top: -28px;" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/<?php echo $this->_tpl_vars['data']['image_round']; ?>
" height="50px" width="50px">    
				 <?php endif; ?>
			<?php endif; ?>
          	<input type="file" class="choose" name="image_round" value="<?php echo $this->_tpl_vars['data']['image_round']; ?>
"> 
          </label>
        </li>
        <li>
         <label>Trapezoid Image </label>
          <label> 
          	 <?php if ($this->_tpl_vars['data']['image_trapezoid'] != ""): ?>
          	     <?php $this->assign('uploadpathlong', "upload/".($this->_tpl_vars['data']['image_trapezoid'])); ?>
	      		 <?php if (file_exists ( $this->_tpl_vars['uploadpathlong'] )): ?>
          			<img style=" margin-top: -28px;" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/<?php echo $this->_tpl_vars['data']['image_trapezoid']; ?>
" height="50px" width="50px">  
				 <?php endif; ?>
			<?php endif; ?>
          	<input type="file" class="choose" name="image_trapezoid" value="<?php echo $this->_tpl_vars['data']['image_trapezoid']; ?>
"> 
          </label>
        </li>
    
        
	        <?php $this->assign('optnarray', ((is_array($_tmp=":")) ? $this->_run_mod_handler('explode', true, $_tmp, $this->_tpl_vars['data']['options_id']) : explode($_tmp, $this->_tpl_vars['data']['options_id']))); ?>  
	         <li >
	         <label>Option</label>
	        <select class="optinMultiselection" name="select_sandwich_option[]" multiple size="3">
				  	<?php $_from = $this->_tpl_vars['sandwichCatagoryData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
				  <option <?php if (((is_array($_tmp=$this->_tpl_vars['b']['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['optnarray']) : in_array($_tmp, $this->_tpl_vars['optnarray'])) == 1): ?> selected="selected"  <?php endif; ?> value="<?php echo $this->_tpl_vars['b']['id']; ?>
"  >  <?php echo $this->_tpl_vars['b']['option_display_name']; ?>
</option>
				   <?php endforeach; endif; unset($_from); ?>    
			</select>
		   	</li> 
		<?php endif; ?>
		
		
		 <li>
          <label>Taxable</label>
          <input type="checkbox" value="1" name="taxable" class="price-menu" <?php if ($this->_tpl_vars['data']['taxable'] == 1): ?> checked <?php endif; ?> >
        </li>  
        
             <li>
          <label>Premium</label>
          <input type="checkbox" value="1" name="premium" class="price-menu" <?php if ($this->_tpl_vars['data']['premium'] == 1): ?> checked <?php endif; ?>  >
        </li>  
        
		
      </ul>
       <div class="button">
           <input type="hidden" value="<?php echo $this->_tpl_vars['data']['category_id']; ?>
" name="category_id">
          <input type="hidden" value="<?php echo $this->_tpl_vars['data']['item_price']; ?>
" name="old_price">
         <input type="submit" class="save save_custom_item" value="SAVE">
        <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/custom" class="cancel">CANCEL</a>
      </div>
      </form>
      
    </div>
  </div>