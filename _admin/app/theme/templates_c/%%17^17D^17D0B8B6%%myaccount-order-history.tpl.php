<?php /* Smarty version 2.6.25, created on 2020-09-30 01:11:36
         compiled from myaccount-order-history.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'myaccount-order-history.tpl', 2, false),)), $this); ?>
         <div class="order-history">
         <?php if (count($this->_tpl_vars['orderhistory']) > 0): ?> 
		        
         <?php $_from = $this->_tpl_vars['orderhistory']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['history']):
?>
			<ul class="order-history-inner">
	            <li> <span class="section1">
	              <h3><?php echo $this->_tpl_vars['history']->delivery_type; ?>
</h3>
	              <h4><?php echo $this->_tpl_vars['history']->date; ?>
</h4>
	              <a href="#" data-order="<?php echo $this->_tpl_vars['history']->order_number; ?>
" data-order-id="<?php echo $this->_tpl_vars['history']->order_id; ?>
" class="view-details">VIEW DETAILS</a> </span> </li>
	            <li> <span class="section2">
	              <h3>
						<?php if ($this->_tpl_vars['history']->address->address1 != ""): ?><?php echo $this->_tpl_vars['history']->address->address1; ?>
<?php endif; ?>
					  <?php if ($this->_tpl_vars['history']->address->address2 != ""): ?>, <?php echo $this->_tpl_vars['history']->address->address2; ?>
<?php endif; ?>
					  <?php if ($this->_tpl_vars['history']->address->zip != ""): ?>, <?php echo $this->_tpl_vars['history']->address->zip; ?>
<?php endif; ?>
	              </h3>
	              <p>
					<?php $_from = $this->_tpl_vars['history']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
						(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 <br/>
					<?php endforeach; endif; unset($_from); ?>
	              </p>
	              </span> </li>
	            <li> <span class="section3">
	              <h3>$<?php echo $this->_tpl_vars['history']->total; ?>
</h3>
	              </span> </li>
	            <li> <span class="section4"> <a href="javascript:void(0);" data-user-id="<?php echo $this->_tpl_vars['history']->user_id; ?>
" data-order="<?php echo $this->_tpl_vars['history']->order_number; ?>
" data-order-id="<?php echo $this->_tpl_vars['history']->order_id; ?>
"class="reorder">REORDER</a> </span> </li>
			</ul>			
         <?php endforeach; endif; unset($_from); ?>
         
        <?php else: ?>
             <ul class="order-history-inner">
				
	            <li style="width:100%;"><span><h3 class="no_order_history">No Previous Orders.</h3></span></li>	            
	        </ul>    
			
         <?php endif; ?>
        </div>
      </div>
</div>