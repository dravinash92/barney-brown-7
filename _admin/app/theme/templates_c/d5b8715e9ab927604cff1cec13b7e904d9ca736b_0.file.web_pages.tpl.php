<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:36:52
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\web_pages.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c83e45f7580_10025634',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd5b8715e9ab927604cff1cec13b7e904d9ca736b' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\web_pages.tpl',
      1 => 1600125374,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c83e45f7580_10025634 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
     <div class="webpage">
       <h1>WEB PAGES</h1>
       
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th>&nbsp;</th>
          <th colspan="2">WEBSITE PAGES</th>
          <th>Order</th>
          <th>Status</th>
        </tr>
        
        <tr class="even">
          <td>&nbsp;</td>
          <td width="94%">Homepage</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="6%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
web/homepage/">edit</a></td>
        </tr>
         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['webpageData']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
        <tr>
          <td>&nbsp;</td>
          <td width="60%"><?php echo $_smarty_tpl->tpl_vars['v']->value->webpage_name;?>
</td>
          <td width="10%"><?php echo $_smarty_tpl->tpl_vars['v']->value->orders;?>
</td>
          <td width="10%"><?php if ($_smarty_tpl->tpl_vars['v']->value->status == 1) {?> Enabled <?php } else { ?> Disabled <?php }?></td>
          <td width="6%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
web/webpage/<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
">edit</a></td>
        </tr>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        
      </tbody></table>

        
    </div>
  </div>
<?php }
}
