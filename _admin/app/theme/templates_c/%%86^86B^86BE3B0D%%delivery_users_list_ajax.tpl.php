<?php /* Smarty version 2.6.25, created on 2019-11-14 08:45:42
         compiled from delivery_users_list_ajax.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'delivery_users_list_ajax.tpl', 13, false),)), $this); ?>

       <form id="updateDeliveryUsersLive" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/updateDeliveryUsersLive">
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="7%">&nbsp;</th>
          <th>USER</th>
          <th>ASSIGNED STORES</th>
          <th># DELIVERIES</th>
          <th> ORDER TOTALS</th>
          <th colspan="2"></th>
        </tr>
      <?php $this->assign('key', 1); ?>
        <?php unset($this->_sections['options']);
$this->_sections['options']['name'] = 'options';
$this->_sections['options']['start'] = (int)0;
$this->_sections['options']['loop'] = is_array($_loop=count($this->_tpl_vars['delivery_users'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['options']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['options']['show'] = true;
$this->_sections['options']['max'] = $this->_sections['options']['loop'];
if ($this->_sections['options']['start'] < 0)
    $this->_sections['options']['start'] = max($this->_sections['options']['step'] > 0 ? 0 : -1, $this->_sections['options']['loop'] + $this->_sections['options']['start']);
else
    $this->_sections['options']['start'] = min($this->_sections['options']['start'], $this->_sections['options']['step'] > 0 ? $this->_sections['options']['loop'] : $this->_sections['options']['loop']-1);
if ($this->_sections['options']['show']) {
    $this->_sections['options']['total'] = min(ceil(($this->_sections['options']['step'] > 0 ? $this->_sections['options']['loop'] - $this->_sections['options']['start'] : $this->_sections['options']['start']+1)/abs($this->_sections['options']['step'])), $this->_sections['options']['max']);
    if ($this->_sections['options']['total'] == 0)
        $this->_sections['options']['show'] = false;
} else
    $this->_sections['options']['total'] = 0;
if ($this->_sections['options']['show']):

            for ($this->_sections['options']['index'] = $this->_sections['options']['start'], $this->_sections['options']['iteration'] = 1;
                 $this->_sections['options']['iteration'] <= $this->_sections['options']['total'];
                 $this->_sections['options']['index'] += $this->_sections['options']['step'], $this->_sections['options']['iteration']++):
$this->_sections['options']['rownum'] = $this->_sections['options']['iteration'];
$this->_sections['options']['index_prev'] = $this->_sections['options']['index'] - $this->_sections['options']['step'];
$this->_sections['options']['index_next'] = $this->_sections['options']['index'] + $this->_sections['options']['step'];
$this->_sections['options']['first']      = ($this->_sections['options']['iteration'] == 1);
$this->_sections['options']['last']       = ($this->_sections['options']['iteration'] == $this->_sections['options']['total']);
?>
        <tr>
          <td width="7%" align="center">
              <span class="multi-left">
               <input type="checkbox" id="checkbox-<?php echo $this->_tpl_vars['key']; ?>
-<?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['uid']; ?>
" class="input-checkbox" name="live[<?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['uid']; ?>
]" value="1" <?php if ($this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['live'] == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-<?php echo $this->_tpl_vars['key']; ?>
-<?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['uid']; ?>
" class="multisel-ckeck"></label>
              </span>
          </td>
          <td width="15%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
reports/showDeliveriesDetails/<?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['uid']; ?>
"><?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['name']; ?>
</a></td>
          <td width="18%"><?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['store_names']; ?>
</td>
          <td width="10%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
reports/showDeliveriesDetails/<?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['uid']; ?>
"><?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['deliveries']; ?>
</a></td>

          <td width="10%"><?php if ($this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['totals'] != ''): ?> $ <?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['totals']; ?>
 <?php else: ?>$ 0.00<?php endif; ?></td>
          
          <td width="5%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/editDeliveryUser/<?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['uid']; ?>
">edit</a></td>
          <td width="5%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/deleteDeliveryUser/<?php echo $this->_tpl_vars['delivery_users'][$this->_sections['options']['index']]['uid']; ?>
">delete</a></td>
        </tr>
         <?php $this->assign('key', $this->_tpl_vars['key']+1); ?>
       <?php endfor; endif; ?>

      </tbody></table>
      <a class="update" href="#" onclick="document.getElementById('updateDeliveryUsersLive').submit()">update</a>
      <a class="add-user" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/delivery_users/">ADD NEW USER</a>
      </form>
    </div>
  </div>