<?php
/* Smarty version 3.1.39, created on 2021-03-31 04:16:51
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\cart-user-address-load.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_60642ff3241640_09838642',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5bb210dd28f1674d7d089d15d3f344140de6704c' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\cart-user-address-load.tpl',
      1 => 1616680100,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60642ff3241640_09838642 (Smarty_Internal_Template $_smarty_tpl) {
?> <label>User Addresses</label>
    <select name="user_address" class="select user_address">
        <option value="">Select Address</option>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addresses']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->address_id;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
,<?php if ($_smarty_tpl->tpl_vars['v']->value->address1 != '') {?> <?php echo $_smarty_tpl->tpl_vars['v']->value->address1;?>
,<?php }?> <?php echo $_smarty_tpl->tpl_vars['v']->value->cross_streets;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->zip;?>
</option>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
    <input type="hidden" name="address_type"  value="delivery" />     
    <a href="javascript:void(0)" class="add-new-address add-address">add new address</a>
	<a href="javascript:void(0)" class="add-new-address edit-address">edit addresses</a>
<?php }
}
