<?php /* Smarty version 2.6.25, created on 2020-09-30 01:59:28
         compiled from customer_search.tpl */ ?>
<div class="container">
      <div class="customers">
      
       <h1>CUSTOMERS</h1>
       
       <form class="new-order-form" id="new-order-form" action="" method="post">
       	<div class="text-box-holder">
          <label>First Name</label>
          <input name="customername" id="customernameinput" type="text" class="main-text-box common-text-width">
          <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
customer/index/" id="customername" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Last Name</label>
          <input name="lastname" type="text" id="lastnameinput" class="main-text-box common-text-width">
          <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
customer/index/" id="lastname" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Phone Number</label>
          <input name="ph1" type="text" class="main-text-box phone-text-width">
          <input name="ph2" type="text" class="main-text-box phone-text-width">
          <input name="ph3" type="text" class="main-text-box phone-text-width">
          <input name="ph4" type="text" class="main-text-box phone-text-width">
        <!--<a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
customer/index/" class="arrow" id="phonesearch">&nbsp;</a>-->
        </div>
        <div class="text-box-holder">
          <label>Email Address</label>
          <input name="customeremail" type="text" id="customeremailinput" class="main-text-box common-text-width">
          <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
customer/index/" id="customeremail" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Company (Optional)</label>
          <input name="company" type="text" class="main-text-box common-text-width">
          <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
customer/index/" id="customercompany" class="arrow">&nbsp;</a>
        </div>
         <div class="text-box-holder">
       <input type="submit" class="new-account createaccount" value="New Account"/>
        </div>
            <div class="text-box-holder"> <span class="rerror_msg"></span> </div>
       </form>
       
       <!--<a href="javascript:void(0);"  class="new-account createaccount">New Account</a>-->
                    <input name="total_item_count" type="hidden" value="<?php echo $this->_tpl_vars['total_item_count']; ?>
" />
		<input type="hidden" name="search_condition" value="<?php echo $this->_tpl_vars['search_condition']; ?>
" class="search_condition" />
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminCustomerList">
          <tbody><tr>
            <th width="22%">CUSTOMER NAME</th>
            <th width="32%">EMAIL</th>
            <th width="18%">PHONE</th>
            <th width="15%">ORDERS</th>
            <th width="13%">SALES TOTAL</th>
          </tr>
          <?php unset($this->_sections['users']);
$this->_sections['users']['name'] = 'users';
$this->_sections['users']['loop'] = is_array($_loop=$this->_tpl_vars['users_data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['users']['show'] = true;
$this->_sections['users']['max'] = $this->_sections['users']['loop'];
$this->_sections['users']['step'] = 1;
$this->_sections['users']['start'] = $this->_sections['users']['step'] > 0 ? 0 : $this->_sections['users']['loop']-1;
if ($this->_sections['users']['show']) {
    $this->_sections['users']['total'] = $this->_sections['users']['loop'];
    if ($this->_sections['users']['total'] == 0)
        $this->_sections['users']['show'] = false;
} else
    $this->_sections['users']['total'] = 0;
if ($this->_sections['users']['show']):

            for ($this->_sections['users']['index'] = $this->_sections['users']['start'], $this->_sections['users']['iteration'] = 1;
                 $this->_sections['users']['iteration'] <= $this->_sections['users']['total'];
                 $this->_sections['users']['index'] += $this->_sections['users']['step'], $this->_sections['users']['iteration']++):
$this->_sections['users']['rownum'] = $this->_sections['users']['iteration'];
$this->_sections['users']['index_prev'] = $this->_sections['users']['index'] - $this->_sections['users']['step'];
$this->_sections['users']['index_next'] = $this->_sections['users']['index'] + $this->_sections['users']['step'];
$this->_sections['users']['first']      = ($this->_sections['users']['iteration'] == 1);
$this->_sections['users']['last']       = ($this->_sections['users']['iteration'] == $this->_sections['users']['total']);
?> 
   		 <tr>
            <td><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
customer/profile/<?php echo $this->_tpl_vars['users_data'][$this->_sections['users']['index']]['uid']; ?>
"><?php echo $this->_tpl_vars['users_data'][$this->_sections['users']['index']]['first_name']; ?>
 <?php echo $this->_tpl_vars['users_data'][$this->_sections['users']['index']]['last_name']; ?>
</a></td>
            <td><?php echo $this->_tpl_vars['users_data'][$this->_sections['users']['index']]['username']; ?>
</td>
            <td></td>
            <td><?php echo $this->_tpl_vars['users_data'][$this->_sections['users']['index']]['sandwich_count']; ?>
</td>
            <td>$<?php echo $this->_tpl_vars['users_data'][$this->_sections['users']['index']]['total_price']; ?>
</td>
          </tr>
		<?php endfor; endif; ?>  
		  
       </tbody></table>
<div class="loadMoreCustomer" style="display:none;text-align:center"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/star.png" alt="Loading" /> </div>

    </div>
     
  </div>
  <?php echo '
<style>
.register {
    border: 1px solid red !important;
}

.rerror_msg {
    color: red;
    font-size: 12px;
}
.loadMoreCustomer {
    float: left;
    display: block;
    width: 100%;
    margin: 20px auto 0;
}


</style>
<script>
$(document).ready(function(){
    $(window).scroll(function(){
if($(window).scrollTop() == $(document).height() - $(window).height() && $(".search_condition").val()==""){

                setTimeout(function() {
                    $(\'.loadMoreCustomer\').show();
        $current_count = $(\'.adminCustomerList tr\').length;
		//alert($current_count);
        $total_items = $(\'input[name="total_item_count"]\').val();
        

        if (parseInt($current_count) < parseInt($total_items)) {
            $.ajax({
                type: "POST",
                data: {
                	"count": $current_count,
                   
                },
                url: SITE_URL + \'customer/loadmoreCustomer\',
                success: function(e) {
                    $(\'.adminCustomerList tr:last\').after(e);
					$(\'.loadMoreCustomer\').hide();
                }
            });
        } else {
            $(\'.loadMoreCustomer\').hide();
        }

                }, 300);
            }    });
});
</script>
  '; ?>
