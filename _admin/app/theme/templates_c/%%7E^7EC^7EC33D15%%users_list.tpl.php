<?php /* Smarty version 2.6.25, created on 2020-09-30 00:22:44
         compiled from users_list.tpl */ ?>
<div class="container">
    <div class="admin-user-account">
       <h1>USER ACCOUNTS</h1>
       <form id="updateUsersLive" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/updateUsersLive">
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="7%">&nbsp;</th>
          <th colspan="1">USER</th>
          <th colspan="3">ACCOUNT TYPE</th>
        </tr>
      
       <?php $_from = $this->_tpl_vars['admin_users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
        <tr>
          <td width="7%" align="center">
              <span class="multi-left">
               <input type="checkbox" id="checkbox-<?php echo $this->_tpl_vars['key']; ?>
-<?php echo $this->_tpl_vars['v']->uid; ?>
" class="input-checkbox" name="live[<?php echo $this->_tpl_vars['v']->uid; ?>
]" value="1" <?php if ($this->_tpl_vars['v']->live == 1): ?> checked <?php endif; ?>>
               <label for="checkbox-<?php echo $this->_tpl_vars['key']; ?>
-<?php echo $this->_tpl_vars['v']->uid; ?>
" class="multisel-ckeck"></label>
              </span>
          </td>
          <td width="43%"><?php echo $this->_tpl_vars['v']->first_name; ?>
  <?php echo $this->_tpl_vars['v']->last_name; ?>
</td>
          <td width="33%"><?php echo $this->_tpl_vars['v']->cat_name; ?>
</td>
          <td width="5%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/editAdminUser/<?php echo $this->_tpl_vars['v']->uid; ?>
">edit</a></td>
          <td width="12%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/deleteAdminUser/<?php echo $this->_tpl_vars['v']->uid; ?>
">delete</a></td>
        </tr>
       <?php endforeach; endif; unset($_from); ?>

      </tbody></table>
      <a class="update" href="#" onclick="document.getElementById('updateUsersLive').submit()">update</a>
      <a class="add-user" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/newUser/">ADD NEW USER</a>
      </form>
    </div>
  </div>