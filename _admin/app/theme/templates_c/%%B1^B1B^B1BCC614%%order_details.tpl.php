<?php /* Smarty version 2.6.25, created on 2019-09-25 09:27:12
         compiled from order_details.tpl */ ?>
<div class="container">
    <div class="place-order">
       <h1>ORDER DETAILS</h1>
       
       <div class="place-order-coustomer-info">
       		<div class="address-details">
           <h3>CUSTOMER INFO</h3>
			<p>Name: <span><?php echo $this->_tpl_vars['orderDetails']->first_name; ?>
 <?php echo $this->_tpl_vars['orderDetails']->last_name; ?>
</span> <br>
              Email: <span><?php echo $this->_tpl_vars['orderDetails']->username; ?>
</span><br/>
              Phone: <span><?php echo $this->_tpl_vars['orderDetails']->phone; ?>
</span><br/> 
              Company: <span><?php echo $this->_tpl_vars['orderDetails']->company; ?>
</span><br/> 
            </p>   
            </div>                          
        <!--</div>-->
       
       <!--<div class="place-order-coustomer-info">-->
       		<div class="address-details">
           <h3>ORDER INFO</h3>
           <p>Sub Total: <span>$<?php echo $this->_tpl_vars['orderDetails']->sub_total; ?>
</span> <br>
              Tax: <span>$<?php echo $this->_tpl_vars['orderDetails']->tax; ?>
</span><br/>
              Delivery Fee: <span>$<?php echo $this->_tpl_vars['orderDetails']->delivery_fee; ?>
</span><br/>
              Tip: <span>$<?php echo $this->_tpl_vars['orderDetails']->tip; ?>
</span><br/>
              Off Amount: <span>$<?php echo $this->_tpl_vars['orderDetails']->off_amount; ?>
</span><br/>              
              Total: <span>$<?php echo $this->_tpl_vars['orderDetails']->total; ?>
</span></p>           
            </div>  
         <!--</div>-->
        
        
       <!--<div class="place-order-coustomer-info">-->
       		<div class="address-details">
           <h3>ADDRESS INFO</h3>
			<p>
				Name: <span><?php echo $this->_tpl_vars['orderDetails']->address->name; ?>
</span> <br>
				Company: <span><?php echo $this->_tpl_vars['orderDetails']->address->company; ?>
</span> <br>						
				Addess: <span>
						<?php if ($this->_tpl_vars['orderDetails']->address->address1 != ""): ?><?php echo $this->_tpl_vars['orderDetails']->address->address1; ?>
<?php endif; ?>
						<?php if ($this->_tpl_vars['orderDetails']->address->address2 != ""): ?>, <?php echo $this->_tpl_vars['orderDetails']->address->address2; ?>
<?php endif; ?>
						<?php if ($this->_tpl_vars['orderDetails']->address->street != ""): ?>, <?php echo $this->_tpl_vars['orderDetails']->address->street; ?>
<?php endif; ?>
						<?php if ($this->_tpl_vars['orderDetails']->address->cross_streets != ""): ?>, <?php echo $this->_tpl_vars['orderDetails']->address->cross_streets; ?>
<?php endif; ?>		</span> <br>
				Zip: <span><?php echo $this->_tpl_vars['orderDetails']->address->zip; ?>
</span> <br>
				Phone: <span><?php echo $this->_tpl_vars['orderDetails']->address->phone; ?>
</span> <br>	
				Delivery Instruction: <span><?php echo $this->_tpl_vars['orderDetails']->address->delivery_instructions; ?>
</span> <br>	
            </p>
            </div>           
        </div>
        
        
        <div class="place-order-coustomer-info">
       		<div class="address-details">
           <h3>ITEMS INFO</h3>
			<?php $_from = $this->_tpl_vars['orderDetails']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>		
				<p>Item Name: <span><?php echo $this->_tpl_vars['item']->name; ?>
</span> <br>
				  Quantity: <span><?php echo $this->_tpl_vars['item']->qty; ?>
</span><br/>				 
				</p> 
				<hr/>
            <?php endforeach; endif; unset($_from); ?>  
            </div>                          
        </div>
        
        
    </div>
</div>         