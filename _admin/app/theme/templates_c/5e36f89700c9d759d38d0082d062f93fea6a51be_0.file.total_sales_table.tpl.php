<?php
/* Smarty version 3.1.39, created on 2021-03-31 02:24:40
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\total_sales_table.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_606415a8be5a78_84323397',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5e36f89700c9d759d38d0082d062f93fea6a51be' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\total_sales_table.tpl',
      1 => 1616681400,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606415a8be5a78_84323397 (Smarty_Internal_Template $_smarty_tpl) {
?>	<?php if (count($_smarty_tpl->tpl_vars['orders']->value) > 0) {?>
		<?php 	
			$items_total= $tax_total = $sub_total = $delivery_fee_total  = $tip_total = $total = $dscnt_total = $gft_total = $refund = $paid = 0;
		?>	
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr>
			  <th>ORDER #</th>
			  <th>DATE/TIME</th>
			  <th>CUSTOMER NAME</th>
			  <!-- <th>ITEMS</th> -->
			  <th>SUBTOT</th>
			  <th>TAX</th>
			  <th>FEE</th>
			  <th>TIP</th>
			  <th>TOTAL</th>
			  <th>DSCNT</th>
			  <th>GFT CRD</th>
			  <th>REFUND</th>
			  <th>PAID</th>
			</tr>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orders']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<?php $_smarty_tpl->_assignInScope('item_total', $_smarty_tpl->tpl_vars['v']->value->item_total-$_smarty_tpl->tpl_vars['v']->value->total_tax);?>				
				<?php $_smarty_tpl->_assignInScope('tax', $_smarty_tpl->tpl_vars['v']->value->total_tax);?>
				<?php $_smarty_tpl->_assignInScope('sub', $_smarty_tpl->tpl_vars['v']->value->sub_total);?>
				<?php $_smarty_tpl->_assignInScope('delivery_fee', $_smarty_tpl->tpl_vars['v']->value->delivery_fee);?>
				<?php $_smarty_tpl->_assignInScope('tip', $_smarty_tpl->tpl_vars['v']->value->tip);?>
				<?php $_smarty_tpl->_assignInScope('calc_total', $_smarty_tpl->tpl_vars['v']->value->calc_total);?>
				<?php $_smarty_tpl->_assignInScope('off_amount', $_smarty_tpl->tpl_vars['v']->value->off_amount);?>
				<?php $_smarty_tpl->_assignInScope('total', $_smarty_tpl->tpl_vars['v']->value->total);?>
				<?php 
					$items_total += $_smarty_tpl->get_template_vars('item_total');
					$tax_total += $_smarty_tpl->get_template_vars('tax');
					$sub_total += $_smarty_tpl->get_template_vars('sub');
					$delivery_fee_total += $_smarty_tpl->get_template_vars('delivery_fee');
					$tip_total += $_smarty_tpl->get_template_vars('tip');
					$calc_total += $_smarty_tpl->get_template_vars('calc_total');
					$dscnt_total += $_smarty_tpl->get_template_vars('off_amount');
					$paid += $_smarty_tpl->get_template_vars('total');
				?>			
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			
			<tr class="subheader">
			  <td colspan="3">TOTAL</td>
			  <!-- <td>$<?php echo sprintf('%0.2f',$items_total)?></td> -->
			  <td>$<?php echo sprintf('%0.2f',$sub_total)?></td>
			  <td>$<?php echo sprintf('%0.2f',$tax_total)?></td>
			  <td>$<?php echo sprintf('%0.2f',$delivery_fee_total)?></td>
			  <td>$<?php echo sprintf('%0.2f',$tip_total)?></td>
			  <td>$<?php echo sprintf('%0.2f',$calc_total+$tax_total)?></td>
			  <td>-$<?php echo sprintf('%0.2f',$dscnt_total)?></td>
			  <td>-$0.00</td>
			  <td>-$0.00</td>
			  <td>$<?php echo sprintf('%0.2f',$calc_total+$tax_total-$dscnt_total)?></td>
			</tr>			
			
			
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orders']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<?php if ((1 & $_smarty_tpl->tpl_vars['k']->value)) {?> 
				<tr class="">
				<?php } else { ?>	
				<tr class="even">
				<?php }?>	
				  <td><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
reports/showOrderDetails/<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
</a></td>
				  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->o_date;?>
</td>
				  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['v']->value->last_name;?>
</td>
				<!--   <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->amount_wt);?>
</td> -->
				  <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->sub_total);?>
</td>
				  <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->total_tax);?>
</td>
				  <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_fee;?>
</td>
				  <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->tip;?>
</td>
				  <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->calc_total+sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->total_tax);?>
</td>
				  <td>-$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->off_amount);?>
</td>
				  <td>-$0.00</td>
				  <td>-$0.00</td>
				  <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->total);?>
</td>
				</tr>				
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>					
			
			
			</tbody>
		</table>	
	<?php } else { ?>
		<h3>No orders found.</h3>	
	<?php }?>	
<?php }
}
