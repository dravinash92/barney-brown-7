<?php /* Smarty version 2.6.25, created on 2020-09-21 03:54:00
         compiled from pickups.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip', 'pickups.tpl', 95, false),)), $this); ?>
<div class="container">
    <div class="deliveries">
       <h1>PICK-UPS</h1>
       <div class="top-deliveries-wrapper">
       		<div class="left">
          		<table class="top-deliveries-table">
								<tbody><tr>
                  <td>NEW</td>
                 <td class="count_new"><?php echo $this->_tpl_vars['count_new']; ?>
</td>
                </tr>
                <tr>
                  <td>PRC</td>
                  <td class="count_prc"><?php echo $this->_tpl_vars['count_prc']; ?>
</td>
                </tr>
                <tr>
                  <td>OUT</td>
                  <td class="count_bag"><?php echo $this->_tpl_vars['count_bag']; ?>
</td>
                </tr>
                <tr>
                  <td>DEL</td>
                  <td class="count_pick"><?php echo $this->_tpl_vars['count_pick']; ?>
</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td class="total_pickUps" ><?php echo $this->_tpl_vars['total_pickUps']; ?>
</td>
                </tr>
              </tbody></table>

          </div>
          <div class="right">
            <form action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
pickups/searchPickupsData" name="delivery_search" method="POST">
          	<div class="text-box-holder">
            	<label>Store Location</label>
              <select name="store_id" id="store_id" class="select">
				<option value="">Select Store</option>
				<?php $_from = $this->_tpl_vars['pickupstores']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
					<option value="<?php echo $this->_tpl_vars['v']->id; ?>
" <?php if ($this->_tpl_vars['data']['store_id'] == $this->_tpl_vars['v']->id): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['v']->store_name; ?>
, <?php echo $this->_tpl_vars['v']->address1; ?>
, <?php echo $this->_tpl_vars['v']->address2; ?>
, <?php echo $this->_tpl_vars['v']->zip; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>	
              </select>
          	</div>
            <div class="text-box-holder">
            	<label>Status</label>
              <select name="delivery_status" id="1" class="select">
              <option value="">--Select--</option>
              <option value="0">New</option>
              	<?php $_from = $this->_tpl_vars['getOrderSatusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
                <option value="<?php echo $this->_tpl_vars['b']->id; ?>
" <?php if ($this->_tpl_vars['del_status'] == $this->_tpl_vars['b']->id): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['b']->label; ?>
</option>
              <?php endforeach; endif; unset($_from); ?>    
              </select>
          	</div>
            <div class="text-box-holder2">
            	<label>Date</label>
              <input name="search_date" id="search_pickup_date" type="text" class="text-box" value="<?php echo $this->_tpl_vars['date']; ?>
">
              <a href="#" class="date-icon"></a>
          	</div>
          	 <input type="submit" name="submit" value="SEARCH" style="margin-top: 15px;margin-left: 26px;">
             </form>
            
          </div>
       </div>
        <?php if ($this->_tpl_vars['total_pickUps'] != 0): ?> 
       <div class="deliveries-table-detail-wrapper">
       	 <h2>NEW</h2>
         <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
         
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
          <?php $_from = $this->_tpl_vars['addressList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
          <?php if ($this->_tpl_vars['v']->order_status == 0): ?>
            <?php if ($this->_tpl_vars['v']->timediff > 45): ?>  
            <tr style="background: #cef2cc;">
            <?php elseif (( $this->_tpl_vars['v']->timediff <= 45 ) && ( $this->_tpl_vars['v']->timediff > 30 )): ?>
            <tr style="background:#fdf5e0;">
            <?php elseif ($this->_tpl_vars['v']->timediff <= 30): ?>
            <tr style="background:#fedfe4;">
            <?php endif; ?>
            <td><?php echo $this->_tpl_vars['v']->name; ?>

            	<h3><?php echo $this->_tpl_vars['v']->timeformat; ?>
</h3>
            	<p><?php echo $this->_tpl_vars['v']->dayname; ?>
</p>
              <p><?php echo $this->_tpl_vars['v']->date; ?>
</p>
              <p><a href="#" id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDet">View order details</a></p>
            </td>
            <td>
            	<p>	<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->store_name)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->store_name; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->phone)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->phone; ?>
<br><?php endif; ?>              
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->address1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->address1; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->address2)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->address2; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->zip)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->zip; ?>
</br><?php endif; ?>    
				</p>
				<p>
				<?php $_from = $this->_tpl_vars['v']->tab; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
					<?php echo $this->_tpl_vars['item']; ?>
 
				<?php endforeach; endif; unset($_from); ?>			
              </p>
            </td>
            <td>
				<?php $_from = $this->_tpl_vars['v']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
					<p>(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 </p>
				<?php endforeach; endif; unset($_from); ?>
            </td>
            <?php $_from = $this->_tpl_vars['getOrderSatusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
             <?php if ($this->_tpl_vars['b']->code != 'pickup_cancel'): ?>
            
             <td><a  onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
<?php if ($this->_tpl_vars['b']->code == 'pickup_prc'): ?>,<?php echo $this->_tpl_vars['v']->order_number; ?>
 <?php endif; ?>)" href="javascript:void(0)">	<img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box1.png" alt=""></td>
             <?php endif; ?>
           <?php if ($this->_tpl_vars['b']->code == 'pickup_cancel'): ?>
           <td>	<a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
)" href="javascript:void(0)">Cancel</a></td>
           <?php endif; ?>
	        
            <?php endforeach; endif; unset($_from); ?>
          </tr>
         <?php endif; ?> 
       <?php endforeach; endif; unset($_from); ?>
          
   
          
        </tbody></table>
				<h2>PROCESSED</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
          
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
           <?php $_from = $this->_tpl_vars['addressList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
          <?php if ($this->_tpl_vars['v']->order_status == '1'): ?>
          
          <tr>
             <td><?php echo $this->_tpl_vars['v']->name; ?>

            	<h3><?php echo $this->_tpl_vars['v']->timeformat; ?>
</h3>
            	<p><?php echo $this->_tpl_vars['v']->dayname; ?>
</p>
              <p><?php echo $this->_tpl_vars['v']->date; ?>
</p>
              <p><a href="#" id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDet">View order details</a></p>
            </td>
           
              <td>
				<p>	<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->store_name)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->store_name; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->phone)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->phone; ?>
<br><?php endif; ?>              
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->address1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->address1; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->address2)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->address2; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->zip)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->zip; ?>
</br><?php endif; ?>              
				</p>
				<p>
				<?php $_from = $this->_tpl_vars['v']->tab; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
					<?php echo $this->_tpl_vars['item']; ?>
 
				<?php endforeach; endif; unset($_from); ?>			
              </p>
            </td>
            <td>
				<?php $_from = $this->_tpl_vars['v']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
					<p>(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 </p>
				<?php endforeach; endif; unset($_from); ?>	
            </td>
             
          
           <?php $_from = $this->_tpl_vars['getOrderSatusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
           			<?php if ($this->_tpl_vars['v']->order_status == 1 && $this->_tpl_vars['b']->id == 1): ?>
           			  <td><a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,0)" href="javascript:void(0)"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
           			<?php endif; ?>
           		   <?php if ($this->_tpl_vars['b']->code != 'pickup_cancel' && $this->_tpl_vars['b']->code != 'pickup_prc'): ?>
             <td><a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
)" href="javascript:void(0)"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box1.png" alt=""></td>
             <?php endif; ?>
           <?php if ($this->_tpl_vars['b']->code == 'pickup_cancel'): ?>
           <td>	<a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
)" href="javascript:void(0)">Cancel</a></td>
           <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
          </tr>
           <?php endif; ?> 
       <?php endforeach; endif; unset($_from); ?>
        </tbody></table>
        <h2>BAGGED / READY</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
         <?php $_from = $this->_tpl_vars['addressList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
          <?php if ($this->_tpl_vars['v']->order_status == 2): ?>
          
          <tr>
             <td><?php echo $this->_tpl_vars['v']->name; ?>

            	<h3><?php echo $this->_tpl_vars['v']->time; ?>
</h3>
              <p><?php echo $this->_tpl_vars['v']->date; ?>
</p>
              <p><a href="#" id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDet">View order details</a></p>
            </td>
           
              <td>
				<p>	<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->store_name)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->store_name; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->phone)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->phone; ?>
<br><?php endif; ?>              
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->address1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->address1; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->address2)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->address2; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->zip)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->zip; ?>
</br><?php endif; ?>              
				</p>
				<p>
				<?php $_from = $this->_tpl_vars['v']->tab; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
					<?php echo $this->_tpl_vars['item']; ?>
 
				<?php endforeach; endif; unset($_from); ?>			
              </p>
            </td>
            <td>
				<?php $_from = $this->_tpl_vars['v']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
					<p>(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 </p>
				<?php endforeach; endif; unset($_from); ?>
     
            </td>
         <?php $_from = $this->_tpl_vars['getOrderSatusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['b']):
?>  
           			<?php if ($this->_tpl_vars['v']->order_status == 2 && $this->_tpl_vars['b']->id == 2): ?>
           			  <td><a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,0)" href="javascript:void(0)"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
           			   <td><a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,1)" href="javascript:void(0)"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
           			<?php endif; ?>
           		<?php if ($this->_tpl_vars['b']->code != 'pickup_cancel' && $this->_tpl_vars['b']->code != 'pickup_prc' && $this->_tpl_vars['b']->code != 'pickup_bag'): ?>
             <td><a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
)" href="javascript:void(0)"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box1.png" alt=""></td>
             <?php endif; ?>
           <?php if ($this->_tpl_vars['b']->code == 'pickup_cancel'): ?>
           <td>	<a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,<?php echo $this->_tpl_vars['b']->id; ?>
)" href="javascript:void(0)">Cancel</a></td>
           <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
          </tr>
           <?php endif; ?> 
       <?php endforeach; endif; unset($_from); ?>
          
        
          
        </tbody></table>
        <h2>PICKED UP</h2>
        <table class="deliveries-new-table border-bottom">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
        
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
          </tr>
          <tr>
           <?php $_from = $this->_tpl_vars['addressList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
          <?php if ($this->_tpl_vars['v']->order_status == 3): ?>
          
          <tr>
             <td><?php echo $this->_tpl_vars['v']->name; ?>

            	<h3><?php echo $this->_tpl_vars['v']->time; ?>
</h3>
              <p><?php echo $this->_tpl_vars['v']->date; ?>
</p>
              <p><a href="#" id="<?php echo $this->_tpl_vars['v']->order_number; ?>
" class="viewOrdDet">View order details</a></p>
            </td>
           
            <td>
				<p>	<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->store_name)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->store_name; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->phone)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->phone; ?>
<br><?php endif; ?>              
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->address1)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->address1; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->address2)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->address2; ?>
</br><?php endif; ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['v']->address->zip)) ? $this->_run_mod_handler('strip', true, $_tmp) : smarty_modifier_strip($_tmp))): ?><?php echo $this->_tpl_vars['v']->address->zip; ?>
</br><?php endif; ?>              
				</p>
				<p>
				<?php $_from = $this->_tpl_vars['v']->tab; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
					<?php echo $this->_tpl_vars['item']; ?>
 
				<?php endforeach; endif; unset($_from); ?>			
              </p>
            </td>
            <td>
				<?php $_from = $this->_tpl_vars['v']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>  
					<p>(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php echo $this->_tpl_vars['item']->name; ?>
 </p>
				<?php endforeach; endif; unset($_from); ?>
            </td>
          
          
            <td><a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,0)" href="javascript:void(0)"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,1)" href="javascript:void(0)""><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="managePickupData(<?php echo $this->_tpl_vars['v']->order_id; ?>
,2)" href="javascript:void(0)""><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/deliveries-box.png" alt=""></a></td>
              
          </tr>
             <?php endif; ?> 
       <?php endforeach; endif; unset($_from); ?>
         
          
        </tbody></table>
       </div>
       <?php endif; ?>
      <?php if ($this->_tpl_vars['total_pickUps'] == 0): ?><div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div><?php endif; ?>
    </div>
  </div>