<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:37:35
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\customer_profile.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c840f61d7e8_67331299',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '62ece01c4873452f15501486b3b4b42c03364aa0' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\customer_profile.tpl',
      1 => 1600125388,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c840f61d7e8_67331299 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container">
      <div class="customer-profile">
        <h1><?php echo $_smarty_tpl->tpl_vars['udata']->value['first_name'];?>
  <?php echo $_smarty_tpl->tpl_vars['udata']->value['last_name'];?>
</h1>
        <a class="create-new-order" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/addtocart/<?php echo $_smarty_tpl->tpl_vars['UID']->value;?>
" >Create new Order</a>
        <nav>
          <ul class="admin_customer_tabs">
            <li><a href="#" class="active">PROFILE</a></li>
            <li><a href="#">ORDER HISTORY</a></li>
          <!--  <li><a href="#">SAVED MEALS</a></li>-->
           <li><a href="#">SAVED ADDRESSES</a></li> 
            <li><a href="#">SAVED BILLING</a></li>
            <!--<li><a href="#">MEAL REMINDER</a></li>-->
          </ul>
        </nav>
        
     <div class="profile-wrapper">        
        <input type="hidden" name="hiduid" value="<?php echo $_smarty_tpl->tpl_vars['UID']->value;?>
" />
      <div class="ajxwrp"> 
		<h3>Profile</h3>
        <div class="account">
          <h4>ACCOUNT INFO</h4>
         
          <ul>
            <li>
               <label>Name: <span><?php echo $_smarty_tpl->tpl_vars['udata']->value['first_name'];?>
  <?php echo $_smarty_tpl->tpl_vars['udata']->value['last_name'];?>
</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
            <li>
               <label> Email Address: <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['udata']->value['username'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['udata']->value['username'];?>
<span></a></label>
               <a href="#" class="edit">EDIT</a>
            </li>
             <li>
               <label>Password: <span>xxxxxxx</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
            <!--
             <li>
               <label>Phone: <span><?php echo $_smarty_tpl->tpl_vars['udata']->value['phone'];?>
</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
             <li>
               <label>Company: <span><?php echo $_smarty_tpl->tpl_vars['udata']->value['company'];?>
</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>-->
          </ul>
        </div>
        <!--
        <div class="tell-us">
          <h4>TELL US ABOUT YOUR EXPERIENCE</h4>
           <form id="user_review">
          <ul>
            <li>
               <label>How did you hear about us?  </label>
               <div class="bgforSelect">
                <select class="wid-input" name="how_know">
                  <option value="">Choose One</option>
                  
                  
                  <?php
$__section_users_review_content_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['user_review_contents']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_users_review_content_0_total = $__section_users_review_content_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_users_review_content'] = new Smarty_Variable(array());
if ($__section_users_review_content_0_total !== 0) {
for ($__section_users_review_content_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_users_review_content']->value['index'] = 0; $__section_users_review_content_0_iteration <= $__section_users_review_content_0_total; $__section_users_review_content_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_users_review_content']->value['index']++){
?> 
                   <option value="<?php echo $_smarty_tpl->tpl_vars['user_review_contents']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users_review_content']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users_review_content']->value['index'] : null)]['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['user_review']->value['how_know'] == $_smarty_tpl->tpl_vars['user_review_contents']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users_review_content']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users_review_content']->value['index'] : null)]['id']) {?> selected="selected" <?php }?>><?php echo $_smarty_tpl->tpl_vars['user_review_contents']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users_review_content']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users_review_content']->value['index'] : null)]['review_text'];?>
</option>
                   <?php
}
}
?>
                   </select>
              </div>
            </li>
           <li>
               <label>How would you rate your ordering experience?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_order" >
                  <option value="">Choose One</option>

                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 1) {?> selected="selected" <?php }?>  value="1">1</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 2) {?> selected="selected" <?php }?>  value="2">2</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 3) {?> selected="selected" <?php }?>  value="3">3</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 4) {?> selected="selected" <?php }?>  value="4">4</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 5) {?> selected="selected" <?php }?>  value="5">5</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 6) {?> selected="selected" <?php }?>  value="6">6</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 7) {?> selected="selected" <?php }?>  value="7">7</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 8) {?> selected="selected" <?php }?>  value="8">8</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 9) {?> selected="selected" <?php }?>  value="9">9</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_order'] == 10) {?> selected="selected" <?php }?>  value="10">10</option>
                
                </select>
              </div>
            </li>
             <li>
               <label>How would you rate your customer service experience?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_customer_service" >
                  <option value="">Choose One</option>
                
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 1) {?> selected="selected" <?php }?>  value="1">1</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 2) {?> selected="selected" <?php }?>  value="2">2</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 3) {?> selected="selected" <?php }?>  value="3">3</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 4) {?> selected="selected" <?php }?>  value="4">4</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 5) {?> selected="selected" <?php }?>  value="5">5</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 6) {?> selected="selected" <?php }?>  value="6">6</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 7) {?> selected="selected" <?php }?>  value="7">7</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 8) {?> selected="selected" <?php }?>  value="8">8</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 9) {?> selected="selected" <?php }?>  value="9">9</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_customer_service'] == 10) {?> selected="selected" <?php }?>  value="10">10</option>
                
                
                </select>
              </div>
            </li>
            
            <li>
               <label>How would you rate the quality of your food?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_food" >
                  <option value="">Choose One</option>
                 
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 1) {?> selected="selected" <?php }?>  value="1">1</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 2) {?> selected="selected" <?php }?>  value="2">2</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 3) {?> selected="selected" <?php }?>  value="3">3</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 4) {?> selected="selected" <?php }?>  value="4">4</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 5) {?> selected="selected" <?php }?>  value="5">5</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 6) {?> selected="selected" <?php }?>  value="6">6</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 7) {?> selected="selected" <?php }?>  value="7">7</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 8) {?> selected="selected" <?php }?>  value="8">8</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 9) {?> selected="selected" <?php }?>  value="9">9</option>
                <option <?php if ($_smarty_tpl->tpl_vars['user_review']->value['rating_food'] == 10) {?> selected="selected" <?php }?>  value="10">10</option>
                
                
                </select>
              </div>
            </li>
            <li>
				<label>What can we do to be better?</label>
				<?php
$__section_reviewmessages_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['user_review_messages']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_reviewmessages_1_total = $__section_reviewmessages_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_reviewmessages'] = new Smarty_Variable(array());
if ($__section_reviewmessages_1_total !== 0) {
for ($__section_reviewmessages_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_reviewmessages']->value['index'] = 0; $__section_reviewmessages_1_iteration <= $__section_reviewmessages_1_total; $__section_reviewmessages_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_reviewmessages']->value['index']++){
?>
				<p><?php echo $_smarty_tpl->tpl_vars['user_review_messages']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_reviewmessages']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_reviewmessages']->value['index'] : null)]['message'];?>
</p> <br>
				<?php
}
}
?>
				</li>
            <li>
				<input type="hidden" name="uid" value="<?php echo $_smarty_tpl->tpl_vars['udata']->value['uid'];?>
" />
				<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['user_review']->value['id'];?>
" />
				<input type="button" class="submit savereview" name="savereview"  value="Submit" />
            </li>
          </ul>
          </form>
        </div>
        -->
         </div>         
     
     </div>
    
    </div>
     
  </div>
<?php }
}
