<?php
/* Smarty version 3.1.39, created on 2021-03-31 04:14:59
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\storedeliveriesajax.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_60642f83517630_87469632',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c22d08eeade8cbdb9a325b5888a2c1b4c0032d0d' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\storedeliveriesajax.tpl',
      1 => 1616681365,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60642f83517630_87469632 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    
 <?php if ($_smarty_tpl->tpl_vars['total_delivery']->value != 0) {?> 
       <div class="deliveries-table-detail-wrapper">
         <h2>NEW</h2>
         <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
          
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
          
        
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DeliveryDataList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 0) {?>
          
          <?php if ($_smarty_tpl->tpl_vars['v']->value->timediff > 45 && $_smarty_tpl->tpl_vars['v']->value->time == 1) {?>  
        <tr style="background: #cef2cc;">
  <?php } elseif ($_smarty_tpl->tpl_vars['v']->value->timediff <= 45 && $_smarty_tpl->tpl_vars['v']->value->timediff > 30 && $_smarty_tpl->tpl_vars['v']->value->time == 1) {?>
  <tr style="background:#fffcb5;">
    <?php } elseif ($_smarty_tpl->tpl_vars['v']->value->timediff <= 30 && $_smarty_tpl->tpl_vars['v']->value->time == 1) {?>
    <tr style="background:#fedfe4;">
      <?php } else { ?>
      <tr>
      <?php }?>
            <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
   
              <h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
             <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->address->name;?>
<br>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->company != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->company;?>
</br><?php }?>
          <?php echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
<br>              
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->address1 != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->address2 != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->zip != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>
              
              </p>
            </td>
            <td>
           
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
        <p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->modifiers)) && ($_smarty_tpl->tpl_vars['item']->value->modifiers != '' && $_smarty_tpl->tpl_vars['item']->value->modifiers != 0)) {?>
          <div class="item-modifier">Modifiers: <?php echo $_smarty_tpl->tpl_vars['item']->value->modifiers;?>
</div>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->spl_instructions)) && $_smarty_tpl->tpl_vars['item']->value->spl_instructions != '') {?>
          <div class="item-modifier">Special Instructions: <?php echo $_smarty_tpl->tpl_vars['item']->value->spl_instructions;?>
</div>
        <?php }?>
      <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             
            </td>
        
           <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
               <?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Cancel') {?>
                 <td><a id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDetStore" onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;
if ($_smarty_tpl->tpl_vars['b']->value->id == 5) {?>,<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;
}?>)" href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt="">
                 
                 </td>
                 <?php }?>
               <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'Delivery_Cancel') {?>
               <td></td>
               <?php }?>
              
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         
          </tr>  
         <?php }?>
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          
          
        </tbody></table>
        <h2>PROCESSED</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
       
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DeliveryDataList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 5) {?>
           <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

              <h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
               <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->address->name;?>
<br>
              <?php echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
<br>              
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->address1 != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->address2 != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->zip != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>
              
              </p>
            </td>
            <td>
           
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
        <p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->modifiers)) && ($_smarty_tpl->tpl_vars['item']->value->modifiers != '' && $_smarty_tpl->tpl_vars['item']->value->modifiers != 0)) {?>
          <div class="item-modifier">Modifiers: <?php echo $_smarty_tpl->tpl_vars['item']->value->modifiers;?>
</div>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->spl_instructions)) && $_smarty_tpl->tpl_vars['item']->value->spl_instructions != '') {?>
          <div class="item-modifier">Special Instructions: <?php echo $_smarty_tpl->tpl_vars['item']->value->spl_instructions;?>
</div>
        <?php }?>
      <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             
            </td>
        
        
             <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
                  <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 5 && $_smarty_tpl->tpl_vars['b']->value->id == 5) {?>
                <td><a onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,0)" href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Cancel' && $_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Pro') {?>
                <td style="position: relative;"><a onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
,'',<?php echo $_smarty_tpl->tpl_vars['v']->value->order_status;?>
)" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt=""></a>
                   <div class="assign-store-order" id="storedelivery_user_assign_<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
" style="display: none;">
                  <i class="close-btn" id="close-storeassign-user_<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
">close</i>
                  <div class="assign-title">
                    <h1>ASSIGN ORDER</h1>
                  </div>
                  <div class="asign-order-select">
                    <select class="storedeliveryUserOptions_<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
"> 
                      <option value="0">SELECT</option>
                    </select>
                  </div>
                  <div class="assign-submit">
                    <button class="assign_storedel_user" data-orderid= "<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
">Assign</button>
                  </div>
                </div>
                </td>
              <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'Delivery_Cancel') {?>
                 <td></td>
             <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
       
          </tr>  
         <?php }?>
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         
          
        </tbody></table>
        <h2>OUT FOR DELIVERY</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>   
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DeliveryDataList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 6) {?>
          <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

              <h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
               <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->address->name;?>
<br>
              <?php echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
<br>              
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->address1 != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->address2 != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->zip != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>
              
              </p>
            </td>
            <td>
           
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
        <p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->modifiers)) && ($_smarty_tpl->tpl_vars['item']->value->modifiers != '' && $_smarty_tpl->tpl_vars['item']->value->modifiers != 0)) {?>
          <div class="item-modifier">Modifiers: <?php echo $_smarty_tpl->tpl_vars['item']->value->modifiers;?>
</div>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->spl_instructions)) && $_smarty_tpl->tpl_vars['item']->value->spl_instructions != '') {?>
          <div class="item-modifier">Special Instructions: <?php echo $_smarty_tpl->tpl_vars['item']->value->spl_instructions;?>
</div>
        <?php }?>
      <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             
            </td>
        
           <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
              <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 6 && $_smarty_tpl->tpl_vars['b']->value->id == 6) {?>
                  <td><a onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,5)" href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a>
                  </td>
                   <td><a onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,5)" href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a>
                   </td>
                <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Cancel' && $_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Pro' && $_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_out') {?>
             <td><a onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
)" href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt=""></td>
             <?php }?>
           <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'Delivery_Cancel') {?>
           <td> </td>
           <?php }?>
              
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         
          </tr>  
           <tr class="no-border-up">
  <td></td>
  <td></td>
  <td></td>
<td><b class="bold">Assigned: </b></td>
  <?php if ($_smarty_tpl->tpl_vars['v']->value->delivery_assigned_to != 0) {?>
  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_uname;?>
</td>
  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_assigned_time;?>
</td>
  <?php } else { ?>
  <td></td>
  <td></td>
  <?php }?>
  <td></td>


</tr> 
         <?php }?>
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        
          
        </tbody></table>
        <h2>DELIVERED</h2>
        <table class="deliveries-new-table border-bottom">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
           
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DeliverySortDataList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 7) {?>
           <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

              <h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
               <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->address->name;?>
<br>
              <?php echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
<br>              
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->address1 != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->address2 != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->zip != '') {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>
              
              </p>
            </td>
            <td>
           
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
        <p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->modifiers)) && ($_smarty_tpl->tpl_vars['item']->value->modifiers != '' && $_smarty_tpl->tpl_vars['item']->value->modifiers != 0)) {?>
          <div class="item-modifier">Modifiers: <?php echo $_smarty_tpl->tpl_vars['item']->value->modifiers;?>
</div>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->spl_instructions)) && $_smarty_tpl->tpl_vars['item']->value->spl_instructions != '') {?>
          <div class="item-modifier">Special Instructions: <?php echo $_smarty_tpl->tpl_vars['item']->value->spl_instructions;?>
</div>
        <?php }?>
      <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             
            </td>
        
        <td><a onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,6)" href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,6)" href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="manageStoreDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,6)" href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
            <td></td>
          </tr>  
          <tr class="no-border-up">
  <td></td>
  <td></td>
  <td></td>
<td><b class="bold">Delivered: </b></td>
 <?php if ($_smarty_tpl->tpl_vars['v']->value->delivery_assigned_to != 0) {?>
  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_uname;?>
</td>
  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->order_delivered;?>
</td>
  <?php } else { ?>
  <td></td>
  <td></td>
  <?php }?>
  <td></td>


</tr> 
         <?php }?>
       <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        
          
        </tbody></table>
       </div>
       <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['total_delivery']->value == 0) {?><div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div><?php }?>
    </div>
  </div>

<?php }
}
