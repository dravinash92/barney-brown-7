<?php /* Smarty version 2.6.25, created on 2020-08-21 14:07:43
         compiled from sandwich_gallery.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'sandwich_gallery.tpl', 83, false),array('modifier', 'is_array', 'sandwich_gallery.tpl', 134, false),array('modifier', 'in_array', 'sandwich_gallery.tpl', 135, false),)), $this); ?>
<div class="container">
     <div class="sandwich-gallery">
       <div class="heading">
         <h1>SANDWICH GALLERY</h1>
         
         <div class="search">
           <p>Search</p>
           <input type="text" name="search_ingallery" > <a class="search-button" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/index/"><img width="26" height="26" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/link.png"> </a>
           <input type="hidden" id="searchTerm" value="<?php echo $this->_tpl_vars['serch_term']; ?>
">
         </div>
       </div>
       
       <div class="leftside-bar">
          <h2>Sort By</h2>
          
           <div class="bgforSelect">
                <select id="" name="" class="wid-input sortby">
                   <option value="1" <?php if ($this->_tpl_vars['sort_id'] == 1): ?> selected="selected" <?php endif; ?>>MOST RECENT</option>
                   <option value="2" <?php if ($this->_tpl_vars['sort_id'] == 2): ?> selected="selected" <?php endif; ?>>MOST LIKED</option>
                   <option value="3" <?php if ($this->_tpl_vars['sort_id'] == 3): ?> selected="selected" <?php endif; ?>>MOST POPULAR</option>
                </select>
              </div>
          
<?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i'] => $this->_tpl_vars['category']):
?>
	            
	            <?php $_from = $this->_tpl_vars['category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['data']):
?>
	              <?php if ($this->_tpl_vars['data']->category_identifier): ?>
         			<div class="items">
           				<h3><?php echo $this->_tpl_vars['data']->category_name; ?>
</h3>
            				<ul>
            				<?php if ($this->_tpl_vars['data']->category_name != 'bread'): ?>
	              <li>
	              <span class="multi-left">
                  <input type="checkbox"  value="null" name="check" id="check_<?php echo $this->_tpl_vars['k']; ?>
_00" class="input-checkbox">
                  <label for="check_<?php echo $this->_tpl_vars['k']; ?>
_00"  class="multisel-ckeck"></label></span>
                  <span style="width: 98px;" class="radio-text">No <?php echo $this->_tpl_vars['data']->category_name; ?>
</span>
                  </li>   
                 <?php endif; ?>  
                 <?php $_from = $this->_tpl_vars['categoryItems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j'] => $this->_tpl_vars['categoryItem']):
?>
		            
		            	<?php $_from = $this->_tpl_vars['categoryItem']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j'] => $this->_tpl_vars['dataitems']):
?>
                     <?php if ($this->_tpl_vars['data']->id == $this->_tpl_vars['dataitems']->category_id): ?>
                <li>
                 <span class="multi-left">
                  <input type="checkbox"  value="check1" name="check" id="check_<?php echo $this->_tpl_vars['k']; ?>
_<?php echo $this->_tpl_vars['j']; ?>
" class="input-checkbox">
                  <label for="check_<?php echo $this->_tpl_vars['k']; ?>
_<?php echo $this->_tpl_vars['j']; ?>
"></label></span>
                  <span style="width: 98px;" class="radio-text"><?php echo $this->_tpl_vars['dataitems']->item_name; ?>
</span>
                </li>
                <?php endif; ?>
              
                  <?php endforeach; endif; unset($_from); ?>
	                 
            <?php endforeach; endif; unset($_from); ?>
             </ul>
             </div>
              <?php endif; ?>  
               <?php endforeach; endif; unset($_from); ?>
	                 
            <?php endforeach; endif; unset($_from); ?>         

         
          
       </div>
       
       <div class="right-side">
       <input name="total_item_count" type="hidden" value="<?php echo $this->_tpl_vars['total_item_count']; ?>
" />
        <input name="sort_type" type="hidden" value="<?php echo $this->_tpl_vars['sort_id']; ?>
" />
       <table width="100%" cellspacing="0" cellpadding="0" border="0" class="adminGalleryList">
        <tbody><tr class="static">
          <th width="10%" height="31">SOTD</th>
          <th width="10%" height="31">TRND</th>
          <th width="10%" height="31">PUBLIC</th>
          <th width="38%">CUSTOM SANDWICHES</th>
          <th width="7%">LIKES</th>
          <th width="11%">PURCHASES</th>
          <th width="10%">PRICE</th>
          <th width="12%">FLAGGED</th>
          <th width="6%">&nbsp;</th>
          <th width="6%">&nbsp;</th>
        </tr>
        
        
        <?php if (count($this->_tpl_vars['GALLARY_DATA']) > 0): ?>
        
        <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['GALLARY_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
     
     
     
              <?php $this->assign('prot_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
              if($c){
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }}
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
      
      
                  <?php if (is_array($this->_supers['session']['orders']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'], $this->_supers['session']['orders']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
        
        <tr <?php if ($this->_sections['sandwitch']['index'] % 2 != 0): ?> class="even" <?php endif; ?> data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-formatdate="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]; ?>
"  data-date="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" >
		
		<td width="10%"><div class="plus <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sotd'] == 1): ?> white <?php else: ?> black <?php endif; ?> sotd" data-sandwich="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
">+</div></td>	
		
		<td width="10%"><div class="plus <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['trnd'] == 1): ?> white <?php else: ?> black <?php endif; ?> trnd" data-sandwich="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
">+</div></td>	
        
        <td width="10%" align="center">
              <span class="multi-left">
               <input <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['is_public'] == 1): ?> checked="checked" <?php endif; ?> type="checkbox" name="" class="input-checkbox" id="checkbox-<?php echo $this->_sections['sandwitch']['index']; ?>
">
               <label class="multisel-ckeck" for="checkbox-<?php echo $this->_sections['sandwitch']['index']; ?>
"></label>
              </span>
          </td>
          <td><?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
</td>
          <td><input name="sandwich_like" id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['like_count']; ?>
" type="text" class="sandwich_like" style="width:80px;" /></td>
          <td><input name="sandwich_purchase" id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['purchase_count']; ?>
" type="text" class="sandwich_purchase" style="width:80px;" /></td>
          <td>$ <?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
</td>
          <td><a href="#" class="flag"><?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['flag'] == 1): ?><img width="15" height="20" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/flag.png">unflag<?php endif; ?></a></td>
          <td width="6%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/edit/<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
">edit</a></td>
          <td width="6%"><a href="#" class="remove_gallery_item">remove</a></td>
        </tr>
        
        <?php endfor; endif; ?>
        
        <?php endif; ?>
	</tbody></table>
<div class="loadMoreGallery" style="text-align:center"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/star.png" alt="Loading" /> </div>
       </div>
       </div>
       
     </div>
  </div>