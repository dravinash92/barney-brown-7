<?php
/* Smarty version 3.1.39, created on 2021-03-25 09:16:13
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\deliveries.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c8d1d91c2a6_44919004',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c44017cfed692c74aa0e95976a94b801c6fcad2f' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\deliveries.tpl',
      1 => 1616678171,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c8d1d91c2a6_44919004 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
  <div class="deliveries">
   <h1>DELIVERIES</h1>

   <div class="top-deliveries-wrapper">
     <div class="left">
      <table class="top-deliveries-table">
        <tbody><tr>
          <td>NEW</td>
          <td class="count_new"><?php echo $_smarty_tpl->tpl_vars['count_new']->value;?>
</td>
        </tr>
        <tr>
          <td>PRC</td>
          <td class="count_prc"><?php echo $_smarty_tpl->tpl_vars['count_prc']->value;?>
</td>
        </tr>
        <tr>
          <td>OUT</td>
          <td class="count_bag"><?php echo $_smarty_tpl->tpl_vars['count_bag']->value;?>
</td>
        </tr>
        <tr>
          <td>DEL</td>
          <td class="count_pick"><?php echo $_smarty_tpl->tpl_vars['count_pick']->value;?>
</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>TOTAL</td>
          <td class="total_delivery"><?php echo $_smarty_tpl->tpl_vars['total_delivery']->value;?>
</td>
        </tr>
      </tbody></table>
    </div>

    <div class="right">
      <form action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
deliveries/searchDeliveryData" name="delivery_search" method="POST">
       <div class="text-box-holder">
         <label>Store Location</label>
         <select name="store_id" id="store_id" class="select">
          <option value="">Select Store</option>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickupstores']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
          <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['store_id'] == $_smarty_tpl->tpl_vars['v']->value->id) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value->store_name;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->address1;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->address2;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->zip;?>
</option>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </select>
      </div>
      <div class="text-box-holder">
       <label>Status</label>
       <select name="delivery_status" id="1" class="select">
        <option value="">--Select--</option>
        <option value="0">New</option>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
        <option value="<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
" <?php if ($_smarty_tpl->tpl_vars['del_status']->value == $_smarty_tpl->tpl_vars['b']->value->id) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['b']->value->label;?>
</option>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>    
      </select>
    </div>
    <div class="text-box-holder2">
     <label>Date</label>
     <input name="search_date" id="search_delivery_date" type="text"  placeholder="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
" class="text-box">
     <a href="#" class="date-icon"></a>
   </div>
   <input type="submit" name="submit" value="SEARCH" style="margin-top: 15px;margin-left: 26px;">
 </form>
</div>

</div>
<?php if ($_smarty_tpl->tpl_vars['total_delivery']->value != 0) {?> 
<div class="deliveries-table-detail-wrapper">
 <h2>NEW</h2>
 <table class="deliveries-new-table">
  <tbody><tr>
    <th>TIME</th>
    <th>ADDRESS</th>
    <th>ORDER DETAILS</th>

    <th>PRC</th>
    <th>OUT</th>
    <th>DEL</th>
    <th>&nbsp;</th>
    
  </tr>


  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DeliveryDataList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
  <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 0) {?>
  <?php if (($_smarty_tpl->tpl_vars['v']->value->timediff > 45) && ($_smarty_tpl->tpl_vars['v']->value->time == 1)) {?>  
  <tr style="background: #cef2cc;">
    <?php } elseif (($_smarty_tpl->tpl_vars['v']->value->timediff <= 45) && ($_smarty_tpl->tpl_vars['v']->value->timediff > 30) && ($_smarty_tpl->tpl_vars['v']->value->time == 1)) {?>
    <tr style="background:#fffcb5;">
      <?php } elseif (($_smarty_tpl->tpl_vars['v']->value->timediff <= 30) && ($_smarty_tpl->tpl_vars['v']->value->time == 1)) {?>
      <tr style="background:#fedfe4;">
        <?php } else { ?>
        <tr>
        <?php }?>

        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
   
         <h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
         <p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
         <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
         <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDet">View order details</a></p>
       </td>
       <td>
         <p>
            <?php echo $_smarty_tpl->tpl_vars['v']->value->address->name;?>
<br>
            <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->company)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->company;?>
</br><?php }?>
            <?php if ($_smarty_tpl->tpl_vars['v']->value->address->phone) {
echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
 <?php if ($_smarty_tpl->tpl_vars['v']->value->address->extn) {?> <?php echo $_smarty_tpl->tpl_vars['v']->value->address->extn;?>
 <?php }?><br><?php }?>              
            <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address1)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
            <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address2)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
            <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->zip)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>
         </p>
       </td>
       <td>

        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
        <p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->modifiers)) && $_smarty_tpl->tpl_vars['item']->value->modifiers != '' || $_smarty_tpl->tpl_vars['item']->value->modifiers != 0) {?><div class="item-modifier">Modifiers: <?php echo $_smarty_tpl->tpl_vars['item']->value->modifiers;?>
</div><?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->spl_instructions)) && $_smarty_tpl->tpl_vars['item']->value->spl_instructions != '') {?><div class="item-modifier">Special Instructions: <?php echo $_smarty_tpl->tpl_vars['item']->value->spl_instructions;?>
</div><?php }?>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

      </td>

      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
      <?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Cancel') {?>
      <td><a id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'Delivery_Pro') {?> class="viewOrdDet" <?php }?> <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;
if ($_smarty_tpl->tpl_vars['b']->value->id == 5) {?>,<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;
}?>)" <?php }?> href="javascript:void(0)" >  <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt="">
      </a>
    </td>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'Delivery_Cancel') {?>
    
    <td></td>
    <?php }?> 

    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

  </tr>  
  <?php }?>
  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


</tbody></table>
<h2>PROCESSED</h2>
<table class="deliveries-new-table">
  <tbody><tr>
    <th>TIME</th>
    <th>ADDRESS</th>
    <th>ORDER DETAILS</th>

    <th>PRC</th>
    <th>OUT</th>
    <th>DEL</th>
    <th>&nbsp;</th>
   
  </tr>
  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DeliveryDataList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
  <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 5) {?>
  <tr>
    <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

     <h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
     <p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
     <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
     <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDet">View order details</a></p>
   </td>
   <td>
              <p><?php echo $_smarty_tpl->tpl_vars['v']->value->address->name;?>
<br>
          <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->company)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->company;?>
</br><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->address->phone) {
echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
 <?php if ($_smarty_tpl->tpl_vars['v']->value->address->extn) {?> <?php echo $_smarty_tpl->tpl_vars['v']->value->address->extn;?>
 <?php }?><br><?php }?>               
          <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address1)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
          <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address2)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
          <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->zip)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>
              
        </p>
            </td>
            <td>
           
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
        <p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->modifiers)) && $_smarty_tpl->tpl_vars['item']->value->modifiers != '' || $_smarty_tpl->tpl_vars['item']->value->modifiers != 0) {?>
          <div class="item-modifier">Modifiers: <?php echo $_smarty_tpl->tpl_vars['item']->value->modifiers;?>
</div>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->spl_instructions)) && $_smarty_tpl->tpl_vars['item']->value->spl_instructions != '') {?>
          <div class="item-modifier">Special Instructions: <?php echo $_smarty_tpl->tpl_vars['item']->value->spl_instructions;?>
</div>
        <?php }?>
      <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             
            </td>


  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
  <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 5 && $_smarty_tpl->tpl_vars['b']->value->id == 5) {?>
  <td><a <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,0)" <?php }?> href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Cancel' && $_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Pro') {?>
  <td style="position: relative;"><a  class="assign-order-wrap" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
, '', <?php echo $_smarty_tpl->tpl_vars['v']->value->order_status;?>
 )" <?php }?> href="javascript:void(0)">
    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt="">
  </a>
        <div class="assign-order" id="delivery_user_assign_<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
" style="display: none;">
          <i class="close-btn" id="close-assign-user_<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
">close</i>
      <div class="assign-title">
      <h1>ASSIGN ORDER</h1>
    </div>
      <div class="asign-order-select">
        <select class ="deliveryUserOptions_<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
"> 
          <option value="0">SELECT</option>
        </select>
      </div>
      <div class="assign-submit">
        <button class ="assign_del_user" data-orderid= "<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
">Assign</button>
      </div>
    </div>

  </td>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'Delivery_Cancel') {?>
    
    <td></td>
    <?php }?>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

  </tr>  
  <?php }?>
  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


</tbody></table>
<h2>OUT FOR DELIVERY</h2>
<table class="deliveries-new-table">
  <tbody><tr>
    <th>TIME</th>
    <th>ADDRESS</th>
    <th>ORDER DETAILS</th>   
    <th>PRC</th>
    <th>OUT</th>
    <th>DEL</th>
    <th>&nbsp;</th>
    
  </tr>
  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DeliveryDataList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
  <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 6) {?>
  <tr>
    <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

     <h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
     <p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
     <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
     <p><a id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDet">View order details</a></p>
   </td>
   <td>
     <p><?php echo $_smarty_tpl->tpl_vars['v']->value->address->name;?>
<br>
       <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->company)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->company;?>
</br><?php }?>
       <?php if ($_smarty_tpl->tpl_vars['v']->value->address->phone) {
echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
 <?php if ($_smarty_tpl->tpl_vars['v']->value->address->extn) {?> <?php echo $_smarty_tpl->tpl_vars['v']->value->address->extn;?>
 <?php }?><br><?php }?>                
       <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address1)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
       <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address2)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
       <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->zip)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>

     </p>
   </td>
   <td>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
    <p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
    <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->modifiers)) && $_smarty_tpl->tpl_vars['item']->value->modifiers != '' || $_smarty_tpl->tpl_vars['item']->value->modifiers != 0) {?>
          <div class="item-modifier">Modifiers: <?php echo $_smarty_tpl->tpl_vars['item']->value->modifiers;?>
</div>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->spl_instructions)) && $_smarty_tpl->tpl_vars['item']->value->spl_instructions != '') {?>
          <div class="item-modifier">Special Instructions: <?php echo $_smarty_tpl->tpl_vars['item']->value->spl_instructions;?>
</div>
        <?php }?>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

  </td>

  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['getOrderSatusList']->value, 'b', false, 'k');
$_smarty_tpl->tpl_vars['b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['b']->value) {
$_smarty_tpl->tpl_vars['b']->do_else = false;
?>  
  <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 6 && $_smarty_tpl->tpl_vars['b']->value->id == 6) {?>
  <td><a <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,5)" <?php }?> href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a>
  </td>
  <td><a <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,5)" <?php }?> href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a>
  </td>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Cancel' && $_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_Pro' && $_smarty_tpl->tpl_vars['b']->value->code != 'Delivery_out') {?>
  <td><a id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'Delivery_Pro') {?> class="viewOrdDet" <?php }?> onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,<?php echo $_smarty_tpl->tpl_vars['b']->value->id;?>
)" <?php }?> href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box1.png" alt=""></td>
   <?php }?>
   <?php if ($_smarty_tpl->tpl_vars['b']->value->code == 'Delivery_Cancel') {?>
   
   <td></td>
   <?php }?>

   <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

 </tr>  
  <tr class="no-border-up">
  <td></td>
  <td></td>
  <td></td>
<td><b class="bold">Assigned: </b></td>
<?php if ($_smarty_tpl->tpl_vars['v']->value->delivery_assigned_to != 0) {?>
  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_uname;?>
</td>
  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_assigned_time;?>
</td>
  <?php } else { ?>
  <td></td>
  <td></td>
  <?php }?>
  <td></td>


</tr>
 <?php }?>
 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


</tbody></table>
<h2>DELIVERED</h2>
<table class="deliveries-new-table border-bottom">
  <tbody><tr>
    <th>TIME</th>
    <th>ADDRESS</th>
    <th>ORDER DETAILS</th>

    <th>PRC</th>
    <th>OUT</th>
    <th>DEL</th>
    <th>&nbsp;</th>
  </tr>
  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DeliverySortDataList']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
  <?php if ($_smarty_tpl->tpl_vars['v']->value->order_status == 7) {?>
  <tr>
    <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>

     <h3><?php echo $_smarty_tpl->tpl_vars['v']->value->timeformat;?>
</h3>
     <p><?php echo $_smarty_tpl->tpl_vars['v']->value->dayname;?>
</p>
     <p><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</p>
     <p><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
" class="viewOrdDet">View order details</a></p>
   </td>
   <td>
     <p><?php echo $_smarty_tpl->tpl_vars['v']->value->address->name;?>
<br>
       <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->company)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->company;?>
</br><?php }?>
       <?php if ($_smarty_tpl->tpl_vars['v']->value->address->phone) {
echo $_smarty_tpl->tpl_vars['v']->value->address->phone;?>
 <?php if ($_smarty_tpl->tpl_vars['v']->value->address->extn) {?> <?php echo $_smarty_tpl->tpl_vars['v']->value->address->extn;?>
 <?php }?><br><?php }?>                 
       <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address1)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address1;?>
</br><?php }?>
       <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->address2)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->address2;?>
</br><?php }?>
       <?php if (preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['v']->value->address->zip)) {
echo $_smarty_tpl->tpl_vars['v']->value->address->zip;?>
</br><?php }?>

     </p>
   </td>
   <td>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['v']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
    <p>(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 </p>
    <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->modifiers)) && $_smarty_tpl->tpl_vars['item']->value->modifiers != '' || $_smarty_tpl->tpl_vars['item']->value->modifiers != 0) {?>
          <div class="item-modifier">Modifiers: <?php echo $_smarty_tpl->tpl_vars['item']->value->modifiers;?>
</div>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value->spl_instructions)) && $_smarty_tpl->tpl_vars['item']->value->spl_instructions != '') {?>
          <div class="item-modifier">Special Instructions: <?php echo $_smarty_tpl->tpl_vars['item']->value->spl_instructions;?>
</div>
        <?php }?>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

  </td>

  <td><a <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,6)" <?php }?> href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
  <td><a <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,6)" <?php }?> href="javascript:void(0)" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
  <td><a <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>  onclick="manageDelivereis(<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
,6)" <?php }?> href="javascript:void(0)"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/deliveries-box.png" alt=""></a></td>
  <td></td>
</tr>  
<tr class="no-border-up">
  <td></td>
  <td></td>
  <td></td>
  <td><b class="bold">Delivered: </b></td>
  
  <?php if ($_smarty_tpl->tpl_vars['v']->value->delivery_assigned_to != 0) {?>
  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_uname;?>
</td>
  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->order_delivered;?>
</td>
  <?php } else { ?>
  <td></td>
  <td></td>
  <?php }?>
  <td></td>


</tr>
<?php }
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>


</tbody></table>
</div>
<?php }
if ($_smarty_tpl->tpl_vars['total_delivery']->value == 0) {?><div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div><?php }?>
</div>
</div>

<?php }
}
