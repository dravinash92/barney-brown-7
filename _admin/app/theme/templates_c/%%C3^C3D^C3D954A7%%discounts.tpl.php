<?php /* Smarty version 2.6.25, created on 2020-09-30 00:23:58
         compiled from discounts.tpl */ ?>
<div class="container">
     <div class="webpage">
       <h1>DISCOUNTS</h1>
       
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="8%">LIVE</th>
          <th width="14%">DISCOUNT NAME</th>
          <th width="12%">CODE</th>
          <th width="11%">DISCOUNT TYPE</th>
          <th width="6%">AMOUNT</th>
          <th width="11%">APPLICABLE TO</th>
          <th width="5%">MIN</th>
          <th width="6%">MAX</th>
          <th width="7%">TIMES USED</th>
          <th colspan="3">USES</th>
        </tr>
		<form name="discount_live" id= "discount_live" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
discounts/updateIsLive" enctype="multipart/form-data">
		<?php $_from = $this->_tpl_vars['discountList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['data']):
?>       
			<tr class="<?php if ($this->_tpl_vars['k']%2 == 0): ?>even<?php endif; ?>">
			  <td>
				 <span class="multi-left">
			
				   <input type="checkbox" id="checkbox-2-<?php echo $this->_tpl_vars['k']; ?>
" class="input-checkbox" name="live[]" value="<?php echo $this->_tpl_vars['data']['id']; ?>
" <?php if ($this->_tpl_vars['data']['is_live'] == 1): ?>checked ="checked"<?php endif; ?> >
				   <label for="checkbox-2-<?php echo $this->_tpl_vars['k']; ?>
" class="multisel-ckeck"></label>
				</span>
			  </td>
			  <td><?php echo $this->_tpl_vars['data']['name']; ?>
</td>
			  <td><?php echo $this->_tpl_vars['data']['code']; ?>
</td>
			  <td><?php if ($this->_tpl_vars['data']['type'] == 0): ?>% Off<?php else: ?>Fixed Amount<?php endif; ?></td>
			  <td><?php if ($this->_tpl_vars['data']['type'] == 1): ?>$<?php endif; ?><?php echo $this->_tpl_vars['data']['discount_amount']; ?>
<?php if ($this->_tpl_vars['data']['type'] == 0): ?>%<?php endif; ?></td>
			  <td><?php if ($this->_tpl_vars['data']['applicable_to'] == 0): ?>Sub-Total<?php else: ?>Total<?php endif; ?></td>
			  <td><?php if ($this->_tpl_vars['data']['minimum_order'] == 1): ?>$<?php echo $this->_tpl_vars['data']['minimum_order_amount']; ?>
<?php else: ?>-<?php endif; ?></td>
			  <td><?php if ($this->_tpl_vars['data']['maximum_order'] == 1): ?>$<?php echo $this->_tpl_vars['data']['maximum_order_amount']; ?>
<?php else: ?>-<?php endif; ?></td>
			  <td><?php echo $this->_tpl_vars['data']['count']; ?>
</td>
			  <td width="11%"><?php if ($this->_tpl_vars['data']['uses_type'] == 0): ?><?php echo $this->_tpl_vars['data']['uses_amount']; ?>
<?php else: ?>Unlimited<?php endif; ?></td>
			  <td width="3%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
discounts/editDiscounts/<?php echo $this->_tpl_vars['data']['id']; ?>
">edit</a></td>
			  <td width="5%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
discounts/deleteItems/<?php echo $this->_tpl_vars['data']['id']; ?>
" onclick="return confirm('Are you sure you want to delete?')">remove</a></td>
			</tr>
		<?php endforeach; endif; unset($_from); ?>		

      </tbody></table>
      
      <input type="submit" value="Update">
  </form>     
      <a class="add-user" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
Discounts/newDiscount">ADD DISCOUNT</a>
        
    </div>
  </div>