<?php
/* Smarty version 3.1.39, created on 2021-03-25 09:45:44
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\edit_sandwich.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c94080c2e67_41769251',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c9649a8876a110c2ed81c1037808d632c6cb5f07' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\edit_sandwich.tpl',
      1 => 1616679939,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c94080c2e67_41769251 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container">
    <div class="sandwich">
      <h1>EDIT SANDWICH</h1>
      
     
      
      <p> ID#: 348650<br>
        User: <?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['user_name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['user_email'];?>
)<br>
        Date Created: 
      
        
        06/15/2013<br>
        Likes: 100<br>
        Purchases: 5 <br>
       
       <?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['description'];?>

       
       <?php $_smarty_tpl->_assignInScope('protien_itemid', $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['PROTEIN']['item_id']);?>
       <?php $_smarty_tpl->_assignInScope('cheese_itemid', $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['CHEESE']['item_id']);?>
       <?php $_smarty_tpl->_assignInScope('topping_itemid', $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['TOPPINGS']['item_id']);?>
       <?php $_smarty_tpl->_assignInScope('cond_itemid', $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['CONDIMENTS']['item_id']);?>
       
       <?php $_smarty_tpl->_assignInScope('protien_item_qty_key', array_keys($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['PROTEIN']['item_qty']));?>
       <?php $_smarty_tpl->_assignInScope('cheese_item_qty_key', array_keys($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['CHEESE']['item_qty']));?>
       <?php $_smarty_tpl->_assignInScope('topping_item_qty_key', array_keys($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['TOPPINGS']['item_qty']));?>
       <?php $_smarty_tpl->_assignInScope('cond_item_qty_key', array_keys($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['CONDIMENTS']['item_qty']));?>
       
   
       <?php $_smarty_tpl->_assignInScope('protien_item_qty_value', array_values($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['PROTEIN']['item_qty']));?>
       <?php $_smarty_tpl->_assignInScope('cheese_item_qty_value', array_values($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['CHEESE']['item_qty']));?>
       <?php $_smarty_tpl->_assignInScope('topping_item_qty_value', array_values($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['TOPPINGS']['item_qty']));?>
       <?php $_smarty_tpl->_assignInScope('cond_item_qty_value', array_values($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['CONDIMENTS']['item_qty']));?>
 
       <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_name'];?>
" name='sandwich_name' />  
       <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_price'];?>
" name='sandwich_price' />       
       <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['menu_is_active'];?>
" name='sandwich_menustate' />
       <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['id'];?>
" name='sandwich_id' />      
       <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['uid'];?>
" name='sandwich_uid' />      
               
       <?php echo '<script'; ?>
>admin_ha.common.json_obj = JSON.parse('<?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data_json'];?>
');<?php echo '</script'; ?>
>   

      </p>
      <ul>
        <li>
          <label>Name of Sandwich</label>
          <input type="text" name="sandwichname" value=" <?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_name'];?>
">
        </li>
        <li>
          <label>Description</label>
          <textarea name="description" id="description"><?php echo $_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['description'];?>
</textarea>
        </li>
        <li> <span class="multi-left">
          <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="public" <?php if ($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['is_public'] == 1) {?> checked="checked" <?php }?> value="">
          <label for="checkbox-2-1" class="multisel-ckeck"></label>
          </span> <span>Public</span> </li>
      </ul>
      <div class="custom-sandwich">
        <h2>CUSTOM SANDWICH</h2>
        
        <?php if (count($_smarty_tpl->tpl_vars['BREAD_DATA']->value) > 0) {?>
        <div class="bread">
          <h3>BREAD</h3>
          <ul>
          
            <?php
$__section_bread_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['BREAD_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_bread_0_start = min(0, $__section_bread_0_loop);
$__section_bread_0_total = min(($__section_bread_0_loop - $__section_bread_0_start), $__section_bread_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_bread'] = new Smarty_Variable(array());
if ($__section_bread_0_total !== 0) {
for ($__section_bread_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index'] = $__section_bread_0_start; $__section_bread_0_iteration <= $__section_bread_0_total; $__section_bread_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index']++){
?>
            
          
            
            <li>
              <div class="radio-section">
                <input 
                
                data-id = "<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index'] : null)]['id'];?>
"
                data-item_price = "<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index'] : null)]['item_price'];?>
"
                 data-item_name = "<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index'] : null)]['item_name'];?>
"
                 data-category = "BREAD"
                <?php if ($_smarty_tpl->tpl_vars['EDIT_DATA']->value[0]['sandwich_data']['BREAD']['item_id'][0] == $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index'] : null)]['id']) {?>   checked="checked" <?php }?>
                
                type="radio" value="Redirect" class="input-radio" name="Redirect" id="1-<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index'] : null)]['id'];?>
">
                <label for="1-<?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index'] : null)]['id'];?>
"></label>
              </div>
              <span class="radio-text"> <?php echo $_smarty_tpl->tpl_vars['BREAD_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_bread']->value['index'] : null)]['item_name'];?>
 </span> </li>
            <?php
}
}
?>
          </ul>
        </div>
        <?php }?>
        
        
        <?php if (count($_smarty_tpl->tpl_vars['PROTEIN_DATA']->value) > 0) {?>
        <div class="meats">
          <h3>Meats</h3>
          <ul>
           <?php
$__section_protein_1_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['PROTEIN_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_protein_1_start = min(0, $__section_protein_1_loop);
$__section_protein_1_total = min(($__section_protein_1_loop - $__section_protein_1_start), $__section_protein_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_protein'] = new Smarty_Variable(array());
if ($__section_protein_1_total !== 0) {
for ($__section_protein_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] = $__section_protein_1_start; $__section_protein_1_iteration <= $__section_protein_1_total; $__section_protein_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']++){
?>
           <?php $_smarty_tpl->_assignInScope('prot_name', $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['item_name']);?>
             <li><span class="multi-left">
              <input 
              
              data-id = "<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['id'];?>
"
              data-item_price = "<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['item_price'];?>
"
              data-item_name = "<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['item_name'];?>
"
              data-image = "<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['item_image'];?>
"
               data-category = "PROTEIN"
               <?php if (in_array($_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['protien_itemid']->value) == 1) {?>
                checked="checked" 
                <?php }?>
              type="checkbox" id="2-<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['id'];?>
" class="input-checkbox" name="" value="<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['item_name'];?>
">
              <label for="2-<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['id'];?>
" class="multisel-ckeck"></label>
              </span> <span class="radio-text"><?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['item_name'];?>
</span> 
              
                 <?php if ($_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'] != 0) {?>
                 
              <select>
                  <?php
$__section_protein_ops_2_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'])) ? count($_loop) : max(0, (int) $_loop));
$__section_protein_ops_2_start = min(0, $__section_protein_ops_2_loop);
$__section_protein_ops_2_total = min(($__section_protein_ops_2_loop - $__section_protein_ops_2_start), $__section_protein_ops_2_loop);
$_smarty_tpl->tpl_vars['__smarty_section_protein_ops'] = new Smarty_Variable(array());
if ($__section_protein_ops_2_total !== 0) {
for ($__section_protein_ops_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index'] = $__section_protein_ops_2_start; $__section_protein_ops_2_iteration <= $__section_protein_ops_2_total; $__section_protein_ops_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index']++){
?>
                 
                 
                    
                  <option 
                  
                  <?php if ($_smarty_tpl->tpl_vars['protien_item_qty_value']->value[1][0] == $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index'] : null)]['option_name']) {?>
                  selected
                  <?php }?>
                  data-id="<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index'] : null)]['id'];?>
" 
                  data-unit="<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index'] : null)]['option_unit'];?>
" 
                  data-mult="<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index'] : null)]['price_mult'];?>
" 
                  data-name="<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index'] : null)]['option_name'];?>
" 
                  value="<?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index'] : null)]['option_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['PROTEIN_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_protein_ops']->value['index'] : null)]['option_name'];?>
</option>
                  <?php
}
}
?>
                 </select> 
                 <?php }?>
              
              </li>
           <?php
}
}
?>
          </ul>
        </div>
        <?php }?>
        
        
         <?php if (count($_smarty_tpl->tpl_vars['CHEESE_DATA']->value) > 0) {?>
        
        <div class="cheese">
          <h3>CHEESES</h3>
          <ul>
            
            
            <?php
$__section_cheese_3_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['CHEESE_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_cheese_3_start = min(0, $__section_cheese_3_loop);
$__section_cheese_3_total = min(($__section_cheese_3_loop - $__section_cheese_3_start), $__section_cheese_3_loop);
$_smarty_tpl->tpl_vars['__smarty_section_cheese'] = new Smarty_Variable(array());
if ($__section_cheese_3_total !== 0) {
for ($__section_cheese_3_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] = $__section_cheese_3_start; $__section_cheese_3_iteration <= $__section_cheese_3_total; $__section_cheese_3_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']++){
?>
           
             <li><span class="multi-left">
              <input 
              
              data-id = "<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['id'];?>
"
              data-item_price = "<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['item_price'];?>
"
              data-item_name = "<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['item_name'];?>
"
              data-image = "<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['item_image'];?>
"
              data-category = "CHEESE"
                <?php if (in_array($_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['cheese_itemid']->value) == 1) {?>
                checked="checked" 
                <?php }?>
              
              type="checkbox" id="3-<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['id'];?>
" class="input-checkbox" name="" value="<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['item_name'];?>
">
              <label for="3-<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['id'];?>
" class="multisel-ckeck"></label>
              </span> <span class="radio-text"><?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['item_name'];?>
</span> 
              
                 <?php if ($_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['options_id'] != 0) {?>
                 <select>
                  <?php
$__section_cheese_ops_4_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['options_id'])) ? count($_loop) : max(0, (int) $_loop));
$__section_cheese_ops_4_start = min(0, $__section_cheese_ops_4_loop);
$__section_cheese_ops_4_total = min(($__section_cheese_ops_4_loop - $__section_cheese_ops_4_start), $__section_cheese_ops_4_loop);
$_smarty_tpl->tpl_vars['__smarty_section_cheese_ops'] = new Smarty_Variable(array());
if ($__section_cheese_ops_4_total !== 0) {
for ($__section_cheese_ops_4_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index'] = $__section_cheese_ops_4_start; $__section_cheese_ops_4_iteration <= $__section_cheese_ops_4_total; $__section_cheese_ops_4_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index']++){
?>
                  <option 
                  data-id="<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index'] : null)]['id'];?>
" 
                  data-unit="<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index'] : null)]['option_unit'];?>
" 
                  data-mult="<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index'] : null)]['price_mult'];?>
" 
                  data-name="<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index'] : null)]['option_name'];?>
" 
                  value="<?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index'] : null)]['option_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['CHEESE_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_cheese_ops']->value['index'] : null)]['option_name'];?>
</option>
                  <?php
}
}
?>
                 </select>
                 <?php }?>
              
              </li>
           <?php
}
}
?>
            
            
          </ul>
        </div>
        
         <?php }?>
        
         <?php if (count($_smarty_tpl->tpl_vars['TOPPING_DATA']->value) > 0) {?>
        <div class="cheese">
          <h3>STANDARD TOPPINGS</h3>
          <ul>
          
          
            <?php
$__section_topping_5_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['TOPPING_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_topping_5_start = min(0, $__section_topping_5_loop);
$__section_topping_5_total = min(($__section_topping_5_loop - $__section_topping_5_start), $__section_topping_5_loop);
$_smarty_tpl->tpl_vars['__smarty_section_topping'] = new Smarty_Variable(array());
if ($__section_topping_5_total !== 0) {
for ($__section_topping_5_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] = $__section_topping_5_start; $__section_topping_5_iteration <= $__section_topping_5_total; $__section_topping_5_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']++){
?>
           
             <li><span class="multi-left">
              <input 
              
              data-id = "<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['id'];?>
"
              data-item_price = "<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['item_price'];?>
"
              data-item_name = "<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['item_name'];?>
"
                data-image = "<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['item_image'];?>
"
              data-category = "TOPPINGS"
               <?php if (in_array($_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['topping_itemid']->value) == 1) {?>
                checked="checked" 
                <?php }?>
              
              type="checkbox" id="4-<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['id'];?>
" class="input-checkbox" name="" value="<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['item_name'];?>
">
              <label for="4-<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['id'];?>
" class="multisel-ckeck"></label>
              </span> <span class="radio-text"><?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['item_name'];?>
</span> 
              
                 <?php if ($_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['options_id'] != 0) {?>
                 <select>
                  <?php
$__section_topping_ops_6_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['options_id'])) ? count($_loop) : max(0, (int) $_loop));
$__section_topping_ops_6_start = min(0, $__section_topping_ops_6_loop);
$__section_topping_ops_6_total = min(($__section_topping_ops_6_loop - $__section_topping_ops_6_start), $__section_topping_ops_6_loop);
$_smarty_tpl->tpl_vars['__smarty_section_topping_ops'] = new Smarty_Variable(array());
if ($__section_topping_ops_6_total !== 0) {
for ($__section_topping_ops_6_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index'] = $__section_topping_ops_6_start; $__section_topping_ops_6_iteration <= $__section_topping_ops_6_total; $__section_topping_ops_6_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index']++){
?>
                  <option 
                  data-id="<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index'] : null)]['id'];?>
" 
                  data-unit="<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index'] : null)]['option_unit'];?>
" 
                  data-mult="<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index'] : null)]['price_mult'];?>
" 
                  data-name="<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index'] : null)]['option_name'];?>
" 
                  value="<?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index'] : null)]['option_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['TOPPING_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_topping_ops']->value['index'] : null)]['option_name'];?>
</option>
                  <?php
}
}
?>
                 </select>
                 <?php }?>
              
              </li>
           <?php
}
}
?>
            
          </ul>
        </div>
          <?php }?>
        
        <?php if (count($_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value) > 0) {?>
        <div class="cheese">
          <h3>CONDIMENTS</h3>
          <ul>
          
          
            <?php
$__section_condiments_7_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_condiments_7_start = min(0, $__section_condiments_7_loop);
$__section_condiments_7_total = min(($__section_condiments_7_loop - $__section_condiments_7_start), $__section_condiments_7_loop);
$_smarty_tpl->tpl_vars['__smarty_section_condiments'] = new Smarty_Variable(array());
if ($__section_condiments_7_total !== 0) {
for ($__section_condiments_7_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] = $__section_condiments_7_start; $__section_condiments_7_iteration <= $__section_condiments_7_total; $__section_condiments_7_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']++){
?>
           
             <li><span class="multi-left">
              <input 
              
              data-id = "<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['id'];?>
"
              data-item_price = "<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['item_price'];?>
"
              data-item_name = "<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['item_name'];?>
"
               data-image = "<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['item_image'];?>
"
               data-category = "CONDIMENTS"
                <?php if (in_array($_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['cond_itemid']->value) == 1) {?>
                checked="checked" 
                <?php }?>
              
              type="checkbox" id="5-<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['id'];?>
" class="input-checkbox" name="" value="<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['item_name'];?>
">
              <label for="5-<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['id'];?>
" class="multisel-ckeck"></label>
              </span> <span class="radio-text"><?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['item_name'];?>
</span> 
              
                 <?php if ($_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['options_id'] != 0) {?>
                 <select>
                  <?php
$__section_condiments_ops_8_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['options_id'])) ? count($_loop) : max(0, (int) $_loop));
$__section_condiments_ops_8_start = min(0, $__section_condiments_ops_8_loop);
$__section_condiments_ops_8_total = min(($__section_condiments_ops_8_loop - $__section_condiments_ops_8_start), $__section_condiments_ops_8_loop);
$_smarty_tpl->tpl_vars['__smarty_section_condiments_ops'] = new Smarty_Variable(array());
if ($__section_condiments_ops_8_total !== 0) {
for ($__section_condiments_ops_8_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index'] = $__section_condiments_ops_8_start; $__section_condiments_ops_8_iteration <= $__section_condiments_ops_8_total; $__section_condiments_ops_8_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index']++){
?>
                  <option 
                  data-id="<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index'] : null)]['id'];?>
" 
                  data-unit="<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index'] : null)]['option_unit'];?>
" 
                  data-mult="<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index'] : null)]['price_mult'];?>
" 
                  data-name="<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index'] : null)]['option_name'];?>
" 
                  value="<?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index'] : null)]['option_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['CONDIMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_condiments_ops']->value['index'] : null)]['option_name'];?>
</option>
                  <?php
}
}
?>
                 </select>
                 <?php }?>
              
              </li>
           <?php
}
}
?>
            
          </ul>
        </div>
        <?php }?>
        
      </div>
      <div class="button"> <a class="save gallery_save" href="#">SAVE</a> <a class="cancel" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich">CANCEL</a> </div>
    </div>
  </div>
<?php }
}
