<?php /* Smarty version 2.6.25, created on 2020-02-28 06:14:29
         compiled from new_store_account.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'new_store_account.tpl', 58, false),)), $this); ?>
<div class="container">
    <div class="user-account">
       <h1>NEW STORE ACCOUNT</h1>
       <form action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/updateStoreAccount" method="POST" name="maincontents">
	    <div class="column column-1">
          <label>Store Name</label>
          <input type="text" value="" name="store_name">
          <label>Store Address Line One</label>
          <input type="text" value="" name="address1">
          <label>Store Address Line Two</label>
          <input type="text" value="" name="address2">
          <label>Store Zipcode</label>
          <input type="text" value="" name="zip">
          <label>Username</label>
          <input type="text" value="" name="username">
          <label>Password</label>
          <input type="password" name="password">
          <label>Confirm Password</label>
          <input type="password" value="" name="con_password">
          <label>Reports Pin</label>
          <input type="text" value="" name="report_pin">
     <span class="multi-left">
               <input type="checkbox" id="checkbox-2-0" class="input-checkbox" name="" value="">
          <label for="checkbox-2-0" class="multisel-ckeck"></label>
         </span><label class="restrict">Restrict IP Access to:</label>
          <input type="text" name="restrict_ip" value="">
          
           <input type="hidden" name="id" value="" />
           
          <button type="button" class="save_store_account">ADD NEW STORE</button>
           <a class="cancel" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts">CANCEL</a>
		
		</div>
      
       
	   <div class="column column-2">
	   <div class="pick-up" style=" width: 368px; ">
         <h3>Pick-Up / Delivery Windows</h3>
          <ul>
           <li class="radio"></li>
           <li class="week">&nbsp;</li>
           <li class="from">From</li>
           <li class="to">To</li>
         </ul>
         
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="day0" checked="checked" value="Sunday" <?php if ($this->_tpl_vars['store_timings_day'][0] == 'Sunday'): ?> checked <?php endif; ?>>
               <label for="checkbox-2-1" class="multisel-ckeck"></label>
              </span>
           </li>
      
           <li class="week">Sunday</li>
           <li class="from"> 
             <select name="day_open0">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][0] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
			</select>

           </li>
           <li class="to">
             <select name="day_close0">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][0] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-0-pickup" class="input-checkbox" name="checkbox-0-pickup" value="1">
                <label for="checkbox-0-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-0-delivery" class="input-checkbox" name="checkbox-0-delivery" value="1">
                <label for="checkbox-0-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
          </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input checked="checked" type="checkbox" id="checkbox-2-2" class="input-checkbox" name="day1" value="Monday" <?php if ($this->_tpl_vars['store_timings_day'][1] == 'Monday'): ?> checked <?php endif; ?>>
               <label for="checkbox-2-2" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Monday</li>
           <li class="from"> 
             <select name="day_open1">
                <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][1] == $this->_tpl_vars['timesection']): ?> selected  <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
             </select>
           </li>
           <li class="to">
             <select name="day_close1">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][1] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
          
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-1-pickup" class="input-checkbox" name="checkbox-1-pickup" value="1">
                <label for="checkbox-1-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-1-delivery" class="input-checkbox" name="checkbox-1-delivery" value="1">
                <label for="checkbox-1-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" checked="checked" id="checkbox-2-3" class="input-checkbox" name="day2" value="Tuesday" <?php if ($this->_tpl_vars['store_timings_day'][2] == 'Tuesday'): ?> checked <?php endif; ?>>
               <label for="checkbox-2-3" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Tuesday</li>
           <li class="from"> 
             <select name="day_open2">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][2] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close2">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][2] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-pickup" class="input-checkbox" name="checkbox-2-pickup" value="1">
                <label for="checkbox-2-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-delivery" class="input-checkbox" name="checkbox-2-delivery" value="1">
                <label for="checkbox-2-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left" >
               <input type="checkbox" checked="checked" id="checkbox-2-4" class="input-checkbox" name="day3" value="Wednesday" <?php if ($this->_tpl_vars['store_timings_day'][3] == 'Wednesday'): ?> checked <?php endif; ?>>
               <label for="checkbox-2-4" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Wednesday</li>
           <li class="from"> 
             <select name="day_open3">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][3] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close3">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][3] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-3-pickup" class="input-checkbox" name="checkbox-3-pickup" value="1">
                <label for="checkbox-3-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-3-delivery" class="input-checkbox" name="checkbox-3-delivery" value="1">
                <label for="checkbox-3-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input checked="checked"  type="checkbox" id="checkbox-2-5" class="input-checkbox" name="day4" value="Thursday" <?php if ($this->_tpl_vars['store_timings_day'][4] == 'Thursday'): ?> checked <?php endif; ?>>
               <label for="checkbox-2-5" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Thursday</li>
           <li class="from"> 
             <select name="day_open4">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][4] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close4">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][4] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
          
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-4-pickup" class="input-checkbox" name="checkbox-4-pickup" value="1">
                <label for="checkbox-4-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input checked="checked" type="checkbox" id="checkbox-4-delivery" class="input-checkbox" name="checkbox-4-delivery" value="1">
                <label for="checkbox-4-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" checked="checked" id="checkbox-2-6" class="input-checkbox" name="day5" value="Friday" <?php if ($this->_tpl_vars['store_timings_day'][5] == 'Friday'): ?> checked <?php endif; ?>>
               <label for="checkbox-2-6" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Friday</li>
           <li class="from"> 
             <select name="day_open5">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][5] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close5">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][5] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
          
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-5-pickup" class="input-checkbox" name="checkbox-5-pickup" value="1">
                <label for="checkbox-5-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-5-delivery" class="input-checkbox" name="checkbox-5-delivery" value="1">
                <label for="checkbox-5-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input checked="checked" type="checkbox" id="checkbox-2-7" class="input-checkbox" name="day6" value="Saturday" <?php if ($this->_tpl_vars['store_timings_day'][6] == 'Saturday'): ?> checked <?php endif; ?>>
               <label for="checkbox-2-7" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Saturday</li>
           <li class="from"> 
             <select name="day_open6">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_open'][6] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close6">
              <?php $_from = $this->_tpl_vars['timesections']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['timesection']):
?>
					<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['timesection'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%H:%M') : smarty_modifier_date_format($_tmp, '%H:%M')); ?>
" <?php if ($this->_tpl_vars['store_timings_close'][6] == $this->_tpl_vars['timesection']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['timesection']; ?>
</option>
			 <?php endforeach; endif; unset($_from); ?>        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-6-pickup" class="input-checkbox" name="checkbox-6-pickup" value="1">
                <label for="checkbox-6-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-6-delivery" class="input-checkbox" name="checkbox-6-delivery" value="1">
                <label for="checkbox-6-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         
        <!-- <div class="deactivate">
            <ul>
              <li>
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-8" class="input-checkbox" name="" value="">
                <label for="checkbox-2-8" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
              </li>
              <li>
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-9" class="input-checkbox" name="" value="">
                <label for="checkbox-2-9" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
              </li>
            </ul>
         
         </div>-->
         
       <!-- </div> -->
		</div>
		<div class="pick-up" style=" width: 368px; " >
         <h3>CLOSE SPECIFIC DAY</h3>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="specificDay" class="input-checkbox" name="specificDayCheckbox" value="specificDay"  >
               <label for="specificDay" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Close Specific Day</li>
          </ul>
           <div style="clear:both;"></div>
          <ul>
           <li class="deactdelvery text-box-holder2" >
               <span class="multi-left">
                  <input name="specificday" id="search_delivery_date" type="text" class="text-box" name="specificDay" value="<?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->specificday; ?>
">
                  <a href="#" class="date-icon"></a>
               </span>
            </li>
         </ul>
         <div style="clear:both;"></div>
         <ul style="margin-top: 20px;">
           <li class="deactdelvery" >
           <label  class="multisel-ckeck">Message</label>
               <span class="multi-left">
                  <textarea rows="" cols="" id="specificday_message" name="specificday_message"><?php echo $this->_tpl_vars['store_accounts_data']->pickup_stores->specificday_message; ?>
</textarea>
               </span>
            </li>
         </ul>
         </div>
	   </div>
       
     	   
	   <div class="column column-3">
	   <div class="zipcode-wrapper ten">
         <h3>ALLOWED ZIP CODES($10 MIN)</h3> 
        
         <div class="zip_wrap ">
			 <ul class="heading">
			   <li class="zip-code">Zip Code</li>
			   <li class="abbr">Abbr.</li>
         <li class="abbr">Delivery Fee</li> 
			 </ul>
			  <ul>
			   <li class="zip-code"><input type="text"  value="" name="zipcode1"></li>
			   <li class="abbr"><input maxlength="3" type="text" value="" name="abbreviation1">
			   <input type="hidden" name="minAmount1" class="minAmount" value="10"></li>
         <li class="delivery-fee"><input type="text" value="" name="deliveryfee1"></li>
			    <li class="zipItemrmv"><a class="removeZip" >Remove</a></li>
			 </ul>
         </div>
         <div style="clear:both"><a class="add zip_add" data-minamount="10" href="javascript:void(0)" style="">add</a> </div>
         
       </div> 
	   
       <div class="zipcode-wrapper hundred">
	   
	   
         <h3>ALLOWED ZIP CODES($100 MIN)</h3> 
         <div class="zip_wrap ">
			 <ul class="heading">
			   <li class="zip-code">Zip Code</li>
			   <li class="abbr">Abbr.</li>
			 </ul>
			  <ul>
			   <li class="zip-code"><input type="text"  value="" name="zipcode2"></li>
			   <li class="abbr"><input maxlength="3" type="text" value="" name="abbreviation2">
			   <input type="hidden" name="minAmount2" class="minAmount" value="100"></li>
         <li class="delivery-fee"><input type="text" value="" name="deliveryfee1"></li>
			    <li class="zipItemrmv"><a class="removeZip" >Remove</a></li>
			 </ul>
         </div>
         <div style="clear:both"><a class="add zip_add_hundred" data-minamount="100" href="javascript:void(0)" style="">add</a> </div>
         
      </div>
        <input type="hidden" name="zipcount" id="zipcount" value="2" />
		
		</div>
       </form>
    </div>
  </div>
 