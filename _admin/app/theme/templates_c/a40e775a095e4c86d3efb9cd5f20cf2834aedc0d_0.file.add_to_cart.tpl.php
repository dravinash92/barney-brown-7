<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:37:38
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\add_to_cart.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c84120c3fa2_35382765',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a40e775a095e4c86d3efb9cd5f20cf2834aedc0d' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\add_to_cart.tpl',
      1 => 1616675067,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c84120c3fa2_35382765 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_admin\\app\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<div class="container">
    <div class="place-order">
       <h1>PLACE ORDER</h1>
       
       <div class="place-order-coustomer-info">
       		<div class="address-details">
           <h3>CUSTOMER INFO</h3>
           <p>Name: <span><?php echo $_smarty_tpl->tpl_vars['user']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->last_name;?>
</span> <input type="text" class="text-box edit_customer_info" name="first_name" placeholder="First Name" style="display:none;" > <input type="text" class="text-box edit_customer_info" name="last_name" placeholder="Last Name"style="display:none;"><br>
              Email: <span><?php echo $_smarty_tpl->tpl_vars['user']->value->username;?>
</span>
               <input type="text" class="text-box edit_customer_info" name="username"  value="" style="display:none;"><br/>
              Phone: <span><?php echo $_smarty_tpl->tpl_vars['user']->value->phone;?>
</span>
              <input type="text" class="text-box edit_customer_info" name="phone" placeholder="000-000-0000" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->phone;?>
" style="display:none;"><br/>
              Total Orders: <?php echo $_smarty_tpl->tpl_vars['user']->value->total_orders;?>
<br>
              Total Sales: $<?php echo $_smarty_tpl->tpl_vars['user']->value->total_sum;?>
</p>
           <a href="javascript:void(0)" class="edit-info edit_info" >Edit Info</a>
           <a href="javascript:void(0)" class="edit-info save_info" style="display:none;">Save Info</a>
           <input type="hidden" name="user_id" class="user_id" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->uid;?>
" />
         </div>
         
         <div class="customer-notes">
         	<h3>CUSTOMER NOTES</h3>
			<div class="customer-notes-list">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['notes']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<div class="note">
					<p><span class="note_date"><?php echo $_smarty_tpl->tpl_vars['v']->value->date;?>
</span><span class="address"><?php echo $_smarty_tpl->tpl_vars['v']->value->notes;?>
</span></p>
					<br/>
					<a href="javascript:void(0)" data-id="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" class="remove remove_customer_note">Remove</a>
         		</div>				
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         	</div>
         	<div class="add-customer-note" style="display:none;">
				<textarea col="20" rows="3" placeholder="Add new note"></textarea>	
				<br/>
				<a href="javascript:void(0)" class="btn-brown add_customer_note">Add</a>   <a href="javascript:void(0)" class="btn-brown cancel_customer_note">Cancel</a>
         	</div>	
            <a href="javascript:void(0)" class="add-new-note">add new note</a>
         </div>
         
         <div class="date-time-order">
         		<h3>DATE/TIME OF ORDER</h3>
            <div class="radio-holder">
            	<input type="radio" class="css-checkbox now_or_specific" id="radio1" name="radio_time" value="now">
              <label class="css-label time_type_label" for="radio1">Now</label>
              <input type="radio" class="css-checkbox now_or_specific" id="radio2" name="radio_time" value="specific">
              <label class="css-label time_type_label" for="radio2">Specific Date/Time</label>
            </div>
            
            <div class="text-box-holder">
            	<label>Date</label>
              <input type="text" class="text-box" name="">
              <a class="date-icon" href="#"></a>
            </div>
            <div class="text-box-holder">
            	<label>Time</label>
				<?php echo $_smarty_tpl->tpl_vars['times']->value;?>

            </div>
         </div>
         
         <div class="pickup-deliver-order">
         	<div class="radio-holder">
				<input type="radio" class="css-checkbox address_type" id="radio3" name="radio_address_type" value="delivery">
				<label class="css-label address_type_label" for="radio3">DELIVERY</label>
				<input type="radio" class="css-checkbox address_type" id="radio4" name="radio_address_type" value="pickup">
				<label class="css-label address_type_label" for="radio4">PICK-UP</label>
            </div>
			<div class="text-box-holder address-container">
           
			</div>
          
         </div>
         
       </div>
       
       <h2>SANDWICHES</h2>
       <ul class="new-order-sandwichilist">
         <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['user']->value->uid;?>
" class="big-buttons-sandwich"><span>+</span>CREATE A SANDWICH</a></li>
         <li><a href="javascript:void(0)" class="big-buttons-sandwich choose-from-gallery"><span>+</span>SANDWICH GALLERY</a></li>
         
         <?php
$__section_sandwitch_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['GALLARY_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_0_start = min(0, $__section_sandwitch_0_loop);
$__section_sandwitch_0_total = min(($__section_sandwitch_0_loop - $__section_sandwitch_0_start), $__section_sandwitch_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_0_total !== 0) {
for ($__section_sandwitch_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_0_start; $__section_sandwitch_0_iteration <= $__section_sandwitch_0_total; $__section_sandwitch_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>
            
            <?php $_smarty_tpl->_assignInScope('sandwich_id', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id']);?>
            <?php $_smarty_tpl->_assignInScope('sandwich_uid', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid']);?>
            <?php $_smarty_tpl->_assignInScope('user_uid', $_smarty_tpl->tpl_vars['user']->value->uid);?>
            <?php $_smarty_tpl->_assignInScope('menu_is_active', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active']);?>
             
            <?php 
				
			if((in_array( $_smarty_tpl->get_template_vars('sandwich_id') , $_smarty_tpl->get_template_vars('temp_sandwiches') ) || $_smarty_tpl->get_template_vars('sandwich_uid') == $_smarty_tpl->get_template_vars('user_uid') ) && $_smarty_tpl->get_template_vars('menu_is_active') == 1 ){
				
            ?> 
             
              <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>
              
              <?php 
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
              ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }
              ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
              ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              ?>
      
      
                  <?php if (is_array($_SESSION['orders']['item_id'])) {?> 
                   <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_SESSION['orders']['item_id']));?>
                   <?php } else { ?>
                   <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
                   <?php }?>
              
              
                <li <?php if ($_smarty_tpl->tpl_vars['items_id']->value == 1) {?> rel="ADDED TO CART" <?php } else { ?> rel="ADD TO CART" <?php }?> id="toastID_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0];?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-flag="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['flag'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
"  data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-sandwich_desc_id ="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc_id'];?>
"  data-menuadds="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_add_count'];?>
" data-userid ="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
" data-toast="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'];?>
" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-type="user_sandwich" data-likeid="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_id'];?>
" data-likecount="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_count'];?>
"> 
                <input type="hidden" name="image" id="sandImg_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png">
                
                <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['by_admin'] == 1) {?> 
                  <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>
                <?php } else { ?>
                  <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['CMS_URL']->value);?>
                <?php }?>
                
                
                <span class="sandwichilist-holder">
					        <img data-href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" width="156" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/thumbnails/sandwich_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png" alt="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
">
					        <h3><?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
</h3>
				          <h4>$<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price'];?>
</h4>
					        
					        <a href="javascript:void(0)" data-type="user_sandwich" data-id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" class="add-cart view-sandwich" >ADD TO CART</a>
				        </span>
                
                </li>
                
            <?php 
			}
            ?>    
                
            <?php
}
}
?>
         
       </ul>
       
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stdCategoryItems']->value, 'category', false, NULL, 'outer', array (
));
$_smarty_tpl->tpl_vars['category']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->do_else = false;
?>
			<h2><?php echo $_smarty_tpl->tpl_vars['category']->value->standard_cat_name;?>
</h2> 
			 <ul class="sides-list">      
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['category']->value->categoryProducts, 'item', false, 'key');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
				<li>
				   <h3><?php echo $_smarty_tpl->tpl_vars['item']->value->product_name;?>
________________</h3>
				   <h4>$<?php echo $_smarty_tpl->tpl_vars['item']->value->product_price;?>
</h4>
				   <span class="arrow-holder">
					 <a href="#" data-event="noupdate" class="arrow-left"></a>
					 <input name="quantity" type="text" class="qty" value="1">
					 <a href="#" data-event="noupdate" class="arrow-right"></a>
				   </span>
				   <!--<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/cartConfirm/" data-type="product" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="add-cart">ADD TO CART</a>-->
				   <a href="javascript:void(0)" data-type="product" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="add-cart <?php if ($_smarty_tpl->tpl_vars['item']->value->allow_spcl_instruction == 1 || $_smarty_tpl->tpl_vars['item']->value->add_modifier == 1) {?> salad-listing <?php } else { ?> add-to-cart <?php }?>" data-sandwich="product" data-sandwich_id="<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
" data-product_name="<?php echo $_smarty_tpl->tpl_vars['item']->value->product_name;?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['item']->value->description;?>
" data-product_image="<?php echo $_smarty_tpl->tpl_vars['item']->value->product_image;?>
" data-product_price="<?php echo $_smarty_tpl->tpl_vars['item']->value->product_price;?>
" data-standard_category_id="<?php echo $_smarty_tpl->tpl_vars['item']->value->standard_category_id;?>
" data-uid=<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
 data-product="product" data-spcl_instr="<?php echo $_smarty_tpl->tpl_vars['item']->value->allow_spcl_instruction;?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['item']->value->add_modifier;?>
" data-modifier_desc="<?php echo $_smarty_tpl->tpl_vars['item']->value->modifier_desc;?>
" data-modifier_isoptional="<?php echo $_smarty_tpl->tpl_vars['item']->value->modifier_isoptional;?>
" data-modifier_is_single="<?php echo $_smarty_tpl->tpl_vars['item']->value->modifier_is_single;?>
" data-modifier_options='<?php echo json_encode($_smarty_tpl->tpl_vars['modifier_options']->value[$_smarty_tpl->tpl_vars['key']->value]);?>
'>ADD TO CART</a>
				</li> 
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</ul>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
       
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cateringdataList']->value, 'list', false, 'j');
$_smarty_tpl->tpl_vars['list']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['list']->value) {
$_smarty_tpl->tpl_vars['list']->do_else = false;
?>
			   
			<?php if (count($_smarty_tpl->tpl_vars['list']->value['data']) > 0) {?>
			<h2><?php echo $_smarty_tpl->tpl_vars['list']->value['cat_name'];?>
</h2> 
			 <ul class="sides-list">  
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value['data'], 'data', false, 'k');
$_smarty_tpl->tpl_vars['data']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->do_else = false;
?>
				<li>
				   <h3><?php echo $_smarty_tpl->tpl_vars['data']->value->product_name;?>
________________</h3>
				   <h4>$<?php echo $_smarty_tpl->tpl_vars['data']->value->product_price;?>
</h4>
				   <span class="arrow-holder">
					 <a href="#" data-event="noupdate" class="arrow-left"></a>
					 <input name="quantity" type="text" class="qty" value="1">
					 <a href="#" data-event="noupdate" class="arrow-right"></a>
				   </span>
				   <!--<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/cartConfirm/" data-type="product" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" class="add-cart">ADD TO CART</a>-->
				   <a href="javascript:void(0)" data-type="product" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value->id;?>
" class="add-cart <?php if ($_smarty_tpl->tpl_vars['data']->value->allow_spcl_instruction == 1 || $_smarty_tpl->tpl_vars['data']->value->add_modifier == 1) {?> salad-listing <?php } else { ?> add-to-cart <?php }?>" data-sandwich="product" data-sandwich_id="<?php echo $_smarty_tpl->tpl_vars['data']->value->id;?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
" data-product_name="<?php echo $_smarty_tpl->tpl_vars['data']->value->product_name;?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['data']->value->description;?>
" data-product_image="<?php echo $_smarty_tpl->tpl_vars['data']->value->product_image;?>
" data-product_price="<?php echo $_smarty_tpl->tpl_vars['data']->value->product_price;?>
" data-standard_category_id="<?php echo $_smarty_tpl->tpl_vars['data']->value->standard_category_id;?>
" data-uid=<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
 data-product="product" data-spcl_instr="<?php echo $_smarty_tpl->tpl_vars['data']->value->allow_spcl_instruction;?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['data']->value->add_modifier;?>
" data-modifier_desc="<?php echo $_smarty_tpl->tpl_vars['data']->value->modifier_desc;?>
" data-modifier_isoptional="<?php echo $_smarty_tpl->tpl_vars['data']->value->modifier_isoptional;?>
" data-modifier_is_single="<?php echo $_smarty_tpl->tpl_vars['data']->value->modifier_is_single;?>
" data-modifier_options='<?php echo $_smarty_tpl->tpl_vars['data']->value->modifier_options;?>
'>ADD TO CART</a>
				</li> 
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</ul>	
			<?php }?>
			
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		
		
		<div class="shopping-cart">
			<?php echo $_smarty_tpl->tpl_vars['shopitems']->value;?>

		</div>
       
       <div class="billing-information-holder">
			<h3>BILLING INFORMATION</h3>
       	
			<div class="billing-card-holder">
			  <div class="text-box-holder">
				<select name="" class="billing-select">
					<option value="">Select Card</option>	
					<option value="no">No Charge</option>	
					<option value="0">Add New</option>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['billingInfo']->value, 'billings', false, 'myId');
$_smarty_tpl->tpl_vars['billings']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['myId']->value => $_smarty_tpl->tpl_vars['billings']->value) {
$_smarty_tpl->tpl_vars['billings']->do_else = false;
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['billings']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['billings']->value->card_type;?>
 card number ending with <?php echo substr($_smarty_tpl->tpl_vars['billings']->value->card_number,-4);?>
</option>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</select>
			  </div>
			  <!--<a href="javascript:void(0)" class="edit-bill">add new billing</a>-->
			  <!--<a href="javascript:void(0)" class="edit-bill">edit billing</a>-->
			</div>
			<br/>
			 <a href="javascript:void(0)" style="display:none;" class="change-billing">CHANGE</a>
			<div class="billing-info-show" style="display:none; color:#000;"></div>
			<input type="hidden" name="billing_card" value="" />
          <a href="javascript:void(0)" class="place-order">PLACE ORDER</a>
       </div>
       
    </div>
  </div>

<div class="popup-wrapper" id="add-new-address">
  <div class="add-new-address-inner"> <a href="#" class="close-button"></a>
	<form class="add_address" method="post" >
    <div class="title-holder">
      <h1>ADD NEW ADDRESS</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Recipient Name</p>
        <input name="recipient" type="text" class="text-box-control" placeholder="Enter Recipient Name">
        </span> <span class="text-box-holder">
        <p>Company</p>
        <input name="company" type="text" class="text-box-control" placeholder="Enter Company Name">
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Street Address 1</p>
        <input name="address1" type="text" class="text-box-control" >
        </span> <span class="text-box-holder">
        <p>Street Address 2</p>
        <input name="address2" type="text" class="text-box-control" >
        </span> <span class="text-box-holder1">
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" >
        </span> <span class="text-box-holder">
        <p>Phone Number</p>
        <input name="phone1" type="text" class="text-box-phone" >
        <input name="phone2" type="text" class="text-box-phone" >
        <input name="phone3" type="text" class="text-box-phone margin-none" >
        </span> <span class="text-box-holder1">
        <p>Ext.</p>
        <input name="extn" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <h2>New York, NY</h2>
        <span class="text-box-zip-holder1">
        <p>Zip Code</p>
        <input name="zip" type="text" class="text-box-zip" >
        </span> </span>
        <h3><?php echo $_smarty_tpl->tpl_vars['title_text']->value;?>
 currently delivers in Midtown Manhattan from 23rd Street to 59th Street and from 3rd Avenue to 8th Avenue.</h3>
      </li>
      <li> <span class="delivery-instructions">
        <p>Delivery Instructions</p>
        <input name="delivery_instructions" type="text" class="text-box-delivery" >
        </span> </li>
      <li> <a href="javaScript:void(0)" class="add-address save_address">ADD ADDRESS</a> </li>
    </ul>
    <input type="hidden" name="uid" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->uid;?>
" />
    </form>
  </div>
</div>
<div class="popup-wrapper" id="edit-address">
  <div class="add-new-address-inner">
	
  </div>
</div>

<div class="popup-wrapper" id="sandwich-gallery">
  <div class="add-new-address-inner">
	  <a href="#" class="close-button"></a>
	<div class="row">
                  
         <?php
$__section_sandwitch_1_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['GALLARY_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_1_start = min(0, $__section_sandwitch_1_loop);
$__section_sandwitch_1_total = min(($__section_sandwitch_1_loop - $__section_sandwitch_1_start), $__section_sandwitch_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_1_total !== 0) {
for ($__section_sandwitch_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_1_start; $__section_sandwitch_1_iteration <= $__section_sandwitch_1_total; $__section_sandwitch_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>
            
            <?php $_smarty_tpl->_assignInScope('sandwich_id', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id']);?>
            
              <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>
              
              <?php 
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
              ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }
              ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
              ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              ?>
      
      
                  <?php if (is_array($_SESSION['orders']['item_id'])) {?> 
                   <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_SESSION['orders']['item_id']));?>
                   <?php } else { ?>
                   <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
                   <?php }?>
                   
                   
                     
                <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['by_admin'] == 1) {?> 
                  <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>
                <?php } else { ?>
                  <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['CMS_URL']->value);?>
                <?php }?>
              
              
                <div class="width33" <?php if ($_smarty_tpl->tpl_vars['items_id']->value == 1) {?> rel="ADDED TO CART" <?php } else { ?> rel="ADD TO CART" <?php }?> data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0];?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
"   > 
                
                
                <span class="sandwichilist-holder">
					<img data-href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" width="156" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png" alt="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
">
					<h3><?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
</h3>
					<h4>$<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_price'];?>
</h4>
					<!--<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/cartConfirm/" class="add-to-cart">ADD TO CART</a>-->
					<a href="javascript:void(0)" data-type="user_sandwich" data-id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" class="button add-to-cart" >SELECT</a>
				</span>
                
                </div>
                
                
            <?php
}
}
?>
         
       </div>
  </div>
</div>

<!--credit card popup-->
<!--Popup Start-->
<div class="popup-wrapper" id="credit-card-details">
  <div class="add-new-address-inner"> <a href="javascript:void(0)" class="close-button">Close</a>
    <div class="title-holder">
      <h1>ADD NEW CREDIT CARD</h1>
    </div>
     <form id="billForm">
    <ul class="from-holder">
   
      <li> <!-- <span class="text-box-holder">
        <p>Credit Card Nick Name</p>
        <input name="cardNickname" type="text" class="text-box-control" placeholder="">
        </span>  --><span class="text-box-holder">
        <p>Card Number</p>
        <input name="cardNo" type="text" class="text-box-control" placeholder="">
        </span> <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="cardCvv" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
      <p>Credit Card</p>
        <span class="credit-card">
        <select name="cardType"  >
          
           <option value="visa">Visa</option>
          <option value="MasterCard">MasterCard</option>
          <option value="Discover">Discover</option>
          <option value="American Express">American Express</option>
        
        </select>
        </span> <br><br><br>
        <p>Expiration Date</p>
        <span class="month">
        <select name="cardMonth"  >
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="03">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <select name="cardYear">
         
          
          
<?php $_smarty_tpl->_assignInScope('currentyear', smarty_modifier_date_format(time(),"%Y"));
$_smarty_tpl->_assignInScope('numyears', 50);
$_smarty_tpl->_assignInScope('totalyears', $_smarty_tpl->tpl_vars['currentyear']->value+$_smarty_tpl->tpl_vars['numyears']->value);
$__section_loopyers_2_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['totalyears']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_loopyers_2_start = (int)@$_smarty_tpl->tpl_vars['currentyear']->value < 0 ? max(0, (int)@$_smarty_tpl->tpl_vars['currentyear']->value + $__section_loopyers_2_loop) : min((int)@$_smarty_tpl->tpl_vars['currentyear']->value, $__section_loopyers_2_loop);
$__section_loopyers_2_total = min(($__section_loopyers_2_loop - $__section_loopyers_2_start), $__section_loopyers_2_loop);
$_smarty_tpl->tpl_vars['__smarty_section_loopyers'] = new Smarty_Variable(array());
if ($__section_loopyers_2_total !== 0) {
for ($__section_loopyers_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index'] = $__section_loopyers_2_start; $__section_loopyers_2_iteration <= $__section_loopyers_2_total; $__section_loopyers_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index']++){
?>
<option><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index'] : null);?>
</option>
<?php
}
}
?>
         
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="cardZip" type="text" class="text-box-billing-zip" >
        </span> </li>
         <!--<li> <span class="text-box-holder">
        <p>Street Address </p>
        <input name="address1" type="text" class="text-box-control required" required >
        </span> 
	<span class="text-box-holder1"> 
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control required" required>
        </span> </li>-->
      <li> <span class="checkbox-save-bill">
        <input  type="checkbox" id="save_billing"  name="save_billing" value="save_billing"/>
        <label for="save_billing">Save Billing Info <span>(Verisign Encryption)</span></label>
        </span> </li>
      <li> <span class="card-holder-margin">
        <h3>One or two sentences about site security, encryption methods, storage of data meeting Y and Z standards (to let people feel secure about storing their credit card info).</h3>
        <a href="javascript:void(0)" class="credit-card-button">ADD</a> </span> </li>
    </ul>
    </form>
  </div>
  
  <form id="_payment_form" action="paymentpage/" method="post" style="display:none">
  <input type = "hidden" name="_card_type" value="" />
  <input type = "hidden" name="_card_number" value="" />
  <input type = "hidden" name="_card_cvv" value="" />
  <input type = "hidden" name="_card_zip" value="" />
  <input type = "hidden" name="_card_name" value="" />
  <input type = "hidden" name="_expiry_month" value="" />
  <input type = "hidden" name="_expiry_year" value="" />
  </form>
  
</div>




<!--credit card popup-->
<?php }
}
