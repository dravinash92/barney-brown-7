<?php /* Smarty version 2.6.25, created on 2019-09-23 10:42:48
         compiled from edit_discounts.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'edit_discounts.tpl', 101, false),)), $this); ?>
<div class="container">
     <div class="new-discount">
 <form name="updateDiscount" id="updateDiscount" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
discounts/updateDiscounts" enctype="multipart/form-data"> 
        <h1>EDIT DISCOUNT</h1>
        <ul>
          <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
">
        <li>
          <label>Name of Discount</label>
          <input type="text" name="name" value="<?php echo $this->_tpl_vars['data']['name']; ?>
"><span class="error_msg"><?php echo $this->_tpl_vars['error']['name']; ?>
</span>
        </li>
        
         <li>
          <label>Discount Code</label>
          <input type="text" name="code" value="<?php echo $this->_tpl_vars['data']['code']; ?>
"><span class="error_msg"><?php echo $this->_tpl_vars['error']['code']; ?>
</span>
        </li>
              
        <li>
          <label>Discount Type</label>
          <div class="radio-section">
            <input type="radio" value="0" class="input-radio" <?php if ($this->_tpl_vars['data']['type'] == 0): ?>checked<?php endif; ?> name="type" id="discount-1">
            <label for="discount-1"></label>
            <span class="radio-text">% Off</span>
            
            <input type="radio" value="1"  class="input-radio" name="type" id="discount-2" <?php if ($this->_tpl_vars['data']['type'] == 1): ?>checked<?php endif; ?> >
            <label for="discount-2"></label>
            <span class="radio-text">Fixed Amount</span> </div>
            
            <div class="off-wrapper">
              <input type="text"  name ="discount_amount" value="<?php echo $this->_tpl_vars['data']['discount_amount']; ?>
" class="off-value">
              <span>% Off</span>
            </div>
              <span class="error_msg"><?php echo $this->_tpl_vars['error']['discount_amount']; ?>
</span>
        </li>
        <li>
          <label>Applicable To</label>
          <div class="radio-section">
            <input type="radio" value="0" class="input-radio" <?php if ($this->_tpl_vars['data']['applicable_to'] == 0): ?>checked<?php endif; ?> name="applicable_to" id="discount-3">
            <label for="discount-3"></label>
            <span class="radio-text">Sub-total</span>
            
            <input type="radio" value="1" class="input-radio" name="applicable_to" id="discount-4" <?php if ($this->_tpl_vars['data']['applicable_to'] == 1): ?>checked<?php endif; ?>>
            <label for="discount-4"></label>
            <span class="radio-text">Total</span> </div>
        </li>
        <li>
          <label>Usage</label>
          <div class="radio-section">
            <input type="radio" value="0" class="input-radio" <?php if ($this->_tpl_vars['data']['uses_type'] == 0): ?>checked<?php endif; ?> name="uses_type" id="discount-5">
            <label for="discount-5"></label>
            <span class="radio-text">Limited Usage</span>
            
            <input type="radio" value="1" class="input-radio" name="uses_type" id="discount-6" <?php if ($this->_tpl_vars['data']['uses_type'] == 1): ?>checked<?php endif; ?> >
            <label for="discount-6"></label>
            <span class="radio-text">Unlimited Usage</span> </div>
            
            <div class="off-wrapper">
              <input type="text" name="uses_amount" value="<?php echo $this->_tpl_vars['data']['uses_amount']; ?>
" class="off-value">
              <span>Uses</span>
            </div>
             <span class="error_msg"> <?php echo $this->_tpl_vars['error']['uses_amount']; ?>
</span>
        </li>
		<li>
          <div class="radio-section">
            <span class="multi-left">
               <input type="checkbox" <?php if ($this->_tpl_vars['data']['one_per_user'] == 1): ?> checked="checked" <?php endif; ?> value = "1" id="checkbox-2-1" class="input-checkbox" name="one_per_user"    >
               <label for="checkbox-2-1" class="multisel-ckeck"></label>
            </span>
            <span class="radio-text">Limit to 1 use per account</span></div>            
            
            <!--<span class="error_msg"><?php echo $this->_tpl_vars['error']['minimum_order_amount']; ?>
</span>-->
        </li>
        <li style="float: right;margin-top: 32px;">
          <div class="radio-section">
            <span class="multi-left">
               <input type="checkbox" <?php if ($this->_tpl_vars['data']['maximum_order'] == 1): ?> checked="checked" <?php endif; ?> value = "1" id="checkbox-2-3" class="input-checkbox" name="maximum_order"    >
               <label for="checkbox-2-3" class="multisel-ckeck"></label>
            </span>
            <span class="radio-text">Maximum Order Amount</span></div>
            
            <div class="off-wrapper">
              <input type="text" value="<?php echo $this->_tpl_vars['data']['maximum_order_amount']; ?>
" class="off-value" name="maximum_order_amount">

            </div>
              <span class="error_msg"><?php echo $this->_tpl_vars['error']['maximum_order_amount']; ?>
</span>
        </li>
        <li style="float: left;height: auto;">
          <div class="radio-section">
            <span class="multi-left">
<input type="checkbox" <?php if ($this->_tpl_vars['data']['minimum_order'] == 1): ?> checked="checked" <?php endif; ?> value="1" id="checkbox-2-2" class="input-checkbox" name="minimum_order">
               <label for="checkbox-2-2" class="multisel-ckeck"></label>
            </span>
            <span class="radio-text">Minumum Order Amount</span></div>
            
            <div class="off-wrapper">
              <input type="text" value="<?php echo $this->_tpl_vars['data']['minimum_order_amount']; ?>
" class="off-value" name="minimum_order_amount">
            </div>
              <span class="error_msg"><?php echo $this->_tpl_vars['error']['minimum_order_amount']; ?>
</span>
        </li>
        <li>
          <label>Expiration</label>
          <input type="text" class="expiration" placeholder="<?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%a, %b %e, %Y") : smarty_modifier_date_format($_tmp, "%a, %b %e, %Y")); ?>
" name="expiration" value="<?php echo $this->_tpl_vars['date']; ?>
" > <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/calendar.png"></a>
       <span class="error_msg"><?php echo $this->_tpl_vars['error']['expiration']; ?>
</span>
 </li>
        
      </ul>
      
      <div class="button">
        <a class="save" href="#" onclick="document.getElementById('updateDiscount').submit();" >SAVE</a>
        <a class="cancel" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
discounts">CANCEL</a>
      </div>

 </form>     
     </div>

     
  </div>