<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:36:49
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\discounts.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c83e16c7547_88779413',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f70896b98b203bae430e1daa9e69da693e3c70c2' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\discounts.tpl',
      1 => 1600125372,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c83e16c7547_88779413 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
     <div class="webpage">
       <h1>DISCOUNTS</h1>
       
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="8%">LIVE</th>
          <th width="14%">DISCOUNT NAME</th>
          <th width="12%">CODE</th>
          <th width="11%">DISCOUNT TYPE</th>
          <th width="6%">AMOUNT</th>
          <th width="11%">APPLICABLE TO</th>
          <th width="5%">MIN</th>
          <th width="6%">MAX</th>
          <th width="7%">TIMES USED</th>
          <th colspan="3">USES</th>
        </tr>
		<form name="discount_live" id= "discount_live" method="post" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
discounts/updateIsLive" enctype="multipart/form-data">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['discountList']->value, 'data', false, 'k');
$_smarty_tpl->tpl_vars['data']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->do_else = false;
?>       
			<tr class="<?php if ($_smarty_tpl->tpl_vars['k']->value%2 == 0) {?>even<?php }?>">
			  <td>
				 <span class="multi-left">
			
				   <input type="checkbox" id="checkbox-2-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" class="input-checkbox" name="live[]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_live'] == 1) {?>checked ="checked"<?php }?> >
				   <label for="checkbox-2-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" class="multisel-ckeck"></label>
				</span>
			  </td>
			  <td><?php echo $_smarty_tpl->tpl_vars['data']->value['name'];?>
</td>
			  <td><?php echo $_smarty_tpl->tpl_vars['data']->value['code'];?>
</td>
			  <td><?php if ($_smarty_tpl->tpl_vars['data']->value['type'] == 0) {?>% Off<?php } else { ?>Fixed Amount<?php }?></td>
			  <td><?php if ($_smarty_tpl->tpl_vars['data']->value['type'] == 1) {?>$<?php }
echo $_smarty_tpl->tpl_vars['data']->value['discount_amount'];
if ($_smarty_tpl->tpl_vars['data']->value['type'] == 0) {?>%<?php }?></td>
			  <td><?php if ($_smarty_tpl->tpl_vars['data']->value['applicable_to'] == 0) {?>Sub-Total<?php } else { ?>Total<?php }?></td>
			  <td><?php if ($_smarty_tpl->tpl_vars['data']->value['minimum_order'] == 1) {?>$<?php echo $_smarty_tpl->tpl_vars['data']->value['minimum_order_amount'];
} else { ?>-<?php }?></td>
			  <td><?php if ($_smarty_tpl->tpl_vars['data']->value['maximum_order'] == 1) {?>$<?php echo $_smarty_tpl->tpl_vars['data']->value['maximum_order_amount'];
} else { ?>-<?php }?></td>
			  <td><?php echo $_smarty_tpl->tpl_vars['data']->value['count'];?>
</td>
			  <td width="11%"><?php if ($_smarty_tpl->tpl_vars['data']->value['uses_type'] == 0) {
echo $_smarty_tpl->tpl_vars['data']->value['uses_amount'];
} else { ?>Unlimited<?php }?></td>
			  <td width="3%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
discounts/editDiscounts/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">edit</a></td>
			  <td width="5%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
discounts/deleteItems/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" onclick="return confirm('Are you sure you want to delete?')">remove</a></td>
			</tr>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>		

      </tbody></table>
      
      <input type="submit" value="Update">
  </form>     
      <a class="add-user" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
Discounts/newDiscount">ADD DISCOUNT</a>
        
    </div>
  </div>
<?php }
}
