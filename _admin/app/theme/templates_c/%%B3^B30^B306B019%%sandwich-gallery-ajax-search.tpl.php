<?php /* Smarty version 2.6.25, created on 2018-04-03 22:05:19
         compiled from sandwich-gallery-ajax-search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'sandwich-gallery-ajax-search.tpl', 1, false),array('modifier', 'is_array', 'sandwich-gallery-ajax-search.tpl', 52, false),array('modifier', 'in_array', 'sandwich-gallery-ajax-search.tpl', 53, false),)), $this); ?>
<?php if (count($this->_tpl_vars['GALLARY_DATA']) > 0): ?>
        
        <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['GALLARY_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
     
     
     
              <?php $this->assign('prot_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
              if($c){
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }}
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
      
      
                  <?php if (is_array($this->_supers['session']['orders']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'], $this->_supers['session']['orders']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
        
        <tr <?php if ($this->_sections['sandwitch']['index'] % 2 != 0): ?> class="even sandwichlist" <?php endif; ?> data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-formatdate="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]; ?>
"  data-date="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" >
		
		<td width="10%"><div class="plus <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sotd'] == 1): ?> white <?php else: ?> black <?php endif; ?> sotd" data-sandwich="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
">+</div></td>	
		
		<td width="10%"><div class="plus <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['trnd'] == 1): ?> white <?php else: ?> black <?php endif; ?> trnd" data-sandwich="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
">+</div></td>	
        
        <td width="10%" align="center">
              <span class="multi-left">
               <input <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['is_public'] == 1): ?> checked="checked" <?php endif; ?> type="checkbox" name="" class="input-checkbox" id="checkbox-<?php echo $this->_sections['sandwitch']['index']; ?>
">
               <label class="multisel-ckeck" for="checkbox-<?php echo $this->_sections['sandwitch']['index']; ?>
"></label>
              </span>
          </td>
          <td><?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
</td>
          <td><input name="sandwich_like" id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['like_count']; ?>
" type="text" class="sandwich_like" style="width:80px;" /></td>
          <td><input name="sandwich_purchase" id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['purchase_count']; ?>
" type="text" class="sandwich_purchase" style="width:80px;" /></td>
          <td>$ <?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
</td>
          <td><a href="#" class="flag"><?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['flag'] == 1): ?><img width="15" height="20" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/flag.png">unflag<?php endif; ?></a></td>
          <td width="6%"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/edit/<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
">edit</a></td>
          <td width="6%"><a href="#" class="remove_gallery_item">remove</a></td>
        </tr>
        
        <?php endfor; endif; ?>
        
        <?php endif; ?>