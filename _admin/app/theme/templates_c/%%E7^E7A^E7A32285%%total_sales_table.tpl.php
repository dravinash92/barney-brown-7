<?php /* Smarty version 2.6.25, created on 2020-09-30 00:23:18
         compiled from total_sales_table.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'total_sales_table.tpl', 1, false),array('modifier', 'string_format', 'total_sales_table.tpl', 66, false),)), $this); ?>
	<?php if (count($this->_tpl_vars['orders']) > 0): ?>
		<?php 	
			$items_total= $tax_total = $sub_total = $delivery_fee_total  = $tip_total = $total = $dscnt_total = $gft_total = $refund = $paid = 0;
		 ?>	
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr>
			  <th>ORDER #</th>
			  <th>DATE/TIME</th>
			  <th>CUSTOMER NAME</th>
			  <!-- <th>ITEMS</th> -->
			  <th>SUBTOT</th>
			  <th>TAX</th>
			  <th>FEE</th>
			  <th>TIP</th>
			  <th>TOTAL</th>
			  <th>DSCNT</th>
			  <th>GFT CRD</th>
			  <th>REFUND</th>
			  <th>PAID</th>
			</tr>
			<?php $_from = $this->_tpl_vars['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<?php $this->assign('item_total', $this->_tpl_vars['v']->item_total-$this->_tpl_vars['v']->total_tax); ?>				
				<?php $this->assign('tax', $this->_tpl_vars['v']->total_tax); ?>
				<?php $this->assign('sub', $this->_tpl_vars['v']->sub_total); ?>
				<?php $this->assign('delivery_fee', $this->_tpl_vars['v']->delivery_fee); ?>
				<?php $this->assign('tip', $this->_tpl_vars['v']->tip); ?>
				<?php $this->assign('calc_total', $this->_tpl_vars['v']->calc_total); ?>
				<?php $this->assign('off_amount', $this->_tpl_vars['v']->off_amount); ?>
				<?php $this->assign('total', $this->_tpl_vars['v']->total); ?>
				<?php 
					$items_total += $this->get_template_vars('item_total');
					$tax_total += $this->get_template_vars('tax');
					$sub_total += $this->get_template_vars('sub');
					$delivery_fee_total += $this->get_template_vars('delivery_fee');
					$tip_total += $this->get_template_vars('tip');
					$calc_total += $this->get_template_vars('calc_total');
					$dscnt_total += $this->get_template_vars('off_amount');
					$paid += $this->get_template_vars('total');
				 ?>			
			<?php endforeach; endif; unset($_from); ?>
			
			<tr class="subheader">
			  <td colspan="3">TOTAL</td>
			  <!-- <td>$<?php echo sprintf('%0.2f',$items_total) ?></td> -->
			  <td>$<?php echo sprintf('%0.2f',$sub_total) ?></td>
			  <td>$<?php echo sprintf('%0.2f',$tax_total) ?></td>
			  <td>$<?php echo sprintf('%0.2f',$delivery_fee_total) ?></td>
			  <td>$<?php echo sprintf('%0.2f',$tip_total) ?></td>
			  <td>$<?php echo sprintf('%0.2f',$calc_total+$tax_total) ?></td>
			  <td>-$<?php echo sprintf('%0.2f',$dscnt_total) ?></td>
			  <td>-$0.00</td>
			  <td>-$0.00</td>
			  <td>$<?php echo sprintf('%0.2f',$calc_total+$tax_total-$dscnt_total) ?></td>
			</tr>			
			
			
			<?php $_from = $this->_tpl_vars['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<?php if (!(!(1 & $this->_tpl_vars['k']))): ?> 
				<tr class="">
				<?php else: ?>	
				<tr class="even">
				<?php endif; ?>	
				  <td><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
reports/showOrderDetails/<?php echo $this->_tpl_vars['v']->order_id; ?>
"><?php echo $this->_tpl_vars['v']->order_number; ?>
</a></td>
				  <td><?php echo $this->_tpl_vars['v']->o_date; ?>
</td>
				  <td><?php echo $this->_tpl_vars['v']->first_name; ?>
 <?php echo $this->_tpl_vars['v']->last_name; ?>
</td>
				<!--   <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->amount_wt)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td> -->
				  <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->sub_total)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				  <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->total_tax)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				  <td>$<?php echo $this->_tpl_vars['v']->delivery_fee; ?>
</td>
				  <td>$<?php echo $this->_tpl_vars['v']->tip; ?>
</td>
				  <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->calc_total+$this->_tpl_vars['v']->total_tax)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				  <td>-$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->off_amount)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				  <td>-$0.00</td>
				  <td>-$0.00</td>
				  <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']->total)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
				</tr>				
			<?php endforeach; endif; unset($_from); ?>					
			
			
			</tbody>
		</table>	
	<?php else: ?>
		<h3>No orders found.</h3>	
	<?php endif; ?>	