<?php
/* Smarty version 3.1.39, created on 2021-03-25 09:07:16
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\total_sales.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c8b04d7da91_97390959',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '47e692e8686ee89975db2b9ac905cda7c12578e1' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\total_sales.tpl',
      1 => 1616677634,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c8b04d7da91_97390959 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <div class="item-report">
      <h1>TOTAL SALES</h1>
      <div class="filter_wrap">
      <form name="totals_sales_report_filter">
      <div class="store-location">
         <label>Name of Stores</label>
         <select name="pickup_store">
           <option value="">Select Store</option>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickupstores']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->store_name;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->address1;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->address2;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->zip;?>
</option>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         </select>
        <ul>
          <li><label>Order Type</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="order_type[]" value="true">
            <label for="checkbox-2-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Delivery</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-5" class="input-checkbox" name="order_type[]" value="false">
            <label for="checkbox-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Pick-Up</span>
          </li>
        </ul>
        <ul>
          <li><label>Order Status</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-3-1" class="input-checkbox" name="order_status[]" value="0,0">
            <label for="checkbox-3-1" class="multisel-ckeck"></label>
            </span> <span class="radio-text">New</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-2" class="input-checkbox" name="order_status[]" value="1,5">
            <label for="checkbox-3-2" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Processed</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-3" class="input-checkbox" name="order_status[]" value="2,6">
            <label for="checkbox-3-3" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Ready</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-4" class="input-checkbox" name="order_status[]" value="3,7">
            <label for="checkbox-3-4" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Out</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-5" class="input-checkbox" name="order_status[]" value="4,8">
            <label for="checkbox-3-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Received</span>
          </li>
        </ul>
      </div>
      
      <div class="date-time-settings">
         <label>Date/Time Settings</label>
         <ul>
           <li>
            <div class="radio-left">
             <div class="radio-section">
                <input type="radio" value="order_date"  class="input-radio" name="order_date_filter" id="1">
                <label for="1"></label>
              </div>
              <span class="radio-text">Order Placement</span>
             </div>
             <div class="radio-right">
              <div class="radio-section">
                  <input type="radio" value="date" checked class="input-radio" name="order_date_filter" id="2">
                <label for="2"></label>
                </div>
                <span class="radio-text">Order Fulfillment</span>
              </div>
           </li>
           <li>
             <label>From</label>
          <input type="text" class="expiration" placeholder="Select date" name="from_date" value="<?php echo $_smarty_tpl->tpl_vars['from_date']->value;?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/calendar.png"></a>
            <select name="from_time" name="from_time">
				<?php echo $_smarty_tpl->tpl_vars['fromTimes']->value;?>

             </select>
           </li>
            <li>
             <label>To</label>
          <input type="text" class="expiration" placeholder="Select date" name="to_date" value="<?php echo $_smarty_tpl->tpl_vars['to_date']->value;?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/calendar.png"></a>
            <select class="to_time" name="to_time">
				<?php echo $_smarty_tpl->tpl_vars['toTimes']->value;?>
				
             </select>
           </li>
         </ul>
      </div>
      
      <div class="type-purchase-report">
        <ul>
          <li><label>Type of Purchase</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-4-5" class="input-checkbox" name="type_purchase[]" value="0">
            <label for="checkbox-4-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Website</span> </li>
            
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-8" class="input-checkbox" name="type_purchase[]" value="1">
            <label for="checkbox-3-8" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Phone Order</span>
          </li>
          
        </ul>
        
      </div>
      
      <div class="type-purchase-discount">
        <ul>
          <li><label>Discounts</label></li>
          <li>
             <select name="discount">
              <option value="">Select Discount</option>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['discounts']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->code;?>
</option>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             </select>
           </li>
           <li><label>Refund Types</label></li>
          <li>
             <select name="refund_types">
              <option value="">Select Refund Types</option>
              <option value="">Refund Types</option>
             </select>
           </li>
           
          <li><label>System User</label></li>
          <li>
             <select name="system_user">
              <option value="">Select User</option>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['systemusers']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['v']->value->last_name;?>
</option>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             </select>
           </li>
           
           <li><label>Search</label></li>
           <li>
             <input type="text" name="search_text" >
             <a href="javascript:void(0)" class="do_total_sales_filter"><img width="26" height="26" alt="" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/link.png"></a>
          </li>          
        </ul>
         
      </div>
      </form>
		</div>
		<div class="reports_table">
			<?php if (count($_smarty_tpl->tpl_vars['orders']->value) > 0) {?>
			<?php 	
				$items_total= $tax_total = $sub_total  = $delivery_fee_total = $tip_total = $total = $dscnt_total = $gft_total = $refund = $paid = 0;
			?>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody>
			<tr>
			  <th>ORDER #</th>
			  <th>DATE/TIME</th>
			  <th>CUSTOMER NAME</th>
			  <!-- <th>ITEMS</th> -->
			  <th>SUBTOT</th>
			  <th>TAX</th>
        <th>DELIVERY FEE</th>
        <th>TIP</th> 
			  <th>TOTAL VALUE</th>
			  <th>DSCNT</th>
			  <th>GFT CRD</th>
			  <th>REFUND</th>
			  <th>TOTAL PAID</th>
			</tr>
			
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orders']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<?php $_smarty_tpl->_assignInScope('item_total', $_smarty_tpl->tpl_vars['v']->value->item_total-$_smarty_tpl->tpl_vars['v']->value->total_tax);?>				
				<?php $_smarty_tpl->_assignInScope('tax', $_smarty_tpl->tpl_vars['v']->value->total_tax);?>
				<?php $_smarty_tpl->_assignInScope('sub', $_smarty_tpl->tpl_vars['v']->value->sub_total);?>
        <?php $_smarty_tpl->_assignInScope('delivery_fee', $_smarty_tpl->tpl_vars['v']->value->delivery_fee);?>
				<?php $_smarty_tpl->_assignInScope('tip', $_smarty_tpl->tpl_vars['v']->value->tip);?>
				<?php $_smarty_tpl->_assignInScope('calc_total', $_smarty_tpl->tpl_vars['v']->value->calc_total);?>
				<?php $_smarty_tpl->_assignInScope('off_amount', $_smarty_tpl->tpl_vars['v']->value->off_amount);?>
				<?php $_smarty_tpl->_assignInScope('total', $_smarty_tpl->tpl_vars['v']->value->total);?>
				<?php 
					$items_total += $_smarty_tpl->get_template_vars('item_total');
					$tax_total += $_smarty_tpl->get_template_vars('tax');
					$sub_total += $_smarty_tpl->get_template_vars('sub');
          $delivery_fee_total += $_smarty_tpl->get_template_vars('delivery_fee');
					$tip_total += $_smarty_tpl->get_template_vars('tip');
					$calc_total += $_smarty_tpl->get_template_vars('calc_total');
					$dscnt_total += $_smarty_tpl->get_template_vars('off_amount');
					$paid += $_smarty_tpl->get_template_vars('total');
				?>			
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			
			<tr class="subheader">
			  <td colspan="3">TOTAL</td>
			  <!-- <td>$<?php echo sprintf('%0.2f',$items_total)?></td> -->
			  <td>$<?php echo sprintf('%0.2f',$sub_total)?></td>
			  <td>$<?php echo sprintf('%0.2f',$tax_total)?></td>
        <td>$<?php echo sprintf('%0.2f',$delivery_fee_total)?></td>
			  <td>$<?php echo sprintf('%0.2f',$tip_total)?></td>
			  <td>$<?php echo sprintf('%0.2f',$calc_total+$tax_total)?></td>
			  <td>-$<?php echo sprintf('%0.2f',$dscnt_total)?></td>
			  <td>-$0.00</td>
			  <td>-$0.00</td>
			  <td>$<?php echo sprintf('%0.2f',$calc_total+$tax_total-$dscnt_total)?></td>
			</tr>			
			
			
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orders']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<?php if ((1 & $_smarty_tpl->tpl_vars['k']->value)) {?> 
				<tr class="">
				<?php } else { ?>	
				<tr class="even">
				<?php }?>	
				  <td><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
reports/showOrderDetails/<?php echo $_smarty_tpl->tpl_vars['v']->value->order_id;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
</a></td>
				  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->o_date;?>
</td>
				  <td><?php echo $_smarty_tpl->tpl_vars['v']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['v']->value->last_name;?>
</td>
				  <!-- <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->amount_wt);?>
</td> -->
				  <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->sub_total);?>
</td>
				  <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->total_tax);?>
</td>
          <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_fee;?>
</td>
				  <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->tip;?>
</td>
				  <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->calc_total+sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->total_tax);?>
</td>
				  <td>-$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->off_amount);?>
</td>
				  <td>-$0.00</td>
				  <td>-$0.00</td>
				  <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->total);?>
</td>
				</tr>				
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>					
			
			</tbody>
		</table>	
		<?php } else { ?>
			<h3>No orders found.</h3>	
		<?php }?>	
		</div>	
		
    </div>
  </div>
<?php }
}
