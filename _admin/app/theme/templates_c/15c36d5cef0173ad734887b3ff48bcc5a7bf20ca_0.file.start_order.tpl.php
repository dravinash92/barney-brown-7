<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:24:38
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\start_order.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c8106055054_46704036',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '15c36d5cef0173ad734887b3ff48bcc5a7bf20ca' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\start_order.tpl',
      1 => 1600125388,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c8106055054_46704036 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <div class="start-order">
       <h1>NEW ORDER</h1>
       <form class="new-order-form" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/index/">
       	<div class="text-box-holder">
          <label>First Name</label>
          <input name="name" type="text" class="main-text-box common-text-width" value="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
">
          <a href="#" class="arrow">&nbsp;</a>
        </div>
       	<div class="text-box-holder">
          <label>Last Name</label>
          <input name="lastname" type="text" class="main-text-box common-text-width" value="<?php echo $_smarty_tpl->tpl_vars['lastname']->value;?>
">
          <a href="#" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Email Address</label>
          <input name="email" type="text" class="main-text-box common-text-width" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
">
          <a href="#" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Phone Number</label>
          <input name="" type="text" class="main-text-box phone-text-width">
          <input name="" type="text" class="main-text-box phone-text-width">
          <input name="" type="text" class="main-text-box phone-text-width">
          <input name="" type="text" class="main-text-box phone-text-width">
          <a href="#" class="arrow">&nbsp;</a>
        </div>                
       </form>
       
       <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/newuseraccount" class="new-account">New Account</a>
       
       
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <th width="22%">CUSTOMER NAME</th>
            <th width="32%">EMAIL</th>
            <th width="18%">PHONE</th>
            <th width="13%">SALES TOTAL</th>
          </tr>
           <?php
$__section_users_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['users_data']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_users_0_total = $__section_users_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_users'] = new Smarty_Variable(array());
if ($__section_users_0_total !== 0) {
for ($__section_users_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] = 0; $__section_users_0_iteration <= $__section_users_0_total; $__section_users_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']++){
?> 
          <tr>
            <td><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/addToCart/<?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['uid'];?>
"><?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['last_name'];?>
</a></td>
            <td><?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['username'];?>
</td>
            <td></td>
            <td>$<?php echo $_smarty_tpl->tpl_vars['users_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_users']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_users']->value['index'] : null)]['total_price'];?>
</td>
          </tr>
		  	<?php
}
}
?>  
       </tbody></table>
       
    </div>
  </div>
<?php }
}
