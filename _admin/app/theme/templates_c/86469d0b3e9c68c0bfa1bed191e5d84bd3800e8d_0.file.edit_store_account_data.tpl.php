<?php
/* Smarty version 3.1.39, created on 2021-03-31 00:37:09
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\edit_store_account_data.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6063fc75225871_60005065',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '86469d0b3e9c68c0bfa1bed191e5d84bd3800e8d' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\edit_store_account_data.tpl',
      1 => 1617165424,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063fc75225871_60005065 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_admin\\app\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<div class="container">
    <div class="user-account">
       <h1>EDIT STORE ACCOUNT</h1>
       <form name="maincontents" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts/updateStoreAccount" method="POST">
	   <div class="column column-1">
        <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>
          <label>Store Name</label>
          <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->store_name;?>
" name="store_name">
          <label>Store Address Line One</label>
          <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->address1;?>
" name="address1">
          <label>Store Address Line Two</label>
          <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->address2;?>
" name="address2">
          <label>Store Zipcode</label>
          <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->zip;?>
" name="zip">
          <label>Username</label>
          <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->username;?>
" name="username" disabled>
          <label>Password</label>
          <input type="password" name="password">
          <label>Confirm Password</label>
          <input type="password" value="" name="con_password">
          <label>Reports Pin</label>
          <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->report_pin;?>
" name="report_pin">
     <span class="multi-left">
               <input type="checkbox" id="checkbox-2-0" class="input-checkbox" name="ip_checked" value="" <?php if ($_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->restrict_ip != '') {?> checked <?php }?>>
          <label for="checkbox-2-0" class="multisel-ckeck"></label>
         </span><label class="restrict">Restrict IP Access to:</label>
          <input type="text" name="restrict_ip" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->restrict_ip;?>
">
          
                   
           <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->id;?>
" />
          
        
          <button type="button" class="save_store_account">SAVE STORE</button>
          <a class="cancel" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts">CANCEL</a>
		
		</div>
		
	    <!--    
      	 <div class="pick-up"  style="margin-left: 312px; width: 368px; height: 623px; margin-top: -578px;"> -->
        <?php } else { ?>
          <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->id;?>
" />
        <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->store_name;?>
" name="store_name">
        <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->address1;?>
" name="address1">
        <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->address2;?>
" name="address2">
        <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->zip;?>
" name="zip">
        <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->username;?>
" name="username" disabled>
        
         <!-- <div class="pick-up" style=" width: 368px; " > -->
        <?php }?>
		<div class="column column-2">
		<div class="pick-up" style=" width: 368px; " >
         <h3>Pick-Up / Delivery Windows</h3>
          <ul>
           <li class="radio"></li>
           <li class="week">&nbsp;</li>
           <li class="from">From</li>
           <li class="to">To</li>
         </ul>
         
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="day0" value="Sunday" <?php if ($_smarty_tpl->tpl_vars['store_timings_day']->value[0] == 'Sunday' && $_smarty_tpl->tpl_vars['store_active']->value[0] == 1) {?> checked <?php }?>>
               <label for="checkbox-2-1" class="multisel-ckeck"></label>
              </span>
           </li>
      
           <li class="week">Sunday</li>
           <li class="from"> 
             <select name="day_open0">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_open']->value[0] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
			</select>

           </li>
           <li class="to">
                        <select name="day_close0">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_close']->value[0] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
          <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['pickup_active']->value[0] == 1) {?> checked <?php }?> id="checkbox-0-pickup" class="input-checkbox" name="checkbox-0-pickup" value="1">
                <label for="checkbox-0-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['delivery_active']->value[0] == 1) {?> checked <?php }?> id="checkbox-0-delivery" class="input-checkbox" name="checkbox-0-delivery" value="1">
                <label for="checkbox-0-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
          </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-2" class="input-checkbox" name="day1" value="Monday" <?php if ($_smarty_tpl->tpl_vars['store_timings_day']->value[1] == 'Monday' && $_smarty_tpl->tpl_vars['store_active']->value[1] == 1) {?> checked <?php }?>>
               <label for="checkbox-2-2" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Monday</li>
           <li class="from"> 
             <select name="day_open1">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_open']->value[1] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected  <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             </select>
           </li>
           <li class="to">
             <select name="day_close1">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_close']->value[1] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
          
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['pickup_active']->value[1] == 1) {?> checked <?php }?> id="checkbox-1-pickup" class="input-checkbox" name="checkbox-1-pickup" value="1">
                <label for="checkbox-1-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['delivery_active']->value[1] == 1) {?> checked <?php }?> id="checkbox-1-delivery" class="input-checkbox" name="checkbox-1-delivery" value="1">
                <label for="checkbox-1-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-3" class="input-checkbox" name="day2" value="Tuesday" <?php if ($_smarty_tpl->tpl_vars['store_timings_day']->value[2] == 'Tuesday' && $_smarty_tpl->tpl_vars['store_active']->value[2] == 1) {?> checked <?php }?>>
               <label for="checkbox-2-3" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Tuesday</li>
           <li class="from"> 
             <select name="day_open2">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_open']->value[2] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close2">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_close']->value[2] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['pickup_active']->value[2] == 1) {?> checked <?php }?> id="checkbox-2-pickup" class="input-checkbox" name="checkbox-2-pickup" value="1">
                <label for="checkbox-2-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['delivery_active']->value[2] == 1) {?> checked <?php }?> id="checkbox-2-delivery" class="input-checkbox" name="checkbox-2-delivery" value="1">
                <label for="checkbox-2-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-4" class="input-checkbox" name="day3" value="Wednesday" <?php if ($_smarty_tpl->tpl_vars['store_timings_day']->value[3] == 'Wednesday' && $_smarty_tpl->tpl_vars['store_active']->value[3] == 1) {?> checked <?php }?>>
               <label for="checkbox-2-4" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Wednesday</li>
           <li class="from"> 
             <select name="day_open3">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_open']->value[3] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close3">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_close']->value[3] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['pickup_active']->value[3] == 1) {?> checked <?php }?> id="checkbox-3-pickup" class="input-checkbox" name="checkbox-3-pickup" value="1">
                <label for="checkbox-3-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['delivery_active']->value[3] == 1) {?> checked <?php }?> id="checkbox-3-delivery" class="input-checkbox" name="checkbox-3-delivery" value="1">
                <label for="checkbox-3-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="day4" value="Thursday" <?php if ($_smarty_tpl->tpl_vars['store_timings_day']->value[4] == 'Thursday' && $_smarty_tpl->tpl_vars['store_active']->value[4] == 1) {?> checked <?php }?>>
               <label for="checkbox-2-5" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Thursday</li>
           <li class="from"> 
             <select name="day_open4">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_open']->value[4] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close4">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_close']->value[4] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
          
           <li class="deactdelvery" >
               <span class="multi-left" >
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['pickup_active']->value[4] == 1) {?> checked <?php }?> id="checkbox-4-pickup" class="input-checkbox" name="checkbox-4-pickup" value="1">
                <label for="checkbox-4-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['delivery_active']->value[4] == 1) {?> checked <?php }?> id="checkbox-4-delivery" class="input-checkbox" name="checkbox-4-delivery" value="1">
                <label for="checkbox-4-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-6" class="input-checkbox" name="day5" value="Friday" <?php if ($_smarty_tpl->tpl_vars['store_timings_day']->value[5] == 'Friday' && $_smarty_tpl->tpl_vars['store_active']->value[5] == 1) {?> checked <?php }?>>
               <label for="checkbox-2-6" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Friday</li>
           <li class="from"> 
             <select name="day_open5">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_open']->value[5] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close5">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_close']->value[5] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
          
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['pickup_active']->value[5] == 1) {?> checked <?php }?> id="checkbox-5-pickup" class="input-checkbox" name="checkbox-5-pickup" value="1">
                <label for="checkbox-5-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['delivery_active']->value[5] == 1) {?> checked <?php }?> id="checkbox-5-delivery" class="input-checkbox" name="checkbox-5-delivery" value="1">
                <label for="checkbox-5-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-7" class="input-checkbox" name="day6" value="Saturday" <?php if ($_smarty_tpl->tpl_vars['store_timings_day']->value[6] == 'Saturday' && $_smarty_tpl->tpl_vars['store_active']->value[6] == 1) {?> checked <?php }?>>
               <label for="checkbox-2-7" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Saturday</li>
           <li class="from"> 
             <select name="day_open6">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_open']->value[6] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           <li class="to">
             <select name="day_close6">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timesections']->value, 'timesection');
$_smarty_tpl->tpl_vars['timesection']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['timesection']->value) {
$_smarty_tpl->tpl_vars['timesection']->do_else = false;
?>
					<option value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M');?>
" <?php if ($_smarty_tpl->tpl_vars['store_timings_close']->value[6] == smarty_modifier_date_format($_smarty_tpl->tpl_vars['timesection']->value,'%H:%M')) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['timesection']->value;?>
</option>
			 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>        
             </select>
           </li>
           
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['pickup_active']->value[6] == 1) {?> checked <?php }?> id="checkbox-6-pickup" class="input-checkbox" name="checkbox-6-pickup" value="1">
                <label for="checkbox-6-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['delivery_active']->value[6] == 1) {?> checked <?php }?> id="checkbox-6-delivery" class="input-checkbox" name="checkbox-6-delivery" value="1">
                <label for="checkbox-6-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
            
         </ul>
         
         <!--
         <div class="deactivate">
            <ul>
              <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-8" class="input-checkbox" name="" value="">
                <label for="checkbox-2-8" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
              </li>
              <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-9" class="input-checkbox" name="" value="">
                <label for="checkbox-2-9" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
              </li>
            </ul>
         
         </div>
         -->
           <?php if ($_smarty_tpl->tpl_vars['isStore']->value) {?>
                 <button type="button" class="save_store_account">SAVE STORE</button>
          <a class="cancel" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
accounts">CANCEL</a>
          <?php }?>
       </div>
	   <div class="pick-up" style=" width: 368px; " >
         <h3>CLOSE SPECIFIC DAY</h3>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="specificDay" class="input-checkbox" name="specificday_active" value="1"  <?php if ($_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->specificday_active == 1) {?>  checked <?php }?>>
               <label for="specificDay" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Close Specific Day</li>
          </ul>
           <div style="clear:both;"></div>
          <ul>
           <li class="deactdelvery text-box-holder2" >
               <span class="multi-left">
                  <input name="specificday" id="search_delivery_date" type="text" class="text-box" name="specificDay" value="<?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->specificday;?>
">
                  <a href="#" class="date-icon"></a>
               </span>
            </li>
         </ul>
         <div style="clear:both;"></div>
         <ul style="margin-top: 20px;">
           <li class="deactdelvery" >
           <label  class="multisel-ckeck">Message</label>
               <span class="multi-left">
                  <textarea rows="" cols="" id="specificday_message" name="specificday_message"><?php echo $_smarty_tpl->tpl_vars['store_accounts_data']->value->pickup_stores->specificday_message;?>
</textarea>
               </span>
            </li>
         </ul>
         </div>
	   </div>
	   
	   <div class="column column-3">
	   
       <?php if ($_smarty_tpl->tpl_vars['isStore']->value == 0) {?>
       <div class="zipcode-wrapper ten">
         <h3>ALLOWED ZIP CODES($10 MIN)</h3> 
           <div class="zip_wrap">
         <ul class="heading">
           <li class="zip-code">Zip Code</li>
           <li class="abbr">Abbr.</li>
           <li class="abbr">Del. Fee</li>
         </ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['store_accounts_data']->value->store_zipcodes, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
         <?php if (($_smarty_tpl->tpl_vars['v']->value->min_order == 10 || $_smarty_tpl->tpl_vars['v']->value->min_order == 0)) {?>
			<ul>
			   <li class="zip-code"><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value->zipcode;?>
" name="zipcode<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
">
			    <input name="zipCodeId" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" />
			   </li>

			   <li class="abbr"><input  maxlength="3" type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->abbreviation;?>
" name="abbreviation<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
">
			    <input type="hidden" name="minAmount<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
" class="minAmount" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->min_order;?>
"></li>
          <li class="delivery-fee"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_fee;?>
" name="deliveryfee<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
"></li>

			    <li class="zipItemrmv"><a class="removeZip" >Remove</a></li>
			</ul>
			<?php }?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</div>
		<input type="hidden" name="zipcount" id="zipcount" value="<?php echo count($_smarty_tpl->tpl_vars['store_accounts_data']->value->store_zipcodes);?>
" />		
       
        <div style="clear:both"><a class="add zip_add" data-minamount="10" href="javascript:void(0)" style="">add</a> </div>
         
       </div>
     <div class="zipcode-wrapper hundred" >
         <h3>ALLOWED ZIP CODES($100 MIN)</h3> 
           <div class="zip_wrap">
         <ul class="heading">
           <li class="zip-code">Zip Code</li>
           <li class="abbr">Abbr.</li>
           <li class="abbr">Del. Fee</li>
         </ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['store_accounts_data']->value->store_zipcodes, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
        <?php if ($_smarty_tpl->tpl_vars['v']->value->min_order == 100) {?>
			<ul>
			   <li class="zip-code"><input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['v']->value->zipcode;?>
" name="zipcode<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
">
			    <input name="zipCodeId" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" />
			   </li>
			   <li class="abbr"><input type="text" maxlength="3" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->abbreviation;?>
" name="abbreviation<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
">
			   <input type="hidden" name="minAmount<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
" class="minAmount" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->min_order;?>
"></li>
         <li class="delivery-fee"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->delivery_fee;?>
" name="deliveryfee<?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
"></li>
			    <li class="zipItemrmv"><a class="removeZip" >Remove</a></li>
			</ul>
			<?php }?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</div>
		<input type="hidden" name="zipcount" id="zipcounthundred" value="<?php echo count($_smarty_tpl->tpl_vars['store_accounts_data']->value->store_zipcodes);?>
" />	
        <div style="clear:both"><a class="add zip_add_hundred" data-minamount="100" href="javascript:void(0)" style="">add</a> </div>
         
       </div>     
         <?php }?>
		 
		</div> 
       </form>
    </div>
  </div>
 
<?php }
}
