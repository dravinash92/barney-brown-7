<?php /* Smarty version 2.6.25, created on 2018-03-14 21:01:48
         compiled from order_search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'order_search.tpl', 11, false),)), $this); ?>
<div class="container">
    <div class="start-order">
       <h1>ORDER SEARCH</h1>
       <form class="new-order-form" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
neworder/orderSearch/">
       	<div class="text-box-holder">
          <label>Search Order Number or Customer</label>
          <input name="search" type="text" class="main-text-box common-text-width" value="<?php echo $this->_tpl_vars['search']; ?>
">
          <a href="#" class="arrow search_order">&nbsp;</a>
        </div>
        </form>
         <?php if (count($this->_tpl_vars['orders']) > 0): ?> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
			  <tbody><tr>
            <th width="22%">CUSTOMER NAME</th>
            <th width="18%">ORDER NUMBER</th>
            <th width="32%">EMAIL</th>
            <th width="18%">PHONE</th>
            <th width="13%">SALES TOTAL</th>
          </tr>
			
		<?php $_from = $this->_tpl_vars['orders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
				<?php if (!(!(1 & $this->_tpl_vars['k']))): ?> 
				<tr class="">
				<?php else: ?>	
				<tr class="even">
				<?php endif; ?>	
			   <td><a href="#"><?php echo $this->_tpl_vars['v']->first_name; ?>
 <?php echo $this->_tpl_vars['v']->last_name; ?>
</a></td>
				<td><?php echo $this->_tpl_vars['v']->order_number; ?>
</td>
				<td><?php echo $this->_tpl_vars['v']->username; ?>
</td>
				<td><?php echo $this->_tpl_vars['v']->phone; ?>
</td>
				<td>$<?php echo $this->_tpl_vars['v']->total; ?>
</td>
			</tr>  
		<?php endforeach; endif; unset($_from); ?> 		
		  
       </tbody>
       </table>
       <?php else: ?>
			<ul class="order-history-inner">				
	            <li style="width:100%;"><span><h3 class="no_order_history">No Previous Orders.</h3></span></li>	            
	        </ul>  
       
       <?php endif; ?>
        
    </div>
  </div>