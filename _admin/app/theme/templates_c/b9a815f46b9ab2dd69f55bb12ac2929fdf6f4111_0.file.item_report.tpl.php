<?php
/* Smarty version 3.1.39, created on 2021-03-31 03:40:51
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\item_report.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6064278382c727_37052924',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b9a815f46b9ab2dd69f55bb12ac2929fdf6f4111' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\item_report.tpl',
      1 => 1616681078,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6064278382c727_37052924 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_admin\\app\\smarty\\libs\\plugins\\function.cycle.php','function'=>'smarty_function_cycle',),1=>array('file'=>'C:\\wamp64\\www\\hashbury\\_admin\\app\\smarty\\libs\\plugins\\function.math.php','function'=>'smarty_function_math',),));
?>
<div class="container">
    <div class="item-report">
      <h1>ITEM REPORT</h1>
      
      <div class="filter_wrap">
      <form name="items_sales_report_filter">
      <div class="store-location">
         <label>Store Location</label>
        <select name="pickup_store">
           <option value="">Select Store</option>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickupstores']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->store_name;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->address1;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->address2;?>
, <?php echo $_smarty_tpl->tpl_vars['v']->value->zip;?>
</option>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         </select>
        <ul>
          <li><label>Order Type</label></li>
          <li>
           <span class="multi-left">
          <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="order_type[]" value="1">
            <label for="checkbox-2-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Delivery</span> </li>
          <li>
            <span class="multi-left">
             <input type="checkbox" id="checkbox-5" class="input-checkbox" name="order_type[]" value="0">
            <label for="checkbox-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Pick-Up</span>
          </li>
        </ul>
        <ul>
          <li><label>Order Status</label></li>
           <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-3-1" class="input-checkbox" name="order_status[]" value="0.0">
            <label for="checkbox-3-1" class="multisel-ckeck"></label>
            </span> <span class="radio-text">New</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-2" class="input-checkbox" name="order_status[]" value="1,5">
            <label for="checkbox-3-2" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Processed</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-3" class="input-checkbox" name="order_status[]" value="2,6">
            <label for="checkbox-3-3" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Ready</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-4" class="input-checkbox" name="order_status[]" value="6,7">
            <label for="checkbox-3-4" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Out</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-5" class="input-checkbox" name="order_status[]" value="7,8">
            <label for="checkbox-3-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Received</span>
          </li>
        </ul>
      </div>
      
      <div class="date-time-settings">
         <label>Date/Time Settings</label>
         <ul>
           <li>
            <div class="radio-left">
             <div class="radio-section">
                <input type="radio" value="order_date" checked class="input-radio" name="order_date_filter" id="1">
                <label for="1"></label>
              </div>
              <span class="radio-text">Order Placement</span>
             </div>
             <div class="radio-right">
              <div class="radio-section">
                  <input type="radio" value="order_delivered" class="input-radio" name="order_date_filter" id="2">
                <label for="2"></label>
                </div>
                <span class="radio-text">Order Fulfillment</span>
              </div>
           </li>
            <li>
             <label>From</label>
          <input type="text" class="expiration" placeholder="Select date" name="from_date" value="<?php echo $_smarty_tpl->tpl_vars['from_date']->value;?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/calendar.png"></a>
            <select name="from_time" name="from_time">				
				<?php echo $_smarty_tpl->tpl_vars['fromTimes']->value;?>

             </select>
           </li>
            <li>
             <label>To</label>
          <input type="text" class="expiration" placeholder="Select date" name="to_date" value="<?php echo $_smarty_tpl->tpl_vars['to_date']->value;?>
"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/calendar.png"></a>
            <select class="to_time" name="to_time">				
				<?php echo $_smarty_tpl->tpl_vars['toTimes']->value;?>
				
             </select>
           </li>
         </ul>
      </div>
      
      <div class="type-purchase">
        <ul>
          <li><label>Type of Purchase</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-4-5" class="input-checkbox" name="type_purchase[]" value="0">
            <label for="checkbox-4-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Website</span> </li>
            
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-8" class="input-checkbox" name="type_purchase[]" value="1">
            <label for="checkbox-3-8" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Phone Order</span>
          </li>
          <li></li>
          <li>
             <label>System User</label>
            <select name="system_user">
              <option value="">Select User</option>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['systemusers']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['v']->value->last_name;?>
</option>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
             </select>
             <a href="javascript:void(0)" class="do_item_report_filter"><img width="26" height="26" alt="" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/link.png"></a>
          </li>           
        </ul>        
      </div>
      </form>
      </div>
      <div class="reports_table">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="sandwiches">
        <tbody><tr>
          <th width="65%">CUSTOM SANDWICHES</th>
           <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        <tr class="subheader">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><span class="hide_totalqty"></span></td>
          <td>$<span class="hide_subtottal"></span></td>
          <td>100%</td>
        </tr>
        <tr class="items">
          <td colspan="5">BREADS</td>
        </tr>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwichitems']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['v']->value->category_id == 1) {?>
		 <tr class="<?php echo smarty_function_cycle(array('values'=>"odd,even"),$_smarty_tpl);?>
 datarow">
          <td><?php echo $_smarty_tpl->tpl_vars['v']->value->item_name;?>
</td>
           <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->item_price;?>

           <?php $_smarty_tpl->_assignInScope('bprice', $_smarty_tpl->tpl_vars['v']->value->item_price);?>
           </td>
          <td>
     
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['breadsanddata']->value, 'bv', false, 'bk');
$_smarty_tpl->tpl_vars['bv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['bk']->value => $_smarty_tpl->tpl_vars['bv']->value) {
$_smarty_tpl->tpl_vars['bv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['bv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('bqty', $_smarty_tpl->tpl_vars['bv']->value['qty']);?>
           <?php echo $_smarty_tpl->tpl_vars['bqty']->value;?>

           <?php $_smarty_tpl->_assignInScope('sum_qty_bread', $_smarty_tpl->tpl_vars['sum_qty_bread']->value+$_smarty_tpl->tpl_vars['bqty']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          
         
          
          </td>
          <td>
         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['breadsanddata']->value, 'bv', false, 'bk');
$_smarty_tpl->tpl_vars['bv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['bk']->value => $_smarty_tpl->tpl_vars['bv']->value) {
$_smarty_tpl->tpl_vars['bv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['bv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('breads_subtotal', $_smarty_tpl->tpl_vars['bqty']->value*$_smarty_tpl->tpl_vars['bprice']->value);?>
          $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['breads_subtotal']->value);?>

          <?php $_smarty_tpl->_assignInScope('sum_all_breads', $_smarty_tpl->tpl_vars['sum_all_breads']->value+$_smarty_tpl->tpl_vars['breads_subtotal']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </td>
		   <td>
		   
		 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['breadsanddata']->value, 'bv', false, 'bk');
$_smarty_tpl->tpl_vars['bv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['bk']->value => $_smarty_tpl->tpl_vars['bv']->value) {
$_smarty_tpl->tpl_vars['bv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['bv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('bqty', $_smarty_tpl->tpl_vars['bv']->value['qty']);?>
         
              <?php echo smarty_function_math(array('equation'=>"(( x / y ) * z )",'x'=>$_smarty_tpl->tpl_vars['bqty']->value,'y'=>$_smarty_tpl->tpl_vars['salesCount']->value,'z'=>100,'format'=>"%.2f"),$_smarty_tpl);?>
%
           <?php $_smarty_tpl->_assignInScope('sum_qty_bread', $_smarty_tpl->tpl_vars['sum_qty_bread']->value+$_smarty_tpl->tpl_vars['bqty']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		   
		   </td>
        </tr>
        <?php }?>
		 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		        
         <tr class="items">
          <td colspan="5">PROTEINS</td>
        </tr>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwichitems']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['v']->value->category_id == 2) {?>
		 <tr class="<?php echo smarty_function_cycle(array('values'=>"odd,even"),$_smarty_tpl);?>
 datarow">
          <td><?php echo $_smarty_tpl->tpl_vars['v']->value->item_name;?>

          </td>
           <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->item_price;?>

           <?php $_smarty_tpl->_assignInScope('pprice', $_smarty_tpl->tpl_vars['v']->value->item_price);?>
           </td>
          <td>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['proteinsanddata']->value, 'pv', false, 'pk');
$_smarty_tpl->tpl_vars['pv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['pk']->value => $_smarty_tpl->tpl_vars['pv']->value) {
$_smarty_tpl->tpl_vars['pv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['pv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('pqty', $_smarty_tpl->tpl_vars['pv']->value['qty']);?>
          <?php echo $_smarty_tpl->tpl_vars['pqty']->value;?>

          <?php $_smarty_tpl->_assignInScope('sum_qty_protein', $_smarty_tpl->tpl_vars['sum_qty_protein']->value+$_smarty_tpl->tpl_vars['pqty']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         </td>
          <td>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['proteinsanddata']->value, 'pv', false, 'pk');
$_smarty_tpl->tpl_vars['pv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['pk']->value => $_smarty_tpl->tpl_vars['pv']->value) {
$_smarty_tpl->tpl_vars['pv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['pv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('protein_subtotal', $_smarty_tpl->tpl_vars['pqty']->value*$_smarty_tpl->tpl_vars['pprice']->value);?>
          $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['protein_subtotal']->value);?>

          <?php $_smarty_tpl->_assignInScope('sum_all_protein', $_smarty_tpl->tpl_vars['sum_all_protein']->value+$_smarty_tpl->tpl_vars['protein_subtotal']->value);?>
           <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </td>
		   <td></td>
        </tr>
		<?php }?>
		 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				
		<tr class="items">
          <td colspan="5">CHEESES</td>
        </tr>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwichitems']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['v']->value->category_id == 3) {?>
		 <tr class="<?php echo smarty_function_cycle(array('values'=>"odd,even"),$_smarty_tpl);?>
 datarow">
          <td><?php echo $_smarty_tpl->tpl_vars['v']->value->item_name;?>
</td>
           <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->item_price;?>

           <?php $_smarty_tpl->_assignInScope('cprice', $_smarty_tpl->tpl_vars['v']->value->item_price);?>
           </td>
          <td><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cheesesanddata']->value, 'cv', false, 'ck');
$_smarty_tpl->tpl_vars['cv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ck']->value => $_smarty_tpl->tpl_vars['cv']->value) {
$_smarty_tpl->tpl_vars['cv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['cv']->value['itemname']) {?>
           <?php $_smarty_tpl->_assignInScope('cqty', $_smarty_tpl->tpl_vars['cv']->value['qty']);?>
          <?php echo $_smarty_tpl->tpl_vars['cqty']->value;?>

           <?php $_smarty_tpl->_assignInScope('sum_qty_cheese', $_smarty_tpl->tpl_vars['sum_qty_cheese']->value+$_smarty_tpl->tpl_vars['cqty']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></td>
          <td>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cheesesanddata']->value, 'cv', false, 'ck');
$_smarty_tpl->tpl_vars['cv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ck']->value => $_smarty_tpl->tpl_vars['cv']->value) {
$_smarty_tpl->tpl_vars['cv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['cv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('cheese_subtotal', $_smarty_tpl->tpl_vars['cqty']->value*$_smarty_tpl->tpl_vars['cprice']->value);?>
          $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['cheese_subtotal']->value);?>

          <?php $_smarty_tpl->_assignInScope('sum_all_cheese', $_smarty_tpl->tpl_vars['sum_all_cheese']->value+$_smarty_tpl->tpl_vars['cheese_subtotal']->value);?>
           <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </td>
		   <td></td>
        </tr>
		<?php }?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				
		<tr class="items">
          <td colspan="5">TOPPINGS</td>
        </tr>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwichitems']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['v']->value->category_id == 4) {?>
		 <tr class="<?php echo smarty_function_cycle(array('values'=>"odd,even"),$_smarty_tpl);?>
 datarow">
          <td><?php echo $_smarty_tpl->tpl_vars['v']->value->item_name;?>
</td>
           <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->item_price;?>

           <?php $_smarty_tpl->_assignInScope('tprice', $_smarty_tpl->tpl_vars['v']->value->item_price);?>
           </td>
          <td><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['toppingssanddata']->value, 'tv', false, 'tk');
$_smarty_tpl->tpl_vars['tv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['tk']->value => $_smarty_tpl->tpl_vars['tv']->value) {
$_smarty_tpl->tpl_vars['tv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['tv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('tqty', $_smarty_tpl->tpl_vars['tv']->value['qty']);?>
          <?php echo $_smarty_tpl->tpl_vars['tqty']->value;?>

          <?php $_smarty_tpl->_assignInScope('sum_qty_toppings', $_smarty_tpl->tpl_vars['sum_qty_toppings']->value+$_smarty_tpl->tpl_vars['tqty']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></td>
          <td>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['toppingssanddata']->value, 'tv', false, 'tk');
$_smarty_tpl->tpl_vars['tv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['tk']->value => $_smarty_tpl->tpl_vars['tv']->value) {
$_smarty_tpl->tpl_vars['tv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['tv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('toppings_subtotal', $_smarty_tpl->tpl_vars['tqty']->value*$_smarty_tpl->tpl_vars['tprice']->value);?>
          $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['toppings_subtotal']->value);?>

          <?php $_smarty_tpl->_assignInScope('sum_all_toppings', $_smarty_tpl->tpl_vars['sum_all_toppings']->value+$_smarty_tpl->tpl_vars['toppings_subtotal']->value);?>
           <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </td>
		   <td>
		   
		   <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['toppingssanddata']->value, 'tv', false, 'tk');
$_smarty_tpl->tpl_vars['tv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['tk']->value => $_smarty_tpl->tpl_vars['tv']->value) {
$_smarty_tpl->tpl_vars['tv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['tv']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('tqty', $_smarty_tpl->tpl_vars['tv']->value['qty']);?>
          <?php echo smarty_function_math(array('equation'=>"(( x / y ) * z )",'x'=>$_smarty_tpl->tpl_vars['tqty']->value,'y'=>$_smarty_tpl->tpl_vars['salesCount']->value,'z'=>100,'format'=>"%.2f"),$_smarty_tpl);?>
%
          <?php $_smarty_tpl->_assignInScope('sum_qty_toppings', $_smarty_tpl->tpl_vars['sum_qty_toppings']->value+$_smarty_tpl->tpl_vars['tqty']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		   
		   </td>
        </tr>
		<?php }?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		
		<tr class="items">
          <td colspan="5">CONDIMENTS</td>
        </tr>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwichitems']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['v']->value->category_id == 5) {?>
		 <tr class="<?php echo smarty_function_cycle(array('values'=>"odd,even"),$_smarty_tpl);?>
 datarow">
          <td><?php echo $_smarty_tpl->tpl_vars['v']->value->item_name;?>
</td>
           <td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->item_price;?>

           <?php $_smarty_tpl->_assignInScope('coprice', $_smarty_tpl->tpl_vars['v']->value->item_price);?>
           </td>
          <td><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['condisanddata']->value, 'cov', false, 'cok');
$_smarty_tpl->tpl_vars['cov']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cok']->value => $_smarty_tpl->tpl_vars['cov']->value) {
$_smarty_tpl->tpl_vars['cov']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['cov']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('coqty', $_smarty_tpl->tpl_vars['cov']->value['qty']);?>
          <?php echo $_smarty_tpl->tpl_vars['coqty']->value;?>

           <?php $_smarty_tpl->_assignInScope('sum_qty_condi', $_smarty_tpl->tpl_vars['sum_qty_condi']->value+$_smarty_tpl->tpl_vars['coqty']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></td>
          <td>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['condisanddata']->value, 'cov', false, 'cok');
$_smarty_tpl->tpl_vars['cov']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cok']->value => $_smarty_tpl->tpl_vars['cov']->value) {
$_smarty_tpl->tpl_vars['cov']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['cov']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('condi_subtotal', $_smarty_tpl->tpl_vars['coqty']->value*$_smarty_tpl->tpl_vars['coprice']->value);?>
          $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['condi_subtotal']->value);?>

           <?php $_smarty_tpl->_assignInScope('sum_all_condi', $_smarty_tpl->tpl_vars['sum_all_condi']->value+$_smarty_tpl->tpl_vars['condi_subtotal']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></td>
		   <td><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['condisanddata']->value, 'cov', false, 'cok');
$_smarty_tpl->tpl_vars['cov']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cok']->value => $_smarty_tpl->tpl_vars['cov']->value) {
$_smarty_tpl->tpl_vars['cov']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->item_name == $_smarty_tpl->tpl_vars['cov']->value['itemname']) {?>
          <?php $_smarty_tpl->_assignInScope('coqty', $_smarty_tpl->tpl_vars['cov']->value['qty']);?>
           <?php echo smarty_function_math(array('equation'=>"(( x / y ) * z )",'x'=>$_smarty_tpl->tpl_vars['coqty']->value,'y'=>$_smarty_tpl->tpl_vars['salesCount']->value,'z'=>100,'format'=>"%.2f"),$_smarty_tpl);?>
%
           <?php $_smarty_tpl->_assignInScope('sum_qty_condi', $_smarty_tpl->tpl_vars['sum_qty_condi']->value+$_smarty_tpl->tpl_vars['coqty']->value);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></td>
        </tr>
		<?php }?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		<?php $_smarty_tpl->_assignInScope('tottal_subtottal', $_smarty_tpl->tpl_vars['sum_all_condi']->value+$_smarty_tpl->tpl_vars['sum_all_breads']->value+$_smarty_tpl->tpl_vars['sum_all_protein']->value+$_smarty_tpl->tpl_vars['sum_all_cheese']->value+$_smarty_tpl->tpl_vars['sum_all_toppings']->value);?>
		<?php $_smarty_tpl->_assignInScope('tottal_qty', $_smarty_tpl->tpl_vars['sum_qty_bread']->value+$_smarty_tpl->tpl_vars['sum_qty_protein']->value+$_smarty_tpl->tpl_vars['sum_qty_cheese']->value+$_smarty_tpl->tpl_vars['sum_qty_toppings']->value+$_smarty_tpl->tpl_vars['sum_qty_condi']->value);?>
    	
      </tbody></table>
	  
	   <table width="100%" cellspacing="0" cellpadding="0" border="0" class="products">
        <tbody><tr>
          <th width="65%">STANDARD ITEMS</th>
          <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standardCatagory']->value, 'sci', false, 'sc');
$_smarty_tpl->tpl_vars['sci']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['sc']->value => $_smarty_tpl->tpl_vars['sci']->value) {
$_smarty_tpl->tpl_vars['sci']->do_else = false;
?>
        <tr class="subheader <?php echo $_smarty_tpl->tpl_vars['sc']->value;?>
">
          <td><?php echo $_smarty_tpl->tpl_vars['sci']->value->category_identifier;?>
</td>
          <td></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-item-<?php echo $_smarty_tpl->tpl_vars['sc']->value;?>
"></span></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-<?php echo $_smarty_tpl->tpl_vars['sc']->value;?>
"></span></td>
          <td></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('sum_all_salades', 0);?>
        <?php $_smarty_tpl->_assignInScope('sum_all_salades_count', 0);?>
        
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standareditems']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['v']->value->standard_category_id == $_smarty_tpl->tpl_vars['sci']->value->id) {?>
		<tr class="<?php echo smarty_function_cycle(array('values'=>"odd,even"),$_smarty_tpl);?>
 datarow st-cat-<?php echo $_smarty_tpl->tpl_vars['sc']->value;?>
">
          <td><?php echo $_smarty_tpl->tpl_vars['v']->value->product_name;?>
</td>
          <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->product_price);?>

          <?php $_smarty_tpl->_assignInScope('price', $_smarty_tpl->tpl_vars['v']->value->product_price);?>
          </td>
          <td><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standareditemsqty']->value, 'sv', false, 'sk');
$_smarty_tpl->tpl_vars['sv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['sk']->value => $_smarty_tpl->tpl_vars['sv']->value) {
$_smarty_tpl->tpl_vars['sv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->id == $_smarty_tpl->tpl_vars['sv']->value->item_id) {?>
          <?php echo $_smarty_tpl->tpl_vars['sv']->value->qty;?>

          <?php $_smarty_tpl->_assignInScope('qty', $_smarty_tpl->tpl_vars['sv']->value->qty);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></td>
          <td>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standareditemsqty']->value, 'sv', false, 'sk');
$_smarty_tpl->tpl_vars['sv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['sk']->value => $_smarty_tpl->tpl_vars['sv']->value) {
$_smarty_tpl->tpl_vars['sv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->id == $_smarty_tpl->tpl_vars['sv']->value->item_id) {?>
          <?php $_smarty_tpl->_assignInScope('salades_subtotal', $_smarty_tpl->tpl_vars['qty']->value*$_smarty_tpl->tpl_vars['price']->value);?>
          $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['salades_subtotal']->value);?>

           <?php $_smarty_tpl->_assignInScope('sum_all_salades', $_smarty_tpl->tpl_vars['sum_all_salades']->value+$_smarty_tpl->tpl_vars['salades_subtotal']->value);?>
           <?php $_smarty_tpl->_assignInScope('sum_all_salades_count', $_smarty_tpl->tpl_vars['sum_all_salades_count']->value+$_smarty_tpl->tpl_vars['qty']->value);?>
           <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
           </td>
		   <td> 
		   
		   <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standareditemsqty']->value, 'sv', false, 'sk');
$_smarty_tpl->tpl_vars['sv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['sk']->value => $_smarty_tpl->tpl_vars['sv']->value) {
$_smarty_tpl->tpl_vars['sv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->id == $_smarty_tpl->tpl_vars['sv']->value->item_id) {?>
          <?php $_smarty_tpl->_assignInScope('stpq', $_smarty_tpl->tpl_vars['sv']->value->qty);?>
          <?php echo smarty_function_math(array('equation'=>"(( x / y ) * z )",'x'=>$_smarty_tpl->tpl_vars['stpq']->value,'y'=>$_smarty_tpl->tpl_vars['salesCount']->value,'z'=>100,'format'=>"%.2f"),$_smarty_tpl);?>
%
          <?php $_smarty_tpl->_assignInScope('qty', $_smarty_tpl->tpl_vars['sv']->value->qty);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		   
		   </td>
        </tr>
		<?php }?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		
		<?php $_smarty_tpl->_assignInScope('tottal_subtottal', $_smarty_tpl->tpl_vars['tottal_subtottal']->value+$_smarty_tpl->tpl_vars['sum_all_salades']->value);?>
		<?php $_smarty_tpl->_assignInScope('tottal_qty', $_smarty_tpl->tpl_vars['tottal_qty']->value+$_smarty_tpl->tpl_vars['sum_all_salades_count']->value);?>
		
		<input type="hidden" id="st-cat-<?php echo $_smarty_tpl->tpl_vars['sc']->value;?>
"  value="<?php echo sprintf('%.2f',$_smarty_tpl->tpl_vars['sum_all_salades']->value);?>
"/>
		<input type="hidden" id="st-cat-item-<?php echo $_smarty_tpl->tpl_vars['sc']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['sum_all_salades_count']->value;?>
"/>
        	
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
		
	   </tbody>
	   </table>
	   
	   
	   
	   <table width="100%" cellspacing="0" cellpadding="0" border="0" class="products">
        <tbody><tr>
          <th width="65%">CATERING ITEMS</th>
          <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cateringCatagory']->value, 'cci', false, 'cc');
$_smarty_tpl->tpl_vars['cci']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cc']->value => $_smarty_tpl->tpl_vars['cci']->value) {
$_smarty_tpl->tpl_vars['cci']->do_else = false;
?>
        <tr class="subheader <?php echo $_smarty_tpl->tpl_vars['cc']->value;?>
">
          <td><?php echo $_smarty_tpl->tpl_vars['cci']->value->category_identifier;?>
</td>
          <td></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-item-<?php echo $_smarty_tpl->tpl_vars['cc']->value;?>
"></span></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-<?php echo $_smarty_tpl->tpl_vars['cc']->value;?>
"></span></td>
          <td></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('sum_all_salades', 0);?>
        <?php $_smarty_tpl->_assignInScope('sum_all_salades_count', 0);?>
        
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standareditems']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
		<?php if ($_smarty_tpl->tpl_vars['v']->value->standard_category_id == $_smarty_tpl->tpl_vars['cci']->value->id) {?>
		<tr class="<?php echo smarty_function_cycle(array('values'=>"odd,even"),$_smarty_tpl);?>
 datarow st-cat-<?php echo $_smarty_tpl->tpl_vars['cc']->value;?>
">
          <td><?php echo $_smarty_tpl->tpl_vars['v']->value->product_name;?>
</td>
          <td>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['v']->value->product_price);?>

          <?php $_smarty_tpl->_assignInScope('price', $_smarty_tpl->tpl_vars['v']->value->product_price);?>
          </td>
          <td><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standareditemsqty']->value, 'sv', false, 'sk');
$_smarty_tpl->tpl_vars['sv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['sk']->value => $_smarty_tpl->tpl_vars['sv']->value) {
$_smarty_tpl->tpl_vars['sv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->id == $_smarty_tpl->tpl_vars['sv']->value->item_id) {?>
          <?php echo $_smarty_tpl->tpl_vars['sv']->value->qty;?>

          <?php $_smarty_tpl->_assignInScope('qty', $_smarty_tpl->tpl_vars['sv']->value->qty);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></td>
          <td>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standareditemsqty']->value, 'sv', false, 'sk');
$_smarty_tpl->tpl_vars['sv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['sk']->value => $_smarty_tpl->tpl_vars['sv']->value) {
$_smarty_tpl->tpl_vars['sv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->id == $_smarty_tpl->tpl_vars['sv']->value->item_id) {?>
          <?php $_smarty_tpl->_assignInScope('salades_subtotal', $_smarty_tpl->tpl_vars['qty']->value*$_smarty_tpl->tpl_vars['price']->value);?>
          $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['salades_subtotal']->value);?>

           <?php $_smarty_tpl->_assignInScope('sum_all_salades', $_smarty_tpl->tpl_vars['sum_all_salades']->value+$_smarty_tpl->tpl_vars['salades_subtotal']->value);?>
           <?php $_smarty_tpl->_assignInScope('sum_all_salades_count', $_smarty_tpl->tpl_vars['sum_all_salades_count']->value+$_smarty_tpl->tpl_vars['qty']->value);?>
           <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
           </td>
		   <td> 
		   
		   <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['standareditemsqty']->value, 'sv', false, 'sk');
$_smarty_tpl->tpl_vars['sv']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['sk']->value => $_smarty_tpl->tpl_vars['sv']->value) {
$_smarty_tpl->tpl_vars['sv']->do_else = false;
?>
          <?php if ($_smarty_tpl->tpl_vars['v']->value->id == $_smarty_tpl->tpl_vars['sv']->value->item_id) {?>
          <?php $_smarty_tpl->_assignInScope('stpq', $_smarty_tpl->tpl_vars['sv']->value->qty);?>
          <?php echo smarty_function_math(array('equation'=>"(( x / y ) * z )",'x'=>$_smarty_tpl->tpl_vars['stpq']->value,'y'=>$_smarty_tpl->tpl_vars['salesCount']->value,'z'=>100,'format'=>"%.2f"),$_smarty_tpl);?>
%
          <?php $_smarty_tpl->_assignInScope('qty', $_smarty_tpl->tpl_vars['sv']->value->qty);?>
          <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		   
		   </td>
        </tr>
		<?php }?>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		
		<?php $_smarty_tpl->_assignInScope('tottal_subtottal', $_smarty_tpl->tpl_vars['tottal_subtottal']->value+$_smarty_tpl->tpl_vars['sum_all_salades']->value);?>
		<?php $_smarty_tpl->_assignInScope('tottal_qty', $_smarty_tpl->tpl_vars['tottal_qty']->value+$_smarty_tpl->tpl_vars['sum_all_salades_count']->value);?>
		
		<input type="hidden" id="st-cat-<?php echo $_smarty_tpl->tpl_vars['sc']->value;?>
"  value="<?php echo sprintf('%.2f',$_smarty_tpl->tpl_vars['sum_all_salades']->value);?>
"/>
		<input type="hidden" id="st-cat-item-<?php echo $_smarty_tpl->tpl_vars['sc']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['sum_all_salades_count']->value;?>
"/>
        	
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
		
	   </tbody>
	   </table>
	   
	   
	   
	   </div>
		 <span class="tottal_subtottal"><?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['tottal_subtottal']->value);?>
</span>
    	 <span class="tottal_qty"><?php echo $_smarty_tpl->tpl_vars['tottal_qty']->value;?>
</span>
    </div>
  </div>
<?php }
}
