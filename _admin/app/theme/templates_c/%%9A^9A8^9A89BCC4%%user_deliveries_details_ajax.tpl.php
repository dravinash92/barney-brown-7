<?php /* Smarty version 2.6.25, created on 2020-02-24 05:11:11
         compiled from user_deliveries_details_ajax.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'user_deliveries_details_ajax.tpl', 1, false),array('modifier', 'string_format', 'user_deliveries_details_ajax.tpl', 61, false),)), $this); ?>
<?php if (count($this->_tpl_vars['del_details']) > 0): ?>
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <th>ORDER #</th>
          <th>PLACEMENT</th>
          <th>FULFILLMENT</th>
          <th>CUSTOMER NAME</th>
          <th>SUBTOT</th>
          <th>TAX</th>
          <th>FEE</th>
          <th>TIP</th>
          <th>VALUE</th>
          <th>DSCNT</th>
          <th>REFUND</th>
          <th>PAID</th>
        </tr>
        <?php  
      $tax_total = $sub_total = $delivery_fee_total  = $tip_total = $total = $dscnt_total  = $paid = 0;
     ?> 
        <?php $_from = $this->_tpl_vars['del_details']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['v']):
?>      
        <?php $this->assign('tax', $this->_tpl_vars['v']['tax']); ?>
        <?php $this->assign('sub', $this->_tpl_vars['v']['SubTot']); ?>
        <?php $this->assign('delivery_fee', $this->_tpl_vars['v']['Fee']); ?>
        <?php $this->assign('tip', $this->_tpl_vars['v']['tip']); ?>
        <?php $this->assign('calc_total', $this->_tpl_vars['v']['Value']); ?>
        <?php $this->assign('off_amount', $this->_tpl_vars['v']['Dscnt']); ?>
        <?php $this->assign('total', $this->_tpl_vars['v']['PaidAmt']); ?>
        <?php 
          $tax_total += $this->get_template_vars('tax');
          $sub_total += $this->get_template_vars('sub');
          $delivery_fee_total += $this->get_template_vars('delivery_fee');
          $tip_total += $this->get_template_vars('tip');
          $calc_total += $this->get_template_vars('calc_total');
          $dscnt_total += $this->get_template_vars('off_amount');
          $paid += $this->get_template_vars('total');
         ?>      
      <?php endforeach; endif; unset($_from); ?>
        <tr class="subheader">
          <td colspan="3">TOTAL</td>
          <td></td>
         <td>$<?php echo sprintf('%0.2f',$sub_total) ?></td>
        <td>$<?php echo sprintf('%0.2f',$tax_total) ?></td>
        <td>$<?php echo sprintf('%0.2f',$delivery_fee_total) ?></td>
        <td>$<?php echo sprintf('%0.2f',$tip_total) ?></td>
        <td>$<?php echo sprintf('%0.2f',$calc_total) ?></td>
        <td>-$<?php echo sprintf('%0.2f',$dscnt_total) ?></td>
        <td>-$0.00</td>
        <td>$<?php echo sprintf('%0.2f',$sub_total+$delivery_fee_total+$tax_total+$tip_total-$dscnt_total) ?></td>
        </tr>
       <?php $_from = $this->_tpl_vars['del_details']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['v']):
?>
        <?php if (!(!(1 & $this->_tpl_vars['k']))): ?> 
        <tr class="">
        <?php else: ?>  
        <tr class="even">
        <?php endif; ?> 
          <td><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
reports/showOrderDetails/<?php echo $this->_tpl_vars['v']['order_id']; ?>
"><?php echo $this->_tpl_vars['v']['OrderNumber']; ?>
</a></td>
          <td><?php echo $this->_tpl_vars['v']['Placement']; ?>
</td>
          <td><?php echo $this->_tpl_vars['v']['Fulfillment']; ?>
</td>
          <td><?php echo $this->_tpl_vars['v']['Name']; ?>
</td>
          <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']['SubTot'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
          <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']['tax'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
          <td>$<?php echo $this->_tpl_vars['v']['Fee']; ?>
</td>
          <td>$<?php echo $this->_tpl_vars['v']['tip']; ?>
</td>
          <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']['Value'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
          <td>-$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']['Dscnt'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
          <td>-$0.00</td>
          <td>$<?php echo ((is_array($_tmp=$this->_tpl_vars['v']['PaidAmt'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
        </tr>       
      <?php endforeach; endif; unset($_from); ?>  
     
    
</tbody>
    </table> 
    <?php else: ?>
    <table class="no-record-wrap" width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr class="no-record">
          <th> No Result Found!</th>
        </tr>
</tbody>
    </table> 
  <?php endif; ?> 