<?php /* Smarty version 2.6.25, created on 2020-09-25 02:45:29
         compiled from edit_standard_product.tpl */ ?>
<div class="container">
      <div class="standard-menu">
           <div class="standard-menu"><div class="condiment">
        <h1>STANDARD MENU ITEMS</h1>
        <form name="newbread" method="post" action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/updateStandardCategoryProducts" enctype="multipart/form-data"> 
        <input type="hidden" name="old_product_image" value="<?php echo $this->_tpl_vars['data']['product_image']; ?>
" />
       
        <ul>
        <li>
          <label>Product Name</label>
          <input  style="width:250px;" type="text" name="product_name" value="<?php echo $this->_tpl_vars['data']['product_name']; ?>
">
        </li>
      
        <li>
          <label>Description </label>
          <textarea style="width:250px;" name="description"><?php echo $this->_tpl_vars['data']['description']; ?>
</textarea>
        </li>

        <li>
          <label>Product Image</label>
          <input style="width:250px;" type="file" name="product_image">           
        </li>
         
         <?php if ($this->_tpl_vars['data']['product_image']): ?>
          <li style="float:left">
            <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
upload/products/<?php echo $this->_tpl_vars['data']['product_image']; ?>
" width="100" height="100">
          </li>
          <?php endif; ?>
       
         
        <li>
          <label>Price</label>
          <input type="text" value="<?php echo $this->_tpl_vars['data']['product_price']; ?>
" name="product_price" class="price-menu">
        </li>  
        
        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="taxable" name="taxable" class="price-menu" <?php if ($this->_tpl_vars['data']['taxable'] == 1): ?> checked <?php endif; ?> >
          <label for="taxable" style="width:45px;">Taxable</label>
        </li>

        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="allow_spcl_instruction" name="allow_spcl_instruction" class="price-menu" <?php if ($this->_tpl_vars['data']['allow_spcl_instruction'] == 1): ?> checked <?php endif; ?> >
          <label for="allow_spcl_instruction" style="width:150px;">Allow Special Instructions</label>
        </li>

        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="add_modifier" name="add_modifier" class="price-menu" <?php if ($this->_tpl_vars['data']['add_modifier'] == 1): ?> checked <?php endif; ?> >
          <label for="add_modifier" style="width:100px;">Add Modifier</label>
        </li>  
      
      </ul>

     <ul class="item-description">
        <div class="description-wrap-item">
          <div class="description-left">

            <div class="description-main">
              <label>Description</label>
              <input type="text" name="modifier_desc" class="descriptor" placeholder="Add a Protein" value="<?php echo $this->_tpl_vars['data']['modifier_desc']; ?>
">
            </div>
             <div class="radio-box">
        <div class="description-radio_options">
<p class="radio-label-itm">Optional or Required</p>
          <div class="input">
            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_isoptional" id="optional" <?php if ($this->_tpl_vars['data']['modifier_isoptional'] == yes): ?> checked <?php endif; ?> value="yes">
             <label for="optional">Optional</label>
            </div>

            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_isoptional" id="required" <?php if ($this->_tpl_vars['data']['modifier_isoptional'] == no): ?> checked <?php endif; ?> value="no">
             <label for="required">Required</label>
            </div>
          </div>
      </div>

      <div class="description-radio_options">
        <p class="radio-label-itm">Amount to Choose</p>
          <div class="input">
            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_is_single" id="limited" <?php if ($this->_tpl_vars['data']['modifier_is_single'] == yes): ?> checked <?php endif; ?> value="yes">
             <label for="limited">Choose One</label>
            </div>

            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_is_single" id="unlimited" <?php if ($this->_tpl_vars['data']['modifier_is_single'] == no): ?> checked <?php endif; ?> value="no">
             <label for="unlimited">Choose Unlimited</label>
            </div>
          </div>
      </div>
      </div>
          </div> 

          <div class="description-right">
         <div class="description-right-wrap">
            <div class="description-main add-more-opt">
              <label>Options</label>
              <?php if (isset ( $this->_tpl_vars['modifier_options'] ) && $this->_tpl_vars['modifier_options'] != ''): ?>
                <?php $_from = $this->_tpl_vars['modifier_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['optprc']):
?>
                <input type="text" name="option[]" class="option_price" id="add_remove" placeholder="" value="<?php echo $this->_tpl_vars['optprc']['option']; ?>
">
                <?php endforeach; endif; unset($_from); ?>
              <?php else: ?>
                <input type="text" name="option[]" class="option_price" id="add_remove" placeholder="" value="">
              <?php endif; ?>
            </div>
            <div class="description-main description-small-feild add-more-prc">
              <label>Price</label>
              <?php if (isset ( $this->_tpl_vars['modifier_options'] ) && $this->_tpl_vars['modifier_options'] != ''): ?>
                <?php $_from = $this->_tpl_vars['modifier_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['optprc']):
?>
                <input type="text" name="price[]" class="option_price" id="add_remove" placeholder="" value="<?php echo $this->_tpl_vars['optprc']['price']; ?>
">
                <?php endforeach; endif; unset($_from); ?>
              <?php else: ?>
                <input type="text" name="price[]" class="option_price" id="add_remove" placeholder="" value="">
              <?php endif; ?>
            </div>
            <div class="remove-btn add-more-rem">
              <?php if (isset ( $this->_tpl_vars['modifier_options'] ) && $this->_tpl_vars['modifier_options'] != ''): ?>
                <?php $_from = $this->_tpl_vars['modifier_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['optprc']):
?>
                <label></label>
                <p class="rem_add_more">remove</p>  
                <?php endforeach; endif; unset($_from); ?> 
              <?php else: ?>
                <label></label>
                <p class="rem_add_more">remove</p>  
              <?php endif; ?>          
            </div>
          </div>
            <div class="desc-additm">
            <p class="add_more">add item</p>
          </div>
          </div>

        </div>
        </ul>
      <input type="hidden" name="hidden_id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
" />
       <div class="button">
         <input type="submit" class="save save_standard_product" style="cursor: pointer;" value="SAVE">
        <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu" class="cancel">CANCEL</a>
    </div>
   </div></div> 
 </form>
    </div>
    
 