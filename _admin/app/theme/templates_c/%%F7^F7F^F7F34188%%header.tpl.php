<?php /* Smarty version 2.6.25, created on 2020-09-21 04:31:30
         compiled from header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'header.tpl', 136, false),)), $this); ?>
<!DOCTYPE html>
<html lang="en">
<head> 
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo $this->_tpl_vars['Title']; ?>
</title>
<link href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/stylesheets/screen.css" rel="stylesheet"/>
<link href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/stylesheets/print.css" rel="stylesheet"/>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/stylesheets/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/js/common.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
ckeditor/ckeditor.js"></script>




<script>
	var SITE_URL = "<?php echo $this->_tpl_vars['SITE_URL']; ?>
";
	var CMS_URL = "<?php echo $this->_tpl_vars['CMS_URL']; ?>
";
	var API_URL  = "<?php echo $this->_tpl_vars['API_URL']; ?>
";
	var SESSION_ID = "<?php echo $this->_tpl_vars['TO_USER_ID']; ?>
";
	var DATA_ID = "<?php echo $this->_tpl_vars['DATA_ID']; ?>
";
	var BASE_FARE = "<?php echo $this->_tpl_vars['BASE_FARE']; ?>
";
	 
</script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/js/sandwichcreator.js"></script>

</head>
<body>


<div class="popupOrderDetailsBg">&nbsp;</div>

<div class="popupOrderDetails">

<div class="clear"></div>


<div class="popup_order_detail" >

<p  class="closeOrd">X</p>
<div id="popOut" >
  
  
  <p class="loader">
  <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/optionload.gif" />
  </p>
  </div>
  
 <!-- end -->
  </div>  

<div class="clear"></div>

</div>

<div id="outer-wrapper">

<div class="container">
     <!--Header Starting-->
     <div class="header">
        <div class="logo">
           <a href="#"><img width="225" height="28" alt="<?php echo $this->_tpl_vars['title_text']; ?>
" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/logo.png"></a>
        </div>  
        
        <div class="user-login">
         
             <?php  if( isset($_SESSION['admin_uid']) ) {   ?>
           <p><?php  echo $_SESSION['uname']  ?></p>
           <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
login/signout/">logout</a>
          <?php  }  ?>
        </div>
        
     </div>
  </div>


<div class="navigation">
    <div class="container">
   <?php  if( isset($_SESSION['admin_uid']) ) {   ?>
      <nav>
        <ul>
		<?php if ($this->_tpl_vars['User_Type'] != store): ?>	
	    <li class="first-child"><a  <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == neworder): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
neworder/">NEW ORDER</a></li>
	  
	    
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == deliveries): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
deliveries/">DELIVERIES</a></li>
		
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == pickup): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
pickups/">PICK-UPS</a></li>
		<?php else: ?>
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == storedeliveries): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
storedeliveries/">DELIVERIES</a></li>
		
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == storepickups): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
storepickups/">PICK-UPS</a></li>
		
		<?php endif; ?>
		
		
		  
		<?php if ($this->_tpl_vars['User_Type'] != store): ?>	
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == customers): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
customer/">CUSTOMERS</a></li>
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == menu): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/">MENU ITEMS</a></li>
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == sandwich): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/">SANDWICH GALLERY</a></li>
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == discounts): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
discounts/">DISCOUNTS </a></li>
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == web): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
web/">WEB PAGES</a></li>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['User_Type'] != store): ?>	
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == reports): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
reports/">REPORTS</a></li>
		<?php else: ?>
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == reports): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
storereports">REPORTS</a></li>
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == storemanage): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
storemanage/">MANAGE STORE</a></li>	
		<?php endif; ?>
		
		<?php if ($this->_tpl_vars['User_Type'] != store): ?>	
		<li><a <?php if ($this->_tpl_vars['ACTIVE_MAIN_MENU_TEXT'] == admin): ?> class="active" <?php endif; ?> href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
accounts/">ADMIN</a></li>
		<?php endif; ?>
   
        </ul>
      </nav>
<?php  }  ?>
    </div>
  </div>
  
<?php if ($this->_tpl_vars['SEC_NAV_OPS']['show'] == true): ?> 

<div class="secondary-navigation">
    <div class="container">
      <nav>
        <ul>
<?php unset($this->_sections['nav']);
$this->_sections['nav']['name'] = 'nav';
$this->_sections['nav']['start'] = (int)0;
$this->_sections['nav']['loop'] = is_array($_loop=count($this->_tpl_vars['SEC_NAV_OPS']['text'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['nav']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['nav']['show'] = true;
$this->_sections['nav']['max'] = $this->_sections['nav']['loop'];
if ($this->_sections['nav']['start'] < 0)
    $this->_sections['nav']['start'] = max($this->_sections['nav']['step'] > 0 ? 0 : -1, $this->_sections['nav']['loop'] + $this->_sections['nav']['start']);
else
    $this->_sections['nav']['start'] = min($this->_sections['nav']['start'], $this->_sections['nav']['step'] > 0 ? $this->_sections['nav']['loop'] : $this->_sections['nav']['loop']-1);
if ($this->_sections['nav']['show']) {
    $this->_sections['nav']['total'] = min(ceil(($this->_sections['nav']['step'] > 0 ? $this->_sections['nav']['loop'] - $this->_sections['nav']['start'] : $this->_sections['nav']['start']+1)/abs($this->_sections['nav']['step'])), $this->_sections['nav']['max']);
    if ($this->_sections['nav']['total'] == 0)
        $this->_sections['nav']['show'] = false;
} else
    $this->_sections['nav']['total'] = 0;
if ($this->_sections['nav']['show']):

            for ($this->_sections['nav']['index'] = $this->_sections['nav']['start'], $this->_sections['nav']['iteration'] = 1;
                 $this->_sections['nav']['iteration'] <= $this->_sections['nav']['total'];
                 $this->_sections['nav']['index'] += $this->_sections['nav']['step'], $this->_sections['nav']['iteration']++):
$this->_sections['nav']['rownum'] = $this->_sections['nav']['iteration'];
$this->_sections['nav']['index_prev'] = $this->_sections['nav']['index'] - $this->_sections['nav']['step'];
$this->_sections['nav']['index_next'] = $this->_sections['nav']['index'] + $this->_sections['nav']['step'];
$this->_sections['nav']['first']      = ($this->_sections['nav']['iteration'] == 1);
$this->_sections['nav']['last']       = ($this->_sections['nav']['iteration'] == $this->_sections['nav']['total']);
?>

<li class="first-child"><a <?php if ($this->_tpl_vars['SEC_NAV_OPS']['active_text'] == $this->_tpl_vars['SEC_NAV_OPS']['text'][$this->_sections['nav']['index']]): ?> class="active" <?php endif; ?>  href="<?php echo $this->_tpl_vars['SEC_NAV_OPS']['url'][$this->_sections['nav']['index']]; ?>
"> <?php echo $this->_tpl_vars['SEC_NAV_OPS']['text'][$this->_sections['nav']['index']]; ?>
 </a></li>
<?php endfor; endif; ?>
         
       </ul>
      </nav>
    </div>
  </div> 
  
<?php endif; ?>


<?php  if( isset($_SESSION['success']) ) {   ?>
<div style="width: 930px;height: auto;margin: 0 auto;clear:both">
<br><p style="color:red;margin-top:10px;"><?php echo $this->_tpl_vars['success']; ?>
</div></div>
<?php  unset($_SESSION['success']); }  ?>
