<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:45:55
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\menu_items.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c86037993f5_92077731',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '19152a96dafeefc9921b68fe0080fe94db3857db' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\menu_items.tpl',
      1 => 1616676352,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c86037993f5_92077731 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
      <div class="store-accounts">
        <h1>MENU ITEMS</h1>
             
        <h2>CUSTOM</h2>       
        
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody><tr>
           <th width="89%">SANDWICHES</th>
           <th width="11%"># OPTIONS</th>
         </tr>
         
         <tr>
           <td>Breads</td>
           <td class="align-right"><?php echo $_smarty_tpl->tpl_vars['counts']->value['BREAD'];?>
</td>
         </tr>
         
         <tr class="even">
           <td>Meats</td>
           <td class="align-right"><?php echo $_smarty_tpl->tpl_vars['counts']->value['PROTEIN'];?>
</td>
         </tr>
		 
		  <tr>
           <td>Cheeses</td>
           <td class="align-right"><?php echo $_smarty_tpl->tpl_vars['counts']->value['CHEESE'];?>
</td>
         </tr>
		 
		 <tr class="even">
           <td>Toppings</td>
           <td class="align-right"><?php echo $_smarty_tpl->tpl_vars['counts']->value['TOPPINGS'];?>
</td>
         </tr>
		 
		  <tr>
           <td>Condiments</td>
           <td class="align-right"><?php echo $_smarty_tpl->tpl_vars['counts']->value['CONDIMENTS'];?>
</td>
         </tr>
         
       </tbody></table>
       <a class="edit" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/custom">EDIT</a>
       <a class="edit" style="margin-left:10px;" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/edit_options">EDIT OPTIONS</a>
        <h2>STANDARD</h2>       
        <a class="category" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/newStandardCategory">NEW CATEGORY</a>
        <p>
          <span>View </span>
          <select>
                 <option value="">Current</option>
                 <option value=""></option>
          </select> 
        </p>
       
       <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dataList']->value, 'list', false, 'j');
$_smarty_tpl->tpl_vars['list']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['list']->value) {
$_smarty_tpl->tpl_vars['list']->do_else = false;
?>

      <div class="store-accounts">
      
   <form name="myForm" method="post" id="myForm" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/updateStandardCategoryPriority" > 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th width="10%" height="33">LIVE</th>
          <th width="16%">PRTY</th>
          <th width="52%"><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</th>
		   <th width="9%">PRICE</th>
		    <th width="4%"></th>
		    <th width="9%"><a style="color: #deb885;text-decoration:underline;" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/deleteStandardCategory/?id=<?php echo $_smarty_tpl->tpl_vars['list']->value['id'];?>
" onclick="return confirm('Are you sure you want to delete?')" class="deletecategory">Delete</a></th>

        </tr>
       
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value['data'], 'data', false, 'k');
$_smarty_tpl->tpl_vars['data']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->do_else = false;
?>
     
        <tr class="<?php if ($_smarty_tpl->tpl_vars['k']->value%2 == 0) {?>even<?php }?>">
          <td width="10%" align="center">
          <span class="multi-left">
         
               <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['data']->value['live']) {?> checked="checked" <?php }?> name="livecheckbox<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" class="input-checkbox" id="checkbox-<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">
             <label class="multisel-ckeck" for="checkbox-<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"></label>
            </span>
          </td>
          <td width="16%"><input type="text" class="field" name="priority[]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['priority'];?>
"></td>
          <td width="52%"><a href="#"><?php echo $_smarty_tpl->tpl_vars['data']->value['product_name'];?>
</a></td>
          <td width="9%"><a href="#"><?php echo '$';
echo $_smarty_tpl->tpl_vars['data']->value['product_price'];?>
</a></td>
          <td width="4%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/editStandardProducts/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">edit</a></td>
          <td width="9%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/deleteStandardCategoryProducts/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" onclick="return confirm('Are you sure you want to delete?')">delete</a></td>
        </tr>
  <input type="hidden" name="hidden_standard_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </tbody></table>  
     

       <a href="#" onclick="document.getElementById('myForm').submit();" class="update update_priority">update</a>
      
      <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/newStandardProducts/<?php echo $_smarty_tpl->tpl_vars['list']->value['id'];?>
" class="add-user">ADD NEW ITEM</a>
   </form>       
    </div>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>



<!------------------------------------------Catering items loop--------------------------------------------------->
        <h2>CATERING</h2>       
        <a class="category" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/newStandardCategory">NEW CATEGORY</a>
        <p>
          <span>View </span>
          <select>
                 <option value="">Current</option>
                 <option value=""></option>
          </select> 
        </p>
       
       <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cateringdataList']->value, 'list', false, 'j');
$_smarty_tpl->tpl_vars['list']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['list']->value) {
$_smarty_tpl->tpl_vars['list']->do_else = false;
?>

      <div class="store-accounts">
      
   <form name="myFormCatering" method="post" id="myForm" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/updateStandardCategoryPriority" > 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th width="10%" height="33">LIVE</th>
          <th width="16%">PRTY</th>
          <th width="52%"><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</th>
		   <th width="9%">PRICE</th>
		    <th width="4%"></th>
		    <th width="9%"><a style="color: #deb885;text-decoration:underline;" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/deleteStandardCategory/?id=<?php echo $_smarty_tpl->tpl_vars['list']->value['id'];?>
" onclick="return confirm('Are you sure you want to delete?')" class="deletecategory">Delete</a></th>

        </tr>
       
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value['data'], 'data', false, 'k');
$_smarty_tpl->tpl_vars['data']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->do_else = false;
?>
     
        <tr class="<?php if ($_smarty_tpl->tpl_vars['k']->value%2 == 0) {?>even<?php }?>">
          <td width="10%" align="center">
          <span class="multi-left">
         
               <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['data']->value['live']) {?> checked="checked" <?php }?> name="livecheckbox<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" class="input-checkbox" id="checkboxcat-<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">
             <label class="multisel-ckeck" for="checkboxcat-<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"></label>
            </span>
          </td>
          <td width="16%"><input type="text" class="field" name="priority[]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['priority'];?>
"></td>
          <td width="52%"><a href="#"><?php echo $_smarty_tpl->tpl_vars['data']->value['product_name'];?>
</a></td>
          <td width="9%"><a href="#"><?php echo '$';
echo $_smarty_tpl->tpl_vars['data']->value['product_price'];?>
</a></td>
          <td width="4%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/editStandardProducts/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">edit</a></td>
          <td width="9%"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/deleteStandardCategoryProducts/<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" onclick="return confirm('Are you sure you want to delete?')">delete</a></td>
        </tr>
  <input type="hidden" name="hidden_standard_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </tbody></table>  
     

       <a href="#" onclick="document.getElementById('myFormCatering').submit();" class="update update_priority">update</a>
      
      <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/newStandardProducts/<?php echo $_smarty_tpl->tpl_vars['list']->value['id'];?>
" class="add-user">ADD NEW ITEM</a>
   </form>       
    </div>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
   
  </div>
  
</div>  
<?php }
}
