<?php
/* Smarty version 3.1.39, created on 2021-03-25 09:34:05
  from 'C:\wamp64\www\hashbury\_admin\app\theme\templates\order_search.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c914dd08845_57049706',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '048b314304a9cb99ed429e1e832649aa4e93eab4' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_admin\\app\\theme\\templates\\order_search.tpl',
      1 => 1600125388,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c914dd08845_57049706 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <div class="start-order">
       <h1>ORDER SEARCH</h1>
       <form class="new-order-form" action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
neworder/orderSearch/">
       	<div class="text-box-holder">
          <label>Search Order Number or Customer</label>
          <input name="search" type="text" class="main-text-box common-text-width" value="<?php echo $_smarty_tpl->tpl_vars['search']->value;?>
">
          <a href="#" class="arrow search_order">&nbsp;</a>
        </div>
        </form>
         <?php if (count($_smarty_tpl->tpl_vars['orders']->value) > 0) {?> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
			  <tbody><tr>
            <th width="22%">CUSTOMER NAME</th>
            <th width="18%">ORDER NUMBER</th>
            <th width="32%">EMAIL</th>
            <th width="18%">PHONE</th>
            <th width="13%">SALES TOTAL</th>
          </tr>
			
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orders']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<?php if ((1 & $_smarty_tpl->tpl_vars['k']->value)) {?> 
				<tr class="">
				<?php } else { ?>	
				<tr class="even">
				<?php }?>	
			   <td><a href="#"><?php echo $_smarty_tpl->tpl_vars['v']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['v']->value->last_name;?>
</a></td>
				<td><?php echo $_smarty_tpl->tpl_vars['v']->value->order_number;?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['v']->value->username;?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['v']->value->phone;?>
</td>
				<td>$<?php echo $_smarty_tpl->tpl_vars['v']->value->total;?>
</td>
			</tr>  
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 		
		  
       </tbody>
       </table>
       <?php } else { ?>
			<ul class="order-history-inner">				
	            <li style="width:100%;"><span><h3 class="no_order_history">No Previous Orders.</h3></span></li>	            
	        </ul>  
       
       <?php }?>
        
    </div>
  </div>
<?php }
}
