         <div class="order-history">
         {if $orderhistory|@count gt 0} 
		        
         {foreach from=$orderhistory key=id item=history}
			<ul class="order-history-inner">
	            <li> <span class="section1">
	              <h3>{$history->delivery_type}</h3>
	              <h4>{$history->date}</h4>
	              <a href="#" data-order="{$history->order_number}" data-order-id="{$history->order_id}" class="view-details">VIEW DETAILS</a> </span> </li>
	            <li> <span class="section2">
	              <h3>
						{if $history->address->address1 ne ""}{$history->address->address1}{/if}
					  {if $history->address->address2 ne ""}, {$history->address->address2}{/if}
					  {if $history->address->zip ne ""}, {$history->address->zip}{/if}
	              </h3>
	              <p>
					{foreach from=$history->items key=id item=item}  
						({$item->qty}) {$item->name} <br/>
					{/foreach}
	              </p>
	              </span> </li>
	            <li> <span class="section3">
	              <h3>${$history->total}</h3>
	              </span> </li>
	            <li> <span class="section4"> <a href="javascript:void(0);" data-user-id="{$history->user_id}" data-order="{$history->order_number}" data-order-id="{$history->order_id}"class="reorder">REORDER</a> </span> </li>
			</ul>			
         {/foreach}
         
        {else}
             <ul class="order-history-inner">
				
	            <li style="width:100%;"><span><h3 class="no_order_history">No Previous Orders.</h3></span></li>	            
	        </ul>    
			
         {/if}
        </div>
      </div>
</div>
