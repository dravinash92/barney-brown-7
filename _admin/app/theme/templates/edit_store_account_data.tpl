<div class="container">
    <div class="user-account">
       <h1>EDIT STORE ACCOUNT</h1>
       <form name="maincontents" action="{$SITE_URL}accounts/updateStoreAccount" method="POST">
	   <div class="column column-1">
        {if $isStore==0}
          <label>Store Name</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->store_name}" name="store_name">
          <label>Store Address Line One</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->address1}" name="address1">
          <label>Store Address Line Two</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->address2}" name="address2">
          <label>Store Zipcode</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->zip}" name="zip">
          <label>Username</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->username}" name="username" disabled>
          <label>Password</label>
          <input type="password" name="password">
          <label>Confirm Password</label>
          <input type="password" value="" name="con_password">
          <label>Reports Pin</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->report_pin}" name="report_pin">
     <span class="multi-left">
               <input type="checkbox" id="checkbox-2-0" class="input-checkbox" name="ip_checked" value="" {if $store_accounts_data->pickup_stores->restrict_ip ne ""} checked {/if}>
          <label for="checkbox-2-0" class="multisel-ckeck"></label>
         </span><label class="restrict">Restrict IP Access to:</label>
          <input type="text" name="restrict_ip" value="{$store_accounts_data->pickup_stores->restrict_ip}">
          
                   
           <input type="hidden" name="id" value="{$store_accounts_data->pickup_stores->id}" />
          
        
          <button type="button" class="save_store_account">SAVE STORE</button>
          <a class="cancel" href="{$SITE_URL}accounts">CANCEL</a>
		
		</div>
		
	    <!--    
      	 <div class="pick-up"  style="margin-left: 312px; width: 368px; height: 623px; margin-top: -578px;"> -->
        {else}
          <input type="hidden" name="id" value="{$store_accounts_data->pickup_stores->id}" />
        <input type="hidden" value="{$store_accounts_data->pickup_stores->store_name}" name="store_name">
        <input type="hidden" value="{$store_accounts_data->pickup_stores->address1}" name="address1">
        <input type="hidden" value="{$store_accounts_data->pickup_stores->address2}" name="address2">
        <input type="hidden" value="{$store_accounts_data->pickup_stores->zip}" name="zip">
        <input type="hidden" value="{$store_accounts_data->pickup_stores->username}" name="username" disabled>
        
         <!-- <div class="pick-up" style=" width: 368px; " > -->
        {/if}
		<div class="column column-2">
		<div class="pick-up" style=" width: 368px; " >
         <h3>Pick-Up / Delivery Windows</h3>
          <ul>
           <li class="radio"></li>
           <li class="week">&nbsp;</li>
           <li class="from">From</li>
           <li class="to">To</li>
         </ul>
         
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="day0" value="Sunday" {if $store_timings_day[0] eq 'Sunday' && $store_active[0] eq 1 } checked {/if}>
               <label for="checkbox-2-1" class="multisel-ckeck"></label>
              </span>
           </li>
      
           <li class="week">Sunday</li>
           <li class="from"> 
             <select name="day_open0">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_open[0] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
			</select>

           </li>
           <li class="to">
                        <select name="day_close0">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_close[0] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
          <input type="checkbox" {if $pickup_active[0] eq 1 } checked {/if} id="checkbox-0-pickup" class="input-checkbox" name="checkbox-0-pickup" value="1">
                <label for="checkbox-0-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" {if $delivery_active[0] eq 1 } checked {/if} id="checkbox-0-delivery" class="input-checkbox" name="checkbox-0-delivery" value="1">
                <label for="checkbox-0-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
          </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-2" class="input-checkbox" name="day1" value="Monday" {if $store_timings_day[1] eq 'Monday' && $store_active[1] eq 1} checked {/if}>
               <label for="checkbox-2-2" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Monday</li>
           <li class="from"> 
             <select name="day_open1">
                {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_open[1] eq $timesection|date_format:'%H:%M' } selected  {/if}>{$timesection}</option>
				{/foreach}
             </select>
           </li>
           <li class="to">
             <select name="day_close1">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_close[1] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
          
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" {if $pickup_active[1] eq 1 } checked {/if} id="checkbox-1-pickup" class="input-checkbox" name="checkbox-1-pickup" value="1">
                <label for="checkbox-1-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" {if $delivery_active[1] eq 1 } checked {/if} id="checkbox-1-delivery" class="input-checkbox" name="checkbox-1-delivery" value="1">
                <label for="checkbox-1-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-3" class="input-checkbox" name="day2" value="Tuesday" {if $store_timings_day[2] eq 'Tuesday' && $store_active[2] eq 1} checked {/if}>
               <label for="checkbox-2-3" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Tuesday</li>
           <li class="from"> 
             <select name="day_open2">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_open[2] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           <li class="to">
             <select name="day_close2">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_close[2] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" {if $pickup_active[2] eq 1 } checked {/if} id="checkbox-2-pickup" class="input-checkbox" name="checkbox-2-pickup" value="1">
                <label for="checkbox-2-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" {if $delivery_active[2] eq 1 } checked {/if} id="checkbox-2-delivery" class="input-checkbox" name="checkbox-2-delivery" value="1">
                <label for="checkbox-2-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-4" class="input-checkbox" name="day3" value="Wednesday" {if $store_timings_day[3] eq 'Wednesday' && $store_active[3] eq 1} checked {/if}>
               <label for="checkbox-2-4" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Wednesday</li>
           <li class="from"> 
             <select name="day_open3">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_open[3] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           <li class="to">
             <select name="day_close3">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_close[3] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" {if $pickup_active[3] eq 1 } checked {/if} id="checkbox-3-pickup" class="input-checkbox" name="checkbox-3-pickup" value="1">
                <label for="checkbox-3-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" {if $delivery_active[3] eq 1 } checked {/if} id="checkbox-3-delivery" class="input-checkbox" name="checkbox-3-delivery" value="1">
                <label for="checkbox-3-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="day4" value="Thursday" {if $store_timings_day[4] eq 'Thursday' && $store_active[4] eq 1} checked {/if}>
               <label for="checkbox-2-5" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Thursday</li>
           <li class="from"> 
             <select name="day_open4">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_open[4] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           <li class="to">
             <select name="day_close4">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_close[4] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
          
           <li class="deactdelvery" >
               <span class="multi-left" >
                  <input type="checkbox" {if $pickup_active[4] eq 1 } checked {/if} id="checkbox-4-pickup" class="input-checkbox" name="checkbox-4-pickup" value="1">
                <label for="checkbox-4-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery">
               <span class="multi-left">
                  <input type="checkbox" {if $delivery_active[4] eq 1 } checked {/if} id="checkbox-4-delivery" class="input-checkbox" name="checkbox-4-delivery" value="1">
                <label for="checkbox-4-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-6" class="input-checkbox" name="day5" value="Friday" {if $store_timings_day[5] eq 'Friday' && $store_active[5] eq 1} checked {/if}>
               <label for="checkbox-2-6" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Friday</li>
           <li class="from"> 
             <select name="day_open5">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_open[5] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           <li class="to">
             <select name="day_close5">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_close[5] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
          
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" {if $pickup_active[5] eq 1 } checked {/if} id="checkbox-5-pickup" class="input-checkbox" name="checkbox-5-pickup" value="1">
                <label for="checkbox-5-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" {if $delivery_active[5] eq 1 } checked {/if} id="checkbox-5-delivery" class="input-checkbox" name="checkbox-5-delivery" value="1">
                <label for="checkbox-5-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-7" class="input-checkbox" name="day6" value="Saturday" {if $store_timings_day[6] eq 'Saturday' && $store_active[6] eq 1} checked {/if}>
               <label for="checkbox-2-7" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Saturday</li>
           <li class="from"> 
             <select name="day_open6">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_open[6] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           <li class="to">
             <select name="day_close6">
              {foreach from=$timesections item=timesection}
					<option value="{$timesection|date_format:'%H:%M'}" {if $store_timings_close[6] eq $timesection|date_format:'%H:%M'} selected {/if}>{$timesection}</option>
			 {/foreach}        
             </select>
           </li>
           
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" {if $pickup_active[6] eq 1 } checked {/if} id="checkbox-6-pickup" class="input-checkbox" name="checkbox-6-pickup" value="1">
                <label for="checkbox-6-pickup" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
           </li>
           <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" {if $delivery_active[6] eq 1 } checked {/if} id="checkbox-6-delivery" class="input-checkbox" name="checkbox-6-delivery" value="1">
                <label for="checkbox-6-delivery" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
            </li>
            
         </ul>
         
         <!--
         <div class="deactivate">
            <ul>
              <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-8" class="input-checkbox" name="" value="">
                <label for="checkbox-2-8" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
              </li>
              <li class="deactdelvery" >
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-9" class="input-checkbox" name="" value="">
                <label for="checkbox-2-9" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
              </li>
            </ul>
         
         </div>
         -->
           {if $isStore}
                 <button type="button" class="save_store_account">SAVE STORE</button>
          <a class="cancel" href="{$SITE_URL}accounts">CANCEL</a>
          {/if}
       </div>
	   <div class="pick-up" style=" width: 368px; " >
         <h3>CLOSE SPECIFIC DAY</h3>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="specificDay" class="input-checkbox" name="specificday_active" value="1"  {if $store_accounts_data->pickup_stores->specificday_active eq 1}  checked {/if}>
               <label for="specificDay" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Close Specific Day</li>
          </ul>
           <div style="clear:both;"></div>
          <ul>
           <li class="deactdelvery text-box-holder2" >
               <span class="multi-left">
                  <input name="specificday" id="search_delivery_date" type="text" class="text-box" name="specificDay" value="{$store_accounts_data->pickup_stores->specificday}">
                  <a href="#" class="date-icon"></a>
               </span>
            </li>
         </ul>
         <div style="clear:both;"></div>
         <ul style="margin-top: 20px;">
           <li class="deactdelvery" >
           <label  class="multisel-ckeck">Message</label>
               <span class="multi-left">
                  <textarea rows="" cols="" id="specificday_message" name="specificday_message">{$store_accounts_data->pickup_stores->specificday_message}</textarea>
               </span>
            </li>
         </ul>
         </div>
	   </div>
	   
	   <div class="column column-3">
	   
       {if $isStore==0}
       <div class="zipcode-wrapper ten">
         <h3>ALLOWED ZIP CODES($10 MIN)</h3> 
           <div class="zip_wrap">
         <ul class="heading">
           <li class="zip-code">Zip Code</li>
           <li class="abbr">Abbr.</li>
           <li class="abbr">Del. Fee</li>
         </ul>
        {foreach from=$store_accounts_data->store_zipcodes key=k item=v}
         {if ($v->min_order eq 10 || $v->min_order eq 0)}
			<ul>
			   <li class="zip-code"><input type="text"  value="{$v->zipcode}" name="zipcode{$k+1}">
			    <input name="zipCodeId" type="hidden" value="{$v->id}" />
			   </li>

			   <li class="abbr"><input  maxlength="3" type="text" value="{$v->abbreviation}" name="abbreviation{$k+1}">
			    <input type="hidden" name="minAmount{$k+1}" class="minAmount" value="{$v->min_order}"></li>
          <li class="delivery-fee"><input type="text" value="{$v->delivery_fee}" name="deliveryfee{$k+1}"></li>

			    <li class="zipItemrmv"><a class="removeZip" >Remove</a></li>
			</ul>
			{/if}
		{/foreach}
		</div>
		<input type="hidden" name="zipcount" id="zipcount" value="{$store_accounts_data->store_zipcodes|@count}" />		
       
        <div style="clear:both"><a class="add zip_add" data-minamount="10" href="javascript:void(0)" style="">add</a> </div>
         
       </div>
     <div class="zipcode-wrapper hundred" >
         <h3>ALLOWED ZIP CODES($100 MIN)</h3> 
           <div class="zip_wrap">
         <ul class="heading">
           <li class="zip-code">Zip Code</li>
           <li class="abbr">Abbr.</li>
           <li class="abbr">Del. Fee</li>
         </ul>
        {foreach from=$store_accounts_data->store_zipcodes key=k item=v}
        {if $v->min_order eq 100}
			<ul>
			   <li class="zip-code"><input type="text"  value="{$v->zipcode}" name="zipcode{$k+1}">
			    <input name="zipCodeId" type="hidden" value="{$v->id}" />
			   </li>
			   <li class="abbr"><input type="text" maxlength="3" value="{$v->abbreviation}" name="abbreviation{$k+1}">
			   <input type="hidden" name="minAmount{$k+1}" class="minAmount" value="{$v->min_order}"></li>
         <li class="delivery-fee"><input type="text" value="{$v->delivery_fee}" name="deliveryfee{$k+1}"></li>
			    <li class="zipItemrmv"><a class="removeZip" >Remove</a></li>
			</ul>
			{/if}
		{/foreach}
		</div>
		<input type="hidden" name="zipcount" id="zipcounthundred" value="{$store_accounts_data->store_zipcodes|@count}" />	
        <div style="clear:both"><a class="add zip_add_hundred" data-minamount="100" href="javascript:void(0)" style="">add</a> </div>
         
       </div>     
         {/if}
		 
		</div> 
       </form>
    </div>
  </div>
 
