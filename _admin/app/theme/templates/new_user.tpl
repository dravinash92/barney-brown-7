<div class="container">
    <div class="user-account">
       <h1>NEW USER ACCOUNT</h1>
       <form action="{$SITE_URL}accounts/addNewUser" method="POST">
          <label>Name</label>
          <input name="admin_user_name" type="text" value="">
          <label>Username</label>
          <input name="admin_user_uname" type="text" value="">
          <label>Password</label>
          <input name="admin_user_pwd" type="password">
          <label>Confirm Password</label>
          <input name="admin_user_cpwd" type="password" value="">
          <label>Access Level</label>
          <select name="access_level">
          	<option value="">--Please Select--</option>
        	{foreach from=$admin_user_category key=k item=b}  
            <option value="{$b->id}">{$b->cat_name}</option>
            {/foreach}
          </select><br/><br/><br/>
          <input type="submit" name="submit" value="ADD NEW USER">
       </form>
    </div>
  </div>
