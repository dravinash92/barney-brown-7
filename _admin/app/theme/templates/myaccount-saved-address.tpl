		<div class="saved-address-wrapper">
          <ul class="saved-address-inner">
            <li> <span class="add-new-address-main"> <a href="#" class="change change-address" ><span>+</span><br />
              ADD NEW<br />
              ADDRESS</a> </span>
            </li>
            {if $addresses|@count gt 0 }
         
            {foreach from=$addresses key=k item=v}
				<li> <span class="address-content">
				  <h3>{$v->name} </h3>
				  <p>
					{$v->company}<br />
					{$v->address1}, {$v->address2}<br />
					{if $v->street ne ""} {$v->street} {/if}					
					{if $v->cross_streets ne "" }, ({$v->cross_streets}) <br /> {/if}
					New York, {$v->zip}<br />
					{$v->phone}</p>
				  {if $v->delivery_instructions ne ""}<p>DELIVERY INSTRUCTIONS:{$v->delivery_instructions|truncate:35:"..."}</p>{/if}<br/>
					{if $v->extn ne ""}<p>Ring buzzer {$v->extn}</p>{/if}
				  </span> 
				<span class="button-holder-address">
					<a data-target="{$v->address_id}" class="edit edit_address">EDIT</a>
					<a data-target="{$v->address_id}" class="remove remove_address">remove</a>
				</span>
				</li>
            {/foreach }
            {/if}
          </ul>
        </div>
      </div>
  </div>
  
 
