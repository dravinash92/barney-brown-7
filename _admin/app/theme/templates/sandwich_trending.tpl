<div class="container">
	<div class="sandwich-gallery">
		<div class="heading">
         <h1>TRENDING SANDWICHES</h1>
         <a class="add_sotd_button">ADD NEW</a>
        </div>
        {if $sandwiches|@count gt 0}
        <table class="listing sandwich-trending">
			<tbody>
				<tr class="static">
				  <th width="10%"> LIVE  </th>		
				  <th width="20%" height="31">Priority</th>				  
				  <th width="25%" height="31">CUSTOM SANDWICHES</th>
				  <th width="15%" height="31">USER</th>
				  <th width="10%">PRICE</th>				  
				  <th width="10%">        </th>	
				  <th width="10%">        </th>					  
				</tr>
				
				{foreach from=$sandwiches key=k item=sandwich }
				<tr class="{cycle values="odd,even"}">
					<td width="10%" align="center"> 
					  <span class="multi-left">
					   <input type="checkbox" name="" class="do-trnd input-checkbox" id="checkbox-{$k}" data-id="{$sandwich->id}" {if $sandwich->trnd_live eq 1} checked {/if}>
					   <label class="multisel-ckeck" for="checkbox-{$k}"></label>
					  </span>
					</td>	
					<td  width="20%"><input class="do-trnd input priority_class" id="{$sandwich->id}" style="max-width: 40%;" type="number" min="1" name="priority" value="{$sandwich->priority}" /></td>	
					<td>{$sandwich->sandwich_name}</td>					
					<td>{$sandwich->first_name} {$sandwich->last_name}</td>
					<td>${$sandwich->sandwich_price}</td>
					<td> <a href="#" class="remove_trnd" data-sandwich="{$sandwich->id}">Remove</a></td>
					<td> <a href="javascript:;" class="set_priority" data-priority="{$sandwich->id}" data-sandwich="{$sandwich->id}">Update</a></td>
				</tr>
				{/foreach}				
			</tbody>	
        </table>
        {else}
			
			<p> No Sandwiches in this section.</p>
			
        {/if}
    </div>     
</div>
