<div class="container">
  <div class="condiment">
    <h1>NEW CONDIMENT</h1>

    <form name="newprotein" method="post" action="{$SITE_URL}menu/addCategoryItems" enctype="multipart/form-data">


      <div class="edit-item--input">
        <label>Name of Condiment</label>
        <input type="text" name="item_name" value="" placeholder="Prociutto">
      </div>

      <div class="edit-item--input">
        <label>Price</label>
        <input type="text" name="item_price" value="" placeholder="Price">
      </div>
      <div class="texa-prem_wrap">
        <div class="texa-prem forms">
          <label>Taxable</label>
          <!--   <input type="checkbox" value="1" name="premium" class="price-menu"  > -->
          <label for="cbk1">
            <input type="checkbox" id="cbk1" value="1" name="taxable">
            <span class="cbx">
              <svg width="12px" height="11px" viewBox="0 0 12 11">
                <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
              </svg>
            </span>

          </label>

        </div> 
          <input type="hidden" value="5" name="category_id">
        <div class="texa-prem forms">
          <label>Premium</label>
          <label for="cbk2">
            <input type="checkbox" id="cbk2" value="1" name="premium">
            <span class="cbx">
              <svg width="12px" height="11px" viewBox="0 0 12 11">
                <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
              </svg>
            </span>

          </label>
        </div>
      </div>

      {foreach from=$option_data key=k item=b}

      <div class="forms file-choose-pro">
        <label for="cbk-{$b.id}">
          <input type="checkbox" id="cbk-{$b.id}" name="select_sandwich_option[]" value="{$b.id}"  onclick="showUploadImage(this)" >
          <span class="cbx">
            <svg width="12px" height="11px" viewBox="0 0 12 11">
              <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
            </svg>
          </span>
        </label>
        <label for="cbk-{$b.id}" class="multisel-ckeck">{$b.option_display_name}</label>
      </div>

      <div class="slect-image--wrap" id="upload-cbk-{$b.id}" style="display: none;">
        <div>
          <label>Long Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="lImg-cbk-{$b.id}" name="lImg-{$b.id}" />
            </label>
            <span id="lImgName-cbk-{$b.id}">No file selected</span>
          </p>
        </div>

        <div>

          <label>Round Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="rImg-cbk-{$b.id}" name="rImg-{$b.id}"/>
            </label>
            <span id="rImgName-cbk-{$b.id}">No file selected</span>
          </p>
        </div>
        <div>
          <label>Trapezoid Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="tImg-cbk-{$b.id}" name="tImg-{$b.id}"/>
            </label>
            <span id="tImgName-cbk-{$b.id}">No file selected</span>
          </p>
        </div>
      </div>
      {/foreach}  


      <div class="button">
       <input type="submit" class="save save_custom_item" value="SAVE">
       <a href="{$SITE_URL}menu/custom" class="cancel">CANCEL</a>
     </div>
   </form>

 </div>
</div>
