	{if $sandwichitems|@count gt 0}	
		<div class="reports_table">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="sandwiches">
        <tbody><tr>
          <th width="65%">CUSTOM SANDWICHES</th>
           <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        <tr class="subheader">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><span class="hide_totalqty"></span></td>
          <td>$<span class="hide_subtottal"></span></td>
          <td>100%</td>
        </tr>
        <tr class="items">
          <td colspan="5">BREADS</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 1}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}</td>
           <td>${$v->item_price}
           {assign var=bprice value=$v->item_price}
           </td>
          <td>{foreach from=$breadsanddata key=bk item=bv}
          {if $v->item_name eq $bv.itemname}
          {assign var=bqty value=$bv.qty}
           {$bqty}
           {assign var="sum_qty_bread" value=$sum_qty_bread+$bqty}
          {/if}
          {/foreach}</td>
          <td>
         {foreach from=$breadsanddata key=bk item=bv}
          {if $v->item_name eq $bv.itemname}
          {assign var=breads_subtotal value=$bqty*$bprice}
          ${$breads_subtotal|string_format:"%.2f"}
          {assign var="sum_all_breads" value=$sum_all_breads+$breads_subtotal}
          {/if}
          {/foreach}
          </td>
		   <td>
		   {foreach from=$breadsanddata key=bk item=bv}
          {if $v->item_name eq $bv.itemname}
          {assign var=bqty value=$bv.qty}
            {math equation="(( x / y ) * z )" x=$bqty y=$salesCount z=100 format="%.2f"}%
           {assign var="sum_qty_bread" value=$sum_qty_bread+$bqty}
          {/if}
          {/foreach}
		   
		   </td>
        </tr>
        {/if}
		 {/foreach}
		        
         <tr class="items">
          <td colspan="5">PROTEINS</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 2}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}
          </td>
           <td>${$v->item_price}
           {assign var=pprice value=$v->item_price}
           </td>
          <td>
          {foreach from=$proteinsanddata key=pk item=pv}
          {if $v->item_name eq $pv.itemname}
          {assign var=pqty value=$pv.qty}
          {$pqty}
          {assign var="sum_qty_protein" value=$sum_qty_protein+$pqty}
          {/if}
          {/foreach}
         </td>
          <td>
          {foreach from=$proteinsanddata key=pk item=pv}
          {if $v->item_name eq $pv.itemname}
          {assign var=protein_subtotal value=$pqty*$pprice}
          ${$protein_subtotal|string_format:"%.2f"}
          {assign var="sum_all_protein" value=$sum_all_protein+$protein_subtotal}
           {/if}
          {/foreach}
          </td>
		   <td>
		   
		   {foreach from=$proteinsanddata key=pk item=pv}
          {if $v->item_name eq $pv.itemname}
          {assign var=pqty value=$pv.qty}
           {math equation="(( x / y ) * z )" x=$pqty y=$salesCount z=100 format="%.2f"}%
          {assign var="sum_qty_protein" value=$sum_qty_protein+$pqty}
          {/if}
          {/foreach}
		   
		
		   
		   </td>
        </tr>
		{/if}
		 {/foreach}
				
		<tr class="items">
          <td colspan="5">CHEESES</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 3}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}</td>
           <td>${$v->item_price}
           {assign var=cprice value=$v->item_price}
           </td>
          <td>{foreach from=$cheesesanddata key=ck item=cv}
          {if $v->item_name eq $cv.itemname}
           {assign var=cqty value=$cv.qty}
          {$cqty}
           {assign var="sum_qty_cheese" value=$sum_qty_cheese+$cqty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$cheesesanddata key=ck item=cv}
          {if $v->item_name eq $cv.itemname}
          {assign var=cheese_subtotal value=$cqty*$cprice}
          ${$cheese_subtotal|string_format:"%.2f"}
          {assign var="sum_all_cheese" value=$sum_all_cheese+$cheese_subtotal}
           {/if}
          {/foreach}
          </td>
		   <td>
		   
		   {foreach from=$cheesesanddata key=ck item=cv}
          {if $v->item_name eq $cv.itemname}
           {assign var=cqty value=$cv.qty}
          {math equation="(( x / y ) * z )" x=$cqty y=$salesCount z=100 format="%.2f"}%
           {assign var="sum_qty_cheese" value=$sum_qty_cheese+$cqty}
          {/if}
          {/foreach}
		   
		   </td>
        </tr>
		{/if}
		{/foreach}
				
		<tr class="items">
          <td colspan="5">TOPPINGS</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 4}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}</td>
           <td>${$v->item_price}
           {assign var=tprice value=$v->item_price}
           </td>
          <td>{foreach from=$toppingssanddata key=tk item=tv}
          {if $v->item_name eq $tv.itemname}
          {assign var=tqty value=$tv.qty}
          {$tqty}
          {assign var="sum_qty_toppings" value=$sum_qty_toppings+$tqty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$toppingssanddata key=tk item=tv}
          {if $v->item_name eq $tv.itemname}
          {assign var=toppings_subtotal value=$tqty*$tprice}
          ${$toppings_subtotal|string_format:"%.2f"}
          {assign var="sum_all_toppings" value=$sum_all_toppings+$toppings_subtotal}
           {/if}
          {/foreach}
          </td>
		   <td>{foreach from=$toppingssanddata key=tk item=tv}
          {if $v->item_name eq $tv.itemname}
          {assign var=tqty value=$tv.qty}
          {math equation="(( x / y ) * z )" x=$tqty y=$salesCount z=100 format="%.2f"}%
          {assign var="sum_qty_toppings" value=$sum_qty_toppings+$tqty}
          {/if}
          {/foreach}</td>
        </tr>
		{/if}
		{/foreach}
		
		<tr class="items">
          <td colspan="5">CONDIMENTS</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 5}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}</td>
           <td>${$v->item_price}
           {assign var=coprice value=$v->item_price}
           </td>
          <td>{foreach from=$condisanddata key=cok item=cov}
          {if $v->item_name eq $cov.itemname}
          {assign var=coqty value=$cov.qty}
          {$coqty}
           {assign var="sum_qty_condi" value=$sum_qty_condi+$coqty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$condisanddata key=cok item=cov}
          {if $v->item_name eq $cov.itemname}
          {assign var=condi_subtotal value=$coqty*$coprice}
          ${$condi_subtotal|string_format:"%.2f"}
           {assign var="sum_all_condi" value=$sum_all_condi+$condi_subtotal}
          {/if}
          {/foreach}</td>
		   <td>
		   
		   {foreach from=$condisanddata key=cok item=cov}
          {if $v->item_name eq $cov.itemname}
          {assign var=coqty value=$cov.qty}
            {math equation="(( x / y ) * z )" x=$coqty y=$salesCount z=100 format="%.2f"}%
           {assign var="sum_qty_condi" value=$sum_qty_condi+$coqty}
          {/if}
          {/foreach}
		   
		   </td>
        </tr>
		{/if}
		{/foreach}
		{assign var="tottal_subtottal" value=$sum_all_condi+$sum_all_breads+$sum_all_protein+$sum_all_cheese+$sum_all_toppings}
		{assign var="tottal_qty" value=$sum_qty_bread+$sum_qty_protein+$sum_qty_cheese+$sum_qty_toppings+$sum_qty_condi}
    	
      </tbody></table>
	  
	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="products">
        <tbody><tr>
          <th width="65%">STANDARD ITEMS</th>
          <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        
        {foreach from=$standardCatagory key=sc item=sci}
        <tr class="subheader {$sc}">
          <td>{$sci->category_identifier}</td>
          <td></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-item-{$sc}"></span></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-{$sc}"></span></td>
          <td></td>
        </tr>
        {assign var="sum_all_salades" value=0}
        {assign var="sum_all_salades_count" value=0}
        
		{foreach from=$standareditems key=k item=v}
		{if $v->standard_category_id eq $sci->id}
		<tr class="{cycle values="odd,even"} datarow st-cat-{$sc}">
          <td>{$v->product_name}</td>
          <td>{$v->product_price|string_format:"%.2f"}
          {assign var=price value=$v->product_price}
          </td>
          <td>{foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {$sv->qty}
          {assign var=qty value=$sv->qty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {assign var=salades_subtotal value=$qty*$price}
          ${$salades_subtotal|string_format:"%.2f"}
           {assign var="sum_all_salades" value=$sum_all_salades+$salades_subtotal}
           {assign var="sum_all_salades_count" value=$sum_all_salades_count+$qty}
           {/if}
          {/foreach}
           </td>
		   <td> 
		   
		   {foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {assign var="stpq" value=$sv->qty}
          {math equation="(( x / y ) * z )" x=$stpq y=$salesCount z=100 format="%.2f"}%
          {assign var=qty value=$sv->qty}
          {/if}
          {/foreach}
		   
		   </td>
        </tr>
		{/if}
		{/foreach}
		
		{assign var="tottal_subtottal" value=$tottal_subtottal+$sum_all_salades}
		{assign var="tottal_qty" value=$tottal_qty+$sum_all_salades_count}
		
		<input type="hidden" id="st-cat-{$sc}"  value="{$sum_all_salades|string_format:'%.2f'}"/>
		<input type="hidden" id="st-cat-item-{$sc}" value="{$sum_all_salades_count}"/>
        	
        {/foreach}	
		
	   </tbody>
	   </table>
	   
	   
	   
	   <table width="100%" cellspacing="0" cellpadding="0" border="0" class="products">
        <tbody><tr>
          <th width="65%">CATERING ITEMS</th>
          <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        
        {foreach from=$cateringCatagory key=cc item=cci}
        <tr class="subheader {$cc}">
          <td>{$cci->category_identifier}</td>
          <td></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-item-{$cc}"></span></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-{$cc}"></span></td>
          <td></td>
        </tr>
        {assign var="sum_all_salades" value=0}
        {assign var="sum_all_salades_count" value=0}
        
		{foreach from=$standareditems key=k item=v}
		{if $v->standard_category_id eq $cci->id}
		<tr class="{cycle values="odd,even"} datarow st-cat-{$cc}">
          <td>{$v->product_name}</td>
          <td>${$v->product_price|string_format:"%.2f"}
          {assign var=price value=$v->product_price}
          </td>
          <td>{foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {$sv->qty}
          {assign var=qty value=$sv->qty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {assign var=salades_subtotal value=$qty*$price}
          ${$salades_subtotal|string_format:"%.2f"}
           {assign var="sum_all_salades" value=$sum_all_salades+$salades_subtotal}
           {assign var="sum_all_salades_count" value=$sum_all_salades_count+$qty}
           {/if}
          {/foreach}
           </td>
		   <td> 
		   
		   {foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {assign var="stpq" value=$sv->qty}
          {math equation="(( x / y ) * z )" x=$stpq y=$salesCount z=100 format="%.2f"}%
          {assign var=qty value=$sv->qty}
          {/if}
          {/foreach}
		   
		   </td>
        </tr>
		{/if}
		{/foreach}
		
		{assign var="tottal_subtottal" value=$tottal_subtottal+$sum_all_salades}
		{assign var="tottal_qty" value=$tottal_qty+$sum_all_salades_count}
		
		<input type="hidden" id="st-cat-{$sc}"  value="{$sum_all_salades|string_format:'%.2f'}"/>
		<input type="hidden" id="st-cat-item-{$sc}" value="{$sum_all_salades_count}"/>
        	
        {/foreach}	
		
	   </tbody>
	   </table>
	   
	   
	   </div>
		 <span class="tottal_subtottal">{$tottal_subtottal|string_format:"%.2f"}</span>
    	 <span class="tottal_qty">{$tottal_qty}</span>

	{else}
		<h3>No orders found.</h3>	
	{/if}	
