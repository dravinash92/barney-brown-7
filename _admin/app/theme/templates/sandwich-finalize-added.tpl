      <div class="menu-list-wrapper">
        <h1>CREATE A SANDWICH</h1>
        <div class="create-sandwich-wrapper">
          <div class="create-sandwich-left">
            <div class="finalize-It-heading">
              <h3>Finalize It!</h3>
            </div>
            <div class="name-your-creation-popup">
              <h2>NAME YOUR CREATION</h2>
              <h3>MOZZ &amp; PICKLES, YUP</h3>
            </div>
            <div class="image-holder"> <img src="{$SITE_URL}app/images/condiments.png" alt="Instagram"> </div>
            <div class="button-holder">
              <ul>
                <li> <a href="javaScript:void(0)" class="edit-sandwich">EDIT SANDWICH</a></li>
              </ul>
            </div>
          </div>
          <div class="create-sandwich-right-wrapper">
            <div class="create-sandwich-right fill-color">
              <div class="final-list-wrapper scroll-bar">
                <ul>
                  <li>
                    <h4>BREAD</h4>
                    <p>Ciabatta</p>
                  </li>
                  <li>
                    <h4>PROTEIN<span>+$3</span></h4>
                    <p>Turkey (1), Ham (0.5), Pepperoni (0.5), Turkey (1), Ham (0.5), Pepperoni (0.5)</p>
                  </li>
                  <li>
                    <h4>CHEESE<span>+$3</span></h4>
                    <p>Cheddar (1)</p>
                  </li>
                  <li>
                    <h4>TOPPINGS<span>+$1</span></h4>
                    <p>Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado</p>
                  </li>
                  <li>
                    <h4>CONDIMENTS</h4>
                    <p>Mayonnaise (N), Blueberry Jam (S)</p>
                  </li>
                </ul>
              </div>
            </div>
            <div class="checkbox-holder-final">
              <input id="check1" type="checkbox" name="check" value="check1">
              <label for="check1">TOAST IT! <span>Yum!</span></label>
            </div>
            <div class="save-button-share">
              <div class="checkbox-holder-final">
                <input id="check2" type="checkbox" checked="checked" name="check" value="check2">
                <label for="check2">Public (Others can order this)</label>
              </div>
              <a href="#" class="share-sandwich">Share</a> <a href="#" class="save-to-my-menu">SAVE TO MY MENU</a> </div>
            <div class="bottom-buttons">
              <h4>$14.50</h4>
              <a href="{$SITE_URL}checkout" class="am-done added-button-color">ADDED TO CART</a> </div>
          </div>
        </div>
      </div>
      <!--Create Sandwich Ending-->
    </div>
  </div>
