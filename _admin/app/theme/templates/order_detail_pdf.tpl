<page>
<div class="pdf_container clearfix">
	<div class="pdf_container_left" >
		<div class="container_head">
			<h1>
				{if $is_delivery eq 1} DELIVERY {else} PICKUP {/if} #{$orderid}<br />TRANSACTION
				ID #{$authorizenet_transaction_id}<br />{$date} {if $time eq 1} -
				(SPECIFIC) {/if}
			</h1>
		</div>
		<div class="container_item">
			{section name="orderitems" loop=$order_item_detail} {if
			$order_item_detail[orderitems]->type eq "user_sandwich"}
<div class="order_details_left">
			<div class="order_details_checkbox-wrapper">
				<div class="checkbox">
					<input type="checkbox"
						name="sqrchk{$smarty.section.orderitems.index}"
						id="sqrchk{$smarty.section.orderitems.index}"
						class="printchksqure" /> <label
						for="sqrchk{$smarty.section.orderitems.index}"
						class="printchksqure_label"></label>
				</div>
				<div class="product">
					<div class="quantity">{$order_item_detail[orderitems]->qty}</div>
					<div class="ingredient">
						<div class="order-item-name">{$order_item_detail[orderitems]->sandwich_name}</div>
						{if $order_item_detail[orderitems]->sandwich_bread }<p><span
							class="ingredient_span">BRD:</span><span class="ingriedient-names">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$order_item_detail[orderitems]->sandwich_bread}</span></p>{/if}
						{if $order_item_detail[orderitems]->sandwich_condiments }<p><span class="ingriedient-names"
							class="ingredient_span">CON:</span><span class="ingriedient-names">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$order_item_detail[orderitems]->sandwich_condiments}</span><p>
						{/if} {if $order_item_detail[orderitems]->sandwich_protein
						}<p><span class="ingredient_span">PRO:</span><span class="ingriedient-names">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$order_item_detail[orderitems]->sandwich_protein}
						</span></p> {/if} {if $order_item_detail[orderitems]->sandwich_cheese
						}<p><span class="ingredient_span">CHE:</span><span class="ingriedient-names">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$order_item_detail[orderitems]->sandwich_cheese}</span></p>{/if}
						{if $order_item_detail[orderitems]->sandwich_topping }<p><span
							class="ingredient_span">TOP:</span><span class="ingriedient-names">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$order_item_detail[orderitems]->sandwich_topping} </span><p>{/if}
						{if $order_item_detail[orderitems]->toasted eq 1} <span class="toasted">Toasted</span>
						{/if}

					</div>
				</div>
			</div>
			</div>

			{else}

			<div class="order_details_checkbox-wrapper">
				<div class="checkbox">
					<input type="checkbox"
						name="sqrchk{$smarty.section.orderitems.index}"
						id="sqrchk{$smarty.section.orderitems.index}"
						class="printchksqure" /> <label
						for="sqrchk{$smarty.section.orderitems.index}"
						class="printchksqure_label"></label>
				</div>
				<div class="product">
					<div class="quantity">{$order_item_detail[orderitems]->qty}
					</div>
					<div class="ingredient"><div class="order-item-name">{$order_item_detail[orderitems]->product_name}
					</div></div>
				</div>
			</div>

			{/if} {/section}
		</div>
	</div>
	<div class="order_details_right">
	<div class="ingredient-wrapper">
	<div class="delivery_address ">
		<div class="container_address">
		  <div class="add-detail">
			<h3>{if $address->name} {$address->name} {/if}</h3>
			<h1 class="ordCount">
				<!-- {$count_order} -->
				{$address->zipAbbr}
			</h1>
                <div class="ingredient-details">
			{if $address->company|strip|count_characters}
			<p>{$address->company}</p>
			{/if} {if $address->addline1|strip|count_characters}
			<p>{$address->addline1}, {/if} {if
				$address->street|strip|count_characters} {$address->street}, {/if}
				{if $address->addline1|strip|count_characters ||
				$address->street|strip|count_characters}</p>
			{/if} {if $address->addline2|strip|count_characters}
			<p>{$address->addline2},</p>
			{/if}
			<p>New York, NY {$address->zip}</p>
			{if $is_delivery eq 1} {if $address->cross_streets }
			<p>({$address->cross_streets})</p>
			{/if} {/if}
			<p>{$address->phone}</p>


			{if $is_delivery eq 1 &&
			$delivery_instructions|strip|count_characters}
			<!--<div class="instructions">
                   <p>INSTRUCTIONS: {$delivery_instructions|truncate:60:"...":true}</p> </div>-->
			{/if} {if
			$order_item_detail[orderitemsdet]->sandwich_details|strip|count_characters}
			<p>{$order_item_detail[orderitemsdet]->sandwich_details|replace:',
				, ':', '}</p>
			{/if}

			<p>
				Order Placed:{$order_date_time|date_format:"%D -
				%H:%M"}
			</p>
			</div>
			</div>
		<div class="add-detail">
			<h3>{if $address->name} {$address->name} {/if}</h3>
			<h1 class="ordCount">
				<!-- {$count_order} -->
				{$address->zipAbbr}
			</h1>
              <div class="ingredient-details">
			{if $address->company|strip|count_characters}
			<p>{$address->company}</p>
			{/if} {if $address->addline1|strip|count_characters}
			<p>{$address->addline1}, {/if} {if
				$address->street|strip|count_characters} {$address->street}, {/if}
				{if $address->addline1|strip|count_characters ||
				$address->street|strip|count_characters}</p>
			{/if} {if $address->addline2|strip|count_characters}
			<p>{$address->addline2},</p>
			{/if}
			<p>New York, NY {$address->zip}</p>
			{if $is_delivery eq 1} {if $address->cross_streets }
			<p>({$address->cross_streets})</p>
			{/if} {/if}
			<p>{$address->phone}</p>


			{if $is_delivery eq 1 &&
			$delivery_instructions|strip|count_characters}
			<!--<div class="instructions">
                   <p>INSTRUCTIONS: {$delivery_instructions|truncate:60:"...":true}</p> </div>-->
			{/if} {if
			$order_item_detail[orderitemsdet]->sandwich_details|strip|count_characters}
			<p>{$order_item_detail[orderitemsdet]->sandwich_details|replace:',
				, ':', '}</p>
			{/if}

			<p>
				Order Placed:{$order_date_time|date_format:"%D -
				%H:%M"}
			</p>
			</div>
			</div>
		</div>
	</div>
		<div class="container_item">
			{assign var="foocount" value="0"} {assign var="barcount" value="0"}
			{assign var="sidebarcount" value="0"} {assign var="boxes" value="9"}
			{assign var="fcount" value=1} {assign var="pageCount" value=1}
			{assign var="gtrcount" value=22} {section name="orderitemsdet"
			loop=$order_item_detail} {if $order_item_detail[orderitemsdet]->type
			neq "product" } {section name=foo start=0
			loop=$order_item_detail[orderitemsdet]->qty step=1} {if $fcount ==
			10 } {assign var="pageCount" value=$pageCount+1} {/if} {if $fcount
			mod 11 eq 0 } {assign var="pageCount" value=$pageCount+1} {/if}


			<div class="ingredient">
				<div class="wrap_item">
					<div class="cell">

						<h3> {if $order_item_detail[orderitemsdet]->type eq "product" }
             {$order_item_detail[orderitemsdet]->product_name|truncate:35:"...":true}
             {else}
             {$order_item_detail[orderitemsdet]->sandwich_name|truncate:35:"...":true}
             {/if}  
              
             </h3>
             {assign var = "sandwichDetails"  value=","|explode:$order_item_detail[orderitemsdet]->sandwich_details}
             
             {assign var='sandwichDet' value=''  }
             {foreach name="det" from=$sandwichDetails item=foo}
             
              {if $smarty.foreach.det.index eq 0}    
                  {assign var='sep' value="" }  
                   {else }
                   {assign var='sep' value=", " }  
                {/if}  
             
              {if $foo neq ' '}
                {assign var='sandwichDet' value= $sandwichDet|cat:$sep|cat:$foo  }  
              {/if}
             {/foreach}


						<!-- <p class="normal_desc">{$sandwichDet|truncate:115:"...":true}</p> -->
						<p class="print_desc">{$sandwichDet|truncate:140:"...":true}</p>

						{if $order_item_detail[orderitemsdet]->toasted}<span class="toasted-detail">Toasted</span>
						{/if}

					</div>
				</div>
			</div>
			{assign var="barcount" value=$barcount+1} {if $sidebarcount gt 9}
			{assign var="y" value=10} {assign var="boxes" value=10} {else}
			{assign var="y" value=9} {/if} {if $boxes eq 9} {if $boxescount eq 8}

			{assign var="boxescount" value=0} {/if} {/if} {if $boxes eq 10} {if
			$boxescount eq 13} {assign var="boxescount" value=0} {/if} {/if} {if
			$fcount eq 10 }
			<!--<div class="page-break1"> &nbsp; &nbsp; &nbsp; &nbsp;</div>-->

			{else} {if $count_order gt 21} {if $fcount eq $gtrcount}
			<!--<div class="page-break2"> &nbsp; &nbsp; &nbsp; &nbsp;</div> -->
			{assign var="gtrcount" value=$gtrcount+12} {/if} {/if} {/if} {assign
			var="sidebarcount" value=$sidebarcount+1} {assign var="boxescount"
			value=$boxescount+1} {assign var="fcount" value=$fcount+1} {/section}
			{/if} {/section}
		</div>
	</div>
	</div>
</div>

</page>

