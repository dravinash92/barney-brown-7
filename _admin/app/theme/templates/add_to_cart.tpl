<div class="container">
    <div class="place-order">
       <h1>PLACE ORDER</h1>
       
       <div class="place-order-coustomer-info">
       		<div class="address-details">
           <h3>CUSTOMER INFO</h3>
           <p>Name: <span>{$user->first_name} {$user->last_name}</span> <input type="text" class="text-box edit_customer_info" name="first_name" placeholder="First Name" style="display:none;" > <input type="text" class="text-box edit_customer_info" name="last_name" placeholder="Last Name"style="display:none;"><br>
              Email: <span>{$user->username}</span>
               <input type="text" class="text-box edit_customer_info" name="username"  value="" style="display:none;"><br/>
              Phone: <span>{$user->phone}</span>
              <input type="text" class="text-box edit_customer_info" name="phone" placeholder="000-000-0000" value="{$user->phone}" style="display:none;"><br/>
              Total Orders: {$user->total_orders}<br>
              Total Sales: ${$user->total_sum}</p>
           <a href="javascript:void(0)" class="edit-info edit_info" >Edit Info</a>
           <a href="javascript:void(0)" class="edit-info save_info" style="display:none;">Save Info</a>
           <input type="hidden" name="user_id" class="user_id" value="{$user->uid}" />
         </div>
         
         <div class="customer-notes">
         	<h3>CUSTOMER NOTES</h3>
			<div class="customer-notes-list">
				{foreach from=$notes key=k item=v}
				<div class="note">
					<p><span class="note_date">{$v->date}</span><span class="address">{$v->notes}</span></p>
					<br/>
					<a href="javascript:void(0)" data-id="{$v->id}" class="remove remove_customer_note">Remove</a>
         		</div>				
				{/foreach}
         	</div>
         	<div class="add-customer-note" style="display:none;">
				<textarea col="20" rows="3" placeholder="Add new note"></textarea>	
				<br/>
				<a href="javascript:void(0)" class="btn-brown add_customer_note">Add</a>   <a href="javascript:void(0)" class="btn-brown cancel_customer_note">Cancel</a>
         	</div>	
            <a href="javascript:void(0)" class="add-new-note">add new note</a>
         </div>
         
         <div class="date-time-order">
         		<h3>DATE/TIME OF ORDER</h3>
            <div class="radio-holder">
            	<input type="radio" class="css-checkbox now_or_specific" id="radio1" name="radio_time" value="now">
              <label class="css-label time_type_label" for="radio1">Now</label>
              <input type="radio" class="css-checkbox now_or_specific" id="radio2" name="radio_time" value="specific">
              <label class="css-label time_type_label" for="radio2">Specific Date/Time</label>
            </div>
            
            <div class="text-box-holder">
            	<label>Date</label>
              <input type="text" class="text-box" name="">
              <a class="date-icon" href="#"></a>
            </div>
            <div class="text-box-holder">
            	<label>Time</label>
				{$times}
            </div>
         </div>
         
         <div class="pickup-deliver-order">
         	<div class="radio-holder">
				<input type="radio" class="css-checkbox address_type" id="radio3" name="radio_address_type" value="delivery">
				<label class="css-label address_type_label" for="radio3">DELIVERY</label>
				<input type="radio" class="css-checkbox address_type" id="radio4" name="radio_address_type" value="pickup">
				<label class="css-label address_type_label" for="radio4">PICK-UP</label>
            </div>
			<div class="text-box-holder address-container">
           
			</div>
          
         </div>
         
       </div>
       
       <h2>SANDWICHES</h2>
       <ul class="new-order-sandwichilist">
         <li><a href="{$SITE_URL}createsandwich/index/{$user->uid}" class="big-buttons-sandwich"><span>+</span>CREATE A SANDWICH</a></li>
         <li><a href="javascript:void(0)" class="big-buttons-sandwich choose-from-gallery"><span>+</span>SANDWICH GALLERY</a></li>
         
         {section name=sandwitch start=0 loop=$GALLARY_DATA|@count step=1 }
            
            {assign var = 'sandwich_id' value = $GALLARY_DATA[$smarty.section.sandwitch.index].id }
            {assign var = 'sandwich_uid' value = $GALLARY_DATA[$smarty.section.sandwitch.index].uid }
            {assign var = 'user_uid' value = $user->uid }
            {assign var = 'menu_is_active' value = $GALLARY_DATA[$smarty.section.sandwitch.index].menu_is_active }
             
            {php}
				
			if((in_array( $_smarty_tpl->get_template_vars('sandwich_id') , $_smarty_tpl->get_template_vars('temp_sandwiches') ) || $_smarty_tpl->get_template_vars('sandwich_uid') == $_smarty_tpl->get_template_vars('user_uid') ) && $_smarty_tpl->get_template_vars('menu_is_active') == 1 ){
				
            {/php} 
             
              {assign var = 'prot_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
                  {if $smarty.session.orders.item_id|@is_array} 
                   {assign var = 'items_id' value = $GALLARY_DATA[$smarty.section.sandwitch.index].id|@in_array:$smarty.session.orders.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
              
              
                <li {if $items_id  eq 1} rel="ADDED TO CART" {else} rel="ADD TO CART" {/if} id="toastID_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-formatdate="{$GALLARY_DATA[$smarty.section.sandwitch.index].formated_date}" data-username="{$GALLARY_DATA[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0]}"  data-date="{$GALLARY_DATA[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_price}" data-id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" data-flag="{$GALLARY_DATA[$smarty.section.sandwitch.index].flag}" data-name="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}"  data-sandwich_desc="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id ="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_desc_id}"  data-menuadds="{$GALLARY_DATA[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}" data-toast="{$GALLARY_DATA[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$GALLARY_DATA[$smarty.section.sandwitch.index].formated_date}" data-username="{$GALLARY_DATA[$smarty.section.sandwitch.index].user_name}" data-bread="{$bread_name}"  data-date="{$GALLARY_DATA[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$GALLARY_DATA[$smarty.section.sandwitch.index].current_price}" data-id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" data-name="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}" data-type="user_sandwich" data-likeid="{$GALLARY_DATA[$smarty.section.sandwitch.index].like_id}" data-likecount="{$GALLARY_DATA[$smarty.section.sandwitch.index].like_count}"> 
                <input type="hidden" name="image" id="sandImg_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="{$image_path}{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}/sandwich_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}_{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}.png">
                
                {if $GALLARY_DATA[$smarty.section.sandwitch.index].by_admin eq 1} 
                  {assign var = 'url' value = $SITE_URL}
                {else}
                  {assign var = 'url' value = $CMS_URL}
                {/if}
                
                
                <span class="sandwichilist-holder">
					        <img data-href="{$url}createsandwich/index/{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" width="156" src="{$image_path}{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}/thumbnails/sandwich_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}_{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}.png" alt="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}">
					        <h3>{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}</h3>
				          <h4>${$GALLARY_DATA[$smarty.section.sandwitch.index].current_price}</h4>
					        
					        <a href="javascript:void(0)" data-type="user_sandwich" data-id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" class="add-cart view-sandwich" >ADD TO CART</a>
				        </span>
                
                </li>
                
            {php}
			}
            {/php}    
                
            {/section}
         
       </ul>
       
		{foreach name=outer item=category from=$stdCategoryItems}
			<h2>{$category->standard_cat_name}</h2> 
			 <ul class="sides-list">      
			{foreach key=key item=item from=$category->categoryProducts}
				<li>
				   <h3>{$item->product_name}________________</h3>
				   <h4>${$item->product_price}</h4>
				   <span class="arrow-holder">
					 <a href="#" data-event="noupdate" class="arrow-left"></a>
					 <input name="quantity" type="text" class="qty" value="1">
					 <a href="#" data-event="noupdate" class="arrow-right"></a>
				   </span>
				   <!--<a href="{$SITE_URL}neworder/cartConfirm/" data-type="product" data-id="{$item->id}" class="add-cart">ADD TO CART</a>-->
				   <a href="javascript:void(0)" data-type="product" data-id="{$item->id}" class="add-cart {if $item->allow_spcl_instruction eq 1 or $item->add_modifier eq 1} salad-listing {else} add-to-cart {/if}" data-sandwich="product" data-sandwich_id="{$item->id}" data-uid="{$uid}" data-product_name="{$item->product_name}" data-description="{$item->description}" data-product_image="{$item->product_image}" data-product_price="{$item->product_price}" data-standard_category_id="{$item->standard_category_id}" data-uid={$uid} data-product="product" data-spcl_instr="{$item->allow_spcl_instruction}" data-add_modifier="{$item->add_modifier}" data-modifier_desc="{$item->modifier_desc}" data-modifier_isoptional="{$item->modifier_isoptional}" data-modifier_is_single="{$item->modifier_is_single}" data-modifier_options='{$modifier_options.$key|@json_encode}'>ADD TO CART</a>
				</li> 
			{/foreach}
			</ul>
		{/foreach}
       
		{foreach from=$cateringdataList key=j  item=list}
			   
			{if $list.data|@count gt 0 }
			<h2>{$list.cat_name}</h2> 
			 <ul class="sides-list">  
			{foreach from=$list.data key=k item=data}
				<li>
				   <h3>{$data->product_name}________________</h3>
				   <h4>${$data->product_price}</h4>
				   <span class="arrow-holder">
					 <a href="#" data-event="noupdate" class="arrow-left"></a>
					 <input name="quantity" type="text" class="qty" value="1">
					 <a href="#" data-event="noupdate" class="arrow-right"></a>
				   </span>
				   <!--<a href="{$SITE_URL}neworder/cartConfirm/" data-type="product" data-id="{$item->id}" class="add-cart">ADD TO CART</a>-->
				   <a href="javascript:void(0)" data-type="product" data-id="{$data->id}" class="add-cart {if $data->allow_spcl_instruction eq 1 or $data->add_modifier eq 1} salad-listing {else} add-to-cart {/if}" data-sandwich="product" data-sandwich_id="{$data->id}" data-uid="{$uid}" data-product_name="{$data->product_name}" data-description="{$data->description}" data-product_image="{$data->product_image}" data-product_price="{$data->product_price}" data-standard_category_id="{$data->standard_category_id}" data-uid={$uid} data-product="product" data-spcl_instr="{$data->allow_spcl_instruction}" data-add_modifier="{$data->add_modifier}" data-modifier_desc="{$data->modifier_desc}" data-modifier_isoptional="{$data->modifier_isoptional}" data-modifier_is_single="{$data->modifier_is_single}" data-modifier_options='{$data->modifier_options}'>ADD TO CART</a>
				</li> 
			{/foreach}
			</ul>	
			{/if}
			
		{/foreach}
		
		
		<div class="shopping-cart">
			{$shopitems}
		</div>
       
       <div class="billing-information-holder">
			<h3>BILLING INFORMATION</h3>
       	
			<div class="billing-card-holder">
			  <div class="text-box-holder">
				<select name="" class="billing-select">
					<option value="">Select Card</option>	
					<option value="no">No Charge</option>	
					<option value="0">Add New</option>
					{foreach from=$billingInfo key=myId item=billings}
						<option value="{$billings->id}">{$billings->card_type} card number ending with {$billings->card_number|substr:-4}</option>
					{/foreach}
				</select>
			  </div>
			  <!--<a href="javascript:void(0)" class="edit-bill">add new billing</a>-->
			  <!--<a href="javascript:void(0)" class="edit-bill">edit billing</a>-->
			</div>
			<br/>
			 <a href="javascript:void(0)" style="display:none;" class="change-billing">CHANGE</a>
			<div class="billing-info-show" style="display:none; color:#000;"></div>
			<input type="hidden" name="billing_card" value="" />
          <a href="javascript:void(0)" class="place-order">PLACE ORDER</a>
       </div>
       
    </div>
  </div>

<div class="popup-wrapper" id="add-new-address">
  <div class="add-new-address-inner"> <a href="#" class="close-button"></a>
	<form class="add_address" method="post" >
    <div class="title-holder">
      <h1>ADD NEW ADDRESS</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Recipient Name</p>
        <input name="recipient" type="text" class="text-box-control" placeholder="Enter Recipient Name">
        </span> <span class="text-box-holder">
        <p>Company</p>
        <input name="company" type="text" class="text-box-control" placeholder="Enter Company Name">
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Street Address 1</p>
        <input name="address1" type="text" class="text-box-control" >
        </span> <span class="text-box-holder">
        <p>Street Address 2</p>
        <input name="address2" type="text" class="text-box-control" >
        </span> <span class="text-box-holder1">
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" >
        </span> <span class="text-box-holder">
        <p>Phone Number</p>
        <input name="phone1" type="text" class="text-box-phone" >
        <input name="phone2" type="text" class="text-box-phone" >
        <input name="phone3" type="text" class="text-box-phone margin-none" >
        </span> <span class="text-box-holder1">
        <p>Ext.</p>
        <input name="extn" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <h2>New York, NY</h2>
        <span class="text-box-zip-holder1">
        <p>Zip Code</p>
        <input name="zip" type="text" class="text-box-zip" >
        </span> </span>
        <h3>{$title_text} currently delivers in Midtown Manhattan from 23rd Street to 59th Street and from 3rd Avenue to 8th Avenue.</h3>
      </li>
      <li> <span class="delivery-instructions">
        <p>Delivery Instructions</p>
        <input name="delivery_instructions" type="text" class="text-box-delivery" >
        </span> </li>
      <li> <a href="javaScript:void(0)" class="add-address save_address">ADD ADDRESS</a> </li>
    </ul>
    <input type="hidden" name="uid" value="{$user->uid}" />
    </form>
  </div>
</div>
<div class="popup-wrapper" id="edit-address">
  <div class="add-new-address-inner">
	
  </div>
</div>

<div class="popup-wrapper" id="sandwich-gallery">
  <div class="add-new-address-inner">
	  <a href="#" class="close-button"></a>
	<div class="row">
                  
         {section name=sandwitch start=0 loop=$GALLARY_DATA|@count step=1 }
            
            {assign var = 'sandwich_id' value = $GALLARY_DATA[$smarty.section.sandwitch.index].id }
            
              {assign var = 'prot_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
                  {if $smarty.session.orders.item_id|@is_array} 
                   {assign var = 'items_id' value = $GALLARY_DATA[$smarty.section.sandwitch.index].id|@in_array:$smarty.session.orders.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
                   
                   
                     
                {if $GALLARY_DATA[$smarty.section.sandwitch.index].by_admin eq 1} 
                  {assign var = 'url' value = $SITE_URL}
                {else}
                  {assign var = 'url' value = $CMS_URL}
                {/if}
              
              
                <div class="width33" {if $items_id  eq 1} rel="ADDED TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-formatdate="{$GALLARY_DATA[$smarty.section.sandwitch.index].formated_date}" data-username="{$GALLARY_DATA[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0]}"  data-date="{$GALLARY_DATA[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_price}" data-id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" data-name="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}"   > 
                
                
                <span class="sandwichilist-holder">
					<img data-href="{$url}createsandwich/index/{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" width="156" src="{$image_path}{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}/sandwich_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}_{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}.png" alt="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}">
					<h3>{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}</h3>
					<h4>${$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_price}</h4>
					<!--<a href="{$SITE_URL}neworder/cartConfirm/" class="add-to-cart">ADD TO CART</a>-->
					<a href="javascript:void(0)" data-type="user_sandwich" data-id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" class="button add-to-cart" >SELECT</a>
				</span>
                
                </div>
                
                
            {/section}
         
       </div>
  </div>
</div>

<!--credit card popup-->
<!--Popup Start-->
<div class="popup-wrapper" id="credit-card-details">
  <div class="add-new-address-inner"> <a href="javascript:void(0)" class="close-button">Close</a>
    <div class="title-holder">
      <h1>ADD NEW CREDIT CARD</h1>
    </div>
     <form id="billForm">
    <ul class="from-holder">
   
      <li> <!-- <span class="text-box-holder">
        <p>Credit Card Nick Name</p>
        <input name="cardNickname" type="text" class="text-box-control" placeholder="">
        </span>  --><span class="text-box-holder">
        <p>Card Number</p>
        <input name="cardNo" type="text" class="text-box-control" placeholder="">
        </span> <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="cardCvv" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
      <p>Credit Card</p>
        <span class="credit-card">
        <select name="cardType"  >
          
           <option value="visa">Visa</option>
          <option value="MasterCard">MasterCard</option>
          <option value="Discover">Discover</option>
          <option value="American Express">American Express</option>
        
        </select>
        </span> <br><br><br>
        <p>Expiration Date</p>
        <span class="month">
        <select name="cardMonth"  >
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="03">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <select name="cardYear">
         
          
          
{assign var="currentyear" value=$smarty.now|date_format:"%Y" }
{assign var="numyears" value=50}
{assign var="totalyears" value=$currentyear+$numyears}
{section name=loopyers  start=$currentyear loop=$totalyears step=1}
<option>{$smarty.section.loopyers.index}</option>
{/section}
         
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="cardZip" type="text" class="text-box-billing-zip" >
        </span> </li>
         <!--<li> <span class="text-box-holder">
        <p>Street Address </p>
        <input name="address1" type="text" class="text-box-control required" required >
        </span> 
	<span class="text-box-holder1"> 
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control required" required>
        </span> </li>-->
      <li> <span class="checkbox-save-bill">
        <input  type="checkbox" id="save_billing"  name="save_billing" value="save_billing"/>
        <label for="save_billing">Save Billing Info <span>(Verisign Encryption)</span></label>
        </span> </li>
      <li> <span class="card-holder-margin">
        <h3>One or two sentences about site security, encryption methods, storage of data meeting Y and Z standards (to let people feel secure about storing their credit card info).</h3>
        <a href="javascript:void(0)" class="credit-card-button">ADD</a> </span> </li>
    </ul>
    </form>
  </div>
  
  <form id="_payment_form" action="paymentpage/" method="post" style="display:none">
  <input type = "hidden" name="_card_type" value="" />
  <input type = "hidden" name="_card_number" value="" />
  <input type = "hidden" name="_card_cvv" value="" />
  <input type = "hidden" name="_card_zip" value="" />
  <input type = "hidden" name="_card_name" value="" />
  <input type = "hidden" name="_expiry_month" value="" />
  <input type = "hidden" name="_expiry_year" value="" />
  </form>
  
</div>
{literal}

{/literal}

<!--credit card popup-->
