<div class="container">
    <div class="start-order">
       <h1>ORDER SEARCH</h1>
       <form class="new-order-form" action="{$SITE_URL}neworder/orderSearch/">
       	<div class="text-box-holder">
          <label>Search Order Number or Customer</label>
          <input name="search" type="text" class="main-text-box common-text-width" value="{$search}">
          <a href="#" class="arrow search_order">&nbsp;</a>
        </div>
        </form>
         {if $orders|@count gt 0} 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
			  <tbody><tr>
            <th width="22%">CUSTOMER NAME</th>
            <th width="18%">ORDER NUMBER</th>
            <th width="32%">EMAIL</th>
            <th width="18%">PHONE</th>
            <th width="13%">SALES TOTAL</th>
          </tr>
			
		{foreach from=$orders key=k item=v}
				{if $k is not even} 
				<tr class="">
				{else}	
				<tr class="even">
				{/if}	
			   <td><a href="#">{$v->first_name} {$v->last_name}</a></td>
				<td>{$v->order_number}</td>
				<td>{$v->username}</td>
				<td>{$v->phone}</td>
				<td>${$v->total}</td>
			</tr>  
		{/foreach} 		
		  
       </tbody>
       </table>
       {else}
			<ul class="order-history-inner">				
	            <li style="width:100%;"><span><h3 class="no_order_history">No Previous Orders.</h3></span></li>	            
	        </ul>  
       
       {/if}
        
    </div>
  </div>
