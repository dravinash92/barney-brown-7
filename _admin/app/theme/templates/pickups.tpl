<div class="container">
    <div class="deliveries">
       <h1>PICK-UPS</h1>
       <div class="top-deliveries-wrapper">
       		<div class="left">
          		<table class="top-deliveries-table">
								<tbody><tr>
                  <td>NEW</td>
                 <td class="count_new">{$count_new}</td>
                </tr>
                <tr>
                  <td>PRC</td>
                  <td class="count_prc">{$count_prc}</td>
                </tr>
                <tr>
                  <td>OUT</td>
                  <td class="count_bag">{$count_bag}</td>
                </tr>
                <tr>
                  <td>DEL</td>
                  <td class="count_pick">{$count_pick}</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td class="total_pickUps" >{$total_pickUps}</td>
                </tr>
              </tbody></table>

          </div>
          <div class="right">
            <form action="{$SITE_URL}pickups/searchPickupsData" name="delivery_search" method="POST">
          	<div class="text-box-holder">
            	<label>Store Location</label>
              <select name="store_id" id="store_id" class="select">
				<option value="">Select Store</option>
				{foreach from=$pickupstores key=k item=v}
					<option value="{$v->id}" {if $data.store_id eq $v->id} selected {/if}>{$v->store_name}, {$v->address1}, {$v->address2}, {$v->zip}</option>
				{/foreach }	
              </select>
          	</div>
            <div class="text-box-holder">
            	<label>Status</label>
              <select name="delivery_status" id="1" class="select">
              <option value="">--Select--</option>
              <option value="0">New</option>
              	{foreach from=$getOrderSatusList key=k item=b}  
                <option value="{$b->id}" {if $del_status eq $b->id} selected {/if}>{$b->label}</option>
              {/foreach}    
              </select>
          	</div>
            <div class="text-box-holder2">
            	<label>Date</label>
              <input name="search_date" id="search_pickup_date" type="text" class="text-box" value="{$date}">
              <a href="#" class="date-icon"></a>
          	</div>
          	 <input type="submit" name="submit" value="SEARCH" style="margin-top: 15px;margin-left: 26px;">
             </form>
            
          </div>
       </div>
        {if $total_pickUps ne 0 } 
       <div class="deliveries-table-detail-wrapper">
       	 <h2>NEW</h2>
         <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
         
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
          {foreach from=$addressList key=k item=v}
          {if $v->order_status eq 0}
            {if $v->timediff gt 45 }  
            <tr style="background: #cef2cc;">
            {elseif ($v->timediff le 45) and ($v->timediff gt 30)}
            <tr style="background:#fdf5e0;">
            {elseif $v->timediff le 30}
            <tr style="background:#fedfe4;">
            {/if}
            <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDet">View order details</a></p>
            </td>
            <td>
            	<p>	{if $v->address->store_name|strip}{$v->address->store_name}</br>{/if}
					{if $v->address->phone|strip}{$v->address->phone}<br>{/if}              
					{if $v->address->address1|strip}{$v->address->address1}</br>{/if}
					{if $v->address->address2|strip}{$v->address->address2}</br>{/if}
					{if $v->address->zip|strip}{$v->address->zip}</br>{/if}    
				</p>
				<p>
				{foreach from=$v->tab key=id item=item}  
					{$item} 
				{/foreach}			
              </p>
            </td>
            <td>
				{foreach from=$v->items key=id item=item}  
					<p>({$item->qty}) {$item->name} </p>
				{/foreach}
            </td>
            {foreach from=$getOrderSatusList key=k item=b}  
             {if $b->code ne 'pickup_cancel'}
            
             <td><a  onclick="managePickupData({$v->order_id},{$b->id}{if $b->code eq 'pickup_prc'},{$v->order_number} {/if})" href="javascript:void(0)">	<img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
             {/if}
           {if $b->code eq 'pickup_cancel'}
           <td>	<a onclick="managePickupData({$v->order_id},{$b->id})" href="javascript:void(0)">Cancel</a></td>
           {/if}
	        
            {/foreach}
          </tr>
         {/if} 
       {/foreach}
          
   
          
        </tbody></table>
				<h2>PROCESSED</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
          
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
           {foreach from=$addressList key=k item=v}
          {if $v->order_status eq '1'}
          
          <tr>
             <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDet">View order details</a></p>
            </td>
           
              <td>
				<p>	{if $v->address->store_name|strip}{$v->address->store_name}</br>{/if}
					{if $v->address->phone|strip}{$v->address->phone}<br>{/if}              
					{if $v->address->address1|strip}{$v->address->address1}</br>{/if}
					{if $v->address->address2|strip}{$v->address->address2}</br>{/if}
					{if $v->address->zip|strip}{$v->address->zip}</br>{/if}              
				</p>
				<p>
				{foreach from=$v->tab key=id item=item}  
					{$item} 
				{/foreach}			
              </p>
            </td>
            <td>
				{foreach from=$v->items key=id item=item}  
					<p>({$item->qty}) {$item->name} </p>
				{/foreach}	
            </td>
             
          
           {foreach from=$getOrderSatusList key=k item=b}  
           			{if $v->order_status eq 1 && $b->id eq 1}
           			  <td><a onclick="managePickupData({$v->order_id},0)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
           			{/if}
           		   {if $b->code ne 'pickup_cancel' && $b->code ne 'pickup_prc'}
             <td><a onclick="managePickupData({$v->order_id},{$b->id})" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
             {/if}
           {if $b->code eq 'pickup_cancel'}
           <td>	<a onclick="managePickupData({$v->order_id},{$b->id})" href="javascript:void(0)">Cancel</a></td>
           {/if}
            {/foreach}
          </tr>
           {/if} 
       {/foreach}
        </tbody></table>
        <h2>BAGGED / READY</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
         {foreach from=$addressList key=k item=v}
          {if $v->order_status eq 2}
          
          <tr>
             <td>{$v->name}
            	<h3>{$v->time}</h3>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDet">View order details</a></p>
            </td>
           
              <td>
				<p>	{if $v->address->store_name|strip}{$v->address->store_name}</br>{/if}
					{if $v->address->phone|strip}{$v->address->phone}<br>{/if}              
					{if $v->address->address1|strip}{$v->address->address1}</br>{/if}
					{if $v->address->address2|strip}{$v->address->address2}</br>{/if}
					{if $v->address->zip|strip}{$v->address->zip}</br>{/if}              
				</p>
				<p>
				{foreach from=$v->tab key=id item=item}  
					{$item} 
				{/foreach}			
              </p>
            </td>
            <td>
				{foreach from=$v->items key=id item=item}  
					<p>({$item->qty}) {$item->name} </p>
				{/foreach}
     
            </td>
         {foreach from=$getOrderSatusList key=k item=b}  
           			{if $v->order_status eq 2 && $b->id eq 2}
           			  <td><a onclick="managePickupData({$v->order_id},0)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
           			   <td><a onclick="managePickupData({$v->order_id},1)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
           			{/if}
           		{if $b->code ne 'pickup_cancel' && $b->code ne 'pickup_prc' && $b->code ne 'pickup_bag'}
             <td><a onclick="managePickupData({$v->order_id},{$b->id})" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
             {/if}
           {if $b->code eq 'pickup_cancel'}
           <td>	<a onclick="managePickupData({$v->order_id},{$b->id})" href="javascript:void(0)">Cancel</a></td>
           {/if}
            {/foreach}
          </tr>
           {/if} 
       {/foreach}
          
        
          
        </tbody></table>
        <h2>PICKED UP</h2>
        <table class="deliveries-new-table border-bottom">
          <tbody><tr>
            <th>TIME</th>
            <th>CUSTOMER / LOCATION</th>
            <th>ORDER DETAILS</th>
        
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
          </tr>
          <tr>
           {foreach from=$addressList key=k item=v}
          {if $v->order_status eq 3}
          
          <tr>
             <td>{$v->name}
            	<h3>{$v->time}</h3>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDet">View order details</a></p>
            </td>
           
            <td>
				<p>	{if $v->address->store_name|strip}{$v->address->store_name}</br>{/if}
					{if $v->address->phone|strip}{$v->address->phone}<br>{/if}              
					{if $v->address->address1|strip}{$v->address->address1}</br>{/if}
					{if $v->address->address2|strip}{$v->address->address2}</br>{/if}
					{if $v->address->zip|strip}{$v->address->zip}</br>{/if}              
				</p>
				<p>
				{foreach from=$v->tab key=id item=item}  
					{$item} 
				{/foreach}			
              </p>
            </td>
            <td>
				{foreach from=$v->items key=id item=item}  
					<p>({$item->qty}) {$item->name} </p>
				{/foreach}
            </td>
          
          
            <td><a onclick="managePickupData({$v->order_id},0)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="managePickupData({$v->order_id},1)" href="javascript:void(0)""><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="managePickupData({$v->order_id},2)" href="javascript:void(0)""><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
              
          </tr>
             {/if} 
       {/foreach}
         
          
        </tbody></table>
       </div>
       {/if}
      {if $total_pickUps eq 0 }
      <div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div>
      {/if}
    </div>
  </div>