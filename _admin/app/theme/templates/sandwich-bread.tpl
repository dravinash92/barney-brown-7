  <h2>Choose Your Bread</h2>
              <h3>CHOOSE FROM OUR SELECTION OF NEW YORK'S AND NEW JERSEY'S FINEST.</h3>
              <div class="main-category-list">
              
         
              
                <ul>             
              

                {section name=options start=0 loop=$BREAD_DATA|@count step=1 }
                
                  <li>
                    <input data-shape="{$BREAD_DATA[$smarty.section.options.index].bread_shape}" data-bread_type="{$BREAD_DATA[$smarty.section.options.index].bread_type}" data-item_image_sliced="{$BREAD_DATA[$smarty.section.options.index].item_image_sliced}" data-type="replace" data-id="{$BREAD_DATA[$smarty.section.options.index].id}" data-itemname="{$BREAD_DATA[$smarty.section.options.index].item_name}" data-price="{$BREAD_DATA[$smarty.section.options.index].item_price}" data-image="{$BREAD_DATA[$smarty.section.options.index].item_image}" type="radio" name="radiog_lite" id="radio{$smarty.section.options.index+1}" class="css-checkbox" />
                    <label for="radio{$smarty.section.options.index+1}" class="css-label">{$BREAD_DATA[$smarty.section.options.index].item_name}</label>
                  </li>
                {/section}                  
                </ul>
              </div>
