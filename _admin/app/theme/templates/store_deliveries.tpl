<div class="container">
    <div class="deliveries">
       <h1>DELIVERIES</h1>
       
       <div class="top-deliveries-wrapper">
          <div class="left">
              <table class="top-deliveries-table">
                <tbody><tr>
                  <td>NEW</td>
                  <td>{$count_new}</td>
                </tr>
                <tr>
                  <td>PRC</td>
                  <td>{$count_prc}</td>
                </tr>
                <tr>
                  <td>OUT</td>
                  <td>{$count_bag}</td>
                </tr>
                <tr>
                  <td>DEL</td>
                  <td>{$count_pick}</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td>{$total_delivery}</td>
                </tr>
              </tbody></table>
          </div>
          
          <div class="right">
          <form action="{$SITE_URL}storedeliveries/searchDeliveryData" name="delivery_search" method="POST">
            <div class="text-box-holder">
              <label>Store Location</label>
              <select name="store_id" id="store_id" class="select" disabled>
               <option value=" ">--Select--</option>
                 {foreach from=$stores key=k item=store}
          <option {if $store->id eq $storeId} selected {/if} value="{$store->id}">{$store->store_name}</option>
                {/foreach}
              </select>
            </div>
            <div class="text-box-holder">
              <label>Status</label>
              <select name="delivery_status" id="1" class="select">
              <option value="">--Select--</option>
              <option value="0">New</option>
                {foreach from=$getOrderSatusList key=k item=b}  
                <option value="{$b->id}" {if $del_status eq $b->id} selected {/if}>{$b->label}</option>
              {/foreach}    
              </select>
            </div>
            <div class="text-box-holder2">
              <label>Date</label>
              <input id="search_delivery_date" name="search_date" type="text"  placeholder="{$date}" value="{$date}" class="text-box">
              <a href="#" class="date-icon"></a>
            </div>
             <input type="submit" name="submit" value="SEARCH" style="margin-top: 15px;margin-left: 26px;">
             </form>
          </div>
         
       </div>
      {if $total_delivery ne 0 } 
       <div class="deliveries-table-detail-wrapper">
         <h2>NEW</h2>
         <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
          
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
            {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 0}
          {if ($v->timediff gt 45) && ($v->time eq 1) }  
          <tr style="background: #cef2cc;">
            {elseif ($v->timediff le 45) && ($v->timediff gt 30) && ($v->time eq 1)}
            <tr style="background:#fffcb5;">
              {elseif ($v->timediff le 30) && ($v->time eq 1)}
              <tr style="background:#fedfe4;">
                {else}
                <tr>
                {/if}
            <td>{$v->name}
              <h3>{$v->timeformat}</h3>
              <p>{$v->dayname}</p>
              <p>{$v->date}</p>
        {if $v->application}<p>From {$v->application}</p>{/if}
              <p><a href="#" id="{$v->order_number}" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p>{$v->address->name}<br>
              {$v->address->phone}<br>              
          {if $v->address->address1 ne ""}{$v->address->address1}</br>{/if}
          {if $v->address->address2 ne ""}{$v->address->address2}</br>{/if}
          {if $v->address->zip ne ""}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
        <p>({$item->qty}) {$item->name} </p>
        {if isset($item->modifiers) && $item->modifiers neq '' && $item->modifiers neq '0'}
        <div class="item-modifier">Modifiers: {$item->modifiers}</div>{/if}
        {if isset($item->spl_instructions) && $item->spl_instructions neq ''}
        <div class="item-modifier">Special Instructions: {$item->spl_instructions}</div>{/if}
      {/foreach}
             
            </td>
        
           {foreach from=$getOrderSatusList key=k item=b}  
                 {if $b->code ne 'Delivery_Cancel'}
                 <td><a id="{$v->order_number}" class="viewOrdDetStore" onclick="manageStoreDelivereis({$v->order_id},{$b->id}{if $b->id eq 5},{$v->order_number}{/if})" href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box1.png" alt="">
                 
                 </td>
                 {/if}
               {if $b->code eq 'Delivery_Cancel'}
               <td></td>
               {/if}
              
            {/foreach}
         
          </tr>  
         {/if}
       {/foreach}
          
          
        </tbody></table>
        <h2>PROCESSED</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
       
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
          {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 5}
           <tr>
            <td>{$v->name}
              <h3>{$v->timeformat}</h3>
              <p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p>{$v->address->name}<br>
              {$v->address->phone}<br>              
          {if $v->address->address1 ne ""}{$v->address->address1}</br>{/if}
          {if $v->address->address2 ne ""}{$v->address->address2}</br>{/if}
          {if $v->address->zip ne ""}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
        <p>({$item->qty}) {$item->name} </p>
        {if isset($item->modifiers) && $item->modifiers neq '' && $item->modifiers neq '0'}
          <div class="item-modifier">Modifiers: {$item->modifiers}</div>
        {/if}
        {if isset($item->spl_instructions) && $item->spl_instructions neq ''}
          <div class="item-modifier">Special Instructions: {$item->spl_instructions}</div>
        {/if}
      {/foreach}
             
            </td>
        
        
             {foreach from=$getOrderSatusList key=k item=b}  
                {if $v->order_status eq 5 && $b->id eq 5}
                <td><a onclick="manageStoreDelivereis({$v->order_id},0)" href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
              {/if}
              {if $b->code ne 'Delivery_Cancel' && $b->code ne 'Delivery_Pro'}
                <td style="position: relative;"><a onclick="manageStoreDelivereis({$v->order_id},{$b->id},'',{$v->order_status})" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></a>
                  <div class="assign-store-order" id="storedelivery_user_assign_{$v->order_id}" style="display: none;">
                  <i class="close-btn" id="close-storeassign-user_{$v->order_id}">close</i>
                  <div class="assign-title">
                    <h1>ASSIGN ORDER</h1>
                  </div>
                  <div class="asign-order-select">
                    <select class="storedeliveryUserOptions_{$v->order_id}"> 
                      <option value="0">SELECT</option>
                    </select>
                  </div>
                  <div class="assign-submit">
                    <button class="assign_storedel_user" data-orderid= "{$v->order_id}">Assign</button>
                  </div>
                </div>
                </td>
              {/if}
             {if $b->code eq 'Delivery_Cancel'}
                 <td></td>
             {/if}
            {/foreach}
       
          </tr>  
         {/if}
       {/foreach}
         
          
        </tbody></table>
        <h2>OUT FOR DELIVERY</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>   
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
            {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 6}
          <tr>
            <td>{$v->name}
              <h3>{$v->timeformat}</h3>
              <p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p>{$v->address->name}<br>
              {$v->address->phone}<br>              
          {if $v->address->address1 ne ""}{$v->address->address1}</br>{/if}
          {if $v->address->address2 ne ""}{$v->address->address2}</br>{/if}
          {if $v->address->zip ne ""}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
        <p>({$item->qty}) {$item->name} </p>
        {if isset($item->modifiers) && $item->modifiers neq '' && $item->modifiers neq '0'}
          <div class="item-modifier">Modifiers: {$item->modifiers}</div>
        {/if}
        {if isset($item->spl_instructions) && $item->spl_instructions neq ''}
          <div class="item-modifier">Special Instructions: {$item->spl_instructions}</div>
        {/if}
      {/foreach}
             
            </td>
        
           {foreach from=$getOrderSatusList key=k item=b}  
              {if $v->order_status eq 6 && $b->id eq 6}
                  <td><a onclick="manageStoreDelivereis({$v->order_id},5)" href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a>
                  </td>
                   <td><a onclick="manageStoreDelivereis({$v->order_id},5)" href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a>
                   </td>
                {/if}
              {if $b->code ne 'Delivery_Cancel' && $b->code ne 'Delivery_Pro' && $b->code ne 'Delivery_out'}
             <td><a onclick="manageStoreDelivereis({$v->order_id},{$b->id})" href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
             {/if}
           {if $b->code eq 'Delivery_Cancel'}
           <td> </td>
           {/if}
              
            {/foreach}
         
          </tr>
          <tr class="no-border-up">
  <td></td>
  <td></td>
  <td></td>
<td><b class="bold">Assigned: </b></td>
  {if $v->delivery_assigned_to ne 0 }
  <td>{$v->delivery_uname}</td>
  <td>{$v->delivery_assigned_time}</td>
  {else}
  <td></td>
  <td></td>
  {/if}
  <td></td>


</tr>  
         {/if}
       {/foreach}
        
          
        </tbody></table>
        <h2>DELIVERED</h2>
        <table class="deliveries-new-table border-bottom">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
           
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
         {foreach from=$DeliverySortDataList key=k item=v}
          {if $v->order_status eq 7}
           <tr>
            <td>{$v->name}
              <h3>{$v->timeformat}</h3>
              <p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
              <p>{$v->address->name}<br>
              {$v->address->phone}<br>              
          {if $v->address->address1 ne ""}{$v->address->address1}</br>{/if}
          {if $v->address->address2 ne ""}{$v->address->address2}</br>{/if}
          {if $v->address->zip ne ""}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
        <p>({$item->qty}) {$item->name} </p>
        {if isset($item->modifiers) && $item->modifiers neq '' && $item->modifiers neq '0'}
          <div class="item-modifier">Modifiers: {$item->modifiers}</div>
        {/if}
        {if isset($item->spl_instructions) && $item->spl_instructions neq ''}
          <div class="item-modifier">Special Instructions: {$item->spl_instructions}</div>
        {/if}
      {/foreach}
             
            </td>
        
        <td><a onclick="manageStoreDelivereis({$v->order_id},6)" href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="manageStoreDelivereis({$v->order_id},6)" href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="manageStoreDelivereis({$v->order_id},6)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td></td>
          </tr>  
          <tr class="no-border-up">
  <td></td>
  <td></td>
  <td></td>
<td><b class="bold">Delivered: </b></td>
 {if $v->delivery_assigned_to ne 0 }
  <td>{$v->delivery_uname}</td>
  <td>{$v->order_delivered}</td>
  {else}
  <td></td>
  <td></td>
  {/if}
  <td></td>


</tr>
         {/if}
       {/foreach}
        
          
        </tbody></table>
       </div>
       {/if}
      {if $total_delivery eq 0 }<div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div>{/if}
    </div>
  </div>
