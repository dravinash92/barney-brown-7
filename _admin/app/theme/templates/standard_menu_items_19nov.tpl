<div class="container">

      <div class="standard-menu"><div class="condiment">
        <h1>STANDARD MENU ITEMS</h1>
        <form name="newbread" method="post" action="{$SITE_URL}menu/addStandardCategoryProducts" enctype="multipart/form-data">        
     <ul>
        <li>
          <label>Product Name</label>
          <input style="width:250px;" type="text" name="product_name" value="">
        </li>
 
        <li>
          <label>Description</label>
          <textarea style="width:250px;" name="description"></textarea>
        </li>

        <li>
          <label>Product Image</label>
          <input style="width:250px;" type="file"  name="product_image">
        </li>
       
        <li>
          <label>Price</label>
          <input type="text" value="" name="product_price" class="price-menu">
        </li>
          
        <li style="float:left">
          <label for="taxable" style="width:45px;">Taxable</label>
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="taxable" name="taxable" class="price-menu" {if $data.taxable eq 1} checked {/if} >
        </li>  
      
      </ul>
     <ul>
        <li>
           <div class="radio-section-extra" style="margin-left:3px;margin-right:2px;">
            <span class="multi-left">
               <input style="margin:auto;" value="1" type="checkbox" id="checkbox-3" class="input-checkbox" name="is_extra">
               <label for="checkbox-3" class="multisel-ckeck"></label>
            </span>
            <span class="radio-text">Extras</span></div>
        </li>
        <li>
         <label>Descriptor</label>
         <input type="text" name="descriptor[]" class="descriptor" value="">
         <input type="hidden" name="descriptor_id[]" value="" />
        </li>
        
        <li>
         <div class="label"><label class="name">Name</label><label class="extra-cost">Extra Cost</label><label class="default">Default</label> </div>
         <div class="input">
           <input type="text" class="name" name="extra_name[]" value="">
            <input type="text" class="extra-cost" name="extra_cost[]"  value="">
            <input type="hidden" name="extra_id[]" value="" />
            <div class="radio-section">
             <input type="radio" value="" class="input-radio"  name="extra_default"  id="dafault1">
             <label for="dafault1"></label>
            </div>
         </div>
        </li>
        
       
        
      </ul>
      <a href="#" class="add-item extras_more">add item</a>

      <input type="hidden" name="hidden_id" value="{$id}">
       <div class="button">
         <input type="submit" class="save save_standard_product" value="SAVE">
        <a href="{$SITE_URL}menu" class="cancel">CANCEL</a>
    </div>
   </div></div> 
 </form>
    </div>
