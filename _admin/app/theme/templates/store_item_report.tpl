<div class="container">
    <div class="item-report">
      <h1>ITEM REPORT</h1>
      
      <div class="filter_wrap">
      <form name="items_sales_report_filter">
      <div class="store-location">
         <label>Store Location</label>
        <select name="pickup_store" disabled>
           <option value="">Select Store</option>
			{foreach from=$pickupstores key=k item=v}
				<option {if $storeId eq $v->id} selected {/if} value="{$v->id}">{$v->store_name}, {$v->address1}, {$v->address2}, {$v->zip}</option>
			{/foreach }
         </select>
        <ul>
          <li><label>Order Type</label></li>
          <li>
           <span class="multi-left">
          <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="order_type[]" value="1">
            <label for="checkbox-2-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Delivery</span> </li>
          <li>
            <span class="multi-left">
             <input type="checkbox" id="checkbox-5" class="input-checkbox" name="order_type[]" value="0">
            <label for="checkbox-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Pick-Up</span>
          </li>
        </ul>
        <ul>
          <li><label>Order Status</label></li>
           <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-3-1" class="input-checkbox" name="order_status[]" value="0">
            <label for="checkbox-3-1" class="multisel-ckeck"></label>
            </span> <span class="radio-text">New</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-2" class="input-checkbox" name="order_status[]" value="1">
            <label for="checkbox-3-2" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Processed</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-3" class="input-checkbox" name="order_status[]" value="2">
            <label for="checkbox-3-3" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Ready</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-4" class="input-checkbox" name="order_status[]" value="6">
            <label for="checkbox-3-4" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Out</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-5" class="input-checkbox" name="order_status[]" value="7">
            <label for="checkbox-3-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Received</span>
          </li>
        </ul>
      </div>
      
       <div class="date-time-settings">
         <label>Date/Time Settings</label>
         <ul>
           <li>
            <div class="radio-left">
             <div class="radio-section">
                <input type="radio" value="order_date" checked class="input-radio" name="order_date_filter" id="1">
                <label for="1"></label>
              </div>
              <span class="radio-text">Order Placement</span>
             </div>
             <div class="radio-right">
              <div class="radio-section">
                  <input type="radio" value="order_delivered" class="input-radio" name="order_date_filter" id="2">
                <label for="2"></label>
                </div>
                <span class="radio-text">Order Fulfillment</span>
              </div>
           </li>
            <li>
             <label>From</label>
          <input type="text" class="expiration" placeholder="Select date" name="from_date" value="{$from_date}"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar.png"></a>
            <select name="from_time" name="from_time">
				<option value="">Select Time</option>
				<option value="9:00">9:00 AM</option>
                <option value="13:00">1:00 PM</option>
                <option value="16:00">4:00 PM</option>
                <option value="20:00">8:00 PM</option>
             </select>
           </li>
            <li>
             <label>To</label>
          <input type="text" class="expiration" placeholder="Select date" name="to_date" value="{$to_date}"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar.png"></a>
            <select class="to_time" name="to_time">
				<option value="">Select Time</option>
				<option value="9:00">9:00 AM</option>
                <option value="13:00">1:00 PM</option>
                <option value="16:00">4:00 PM</option>
                <option value="20:00">8:00 PM</option>
             </select>
           </li>
         </ul>
      </div>
      
      <div class="type-purchase">
        <ul>
          <li><label>Type of Purchase</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-4-5" class="input-checkbox" name="type_purchase[]" value="0">
            <label for="checkbox-4-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Website</span> </li>
            
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-8" class="input-checkbox" name="type_purchase[]" value="1">
            <label for="checkbox-3-8" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Phone Order</span>
          </li>
          <li></li>
          <li>
             <label>System User</label>
            <select name="system_user">
              <option value="">Select User</option>
				{foreach from=$systemusers key=k item=v}
					<option value="{$v->uid}">{$v->first_name} {$v->last_name}</option>
				{/foreach }
             </select>
             <a href="javascript:void(0)" class="do_store_item_report_filter"><img width="26" height="26" alt="" src="{$SITE_URL}app/images/link.png"></a>
          </li>           
        </ul>        
      </div>
      </form>
      </div>
      <div class="reports_table">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="sandwiches">
        <tbody><tr>
          <th width="65%">CUSTOM SANDWICHES</th>
           <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        <tr class="subheader">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><span class="hide_totalqty"></span></td>
          <td>$<span class="hide_subtottal"></span></td>
          <td>100%</td>
        </tr>
        <tr class="items">
          <td colspan="5">BREADS</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 1}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}</td>
           <td>${$v->item_price}
           {assign var=bprice value=$v->item_price}
           </td>
          <td>
     
          {foreach from=$breadsanddata key=bk item=bv}
          {if $v->item_name eq $bv.itemname}
          {assign var=bqty value=$bv.qty}
           {$bqty}
           {assign var="sum_qty_bread" value=$sum_qty_bread+$bqty}
          {/if}
          {/foreach}
          
         
          
          </td>
          <td>
         {foreach from=$breadsanddata key=bk item=bv}
          {if $v->item_name eq $bv.itemname}
          {assign var=breads_subtotal value=$bqty*$bprice}
          ${$breads_subtotal|string_format:"%.2f"}
          {assign var="sum_all_breads" value=$sum_all_breads+$breads_subtotal}
          {/if}
          {/foreach}
          </td>
		   <td>
		   
		 {foreach from=$breadsanddata key=bk item=bv}
          {if $v->item_name eq $bv.itemname}
          {assign var=bqty value=$bv.qty}
         
              {math equation="(( x / y ) * z )" x=$bqty y=$salesCount z=100 format="%.2f"}%
           {assign var="sum_qty_bread" value=$sum_qty_bread+$bqty}
          {/if}
          {/foreach}
		   
		   </td>
        </tr>
        {/if}
		 {/foreach}
		        
         <tr class="items">
          <td colspan="5">PROTEINS</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 2}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}
          </td>
           <td>${$v->item_price}
           {assign var=pprice value=$v->item_price}
           </td>
          <td>
          {foreach from=$proteinsanddata key=pk item=pv}
          {if $v->item_name eq $pv.itemname}
          {assign var=pqty value=$pv.qty}
          {$pqty}
          {assign var="sum_qty_protein" value=$sum_qty_protein+$pqty}
          {/if}
          {/foreach}
         </td>
          <td>
          {foreach from=$proteinsanddata key=pk item=pv}
          {if $v->item_name eq $pv.itemname}
          {assign var=protein_subtotal value=$pqty*$pprice}
          ${$protein_subtotal|string_format:"%.2f"}
          {assign var="sum_all_protein" value=$sum_all_protein+$protein_subtotal}
           {/if}
          {/foreach}
          </td>
		   <td></td>
        </tr>
		{/if}
		 {/foreach}
				
		<tr class="items">
          <td colspan="5">CHEESES</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 3}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}</td>
           <td>${$v->item_price}
           {assign var=cprice value=$v->item_price}
           </td>
          <td>{foreach from=$cheesesanddata key=ck item=cv}
          {if $v->item_name eq $cv.itemname}
           {assign var=cqty value=$cv.qty}
          {$cqty}
           {assign var="sum_qty_cheese" value=$sum_qty_cheese+$cqty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$cheesesanddata key=ck item=cv}
          {if $v->item_name eq $cv.itemname}
          {assign var=cheese_subtotal value=$cqty*$cprice}
          ${$cheese_subtotal|string_format:"%.2f"}
          {assign var="sum_all_cheese" value=$sum_all_cheese+$cheese_subtotal}
           {/if}
          {/foreach}
          </td>
		   <td></td>
        </tr>
		{/if}
		{/foreach}
				
		<tr class="items">
          <td colspan="5">TOPPINGS</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 4}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}</td>
           <td>${$v->item_price}
           {assign var=tprice value=$v->item_price}
           </td>
          <td>{foreach from=$toppingssanddata key=tk item=tv}
          {if $v->item_name eq $tv.itemname}
          {assign var=tqty value=$tv.qty}
          {$tqty}
          {assign var="sum_qty_toppings" value=$sum_qty_toppings+$tqty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$toppingssanddata key=tk item=tv}
          {if $v->item_name eq $tv.itemname}
          {assign var=toppings_subtotal value=$tqty*$tprice}
          ${$toppings_subtotal|string_format:"%.2f"}
          {assign var="sum_all_toppings" value=$sum_all_toppings+$toppings_subtotal}
           {/if}
          {/foreach}
          </td>
		   <td>
		   
		   {foreach from=$toppingssanddata key=tk item=tv}
          {if $v->item_name eq $tv.itemname}
          {assign var=tqty value=$tv.qty}
          {math equation="(( x / y ) * z )" x=$tqty y=$salesCount z=100 format="%.2f"}%
          {assign var="sum_qty_toppings" value=$sum_qty_toppings+$tqty}
          {/if}
          {/foreach}
		   
		   </td>
        </tr>
		{/if}
		{/foreach}
		
		<tr class="items">
          <td colspan="5">CONDIMENTS</td>
        </tr>
		{foreach from=$sandwichitems key=k item=v}
		{if $v->category_id eq 5}
		 <tr class="{cycle values="odd,even"} datarow">
          <td>{$v->item_name}</td>
           <td>${$v->item_price}
           {assign var=coprice value=$v->item_price}
           </td>
          <td>{foreach from=$condisanddata key=cok item=cov}
          {if $v->item_name eq $cov.itemname}
          {assign var=coqty value=$cov.qty}
          {$coqty}
           {assign var="sum_qty_condi" value=$sum_qty_condi+$coqty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$condisanddata key=cok item=cov}
          {if $v->item_name eq $cov.itemname}
          {assign var=condi_subtotal value=$coqty*$coprice}
          ${$condi_subtotal|string_format:"%.2f"}
           {assign var="sum_all_condi" value=$sum_all_condi+$condi_subtotal}
          {/if}
          {/foreach}</td>
		   <td>{foreach from=$condisanddata key=cok item=cov}
          {if $v->item_name eq $cov.itemname}
          {assign var=coqty value=$cov.qty}
           {math equation="(( x / y ) * z )" x=$coqty y=$salesCount z=100 format="%.2f"}%
           {assign var="sum_qty_condi" value=$sum_qty_condi+$coqty}
          {/if}
          {/foreach}</td>
        </tr>
		{/if}
		{/foreach}
		{assign var="tottal_subtottal" value=$sum_all_condi+$sum_all_breads+$sum_all_protein+$sum_all_cheese+$sum_all_toppings}
		{assign var="tottal_qty" value=$sum_qty_bread+$sum_qty_protein+$sum_qty_cheese+$sum_qty_toppings+$sum_qty_condi}
    	
      </tbody></table>
	  
	   <table width="100%" cellspacing="0" cellpadding="0" border="0" class="products">
        <tbody><tr>
          <th width="65%">STANDARD ITEMS</th>
          <th width="10%">PRICE</th>
          <th width="5%">QTY</th>
          <th width="10%">SUBTOTAL</th>
          <th width="10%">% SALES</th>
        </tr>
        
        {foreach from=$standardCatagory key=sc item=sci}
        <tr class="subheader {$sc}">
          <td>{$sci->category_identifier}</td>
          <td></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-item-{$sc}"></span></td>
          <td><span class="hide_salades_subtottal" data-id="#st-cat-{$sc}"></span></td>
          <td></td>
        </tr>
        {assign var="sum_all_salades" value=0}
        {assign var="sum_all_salades_count" value=0}
        
		{foreach from=$standareditems key=k item=v}
		{if $v->standard_category_id eq $sci->id}
		<tr class="{cycle values="odd,even"} datarow st-cat-{$sc}">
          <td>{$v->product_name}</td>
          <td>${$v->product_price|string_format:"%.2f"}
          {assign var=price value=$v->product_price}
          </td>
          <td>{foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {$sv->qty}
          {assign var=qty value=$sv->qty}
          {/if}
          {/foreach}</td>
          <td>
          {foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {assign var=salades_subtotal value=$qty*$price}
          ${$salades_subtotal|string_format:"%.2f"}
           {assign var="sum_all_salades" value=$sum_all_salades+$salades_subtotal}
           {assign var="sum_all_salades_count" value=$sum_all_salades_count+$qty}
           {/if}
          {/foreach}
           </td>
		   <td> 
		   
		   {foreach from=$standareditemsqty key=sk item=sv}
          {if $v->id eq $sv->item_id}
          {assign var="stpq" value=$sv->qty}
          {math equation="(( x / y ) * z )" x=$stpq y=$salesCount z=100 format="%.2f"}%
          {assign var=qty value=$sv->qty}
          {/if}
          {/foreach}
		   
		   </td>
        </tr>
		{/if}
		{/foreach}
		
		{assign var="tottal_subtottal" value=$tottal_subtottal+$sum_all_salades}
		{assign var="tottal_qty" value=$tottal_qty+$sum_all_salades_count}
		
		<input type="hidden" id="st-cat-{$sc}"  value="{$sum_all_salades|string_format:'%.2f'}"/>
		<input type="hidden" id="st-cat-item-{$sc}" value="{$sum_all_salades_count}"/>
        	
        {/foreach}	
		
	   </tbody>
	   </table>
	   </div>
		 <span class="tottal_subtottal">{$tottal_subtottal|string_format:"%.2f"}</span>
    	 <span class="tottal_qty">{$tottal_qty}</span>
    </div>
  </div>
