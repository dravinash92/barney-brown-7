<div class="container">
      <div class="standard-menu">
           <div class="standard-menu"><div class="condiment">
        <h1>STANDARD MENU ITEMS</h1>
        <form name="newbread" method="post" action="{$SITE_URL}menu/updateStandardCategoryProducts" enctype="multipart/form-data"> 
        <input type="hidden" name="old_product_image" value="{$data.product_image}" />
       
        <ul>
        <li>
          <label>Product Name</label>
          <input  style="width:250px;" type="text" name="product_name" value="{$data.product_name}">
        </li>
      
        <li>
          <label>Description </label>
          <textarea style="width:250px;" name="description">{$data.description}</textarea>
        </li>

        <li>
          <label>Product Image</label>
          <input style="width:250px;" type="file" name="product_image">           
        </li>
         
         {if $data.product_image }
          <li style="float:left">
            <img src="{$SITE_URL}upload/products/{$data.product_image}" width="100" height="100">
          </li>
          {/if}
       
         
        <li>
          <label>Price</label>
          <input type="text" value="{$data.product_price}" name="product_price" class="price-menu">
        </li>  
        
        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="taxable" name="taxable" class="price-menu" {if $data.taxable eq 1} checked {/if} >
          <label for="taxable" style="width:45px;">Taxable</label>
        </li>

        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="allow_spcl_instruction" name="allow_spcl_instruction" class="price-menu" {if $data.allow_spcl_instruction eq 1} checked {/if} >
          <label for="allow_spcl_instruction" style="width:150px;">Allow Special Instructions</label>
        </li>

        <li style="float:left">
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="add_modifier" name="add_modifier" class="price-menu" {if $data.add_modifier eq 1} checked {/if} >
          <label for="add_modifier" style="width:100px;">Add Modifier</label>
        </li>  
      
      </ul>

     <ul class="item-description">
        <div class="description-wrap-item">
          <div class="description-left">

            <div class="description-main">
              <label>Description</label>
              <input type="text" name="modifier_desc" class="descriptor" placeholder="Add a Protein" value="{$data.modifier_desc}">
            </div>
             <div class="radio-box">
        <div class="description-radio_options">
<p class="radio-label-itm">Optional or Required</p>
          <div class="input">
            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_isoptional" id="optional" {if $data.modifier_isoptional eq yes} checked {/if} value="yes">
             <label for="optional">Optional</label>
            </div>

            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_isoptional" id="required" {if $data.modifier_isoptional eq no} checked {/if} value="no">
             <label for="required">Required</label>
            </div>
          </div>
      </div>

      <div class="description-radio_options">
        <p class="radio-label-itm">Amount to Choose</p>
          <div class="input">
            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_is_single" id="limited" {if $data.modifier_is_single eq yes} checked {/if} value="yes">
             <label for="limited">Choose One</label>
            </div>

            <div class="radio_desc_wrap">
             <input type="radio" name="modifier_is_single" id="unlimited" {if $data.modifier_is_single eq no} checked {/if} value="no">
             <label for="unlimited">Choose Unlimited</label>
            </div>
          </div>
      </div>
      </div>
          </div> 

          <div class="description-right">
         <div class="description-right-wrap">
            <div class="description-main add-more-opt">
              <label>Options</label>
              {if isset($modifier_options) && $modifier_options != ''}
                {foreach from=$modifier_options key=k item=optprc}
                <input type="text" name="option[]" class="option_price" id="add_remove" placeholder="" value="{$optprc.option}">
                {/foreach}
              {else}
                <input type="text" name="option[]" class="option_price" id="add_remove" placeholder="" value="">
              {/if}
            </div>
            <div class="description-main description-small-feild add-more-prc">
              <label>Price</label>
              {if isset($modifier_options) && $modifier_options != ''}
                {foreach from=$modifier_options key=k item=optprc}
                <input type="text" name="price[]" class="option_price" id="add_remove" placeholder="" value="{$optprc.price}">
                {/foreach}
              {else}
                <input type="text" name="price[]" class="option_price" id="add_remove" placeholder="" value="">
              {/if}
            </div>
            <div class="remove-btn add-more-rem">
              {if isset($modifier_options) && $modifier_options != ''}
                {foreach from=$modifier_options key=k item=optprc}
                <label></label>
                <p class="rem_add_more">remove</p>  
                {/foreach} 
              {else}
                <label></label>
                <p class="rem_add_more">remove</p>  
              {/if}          
            </div>
          </div>
            <div class="desc-additm">
            <p class="add_more">add item</p>
          </div>
          </div>

        </div>
        </ul>
      <input type="hidden" name="hidden_id" value="{$data.id}" />
       <div class="button">
         <input type="submit" class="save save_standard_product" style="cursor: pointer;" value="SAVE">
        <a href="{$SITE_URL}menu" class="cancel">CANCEL</a>
    </div>
   </div></div> 
 </form>
    </div>
    
 
