<div class="container">
    <div class="user-account">
       <h1>EDIT STORE ACCOUNT</h1>
       <form action="{$SITE_URL}accounts/updateStoreAccount" method="POST">
          <label>Store Name</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->store_name}" name="store_name">
          <label>Store Address Line One</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->address1}" name="address1">
          <label>Store Address Line Two</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->address2}" name="address2">
          <label>Store Zipcode</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->zip}" name="zip">
          <label>Username</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->username}" name="username" disabled>
          <label>Password</label>
          <input type="password" name="password">
          <label>Confirm Password</label>
          <input type="password" value="" name="con_password">
          <label>Reports Pin</label>
          <input type="text" value="{$store_accounts_data->pickup_stores->report_pin}" name="report_pin">
     <span class="multi-left">
               <input type="checkbox" id="checkbox-2-0" class="input-checkbox" name="ip_checked" value="" {if $store_accounts_data->pickup_stores->restrict_ip ne ""} checked {/if}>
          <label for="checkbox-2-0" class="multisel-ckeck"></label>
         </span><label class="restrict">Restrict IP Access to:</label>
          <input type="text" name="restrict_ip" value="{$store_accounts_data->pickup_stores->restrict_ip}">
          
                   
           <input type="hidden" name="id" value="{$store_accounts_data->pickup_stores->id}" />
          
          
          <button type="button" class="save_store_account">SAVE STORE</button>
          <a class="cancel" href="{$SITE_URL}accounts">CANCEL</a>
      
       
       <div class="pick-up"  style="margin-left: 312px; width: 368px; height: 623px; margin-top: -578px;">
         <h3>Pick-Up / Delivery Windows</h3>
          <ul>
           <li class="radio"></li>
           <li class="week">&nbsp;</li>
           <li class="from">From</li>
           <li class="to">To</li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="day0" value="Sunday" {if $store_timings_day[0] eq 'Sunday'} checked {/if}>
               <label for="checkbox-2-1" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Sunday</li>
           <li class="from"> 
             <select name="day_open0">
              <option value="10:00" {if $store_timings_open[0] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_open[0] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
           <li class="to">
             <select name="day_close0">
              <option value="10:00" {if $store_timings_close[0] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_close[0] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
         </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-2" class="input-checkbox" name="day1" value="Monday" {if $store_timings_day[1] eq 'Monday'} checked {/if}>
               <label for="checkbox-2-2" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Monday</li>
           <li class="from"> 
             <select name="day_open1">
				<option value="10:00" {if $store_timings_open[1] eq '10:00'} selected {/if}>10:00AM</option>
				<option value="22:00" {if $store_timings_open[1] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
           <li class="to">
             <select name="day_close1">
               <option value="10:00" {if $store_timings_close[1] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_close[1] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
         </ul>
          <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-3" class="input-checkbox" name="day2" value="Tuesday" {if $store_timings_day[2] eq 'Tuesday'} checked {/if}>
               <label for="checkbox-2-3" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Tuesday</li>
           <li class="from"> 
             <select name="day_open2">
             <option value="10:00" {if $store_timings_open[2] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_open[2] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
           <li class="to">
             <select name="day_close2">
                 <option value="10:00" {if $store_timings_close[2] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_close[2] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-4" class="input-checkbox" name="day3" value="Wednesday" {if $store_timings_day[3] eq 'Wednesday'} checked {/if}>
               <label for="checkbox-2-4" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Wednesday</li>
           <li class="from"> 
             <select name="day_open3">
				<option value="10:00" {if $store_timings_open[3] eq '10:00'} selected {/if}>10:00AM</option>
				<option value="22:00" {if $store_timings_open[3] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
           <li class="to">
             <select name="day_close3">
                 <option value="10:00" {if $store_timings_close[3] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_close[3] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="day4" value="Thursday" {if $store_timings_day[4] eq 'Thursday'} checked {/if}>
               <label for="checkbox-2-5" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Thursday</li>
           <li class="from"> 
             <select name="day_open4">
				<option value="10:00" {if $store_timings_open[4] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_open[4] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
           <li class="to">
             <select name="day_close4">
                <option value="10:00" {if $store_timings_close[4] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_close[4] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-6" class="input-checkbox" name="day5" value="Friday" {if $store_timings_day[5] eq 'Friday'} checked {/if}>
               <label for="checkbox-2-6" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Friday</li>
           <li class="from"> 
             <select name="day_open5">
				<option value="10:00" {if $store_timings_open[5] eq '10:00'} selected {/if}>10:00AM</option>
				<option value="22:00" {if $store_timings_open[5] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
           <li class="to">
             <select name="day_close5">
				  <option value="10:00" {if $store_timings_close[5] eq '10:00'} selected {/if}>10:00AM</option>
				<option value="22:00" {if $store_timings_close[5] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
         </ul>
         <ul>
           <li class="radio">
             <span class="multi-left">
               <input type="checkbox" id="checkbox-2-7" class="input-checkbox" name="day6" value="Saturday" {if $store_timings_day[6] eq 'Saturday'} checked {/if}>
               <label for="checkbox-2-7" class="multisel-ckeck"></label>
              </span>
           </li>
           <li class="week">Saturday</li>
           <li class="from"> 
             <select name="day_open6">
				<option value="10:00" {if $store_timings_open[6] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_open[6] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
           <li class="to">
             <select name="day_close6">
				  <option value="10:00" {if $store_timings_close[6] eq '10:00'} selected {/if}>10:00AM</option>
              <option value="22:00" {if $store_timings_close[6] eq '22:00'} selected {/if}>10:00PM</option>
             </select>
           </li>
         </ul>
         
         <div class="deactivate">
            <ul>
              <li>
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-8" class="input-checkbox" name="" value="">
                <label for="checkbox-2-8" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Pick-Ups</label>
              </li>
              <li>
               <span class="multi-left">
                  <input type="checkbox" id="checkbox-2-9" class="input-checkbox" name="" value="">
                <label for="checkbox-2-9" class="multisel-ckeck"></label>
               </span>
                <label>Deactivate Deliveries</label>
              </li>
            </ul>
         
         </div>
         
       </div>
       
       <div class="zipcode-wrapper" style="margin-left: 708px; width: 261px; margin-bottom: 0px; border-bottom-width: 0px; margin-top: -624px;">
         <h3>Pick-Up / Delivery Windows</h3> 
           <div class="zip_wrap">
         <ul class="heading">
           <li class="zip-code">Zip Code</li>
           <li class="abbr">Abbr.</li>
         </ul>
        {foreach from=$store_accounts_data->store_zipcodes key=k item=v}
			<ul>
			   <li class="zip-code"><input type="text"  value="{$v->zipcode}" name="zipcode{$k+1}"></li>
			   <li class="abbr"><input type="text" value="{$v->abbreviation}" name="abbreviation{$k+1}"></li>
			</ul>
		{/foreach}
		</div>
		<input type="hidden" name="zipcount" id="zipcount" value="{$store_accounts_data->store_zipcodes|@count}" />		
        
		  <a class="add zip_add" href="javascript:void(0)" style="margin-top: 41px; margin-right: 0px; margin-left: -175px;">add</a> 
         <!--<a class="add" href="#" style="margin-top: 41px; margin-right: 0px; margin-left: -175px;">add</a>-->
         
       </div>
       </form>
    </div>
  </div>
 
