<div class="container">
    <div class="store-accounts">
       <h1>STORE ACCOUNTS</h1>
       <form id="updateStoreOrder" action="{$SITE_URL}accounts/updateStoreOrder" method="post">
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="9%">&nbsp;</th>
          <th colspan="1">&nbsp;</th>
          <th colspan="3">STORE</th>
        </tr>
          {foreach from=$store_accounts key=k item=v}
        <tr>
          <td width="9%" align="center">
          <span class="multi-left">
               <input type="checkbox" id="checkbox-{$key}-{$v->id}" class="input-checkbox" name="live[{$v->id}]" value="1" {if $v->live eq 1} checked {/if}>
             <label for="checkbox-{$key}-{$v->id}" class="multisel-ckeck"></label>
            </span>
          </td>
          <td width="16%"><input type="text" value="{$v->ordered}" class="field" name="ordered[{$v->id}]"></td>
          <td width="64%">{$v->store_name}</td>
          <td width="4%"><a href="{$SITE_URL}accounts/editStoreAccount/{$v->id}">edit</a></td>
          <td width="7%"><a href="{$SITE_URL}accounts/deleteStoreAccount/{$v->id}/{$v->uid}">delete</a></td>
        </tr>
     {/foreach}
      </tbody></table>
      
       <a class="update" href="#" onclick="document.getElementById('updateStoreOrder').submit()">update</a>
      <a class="add-user" href="{$SITE_URL}accounts/newStoreUser/">ADD STORE</a>
      </form>  
        
    </div>
  </div>
