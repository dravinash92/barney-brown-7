         <div class="order-history">
         {if $orderhistory|@count gt 0} 
		        
         {foreach from=$orderhistory key=id item=history}
			<ul class="order-history-inner">
	            <li> <span class="section1">
	              <h3>{$history->delivery_type}</h3>
	              <h4>{$history->date}</h4>
	              </span> </li>
	            <li> <span class="section2">
	              <h3>{$history->meal_name}</h3>
	              <p>
					{foreach from=$history->items key=id item=item}  
						({$item->qty}) {$item->name} <br/>
					{/foreach}
	              </p>
	              </span> </li>
	            <li> <span class="section3">
	              <h3>${$history->total}</h3>
	              </span> </li>
	            <li> <span class="section4"></span> </li>
			</ul>			
         {/foreach}
         
        {else}
             <ul class="order-history-inner">
				
	            <li style="width:100%;"><span><h3 class="no_order_history">No Previous Orders.</h3></span></li>	            
	        </ul>    
			
         {/if}
        </div>
      </div>
</div>
