        <div class="saved-address-wrapper">
          <ul class="saved-address-inner">
            <li> <span class="add-new-address-main"> <a href="#" data-id="0" class="delivery-address add-credit-card"><span>+</span><br />
              ADD NEW<br />
              CREDIT CARD</a> </span> </li>
           
            
            {foreach from=$card_details key=k item=card}			
				<li>
					<span class="address-content">
						<h3 style="text-transform: uppercase;">{$card->card_type}</h3>
						<p style="text-transform: capitalize;">{$card->first_name} {$card->last_name}<br />
							{$card->card_type}<br/>
							XXXX-XXXX-XXXX-{$card->card_number|substr:-4}<br />
							{$card->card_zip}
						</p>
					</span> <span class="button-holder-address"> <a href="javascript:void(0)" data-id="{$card->id}" class="edit edit_card">EDIT</a> <a href="javascript:void(0)" data-id="{$card->id}" class="remove remove_card">remove</a> </span>
				</li>
			{/foreach}
            
          </ul>
        </div>
      </div>
  </div>

