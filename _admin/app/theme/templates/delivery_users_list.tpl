<div class="container">
    <div class="admin-user-account showdel-head">
       <h1>DELIVERY ACCOUNTS</h1>
    <form class="showdel-form" >
      <!-- <form method="post" class="showdel-form" action="{$SITE_URL}accounts/deliveriesByDate"> -->
            <div class="input-wrap_date">
        <div class="input-main">
        <input type="hidden" name="uid" value="{$uid}">
             <label>From</label>
          <input type="text" class="expiration" placeholder="Select date" name="from_date" id="from_date" value="{$from_date}"> 

          <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar.png"></a>
          

            <select name="from_time" id="from_time">
        {$fromTimes}
             </select>
           </div>
           <div>
             <label>To</label>
          <input type="text" class="expiration" placeholder="Select date" name="to_date" id="to_date" value="{$to_date}"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar.png"></a>
            <select class="to_time" id="to_time">
        {$toTimes}        
             </select>
           </div>
             <button id="searchDelUser" type="button"><img width="26" height="26" alt="" src="{$SITE_URL}app/images/link.png"></button>
             </div>
           </form>
      <div id="ajaxList"></div>  
      <div id="delUserList"> 
       <form id="updateDeliveryUsersLive" method="post" action="{$SITE_URL}accounts/updateDeliveryUsersLive">
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="7%">&nbsp;</th>
          <th>USER</th>
          <th>ASSIGNED STORES</th>
          <th># DELIVERIES</th>
          <th> ORDER TOTALS</th>
          <th colspan="2"></th>
        </tr>
      {assign var =key value=1 }
        {section name=options start=0 loop=$delivery_users|@count step=1 }
        <tr>
          <td width="7%" align="center">
              <span class="multi-left">
               <input type="checkbox" id="checkbox-{$key}-{$delivery_users[$smarty.section.options.index].uid}" class="input-checkbox" name="live[{$delivery_users[$smarty.section.options.index].uid}]" value="1" {if $delivery_users[$smarty.section.options.index].live eq 1} checked {/if}>
               <label for="checkbox-{$key}-{$delivery_users[$smarty.section.options.index].uid}" class="multisel-ckeck"></label>
              </span>
          </td>
          <td width="15%"><a href="{$SITE_URL}reports/showDeliveriesDetails/{$delivery_users[$smarty.section.options.index].uid}">{$delivery_users[$smarty.section.options.index].name}</a></td>
          <td width="18%">{$delivery_users[$smarty.section.options.index].store_names}</td>
          <td width="10%"><a href="{$SITE_URL}reports/showDeliveriesDetails/{$delivery_users[$smarty.section.options.index].uid}">{$delivery_users[$smarty.section.options.index].deliveries}</a></td>

          <td width="10%">{if $delivery_users[$smarty.section.options.index].totals ne ''} $ {$delivery_users[$smarty.section.options.index].totals} {else}$ 0.00{/if}</td>
          
          <td width="5%"><a href="{$SITE_URL}accounts/editDeliveryUser/{$delivery_users[$smarty.section.options.index].uid}">edit</a></td>
          <td width="5%"><a href="{$SITE_URL}accounts/deleteDeliveryUser/{$delivery_users[$smarty.section.options.index].uid}">delete</a></td>
        </tr>
         {assign var=key value=$key+1}
       {/section}

      </tbody></table>
      <a class="update" href="#" onclick="document.getElementById('updateDeliveryUsersLive').submit()">update</a>
      <a class="add-user" href="{$SITE_URL}accounts/delivery_users/">ADD NEW USER</a>
      </form></div> 

    </div>
  </div>
