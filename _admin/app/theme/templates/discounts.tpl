<div class="container">
     <div class="webpage">
       <h1>DISCOUNTS</h1>
       
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="8%">LIVE</th>
          <th width="14%">DISCOUNT NAME</th>
          <th width="12%">CODE</th>
          <th width="11%">DISCOUNT TYPE</th>
          <th width="6%">AMOUNT</th>
          <th width="11%">APPLICABLE TO</th>
          <th width="5%">MIN</th>
          <th width="6%">MAX</th>
          <th width="7%">TIMES USED</th>
          <th colspan="3">USES</th>
        </tr>
		<form name="discount_live" id= "discount_live" method="post" action="{$SITE_URL}discounts/updateIsLive" enctype="multipart/form-data">
		{foreach from=$discountList key=k item=data}       
			<tr class="{if $k%2 == 0}even{/if}">
			  <td>
				 <span class="multi-left">
			
				   <input type="checkbox" id="checkbox-2-{$k}" class="input-checkbox" name="live[]" value="{$data.id}" {if $data.is_live == 1}checked ="checked"{/if} >
				   <label for="checkbox-2-{$k}" class="multisel-ckeck"></label>
				</span>
			  </td>
			  <td>{$data.name}</td>
			  <td>{$data.code}</td>
			  <td>{if $data.type == 0}% Off{else}Fixed Amount{/if}</td>
			  <td>{if $data.type == 1}${/if}{$data.discount_amount}{if $data.type == 0}%{/if}</td>
			  <td>{if $data.applicable_to == 0}Sub-Total{else}Total{/if}</td>
			  <td>{if $data.minimum_order == 1}${$data.minimum_order_amount}{else}-{/if}</td>
			  <td>{if $data.maximum_order == 1}${$data.maximum_order_amount}{else}-{/if}</td>
			  <td>{$data.count}</td>
			  <td width="11%">{if $data.uses_type == 0}{$data.uses_amount}{else}Unlimited{/if}</td>
			  <td width="3%"><a href="{$SITE_URL}discounts/editDiscounts/{$data.id}">edit</a></td>
			  <td width="5%"><a href="{$SITE_URL}discounts/deleteItems/{$data.id}" onclick="return confirm('Are you sure you want to delete?')">remove</a></td>
			</tr>
		{/foreach}		

      </tbody></table>
      
      <input type="submit" value="Update">
  </form>     
      <a class="add-user" href="{$SITE_URL}Discounts/newDiscount">ADD DISCOUNT</a>
        
    </div>
  </div>
