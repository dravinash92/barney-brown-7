<div class="container">
    <div class="start-order">
       <h1>NEW ORDER</h1>
       <form class="new-order-form" action="{$SITE_URL}neworder/index/">
       	<div class="text-box-holder">
          <label>First Name</label>
          <input name="name" type="text" class="main-text-box common-text-width" value="{$name}">
          <a href="#" class="arrow">&nbsp;</a>
        </div>
       	<div class="text-box-holder">
          <label>Last Name</label>
          <input name="lastname" type="text" class="main-text-box common-text-width" value="{$lastname}">
          <a href="#" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Email Address</label>
          <input name="email" type="text" class="main-text-box common-text-width" value="{$email}">
          <a href="#" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Phone Number</label>
          <input name="" type="text" class="main-text-box phone-text-width">
          <input name="" type="text" class="main-text-box phone-text-width">
          <input name="" type="text" class="main-text-box phone-text-width">
          <input name="" type="text" class="main-text-box phone-text-width">
          <a href="#" class="arrow">&nbsp;</a>
        </div>                
       </form>
       
       <a href="{$SITE_URL}neworder/newuseraccount" class="new-account">New Account</a>
       
       
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <th width="22%">CUSTOMER NAME</th>
            <th width="32%">EMAIL</th>
            <th width="18%">PHONE</th>
            <th width="13%">SALES TOTAL</th>
          </tr>
           {section name=users loop=$users_data } 
          <tr>
            <td><a href="{$SITE_URL}neworder/addToCart/{$users_data[users].uid}">{$users_data[users].first_name} {$users_data[users].last_name}</a></td>
            <td>{$users_data[users].username}</td>
            <td></td>
            <td>${$users_data[users].total_price}</td>
          </tr>
		  	{/section}  
       </tbody></table>
       
    </div>
  </div>
