
  <h2> Choose Your Condiments </h2>
  <h3><!--    CHOOSE UP TO THREE CONDIMENTS. <br>  $0.50 PER EXTRA CONDIMENT. --> </h3>
  <div class="protein-select-wrapper scroll-bar " style="position: relative; overflow: visible;">
  
  <ul>




                
                {section name=options start=0 loop=$CONDIEMENTS_DATA|@count step=1 }
                
                
            
                
                  <li>
                    <input data-priority = "{$CONDIEMENTS_DATA[$smarty.section.options.index].image_priority}" data-empty="false" data-id="{$CONDIEMENTS_DATA[$smarty.section.options.index].id}"  data-price="{$CONDIEMENTS_DATA[$smarty.section.options.index].item_price}" data-image_trapezoid="{$CONDIEMENTS_DATA[$smarty.section.options.index].image_trapezoid}" data-image_round="{$CONDIEMENTS_DATA[$smarty.section.options.index].image_round}" data-image_long="{$CONDIEMENTS_DATA[$smarty.section.options.index].image_long}" data-itemname="{$CONDIEMENTS_DATA[$smarty.section.options.index].item_name}" type="checkbox" value="" name="check" id="condiment-check{$smarty.section.options.index+1}">
                    <label {if $CONDIEMENTS_DATA[$smarty.section.options.index].premium} class="premium-icon" {/if} for="condiment-check{$smarty.section.options.index+1}">{$CONDIEMENTS_DATA[$smarty.section.options.index].item_name}</label>
                  
                    {if $CONDIEMENTS_DATA[$smarty.section.options.index].options_id neq 0}
                    
                    <div class="sub-value"> <a href="#" class="left"></a>
                    <span class="text-box">
                    
                  
                     
                    {section name=subval start=0 loop=$CONDIEMENTS_DATA[$smarty.section.options.index].options_id|@count step=1 }

                    {assign var = "option_id" value=$CONDIEMENTS_DATA[$smarty.section.options.index].options_id[$smarty.section.subval.index].id }
                    
                    {assign var = "display" value = "none"    }
                    {assign var =  "current" value =  "false" }
                    
                    {if $smarty.section.subval.index eq 0   } 
                    
                    {assign var = "display" value =  "block"  }   
                    {assign  var = "current" value = "true"  }
                    
                    {/if}
                  
                    <input rel="slide" readonly style="display:{$display}"  data-unit="{$CONDIEMENTS_DATA[$smarty.section.options.index].options_id[$smarty.section.subval.index].option_unit}" data-current="{$current}" data-optionid="{$option_id}" data-image="" data-image_trapezoid="{$CONDIEMENTS_DATA[$smarty.section.options.index].option_images[$option_id].tImg}" data-image_round="{$CONDIEMENTS_DATA[$smarty.section.options.index].option_images[$option_id].rImg}" data-image_long="{$CONDIEMENTS_DATA[$smarty.section.options.index].option_images[$option_id].lImg}" data-price_mul="{$CONDIEMENTS_DATA[$smarty.section.options.index].options_id[$smarty.section.subval.index].price_mult}"  type="text" name="" class="text-box" value="{$CONDIEMENTS_DATA[$smarty.section.options.index].options_id[$smarty.section.subval.index].option_name}"/>
                    
                    {/section}
                     
                    </span>
            
                   <a href="#" class="right right-hover"></a> 
           </div> 
                    
                    {/if}
                  
                  </li>
                  
                 {/section}
                  
                  

                </ul>
               </div>
           