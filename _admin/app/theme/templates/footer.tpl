</div>

<!--Address Popup-->

<div class="popup-wrapper" id="add-new-address">
  <div class="add-new-address-inner"> <a href="#" class="close-button">Close</a>
	<form class="add_address" method="post" >
    <div class="title-holder">
      <h1>ADD NEW ADDRESS</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Recipient Name</p>
        <input name="recipient" type="text" class="text-box-control required" placeholder="Enter Recipient Name">
        </span> <span class="text-box-holder">
        <p>Company</p>
        <input name="company" type="text" class="text-box-control required" placeholder="Enter Company Name" required>
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Street Address</p>
        <input name="address1" type="text" class="text-box-control required" required >
        </span>  <span class="text-box-holder1">
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control required" required>
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" >
        </span> <span class="text-box-holder">
        <p>Phone Number</p>
        <input name="phone1" type="text" maxlength="3" class="text-box-phone" >
        <input name="phone2" type="text" maxlength="3" class="text-box-phone" >
        <input name="phone3" type="text" maxlength="4" class="text-box-phone margin-none" >
        </span> <span class="text-box-holder1">
        <p>Ext.</p>
        <input name="extn" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <h2>New York, NY</h2>
        <span class="text-box-zip-holder1">
        <p>Zip Code</p>
        <input name="zip" type="text" class="text-box-zip required" required >
        </span> </span>
        <h3>{$title_text} currently delivers in Midtown Manhattan from 23rd Street to 59th Street and from 3rd Avenue to 8th Avenue.</h3>
      </li>
      <li> <span class="delivery-instructions">
        <p>Delivery Instructions</p>
        <input name="delivery_instructions" type="text" class="text-box-delivery" >
        </span> </li>
      <li> <a href="javaScript:void(0)" class="add-address save_address">ADD ADDRESS</a> </li>
      <input type="hidden" name="uid" value="" />
    </ul>
    </form>
  </div>
</div>
<div class="popup-wrapper" id="ajax-loader" style="display: none;">
		<img src="{$SITE_URL}/app/images/ajax-loader.gif" />
</div>

<div class="popup-wrapper" id="edit-address">
  <div class="add-new-address-inner">
	
  </div>
</div>


<!--Address Popup-->

<!-- SOTD Popup-->
<div class="popup-wrapper" id="SOTD">
	<div class="add-new-address-inner"> <a href="#" class="close-button">Close</a>
		<div class="container">
			<form name="sotd" class="sotd" >
				<p>Please select a date to feature this sandwich.</p>
				<div class="text-box-holder">
					<input name="sotd_date" id="sotd_date" type="text" placeholder="{$date}" value="{$date}" class="text-box" readonly />
					<a class="calendar" href="#"><img class="calendarimg" width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar_new.png"></a>
				</div>  
				<div class="text-box-holder">
					<input type="hidden" name="id" id="sotd_sandwich_id" value="" />
					<input type="button" class="add_sotd" name="add_sotd" value="SCHEDULE">
				</div>
			</form>			
		</div>
	</div>
</div>

<!-- SOTD Popup-->

<!-- Card Popup-->
	<div class="popup-wrapper" id="credit-card-details">
  <div class="add-new-address-inner"> <a href="#" class="close-button">Close</a>
    <div class="title-holder">
      <h1>ADD NEW CREDIT CARD</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Credit Card</p>
        <span class="credit-card">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>Visa</option>
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Card Number</p>
        <input name="" type="text" class="text-box-control" placeholder="">
        </span> <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Expiration Date</p>
        <span class="month">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>01 - January</option>
        </select>
        </span> <span class="year">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>2015</option>
			<option>2016</option>
			<option>2017</option>
			<option>2018</option>
			<option>2019</option>
			<option>2020</option>
			<option>2021</option>
			<option>2022</option>
			<option>2023</option>
			<option>2024</option>
			<option>2025</option>
			<option>2026</option>
			<option>2027</option>
			<option>2028</option>
			<option>2029</option>
			<option>2030</option>
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="" type="text" class="text-box-billing-zip" >
        </span> </li>
     
      <li> <span class="card-holder-margin">
        <h3>One or two sentences about site security, encryption methods, storage of data meeting Y and Z standards (to let people feel secure about storing their credit card info).</h3>
        <a href="#" class="add-address add-credit-card">ADD</a> </span> </li>
    </ul>
  </div>
</div>

<div class="popup-wrapper" id="credit-card-edit-details" style="display: none;">
  <div class="add-new-address-inner"> <a href="javascript:void(0)" class="close-button">Close</a>
    <div class="title-holder">
      <h1>EDIT CREDIT CARD</h1>
    </div>
     <form id="edit-billForm">
    <ul class="from-holder">
   
      <li> <span class="text-box-holder">
        <p>Card Number</p>
        <input name="card_number" type="text" class="text-box-control" placeholder="">
        </span>
        <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="card_cvv" type="text" class="text-box-control" value="">
        </span>
        </li>
      <li> <span class="text-box-holder">
      <p>Credit Card</p>
        <span class="credit-card">
        <select name="card_type">
          <option value="visa">Visa</option>
          <option value="MasterCard">MasterCard</option>
          <option value="Discover">Discover</option>
          <option value="American Express">American Express</option>
        </select>
        </span> <br><br><br>
        <p>Expiration Date</p>
        <span class="month">
        <select name="expire_month">
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="03">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <select name="expire_year">
		<option>2019</option>
		<option>2020</option>
		<option>2021</option>
		<option>2022</option>
		<option>2023</option>
		<option>2024</option>
		<option>2025</option>
		<option>2026</option>
		<option>2027</option>
		<option>2028</option>
		<option>2029</option>
		<option>2030</option>
		<option>2031</option>
		<option>2032</option>
		<option>2033</option>
		<option>2034</option>
		<option>2035</option>
		<option>2036</option>
		<option>2037</option>
		<option>2038</option>
		<option>2039</option>
		<option>2040</option>
		<option>2041</option>
		<option>2042</option>
		<option>2043</option>
		<option>2044</option>
		<option>2045</option>
		<option>2046</option>
		<option>2047</option>
		<option>2048</option>
		<option>2049</option>
		<option>2050</option>
		<option>2051</option>
		<option>2052</option>
		<option>2053</option>
		<option>2054</option>
		<option>2055</option>
		<option>2056</option>
		<option>2057</option>
		<option>2058</option>
		<option>2059</option>
		<option>2060</option>
		<option>2061</option>
		<option>2062</option>
		<option>2063</option>
		<option>2064</option>
        <option>2065</option>
		<option>2066</option>
		<option>2067</option>
		<option>2068</option>
         
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="card_zip" type="text" class="text-box-billing-zip">
        </span> </li>
      <input type="hidden" name="id" value="" />
      <input type="hidden" name="uid" value="" />
      <input type="hidden" name="display_dropdown" value="1" />
      <li> <span class="card-holder-margin">
        <h3>One or two sentences about site security, encryption methods, storage of data meeting Y and Z standards (to let people feel secure about storing their credit card info).</h3>
        <a href="javascript:void(0)" class="add-address save-card-detail">Save</a> </span> </li>
    </ul>
     
    </form>
  </div>
  
</div>

<!-- Card Popup-->

<div class="popup-wrapper" id="product-descriptor">
  <div class="add-new-address-inner"> <a href="#" class="close-button">Close</a>
	<form class="add_address" method="post" >
    <div class="title-holder">
      <h1></h1>
    </div>
    <ul class="from-holder">
      
    </ul>
    <div class="hidden_fields"></div>
    <input type="button" class="add_desc" name="add_desc" value="ADD TO CART"/>
    </form>
   </div>
</div>

<!-- SALAD popup start -->
   <div class="popup-wrapper salad-item--options" id="salad_popup" style="display: none;">
      <div class="popup-edit-sandwich">
         <a href="javascript:void(0);" class="close-button">Close</a>
         <div class="salads_popup--wrap">
            <form>
               <!-- <div class="salad_item__image">
                  <img src="images/featured_sandwitch/salad.png">
               </div> -->
               <div class="salad_item__description">
                  <h4>
                     Asian Crisp Salad
                  </h4>
                  <p>
                     Romaine lettuce, cabbage, mandarin orange, crispy wonton, green onion, shredded carrot, edamame,
                     sesame seeds, and thai peanut dressing
                  </p>
                  <div class="hidden-values">
                    
                  </div>
                  <div class="salad_item__options">
                     <a href="javascript:void(0);" class="collapse-expand add_modifier">
                       <span>+</span> <p class="modifier_desc" style="font-size: unset;margin: 0px;">Add a Protein</p>
                     </a>
                     <div class="salad-protien__checkbox collapse" style="display: none;">
                       <div class="salad-protien--sec">
                      </div>

                     </div>
                     <a href="javascript:void(0);" class="collapse-expand spcl_instruction">
                        <span>+</span> Special Instructions
                     </a>
                     <div class="salad-options-special_instructions collapse" style="display: none">
                        <input type="text" name="spcl_instructions" value="">
                     </div>
                  </div>
               </div>

               <div class="salad-item--bottom">
                  <div class="popup_spinner">
                    <a href="javascript:void(0);" class="left_spinner"></a>
              <input name="itemQuatity" type="text" class="text-box" value="01" readonly="">
              <a href="javascript:void(0);" class="right_spinner"></a>
                  </div>
                  <a href="javascript:void(0);" class="salad-add--cart add-salad-to-cart addCart">Add to Cart</a>
               </div>
            </form>
         </div>
      </div>
   </div>
<!-- popup end -->


<!-- Snadwhich popup view -->
<div class="popup-wrapper" id="sandwiches-popup" style="display: none;">
    <div class="popup-wrapper quickedit-loader" id="sandwiches-popup_ajax_loader" style="display: none;">
      <img src="{$SITE_URL}app/images/ajax-loader.gif">
    </div>
    <div class="added-menu-sandwiches-inner">
      <a href="javascript:void(0);" class="close-button">Close</a> 
      <img src="{$SITE_URL}app/images/added-menu-sandwiches.png" alt="">
        <div class="title-holder">
          <h1>VASANTH'S PEPPERONI TREAT</h1>
          <h2>$10.00</h2>
        </div>

        <p>Whole Wheat Wrap, Pepperoni (1.0), Muenster (N), Cucumbers (H), Dijon Mustard (S)</p>

        <div class="title2-holder">
          <div class="checkbox-holder">
            <input id="check18" type="checkbox" class="menucheck savetousertoastnew" name="check" value="check18">
            <label for="check18">Toast It! <!--<span>Yum!</span>--></label>
          </div>
          
          <p></p>
        </div>
        <div class="button-holder">  
          
          <input type="hidden" name="hidden_sandwich_data" id="hidden_sandwich_data" value="user_sandwich">
          <input type="hidden" name="hidden_sandwich_id" id="hidden_sandwich_id" value="">
          <div class="popup_spinner">
              <a href="javascript:void(0);" class="left_spinner"></a>
              <input name="itemQuty" type="text" class="text-box" value="01" readonly="">
              <a href="javascript:void(0);" class="right_spinner"></a>
          </div>
          <a href="javascript:void(0);" data-type="user_sandwich" data-id=" " class="add-to-cart link">ADD TO CART</a> 
        </div>
    </div>
</div>





</body>
</html>
