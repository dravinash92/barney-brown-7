<div class="container">
     <div class="webpage">
       <h1>WEB PAGES</h1>
       
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th>&nbsp;</th>
          <th colspan="2">WEBSITE PAGES</th>
          <th>Order</th>
          <th>Status</th>
        </tr>
        
        <tr class="even">
          <td>&nbsp;</td>
          <td width="94%">Homepage</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="6%"><a href="{$SITE_URL}web/homepage/">edit</a></td>
        </tr>
         {foreach from=$webpageData key=k item=v}
        <tr>
          <td>&nbsp;</td>
          <td width="60%">{$v->webpage_name}</td>
          <td width="10%">{$v->orders}</td>
          <td width="10%">{if $v->status eq 1} Enabled {else} Disabled {/if}</td>
          <td width="6%"><a href="{$SITE_URL}web/webpage/{$v->id}">edit</a></td>
        </tr>
        {/foreach}
        
      </tbody></table>

        
    </div>
  </div>
