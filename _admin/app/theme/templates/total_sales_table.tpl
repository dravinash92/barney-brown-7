	{if $orders|@count gt 0}
		{php}	
			$items_total= $tax_total = $sub_total = $delivery_fee_total  = $tip_total = $total = $dscnt_total = $gft_total = $refund = $paid = 0;
		{/php}	
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr>
			  <th>ORDER #</th>
			  <th>DATE/TIME</th>
			  <th>CUSTOMER NAME</th>
			  <!-- <th>ITEMS</th> -->
			  <th>SUBTOT</th>
			  <th>TAX</th>
			  <th>FEE</th>
			  <th>TIP</th>
			  <th>TOTAL</th>
			  <th>DSCNT</th>
			  <th>GFT CRD</th>
			  <th>REFUND</th>
			  <th>PAID</th>
			</tr>
			{foreach from=$orders key=k item=v}
				{assign var='item_total' value=$v->item_total-$v->total_tax}				
				{assign var='tax' value=$v->total_tax}
				{assign var='sub' value=$v->sub_total}
				{assign var='delivery_fee' value=$v->delivery_fee}
				{assign var='tip' value=$v->tip}
				{assign var='calc_total' value=$v->calc_total}
				{assign var='off_amount' value=$v->off_amount}
				{assign var='total' value=$v->total}
				{php}
					$items_total += $_smarty_tpl->get_template_vars('item_total');
					$tax_total += $_smarty_tpl->get_template_vars('tax');
					$sub_total += $_smarty_tpl->get_template_vars('sub');
					$delivery_fee_total += $_smarty_tpl->get_template_vars('delivery_fee');
					$tip_total += $_smarty_tpl->get_template_vars('tip');
					$calc_total += $_smarty_tpl->get_template_vars('calc_total');
					$dscnt_total += $_smarty_tpl->get_template_vars('off_amount');
					$paid += $_smarty_tpl->get_template_vars('total');
				{/php}			
			{/foreach}
			
			<tr class="subheader">
			  <td colspan="3">TOTAL</td>
			  <!-- <td>${php}echo sprintf('%0.2f',$items_total){/php}</td> -->
			  <td>${php}echo sprintf('%0.2f',$sub_total){/php}</td>
			  <td>${php}echo sprintf('%0.2f',$tax_total){/php}</td>
			  <td>${php}echo sprintf('%0.2f',$delivery_fee_total){/php}</td>
			  <td>${php}echo sprintf('%0.2f',$tip_total){/php}</td>
			  <td>${php}echo sprintf('%0.2f',$calc_total+$tax_total){/php}</td>
			  <td>-${php}echo sprintf('%0.2f',$dscnt_total){/php}</td>
			  <td>-$0.00</td>
			  <td>-$0.00</td>
			  <td>${php}echo sprintf('%0.2f',$calc_total+$tax_total-$dscnt_total){/php}</td>
			</tr>			
			
			
			{foreach from=$orders key=k item=v}
				{if $k is not even} 
				<tr class="">
				{else}	
				<tr class="even">
				{/if}	
				  <td><a href="{$SITE_URL}reports/showOrderDetails/{$v->order_id}">{$v->order_number}</a></td>
				  <td>{$v->o_date}</td>
				  <td>{$v->first_name} {$v->last_name}</td>
				<!--   <td>${$v->amount_wt|string_format:"%.2f"}</td> -->
				  <td>${$v->sub_total|string_format:"%.2f"}</td>
				  <td>${$v->total_tax|string_format:"%.2f"}</td>
				  <td>${$v->delivery_fee}</td>
				  <td>${$v->tip}</td>
				  <td>${$v->calc_total+$v->total_tax|string_format:"%.2f"}</td>
				  <td>-${$v->off_amount|string_format:"%.2f"}</td>
				  <td>-$0.00</td>
				  <td>-$0.00</td>
				  <td>${$v->total|string_format:"%.2f"}</td>
				</tr>				
			{/foreach }					
			
			
			</tbody>
		</table>	
	{else}
		<h3>No orders found.</h3>	
	{/if}	
