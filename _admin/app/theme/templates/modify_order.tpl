<div class="container">
    <div class="place-order">
       <h1>MODIFY ORDER</h1>
       
       <div class="place-order-coustomer-info">
       		<div class="address-details">
             <p>ORDER #: 37849579</p>
           <h3>CUSTOMER INFO</h3>
           <p>Name: Matthew Baer<br>
              Email: mbaer201@gmail.com<br>
              Phone: 201-787-5073<br>
              Total Orders: 24<br>
              Total Sales: $450.00</p>
           <a href="#" class="edit-info">edit info</a>
         </div>
         
         <div class="customer-notes">
         		<h3>CUSTOMER NOTES</h3>
            <p><span>3/15/14</span><span class="address">Places corporate orders for AOL, Time Warner, and Sony</span></p>
         		<a href="#" class="remove">Remove</a>
            <a href="#" class="add-new-note">add new note</a>
         </div>
         
         <div class="date-time-order">
         		<h3>DATE/TIME OF ORDER</h3>
            <div class="radio-holder">
            	<input type="radio" class="css-checkbox" id="radio1" name="radiog_lite">
              <label class="css-label" for="radio1">Now</label>
              <input type="radio" class="css-checkbox" id="radio2" name="radiog_lite">
              <label class="css-label" for="radio2">Specific Date/Time</label>
            </div>
            
            <div class="text-box-holder">
            	<label>Date</label>
              <input type="text" class="text-box" name="">
              <a class="date-icon" href="#"></a>
            </div>
            <div class="text-box-holder">
            	<label>Time</label>
              <select name="" class="select">
              	<option>12:00 PM</option>
              </select>
            </div>
         </div>
         
         <div class="pickup-deliver-order">
         		<div class="radio-holder">
            	<input type="radio" class="css-checkbox" id="radio3" name="radiog_lite">
              <label class="css-label" for="radio3">DELIVERY</label>
              <input type="radio" class="css-checkbox" id="radio4" name="radiog_lite">
              <label class="css-label" for="radio4">PICK-UP</label>
            </div>
           <div class="text-box-holder">
            <label>Address</label>
            <select name="" class="select">
              <option>168 W 25TH ST</option>
            </select>
          </div>
          <a href="#" class="add-new-address">add new address</a>
          <a href="#" class="add-new-address">edit addresses</a>
         </div>
         
       </div>
      
      <div class="saved-sandwiches-new">
      	<h2>SAVED SANDWICHES</h2>
        
        	<table class="saved-sandwiches-table">
            <tbody><tr>
              <td>
              	<p><span>MATT'S TURKEY SURPRISE</span> ______________________________________________________________________________________________</p>
                <p>Baguette, Turkey (Double), Swiss, Lettuce, Tomato, Mayonnaise</p>
              </td>
              <td>$10.00</td>
              <td><a href="#" class="add-to-cart">ADD TO CART</a></td>
            </tr>
            <tr>
              <td>
              	<p><span>MATT'S ROAST BEEF THRILL</span> ____________________________________________________________________________________________</p>
                <p>Baguette, Roast Beef (Double), Swiss, Lettuce, Tomato, Mayonnaise</p>
              </td>
              <td>$10.00</td>
              <td><a href="#" class="add-to-cart">ADD TO CART</a></td>
            </tr>
          </tbody></table>

          
      </div>
      
      <div class="custom-sandwich-wrapper">
       		<h2>CUSTOM SANDWICH</h2>
          
        <div class="bread">
          <h3>BREAD</h3>
          <ul>
            <li>
              <div class="radio-section">
                <input id="1-1" type="radio" name="Redirect" class="input-radio" value="Redirect">
                <label for="1-1"></label>
              </div>
              <span class="radio-text">Baguette</span> </li>
            <li>
              <div class="radio-section">
                <input id="2-2" type="radio" name="Redirect" class="input-radio" value="Redirect">
                <label for="2-2"></label>
              </div>
              <span class="radio-text">Sesame Roll</span> </li>
            <li>
              <div class="radio-section">
                <input id="3-3" type="radio" name="Redirect" class="input-radio" value="Redirect">
                <label for="3-3"></label>
              </div>
              <span class="radio-text">Croissant</span> </li>
            <li>
              <div class="radio-section">
                <input id="4" type="radio" name="Redirect" class="input-radio" value="Redirect">
                <label for="4"></label>
              </div>
              <span class="radio-text">Pumpernickel</span> </li>
            <li>
              <div class="radio-section">
                <input id="5" type="radio" name="Redirect" class="input-radio" value="Redirect">
                <label for="5"></label>
              </div>
              <span class="radio-text">Ciabatta</span> </li>
            <li>
              <div class="radio-section">
                <input id="6" type="radio" name="Redirect" class="input-radio" value="Redirect">
                <label for="6"></label>
              </div>
              <span class="radio-text">Brioche</span> </li>
          </ul>
        </div>
        <div class="meats">
          <h3>Meats</h3>
          <ul>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-2-2">
              <label class="" for="checkbox-2-2"></label>
              </span> <span class="radio-text">Smoked Turkey</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-2-3">
              <label class="" for="checkbox-2-3"></label>
              </span> <span class="radio-text">Honey Glazed Ham</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-2-4">
              <label class="" for="checkbox-2-4"></label>
              </span> <span class="radio-text">Genoa Salami</span>
                 <select>
                 <option value="">Normal Portion</option>
                 <option value=""></option>
                 </select>
               </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-2-5">
              <label class="" for="checkbox-2-5"></label>
              </span> <span class="radio-text">Proscuitto Di Parma</span> </li>
          </ul>
        </div>
        
        <div class="cheese">
          <h3>CHEESES</h3>
          <ul>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-3-2">
              <label class="" for="checkbox-3-2"></label>
              </span> <span class="radio-text">Swiss</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-3-3">
              <label class="" for="checkbox-3-3"></label>
              </span> <span class="radio-text">Cheddar</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-3-4">
              <label class="" for="checkbox-3-4"></label>
              </span> <span class="radio-text">Brie</span>
               </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-3-5">
              <label class="" for="checkbox-3-5"></label>
              </span> <span class="radio-text">Mozzarella</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-3-6">
              <label class="" for="checkbox-3-6"></label>
              </span> <span class="radio-text">Muenster</span> </li>
          </ul>
        </div>
        
        <div class="cheese">
          <h3>STANDARD TOPPINGS</h3>
          <ul>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-4-2">
              <label class="" for="checkbox-4-2"></label>
              </span> <span class="radio-text">Lettuce</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-4-3">
              <label class="" for="checkbox-4-3"></label>
              </span> <span class="radio-text">Tomatoes</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-4-4">
              <label class="" for="checkbox-4-4"></label>
              </span> <span class="radio-text">Red Onions</span>
               </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-4-5">
              <label class="" for="checkbox-4-5"></label>
              </span> <span class="radio-text">Green Bell Peppers</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-4-6">
              <label class="" for="checkbox-4-6"></label>
              </span> <span class="radio-text">Banana Peppers</span> </li>
           </ul>
        </div>
        
        <div class="cheese">
          <h3>CONDIMENTS</h3>
          <ul>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-5-2">
              <label class="" for="checkbox-5-2"></label>
              </span> <span class="radio-text">Regular Mayonnaise</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-5-3">
              <label class="" for="checkbox-5-3"></label>
              </span> <span class="radio-text">Light Mayonnaise</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-5-4">
              <label class="" for="checkbox-5-4"></label>
              </span> <span class="radio-text">Chipotle Mayonnaise</span>
               </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-5-5">
              <label class="" for="checkbox-5-5"></label>
              </span> <span class="radio-text">Mustard</span> </li>
            <li> <span class="multi-left">
              <input type="checkbox" name="" class="input-checkbox" id="checkbox-5-6">
              <label class="" for="checkbox-5-6"></label>
              </span> <span class="radio-text">Russian Dressing</span> </li>
          </ul>
        </div>
          
          
          <div class="button-holder">
          	<div class="check-box-holder">
            	<input type="checkbox" id="checkbox-5-12" class="input-checkbox" name="">
              <label for="checkbox-5-12" class="multisel-ckeck"></label>
              <span>Save</span>
            </div>
            <div class="sandwich-name">
            	<label>Sandwich Name</label>
              <input type="text" class="text-box" name="">
            </div>
            <div class="radio-holder">
             <input type="radio" value="Redirect" class="input-radio" name="Redirect" id="6">
             <label for="6"></label>
             <span>Public</span>
           </div>
           <div class="radio-holder">
             <input type="radio" value="Redirect" class="input-radio" name="Redirect" id="7">
             <label for="7"></label>
             <span>Private</span>
           </div>
           <a class="add-cart" href="#">ADD TO CART</a>
          </div>
          
       </div>
      
      <div class="saved-sandwiches-new">
      	<h2>SANDWICH GALLERY</h2>
        	
          <div class="text-box-holder">
          	<input type="text" class="text-box" name="" placeholder="Search Sandwich Name, ID, Creator">
            <a class="shopping-cart-select" href="#"></a>
          </div>
         
      </div>
      
      
      <div class="sides-drink-wrapper">
          <div class="sides-wrapper">
          	<h2>SIDES</h2>
            <ul class="sides-list">
               <li>
                 <h3>Side 1 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Side 2 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Side 3 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Side 4 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               <li>
                 <h3>Side 5 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               <li>
                 <h3>Side 6 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
         	</ul>
          </div>
          <div class="drink-wrapper">
          	<h2>DRINKS</h2>
            
            <ul class="sides-list">
               <li>
                 <h3>Drink 1 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Drink 2 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Drink 3 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Drink 4 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               <li>
                 <h3>Drink 5 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               <li>
                 <h3>Drink 6 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
         	</ul>
            
          </div>
       </div>
      
      
      <div class="shopping-cart">
     		<h3>SHOPPING CART</h3>
          
         <table class="shopping-cart-table-new">
           <tbody><tr>
             <th>ORDER DETAILS</th>
             <th>&nbsp;</th>
             <th>QTY</th>
             <th>TOTAL</th>
             <th>&nbsp;</th>
           </tr>
           <tr>
             <td colspan="2">
             	 <p>CUSTOM SANDWICH _______________________________________________________________________________________________</p>
               <p>Baguette, Smoked Turkey, Swiss Cheese, Lettuce, Tomato, Onion, Light Mayonnaise</p>
             </td>
             <td>
             	<span class="arrow-holder">
               <a href="#" class="arrow-left"></a>
               <input type="text" name="" value="0">
               <a href="#" class="arrow-right"></a>
             </span>
             </td>
             <td>$10.00</td>
             <td><a href="#" class="remove">REMOVE</a></td>
           </tr>
           <tr>
             <td colspan="2"><p>SIDE 1 ___________________________________________________________________________________________________________</p></td>
             <td>
             	<span class="arrow-holder">
               <a href="#" class="arrow-left"></a>
               <input type="text" name="" value="2">
               <a href="#" class="arrow-right"></a>
             </span>
             </td>
             <td>$4.00</td>
             <td><a href="#" class="remove">REMOVE</a></td>
           </tr>
           <tr>
             <td colspan="2">DRINK 1 __________________________________________________________________________________________________________</td>
             <td>
             	<span class="arrow-holder">
               <a href="#" class="arrow-left"></a>
               <input type="text" name="" value="2">
               <a href="#" class="arrow-right"></a>
             </span>
             </td>
             <td>$4.00</td>
             <td><a href="#" class="remove">REMOVE</a></td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">SUBTOTAL</td>
            <td>$18.00 </td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">TIP</td>
            <td colspan="2">
              <select name="" class="select-tip">
                <option>$3.00</option>
              </select>
            </td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL VALUE</h4></td>
            <td><h4>$21.00</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">DISCOUNT:  30% OFF SPECIAL</td>
            <td>-$5.20</td>
            <td><a href="#" class="remove">REMOVE</a></td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">DISCOUNT / GIFT CARD4</p>
            	<span class="text-box-holder">
              	<input name="" type="text" class="text-box">
                <a href="#" class="shopping-cart-select"></a>
              </span>
            </td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">MANUAL DISCOUNT</p>
            	<span class="text-box-holder">
              	<select name="" class="select">
                	<option>Select</option>
             		</select>
                <a href="#" class="shopping-cart-select"></a>
              </span>
            </td>
           </tr>
           
            <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>UPDATED ORDER TOTAL</h4></td>
            <td><h4>$15.60</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">PAYMENT 08/15/14 4:23PM</td>
            <td>-($12.80)</td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL OWED</h4></td>
            <td><h4>$2.80</h4></td>
            <td>&nbsp;</td>
           </tr>
         </tbody></table>
			</div>
       
       <div class="billing-information-holder">
       		<h3>BILLING INFORMATION</h3>
          <div class="text-box-holder ">
          	<select name="" class="billing-select">
              <option>Visa ending in 1089</option>
            </select>
          </div>
          <a href="#" class="edit-bill">add new billing</a>
          <a href="#" class="edit-bill">edit billing</a>
          <a href="#" class="place-order">MODIFY ORDER</a>
       </div>
       
   		<div class="pay-total-wrapper">
       <div class="shopping-cart">
          <h5>PAYMENT : 08/15/14 4:23PM</h5>
          <h6>PAYMENT METHOD: Amex ending in 4089</h6>
         <table class="shopping-cart-table-new">
           <tbody><tr>
             <th>ORDER DETAILS</th>
             <th>&nbsp;</th>
             <th>QTY</th>
             <th>TOTAL</th>
             <th>&nbsp;</th>
           </tr>
           <tr>
             <td colspan="2">
             	 <p>CUSTOM SANDWICH _______________________________________________________________________________________________</p>
               <p>Baguette, Smoked Turkey, Swiss Cheese, Lettuce, Tomato, Onion, Light Mayonnaise</p>
             </td>
             <td class="text-center"> 1 </td>
             <td>$10.00</td>
             <td></td>
           </tr>
           <tr>
             <td colspan="2"><p>SIDE 1 ___________________________________________________________________________________________________________</p></td>
             <td class="text-center"> 1 </td>
             <td>$2.00</td>
             <td></td>
           </tr>
           <tr>
             <td colspan="2">DRINK 1 __________________________________________________________________________________________________________</td>
             <td class="text-center"> 1 </td>
             <td>$2.00</td>
             <td></td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">SUBTOTAL</td>
            <td>$14.00 </td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">TIP</td>
            <td>$3.00</td>
            <td></td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL VALUE</h4></td>
            <td><h4>$17.00</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">DISCOUNT:  30% OFF SPECIAL</td>
            <td>-$5.20</td>
            <td></td>
           </tr>
           
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL PAID</h4></td>
            <td><h4>$2.80</h4></td>
            <td>&nbsp;</td>
           </tr>
         </tbody></table>
			</div>
      </div> 
    </div>
  </div>