{if $del_details|@count gt 0}
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <th>ORDER #</th>
          <th>PLACEMENT</th>
          <th>FULFILLMENT</th>
          <th>CUSTOMER NAME</th>
          <th>SUBTOT</th>
          <th>TAX</th>
          <th>FEE</th>
          <th>TIP</th>
          <th>VALUE</th>
          <th>DSCNT</th>
          <th>REFUND</th>
          <th>PAID</th>
        </tr>
        {php} 
      $tax_total = $sub_total = $delivery_fee_total  = $tip_total = $total = $dscnt_total  = $paid = 0;
    {/php} 
        {foreach from=$del_details  item=v}      
        {assign var='tax' value=$v.tax}
        {assign var='sub' value=$v.SubTot}
        {assign var='delivery_fee' value=$v.Fee}
        {assign var='tip' value=$v.tip}
        {assign var='calc_total' value=$v.Value}
        {assign var='off_amount' value=$v.Dscnt}
        {assign var='total' value=$v.PaidAmt}
        {php}
          $tax_total += $_smarty_tpl->get_template_vars('tax');
          $sub_total += $_smarty_tpl->get_template_vars('sub');
          $delivery_fee_total += $_smarty_tpl->get_template_vars('delivery_fee');
          $tip_total += $_smarty_tpl->get_template_vars('tip');
          $calc_total += $_smarty_tpl->get_template_vars('calc_total');
          $dscnt_total += $_smarty_tpl->get_template_vars('off_amount');
          $paid += $_smarty_tpl->get_template_vars('total');
        {/php}      
      {/foreach}
        <tr class="subheader">
          <td colspan="3">TOTAL</td>
          <td></td>
         <td>${php}echo sprintf('%0.2f',$sub_total){/php}</td>
        <td>${php}echo sprintf('%0.2f',$tax_total){/php}</td>
        <td>${php}echo sprintf('%0.2f',$delivery_fee_total){/php}</td>
        <td>${php}echo sprintf('%0.2f',$tip_total){/php}</td>
        <td>${php}echo sprintf('%0.2f',$calc_total){/php}</td>
        <td>-${php}echo sprintf('%0.2f',$dscnt_total){/php}</td>
        <td>-$0.00</td>
        <td>${php}echo sprintf('%0.2f',$sub_total+$delivery_fee_total+$tax_total+$tip_total-$dscnt_total){/php}</td>
        </tr>
       {foreach from=$del_details item=v}
        {if $k is not even} 
        <tr class="">
        {else}  
        <tr class="even">
        {/if} 
          <td><a href="{$SITE_URL}reports/showOrderDetails/{$v.order_id}">{$v.OrderNumber}</a></td>
          <td>{$v.Placement}</td>
          <td>{$v.Fulfillment}</td>
          <td>{$v.Name}</td>
          <td>${$v.SubTot|string_format:"%.2f"}</td>
          <td>${$v.tax|string_format:"%.2f"}</td>
          <td>${$v.Fee}</td>
          <td>${$v.tip}</td>
          <td>${$v.Value|string_format:"%.2f"}</td>
          <td>-${$v.Dscnt|string_format:"%.2f"}</td>
          <td>-$0.00</td>
          <td>${$v.PaidAmt|string_format:"%.2f"}</td>
        </tr>       
      {/foreach }  
     
    
</tbody>
    </table> 
    {else}
    <table class="no-record-wrap" width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr class="no-record">
          <th> No Result Found!</th>
        </tr>
</tbody>
    </table> 
  {/if} 