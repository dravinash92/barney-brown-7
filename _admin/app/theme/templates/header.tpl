<!DOCTYPE html>
<html lang="en">
<head> 
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>{$Title}</title>
<link href="{$SITE_URL}app/stylesheets/screen.css" rel="stylesheet"/>
<link href="{$SITE_URL}app/stylesheets/print.css" rel="stylesheet"/>
<link rel="stylesheet" href="{$SITE_URL}app/stylesheets/jquery-ui.css"/>
<script type="text/javascript" src="{$SITE_URL}app/js/jquery.min.js"></script>
<script type="text/javascript" src="{$SITE_URL}app/js/jquery-ui.js"></script>
<script type="text/javascript" src="{$SITE_URL}app/js/common.js"></script>
<script type="text/javascript" src="{$SITE_URL}app/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="{$SITE_URL}ckeditor/ckeditor.js"></script>




<script>
	var SITE_URL = "{$SITE_URL}";
	var CMS_URL = "{$CMS_URL}";
	var API_URL  = "{$API_URL}";
	var SESSION_ID = "{$TO_USER_ID}";
	var DATA_ID = "{$DATA_ID}";
	var BASE_FARE = "{$BASE_FARE}";
	 
</script>
<script type="text/javascript" src="{$SITE_URL}app/js/sandwichcreator.js"></script>

</head>
<body>


<div class="popupOrderDetailsBg">&nbsp;</div>

<div class="popupOrderDetails">

<div class="clear"></div>


<div class="popup_order_detail" >

<p  class="closeOrd">X</p>
<div id="popOut" >
  
  
  <p class="loader">
  <img src="{$SITE_URL}app/images/optionload.gif" />
  </p>
  </div>
  
 <!-- end -->
  </div>  

<div class="clear"></div>

</div>

<div id="outer-wrapper">

<div class="container">
     <!--Header Starting-->
     <div class="header">
        <div class="logo">
           <a href="#"><img width="225" height="28" alt="{$title_text}" src="{$SITE_URL}app/images/logo.png"></a>
        </div>  
        
        <div class="user-login">
         
             {php} if( isset($_SESSION['admin_uid']) ) {  {/php}
           <p>{php} echo $_SESSION['uname'] {/php}</p>
           <a href="{$SITE_URL}login/signout/">logout</a>
          {php} } {/php}
        </div>
        
     </div>
  </div>


<div class="navigation">
    <div class="container">
   {php} if( isset($_SESSION['admin_uid']) ) {  {/php}
      <nav>
        <ul>
		{if $User_Type ne store}	
	    <li class="first-child"><a  {if $ACTIVE_MAIN_MENU_TEXT eq neworder   } class="active" {/if} href="{$SITE_URL}neworder/">NEW ORDER</a></li>
	  
	    
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq deliveries } class="active" {/if} href="{$SITE_URL}deliveries/">DELIVERIES</a></li>
		
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq pickup     } class="active" {/if} href="{$SITE_URL}pickups/">PICK-UPS</a></li>
		{else}
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq storedeliveries } class="active" {/if} href="{$SITE_URL}storedeliveries/">DELIVERIES</a></li>
		
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq storepickups     } class="active" {/if} href="{$SITE_URL}storepickups/">PICK-UPS</a></li>
		
		{/if}
		
		
		  
		{if $User_Type ne store}	
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq customers  } class="active" {/if} href="{$SITE_URL}customer/">CUSTOMERS</a></li>
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq menu       } class="active" {/if} href="{$SITE_URL}menu/">MENU ITEMS</a></li>
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq sandwich   } class="active" {/if} href="{$SITE_URL}sandwich/">SANDWICH GALLERY</a></li>
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq discounts  } class="active" {/if} href="{$SITE_URL}discounts/">DISCOUNTS </a></li>
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq web        } class="active" {/if} href="{$SITE_URL}web/">WEB PAGES</a></li>
		{/if}
		{if $User_Type ne store}	
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq reports    } class="active" {/if} href="{$SITE_URL}reports/">REPORTS</a></li>
		{else}
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq reports    } class="active" {/if} href="{$SITE_URL}storereports">REPORTS</a></li>
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq storemanage    } class="active" {/if} href="{$SITE_URL}storemanage/">MANAGE STORE</a></li>	
		{/if}
		
		{if $User_Type ne store}	
		<li><a {if $ACTIVE_MAIN_MENU_TEXT eq admin      } class="active" {/if} href="{$SITE_URL}accounts/">ADMIN</a></li>
		{/if}
   
        </ul>
      </nav>
{php} } {/php}
    </div>
  </div>
  
{if $SEC_NAV_OPS.show eq true } 

<div class="secondary-navigation">
    <div class="container">
      <nav>
        <ul>
{section name=nav start=0 loop=$SEC_NAV_OPS.text|@count step=1 }

<li class="first-child"><a {if $SEC_NAV_OPS.active_text eq  $SEC_NAV_OPS.text[nav] } class="active" {/if}  href="{$SEC_NAV_OPS.url[nav]}"> {$SEC_NAV_OPS.text[nav]} </a></li>
{/section}
         
       </ul>
      </nav>
    </div>
  </div> 
  
{/if}


{php} if( isset($_SESSION['success']) ) {{/php}
<div style="width: 930px;height: auto;margin: 0 auto;clear:both">
<br><p style="color:red;margin-top:10px;">{$success}</div></div>
{php} unset($_SESSION['success']); } {/php}

