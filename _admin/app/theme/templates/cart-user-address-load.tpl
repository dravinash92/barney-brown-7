 <label>User Addresses</label>
    <select name="user_address" class="select user_address">
        <option value="">Select Address</option>
		{foreach from=$addresses key=k item=v}
			<option value="{$v->address_id}">{$v->name},{if $v->address1 neq ""} {$v->address1},{/if} {$v->cross_streets}, {$v->zip}</option>
        {/foreach }
    </select>
    <input type="hidden" name="address_type"  value="delivery" />     
    <a href="javascript:void(0)" class="add-new-address add-address">add new address</a>
	<a href="javascript:void(0)" class="add-new-address edit-address">edit addresses</a>
