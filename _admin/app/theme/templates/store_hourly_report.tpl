<div class="container">
    <div class="hourly-report">
      
      <div class="heading">
        <h1>HOURLY REPORT</h1>
        
        <div class="date">
          <p>
            <span>Date</span>
            <input id="storedatehourreport" type="text" value="{$date}" placeholder="{$date}" ><a class="date-icon" href="#"><img width="24" height="23" src="{$SITE_URL}app/images/calendar.png"></a>
          </p>
        </div>
      </div>
      
      <div class="store-wrapper">
           <div class="right">
              <span>Store Location</span>
              <select disabled> 
               <option value="">Select Store</option>
              {foreach from=$pickupstores key=j  item=stores}
               <option {if $storeId eq $stores->id} selected {/if} value="{$stores->id}" id="store{$stores->id}">{$stores->store_name}</option>
               
                {/foreach}
              </select>
           </div>
           
           <div class="left"> 
             <ul>
              <li>
              <span>To</span>
              <select name="tohour" id="storetohour">
               {$times}
               </select>
             </li>
             <li>
              <span>From</span>
               <select  name="fromhour" id="fromhour">
                {$times}
                </select>
             </li>
             </ul>
           </div>
           
      </div>
      <div class="reporttable">
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="62%">TIME</th>
          <th width="7%">PICK-UP</th>
          <th width="7%">DELIVERY</th>
          <th width="7%"> TOTAL</th>
          <th width="8%">SALES</th>
           <th width="9%">%SALES</th>
        </tr>
        
         <tr class="heading-color">
          <td></td>
          <td class="center">{$total_pickups}</td>
          <td class="center">{$total_delivery}</td>
          <td class="center">{$total|number_format}</td>
          <td>${$total_sales|string_format:"%.2f"}</td>
          <td>100%</td>
        </tr>
      {foreach from=$orderedItems key=i item=orderedItem}
      
        <tr {if $i%2==0} class="even" {/if}>
          <td>{$orderedItem->time}</td>
          <td class="center">{$orderedItem->pickups}</td>
          <td class="center">{$orderedItem->delivery}</td>
          <td class="center">{$orderedItem->total->total_order}</td>
          <td>{$orderedItem->total->total_sales}</td>
          <td> 
			{if $orderedItem->total->total_order gt 0 }
				{math equation="(( x / y ) * z )" x=$orderedItem->total->total_sales y=$total_sales z=100 format="%.2f"}%
          
			{/if}          
          </td>
        </tr>
				
    
      {/foreach}
        </tbody></table>
        </div>
      <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="62%">TIME</th>
          <th width="7%">PICK-UP</th>
          <th width="7%">DELIVERY</th>
          <th width="7%"> TOTAL</th>
          <th width="8%">SALES</th>
           <th width="9%">%SALES</th>
        </tr>
        
         <tr class="heading-color">
          <td></td>
          <td class="center">445</td>
          <td class="center">550</td>
          <td class="center">985</td>
          <td>$9850.00</td>
          <td>100.000%</td>
        </tr>
        
        <tr>
          <td>12:00AM - 12:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		 <tr class="even">
          <td>1:00AM - 1:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>2:00AM - 2:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
        
		 <tr class="even">
          <td>3:00AM - 3:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>4:00AM - 4:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>5:00AM - 5:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>6:00AM - 6:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>7:00AM - 7:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>8:00AM - 8:59AM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>9:00AM - 9:59AM</td>
          <td class="center">5</td>
          <td class="center">15</td>
          <td class="center">20</td>
          <td>$200.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>10:00AM - 10:59AM</td>
          <td class="center">15</td>
          <td class="center">25</td>
          <td class="center">40</td>
          <td>$400.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>11:00AM - 11:59AM</td>
          <td class="center">45</td>
          <td class="center">55</td>
          <td class="center">100</td>
          <td>$1000.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>12:00PM - 12:59PM</td>
          <td class="center">85</td>
          <td class="center">105</td>
          <td class="center">190</td>
          <td>$1900.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>1:00PM - 1:59PM</td>
          <td class="center">90</td>
          <td class="center">120</td>
          <td class="center">210</td>
          <td>$2100.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>2:00PM - 2:59PM</td>
          <td class="center">70</td>
          <td class="center">100</td>
          <td class="center">100</td>
          <td>$1000.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>3:00PM - 3:59PM</td>
          <td class="center">30</td>
          <td class="center">50</td>
          <td class="center">80</td>
          <td>$800.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>4:00PM - 4:59PM</td>
          <td class="center">20</td>
          <td class="center">30</td>
          <td class="center">50</td>
          <td>$500.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>5:00PM - 5:59PM</td>
          <td class="center">10</td>
          <td class="center">15</td>
          <td class="center">25</td>
          <td>$250.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>6:00PM - 6:59PM</td>
          <td class="center">25</td>
          <td class="center">25</td>
          <td class="center">50</td>
          <td>$500.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>7:00PM - 7:59PM</td>
          <td class="center">30</td>
          <td class="center">40</td>
          <td class="center">70</td>
          <td>$700.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>8:00PM - 8:59PM</td>
          <td class="center">15</td>
          <td class="center">20</td>
          <td class="center">35</td>
          <td>$350.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>9:00PM - 9:59PM</td>
          <td class="center">5</td>
          <td class="center">10</td>
          <td class="center">15</td>
          <td>$150.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr>
          <td>10:00PM - 10:59PM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
		
		<tr class="even">
          <td>11:00PM - 11:59PM</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td class="center">0</td>
          <td>$0.00</td>
          <td>0.000%</td>
        </tr>
	
		
      </tbody></table>-->
    </div>
  </div>
