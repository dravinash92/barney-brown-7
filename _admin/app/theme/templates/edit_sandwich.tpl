
<div class="container">
    <div class="sandwich">
      <h1>EDIT SANDWICH</h1>
      
     
      
      <p> ID#: 348650<br>
        User: {$EDIT_DATA[0].user_name} ({$EDIT_DATA[0].user_email})<br>
        Date Created: 
      
        
        06/15/2013<br>
        Likes: 100<br>
        Purchases: 5 <br>
       
       {$EDIT_DATA[0].description}
       
       {assign var="protien_itemid"  value=$EDIT_DATA[0].sandwich_data.PROTEIN.item_id}
       {assign var="cheese_itemid"  value=$EDIT_DATA[0].sandwich_data.CHEESE.item_id}
       {assign var="topping_itemid"  value=$EDIT_DATA[0].sandwich_data.TOPPINGS.item_id}
       {assign var="cond_itemid"  value=$EDIT_DATA[0].sandwich_data.CONDIMENTS.item_id}
       
       {assign var="protien_item_qty_key"  value=$EDIT_DATA[0].sandwich_data.PROTEIN.item_qty|@array_keys}
       {assign var="cheese_item_qty_key"  value=$EDIT_DATA[0].sandwich_data.CHEESE.item_qty|@array_keys}
       {assign var="topping_item_qty_key"  value=$EDIT_DATA[0].sandwich_data.TOPPINGS.item_qty|@array_keys}
       {assign var="cond_item_qty_key"  value=$EDIT_DATA[0].sandwich_data.CONDIMENTS.item_qty|@array_keys}
       
   
       {assign var="protien_item_qty_value"  value=$EDIT_DATA[0].sandwich_data.PROTEIN.item_qty|@array_values}
       {assign var="cheese_item_qty_value"  value=$EDIT_DATA[0].sandwich_data.CHEESE.item_qty|@array_values}
       {assign var="topping_item_qty_value"  value=$EDIT_DATA[0].sandwich_data.TOPPINGS.item_qty|@array_values}
       {assign var="cond_item_qty_value"  value=$EDIT_DATA[0].sandwich_data.CONDIMENTS.item_qty|@array_values}
 
       <input type="hidden" value="{$EDIT_DATA[0].sandwich_name}" name='sandwich_name' />  
       <input type="hidden" value="{$EDIT_DATA[0].sandwich_price}" name='sandwich_price' />       
       <input type="hidden" value="{$EDIT_DATA[0].menu_is_active}" name='sandwich_menustate' />
       <input type="hidden" value="{$EDIT_DATA[0].id}" name='sandwich_id' />      
       <input type="hidden" value="{$EDIT_DATA[0].uid}" name='sandwich_uid' />      
               
       <script>admin_ha.common.json_obj = JSON.parse('{$EDIT_DATA[0].sandwich_data_json}');</script>   

      </p>
      <ul>
        <li>
          <label>Name of Sandwich</label>
          <input type="text" name="sandwichname" value=" {$EDIT_DATA[0].sandwich_name}">
        </li>
        <li>
          <label>Description</label>
          <textarea name="description" id="description">{$EDIT_DATA[0].description}</textarea>
        </li>
        <li> <span class="multi-left">
          <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="public" {if $EDIT_DATA[0].is_public eq 1} checked="checked" {/if} value="">
          <label for="checkbox-2-1" class="multisel-ckeck"></label>
          </span> <span>Public</span> </li>
      </ul>
      <div class="custom-sandwich">
        <h2>CUSTOM SANDWICH</h2>
        
        {if $BREAD_DATA|@count gt 0}
        <div class="bread">
          <h3>BREAD</h3>
          <ul>
          
            {section name=bread start=0 loop=$BREAD_DATA|@count step=1 }
            
          
            
            <li>
              <div class="radio-section">
                <input 
                
                data-id = "{$BREAD_DATA[$smarty.section.bread.index].id}"
                data-item_price = "{$BREAD_DATA[$smarty.section.bread.index].item_price}"
                 data-item_name = "{$BREAD_DATA[$smarty.section.bread.index].item_name}"
                 data-category = "BREAD"
                {if $EDIT_DATA[0].sandwich_data.BREAD.item_id[0] eq $BREAD_DATA[$smarty.section.bread.index].id }   checked="checked" {/if}
                
                type="radio" value="Redirect" class="input-radio" name="Redirect" id="1-{$BREAD_DATA[$smarty.section.bread.index].id}">
                <label for="1-{$BREAD_DATA[$smarty.section.bread.index].id}"></label>
              </div>
              <span class="radio-text"> {$BREAD_DATA[$smarty.section.bread.index].item_name} </span> </li>
            {/section}
          </ul>
        </div>
        {/if}
        
        
        {if $PROTEIN_DATA|@count gt 0}
        <div class="meats">
          <h3>Meats</h3>
          <ul>
           {section name=protein start=0 loop=$PROTEIN_DATA|@count step=1 }
           {assign var="prot_name" value =$PROTEIN_DATA[$smarty.section.protein.index].item_name}
             <li><span class="multi-left">
              <input 
              
              data-id = "{$PROTEIN_DATA[$smarty.section.protein.index].id}"
              data-item_price = "{$PROTEIN_DATA[$smarty.section.protein.index].item_price}"
              data-item_name = "{$PROTEIN_DATA[$smarty.section.protein.index].item_name}"
              data-image = "{$PROTEIN_DATA[$smarty.section.protein.index].item_image}"
               data-category = "PROTEIN"
               {if $PROTEIN_DATA[$smarty.section.protein.index].id|in_array:$protien_itemid eq 1 }
                checked="checked" 
                {/if}
              type="checkbox" id="2-{$PROTEIN_DATA[$smarty.section.protein.index].id}" class="input-checkbox" name="" value="{$PROTEIN_DATA[$smarty.section.protein.index].item_name}">
              <label for="2-{$PROTEIN_DATA[$smarty.section.protein.index].id}" class="multisel-ckeck"></label>
              </span> <span class="radio-text">{$PROTEIN_DATA[$smarty.section.protein.index].item_name}</span> 
              
                 {if $PROTEIN_DATA[$smarty.section.protein.index].options_id neq 0}
                 
              <select>
                  {section name=protein_ops start=0 loop=$PROTEIN_DATA[$smarty.section.protein.index].options_id|@count step=1 }
                 
                 
                    
                  <option 
                  
                  {if $protien_item_qty_value.1.0 eq $PROTEIN_DATA[$smarty.section.protein.index].options_id[$smarty.section.protein_ops.index].option_name }
                  selected
                  {/if}
                  data-id="{$PROTEIN_DATA[$smarty.section.protein.index].options_id[$smarty.section.protein_ops.index].id}" 
                  data-unit="{$PROTEIN_DATA[$smarty.section.protein.index].options_id[$smarty.section.protein_ops.index].option_unit}" 
                  data-mult="{$PROTEIN_DATA[$smarty.section.protein.index].options_id[$smarty.section.protein_ops.index].price_mult}" 
                  data-name="{$PROTEIN_DATA[$smarty.section.protein.index].options_id[$smarty.section.protein_ops.index].option_name}" 
                  value="{$PROTEIN_DATA[$smarty.section.protein.index].options_id[$smarty.section.protein_ops.index].option_name}">{$PROTEIN_DATA[$smarty.section.protein.index].options_id[$smarty.section.protein_ops.index].option_name}</option>
                  {/section}
                 </select> 
                 {/if}
              
              </li>
           {/section}
          </ul>
        </div>
        {/if}
        
        
         {if $CHEESE_DATA|@count gt 0}
        
        <div class="cheese">
          <h3>CHEESES</h3>
          <ul>
            
            
            {section name=cheese start=0 loop=$CHEESE_DATA|@count step=1 }
           
             <li><span class="multi-left">
              <input 
              
              data-id = "{$CHEESE_DATA[$smarty.section.cheese.index].id}"
              data-item_price = "{$CHEESE_DATA[$smarty.section.cheese.index].item_price}"
              data-item_name = "{$CHEESE_DATA[$smarty.section.cheese.index].item_name}"
              data-image = "{$CHEESE_DATA[$smarty.section.cheese.index].item_image}"
              data-category = "CHEESE"
                {if $CHEESE_DATA[$smarty.section.cheese.index].id|in_array:$cheese_itemid eq 1 }
                checked="checked" 
                {/if}
              
              type="checkbox" id="3-{$CHEESE_DATA[$smarty.section.cheese.index].id}" class="input-checkbox" name="" value="{$CHEESE_DATA[$smarty.section.cheese.index].item_name}">
              <label for="3-{$CHEESE_DATA[$smarty.section.cheese.index].id}" class="multisel-ckeck"></label>
              </span> <span class="radio-text">{$CHEESE_DATA[$smarty.section.cheese.index].item_name}</span> 
              
                 {if $CHEESE_DATA[$smarty.section.cheese.index].options_id neq 0}
                 <select>
                  {section name=cheese_ops start=0 loop=$CHEESE_DATA[$smarty.section.cheese.index].options_id|@count step=1 }
                  <option 
                  data-id="{$CHEESE_DATA[$smarty.section.cheese.index].options_id[$smarty.section.cheese_ops.index].id}" 
                  data-unit="{$CHEESE_DATA[$smarty.section.cheese.index].options_id[$smarty.section.cheese_ops.index].option_unit}" 
                  data-mult="{$CHEESE_DATA[$smarty.section.cheese.index].options_id[$smarty.section.cheese_ops.index].price_mult}" 
                  data-name="{$CHEESE_DATA[$smarty.section.cheese.index].options_id[$smarty.section.cheese_ops.index].option_name}" 
                  value="{$CHEESE_DATA[$smarty.section.cheese.index].options_id[$smarty.section.cheese_ops.index].option_name}">{$CHEESE_DATA[$smarty.section.cheese.index].options_id[$smarty.section.cheese_ops.index].option_name}</option>
                  {/section}
                 </select>
                 {/if}
              
              </li>
           {/section}
            
            
          </ul>
        </div>
        
         {/if}
        
         {if $TOPPING_DATA|@count gt 0}
        <div class="cheese">
          <h3>STANDARD TOPPINGS</h3>
          <ul>
          
          
            {section name=topping start=0 loop=$TOPPING_DATA|@count step=1 }
           
             <li><span class="multi-left">
              <input 
              
              data-id = "{$TOPPING_DATA[$smarty.section.topping.index].id}"
              data-item_price = "{$TOPPING_DATA[$smarty.section.topping.index].item_price}"
              data-item_name = "{$TOPPING_DATA[$smarty.section.topping.index].item_name}"
                data-image = "{$TOPPING_DATA[$smarty.section.topping.index].item_image}"
              data-category = "TOPPINGS"
               {if $TOPPING_DATA[$smarty.section.topping.index].id|in_array:$topping_itemid eq 1 }
                checked="checked" 
                {/if}
              
              type="checkbox" id="4-{$TOPPING_DATA[$smarty.section.topping.index].id}" class="input-checkbox" name="" value="{$TOPPING_DATA[$smarty.section.topping.index].item_name}">
              <label for="4-{$TOPPING_DATA[$smarty.section.topping.index].id}" class="multisel-ckeck"></label>
              </span> <span class="radio-text">{$TOPPING_DATA[$smarty.section.topping.index].item_name}</span> 
              
                 {if $TOPPING_DATA[$smarty.section.topping.index].options_id neq 0}
                 <select>
                  {section name=topping_ops start=0 loop=$TOPPING_DATA[$smarty.section.topping.index].options_id|@count step=1 }
                  <option 
                  data-id="{$TOPPING_DATA[$smarty.section.topping.index].options_id[$smarty.section.topping_ops.index].id}" 
                  data-unit="{$TOPPING_DATA[$smarty.section.topping.index].options_id[$smarty.section.topping_ops.index].option_unit}" 
                  data-mult="{$TOPPING_DATA[$smarty.section.topping.index].options_id[$smarty.section.topping_ops.index].price_mult}" 
                  data-name="{$TOPPING_DATA[$smarty.section.topping.index].options_id[$smarty.section.topping_ops.index].option_name}" 
                  value="{$TOPPING_DATA[$smarty.section.topping.index].options_id[$smarty.section.topping_ops.index].option_name}">{$TOPPING_DATA[$smarty.section.topping.index].options_id[$smarty.section.topping_ops.index].option_name}</option>
                  {/section}
                 </select>
                 {/if}
              
              </li>
           {/section}
            
          </ul>
        </div>
          {/if}
        
        {if $CONDIMENTS_DATA|@count gt 0}
        <div class="cheese">
          <h3>CONDIMENTS</h3>
          <ul>
          
          
            {section name=condiments start=0 loop=$CONDIMENTS_DATA|@count step=1 }
           
             <li><span class="multi-left">
              <input 
              
              data-id = "{$CONDIMENTS_DATA[$smarty.section.condiments.index].id}"
              data-item_price = "{$CONDIMENTS_DATA[$smarty.section.condiments.index].item_price}"
              data-item_name = "{$CONDIMENTS_DATA[$smarty.section.condiments.index].item_name}"
               data-image = "{$CONDIMENTS_DATA[$smarty.section.condiments.index].item_image}"
               data-category = "CONDIMENTS"
                {if $CONDIMENTS_DATA[$smarty.section.condiments.index].id|in_array:$cond_itemid eq 1 }
                checked="checked" 
                {/if}
              
              type="checkbox" id="5-{$CONDIMENTS_DATA[$smarty.section.condiments.index].id}" class="input-checkbox" name="" value="{$CONDIMENTS_DATA[$smarty.section.condiments.index].item_name}">
              <label for="5-{$CONDIMENTS_DATA[$smarty.section.condiments.index].id}" class="multisel-ckeck"></label>
              </span> <span class="radio-text">{$CONDIMENTS_DATA[$smarty.section.condiments.index].item_name}</span> 
              
                 {if $CONDIMENTS_DATA[$smarty.section.condiments.index].options_id neq 0}
                 <select>
                  {section name=condiments_ops start=0 loop=$CONDIMENTS_DATA[$smarty.section.condiments.index].options_id|@count step=1 }
                  <option 
                  data-id="{$CONDIMENTS_DATA[$smarty.section.condiments.index].options_id[$smarty.section.condiments_ops.index].id}" 
                  data-unit="{$CONDIMENTS_DATA[$smarty.section.condiments.index].options_id[$smarty.section.condiments_ops.index].option_unit}" 
                  data-mult="{$CONDIMENTS_DATA[$smarty.section.condiments.index].options_id[$smarty.section.condiments_ops.index].price_mult}" 
                  data-name="{$CONDIMENTS_DATA[$smarty.section.condiments.index].options_id[$smarty.section.condiments_ops.index].option_name}" 
                  value="{$CONDIMENTS_DATA[$smarty.section.condiments.index].options_id[$smarty.section.condiments_ops.index].option_name}">{$CONDIMENTS_DATA[$smarty.section.condiments.index].options_id[$smarty.section.condiments_ops.index].option_name}</option>
                  {/section}
                 </select>
                 {/if}
              
              </li>
           {/section}
            
          </ul>
        </div>
        {/if}
        
      </div>
      <div class="button"> <a class="save gallery_save" href="#">SAVE</a> <a class="cancel" href="{$SITE_URL}sandwich">CANCEL</a> </div>
    </div>
  </div>
