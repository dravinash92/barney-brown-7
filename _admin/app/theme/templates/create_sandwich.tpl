<div class="container">
      <!--Header Ending-->
      <!--Create Sandwich Ending-->
      
      
      
      
   
      
      
     <!--  {$SANDWICH_IMAGES} -->
      
      <input type="hidden" value="{$fname}" name="user_fname"> 
      
      <div class="menu-list-wrapper">
        <h1>CREATE A SANDWICH</h1>
        
        
        <div id="ovrlayx" style="position:absolute; display:none; width:960px; height:500px; z-index:1000">

          <div style="position:relative" class="create-sandwich-wrapper">
          <div class="create-sandwich-left">
            <div class="finalize-It-heading">
              <h3>Finalize It!</h3>
            </div>
            <div class="name-your-creation-popup">
              <h2>NAME YOUR CREATION</h2>
             <h3><input type="text" value="{$SANDWICH_NAME}" id="namecreation"></h3>
            </div>
            
            <div class="button-holder" style="margin-top:383px">
              <ul>
                <li> <a href="javaScript:void(0)"  class="edit-sandwich">EDIT SANDWICH</a></li>
              </ul>
            </div>
          </div>
          <div class="create-sandwich-right-wrapper">
            <div class="create-sandwich-right fill-color">
              <div class="final-list-wrapper scroll-bar mCustomScrollbar _mCS_7 mCS-autoHide" style="position: relative; overflow: visible;">
              <div class="mCustomScrollBox mCS-minimal mCSB_vertical mCSB_outside" id="mCSB_7" tabindex="0"><div dir="ltr" style="position: relative; top: -14px; left: 0px;" class="mCSB_container" id="mCSB_7_container">
                <ul id="final_out">
                  
                </ul>
              </div></div><div class="mCSB_scrollTools mCSB_7_scrollbar mCS-minimal mCSB_scrollTools_vertical" id="mCSB_7_scrollbar_vertical" style="display: block;"><div class="mCSB_draggerContainer"><div oncontextmenu="return false;" style="position: absolute; min-height: 30px; display: block; height: 229px; max-height: 231px; top: 12px;" class="mCSB_dragger" id="mCSB_7_dragger_vertical"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div>
            </div>
            <div class="checkbox-holder-final">
              <input type="checkbox" class="menucheck" value="ov_check1" name="check" id="ov_check1">
              <label for="ov_check1">TOAST IT! <span>Yum!</span></label>
            </div>
            <div class="save-button-share">
              <div class="checkbox-holder-final">
                <input type="checkbox" value="ov_check2" name="check"  checked="checked"  id="ov_check2">
                <label for="ov_check2">Add to Sandwich Collection</label>
              </div>
              <a class="share-sandwich" href="#">Share</a> <a class="save-to-my-menu" href="#">SAVE TO MY MENU</a> </div>
            <div class="bottom-buttons">
              <h4 id="totalPrice">$0.00</h4>
              <a class="am-done"  href="javaScript:void(0)">ADD TO CART</a> </div>
          </div>
        </div>
    
  </div>
        
        
        
        
        
        <div class="create-sandwich-wrapper">
          <div class="create-sandwich-left">
            <div class="create-sandwich-menu">
              <ul>
                <li><a href="javaScript:void(0)"  class="active">BREAD</a></li>
                <li><a href="javaScript:void(0)" >PROTEIN</a></li>
                <li><a href="javaScript:void(0)" >CHEESE</a></li>
                <li><a href="javaScript:void(0)" >TOPPINGS</a></li>
                <li><a href="javaScript:void(0)" >CONDIMENTS</a></li>
              </ul>
            </div>
            
        
            
            {$LANDING_PAGE}
            
          
            
            <div class="bread_images">
            
            <span class="bread_image">
            <div class="image-holder"> <img style="display:none" src="{$SITE_URL}app/images/create-sandwich-bread.png" alt="Instagram"> </div>
            </span>
            
            <span class="protein_image">
            </span>
            
            <span class="cheese_image">
            </span>
            
            <span class="topping_image">
            </span>

            <span class="condiments_image">
            </span>
            
            </div>
            <div class="button-holder" style="display:none">
              <ul>
                <li> <a href="javaScript:void(0)" class="back">BACK</a></li>
                <li><a href="javaScript:void(0)" class="next">NEXT</a></li>
              </ul>
            </div>
          </div>
          <div class="create-sandwich-right-wrapper" >
           <div class="optionLoader"><img src="{$SITE_URL}app/images/optionload.gif" /></div>   
            <div class="create-sandwich-right" id="optionList">     
                   
             {$OPTN_DATA}
            </div>
            <div class="bottom-buttons">
              <h4 class="price">${$BASE_FARE}.00</h4>
              <a href="javaScript:void(0)" id="finished" class="am-done link">I'M DONE</a> </div>
          </div>
        </div>
      </div>
      <!--Create Sandwich Ending-->
    </div>
  </div>

</div><!--container-->
