<div class="container">
    <div class="condiment">
      <h1>NEW BREAD</h1>
      <form name="newbread" method="post" action="{$SITE_URL}menu/addCategoryItems" enctype="multipart/form-data">
      <ul>
        <li>
          <label>Name of Bread</label>
          <input type="text" name="item_name" value="">
        </li>
        
        <li>
          <label>Price</label>
          <input type="text" name="item_price" value="">
        </li>
        <li>
          <label>Bread Type</label>
          <select  name="bread_type">
          	<option value="0">None</option>
          	<option value="1">3 foot</option>
          	<option value="2">6 foot</option>
          	
          </select>
        </li>
         <li>
          <label>Bread Shape</label>
          <select  name="bread_shape ">
          	<option value="0">Long</option>
          	<option value="1">Round</option>
          	<option value="2">Trapezoid</option>
          	
          </select>
        </li>
        
        <!-- <li>
          <label>Description (Optional)</label>
          <div gramm="gramm" contenteditable="" style="border: 1px solid transparent; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; box-sizing: content-box; display: block; height: 96px; width: 293.375px; padding: 3px 5px; margin: 0px 0px 8px; position: absolute; color: transparent; white-space: pre-wrap; overflow: hidden; z-index: 0; text-align: left; -webkit-transform: translate(0px, 70px); transform: translate(0px, 70px); background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);" id="b5a5d32d-b1b2-ed53-22fe-f66acdeb77bf" gramm_id="b5a5d32d-b1b2-ed53-22fe-f66acdeb77bf" gr_new_editor="true"><span style="line-height: normal; white-space: pre-wrap; text-align: start; clear: none; box-sizing: border-box; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; display: inline-block; width: 100%; font-family: monospace; font-size: 13px; font-weight: normal; font-style: normal; letter-spacing: normal; text-shadow: none; color: transparent; height: 102px;"></span><br></div><textarea gramm="" txt_gramm_id="4d6efe49-ae3f-c5c5-c33a-461e19cdb53a" gramm_id="c82bf7d2-5c61-2cda-8e5c-815df6eeb63f" spellcheck="false" gr_new_editor="true" style="white-space: pre-wrap; z-index: auto; position: relative; line-height: normal; font-size: 13px; -webkit-transition: none; transition: none; height: 96px; overflow: auto; background: transparent !important;"></textarea><div class="gr-btn-freemium gr-shown" style="display: none; z-index: 2; margin-left: 487px; margin-top: 364px; opacity: 1;">	<div class="gr-btn-status" title="Protected by Grammarly"></div></div>
        </li> -->
        
        <li>
          <label>Image</label>
          <input type="file" class="choose" name="item_image" value="no file selected"> 
        </li>
         <li>
          <label>Image_slice</label>
          <input type="file" class="choose" name="item_image_sliced" value="no file selected"> 
        </li>
        
          <!-- <li>
          <label>Bread Inside</label>
          <input type="file" class="choose" value="no file selected"> 
        </li> -->
        
        
         <li>
          <label>Taxable</label>
          <input type="checkbox" value="1" name="taxable" class="price-menu"  >
        </li>  
        
         <li>
          <label>Premium</label>
          <input type="checkbox" value="1" name="premium" class="price-menu"  >
        </li>  
        
        
        
        <input type="hidden" value="1" name="category_id">
        
        
      </ul>
      
       <div class="button">
       <input type="submit" class="save save_custom_item" value="SAVE">
        <a href="{$SITE_URL}menu/custom" class="cancel">CANCEL</a>
      </div>
      </form>
    </div>
  </div>
