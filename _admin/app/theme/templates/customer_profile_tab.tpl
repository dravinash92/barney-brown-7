<h3>Profile</h3>
        <div class="account">
          <h4>ACCOUNT INFO</h4>
          <ul>
            <li>
               <label>Name: <span>{$udata.first_name}  {$udata.last_name}</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
            <li>
               <label> Email Address: <a href="mailto:{$udata.username}"><span>{$udata.username}<span></a></label>
               <a href="#" class="edit">EDIT</a>
            </li>
             <li>
               <label>Password: <span>xxxxxxx</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
            <!--
             <li>
               <label>Phone: <span>{$udata.phone}</span></label>
               <a href="#" class="edit">EDIT</a>
            </li>
             <li>
               <label>Company: <span>{$udata.company}</span></label>
               <a href="#" class="edit">EDIT</a> 
            </li>-->
          </ul>
        </div>
        <!--
        <div class="tell-us">
          <h4>TELL US ABOUT YOUR EXPERIENCE</h4>
        <form id="user_review">
          <ul>
            <li>
               <label>How did you hear about us?  </label>
               <div class="bgforSelect">
                <select class="wid-input" name="how_know">
                  <option value="">Choose One</option>
                  
                  
                  {section name=users_review_content loop=$user_review_contents } 
                   <option value="{$user_review_contents[users_review_content].id}" {if $user_review.how_know eq $user_review_contents[users_review_content].id} selected="selected" {/if}>{$user_review_contents[users_review_content].review_text}</option>
                   {/section}
                   </select>
              </div>
            </li>
           <li>
               <label>How would you rate your ordering experience?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_order" >
                  <option value="">Choose One</option>

                <option {if $user_review.rating_order eq 1 } selected="selected" {/if}  value="1">1</option>
                <option {if $user_review.rating_order eq 2 } selected="selected" {/if}  value="2">2</option>
                <option {if $user_review.rating_order eq 3 } selected="selected" {/if}  value="3">3</option>
                <option {if $user_review.rating_order eq 4 } selected="selected" {/if}  value="4">4</option>
                <option {if $user_review.rating_order eq 5 } selected="selected" {/if}  value="5">5</option>
                <option {if $user_review.rating_order eq 6 } selected="selected" {/if}  value="6">6</option>
                <option {if $user_review.rating_order eq 7 } selected="selected" {/if}  value="7">7</option>
                <option {if $user_review.rating_order eq 8 } selected="selected" {/if}  value="8">8</option>
                <option {if $user_review.rating_order eq 9 } selected="selected" {/if}  value="9">9</option>
                <option {if $user_review.rating_order eq 10 } selected="selected" {/if}  value="10">10</option>
                
                </select>
              </div>
            </li>
             <li>
               <label>How would you rate your customer service experience?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_customer_service" >
                  <option value="">Choose One</option>
                
                <option {if $user_review.rating_customer_service eq 1 } selected="selected" {/if}  value="1">1</option>
                <option {if $user_review.rating_customer_service eq 2 } selected="selected" {/if}  value="2">2</option>
                <option {if $user_review.rating_customer_service eq 3 } selected="selected" {/if}  value="3">3</option>
                <option {if $user_review.rating_customer_service eq 4 } selected="selected" {/if}  value="4">4</option>
                <option {if $user_review.rating_customer_service eq 5 } selected="selected" {/if}  value="5">5</option>
                <option {if $user_review.rating_customer_service eq 6 } selected="selected" {/if}  value="6">6</option>
                <option {if $user_review.rating_customer_service eq 7 } selected="selected" {/if}  value="7">7</option>
                <option {if $user_review.rating_customer_service eq 8 } selected="selected" {/if}  value="8">8</option>
                <option {if $user_review.rating_customer_service eq 9 } selected="selected" {/if}  value="9">9</option>
                <option {if $user_review.rating_customer_service eq 10 } selected="selected" {/if}  value="10">10</option>
                
                
                </select>
              </div>
            </li>
            
            <li>
               <label>How would you rate the quality of your food?</label>
               <div class="bgforSelect">
                <select class="wid-input" name="rating_food" >
                  <option value="">Choose One</option>
                 
                <option {if $user_review.rating_food eq 1 } selected="selected" {/if}  value="1">1</option>
                <option {if $user_review.rating_food eq 2 } selected="selected" {/if}  value="2">2</option>
                <option {if $user_review.rating_food eq 3 } selected="selected" {/if}  value="3">3</option>
                <option {if $user_review.rating_food eq 4 } selected="selected" {/if}  value="4">4</option>
                <option {if $user_review.rating_food eq 5 } selected="selected" {/if}  value="5">5</option>
                <option {if $user_review.rating_food eq 6 } selected="selected" {/if}  value="6">6</option>
                <option {if $user_review.rating_food eq 7 } selected="selected" {/if}  value="7">7</option>
                <option {if $user_review.rating_food eq 8 } selected="selected" {/if}  value="8">8</option>
                <option {if $user_review.rating_food eq 9 } selected="selected" {/if}  value="9">9</option>
                <option {if $user_review.rating_food eq 10 } selected="selected" {/if}  value="10">10</option>
                
                
                </select>
              </div>
            </li>
            <li>
				<label>What can we do to be better?</label>
				
			
				{section name="reviewmessages" loop=$user_review_messages}
				<p>{$user_review_messages[reviewmessages].message}</p> <br>
				{/section}
				
            </li>
            <li>
				<input type="hidden" name="uid" value="{$udata.uid}" />
				<input type="hidden" name="id" value="{$user_review.id}" />
				<input type="button" class="submit savereview" name="savereview"  value="Submit" />
            </li>
          </ul>
          </form>
          </ul>
        </div>-->
      
     
