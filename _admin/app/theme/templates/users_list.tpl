<div class="container">
    <div class="admin-user-account">
       <h1>USER ACCOUNTS</h1>
       <form id="updateUsersLive" method="post" action="{$SITE_URL}accounts/updateUsersLive">
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="7%">&nbsp;</th>
          <th colspan="1">USER</th>
          <th colspan="3">ACCOUNT TYPE</th>
        </tr>
      
       {foreach from=$admin_users key=k item=v}
        <tr>
          <td width="7%" align="center">
              <span class="multi-left">
               <input type="checkbox" id="checkbox-{$key}-{$v->uid}" class="input-checkbox" name="live[{$v->uid}]" value="1" {if $v->live eq 1} checked {/if}>
               <label for="checkbox-{$key}-{$v->uid}" class="multisel-ckeck"></label>
              </span>
          </td>
          <td width="43%">{$v->first_name}  {$v->last_name}</td>
          <td width="33%">{$v->cat_name}</td>
          <td width="5%"><a href="{$SITE_URL}accounts/editAdminUser/{$v->uid}">edit</a></td>
          <td width="12%"><a href="{$SITE_URL}accounts/deleteAdminUser/{$v->uid}">delete</a></td>
        </tr>
       {/foreach}

      </tbody></table>
      <a class="update" href="#" onclick="document.getElementById('updateUsersLive').submit()">update</a>
      <a class="add-user" href="{$SITE_URL}accounts/newUser/">ADD NEW USER</a>
      </form>
    </div>
  </div>
