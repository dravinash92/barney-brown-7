<div class="container">
    <div class="item-report">
      <h1>TOTAL SALES</h1>
      <div class="filter_wrap">
      <form name="totals_sales_report_filter">
      <div class="store-location">
         <label>Name of Stores</label>
         <select name="pickup_store">
           <option value="">Select Store</option>
			{foreach from=$pickupstores key=k item=v}
				<option value="{$v->id}">{$v->store_name}, {$v->address1}, {$v->address2}, {$v->zip}</option>
			{/foreach }
         </select>
        <ul>
          <li><label>Order Type</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-2-5" class="input-checkbox" name="order_type[]" value="true">
            <label for="checkbox-2-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Delivery</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-5" class="input-checkbox" name="order_type[]" value="false">
            <label for="checkbox-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Pick-Up</span>
          </li>
        </ul>
        <ul>
          <li><label>Order Status</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-3-1" class="input-checkbox" name="order_status[]" value="0,0">
            <label for="checkbox-3-1" class="multisel-ckeck"></label>
            </span> <span class="radio-text">New</span> </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-2" class="input-checkbox" name="order_status[]" value="1,5">
            <label for="checkbox-3-2" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Processed</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-3" class="input-checkbox" name="order_status[]" value="2,6">
            <label for="checkbox-3-3" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Ready</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-4" class="input-checkbox" name="order_status[]" value="3,7">
            <label for="checkbox-3-4" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Out</span>
          </li>
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-5" class="input-checkbox" name="order_status[]" value="4,8">
            <label for="checkbox-3-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Received</span>
          </li>
        </ul>
      </div>
      
      <div class="date-time-settings">
         <label>Date/Time Settings</label>
         <ul>
           <li>
            <div class="radio-left">
             <div class="radio-section">
                <input type="radio" value="order_date"  class="input-radio" name="order_date_filter" id="1">
                <label for="1"></label>
              </div>
              <span class="radio-text">Order Placement</span>
             </div>
             <div class="radio-right">
              <div class="radio-section">
                  <input type="radio" value="date" checked class="input-radio" name="order_date_filter" id="2">
                <label for="2"></label>
                </div>
                <span class="radio-text">Order Fulfillment</span>
              </div>
           </li>
           <li>
             <label>From</label>
          <input type="text" class="expiration" placeholder="Select date" name="from_date" value="{$from_date}"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar.png"></a>
            <select name="from_time" name="from_time">
				{$fromTimes}
             </select>
           </li>
            <li>
             <label>To</label>
          <input type="text" class="expiration" placeholder="Select date" name="to_date" value="{$to_date}"> <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar.png"></a>
            <select class="to_time" name="to_time">
				{$toTimes}				
             </select>
           </li>
         </ul>
      </div>
      
      <div class="type-purchase-report">
        <ul>
          <li><label>Type of Purchase</label></li>
          <li>
           <span class="multi-left">
           <input type="checkbox" id="checkbox-4-5" class="input-checkbox" name="type_purchase[]" value="0">
            <label for="checkbox-4-5" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Website</span> </li>
            
          <li>
            <span class="multi-left">
            <input type="checkbox" id="checkbox-3-8" class="input-checkbox" name="type_purchase[]" value="1">
            <label for="checkbox-3-8" class="multisel-ckeck"></label>
            </span> <span class="radio-text">Phone Order</span>
          </li>
          
        </ul>
        
      </div>
      
      <div class="type-purchase-discount">
        <ul>
          <li><label>Discounts</label></li>
          <li>
             <select name="discount">
              <option value="">Select Discount</option>
				{foreach from=$discounts key=k item=v}
				<option value="{$v->id}">{$v->name}, {$v->code}</option>
				{/foreach }
             </select>
           </li>
           <li><label>Refund Types</label></li>
          <li>
             <select name="refund_types">
              <option value="">Select Refund Types</option>
              <option value="">Refund Types</option>
             </select>
           </li>
           
          <li><label>System User</label></li>
          <li>
             <select name="system_user">
              <option value="">Select User</option>
				{foreach from=$systemusers key=k item=v}
					<option value="{$v->uid}">{$v->first_name} {$v->last_name}</option>
				{/foreach }
             </select>
           </li>
           
           <li><label>Search</label></li>
           <li>
             <input type="text" name="search_text" >
             <a href="javascript:void(0)" class="do_total_sales_filter"><img width="26" height="26" alt="" src="{$SITE_URL}app/images/link.png"></a>
          </li>          
        </ul>
         
      </div>
      </form>
		</div>
		<div class="reports_table">
			{if $orders|@count gt 0}
			{php}	
				$items_total= $tax_total = $sub_total  = $delivery_fee_total = $tip_total = $total = $dscnt_total = $gft_total = $refund = $paid = 0;
			{/php}
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody>
			<tr>
			  <th>ORDER #</th>
			  <th>DATE/TIME</th>
			  <th>CUSTOMER NAME</th>
			  <!-- <th>ITEMS</th> -->
			  <th>SUBTOT</th>
			  <th>TAX</th>
        <th>DELIVERY FEE</th>
        <th>TIP</th> 
			  <th>TOTAL VALUE</th>
			  <th>DSCNT</th>
			  <th>GFT CRD</th>
			  <th>REFUND</th>
			  <th>TOTAL PAID</th>
			</tr>
			
			{foreach from=$orders key=k item=v}
				{assign var='item_total' value=$v->item_total-$v->total_tax}				
				{assign var='tax' value=$v->total_tax}
				{assign var='sub' value=$v->sub_total}
        {assign var='delivery_fee' value=$v->delivery_fee}
				{assign var='tip' value=$v->tip}
				{assign var='calc_total' value=$v->calc_total}
				{assign var='off_amount' value=$v->off_amount}
				{assign var='total' value=$v->total}
				{php}
					$items_total += $_smarty_tpl->get_template_vars('item_total');
					$tax_total += $_smarty_tpl->get_template_vars('tax');
					$sub_total += $_smarty_tpl->get_template_vars('sub');
          $delivery_fee_total += $_smarty_tpl->get_template_vars('delivery_fee');
					$tip_total += $_smarty_tpl->get_template_vars('tip');
					$calc_total += $_smarty_tpl->get_template_vars('calc_total');
					$dscnt_total += $_smarty_tpl->get_template_vars('off_amount');
					$paid += $_smarty_tpl->get_template_vars('total');
				{/php}			
			{/foreach}
			
			<tr class="subheader">
			  <td colspan="3">TOTAL</td>
			  <!-- <td>${php}echo sprintf('%0.2f',$items_total){/php}</td> -->
			  <td>${php}echo sprintf('%0.2f',$sub_total){/php}</td>
			  <td>${php}echo sprintf('%0.2f',$tax_total){/php}</td>
        <td>${php}echo sprintf('%0.2f',$delivery_fee_total){/php}</td>
			  <td>${php}echo sprintf('%0.2f',$tip_total){/php}</td>
			  <td>${php}echo sprintf('%0.2f',$calc_total+$tax_total){/php}</td>
			  <td>-${php}echo sprintf('%0.2f',$dscnt_total){/php}</td>
			  <td>-$0.00</td>
			  <td>-$0.00</td>
			  <td>${php}echo sprintf('%0.2f',$calc_total+$tax_total-$dscnt_total){/php}</td>
			</tr>			
			
			
			{foreach from=$orders key=k item=v}
				{if $k is not even} 
				<tr class="">
				{else}	
				<tr class="even">
				{/if}	
				  <td><a href="{$SITE_URL}reports/showOrderDetails/{$v->order_id}">{$v->order_number}</a></td>
				  <td>{$v->o_date}</td>
				  <td>{$v->first_name} {$v->last_name}</td>
				  <!-- <td>${$v->amount_wt|string_format:"%.2f"}</td> -->
				  <td>${$v->sub_total|string_format:"%.2f"}</td>
				  <td>${$v->total_tax|string_format:"%.2f"}</td>
          <td>${$v->delivery_fee}</td>
				  <td>${$v->tip}</td>
				  <td>${$v->calc_total+$v->total_tax|string_format:"%.2f"}</td>
				  <td>-${$v->off_amount|string_format:"%.2f"}</td>
				  <td>-$0.00</td>
				  <td>-$0.00</td>
				  <td>${$v->total|string_format:"%.2f"}</td>
				</tr>				
			{/foreach }					
			
			</tbody>
		</table>	
		{else}
			<h3>No orders found.</h3>	
		{/if}	
		</div>	
		
    </div>
  </div>
