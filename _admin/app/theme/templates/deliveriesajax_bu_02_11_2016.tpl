<div class="container">
    
 {if $total_delivery ne 0 } 
       <div class="deliveries-table-detail-wrapper">
       	 <h2>NEW</h2>
         <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
          
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>CANCEL</th>
          </tr>
          
        
            {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 0}
          
          <tr { if $v->time eq 1 } style="background:rgb(218, 245, 255);" {/if}>
            <td>{$v->name}   
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDet">View order details</a></p>
            </td>
            <td>
            	<p>{$v->address->name}<br>
					{if $v->address->company|strip}{$v->address->company}</br>{/if}
					{if $v->address->phone}{$v->address->phone}<br>{/if}              
					{if $v->address->address1|strip}{$v->address->address1}</br>{/if}
					{if $v->address->address2|strip}{$v->address->address2}</br>{/if}
					{if $v->address->zip|strip}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
				<p>({$item->qty}) {$item->name} </p>
			{/foreach}
             
            </td>
        
		       {foreach from=$getOrderSatusList key=k item=b}  
		             {if $b->code ne 'Delivery_Cancel'}
		             <td><a id="{$v->order_number}" class="viewOrdDet" {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},{$b->id}{if $b->id eq 5},{$v->order_number}{/if})" {/if} href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box1.png" alt="">
		             
		             </td>
		             {/if}
		           {if $b->code eq 'Delivery_Cancel'}
		           <td>	<a id="{$v->order_number}" class="viewOrdDet" {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},{$b->id})" {/if} href="javascript:void(0)" >Cancel</a></td>
		           {/if}
			        
		        {/foreach}
         
          </tr>  
         {/if}
       {/foreach}
          
          
        </tbody></table>
				<h2>PROCESSED</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
       
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>CANCEL</th>
          </tr>
          {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 5}
           <tr>
            <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
               <p><a  href="#" id="{$v->order_number}" class="viewOrdDet">View order details</a></p>
            </td>
            <td>
            	<p>{$v->address->name}<br>
					{if $v->address->company|strip}{$v->address->company}</br>{/if}
					{if $v->address->phone}{$v->address->phone}<br>{/if}              
					{if $v->address->address1|strip}{$v->address->address1}</br>{/if}
					{if $v->address->address2|strip}{$v->address->address2}</br>{/if}
					{if $v->address->zip|strip}{$v->address->zip}</br>{/if}
              
				</p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
				<p>({$item->qty}) {$item->name} </p>
			{/foreach}
             
            </td>
        
		    
		         {foreach from=$getOrderSatusList key=k item=b}  
		            {if $v->order_status eq 5 && $b->id eq 5}
           			<td><a  {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},0)" {/if} href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
           		{/if}
           		{if $b->code ne 'Delivery_Cancel' && $b->code ne 'Delivery_Pro'}
             		<td><a id="{$v->order_number}" class="viewOrdDet" {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},{$b->id})" {/if} href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
             	{/if}
	           {if $b->code eq 'Delivery_Cancel'}
	          		 <td>	<a id="{$v->order_number}" class="viewOrdDet" {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},{$b->id})" {/if} href="javascript:void(0)">Cancel</a></td>
	           {/if}
		        {/foreach}
			 
          </tr>  
         {/if}
       {/foreach}
         
          
        </tbody></table>
        <h2>OUT FOR DELIVERY</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>   
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>CANCEL</th>
          </tr>
            {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 6}
          <tr>
            <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
               <p><a class="viewOrdDet" href="#" id="{$v->order_number}" class="viewOrdDet">View order details</a></p>
            </td>
            <td>
            	<p>{$v->address->name}<br>
					{if $v->address->company|strip}{$v->address->company}</br>{/if}
					{if $v->address->phone}{$v->address->phone}<br>{/if}              
					{if $v->address->address1|strip}{$v->address->address1}</br>{/if}
					{if $v->address->address2|strip}{$v->address->address2}</br>{/if}
					{if $v->address->zip|strip}{$v->address->zip}</br>{/if}
              
				</p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
				<p>({$item->qty}) {$item->name} </p>
			{/foreach}
             
            </td>
        
		       {foreach from=$getOrderSatusList key=k item=b}  
		    			{if $v->order_status eq 6 && $b->id eq 6}
           			  <td><a  {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},5)" {/if} href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a>
           			  </td>
           			   <td><a  {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},5)" {/if} href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a>
           			   </td>
           			{/if}
           		{if $b->code ne 'Delivery_Cancel' && $b->code ne 'Delivery_Pro' && $b->code ne 'Delivery_out'}
             <td><a id="{$v->order_number}" class="viewOrdDet" {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},{$b->id})" {/if} href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
             {/if}
           {if $b->code eq 'Delivery_Cancel'}
           <td>	<a id="{$v->order_number}" class="viewOrdDet" {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},{$b->id})" {/if} href="javascript:void(0)" >Cancel</a></td>
           {/if}
			        
		        {/foreach}
         
          </tr>  
         {/if}
       {/foreach}
        
          
        </tbody></table>
        <h2>DELIVERED</h2>
        <table class="deliveries-new-table border-bottom">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
           
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>&nbsp;</th>
          </tr>
         {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 7}
           <tr>
            <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
               <p><a  href="#" id="{$v->order_number}" class="viewOrdDet">View order details</a></p>
            </td>
            <td>
            	<p>{$v->address->name}<br>
					{if $v->address->company|strip}{$v->address->company}</br>{/if}
					{if $v->address->phone}{$v->address->phone}<br>{/if}              
					{if $v->address->address1|strip}{$v->address->address1}</br>{/if}
					{if $v->address->address2|strip}{$v->address->address2}</br>{/if}
					{if $v->address->zip|strip}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
				<p>({$item->qty}) {$item->name} </p>
			{/foreach}
             
            </td>
        
		    <td><a  {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},6)" {/if} href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td><a  {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},6)" {/if} href="javascript:void(0)" ><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td><a  {if $isStore eq 0}  onclick="manageDelivereis({$v->order_id},6)" {/if} href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td></td>
          </tr>  
         {/if}
       {/foreach}
        
          
        </tbody></table>
       </div>
       {/if}
      {if $total_delivery eq 0 }<div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div>{/if}
    </div>
  </div>

