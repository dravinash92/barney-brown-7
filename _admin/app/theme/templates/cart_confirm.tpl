<div class="container">
    <div class="place-order">
       <h1>PLACE ORDER</h1>
       
       <div class="place-order-coustomer-info">
       		<div class="address-details">
           <h3>CUSTOMER INFO</h3>
           <p>Name: Matthew Baer<br>
              Email: mbaer201@gmail.com<br>
              Phone: 201-787-5073<br>
              Total Orders: 24<br>
              Total Sales: $450.00</p>
           <a href="#" class="edit-info">edit info</a>
         </div>
         
         <div class="customer-notes">
         		<h3>CUSTOMER NOTES</h3>
            <p><span>3/15/14</span><span class="address">Places corporate orders for AOL, Time Warner, and Sony</span></p>
         		<a href="#" class="remove">Remove</a>
            <a href="#" class="add-new-note">add new note</a>
         </div>
         
         <div class="date-time-order">
         		<h3>DATE/TIME OF ORDER</h3>
            <div class="radio-holder">
            	<input type="radio" class="css-checkbox" id="radio1" name="radiog_lite">
              <label class="css-label" for="radio1">Now</label>
              <input type="radio" class="css-checkbox" id="radio2" name="radiog_lite">
              <label class="css-label" for="radio2">Specific Date/Time</label>
            </div>
            
            <div class="text-box-holder">
            	<label>Date</label>
              <input type="text" class="text-box" name="">
              <a class="date-icon" href="#"></a>
            </div>
            <div class="text-box-holder">
            	<label>Time</label>
              <select name="" class="select">
              	<option>12:00 PM</option>
              </select>
            </div>
         </div>
         
         <div class="pickup-deliver-order">
         		<div class="radio-holder">
            	<input type="radio" class="css-checkbox" id="radio3" name="radiog_lite">
              <label class="css-label" for="radio3">DELIVERY</label>
              <input type="radio" class="css-checkbox" id="radio4" name="radiog_lite">
              <label class="css-label" for="radio4">PICK-UP</label>
            </div>
           <div class="text-box-holder">
            <label>Address</label>
            <select name="" class="select">
              <option>168 W 25TH ST</option>
            </select>
          </div>
          <a href="#" class="add-new-address">add new address</a>
          <a href="#" class="add-new-address">edit addresses</a>
         </div>
         
       </div>
      
      <div class="saved-sandwiches-new">
      	<h2>SAVED SANDWICHES</h2>
        
        	<table class="saved-sandwiches-table">
            <tbody><tr>
              <td>
              	<p><span>MATT'S TURKEY SURPRISE</span> ________________________________________________________________________________________________</p>
                <p>Baguette, Turkey (Double), Swiss, Lettuce, Tomato, Mayonnaise</p>
              </td>
              <td>$10.00</td>
              <td><a href="#" class="add-to-cart">ADD TO CART</a></td>
            </tr>
            <tr>
              <td>
              	<p><span>MATT'S ROAST BEEF THRILL</span> ______________________________________________________________________________________________</p>
                <p>Baguette, Roast Beef (Double), Swiss, Lettuce, Tomato, Mayonnaise</p>
              </td>
              <td>$10.00</td>
              <td><a href="#" class="add-to-cart">ADD TO CART</a></td>
            </tr>
          </tbody></table>

          
      </div>
      
      <div class="custom-sandwich-wrapper">
       		<h2>CUSTOM SANDWICH</h2>
          
          <div class="button-holder">
          	<div class="check-box-holder">
            	<input type="checkbox" id="checkbox-5-12" class="input-checkbox" name="">
              <label for="checkbox-5-12" class="multisel-ckeck"></label>
              <span>Save</span>
            </div>
            <div class="sandwich-name">
            	<label>Sandwich Name</label>
              <input type="text" class="text-box" name="">
            </div>
            <div class="radio-holder">
             <input type="radio" value="Redirect" class="input-radio" name="Redirect" id="6">
             <label for="6"></label>
             <span>Public</span>
           </div>
           <div class="radio-holder">
             <input type="radio" value="Redirect" class="input-radio" name="Redirect" id="7">
             <label for="7"></label>
             <span>Private</span>
           </div>
           <a class="add-cart" href="#">ADD TO CART</a>
          </div>
          
       </div>
      
      <div class="saved-sandwiches-new">
      	<h2>SANDWICH GALLERY</h2>
        	
          <div class="text-box-holder">
          	<input type="text" class="text-box" name="" placeholder="Search Sandwich Name, ID, Creator">
            <a class="shopping-cart-select" href="#"></a>
          </div>
        
        	<table class="saved-sandwiches-table">
            <tbody><tr>
              <td>
              	<p><span>JIM'S TURKEY SURPRISE</span> __________________________________________________________________________________________________</p>
                <p>Baguette, Turkey (Double), Swiss, Lettuce, Tomato, Mayonnaise</p>
              </td>
              <td>$10.00</td>
              <td><a href="#" class="add-to-cart">ADD TO CART</a></td>
            </tr>
            <tr>
              <td>
              	<p><span>MARY'S TURKEY EXPLOSION</span> ______________________________________________________________________________________________</p>
                <p>Baguette, Turkey (Double), Swiss, Lettuce, Tomato, Mayonnaise</p>
              </td>
              <td>$10.00</td>
              <td><a href="#" class="add-to-cart">ADD TO CART</a></td>
            </tr>
          </tbody></table>

          
      </div>
      
      
      <div class="sides-drink-wrapper">
          <div class="sides-wrapper">
          	<h2>SIDES</h2>
            <ul class="sides-list">
               <li>
                 <h3>Side 1 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Side 2 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Side 3 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Side 4 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               <li>
                 <h3>Side 5 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               <li>
                 <h3>Side 6 ______________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
         	</ul>
          </div>
          <div class="drink-wrapper">
          	<h2>DRINKS</h2>
            
            <ul class="sides-list">
               <li>
                 <h3>Drink 1 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Drink 2 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Drink 3 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               
               <li>
                 <h3>Drink 4 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               <li>
                 <h3>Drink 5 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
               <li>
                 <h3>Drink 6 _____________</h3>
                 <h4>$2.00</h4>
                 <span class="arrow-holder">
                   <a href="#" class="arrow-left"></a>
                   <input type="text" name="" value="0">
                   <a href="#" class="arrow-right"></a>
                 </span>
                 <a href="#" class="add-cart">ADD TO CART</a>
               </li>
         	</ul>
            
          </div>
       </div>
      
      
      <div class="shopping-cart">
     		<h3>SHOPPING CART</h3>
          
         <table class="shopping-cart-table-new">
           <tbody><tr>
             <th>ORDER DETAILS</th>
             <th>&nbsp;</th>
             <th>QTY</th>
             <th>TOTAL</th>
             <th>&nbsp;</th>
           </tr>
           <tr>
             <td colspan="2">
             	 <p>CUSTOM SANDWICH _______________________________________________________________________________________________</p>
               <p>Baguette, Smoked Turkey, Swiss Cheese, Lettuce, Tomato, Onion, Light Mayonnaise</p>
             </td>
             <td>
             	<span class="arrow-holder">
               <a href="#" class="arrow-left"></a>
               <input type="text" name="" value="0">
               <a href="#" class="arrow-right"></a>
             </span>
             </td>
             <td>$10.00</td>
             <td><a href="#" class="remove">REMOVE</a></td>
           </tr>
           <tr>
             <td colspan="2"><p>SIDE 1 ___________________________________________________________________________________________________________</p></td>
             <td>
             	<span class="arrow-holder">
               <a href="#" class="arrow-left"></a>
               <input type="text" name="" value="0">
               <a href="#" class="arrow-right"></a>
             </span>
             </td>
             <td>$2.00</td>
             <td><a href="#" class="remove">REMOVE</a></td>
           </tr>
           <tr>
             <td colspan="2">DRINK 1 __________________________________________________________________________________________________________</td>
             <td>
             	<span class="arrow-holder">
               <a href="#" class="arrow-left"></a>
               <input type="text" name="" value="0">
               <a href="#" class="arrow-right"></a>
             </span>
             </td>
             <td>$2.00</td>
             <td><a href="#" class="remove">REMOVE</a></td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">SUBTOTAL</td>
            <td>$14.00 </td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">TIP</td>
            <td colspan="2">
              <select name="" class="select-tip">
                <option>$3.00</option>
              </select>
            </td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL VALUE</h4></td>
            <td><h4>$17.00</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">DISCOUNT:  30% OFF SPECIAL</td>
            <td>-$4.20</td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">DISCOUNT / GIFT CARD4</p>
            	<span class="text-box-holder">
              	<input name="" type="text" class="text-box">
                <a href="#" class="shopping-cart-select"></a>
              </span>
            </td>
           </tr>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">MANUAL DISCOUNT</p>
            	<span class="text-box-holder">
              	<select name="" class="select">
                	<option>Select</option>
             		</select>
                <a href="#" class="shopping-cart-select"></a>
              </span>
            </td>
           </tr>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL OWED</h4></td>
            <td><h4>$12.80</h4></td>
            <td>&nbsp;</td>
           </tr>
         </tbody></table>
			</div>
       
       <div class="billing-information-holder">
       		<h3>BILLING INFORMATION</h3>
          <div class="text-box-holder ">
          	<select name="" class="billing-select">
              <option>Visa ending in 1089</option>
            </select>
          </div>
          <a href="#" class="edit-bill">add new billing</a>
          <a href="{$SITE_URL}neworder/cartModify/" class="edit-bill">edit billing</a>
          <a href="#" class="place-order">PLACE ORDER</a>
       </div>
       
    </div>
  </div>