<div class="container">
    <div class="condiment">
      <h1>NEW STANDARD CATEGORY</h1>
      <form name="newStandardCategory" method="post" action="{$SITE_URL}menu/addStandardCategoryItems" enctype="multipart/form-data">
      <ul>
        <li>
          <label>Name of Category</label>
          <input type="text" name="category_name" value="">
        </li>
        <li>
          <label>Type of Category</label>
			<label style="display:inline-block; width:50%">Standard :</label>	<input type="radio" name="type" value="standard" checked style="display:inline-block; width:50%"/><br/>
			<label style="display:inline-block; width:50%">Catering :</label>  <input type="radio" name="type" value="catering" style="display:inline-block; width:50%" />
        </li>

      </ul>
      
       <div class="button">
        <input type="submit" class="save" value="SAVE">
        <a href="{$SITE_URL}menu" class="cancel">CANCEL</a>
      </div>
      </form>
    </div>
  </div> 
