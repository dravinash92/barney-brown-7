<div class="container">
    <div class="user-account">
       <h1>EDIT USER ACCOUNT</h1>
    
		   <form action="{$SITE_URL}accounts/updateAdminUser" method="POST">
		             {foreach from=$adminData key=k item=v}
		          <label>Name</label>
		          <input name="admin_user_name" type="text" value="{$v->first_name}">
		          <input type="hidden" name="hidden_uid" value="{$v->uid}">
		          <label>Username</label>
		          <input name="admin_user_uname" type="text" value="{$v->user_name}">
		          <label>Password</label>
		          <input name="admin_user_pwd" type="password">
		          <label>Confirm Password</label>
		          <input name="admin_user_cpwd" type="password" value="">
		          <label>Access Level</label>
		          <select name="access_level">
		          	<option value="">--Please Select--</option>
		        	{foreach from=$admin_user_category key=k item=b}  
		            <option value="{$b->id}" {if $b->id eq $v->admin_cat_id} selected {/if}>{$b->cat_name}</option>
		            {/foreach}
		          </select><br/><br/><br/>
		          <input type="submit" name="submit" value="UPDATE USER">
		           {/foreach}
		       </form>
      
    </div>
  </div>
