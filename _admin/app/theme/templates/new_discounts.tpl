<div class="container">
     <div class="new-discount">
 <form name="discount" id="addDiscount" method="post" action="{$SITE_URL}discounts/addDiscounts" enctype="multipart/form-data"> 
        <h1>NEW DISCOUNT</h1>
        <ul>
        <li>
          <label>Name of Discount</label>
          <input type="text" name="name" value="{$data.name}"> <span class="error_msg">{$error.name}</span>

        </li>
        
         <li>
          <label>Discount Code</label>
          <input type="text" name="code" value="{$data.code}"><span class="error_msg">{$error.code}</span>

        </li>
           
        <li>
          <label>Discount Type</label>
          <div class="radio-section">
            <input type="radio" value="0" class="input-radio" {if $data.type == 0}checked{/if} name="type" id="discount-1">
            <label for="discount-1"></label>
            <span class="radio-text">% Off</span>
            
            <input type="radio" value="1" class="input-radio" name="type" id="discount-2" {if $data.type == 1}checked{/if} >
            <label for="discount-2"></label>
            <span class="radio-text">Fixed Amount</span> </div>
            
            <div class="off-wrapper">
              <input type="text"  name ="discount_amount" value="{$data.discount_amount}" class="off-value">
              <span>% Off</span>

            </div>
              <span class="error_msg">{$error.discount_amount}</span>
        </li>
        <li>
          <label>Applicable To</label>
          <div class="radio-section">
            <input type="radio" value="0" class="input-radio" checked="" name="applicable_to" id="discount-3" {if $data.applicable_to == 0}checked{/if} >
            <label for="discount-3"></label>
            <span class="radio-text">Sub-total</span>
            
            <input type="radio" value="1" class="input-radio" name="applicable_to" id="discount-4" {if $data.applicable_to == 1}checked{/if}  >
            <label for="discount-4"></label>
            <span class="radio-text">Total</span> </div>
        </li>
        <li>
          <label>Usage</label>
          <div class="radio-section">
            <input type="radio" value="0" class="input-radio" checked="" name="uses_type" id="discount-5" {if $data.uses_type == 0}checked{/if} >
            <label for="discount-5"></label>
            <span class="radio-text">Limited Usage</span>
            
            <input type="radio" value="1" class="input-radio" name="uses_type" id="discount-6"  {if $data.uses_type == 1}checked{/if} >
            <label for="discount-6"></label>
            <span class="radio-text">Unlimited Usage</span> </div>
            
            <div class="off-wrapper">
              <input type="text" name="uses_amount" value="{$data.uses_amount}" class="off-value">
              <span>Uses</span>

            </div>
             <span class="error_msg"> {$error.uses_amount}</span>
        </li>
        <li>
          <div class="radio-section">
            <span class="multi-left">
               <input type="checkbox" {if $data.one_per_user == 1} checked="checked" {/if} value = "1" id="checkbox-2-1" class="input-checkbox" name="one_per_user"    >
               <label for="checkbox-2-1" class="multisel-ckeck"></label>
            </span>
            <span class="radio-text">Limit to 1 use per account</span></div>            
            
            <!--<span class="error_msg">{$error.minimum_order_amount}</span>-->
        </li>
        <li style="float: right;margin-top: 32px;">
          <div class="radio-section">
            <span class="multi-left">
               <input type="checkbox" {if $data.maximum_order == 1} checked="checked" {/if} value = "1" id="checkbox-2-3" class="input-checkbox" name="maximum_order"    >
               <label for="checkbox-2-3" class="multisel-ckeck"></label>
            </span>
            <span class="radio-text">Maximum Order Amount</span></div>
            
            <div class="off-wrapper">
              <input type="text" value="{$data.maximum_order_amount}" class="off-value" name="maximum_order_amount">

            </div>
              <span class="error_msg">{$error.maximum_order_amount}</span>
        </li>
		<li style="float: left;height: auto;">
          <div class="radio-section">
            <span class="multi-left">
               <input type="checkbox" {if $data.minimum_order == 1} checked="checked" {/if} value = "1" id="checkbox-2-2" class="input-checkbox" name="minimum_order"    >
               <label for="checkbox-2-2" class="multisel-ckeck"></label>
            </span>
            <span class="radio-text">Minumum Order Amount</span></div>
            
            <div class="off-wrapper">
              <input type="text" value="{$data.minimum_order_amount}" class="off-value" name="minimum_order_amount">

            </div>
              <span class="error_msg">{$error.minimum_order_amount}</span>
        </li>
        <li>
          <label>Expiration</label>
          <input type="text" class="expiration" placeholder="{$smarty.now|date_format:"%a, %b %e, %Y"}" value="{$data.expiration}" name="expiration" > <a class="calendar" href="#"><img width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar.png"></a>
<span class="error_msg">{$error.expiration}</span>
        </li>
        
      </ul>
      
      <div class="button">
        <a class="save" href="#" onclick="document.getElementById('addDiscount').submit();" >SAVE</a>
        <a class="cancel" href="{$SITE_URL}Discounts/">CANCEL</a>
      </div>

 </form>     
     </div>

     
  </div>
