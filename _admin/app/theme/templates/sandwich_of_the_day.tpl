<div class="container">
	<div class="sandwich-gallery">
		<div class="heading">
         <h1>SANDWICH OF THE DAY</h1>
         <a class="add_sotd_button">ADD NEW</a>
        </div>
        {if $sandwiches|@count gt 0}
        <table class="listing">
			<tbody>
				<tr class="static">
				  <th width="25%" height="31">DATE SCHEDULED</th>
				  <th width="25%" height="31">CUSTOM SANDWICHES</th>
				  <th width="20%" height="31">USER</th>
				  <th width="15%">PRICE</th>				  
				  <th width="15%">        </th>				  
				</tr>
				
				{foreach from=$sandwiches key=k item=sandwich }
				<tr class="{cycle values="odd,even"}">
					<td><input name="sotd_date" id="sotd_date{$k}" type="text" placeholder="{$sandwich->sotd_date|date_format}" value="{$sandwich->sotd_date|date_format}" class="text-box" readonly />
					<a class="calendar" href="#"><img class="calendarimg" width="24" height="23" alt="calendar" src="{$SITE_URL}app/images/calendar_new.png"></a></td>
					
					<td>{$sandwich->sandwich_name}</td>
					
					<td>{$sandwich->first_name} {$sandwich->last_name}</td>
					<td>${$sandwich->sandwich_price}</td>
					<td> <a href="#" class="remove_sotd" data-sandwich="{$sandwich->id}">Remove</a></td>
				</tr>
				{/foreach}				
			</tbody>	
        </table>
        {else}
			
			<p> No Sandwiches in this section.</p>
			
        {/if}
    </div>     
</div>
