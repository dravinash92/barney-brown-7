<div class="container">
     <div class="sandwich-gallery">
       <div class="heading">
         <h1>SANDWICH GALLERY</h1>
         
         <div class="search">
           <p>Search</p>
           <input type="text" name="search_ingallery" > <a class="search-button" href="{$SITE_URL}sandwich/index/"><img width="26" height="26" src="{$SITE_URL}app/images/link.png"> </a>
           <input type="hidden" id="searchTerm" value="{$serch_term}">
         </div>
       </div>
       
       <div class="leftside-bar">
          <h2>Sort By</h2>
          
           <div class="bgforSelect">
                <select id="" name="" class="wid-input sortby">
                   <option value="1" {if $sort_id eq 1} selected="selected" {/if}>MOST RECENT</option>
                   <option value="2" {if $sort_id eq 2} selected="selected" {/if}>MOST LIKED</option>
                   <option value="3" {if $sort_id eq 3} selected="selected" {/if}>MOST POPULAR</option>
                </select>
              </div>
          
{foreach from=$categories key=i item=category}
	            
	            {foreach from=$category key=k item=data}
	              {if $data->category_identifier }
         			<div class="items">
           				<h3>{$data->category_name}</h3>
            				<ul>
            				{if $data->category_name neq "bread" }
	              <li>
	              <span class="multi-left">
                  <input type="checkbox"  value="null" name="check" id="check_{$k}_00" class="input-checkbox">
                  <label for="check_{$k}_00"  class="multisel-ckeck"></label></span>
                  <span style="width: 98px;" class="radio-text">No {$data->category_name}</span>
                  </li>   
                 {/if}  
                 {foreach from=$categoryItems key=j item=categoryItem}
		            
		            	{foreach from=$categoryItem key=j item=dataitems}
                     {if $data->id eq $dataitems->category_id}
                <li>
                 <span class="multi-left">
                  <input type="checkbox"  value="check1" name="check" id="check_{$k}_{$j}" class="input-checkbox">
                  <label for="check_{$k}_{$j}"></label></span>
                  <span style="width: 98px;" class="radio-text">{$dataitems->item_name}</span>
                </li>
                {/if}
              
                  {/foreach}
	                 
            {/foreach}
             </ul>
             </div>
              {/if}  
               {/foreach}
	                 
            {/foreach}         

         
          
       </div>
       
       <div class="right-side">
       <input name="total_item_count" type="hidden" value="{$total_item_count}" />
        <input name="sort_type" type="hidden" value="{$sort_id}" />
       <table width="100%" cellspacing="0" cellpadding="0" border="0" class="adminGalleryList">
        <tbody><tr class="static">
          <th width="10%" height="31">SOTD</th>
          <th width="10%" height="31">TRND</th>
          <th width="10%" height="31">PUBLIC</th>
          <th width="38%">CUSTOM SANDWICHES</th>
          <th width="7%">LIKES</th>
          <th width="11%">PURCHASES</th>
          <th width="10%">PRICE</th>
          <th width="12%">FLAGGED</th>
          <th width="6%">&nbsp;</th>
          <th width="6%">&nbsp;</th>
        </tr>
        
        
        {if $GALLARY_DATA|@count gt 0}
        
        {section name=sandwitch start=0 loop=$GALLARY_DATA|@count step=1 }
     
     
     
              {assign var = 'prot_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
              if($c){
              foreach($c as $c){
               $result_1 .= ' '.$c;
              }}
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
                  {if $smarty.session.orders.item_id|@is_array} 
                   {assign var = 'items_id' value = $GALLARY_DATA[$smarty.section.sandwitch.index].id|@in_array:$smarty.session.orders.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
        
        <tr {if  $smarty.section.sandwitch.index mod 2 neq 0 } class="even" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-formatdate="{$GALLARY_DATA[$smarty.section.sandwitch.index].formated_date}" data-username="{$GALLARY_DATA[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0]}"  data-date="{$GALLARY_DATA[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_price}" data-id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" data-name="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}" >
		
		<td width="10%"><div class="plus {if $GALLARY_DATA[$smarty.section.sandwitch.index].sotd eq 1 } white {else} black {/if} sotd" data-sandwich="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}">+</div></td>	
		
		<td width="10%"><div class="plus {if $GALLARY_DATA[$smarty.section.sandwitch.index].trnd eq 1 } white {else} black {/if} trnd" data-sandwich="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}">+</div></td>	
        
        <td width="10%" align="center">
              <span class="multi-left">
               <input {if $GALLARY_DATA[$smarty.section.sandwitch.index].is_public eq 1 } checked="checked" {/if} type="checkbox" name="" class="input-checkbox" id="checkbox-{$smarty.section.sandwitch.index}">
               <label class="multisel-ckeck" for="checkbox-{$smarty.section.sandwitch.index}"></label>
              </span>
          </td>
          <td>{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}</td>
          <td><input name="sandwich_like" id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="{$GALLARY_DATA[$smarty.section.sandwitch.index].like_count}" type="text" class="sandwich_like" style="width:80px;" /></td>
          <td><input name="sandwich_purchase" id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="{$GALLARY_DATA[$smarty.section.sandwitch.index].purchase_count}" type="text" class="sandwich_purchase" style="width:80px;" /></td>
          <td>$ {$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_price}</td>
          <td><a href="#" class="flag">{if $GALLARY_DATA[$smarty.section.sandwitch.index].flag eq 1}<img width="15" height="20" src="{$SITE_URL}app/images/flag.png">unflag{/if}</a></td>
          <td width="6%"><a href="{$SITE_URL}sandwich/edit/{$GALLARY_DATA[$smarty.section.sandwitch.index].id}">edit</a></td>
          <td width="6%"><a href="#" class="remove_gallery_item">remove</a></td>
        </tr>
        
        {/section}
        
        {/if}
	</tbody></table>
<div class="loadMoreGallery" style="text-align:center"><img src="{$SITE_URL}app/images/star.png" alt="Loading" /> </div>
       </div>
       </div>
       
     </div>
  </div>
