
<div class="container">
    <div class="condiment">
      <h1>EDIT ITEM </h1>
      <form name="edititems" method="post" action="{$SITE_URL}menu/updateItems" enctype="multipart/form-data">
          <input type="hidden" name="id" value="{$data.id}">
          <input type="hidden" name="old_image" value="{$data.item_image}">
          <input type="hidden" name="old_image_sliced" value="{$data.item_image_sliced}">
          <input type="hidden" name="old_image_long" value="{$data.image_long}">
          <input type="hidden" name="old_image_round" value="{$data.image_round}">
          <input type="hidden" name="old_image_trapezoid" value="{$data.image_trapezoid}">
      <ul>
        <li>
          <label>Name of Protein</label>
          <input type="text" name="item_name" value="{$data.item_name}">
        </li>
        
        <li>
          <label>Price</label>
          <input type="text" name="item_price" value="{$data.item_price}">
        </li>
        
        <!--  <li>
          <label>Description (Optional)</label>
          <div gramm="gramm" contenteditable="" style="border: 1px solid transparent; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; box-sizing: content-box; display: block; height: 96px; width: 293.375px; padding: 3px 5px; margin: 0px 0px 8px; position: absolute; color: transparent; white-space: pre-wrap; overflow: hidden; z-index: 0; text-align: left; -webkit-transform: translate(0px, 70px); transform: translate(0px, 70px); background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);" id="b5a5d32d-b1b2-ed53-22fe-f66acdeb77bf" gramm_id="b5a5d32d-b1b2-ed53-22fe-f66acdeb77bf" gr_new_editor="true"><span style="line-height: normal; white-space: pre-wrap; text-align: start; clear: none; box-sizing: border-box; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; display: inline-block; width: 100%; font-family: monospace; font-size: 13px; font-weight: normal; font-style: normal; letter-spacing: normal; text-shadow: none; color: transparent; height: 102px;"></span><br></div><textarea gramm="" txt_gramm_id="4d6efe49-ae3f-c5c5-c33a-461e19cdb53a" gramm_id="c82bf7d2-5c61-2cda-8e5c-815df6eeb63f" spellcheck="false" gr_new_editor="true" style="white-space: pre-wrap; z-index: auto; position: relative; line-height: normal; font-size: 13px; -webkit-transition: none; transition: none; height: 96px; overflow: auto; background: transparent !important;"></textarea><div class="gr-btn-freemium gr-shown" style="display: none; z-index: 2; margin-left: 487px; margin-top: 364px; opacity: 1;">	<div class="gr-btn-status" title="Protected by Grammarly"></div></div>
        </li> -->
      {if $data.category_id eq 1}
		 <li>
          <label>Bread Type</label>
          <select  name="bread_type">
          	<option {if $data.bread_type eq 0} selected="selected" {/if} value="0">None</option>
          	<option {if $data.bread_type eq 1} selected="selected" {/if} value="1">3 foot</option>
          	<option {if $data.bread_type eq 2} selected="selected" {/if}value="2">6 foot</option>
          	
          </select>
        </li>
         <li>
          <label>Bread Shape</label>
          <select  name="bread_shape">
          	<option {if $data.bread_shape eq 0} selected="selected" {/if} value="0">Long</option>
          	<option {if $data.bread_shape eq 1} selected="selected" {/if} value="1">Round</option>
          	<option {if $data.bread_shape eq 2} selected="selected" {/if}value="2">Trapezoid</option>
          	
          </select>
        </li>
	 {/if} 
	   {if $data.category_id eq 1}
        <li>
         <label>Image</label>
          <label>
           {if $data.item_image neq ""}
          	 {assign var="uploadpath" value="upload/`$data.item_image`"}
           
      		 {if file_exists($uploadpath)}
      		 		<img  style=" margin-top: -28px;"  src="{$SITE_URL}upload/{$data.item_image}" height="50px" width="50px">  
			 {/if}
			 {/if} 
          	 <input type="file" class="choose" name="item_image" value="{$data.item_image}"> 
          </label>
      
        </li> {/if}
          {if $data.category_id eq 1}
        <li>
         <label>Image Sliced</label>
         <label> 
          {if $data.item_image_sliced neq ""}
	          {assign var="uploadpathslice" value="upload/`$data.item_image_sliced`"}
	      		 {if file_exists($uploadpathslice)}
	         	<img  style=" margin-top: -28px;" src="{$SITE_URL}upload/{$data.item_image_sliced}" height="50px" width="50px"> 
				 {/if}
			{/if}
         	<input type="file" class="choose" name="item_image_sliced" value="{$data.item_image_sliced}"> 
         </label>
        </li>
       
        {/if}
          {if $data.category_id ne 1}
        <li>
         <label>Long Image </label>
          <label> 
           {if $data.image_long neq ""}
	           	{assign var="uploadpathlong" value="upload/`$data.image_long`"}
	      		 {if file_exists($uploadpathlong)}
	          		<img  style=" margin-top: -28px;" src="{$SITE_URL}upload/{$data.image_long}" height="50px" width="50px"> 
				 {/if}
			{/if}
          	<input type="file" class="choose" name="image_long" value="{$data.image_long}"> 
          </label>
        </li>
        <li>
         <label>Round Image </label>
          <label> 
           {if $data.image_round neq ""}
	           	{assign var="uploadpathlong" value="upload/`$data.image_round`"}
	      		 {if file_exists($uploadpathlong)}
          			<img  style=" margin-top: -28px;" src="{$SITE_URL}upload/{$data.image_round}" height="50px" width="50px">    
				 {/if}
			{/if}
          	<input type="file" class="choose" name="image_round" value="{$data.image_round}"> 
          </label>
        </li>
        <li>
         <label>Trapezoid Image </label>
          <label> 
          	 {if $data.image_trapezoid neq ""}
          	     {assign var="uploadpathlong" value="upload/`$data.image_trapezoid`"}
	      		 {if file_exists($uploadpathlong)}
          			<img style=" margin-top: -28px;" src="{$SITE_URL}upload/{$data.image_trapezoid}" height="50px" width="50px">  
				 {/if}
			{/if}
          	<input type="file" class="choose" name="image_trapezoid" value="{$data.image_trapezoid}"> 
          </label>
        </li>
    
        
	        {assign var="optnarray" value=":"|explode:$data.options_id }  
	         <li >
	         <label>Option</label>
	        <select class="optinMultiselection" name="select_sandwich_option[]" multiple size="3">
				  	{foreach from=$sandwichCatagoryData key=k item=b}  
				  <option {if $b.id|in_array:$optnarray eq 1} selected="selected"  {/if} value="{$b.id}"  >  {$b.option_display_name}</option>
				   {/foreach}    
			</select>
		   	</li> 
		{/if}
		
		
		 <li>
          <label>Taxable</label>
          <input type="checkbox" value="1" name="taxable" class="price-menu" {if $data.taxable eq 1} checked {/if} >
        </li>  
        
             <li>
          <label>Premium</label>
          <input type="checkbox" value="1" name="premium" class="price-menu" {if $data.premium eq 1} checked {/if}  >
        </li>  
        
		
      </ul>
       <div class="button">
           <input type="hidden" value="{$data.category_id}" name="category_id">
          <input type="hidden" value="{$data.item_price}" name="old_price">
         <input type="submit" class="save save_custom_item" value="SAVE">
        <a href="{$SITE_URL}menu/custom" class="cancel">CANCEL</a>
      </div>
      </form>
      
    </div>
  </div>
