        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <th width="62%">TIME</th>
          <th width="7%">PICK-UP</th>
          <th width="7%">DELIVERY</th>
          <th width="7%"> TOTAL</th>
          <th width="8%">SALES</th>
           <th width="9%">%SALES</th>
        </tr>
        
         <tr class="heading-color">
          <td></td>
          <td class="center">{$total_pickups}</td>
          <td class="center">{$total_delivery}</td>
          <td class="center">{$total|number_format}</td>
          <td>${$total_sales|string_format:"%.2f"}</td>
          <td>100%</td>
        </tr>
      {foreach from=$orderedItems key=i item=orderedItem}
      
        <tr {if $i%2==0} class="even" {/if}>
          <td>{$orderedItem->time}</td>
          <td class="center">{$orderedItem->pickups}</td>
          <td class="center">{$orderedItem->delivery}</td>
          <td class="center">{$orderedItem->total->total_order}</td>
          <td>${$orderedItem->total->total_sales}</td>
          <td>  {if $orderedItem->total->total_order gt 0 }
           {math equation="(( x / y ) * z )" x=$orderedItem->total->total_sales y=$total_sales z=100 format="%.2f"}%          
			{/if}
			</td>
        </tr>
				
    
      {/foreach}
      </tbody></table>
      
