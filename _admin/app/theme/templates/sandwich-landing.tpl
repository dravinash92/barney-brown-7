  <div class="landing-page"  >
               <h2>MAKE ONE OF YOUR GO-TO SANDWICHES, OR TRY SOMETHING COMPLETELY NEW! </h2> 
              <!--  <p>Sandwiches include your choice of one of our fine artisinal breads, unlimited toppings, unlimited condiments, plus any of the add-ons listed below. Start creating your sandwich by selecting one of the breads on the right!</p> -->
               <div class="price">
                 <ul>
                   <li>STARTING AT  <span class="starting">${$BASE_FARE}.00</span></li>
                   <li>PROTEINS: <span>+ $2.00</span></li>
                   <li>CHEESES: <span>+ $1.00</span></li>
                   <li>PREMIUMS: <span>+ $1.00</span></li>
                 </ul>
               </div>
            </div>
