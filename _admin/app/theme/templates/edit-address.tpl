<a href="#" class="close-button">Close</a>
<form class="add_address" method="post" >
    <div class="title-holder">
      <h1>EDIT ADDRESS</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Recipient Name</p>
        <input name="recipient" type="text" class="text-box-control" placeholder="Enter Recipient Name" value="{$address->name}">
        </span> <span class="text-box-holder">
        <p>Company</p>
        <input name="company" type="text" class="text-box-control" placeholder="Enter Company Name" value="{$address->company}">
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Street Address</p>
        <input name="address1" type="text" class="text-box-control" value="{$address->address1}" >
        </span><!-- <span class="text-box-holder">
        <p>Street Address 2</p>
        <input name="address2" type="text" class="text-box-control" value="{$address->address2}" >
        </span>--> <span class="text-box-holder1">
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control" value="{$address->street}" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" value="{$address->cross_streets}">
        </span> <span class="text-box-holder">
        <p>Phone Number</p>
        <input name="phone1" type="text" class="text-box-phone" value="{$phone1}">
        <input name="phone2" type="text" class="text-box-phone" value="{$phone2}">
        <input name="phone3" type="text" class="text-box-phone margin-none" value="{$phone3}">
        </span> <span class="text-box-holder1">
        <p>Ext.</p>
        <input name="extn" type="text" class="text-box-control" value="{$address->extn}">
        </span> </li>
      <li> <span class="text-box-holder">
        <h2>New York, NY</h2>
        <span class="text-box-zip-holder1">
        <p>Zip Code</p>
        <input name="zip" type="text" class="text-box-zip" value="{$address->zip}">
        </span> </span>
        <h3>{$title_text} currently delivers in Midtown Manhattan from 23rd Street to 59th Street and from 3rd Avenue to 8th Avenue.</h3>
      </li>
      <li> <span class="delivery-instructions">
        <p>Delivery Instructions</p>
        <input name="delivery_instructions" type="text" class="text-box-delivery" value="{$address->delivery_instructions}" >
        <input name="address_id" type="hidden" value="{$address->address_id}" >
        </span> </li>
      <li> <a href="javaScript:void(0)" class="add-address save_address">SAVE ADDRESS</a> </li>
    </ul>
</form>
