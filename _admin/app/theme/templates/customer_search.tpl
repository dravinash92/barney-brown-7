<div class="container">
      <div class="customers">
      
       <h1>CUSTOMERS</h1>
       
       <form class="new-order-form" id="new-order-form" action="" method="post">
       	<div class="text-box-holder">
          <label>First Name</label>
          <input name="customername" id="customernameinput" type="text" class="main-text-box common-text-width">
          <a href="{$SITE_URL}customer/index/" id="customername" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Last Name</label>
          <input name="lastname" type="text" id="lastnameinput" class="main-text-box common-text-width">
          <a href="{$SITE_URL}customer/index/" id="lastname" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Phone Number</label>
          <input name="ph1" type="text" class="main-text-box phone-text-width">
          <input name="ph2" type="text" class="main-text-box phone-text-width">
          <input name="ph3" type="text" class="main-text-box phone-text-width">
          <input name="ph4" type="text" class="main-text-box phone-text-width">
        <!--<a href="{$SITE_URL}customer/index/" class="arrow" id="phonesearch">&nbsp;</a>-->
        </div>
        <div class="text-box-holder">
          <label>Email Address</label>
          <input name="customeremail" type="text" id="customeremailinput" class="main-text-box common-text-width">
          <a href="{$SITE_URL}customer/index/" id="customeremail" class="arrow">&nbsp;</a>
        </div>
        <div class="text-box-holder">
          <label>Company (Optional)</label>
          <input name="company" type="text" class="main-text-box common-text-width">
          <a href="{$SITE_URL}customer/index/" id="customercompany" class="arrow">&nbsp;</a>
        </div>
         <div class="text-box-holder">
       <input type="submit" class="new-account createaccount" value="New Account"/>
        </div>
            <div class="text-box-holder"> <span class="rerror_msg"></span> </div>
       </form>
       
       <!--<a href="javascript:void(0);"  class="new-account createaccount">New Account</a>-->
                    <input name="total_item_count" type="hidden" value="{$total_item_count}" />
		<input type="hidden" name="search_condition" value="{$search_condition}" class="search_condition" />
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminCustomerList">
          <tbody><tr>
            <th width="22%">CUSTOMER NAME</th>
            <th width="32%">EMAIL</th>
            <th width="18%">PHONE</th>
            <th width="15%">ORDERS</th>
            <th width="13%">SALES TOTAL</th>
          </tr>
          {section name=users loop=$users_data } 
   		 <tr>
            <td><a href="{$SITE_URL}customer/profile/{$users_data[users].uid}">{$users_data[users].first_name} {$users_data[users].last_name}</a></td>
            <td>{$users_data[users].username}</td>
            <td></td>
            <td>{$users_data[users].sandwich_count}</td>
            <td>${$users_data[users].total_price}</td>
          </tr>
		{/section}  
		  
       </tbody></table>
<div class="loadMoreCustomer" style="display:none;text-align:center"><img src="{$SITE_URL}app/images/star.png" alt="Loading" /> </div>

    </div>
     
  </div>
  {literal}
<style>
.register {
    border: 1px solid red !important;
}

.rerror_msg {
    color: red;
    font-size: 12px;
}
.loadMoreCustomer {
    float: left;
    display: block;
    width: 100%;
    margin: 20px auto 0;
}


</style>
<script>
$(document).ready(function(){
    $(window).scroll(function(){
if($(window).scrollTop() == $(document).height() - $(window).height() && $(".search_condition").val()==""){

                setTimeout(function() {
                    $('.loadMoreCustomer').show();
        $current_count = $('.adminCustomerList tr').length;
		//alert($current_count);
        $total_items = $('input[name="total_item_count"]').val();
        

        if (parseInt($current_count) < parseInt($total_items)) {
            $.ajax({
                type: "POST",
                data: {
                	"count": $current_count,
                   
                },
                url: SITE_URL + 'customer/loadmoreCustomer',
                success: function(e) {
                    $('.adminCustomerList tr:last').after(e);
					$('.loadMoreCustomer').hide();
                }
            });
        } else {
            $('.loadMoreCustomer').hide();
        }

                }, 300);
            }    });
});
</script>
  {/literal}
