<div class="container">
      <div class="store-accounts">
        <h1>MENU ITEMS</h1>
             
        <h2>CUSTOM</h2>       
        
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody><tr>
           <th width="89%">SANDWICHES</th>
           <th width="11%"># OPTIONS</th>
         </tr>
         
         <tr>
           <td>Breads</td>
           <td class="align-right">{$counts.BREAD}</td>
         </tr>
         
         <tr class="even">
           <td>Meats</td>
           <td class="align-right">{$counts.PROTEIN}</td>
         </tr>
		 
		  <tr>
           <td>Cheeses</td>
           <td class="align-right">{$counts.CHEESE}</td>
         </tr>
		 
		 <tr class="even">
           <td>Toppings</td>
           <td class="align-right">{$counts.TOPPINGS}</td>
         </tr>
		 
		  <tr>
           <td>Condiments</td>
           <td class="align-right">{$counts.CONDIMENTS}</td>
         </tr>
         
       </tbody></table>
       <a class="edit" href="{$SITE_URL}menu/custom">EDIT</a>
       <a class="edit" style="margin-left:10px;" href="{$SITE_URL}menu/edit_options">EDIT OPTIONS</a>
        <h2>STANDARD</h2>       
        <a class="category" href="{$SITE_URL}menu/newStandardCategory">NEW CATEGORY</a>
        <p>
          <span>View </span>
          <select>
                 <option value="">Current</option>
                 <option value=""></option>
          </select> 
        </p>
       
       {foreach from=$dataList key=j  item=list}

      <div class="store-accounts">
      
   <form name="myForm" method="post" id="myForm" action="{$SITE_URL}menu/updateStandardCategoryPriority" > 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th width="10%" height="33">LIVE</th>
          <th width="16%">PRTY</th>
          <th width="52%">{$list.name}</th>
		   <th width="9%">PRICE</th>
		    <th width="4%"></th>
		    <th width="9%"><a style="color: #deb885;text-decoration:underline;" href="{$SITE_URL}menu/deleteStandardCategory/?id={$list.id}" onclick="return confirm('Are you sure you want to delete?')" class="deletecategory">Delete</a></th>

        </tr>
       
{foreach from=$list.data key=k item=data}
     
        <tr class="{if $k%2 == 0}even{/if}">
          <td width="10%" align="center">
          <span class="multi-left">
         
               <input type="checkbox" {if $data.live} checked="checked" {/if} name="livecheckbox{$data.id}" class="input-checkbox" id="checkbox-{$j}-{$k}">
             <label class="multisel-ckeck" for="checkbox-{$j}-{$k}"></label>
            </span>
          </td>
          <td width="16%"><input type="text" class="field" name="priority[]" value="{$data.priority}"></td>
          <td width="52%"><a href="#">{$data.product_name}</a></td>
          <td width="9%"><a href="#">{'$'}{$data.product_price}</a></td>
          <td width="4%"><a href="{$SITE_URL}menu/editStandardProducts/{$data.id}">edit</a></td>
          <td width="9%"><a href="{$SITE_URL}menu/deleteStandardCategoryProducts/{$data.id}" onclick="return confirm('Are you sure you want to delete?')">delete</a></td>
        </tr>
  <input type="hidden" name="hidden_standard_ids[]" value="{$data.id}">

{/foreach}
      </tbody></table>  
     

       <a href="#" onclick="document.getElementById('myForm').submit();" class="update update_priority">update</a>
      
      <a href="{$SITE_URL}menu/newStandardProducts/{$list.id}" class="add-user">ADD NEW ITEM</a>
   </form>       
    </div>

{/foreach}



<!------------------------------------------Catering items loop--------------------------------------------------->
        <h2>CATERING</h2>       
        <a class="category" href="{$SITE_URL}menu/newStandardCategory">NEW CATEGORY</a>
        <p>
          <span>View </span>
          <select>
                 <option value="">Current</option>
                 <option value=""></option>
          </select> 
        </p>
       
       {foreach from=$cateringdataList key=j  item=list}

      <div class="store-accounts">
      
   <form name="myFormCatering" method="post" id="myForm" action="{$SITE_URL}menu/updateStandardCategoryPriority" > 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th width="10%" height="33">LIVE</th>
          <th width="16%">PRTY</th>
          <th width="52%">{$list.name}</th>
		   <th width="9%">PRICE</th>
		    <th width="4%"></th>
		    <th width="9%"><a style="color: #deb885;text-decoration:underline;" href="{$SITE_URL}menu/deleteStandardCategory/?id={$list.id}" onclick="return confirm('Are you sure you want to delete?')" class="deletecategory">Delete</a></th>

        </tr>
       
{foreach from=$list.data key=k item=data}
     
        <tr class="{if $k%2 == 0}even{/if}">
          <td width="10%" align="center">
          <span class="multi-left">
         
               <input type="checkbox" {if $data.live } checked="checked" {/if} name="livecheckbox{$data.id}" class="input-checkbox" id="checkboxcat-{$j}-{$k}">
             <label class="multisel-ckeck" for="checkboxcat-{$j}-{$k}"></label>
            </span>
          </td>
          <td width="16%"><input type="text" class="field" name="priority[]" value="{$data.priority}"></td>
          <td width="52%"><a href="#">{$data.product_name}</a></td>
          <td width="9%"><a href="#">{'$'}{$data.product_price}</a></td>
          <td width="4%"><a href="{$SITE_URL}menu/editStandardProducts/{$data.id}">edit</a></td>
          <td width="9%"><a href="{$SITE_URL}menu/deleteStandardCategoryProducts/{$data.id}" onclick="return confirm('Are you sure you want to delete?')">delete</a></td>
        </tr>
  <input type="hidden" name="hidden_standard_ids[]" value="{$data.id}">

{/foreach}
      </tbody></table>  
     

       <a href="#" onclick="document.getElementById('myFormCatering').submit();" class="update update_priority">update</a>
      
      <a href="{$SITE_URL}menu/newStandardProducts/{$list.id}" class="add-user">ADD NEW ITEM</a>
   </form>       
    </div>

{/foreach}
   
  </div>
  
</div>  
