<div class="container">
    <div class="place-order">
       <h1>ORDER DETAILS</h1>
       
       <div class="place-order-coustomer-info">
       		<div class="address-details">
           <h3>CUSTOMER INFO</h3>
			<p>Name: <span>{$orderDetails->first_name} {$orderDetails->last_name}</span> <br>
              Email: <span>{$orderDetails->username}</span><br/>
              Phone: <span>{$orderDetails->phone}</span><br/> 
              Company: <span>{$orderDetails->company}</span><br/> 
            </p>   
            </div>                          
        <!--</div>-->
       
       <!--<div class="place-order-coustomer-info">-->
       		<div class="address-details">
           <h3>ORDER INFO</h3>
           <p>Sub Total: <span>${$orderDetails->sub_total}</span> <br>
              Fee: <span>${$orderDetails->tip}</span><br/>
              Off Amount: <span>${$orderDetails->off_amount}</span><br/>              
              Total: <span>${$orderDetails->total}</span></p>           
            </div>  
         <!--</div>-->
        
        
       <!--<div class="place-order-coustomer-info">-->
       		<div class="address-details">
           <h3>ADDRESS INFO</h3>
			<p>
				Name: <span>{$orderDetails->address->name}</span> <br>
				Company: <span>{$orderDetails->address->company}</span> <br>						
				Addess: <span>
						{if $orderDetails->address->address1 ne ""}{$orderDetails->address->address1}{/if}
						{if $orderDetails->address->address2 ne ""}, {$orderDetails->address->address2}{/if}
						{if $orderDetails->address->street ne ""}, {$orderDetails->address->street}{/if}
						{if $orderDetails->address->cross_streets ne ""}, {$orderDetails->address->cross_streets}{/if}		</span> <br>
				Zip: <span>{$orderDetails->address->zip}</span> <br>	
				Delivery Instruction: <span>{$orderDetails->address->delivery_instructions}</span> <br>	
            </p>
            </div>           
        </div>
        
        
        <div class="place-order-coustomer-info">
       		<div class="address-details">
           <h3>ITEMS INFO</h3>
			{foreach from=$orderDetails->items item=item}		
				<p>Item Name: <span>{$item->name}</span> <br>
				  Quantity: <span>{$item->qty}</span><br/>				 
				</p> 
				<hr/>
            {/foreach}  
            </div>                          
        </div>
        
        
    </div>
</div>         
