<div class="container">
    <div class="user-account web-account">
       <h1>SITE SOCIAL LINKS</h1>
       <form action="{$SITE_URL}accounts/saveSocialLinks" method="POST">
          <label>Facebook</label>
          <input name="facebook" type="text" value="{$social->facebook}">
          <label>Twitter</label>
          <input name="twitter" type="text" value="{$social->twitter}">
          <label>Instagram</label>
          <input name="instagram" type="text" value="{$social->instagram}">
         <br/><br/>
          <input type="submit" name="submit" value="SAVE LINKS">
          <a class="button" href="{$SITE_URL}/accounts">Cancel</a>
       </form>
    </div>
  </div>
