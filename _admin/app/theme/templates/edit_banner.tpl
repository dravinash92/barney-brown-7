<div class="container">
     <div class="homepage-banner">
     <form action="{$SITE_URL}web/updateWebPageBannerData" name="edit_banner_form" id="edit_banner_form" enctype="multipart/form-data" method="POST">
       {foreach from=$bannerData key=k item=v}
       <h1>homepage</h1>
       <ul>
        <li>
          <label>Name of Banner</label>
          <input type="text" name="banner_name" value="{$v->banner_name}">
          <input type="hidden" name="hidden_id" value="{$v->id}">
        </li>
        <li>
          <label>Type of Banner</label>
          
          <div class="banner">
            <img width="120" height="54" src="{$SITE_URL}app/images/banner1.png">
            <input type="radio" value="1" {if $v->banner_type eq '1'} checked {/if} class="input-radio" name="banner" id="Insurancereq1">
            <label for="Insurancereq1"></label>
               <label> Dimension :  384x331</label>
               <label> Size :  2 MB</label>
          </div>
          <div class="banner">
            <img width="120" height="54" src="{$SITE_URL}app/images/banner2.png">
            <input type="radio" value="2"  {if $v->banner_type eq '2'} checked {/if} class="input-radio" name="banner" id="Insurancereq2">
            <label for="Insurancereq2"></label>
              <label> Dimension : </label>
            <label> Size :  </label>
          </div>
          <div class="banner">
            <img width="120" height="54" src="{$SITE_URL}app/images/banner3.png">
            <input type="radio" value="3"  {if $v->banner_type eq '3'} checked {/if} class="input-radio" name="banner" id="Insurancereq3">
            <label for="Insurancereq3"></label>
              <label> Dimension : </label>
            <label> Size :  </label>
          </div>
        </li>
        <li>
           <label>Image</label>
           <label><img width="80" height="54" src="{$SITE_URL}upload/{$v->image}"></label>
           <input type="file" value="choose file" name="item_image" class="choose">
        </li>
        <li>
          <label>Text</label><br/>
          <textarea name="text">{$v->text}</textarea>
        </li>
        <li>
          <span class="multi-left">
             <input type="checkbox" id="checkbox-2-1" class="input-checkbox" name="banner_status" {if $v->banner_status eq 1} checked {/if} value="1">
             <label for="checkbox-2-1" class="multisel-ckeck"></label>
          </span>
        <p>Activate Button</p>
        </li>
          <li>
          <label>Button Text</label>
          <input type="text" name="button_text" value="{$v->button_text}">
        </li>
        <li>
          <label>Banner Link To Address</label>
          <input type="text" name="link" value="{$v->link}">
        </li>
        <li>
          <div class="radio-section">
            <input type="radio" value="1" {if $v->redirection_status eq 1} checked {/if}  class="input-radio" name="redirection_status" id="Insurancereq5">
            <label for="Insurancereq5"></label>
            <span class="radio-text">Redirect</span>
              <!-- 
            <input type="radio" value="2" {if $v->redirection_status eq 2} checked {/if} class="input-radio" name="redirection_status" id="Insurancereq6">
            <label for="Insurancereq6"></label>
            <span class="radio-text">New Window</span> -->  </div>
        </li>
        <li></li>

      </ul>
      <input type="hidden" name="org_file" value="{$v->image}" />
        
       <input type="button" name="submit" class="save_banner" value="SAVE">
       <input type="submit" name="submit" style="display:none;" />
        
        <a class="cancel" href="{$SITE_URL}web/homepage">CANCEL</a>
        
    </div>
    {/foreach}
    </form>
  </div>
