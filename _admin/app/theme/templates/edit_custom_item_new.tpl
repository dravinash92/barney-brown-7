<div class="container">
  <div class="condiment">
    <h1>EDIT ITEM</h1>

    <form name="newprotein" method="post" action="{$SITE_URL}menu/updateItems" enctype="multipart/form-data">

      <input type="hidden" name="id" value="{$data.id}">
      
      <input type="hidden" name="old_image" value="{$data.item_image}">
      <input type="hidden" name="old_image_sliced" value="{$data.item_image_sliced}">

      <div class="edit-item--input">
        <label>Name of Protein</label>
        <input type="text" name="item_name" value="{$data.item_name}">
      </div>

      <div class="edit-item--input">
        <label>Price</label>
        <input type="text" name="item_price" value="{$data.item_price}">
      </div>
      <div class="texa-prem_wrap">
        <div class="texa-prem forms">
          <label>Taxable</label>
          <!--   <input type="checkbox" value="1" name="premium" class="price-menu"  > -->
          <label for="cbk1">
            <input type="checkbox" id="cbk1" value="1" name="taxable" {if $data.taxable eq 1} checked {/if}>
            <span class="cbx">
              <svg width="12px" height="11px" viewBox="0 0 12 11">
                <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
              </svg>
            </span>

          </label>

        </div> 
          <input type="hidden" value="2" name="category_id">
        <div class="texa-prem forms">
          <label>Premium</label>
          <label for="cbk2">
            <input type="checkbox" id="cbk2" value="1" name="premium" {if $data.premium eq 1} checked {/if}>
            <span class="cbx">
              <svg width="12px" height="11px" viewBox="0 0 12 11">
                <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
              </svg>
            </span>

          </label>
        </div>
      </div>
      {assign var="optnarray" value=":"|explode:$data.options_id }
      {foreach from=$sandwichCatagoryData key=k item=b}



      <div class="forms file-choose-pro">
        <label for="cbk-{$b.id}">
          <input type="checkbox" id="cbk-{$b.id}" name="select_sandwich_option[]" value="{$b.id}"  onclick="showUploadImage(this)" {if $b.id|in_array:$optnarray eq 1} checked {/if} >
          <span class="cbx">
            <svg width="12px" height="11px" viewBox="0 0 12 11">
              <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
            </svg>
          </span>
        </label>
        <label for="cbk-{$b.id}" class="multisel-ckeck">{$b.option_display_name}</label>
      </div>

      <div class="slect-image--wrap" id="upload-cbk-{$b.id}" style="display: none;">
        <div>
          <label>Long Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="lImg-cbk-{$b.id}" name="lImg-{$b.id}" />
             
            </label>
            <span id="lImgName-cbk-{$b.id}">No file selected</span>
            {foreach from=$optionImgs key=ky item=op}
            {if $ky eq $b.id && $op.lImg ne ""}
            <img src="{$SITE_URL}upload/{$op.lImg}">
            <!-- <input type="hidden" name="{$b.id}-old-lImg-cbk" value="{$op.lImg}"> -->
            <input type="hidden" name="oldOptionImgs[{$op.lImg}]" value="{$ky}">
            {/if}
            {/foreach}
          </p>
        </div>

        <div>

          <label>Round Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="rImg-cbk-{$b.id}" name="rImg-{$b.id}" value="" />
              
            </label>
            <span id="rImgName-cbk-{$b.id}">No file selected</span>
            {foreach from=$optionImgs key=ky item=op}
            {if $ky eq $b.id && $op.rImg ne ""}
            <img src="{$SITE_URL}upload/{$op.rImg}">
            <!-- <input type="hidden" name="{$b.id}-old-rImg-cbk" value="{$op.rImg}"> -->
            <input type="hidden" name="oldOptionImgs[{$op.rImg}]" value="{$ky}">
            {/if}
            {/foreach}
          </p>
        </div>
        <div>
          <label>Trapezoid Image</label>
          <p>
            <label class="fileContainer">Choose file
              <input type="file" id="tImg-cbk-{$b.id}" name="tImg-{$b.id}"/>

            </label>
            <span id="tImgName-cbk-{$b.id}">No file selected</span>

            {foreach from=$optionImgs key=ky item=op}
            {if $ky eq $b.id && $op.tImg ne ""}
            <img src="{$SITE_URL}upload/{$op.tImg}">
            <!-- <input type="hidden" name="{$b.id}-old-tImg-cbk" value="{$op.tImg}"> -->
            <input type="hidden" name="oldOptionImgs[{$op.tImg}]" value="{$ky}">
            {/if}
            {/foreach}

          </p>
        </div>
      </div>
      {/foreach}  

<input type="hidden" value="{$data.item_price}" name="old_price">
      <div class="button">
       <input type="submit" class="save save_custom_item" value="SAVE">
       <a href="{$SITE_URL}menu/custom" class="cancel">CANCEL</a>
     </div>
   </form>

 </div>
</div>
