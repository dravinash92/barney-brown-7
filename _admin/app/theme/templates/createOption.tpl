<div class="container">

      <div class="standard-menu"><div class="condiment">
          
          <h1> {if $id } Edit {else} Add {/if } Option</h1>
          <form  action="{$SITE_URL}menu/addoptions" method="post" >        
          <ul>
          <li>
          <label>Option Display Name</label>
          <input type="text" value="{$OPTION_DATA[0]->option_display_name}" name="displayname" style="width:250px;">
          </li>
          <li>
          <label>Option Name</label>
          <input type="text" value="{$OPTION_DATA[0]->option_name}" name="optionname" style="width:250px;">
          </li>
           <li>
          <label>Option Price Multiplier</label>
          <input type="text" value="{$OPTION_DATA[0]->price_mult}" name="pricemult" style="width:250px;">
          </li>
          <li>
          <label>Option Unit</label>
          <input type="text" value="{$OPTION_DATA[0]->option_unit}" name="unit" style="width:250px;">
          </li>
          </ul>
          <div class="button">
          <input type="hidden" name="hid" value="{$id}" />
          <input type="submit" value="SAVE" class="save">
          <a class="cancel" href="{$SITE_URL}menu">CANCEL</a>
          </div>
     </form>
    </div>
    </div> 
   </div>
