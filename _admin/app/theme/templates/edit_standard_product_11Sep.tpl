<div class="container">
      <div class="standard-menu">
           <div class="standard-menu"><div class="condiment">
        <h1>STANDARD MENU ITEMS</h1>
        <form name="newbread" method="post" action="{$SITE_URL}menu/updateStandardCategoryProducts" enctype="multipart/form-data">        
        <ul>
        <li>
          <label>Product Name</label>
          <input  style="width:250px;" type="text" name="product_name" value="{$data.product_name}">
        </li>
      
         <li>
          <label>Description </label>
          <textarea style="width:250px;" name="description">{$data.description}</textarea>
         </li>
         
         <li>
          <label>Price</label>
          <input type="text" value="{$data.product_price}" name="product_price" class="price-menu">
        </li>  
        
       <li style="float:left">
          <label for="taxable" style="width:45px;">Taxable</label>
          <input style="width:auto;height:auto; margin-top:12px;" type="checkbox" value="1" id="taxable" name="taxable" class="price-menu" {if $data.taxable eq 1} checked {/if} >
        </li>  
      
      </ul>
     <ul>
        <li>
           <div class="radio-section-extra" style="margin-left:3px;margin-right:2px;">
            <span class="multi-left">
               <input style="margin:auto;" {if $data.active_extras eq 1 } checked="checked" {/if}  value="1" type="checkbox" id="checkbox-3" class="input-checkbox" name="is_extra">
               <label for="checkbox-3" class="multisel-ckeck"></label>
            </span>
            <span class="radio-text">Extras </span></div>
        </li>
    
        {if $extras_data|@count gt 0 }
       
        {section name="extras" loop=$extras_data } 
        
          <li>
         <label>Descriptor</label>
         <input type="text" name="descriptor[]" class="descriptor" value="{$extras_data[extras].desc}">
         <input type="hidden" name="descriptor_id[]" value="{$extras_data[extras].id}" />
        </li>
         {assign var="extraItm" value=$extras_data[extras].extra }
         
        
         
         {if $extraItm|@count gt 0 }
         
         {section name="extrasItems" loop=$extraItm }
        <li>
         <div class="label"><label class="name">Name</label><label class="extra-cost">Extra Cost</label>
         <label class="default">Default</label> </div>
         <div class="input">
           <input type="text" class="name" name="extra_name[]" value="{$extraItm[extrasItems].name}">
            <input type="text" class="extra-cost" name="extra_cost[]"  value="{$extraItm[extrasItems].cost}">
            <input type="hidden" name="extra_id[]" value="{$extraItm[extrasItems].id}" />
            <div class="radio-section">
             <input type="radio" value="{$extraItm[extrasItems].name}" class="input-radio" {if $extraItm[extrasItems].is_default eq 1 } checked="checked" {/if}  name="extra_default"  id="dafault{$extraItm[extrasItems].id}">
             <label for="dafault{$extraItm[extrasItems].id}"></label>
            </div>
         </div>
        </li>
        {/section} 
        {else} 
       
       
         <li>
         <div class="label"><label class="name">Name</label><label class="extra-cost">Extra Cost</label>
         <label class="default">Default</label> </div>
         <div class="input">
           <input type="text" class="name" name="extra_name[]" value="{$extrasItems[extrasItems].name}">
            <input type="text" class="extra-cost" name="extra_cost[]"  value="{$extrasItems[extrasItems].cost}">
            <input type="hidden" name="extra_id[]" value="{$extrasItems[extrasItems].id}" />
            <div class="radio-section">
             <input type="radio" value="{$extrasItems[extrasItems].name}" class="input-radio" {if $extras_data[extras].is_default eq 1 } checked="checked" {/if}  name="extra_default"  id="dafault{$extrasItems[extrasItems].id}">
             <label for="dafault{$extrasItems[extrasItems].id}"></label>
            </div>
         </div>
        </li>
       
        
         {/if}
        
        
       {/section} 
       
       {else} 
       
         <li>
         <label>Descriptor</label>
         <input type="text" name="descriptor[]" class="descriptor" value="{$extras_data[extras].desc}">
         <input type="hidden" name="descriptor_id[]" value="{$extras_data[extras].id}" />
        </li>
        
              <li>
         <div class="label"><label class="name">Name</label><label class="extra-cost">Extra Cost</label>
         <label class="default">Default</label> </div>
         <div class="input">
           <input type="text" class="name" name="extra_name[]" value="{$extrasItems[extrasItems].name}">
            <input type="text" class="extra-cost" name="extra_cost[]"  value="{$extrasItems[extrasItems].cost}">
            <input type="hidden" name="extra_id[]" value="{$extrasItems[extrasItems].id}" />
            <div class="radio-section">
             <input type="radio" value="{$extrasItems[extrasItems].name}" class="input-radio" {if $extras_data[extras].is_default eq 1 } checked="checked" {/if}  name="extra_default"  id="dafault{$extrasItems[extrasItems].id}">
             <label for="dafault{$extrasItems[extrasItems].id}"></label>
            </div>
         </div>
        </li>
       
       {/if}
       
        
      </ul>
      <a href="#" class="add-item extras_more">add item</a>

      <input type="hidden" name="hidden_id" value="{$data.id}" />
       <div class="button">
         <input type="submit" class="save save_standard_product" value="SAVE">
        <a href="{$SITE_URL}menu" class="cancel">CANCEL</a>
    </div>
   </div></div> 
 </form>
    </div>
    
 
