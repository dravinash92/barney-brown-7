<div class="container">
    <div class="deliveries">
       <h1>DELIVERIES</h1>
       
       <div class="top-deliveries-wrapper">
       		<div class="left">
          		<table class="top-deliveries-table">
								<tbody><tr>
                  <td>NEW</td>
                  <td>{$count_new}</td>
                </tr>
                <tr>
                  <td>PRC</td>
                  <td>{$count_prc}</td>
                </tr>
                <tr>
                  <td>OUT</td>
                  <td>{$count_bag}</td>
                </tr>
                <tr>
                  <td>DEL</td>
                  <td>{$count_pick}</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td>{$total_delivery}</td>
                </tr>
              </tbody></table>
          </div>
          
          <div class="right">
          <form action="{$SITE_URL}storepickups/searchPickupsData" name="delivery_search" method="POST">
          	<div class="text-box-holder">
            	<label>Store Location</label>
              <select name="store_id" id="store_id" class="select" disabled>
               <option value=" ">--Select--</option>
                {foreach from=$stores key=k item=store}
					<option {if $store->id eq $storeId} selected {/if} value="{$store->id}">{$store->store_name}</option>
                {/foreach}
              </select>
          	</div>
          	
          	{$del_status}
            <div class="text-box-holder">
            	<label>Status</label>
              <select name="delivery_status" id="1" class="select">
              <option value="">--Select--</option>
              <option value="0">New</option>
              	{foreach from=$getOrderSatusList key=k item=b}  
                <option value="{$b->id}" {if $del_status eq $b->id} selected {/if}>{$b->label}</option>
              {/foreach}    
              </select>
          	</div>
            <div class="text-box-holder2">
            	<label>Date</label>
              <input id="search_pickup_date" name="search_date" type="text"  placeholder="{$date}" value="{$date}" class="text-box">
              <a href="#" class="date-icon"></a>
          	</div>
             <input type="submit" name="submit" value="SEARCH" style="margin-top: 15px;margin-left: 26px;">
             </form>
          </div>
         
       </div>
      {if $total_delivery ne 0 } 
       <div class="deliveries-table-detail-wrapper">
       	 <h2>NEW</h2>
         <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
          
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>CANCEL</th>
          </tr>
            {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 0}
          <tr>
            <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
            	<p>{$v->address->name}<br>
              {$v->address->phone}<br>              
					{if $v->address->address1 ne ""}{$v->address->address1}</br>{/if}
					{if $v->address->address2 ne ""}{$v->address->address2}</br>{/if}
					{if $v->address->zip ne ""}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
				<p>({$item->qty}) {$item->name} </p>
			{/foreach}
             
            </td>
        
		       {foreach from=$getOrderSatusList key=k item=b}  
		            {if $b->code ne 'pickup_cancel'}
					 <td><a onclick="manageStorePickupData({$v->order_id},{$b->id},{$v->order_number})" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
					 {/if}
					{if $b->code eq 'pickup_cancel'}
						<td>	<a onclick="manageStorePickupData({$v->order_id},{$b->id})" href="javascript:void(0)">Cancel</a></td>
					{/if}
	        
			        
		        {/foreach}
         
          </tr>  
         {/if}
       {/foreach}
          
          
        </tbody></table>
				<h2>PROCESSED</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
       
            <th>PRC</th>
            <th>OUT</th>
            <th>DEL</th>
            <th>CANCEL</th>
          </tr>
          {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 1}
           <tr>
            <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
            </td>
            <td>
            	<p>{$v->address->name}<br>
              {$v->address->phone}<br>              
					{if $v->address->address1 ne ""}{$v->address->address1}</br>{/if}
					{if $v->address->address2 ne ""}{$v->address->address2}</br>{/if}
					{if $v->address->zip ne ""}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
				<p>({$item->qty}) {$item->name} </p>
			{/foreach}
             
            </td>       
		    
		        {foreach from=$getOrderSatusList key=k item=b}  
					{if $v->order_status eq 1 && $b->id eq 1}
           			  <td><a onclick="manageStorePickupData({$v->order_id},0)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
           			{/if}
					{if $b->code ne 'pickup_cancel' && $b->code ne 'pickup_prc'}
						<td><a onclick="manageStorePickupData({$v->order_id},{$b->id})" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
					{/if}
					{if $b->code eq 'pickup_cancel'}
						<td>	<a onclick="manageStorePickupData({$v->order_id},{$b->id})" href="javascript:void(0)">Cancel</a></td>
					{/if}
		        {/foreach}
			 
          </tr>  
         {/if}
       {/foreach}
         
          
        </tbody></table>
        <h2>BAGGED / READY</h2>
        <table class="deliveries-new-table">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>   
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>CANCEL</th>
          </tr>
            {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 2}
          <tr>
            <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
            	<p>{$v->address->name}<br>
              {$v->address->phone}<br>              
					{if $v->address->address1 ne ""}{$v->address->address1}</br>{/if}
					{if $v->address->address2 ne ""}{$v->address->address2}</br>{/if}
					{if $v->address->zip ne ""}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
				<p>({$item->qty}) {$item->name} </p>
			{/foreach}
             
            </td>
        
		       {foreach from=$getOrderSatusList key=k item=b}  
					{if $v->order_status eq 2 && $b->id eq 2}
           			  <td><a onclick="manageStorePickupData({$v->order_id},0)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
           			   <td><a onclick="manageStorePickupData({$v->order_id},1)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
           			{/if}
					{if $b->code ne 'pickup_cancel' && $b->code ne 'pickup_prc' && $b->code ne 'pickup_bag'}
						<td><a onclick="manageStorePickupData({$v->order_id},{$b->id})" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box1.png" alt=""></td>
					{/if}
					{if $b->code eq 'pickup_cancel'}
						<td>	<a onclick="manageStorePickupData({$v->order_id},{$b->id})" href="javascript:void(0)">Cancel</a></td>
					{/if}			        
		        {/foreach}
         
          </tr>  
         {/if}
       {/foreach}
        
          
        </tbody></table>
        <h2>PICKED UP</h2>
        <table class="deliveries-new-table border-bottom">
          <tbody><tr>
            <th>TIME</th>
            <th>ADDRESS</th>
            <th>ORDER DETAILS</th>
           
            <th>PRC</th>
            <th>BAG</th>
            <th>OUT</th>
            <th>&nbsp;</th>
          </tr>
         {foreach from=$DeliveryDataList key=k item=v}
          {if $v->order_status eq 4}
           <tr>
            <td>{$v->name}
            	<h3>{$v->timeformat}</h3>
            	<p>{$v->dayname}</p>
              <p>{$v->date}</p>
              <p><a href="#" id="{$v->order_number}" class="viewOrdDetStore">View order details</a></p>
            </td>
            <td>
            	<p>{$v->address->name}<br>
              {$v->address->phone}<br>              
					{if $v->address->address1 ne ""}{$v->address->address1}</br>{/if}
					{if $v->address->address2 ne ""}{$v->address->address2}</br>{/if}
					{if $v->address->zip ne ""}{$v->address->zip}</br>{/if}
              
              </p>
            </td>
            <td>
           
            {foreach from=$v->items key=id item=item}  
				<p>({$item->qty}) {$item->name} </p>
			{/foreach}
             
            </td>
        
		    <td><a onclick="manageStorePickupData({$v->order_id},0)" href="javascript:void(0)"><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="manageStorePickupData({$v->order_id},1)" href="javascript:void(0)""><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
            <td><a onclick="manageStorePickupData({$v->order_id},2)" href="javascript:void(0)""><img src="{$SITE_URL}app/images/deliveries-box.png" alt=""></a></td>
          </tr>  
         {/if}
       {/foreach}
        
          
        </tbody></table>
       </div>
       {/if}
      {if $total_delivery eq 0 }<div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div>{/if}
    </div>
  </div>
