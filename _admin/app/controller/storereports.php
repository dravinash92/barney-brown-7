<?php

class StoreReports extends Base {
	
	public $model;
	
	public function __construct(){
		$this->model = $this->load_model('storeReportsModel');
	}

	function index(){
		
		
		if(!isset($_SESSION['report_pin']) && $_SESSION['report_pin'] != true ){
			header("Location:".SITE_URL."storereports/verifyReportPin");
		}
		$model = $this->model;
		
		$isStore = 0; 
		$storeId = 0;
		if(isset($_SESSION['user_type']) &&  $_SESSION['user_type'] == 'store'){
			$isStore = 1;	
			$storeId = $_SESSION['admin_uid'];
		}
		
		$from_date = date("d M Y", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
		$to_date = date("d M Y");
		$this->smarty->assign('from_date',$from_date);
		$this->smarty->assign('to_date',$to_date);	
		
		$data = array('order_date_filter' => 'order_date', 'from_date' => $from_date, 'to_date' => $to_date, 'order_date_filter' => 'order_date', "pickup_store"=>$storeId);
		
		$this->smarty->assign('isStore',$isStore);
		$this->smarty->assign('storeId',$storeId);
		
		//Pickup stores
		$pickupstores = $model->getPickupStores();
		$this->smarty->assign('pickupstores',$pickupstores);
		
		
		//getOrders
		$orders = $model->getOrders($data);
		$this->smarty->assign('orders',$orders);
		
		//Discounts list
		$discounts = $model->getDiscountsList();
		$this->smarty->assign('discounts',$discounts);
		
		//System users
		$systemusers = $model->getSystemUsers();
		$this->smarty->assign('systemusers',$systemusers);
		
		$this->title = TITLE." | Admin-Total Sales";
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('content',"store_total_sales.tpl");
		$nav_ops  = array('show'  => true , 
						'url'  => array(SITE_URL.'storereports',SITE_URL.'storereports/itemReport',SITE_URL.'storereports/hourlyReport'), 
					'text' => array('TOTAL SALES','ITEM REPORT','HOURLY REPORT'),
								'active_text' => 'TOTAL SALES');
		$this->smarty->assign('SEC_NAV_OPS',$nav_ops);
		$this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','reports');	
	}

    function itemReport(){ 
		$model = $this->model;
		
		$isStore = 0; 
		$storeId = 0;
		if(isset($_SESSION['user_type']) &&  $_SESSION['user_type'] == 'store'){
			$isStore = 1;	
			$storeId = $_SESSION['admin_uid'];
		}
		$this->smarty->assign('isStore',$isStore);
		$this->smarty->assign('storeId',$storeId);
		$data =array("store_id"=>$storeId);
		
		
		$date = date("D M d, Y");
		$this->smarty->assign('date',$date);
		
		//Pickup stores
		$pickupstores = $model->getPickupStores();
		$this->smarty->assign('pickupstores',$pickupstores);
		
		$standardCatagory = $model->getStandardCategory();
		$this->smarty->assign('standardCatagory',$standardCatagory);
				
		//Discounts list
		$discounts = $model->getDiscountsList();
		$this->smarty->assign('discounts',$discounts);
		
		//System users
		$systemusers = $model->getSystemUsers();
		$this->smarty->assign('systemusers',$systemusers);
		
		$sandwichitems = $model->getSandwichItems();
		$this->smarty->assign('sandwichitems',$sandwichitems);
		
		$item_ids = array();
		foreach($sandwichitems as $ids){
			if(!in_array($ids->id,$item_ids))  $item_ids[] = $ids->id;
		}
	 
		 $standareditems = $model->getStandaredItems(); 
		$this->smarty->assign('standareditems',$standareditems);
		
		foreach($standareditems as $sids){
			if(!in_array($sids->id,$item_ids)) $item_ids[] = $sids->id;
		}
		
		
		 $ids_ = implode(',',$item_ids);
		 $total_oredr_count = $model->getTotalSaleCount($ids_); 
		
		$this->smarty->assign('salesCount',$total_oredr_count);
		
		$from_date = date("d M Y", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
		$to_date = date("d M Y");
		$this->smarty->assign('from_date',$from_date);
		$this->smarty->assign('to_date',$to_date);
		$data = array('order_date_filter' => 'order_date', 'from_date' => $from_date, 'to_date' => $to_date, 'pickup_store' =>$storeId);
		
		
		
		$standareditemsqty = $model->getStandaredItemsQty($data);
		$this->smarty->assign('standareditemsqty',$standareditemsqty);
		$customitems = $model->getCustomItems($data);
		
		$sandwich_data=array();
		foreach($customitems as $sandwichdata){
		$sandwich_data[] = $sandwichdata->sandwich_data;
		}
		for($i=0;$i<COUNT($sandwich_data);$i++){
		$json_sandwich[] = json_decode($sandwich_data[$i]);
		}
		
		/* Start PROTEIN DATA */
		$proiteindata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $pdata) {
			@$proiteindata[] = $pdata->PROTEIN->item_name;
			}
		}
		$prodataarray = array();
		if(count($proiteindata)> 0){
			foreach($proiteindata as $pro) {
				for($a=0;$a<count($pro);$a++){
					$prodataarray[]= $pro[$a];
				}
			}
		}
		$proteincount = array();
		$proteincount[] = array_count_values($prodataarray);
		$proteinsanddata = array();
		$q = 0;
		foreach($proteincount[0] as $key=>$value){
			$proteinsanddata[$q]['itemname'] = $key;
			$proteinsanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('proteinsanddata',$proteinsanddata);		
		/* End PROTEIN DATA */
		
		/* Start BREAD DATA */
		$breaddata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $bdata) {
				@$breaddata[] = $bdata->BREAD->item_name;
			}
		}
		$breaddataarray = array();
		if(count($breaddata)> 0){
			foreach($breaddata as $bre) {
				for($a=0;$a<count($bre);$a++){
					$breaddataarray[]= $bre[$a];
				}
			}
		}
		$breadcount = array();
		$breadcount[] = array_count_values($breaddataarray);
		$breadsanddata = array();
		$q = 0;
		foreach($breadcount[0] as $key=>$value){
			$breadsanddata[$q]['itemname'] = $key;
			$breadsanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('breadsanddata',$breadsanddata);
		/* End BREAD DATA */
		
		/* Start CHEESE DATA */
		$cheesedata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $cdata) {
				@$cheesedata[] = $cdata->CHEESE->item_name;
			}
		}
		$cheesedataarray = array();
		if(count($cheesedata)> 0){
			foreach($cheesedata as $che) {
				for($a=0;$a<count($che);$a++){
					$cheesedataarray[]= $che[$a];
				}
			}
		}
		$cheesecount = array();
		$cheesecount[] = array_count_values($cheesedataarray);
		$cheesesanddata = array();
		$q = 0;
		foreach($cheesecount[0] as $key=>$value){
			$cheesesanddata[$q]['itemname'] = $key;
			$cheesesanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('cheesesanddata',$cheesesanddata);
		/* End CHEESE DATA */
		
		/* Start TOPPINGS DATA */
		$toppingsdata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $tdata) {
				@$toppingsdata[] = $tdata->TOPPINGS->item_name;
			}
		}
		$toppingsdataarray = array();
		if(count($toppingsdata)> 0){
			foreach($toppingsdata as $top) {
				for($a=0;$a<count($top);$a++){
					$toppingsdataarray[]= $top[$a];
				}
			}
		}
		$toppingscount = array();
		$toppingscount[] = array_count_values($toppingsdataarray);
		$toppingssanddata = array();
		$q = 0;
		foreach($toppingscount[0] as $key=>$value){
			$toppingssanddata[$q]['itemname'] = $key;
			$toppingssanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('toppingssanddata',$toppingssanddata);
		/* End TOPPINGS DATA */
		
		/* Start CONDIMENTS DATA */
		$condidata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $codata) {
				@$condidata[] = $codata->CONDIMENTS->item_name;
			}
		}
		$condidataarray = array();
		if(count($condidata)> 0){
			foreach($condidata as $con) {
				for($a=0;$a<count($con);$a++){
					$condidataarray[]= $con[$a];
				}
			}
		}
		$condicount = array();
		$condicount[] = array_count_values($condidataarray);
		$condisanddata = array();
		$q = 0;
		foreach($condicount[0] as $key=>$value){
			$condisanddata[$q]['itemname'] = $key;
			$condisanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('condisanddata',$condisanddata);
		/* End CONDIMENTS DATA */
		
		/*Prepare standard category data*/
		foreach($standareditemscategory as $keyvp=>$vp){
			
			$items = array();
			$vp->subtotal = 0;
			$vp->qty = 0;
			foreach($standareditems as $keyv=>$v ){				
				
				if($v->standard_category_id == $vp->id){
										
					foreach($standareditemsqty as $sv){
						if($v->id == $sv->item_id){
							$v->qty = $sv->qty;
							$v->subtotal = $sv->qty * $v->product_price;
							
							$vp->subtotal += $v->subtotal;
							$vp->qty += $v->qty;
						}
					}
					
					$items[] = $v;	
				}
				
			}
			$vp->items = $items;
									
			$standareditemscategory[$keyvp] = $vp;
		}
		
		$this->smarty->assign('standareditemscategory',$standareditemscategory);		
		/*Prepare standard category data*/
		
		$this->smarty->assign('sandwich_data',$json_sandwich);
		
        $this->smarty->assign('SITE_URL',SITE_URL);
        $this->title = TITLE." | Store-Item Report";
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('content',"store_item_report.tpl");
		$nav_ops  = array('show'  => true , 
		            'url'  => array(SITE_URL.'storereports',SITE_URL.'storereports/itemReport',SITE_URL.'storereports/hourlyReport'), 
			    'text' => array('TOTAL SALES','ITEM REPORT','HOURLY REPORT'),
                            'active_text' => 'ITEM REPORT');
		$this->smarty->assign('SEC_NAV_OPS',$nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','reports');
    }


    function hourlyReport(){
    	$model = $this->model;
    	
    	$isStore = 0; 
		$storeId = 0;
		if(isset($_SESSION['user_type']) &&  $_SESSION['user_type'] == 'store'){
			$isStore = 1;	
			$storeId = $_SESSION['admin_uid'];
		}
		$this->smarty->assign('isStore',$isStore);
		$this->smarty->assign('storeId',$storeId);
		$data =array("store_id"=>$storeId);
		
		
		$date = date("D M d, Y");
		$this->smarty->assign('date',$date);
		
    	//Pickup stores
    	$pickupstores = $model->getPickupStores();
    	    	
    	$from = date('H:i:s', strtotime('12am'));
    	$todate = date('H:i:s', strtotime('11pm'));
    	
    	
    	sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
    	$lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
    	
    	sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
    	$upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
    	$times = "";
    	if ( empty( $format ) ) {
    		$format = 'g:i a';
    	}
    	$step=3600;
    	foreach ( range( $lower, $upper, $step ) as $increment ) {
    		$increment = gmdate( 'H:i', $increment );
    	
    		list( $hour, $minutes ) = explode( ':', $increment );
    	
    		$date = new DateTime( $hour . ':' . $minutes );
    		$date=$date->format( $format );
    	
    		$times .= "<option value=".$hour.">".$date."</option>";
    	}
    	$orderedItems=$model->getOrderedItems($data);
    	
    	$this->smarty->assign('orderedItems',$orderedItems);
    	$total_pickups = $total_delivery = $total = $total_sales = $total_sales_per = 0;
    	foreach($orderedItems as $time){
			$total_pickups += $time->pickups;
			$total_delivery += $time->delivery;
			$total += $time->total->total_order;
			$total_sales += $time->total->total_sales;
			$total_sales_per += $time->sales;
		}
		
		$this->smarty->assign('total_pickups',$total_pickups);
		$this->smarty->assign('total_delivery',$total_delivery);
		$this->smarty->assign('total',$total);
		$this->smarty->assign('total_sales',$total_sales);
		$this->smarty->assign('total_sales_per',$total_sales_per);
		
    	
    	
    	$this->smarty->assign('times',$times);
        $this->smarty->assign('SITE_URL',SITE_URL);
        $this->title = TITLE." | Hourly Sales";
        $this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('pickupstores',$pickupstores);
		$this->smarty->assign('content',"store_hourly_report.tpl");
		$nav_ops  = array('show'  => true , 
		            'url'  => array(SITE_URL.'reports',SITE_URL.'reports/itemReport',SITE_URL.'reports/hourlyReport'), 
			    'text' => array('TOTAL SALES','ITEM REPORT','HOURLY REPORT'),
                            'active_text' => 'HOURLY REPORT');
		$this->smarty->assign('SEC_NAV_OPS',$nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','reports');
    }
    public function getHourlyOrderReport()
    {
    	$model = $this->model;
    	
    	$isStore = 0; 
		$storeId = 0;
		if(isset($_SESSION['user_type']) &&  $_SESSION['user_type'] == 'store'){
			$isStore = 1;	
			$storeId = $_SESSION['admin_uid'];
		}
		$this->smarty->assign('isStore',$isStore);
		$this->smarty->assign('storeId',$storeId);
		$data =array("store_id"=>$storeId);
		
    	$_POST['store_id'] = $storeId;   	
		$_POST['from'] = intval($_POST['from']);  	 
		$_POST['to'] = intval($_POST['to']);  	 
    	$_POST['datehourreport'] = date('Y-m-d',strtotime($_POST['datehourreport']));   	
    	
    	$orderedItems=$model->getHourlyOrderReport($_POST);
    	$this->smarty->assign('orderedItems',$orderedItems);
    	
    	$total_pickups = $total_delivery = $total = $total_sales = $total_sales_per = 0;
    	foreach($orderedItems as $time){
			$total_pickups += $time->pickups;
			$total_delivery += $time->delivery;
			$total += $time->total->total_order;
			$total_sales += $time->total->total_sales;
			$total_sales_per += $time->sales;
		}
		
		$this->smarty->assign('total_pickups',$total_pickups);
		$this->smarty->assign('total_delivery',$total_delivery);
		$this->smarty->assign('total',$total);
		$this->smarty->assign('total_sales',$total_sales);
		$this->smarty->assign('total_sales_per',$total_sales_per);
    	
		$output = $this->smarty->fetch("hourly_report_ajax.tpl");
		print_r($output);
    	exit;
    }
    
    function doTotalSalesFilterReports(){
		$model = $this->model;
		
		$isStore = 0; 
		$storeId = 0;
		if(isset($_SESSION['user_type']) &&  $_SESSION['user_type'] == 'store'){
			$isStore = 1;	
			$storeId = $_SESSION['admin_uid'];
		}
		
		$_POST['pickup_store'] = $storeId;
		$orders = $model->doTotalSalesFilterReports($_POST);
		$this->smarty->assign('orders',$orders);
		$output = $this->smarty->fetch("total_sales_table.tpl");
		print_r($output);
		exit;		
	}
	
	function doTotalItemsFilterReports(){
		$model = $this->model;
		
		$isStore = 0; 
		$storeId = 0;
		if(isset($_SESSION['user_type']) &&  $_SESSION['user_type'] == 'store'){
			$isStore = 1;	
			$storeId = $_SESSION['admin_uid'];
		}
		$this->smarty->assign('isStore',$isStore);
		$this->smarty->assign('storeId',$storeId);
		$data =array("store_id"=>$storeId);
		
		
		$_POST['pickup_store'] = $storeId;
						
		$standardCatagory = $model->getStandardCategory();
		$this->smarty->assign('standardCatagory',$standardCatagory);
		
		$sandwichitems = $model->getSandwichItems();
		$this->smarty->assign('sandwichitems',$sandwichitems);
		
		$standareditems = $model->getStandaredItems();
		$this->smarty->assign('standareditems',$standareditems);
		
		$standareditemsqty = $model->doTotalItemsFilterReports($_POST);
		$this->smarty->assign('standareditemsqty',$standareditemsqty);
		
		
		$item_ids = array();
		
		foreach($sandwichitems as $ids){
			if(!in_array($ids->id,$item_ids))  $item_ids[] = $ids->id;
		}
		
		$standareditems = $model->getStandaredItems();
		$this->smarty->assign('standareditems',$standareditems);
		
		foreach($standareditems as $sids){
			if(!in_array($sids->id,$item_ids)) $item_ids[] = $sids->id;
		}
		
		
		$ids_ = implode(',',$item_ids);
		$total_oredr_count = $model->getTotalSaleCount($ids_);
		$this->smarty->assign('salesCount',$total_oredr_count);
		
		
		$customitems = $model->doTotalCustomItemsFilterReports($_POST);
		
		$sandwich_data=array();
		$json_sandwich=array();
		foreach($customitems as $sandwichdata){
			if($sandwichdata->sandwich_data)
			$sandwich_data[] = $sandwichdata->sandwich_data;
		}
		
		for($i=0;$i<COUNT($sandwich_data);$i++){
			$json_sandwich[] = json_decode($sandwich_data[$i]);
		}
		
		
		/* Start PROTEIN DATA */
		$proiteindata = array();
		
		if(count($json_sandwich)){
			foreach($json_sandwich as $pdata) {
				$proiteindata[] = $pdata->PROTEIN->item_name;
			}
		}
			
		$prodataarray = array();
		if(count($proiteindata)> 0){
			foreach($proiteindata as $pro) {
				for($a=0;$a<count($pro);$a++){
					$prodataarray[]= $pro[$a];
				}
			}
		}
		$proteincount = array();
		$proteincount[] = array_count_values($prodataarray);
		$proteinsanddata = array();
		$q = 0;
		foreach($proteincount[0] as $key=>$value){
			$proteinsanddata[$q]['itemname'] = $key;
			$proteinsanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('proteinsanddata',$proteinsanddata);
		/* End PROTEIN DATA */
		
		/* Start BREAD DATA */
		$breaddata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $bdata) {
				$breaddata[] = $bdata->BREAD->item_name;
			}
		}
		$breaddataarray = array();
		if(count($breaddata)> 0){
			foreach($breaddata as $bre) {
				for($a=0;$a<count($bre);$a++){
					$breaddataarray[]= $bre[$a];
				}
			}
		}
		$breadcount = array();
		$breadcount[] = array_count_values($breaddataarray);
		$breadsanddata = array();
		$q = 0;
		foreach($breadcount[0] as $key=>$value){
			$breadsanddata[$q]['itemname'] = $key;
			$breadsanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('breadsanddata',$breadsanddata);
		/* End BREAD DATA */
		
		/* Start CHEESE DATA */
		$cheesedata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $cdata) {
				$cheesedata[] = $cdata->CHEESE->item_name;
			}
		}
		$cheesedataarray = array();
		if(count($cheesedata)> 0){
			foreach($cheesedata as $che) {
				for($a=0;$a<count($che);$a++){
					$cheesedataarray[]= $che[$a];
				}
			}
		}
		$cheesecount = array();
		$cheesecount[] = array_count_values($cheesedataarray);
		$cheesesanddata = array();
		$q = 0;
		foreach($cheesecount[0] as $key=>$value){
			$cheesesanddata[$q]['itemname'] = $key;
			$cheesesanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('cheesesanddata',$cheesesanddata);
		/* End CHEESE DATA */
		
		/* Start TOPPINGS DATA */
		$toppingsdata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $tdata) {
				$toppingsdata[] = $tdata->TOPPINGS->item_name;
			}
		}
		$toppingsdataarray = array();
		if(count($toppingsdata)> 0){
			foreach($toppingsdata as $top) {
				for($a=0;$a<count($top);$a++){
					$toppingsdataarray[]= $top[$a];
				}
			}
		}
		$toppingscount = array();
		$toppingscount[] = array_count_values($toppingsdataarray);
		$toppingssanddata = array();
		$q = 0;
		foreach($toppingscount[0] as $key=>$value){
			$toppingssanddata[$q]['itemname'] = $key;
			$toppingssanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('toppingssanddata',$toppingssanddata);
		/* End TOPPINGS DATA */
		
				
		/* Start CONDIMENTS DATA */
		$condidata = array();
		if(count($json_sandwich)){
			foreach($json_sandwich as $codata) {
				$condidata[] = $codata->CONDIMENTS->item_name;
			}
		}
		$condidataarray = array();
		if(count($condidata)> 0){
			foreach($condidata as $con) {
				for($a=0;$a<count($con);$a++){
					$condidataarray[]= $con[$a];
				}
			}
		}
		$condicount = array();
		$condicount[] = array_count_values($condidataarray);
		$condisanddata = array();
		$q = 0;
		foreach($condicount[0] as $key=>$value){
			$condisanddata[$q]['itemname'] = $key;
			$condisanddata[$q]['qty'] = $value;
			$q++;
		}
		$this->smarty->assign('condisanddata',$condisanddata);
		/* End CONDIMENTS DATA */
		
		$output = $this->smarty->fetch("store_total_items_table.tpl");
		print_r($output);
		exit;
	}
	
	public function showOrderDetails(){
		$model = $this->model;
		$this->title = TITLE." | Admin-Customer Profile";
		$this->smarty->assign('SITE_URL',SITE_URL);
		$order_id = $this->UrlArray[2];
		
		$orderDetails = $model->getOrderDetails(array("order_id"=>$order_id));
		$this->smarty->assign('orderDetails',$orderDetails[0]);
	 
		
		$this->smarty->assign('content',"order_details.tpl");
		$nav_ops  = array('show'  => true , 
						'url'  => array(SITE_URL.'reports',SITE_URL.'reports/itemReport',SITE_URL.'reports/hourlyReport'), 
					'text' => array('TOTAL SALES','ITEM REPORT','HOURLY REPORT'),
								'active_text' => 'TOTAL SALES');
		$this->smarty->assign('SEC_NAV_OPS',$nav_ops);
		$this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','reports');	
	}
	
	public function verifyReportPin(){
		
		$model = $this->model;
		$this->title = TITLE." | Admin-Reports";
		$this->smarty->assign('SITE_URL',SITE_URL);
		$redirect="";
		if(isset($_GET['r']))
			$redirect=$_GET['r'];
		if(isset($_SESSION['report_pin']) && $_SESSION['report_pin'] == true ){
			if($redirect="storemanage")
			header("Location:".SITE_URL."storemanage");
				
				else
			header("Location:".SITE_URL."storereports");				
		}else{
			
			$error = 0;
			if(isset($_GET['error']))
				$error = 1;
			
			$storeId = $_SESSION['admin_uid'];
			$storeName = $_SESSION['uname'];
			$this->smarty->assign('storeId',$storeId);
			$this->smarty->assign('storeName',$storeName);
			$this->smarty->assign('error',$error);
			$this->smarty->assign('redirect',$redirect);
			$this->smarty->assign('content',"verify_report.tpl");	
		
		}
	}
		
	public function checkReportPin(){
		
		$model = $this->model;
		
		$response = $model->checkReportPin($_POST);
		if($response==true){
			$_SESSION['report_pin'] = true;
			if($_POST['redirect']=="storemanage")
			header("Location:".SITE_URL."storemanage");
				
				else
			header("Location:".SITE_URL."storereports");
		}else{
			header("Location:".SITE_URL."storereports/verifyReportPin/?error=1");				
		}
	}
	
}
