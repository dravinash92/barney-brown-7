<?php

class StoreManage extends Base
{
	public $model;

	public function __construct(){

		$this->model = $this->load_model('accountsModel');
	}

	function index(){
		$model = $this->model;
		if(!isset($_SESSION['report_pin']) && $_SESSION['report_pin'] != true ){
			header("Location:".SITE_URL."storereports/verifyReportPin/?r=storemanage");
		}
		$this->title = TITLE." | Manage Store";
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('SITE_URL',SITE_URL);
		
		
		$isStore = 0;
		$storeId = 0;
		if(isset($_SESSION['user_type']) &&  $_SESSION['user_type'] == 'store'){
			$isStore = 1;
			$storeId = $_SESSION['admin_uid'];
		}
		
		$this->smarty->assign('isStore',$isStore);
		$this->smarty->assign('storeId',$storeId);
		$data['id'] = $storeId;
		
		$storeData=  $model->get_store_account(API_URL,$data);
		$store_timing_id=array();
		$days	=	array();
		$open	=	array();
		$close	=	array();
                $store_active    = array();
		
		
		foreach($storeData->store_timings as $c){
			$store_timing_id[]=$c->id;
			$days[]=$c->day;
			$open[]= $c->open;
			$close[]= $c->close;
			$delivery[]=$c->delivery_active;
			$pickup[]=$c->pickup_active;
                        $store_active[]	  = $c->store_active;
			
		}
	 
		
		$timesections = array();
		if ( empty( $format ) ) {
			$format = 'g:i a';
		}
		$step=900;
		$from = date('H:i:s', strtotime('8am'));
		$todate = date('H:i:s', strtotime('10pm'));
		
		
		sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
		$lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		
		
		sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
		$upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		
		foreach ( range( $lower, $upper, $step ) as $increment ) {
			$increment = gmdate( 'H:i', $increment );
		
			list( $hour, $minutes ) = explode( ':', $increment );
		
			$date = new DateTime( $hour . ':' . $minutes );
			$date=$date->format( $format );
		
			$timesections[] .= $date;
		}
		 
		
		$this->smarty->assign('timesections',$timesections);
		$this->smarty->assign('store_timings_id',$store_timing_id);
		$this->smarty->assign('store_timings_day',$days);
		$this->smarty->assign('store_timings_open',$open);
		$this->smarty->assign('store_timings_close',$close);
		$this->smarty->assign('delivery_active',$delivery);
		$this->smarty->assign('pickup_active',$pickup);
                $this->smarty->assign('store_active', $store_active);
		
		

		$store_accounts = $model->get_store_accounts_data(API_URL,$data);
		$this->smarty->assign('store_accounts',$store_accounts);
		
		$this->smarty->assign('store_accounts_data',$storeData);
		$this->smarty->assign('isStore',$isStore);
		
		$this->smarty->assign('content',"edit_store_account_data.tpl");
	}
}