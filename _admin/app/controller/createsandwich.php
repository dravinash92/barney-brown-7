<?php
ini_set('display_errors',2);
class CreateSandwich extends Base
{
    public $model;
    
    public function __construct()
    {
        $this->model = $this->load_model('createSandwichModel');
    }
    
    function index()
    {

        $user_id = $this->UrlArray[2];
        if (!intval($user_id)) {
            echo "<script>window.location='" . SITE_URL . "neworder';</script>";
            exit;
        }
        
        $dataid      = $this->UrlArray[3];
        $model       = $this->model;
        $bread_data  = $model->load_data(1);
        // echo "<pre>";print_r($bread_data);exit;
        $this->title = TITLE;
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('CMS_URL', CMS_URL);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('BREAD_DATA', $bread_data);
        $this->smarty->assign('OPTN_DATA', $this->smarty->fetch('sandwich-bread.tpl'));
        $data = json_decode($model->get_user_sanwich_data($dataid, $user_id));
        $this->smarty->assign('DATA_ID', $dataid);
        $this->smarty->assign('TO_USER_ID', $user_id);
        $this->smarty->assign('SANDWICH_NAME', @$data->Data[0]->sandwich_name);
        $sandwich_name = json_decode($model->get_random_name());
        $this->smarty->assign('BASE_FARE', BASE_FARE);
        $this->smarty->assign('LANDING_PAGE', $this->smarty->fetch('sandwich-landing.tpl'));
        if (@$data->Data[0]->sandwich_name)
            $this->smarty->assign('SANDWICH_NAME', @$data->Data[0]->sandwich_name);
        else
            $this->smarty->assign('SANDWICH_NAME', $sandwich_name->Data->name);
        $image_data = [];
        //$image_data = $this->preload_images();
        //$this->smarty->assign('SANDWICH_IMAGES', $image_data);
        $this->smarty->assign('IS_PUB', @$data->Data[0]->is_public);
        $uid      = $this->UrlArray[2];
        $usrfname = $model->get_user_firstName($uid);
        $this->smarty->assign('fname', $usrfname);
        $this->smarty->assign('content', "create_sandwich.tpl");
        
        
    }
    
    
    function preload_images()
    {
        $model  = $this->model;
        $images = $model->get_all_images();
        $this->smarty->assign('images', $images);
        $this->smarty->assign('ADMIN_URL', SITE_URL);
        $data = $this->smarty->fetch('preload.tpl');
        return $data;
    }
    
    
    function get_category_items_data()
    {
        
        $ids = $_POST['ids'];
        if ($ids) {
            $json   = $this->model->get_category_items_data($ids);
            $unjson = json_decode($json);
            echo json_encode($unjson->Data);
        }
        exit;
    }
    
    
    
    function getRandom_sandwich_name()
    {
        $sandwich_name = json_decode($this->model->get_random_name());
        echo $sandwich_name->Data->name;
        exit;
    }
    
    function edit()
    {
        
        $user_id = $this->UrlArray[2];
        if (!intval($user_id)) {
            echo "<script>window.location='" . SITE_URL . "neworder';</script>";
            exit;
        }
        
        $dataid      = $this->UrlArray[3];
        $model       = $this->model;
        $bread_data  = $model->load_data(1);
        $this->title = TITLE;
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('CMS_URL', CMS_URL);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('BREAD_DATA', $bread_data);
        $this->smarty->assign('OPTN_DATA', $this->smarty->fetch('sandwich-bread.tpl'));
        $data = json_decode($model->get_user_sanwich_data($dataid, $user_id));
        $this->smarty->assign('DATA_ID', $dataid);
        $this->smarty->assign('TO_USER_ID', $user_id);
        $this->smarty->assign('SANDWICH_NAME', @$data->Data[0]->sandwich_name);
        $this->smarty->assign('IS_PUB', @$data->Data[0]->is_public);
        $this->smarty->assign('content', "create_sandwich.tpl");
    }
    
    function user_input()
    {
        $model = $this->model;
        $data  = array(
            'id' => @$_POST['id'],
            'data' => @$_POST['data'],
            'uid' => @$_POST['uid'],
            'price' => @$_POST['price'],
            'is_pub' => @$_POST['is_pub'],
            'name' => @$_POST['name'],
            'sandwich_items' => @$_POST['items_names'],
            'tempname' => @$_POST['tempname'],
            'menu_active' => @$_POST['menu_active'],
            'toast' => @$_POST['toast'],
            'by_admin' => 1,
            'is_saved' => @$_POST['is_saved']
            
        );
        echo $model->input_user_sanwich_data($data);
        exit;
    }
    
    
    function get_sandwich_data()
    {
        $model = $this->model;
        $uid   = $_POST['uid'];
        $id    = $_POST['id'];
        echo $model->get_user_sanwich_data($id, $uid);
        exit;
    }
    
    
    function load_bread_data()
    {
        $model       = $this->model;
        $bread_data  = $model->load_data(1);
        $this->title = TITLE;
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('BREAD_DATA', $bread_data);
        $data = $this->smarty->fetch('sandwich-bread.tpl');
        echo $data;
        exit;
    }
    
    
    function load_protien_data()
    {
        $model        = $this->model;
        $protien_data = $model->load_data(2);
        //echo "<pre>";print_r($protien_data);exit;
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('PROTIEN_DATA', $protien_data);
        $data = $this->smarty->fetch('sandwich-protein.tpl');
        echo $data;
        
        exit;
    }
    
    function load_cheese_data()
    {
        $model       = $this->model;
        $cheese_data = $model->load_data(3);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('CHEESE_DATA', $cheese_data);
        $data = $this->smarty->fetch('sandwich-cheese.tpl');
        echo $data;
        exit;
    }
    
    
    function load_toppings_data()
    {
        $model         = $this->model;
        $toppings_data = $model->load_data(4);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('TOPPINGS_DATA', $toppings_data);
        $data = $this->smarty->fetch('sandwich-toppings.tpl');
        echo $data;
        exit;
    }
    
    
    function load_condiments_data()
    {
        $model           = $this->model;
        $condiments_data = $model->load_data(5);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('CONDIEMENTS_DATA', $condiments_data);
        $data = $this->smarty->fetch('sandwich-condiments.tpl');
        echo $data;
        exit;
    }
    
    
    function filter_words()
    {
        echo $this->model->filter_words();
        exit;
    }
    
    
    function save_image()
    {
        
        $width             = @$_POST['width'];
        $height            = @$_POST['height'];
        $json_image_string = @$_POST['json_string'];
        $path              = @$_POST['path'];
        $file              = @$_POST['file_name'];
        if (isset($_POST['path']))
        	$path = SANDWICH_UPLOAD_DIR . $_POST['path'];
        	else
        		$path = SANDWICH_UPLOAD_DIR;
        $img = json_decode($json_image_string);
        if (!extension_loaded('gd')) {
            exit('GD requires to do this operation');
        }
        
        
        if (!is_dir($path)) {
            if (!mkdir($path, 0777, true)) {
                exit('error creating path folder');
            }
        }

        if (!is_dir($path . "thumbnails/")) {
            if (!mkdir($path . "thumbnails/", 0777, true)) {
                exit('error creating thumbnail path folder');
            }
        }
        
        
        $layers = array();
        $width  = array();
        $height = array();
        foreach ($img->images as $images) {
            $layers[]  = imagecreatefrompng(SITE_URL . 'upload/' . $images);
            $data      = getimagesize(SITE_URL . 'upload/' . $images);
            $widths[]  = $data[0];
            $heights[] = $data[1];
        }
        
        $width  = max($widths);
        $height = max($heights);
        
        $image = imagecreatetruecolor($width, $height);
        $thumb_image = imagecreatetruecolor(250, 150); // for thumbnail
        
        //to make background transparent
        imagealphablending($image, false);
        $transparency = imagecolorallocatealpha($image, 0, 0, 0, 127);
        imagefill($image, 0, 0, $transparency);
        imagesavealpha($image, true);

        // to make thumbnail background transparent
        imagealphablending($thumb_image, false);
        $transparency = imagecolorallocatealpha($thumb_image, 0, 0, 0, 127);
        imagefill($thumb_image, 0, 0, $transparency);
        imagesavealpha($thumb_image, true);
        
        /* if you want to set background color
        $white = imagecolorallocate($image, 255, 255, 255);
        imagefill($image, 0, 0, $white);
        */
        
        imagealphablending($image, true);
        for ($i = 0; $i < count($layers); $i++) {
            imagecopy($image, $layers[$i], 0, 0, 0, 0, $width, $height);
        }
        imagealphablending($image, false);
        imagesavealpha($image, true);

        //for thumbnail
        imagealphablending($thumb_image, true);
        for ($i = 0; $i < count($layers); $i++) {
            imagecopy($thumb_image, $layers[$i], 0, 0, 0, 0, $width, $height);
        }
        imagealphablending($thumb_image, false);
        imagesavealpha($thumb_image, true);

        echo imagepng($image, $path . $file);
       if(file_exists($path . "thumbnails/". $file)) unlink($path . "thumbnails/". $file);
            imagecopyresampled($thumb_image, $image, 0, 0, 26, 6, 250, 150, 597, 354);
        echo imagepng($thumb_image, $path . "thumbnails/". $file);
        exit;
        
    }
    
    public function setTempSandwichesToShow()
    {
        if (!isset($_SESSION['temp_sandwiches'])) {
            $_SESSION['temp_sandwiches'] = array();
        }
        
        $_SESSION['temp_sandwiches'][] = $_POST['sandwich_id'];
        exit;
    }
}
