<?php

class Customer extends Base
{
    public $model;
    public function __construct()
    {
        $this->model = $this->load_model('customerModel');
    }
    
    function index()
    {
        $model       = $this->model;
		$limit = 100;
		$page = empty($_GET['page']) ? 1 : (int)$_GET['page'];
		$start = $page == 1 ? 0 : (($page - 1) * $limit - 1);
		
		$data['start'] =  $start;
		$data['limit'] =  $limit;
		$totalCount = $model->get_customer_data_count($data);
		$this->smarty->assign('totalCount',$totalCount);
		$lastPage = (int)($totalCount/$limit);
		$remain = $totalCount%$limit;
		if($remain) $lastPage = $lastPage+1;
		$this->smarty->assign('lastPage',$lastPage);
        $this->title = TITLE . " | Admin-Customers Search";
        
        $type  = '';
        $value = '';
        if (!empty($_GET['searchbyname'])) {
            $type  = 'name';
            $value = $_GET['searchbyname'];
        }
        if (!empty($_GET['searchbylastname'])) {
            $type  = 'lastname';
            $value = $_GET['searchbylastname'];
        }
        if (!empty($_GET['searchphone'])) {
            $type  = 'phone';
            $value = $_GET['searchphone'];
        }
        if (!empty($_GET['searchemail'])) {
            $type  = 'email';
            $value = $_GET['searchemail'];
        }
        if (!empty($_GET['searchcompany'])) {
            $type  = 'company';
            $value = $_GET['searchcompany'];
        }
        
        $moddata = array(
            'search' => $type,
            'searchval' => $value,'limit'=>$limit,'start'=>$start
        );
        
        
        
        $response_users = $model->get_customer_data($moddata);
        
        if ($response_users) {
            if ($response_users->Data)
                $users_data = $model->process_user_data($response_users->Data);
            else
                $users_data = "";
        } else {
            $users_data = "";
        }
        
        if (isset($users_data[0]['uid'])) {
            $this->smarty->assign('users_data', $users_data);
        }
        $this->smarty->assign('total_item_count',$totalCount);
        $this->smarty->assign('search_condition',$type);
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('content', "customer_search.tpl");
        $nav_ops = array(
            'show' => false
        );
        
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'customers');
        
    }
    
    function loadmoreCustomer()
	{
		 
        
        $model       = $this->model;
		$count            =  $_POST['count'];
		$limit            =  50;
		$page             =  round($count/$limit);
		$data['limit']    =  $limit;
		if($page == 0) $data['start']    = $count;
		else { $data['start']    = $limit*$page;  }
		
	
		$totalCount = $model->get_customer_data_count($data);
		$this->smarty->assign('totalCount',$totalCount);
		$lastPage = (int)($totalCount/$limit);
		$remain = $totalCount%$limit;
		if($remain) $lastPage = $lastPage+1;
		$this->smarty->assign('lastPage',$lastPage);
        $this->title = TITLE . " | Admin-Customers Search";
        
        $type  = '';
        $value = '';
        if (!empty($_GET['searchbyname'])) {
            $type  = 'name';
            $value = $_GET['searchbyname'];
        }
        if (!empty($_GET['searchbylastname'])) {
            $type  = 'lastname';
            $value = $_GET['searchbylastname'];
        }
        if (!empty($_GET['searchphone'])) {
            $type  = 'phone';
            $value = $_GET['searchphone'];
        }
        if (!empty($_GET['searchemail'])) {
            $type  = 'email';
            $value = $_GET['searchemail'];
        }
        if (!empty($_GET['searchcompany'])) {
            $type  = 'company';
            $value = $_GET['searchcompany'];
        }
        
        $moddata = array(
            'search' => $type,
            'searchval' => $value,'limit'=>$limit,'start'=>$data['start']
        );
        
        
        
        $response_users = $model->get_customer_data($moddata);
        
        if ($response_users) {
            if ($response_users->Data)
                $users_data = $model->process_user_data($response_users->Data);
            else
                $users_data = "";
        } else {
            $users_data = "";
        }
        
        if (isset($users_data[0]['uid'])) {
            $this->smarty->assign('users_data', $users_data);
        }
        $this->smarty->assign('total_item_count',$totalCount);
        $this->smarty->assign('SITE_URL', SITE_URL);
        echo $this->smarty->fetch("customer_loadmore.tpl");
		exit;
	}
    function profile()
    {
        
        $model       = $this->model;
        $this->title = TITLE . " | Admin-Customer Profile";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $uid = $this->UrlArray[2];
        
        $get_customer_detais  = $model->get_customers_details(API_URL, array(
            'id' => $uid
        ));
        $data                 = json_decode($get_customer_detais);
        $data                 = get_object_vars($data->Data[0]);
        $user_review          = $model->get_user_review(array(
            'uid' => $uid
        ));
        $user_review_contents = $model->get_all_review_contents();
        $this->smarty->assign('user_review_contents', $user_review_contents);
        $this->smarty->assign('user_review', $user_review);
        if (isset($user_review['id']))
            $this->smarty->assign('user_review_messages', $model->get_user_review_messages(array(
                'id' => $user_review['id']
            )));
        $this->smarty->assign('udata', $data);
        $this->smarty->assign('UID', $uid);
        $this->smarty->assign('content', "customer_profile.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'customers');
        
    }
    
    function ajax_tab_load()
    {
        $type  = $_POST['type'];
        $model = $this->model;
        $uid   = $_POST['uid'];
        
        switch ($type) {
            
            case 1:
                $get_customer_detais  = $model->get_customers_details(API_URL, array(
                    'id' => $uid
                ));
                $data                 = json_decode($get_customer_detais);
                $data                 = get_object_vars($data->Data[0]);
                $user_review          = $model->get_user_review(array(
                    'uid' => $uid
                ));
                $user_review_contents = $model->get_all_review_contents();
                $this->smarty->assign('user_review_contents', $user_review_contents);
                $this->smarty->assign('user_review', $user_review);
                if (isset($user_review['id']))
                    $this->smarty->assign('user_review_messages', $model->get_user_review_messages(array(
                        'id' => $user_review['id']
                    )));
                $this->smarty->assign('udata', $data);
                $this->smarty->assign('UID', $uid);
                
                $HTML = $this->smarty->fetch("customer_profile_tab.tpl");
                break;
            
            case 2:
                
                $data['user_id'] = $uid;
                $orderhistory    = $model->orderhistory($data);
                $this->smarty->assign('orderhistory', $orderhistory);
                $HTML = $this->smarty->fetch("myaccount-order-history.tpl");
                
                break;
            
            case 3:
                $data['user_id'] = $uid;
                $orderhistory    = $model->getSavedMeals($data);
                $this->smarty->assign('orderhistory', $orderhistory);
                $HTML = $this->smarty->fetch("myaccount-saved-meals.tpl");
                
                break;
            
            case 4:
                
                $addresses = $model->listSavedAddress($uid);
                $this->smarty->assign('addresses', $addresses);
                $HTML = $this->smarty->fetch("myaccount-saved-address.tpl");
                
                break;
            
            case 5:
                
                $data['uid']  = $uid;
                $card_details = $model->savedBilling($data);
                $this->smarty->assign('card_details', $card_details);
                $HTML = $this->smarty->fetch("myaccount-saved-billing.tpl");
                
                
                break;
            
            case 6:
                
                $HTML = $this->smarty->fetch("myaccount-mail-reminder.tpl");
                
                break;
            
            default:
                $get_customer_detais  = $model->get_customers_details(API_URL, array(
                    'id' => $uid
                ));
                $data                 = json_decode($get_customer_detais);
                $data                 = get_object_vars($data->Data[0]);
                $user_review          = $model->get_user_review(array(
                    'uid' => $uid
                ));
                $user_review_contents = $model->get_all_review_contents();
                $this->smarty->assign('user_review_contents', $user_review_contents);
                $this->smarty->assign('user_review', $user_review);
                $this->smarty->assign('udata', $data);
                $this->smarty->assign('UID', $uid);
                $HTML = $this->smarty->fetch("customer_profile_tab.tpl");
                break;
                
        }
        echo $HTML;
        exit;
    }
    
    
    function update_customer()
    {
        
        $model = $this->model;
        $data  = array();
        
        if (isset($_POST['type']) AND $_POST['type'] == 'name') {
            $name          = preg_split('/\s+/', trim($_POST['name']));
            $data['fname'] = $name[0];
            $data['lname'] = $name[1];
        }
        
        if (isset($_POST['nameinp'])) {
            $data['nameinp'] = $_POST['nameinp'];
        }
        
        if (isset($_POST['type']) AND $_POST['type'] == 'email') {
            $data['email'] = $_POST['email'];
            
            if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
                exit('Error: Invalid Email');
            }
        }
        
        
        if (isset($_POST['type']) AND $_POST['type'] == 'password') {
            $data['password'] = $_POST['password'];
            if (strlen($_POST['password']) < 6)
                exit('Error: Your password must be at least 6 characters long');
        }
        
        
        if (isset($_POST['type']) AND $_POST['type'] == 'phone') {
            $data['phone'] = $_POST['name'];
        }
        
        if (isset($_POST['type']) AND $_POST['type'] == 'company') {
            $data['company'] = $_POST['name'];
        }
        
        $data['uid'] = $_POST['uid'];
        
        $data['type'] = $_POST['type'];
        
        
        $model->update_customer($data);
        exit;
    }
    
    /*Added */
    function saveCustomerInfo()
    {
        $model  = $this->model;
        $result = $model->savingCustomerInfo($_POST);
        echo $result;
        exit;
    }
    
    function addCustomerNote()
    {
        $model = $this->model;
        $notes = $model->addCustomerNote($_POST);
        
        $output = "";
        
        foreach ($notes as $note) {
            $output .= '<div class="note">
				<p><span class = "note_date">' . $note->date . '</span><span class="address">' . $note->notes . '</span></p>
				<br/>
				<a href="javascript:void(0)" data-id="' . $note->id . '" class="remove remove_customer_note">Remove</a>
         	</div>';
        }
        
        echo $output;
        
        exit;
    }
    
    function removeCustomerNote()
    {
        $model  = $this->model;
        $result = $model->removeCustomerNote($_POST);
        echo $result;
        exit;
    }
    
    function savingAddress()
    {
        $model  = $this->model;
        $result = $model->savingUserAddress($_POST);
        echo $result;
        exit;
    }
    
    function removeAddress()
    {
        $model  = $this->model;
        $result = $model->removeAddress($_POST);
        echo $result;
        exit;
    }
    
    function editAddress()
    {
        $model   = $this->model;
        $result  = $model->editAddress($_POST);
        $address = $result[0];
        $this->smarty->assign('SITE_URL', SITE_URL);
        if ($address->address_id) {
            $phone = explode("-", $address->phone);
            $this->smarty->assign('phone1', $phone[0]);
            $this->smarty->assign('phone2', $phone[1]);
            $this->smarty->assign('phone3', $phone[2]);
            $this->smarty->assign('address', $address);
            $output = $this->smarty->fetch("edit-address.tpl");
        } else {
            $output = "No address found";
        }
        echo $output;
        exit;
    }
    
    function get_billing()
    {
        $model = $this->model;
        $model->get_Billinginfo($_POST, true);
        exit;
    }
    
    function cardAddEditEvent()
    {
        $model = $this->model;
        $data  = $model->cardAddEditEvent($_POST);
        echo $data;//json_encode($data);
        exit;
    }

     function cardRemoveEvent(){
        $model  = $this->model;
        $data = $model->cardRemoveEvent($_POST);
        exit;
    }
    
    function newUserAccount()
    {
        
        $data = array();
        if (isset($_POST['customername'])) {
            $data['first_name'] = $_POST['customername'];
        }
        if (isset($_POST['lastname'])) {
            $data['last_name'] = $_POST['lastname'];
        }
        if (isset($_POST['customeremail'])) {
            $data['username'] = $_POST['customeremail'];
        }
        if (isset($_POST['company'])) {
            $data['company'] = $_POST['company'];
        }
        
        $data['is_admin_created'] = 1;
        
        $model    = $this->model;
        $response = $model->createnewUser($data);
        
        echo json_encode($response);
        
        exit;
    }
    
    function addUserReview()
    {
        $model = $this->model;
        $data  = $model->addUserReview($_POST);
        echo json_encode($data);
        exit;
    }
    public function reorder(){
    	$model = $this->model;
    	$response = $model->reorder($_POST);
    	echo $response;
    	exit;
    }

    
}
