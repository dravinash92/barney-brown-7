<?php

class Web extends Base
{
	public $model;
	
	public function __construct(){
		$this->model = $this->load_model('webModel');
	}	
	
	function index(){
	
	 $this->title = TITLE." | Webpage";
	 $this->smarty->assign('SITE_URL',SITE_URL);
	 $data ="";
	 $model = $this->model;
	 $webpageData =  $model->get_webpages_data(API_URL,$data);
	  
	 
	 $this->smarty->assign('webpageData',$webpageData);
	 $this->smarty->assign('content',"web_pages.tpl");	
	 $nav_ops  = array('show'  => false );
     $this->smarty->assign('SEC_NAV_OPS',$nav_ops);
     $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','web');
		
	}
	
	
	function homepage(){
		
	 $this->title = TITLE." | Admin-Homepage";
	 $this->smarty->assign('SITE_URL',SITE_URL);
	 $data ="";
	 $model = $this->model;
	 $homepageData =  $model->get_web_homepage_data(API_URL,$data);

	 
	 $this->smarty->assign('homepageData',$homepageData);
	 $this->smarty->assign('content',"homepage.tpl");
	 $nav_ops  = array('show'  => false );
	 $this->smarty->assign('SEC_NAV_OPS',$nav_ops);
	 $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','web');
	
	}
	
	function newBanner(){
	
     $this->title = TITLE." | Admin-Homepage Banner";
	 $this->smarty->assign('SITE_URL',SITE_URL);
	 $this->smarty->assign('content',"new_banner.tpl");
	 $nav_ops  = array('show'  => false);
	 $this->smarty->assign('SEC_NAV_OPS',$nav_ops);
	 $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','web');
	
	}
	
	function addWebPageBannerData(){
		
		    $model = $this->model;
        	
        	$image ="";
			
			if(!empty($_FILES['item_image']['name'])){
				$target_dir = "upload/";
				$target_file = $target_dir . basename($_FILES["item_image"]["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$final_file = date("Ymd_his").".".$imageFileType;
        		
				if ($_FILES["item_image"]["size"] > 500000) {
					echo "Sorry, your file is too large.";
					$uploadOk = 0;
				}
				
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"	&& $imageFileType != "gif" ) {
					echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					$uploadOk = 0;
				}
			
				if ($uploadOk == 0) {
					echo "Sorry, your file was not uploaded.";
					
				} else {
					if(!is_dir($target_dir)) mkdir($target_dir,0777,true);
					if (move_uploaded_file($_FILES["item_image"]["tmp_name"], $target_dir.$final_file)) {
						"The file ". basename( $_FILES["item_image"]["name"]). " has been uploaded.";
					} else {
						echo "Sorry, there was an error uploading your file.";
						exit;
					}
				}
				
				$image = $final_file;
        	}else{
				
				$image = $_POST['org_file'];
			}
			if($_POST['link'] == "") $_POST['link'] = '#';
        	
        	$data     = array(
        			'banner_name'=> @$_POST['banner_name'],
        			'banner_type'=> @$_POST['banner'],
        			'text'=> @$_POST['text'],
        			'banner_status'=>@$_POST['banner_status'],
        			'button_text'=>@$_POST['button_text'],
        			'link'=>@$_POST['link'],
        			'redirection_status'=>@$_POST['redirection_status'],
        			'image' => $image
        	);
        	$getDeliveryDetails =  $model->add_webpage_banner_data(API_URL,$data);
        	header("Location:".SITE_URL."web/homepage");
        	exit;
        
	}
	
	function updateWebHomePagePriority(){
		
		$a=implode(',',$_POST['hidden_webhomepage_ids']);
		$b=implode(',',$_POST['priority']);
		if(isset($_POST['live']))
			$live = $_POST['live'];
		else 
			$live=0;
		$data     = array(
				'id'=>$a,
				'priority'=> $_POST['priority'],
				'live'=>$live
		);
		$model = $this->model;
		$test = $model->update_web_home_page_priority(API_URL,$data);
		$_SESSION['success'] = "Updated Successfully";
		header("Location:".SITE_URL."web/homepage");
		exit;
	}
	
	
	function editWebHomePagePriority(){
		$this->title = TITLE." | Edit Admin-User Account";
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['id'] = $this->UrlArray[2];
		$model = $this->model;
		$bannerData =  $model->get_web_homepage_banner(API_URL,$data);
		$this->smarty->assign('bannerData',$bannerData);
		$this->smarty->assign('content',"edit_banner.tpl");	
	}
	
	
	
	function updateWebPageBannerData(){
		$model = $this->model;
		$image ="";
			
			if(!empty($_FILES['item_image']['name'])){
				$target_dir = "upload/";
				$target_file = $target_dir . basename($_FILES["item_image"]["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$final_file = date("Ymd_his").".".$imageFileType;
        		
		
				
				if ($_FILES["item_image"]["size"] > 2097152) {
					echo "Sorry, your file is too large.";
					$uploadOk = 0;
				}
				
if($_FILES["item_image"]["type"] != "image/jpeg" && $_FILES["item_image"]["type"] != "image/pjpeg" && $_FILES["item_image"]["type"]  != "image/png" && $_FILES["item_image"]["type"]  != "image/gif" ) {
					echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					$uploadOk = 0;
				}
			
				if ($uploadOk == 0) {
					echo "Sorry, your file was not uploaded.";
					exit;
				} else {
					if(!is_dir($target_dir)) mkdir($target_dir,0777,true);
					if (move_uploaded_file($_FILES["item_image"]["tmp_name"], $target_dir.$final_file)) {
						"The file ". basename( $_FILES["item_image"]["name"]). " has been uploaded.";
					} else {
						echo "Sorry, there was an error uploading your file.";
						
					}
				}
				
				$image = $final_file;
        	}else{
				
				$image = $_POST['org_file'];
			}
			
			if($_POST['link'] == "") $_POST['link'] = '#';
		
		$data     = array(
				'id'=>@$_POST['hidden_id'],
				'banner_name'=> @$_POST['banner_name'],
				'banner_type'=> @$_POST['banner'],
				'text'=> @$_POST['text'],
				'banner_status'=>@$_POST['banner_status'],
				'button_text'=>@$_POST['button_text'],
				'link'=>@$_POST['link'],
				'redirection_status'=>@$_POST['redirection_status'],
				'image' =>$image
		);
		 
		
		$getDeliveryDetails =  $model->update_webpage_banner_data(API_URL,$data);
		header("Location:".SITE_URL."web/homepage");
		
	
	}
	
	function webpage(){
		$this->title = TITLE." | Edit Admin-User Account";
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['id'] = $this->UrlArray[2];	
		$model = $this->model;
		$webpageData =  $model->get_webpagedata_data(API_URL,$data);
		$this->smarty->assign('webpageData',$webpageData);
		$this->smarty->assign('content',"edit_web_page.tpl");	
	}
	
	function updateWebPageData(){
		$model = $this->model;
		$getDeliveryDetails =  $model->update_webpage_data(API_URL,$_POST);
		header("Location:".SITE_URL."web");
		exit;
	}
	
	function deleteHomepageBanner(){
		$data['id'] = $this->UrlArray[2];
		$model = $this->model;
		$admin_user_category = $model->delete_home_page_banner(API_URL,$data);
		header("Location:".SITE_URL."web/homepage");	
	}
}