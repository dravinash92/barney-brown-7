<?php

class Pickups extends Base
{
    public $model;
    
    public function __construct()
    {
        $this->model = $this->load_model('pickupsModel');
    }
    
    function index()
    {
        $this->title = TITLE . " | Admin-Pickups";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data  = "";
        $model = $this->model;
        
        $date = date("d M Y");
        $this->smarty->assign('date', $date);
        //Pickup stores
        $pickupstores = $model->getPickupStores();
        $this->smarty->assign('pickupstores', $pickupstores);
        
        $getAddress = $model->get_all_pickups_items(API_URL, $data);
        // /echo "<pre>";print_r($getAddress);exit;

		if(count($getAddress)>0){
			foreach ($getAddress as $result) {
				
				if ($result->order_status == 0) {
					$count_new[] = $result->order_status;
				}
				
				if ($result->order_status == 1) {
					$count_prc[] = $result->order_status;
				}
				
				if ($result->order_status == 2) {
					$count_bag[] = $result->order_status;
				}
				
				if ($result->order_status == 3) {
					$count_pick[] = $result->order_status;
				}
				
			}
        }
        $orderAdd = 'Order';
        $this->smarty->assign('count_new', count(@$count_new));
        $this->smarty->assign('count_prc', count(@$count_prc));
        $this->smarty->assign('count_bag', count(@$count_bag));
        $this->smarty->assign('count_pick', count(@$count_pick));
        
        
        $total_pickUps = count(@$count_new) + count(@$count_prc) + count(@$count_bag) + count(@$count_pick);
        
        $this->smarty->assign('total_pickUps', $total_pickUps);
        
        $data['status'] = '0';
        $getOrderSatus  = $model->get_all_order_status(API_URL, $data);
        $this->smarty->assign('getOrderSatusList', $getOrderSatus);
        
        $this->smarty->assign('order', $orderAdd);
        $this->smarty->assign('addressList', $getAddress);
        $this->smarty->assign('content', "pickups.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'pickup');
    }
    
     
    function get_order_popup_data()
    {
        
        $order_id  = @$_POST['order_id'];
        $order_det = $this->model->get_order_popup_data($order_id);
        
        $this->smarty->assign('authorizenet_transaction_id', @$order_det->Data->authorizenet_transaction_id);
        $this->smarty->assign('order_total_count', @$order_det->Data->orderQty);
        $this->smarty->assign('address_non_modified', @$order_det->Data->address_non_modified);
		$this->smarty->assign('order_date_time', @$order_det->Data->order_date);
        $this->smarty->assign('date', @$order_det->Data->date);
        $this->smarty->assign('time', @$order_det->Data->time);
        $this->smarty->assign('billing_type', @$order_det->Data->billing_type);
        $this->smarty->assign('billing_card_no', @$order_det->Data->billing_card_no);
        $this->smarty->assign('address', @$order_det->Data->address);
        $this->smarty->assign('delivery_instructions', @$order_det->Data->delivery_instructions);
        $this->smarty->assign('total', @$order_det->Data->total);
        $this->smarty->assign('sub_total', @$order_det->Data->sub_total);
        $this->smarty->assign('tip', @$order_det->Data->tip);
        $this->smarty->assign('order_item_detail', @$order_det->Data->order_item_detail);
        
        $this->smarty->assign('specific_time', @$order_det->Data->specific_time);
        $this->smarty->assign('discount', @$order_det->Data->discount);
        $this->smarty->assign('orderid', $order_id);
        echo $this->smarty->fetch("order_detail_popup.tpl");
        exit;
        
    }
    function managePickupData()
    {
        $order_id    = $_POST['order_id'];
        $status_code = $_POST['id'];
        
        
        if (isset($_POST['status_code']) == 1) {
            $status = "new_prc";
        }
        
        $data       = array(
            'status_code' => $status_code,
            'order_id' => $order_id
            
        );
        $model      = $this->model;
        $getAddress = $model->update_order_status(API_URL, $data);
        print_r($getAddress);
        exit;
    }
    function searchPickupsDataAjax()
    {
        $d = $_POST['search_date'];
        
        $date_replace = str_replace(',', ' ', $d);
        
        $str_to_time = date('Y-m-d', strtotime($date_replace));
        
        $date = date("d M Y", strtotime($date_replace));
        $this->smarty->assign('date', $date);
        
        $this->title = TITLE . " | Admin-Delivery";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data = array(
            'order_status' => $_POST['delivery_status'],
            'store_id' => @$_POST['store_id'],
            'date' => $str_to_time,
            'status_flag' => @$_POST['delivery_status'],
            'delivery' => '0'
        );
        $this->smarty->assign('data', $data);
        
        $this->title = TITLE . " | Admin-Pickups";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $model = $this->model;
        
        //Pickup stores
        $pickupstores = $model->getPickupStores();
        $this->smarty->assign('pickupstores', $pickupstores);
        
        $getAddress = $model->get_search_delivery_items(API_URL, $data);
        
        $del_status = $_POST['delivery_status'];
        $this->smarty->assign('del_status', $del_status);
        
        foreach ($getAddress as $result) {
            
            if ($result->order_status == 0) {
                $count_new[] = $result->order_status;
            }
            
            if ($result->order_status == 1) {
                $count_prc[] = $result->order_status;
            }
            
            if ($result->order_status == 2) {
                $count_bag[] = $result->order_status;
            }
            
            if ($result->order_status == 3) {
                $count_pick[] = $result->order_status;
            }
            
        }
        
        $orderAdd = 'Order';
        
        $this->smarty->assign('count_new', count(@$count_new));
        $this->smarty->assign('count_prc', count(@$count_prc));
        $this->smarty->assign('count_bag', count(@$count_bag));
        $this->smarty->assign('count_pick', count(@$count_pick));
        
        
        $total_pickUps = count(@$count_new) + count(@$count_prc) + count(@$count_bag) + count(@$count_pick);
        
        $this->smarty->assign('total_pickUps', $total_pickUps);
        
        $data['status'] = '0';
        $getOrderSatus  = $model->get_all_order_status(API_URL, $data);
        $this->smarty->assign('getOrderSatusList', $getOrderSatus);
        
        $this->smarty->assign('order', $orderAdd);
        
        $this->smarty->assign('addressList', $getAddress);
        $this->smarty->assign('content', "pickupsajax.tpl");
        $data                    = $this->smarty->fetch('pickupsajax.tpl');
        $response                = new stdClass;
        $response->html          = $data;
        $response->count_new     = count(@$count_new);
        $response->count_prc     = count(@$count_prc);
        $response->count_bag     = count(@$count_bag);
        $response->count_pick    = count(@$count_pick);
        $response->total_pickUps = $total_pickUps;
        echo json_encode($response);
        exit;
    }
    function searchPickupsData()
    {
        
        $d = $_POST['search_date'];
        
        $date_replace = str_replace(',', ' ', $d);
        
        $str_to_time = date('Y-m-d', strtotime($date_replace));
        
        $date = date("d M Y", strtotime($date_replace));
        $this->smarty->assign('date', $date);
        
        $this->title = TITLE . " | Admin-Delivery";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data = array(
            'order_status' => $_POST['delivery_status'],
            'store_id' => @$_POST['store_id'],
            'date' => $str_to_time,
            'status_flag' => @$_POST['delivery_status'],
            'delivery' => '0'
        );
        $this->smarty->assign('data', $data);
        
        $this->title = TITLE . " | Admin-Pickups";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $model = $this->model;
        
        //Pickup stores
        $pickupstores = $model->getPickupStores();
        $this->smarty->assign('pickupstores', $pickupstores);
        
        $getAddress = $model->get_search_delivery_items(API_URL, $data); 
        //echo "<pre>";print_r($getAddress);exit;
        $del_status = $_POST['delivery_status'];
        $this->smarty->assign('del_status', $del_status);
        
        foreach ($getAddress as $result) {
            
            if ($result->order_status == 0) {
                $count_new[] = $result->order_status;
            }
            
            if ($result->order_status == 1) {
                $count_prc[] = $result->order_status;
            }
            
            if ($result->order_status == 2) {
                $count_bag[] = $result->order_status;
            }
            
            if ($result->order_status == 3) {
                $count_pick[] = $result->order_status;
            }
            
        }
        
        $orderAdd = 'Order';
        
        $this->smarty->assign('count_new', count(@$count_new));
        $this->smarty->assign('count_prc', count(@$count_prc));
        $this->smarty->assign('count_bag', count(@$count_bag));
        $this->smarty->assign('count_pick', count(@$count_pick));
        
        
        $total_pickUps = count(@$count_new) + count(@$count_prc) + count(@$count_bag) + count(@$count_pick);
        
        $this->smarty->assign('total_pickUps', $total_pickUps);
        
        $data['status'] = '0';
        $getOrderSatus  = $model->get_all_order_status(API_URL, $data);
        $this->smarty->assign('getOrderSatusList', $getOrderSatus);
        
        $this->smarty->assign('order', $orderAdd);
        $this->smarty->assign('addressList', $getAddress);
        $this->smarty->assign('content', "pickups.tpl");
        
    }
    
}