<?php

class Discounts extends Base
{
    public $model;
    
    public function __construct($config = array())
    {
        $this->model = $this->load_model('discountsModel');
        $this->check_isvalidated();
        
    }
    private function check_isvalidated()
    {
        
        if (!$_SESSION['admin_uid']) {
            header("Location:" . SITE_URL . "login/");
        }
        
    }
    
    function index()
    {
        
        if (isset($_SESSION['success']))
            $this->smarty->assign('success', $_SESSION['success']);
        
        $this->title = TITLE . " | Admin-Discounts";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        //getting Discount Data
        $data         = "";
        $model        = $this->model;
        $discountData = $model->get_discount_list(API_URL, $data);
        
        $discountData = json_decode($discountData, true);
        //echo "<pre>";print_r($discountData['Data']);exit;
        $this->smarty->assign('discountList', $discountData['Data']);
        
        $this->smarty->assign('data', "");
        $this->smarty->assign('error', "");
        $this->smarty->assign('content', "discounts.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'discounts');
        
    }
    
    
    function newDiscount()
    {
        
        $this->title = TITLE . " | Admin-New Discount";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $this->smarty->assign('content', "new_discounts.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'discounts');
    }
    
    function addDiscounts()
    {
        $data = $_POST;
        $error = $this->validateData($data);
        
        if ($error != null) {
            $this->title = TITLE . " | Admin-New Discount";
            $this->smarty->assign('SITE_URL', SITE_URL);
            $this->smarty->assign('data', $data);
            $this->smarty->assign('error', $error);
            
            $this->smarty->assign('content', "new_discounts.tpl");
        } else {
            
            //adding Data
            if (!isset($data['minimum_order'])) {
                $data['minimum_order'] = 0;
            }
            if (!isset($data['maximum_order'])) {
                $data['maximum_order'] = 0;
            }
            $model               = $this->model;
            $test                = $model->add_discount(API_URL, $data);
            
            $_SESSION['success'] = "Added Successfully";
            header("Location:" . SITE_URL . "discounts/");
            exit;
        }
    }
    
    function validateData($data)
    {
        $error = array();
        if ($data['name'] == '') {
            $error['name'] = 'Enter Discount Name';
        }
        
        if ($data['code'] == '') {
            $error['code'] = 'Enter Discount Code';
        }
        
        if ($data['discount_amount'] == '') {
            $error['discount_amount'] = 'Enter Discount Amount';
        } else if (!is_numeric($data['discount_amount'])) {
            $error['discount_amount'] = 'Invalid Discount Amount';
        }
        
        
        if ($data['uses_type'] == 0) {
			if ($data['uses_amount'] == '') {
				$error['uses_amount'] = 'Enter usage count';
			} if ($data['uses_amount'] == 0) {
				$error['uses_amount'] = 'Invalid usage count';
			} else if (!is_numeric($data['uses_amount'])) {
				$error['uses_amount'] = 'Invalid usage count';
			}
		}
        
        if (isset($data['minimum_order'])) {
            if ($data['minimum_order_amount'] == '') {
                $error['minimum_order_amount'] = 'Enter Minimum Order Amount';
            } else if (!is_numeric($data['minimum_order_amount'])) {
                $error['minimum_order_amount'] = 'Invalid Minimum order Amount';
            }
        }
        
        if ($data['expiration'] == '') {
            $error['expiration'] = 'Enter Expiration Time';
        } else if (strtotime($data['expiration']) < time()) {
            $error['expiration'] = 'Invalid Expiration Time';
        }
        
        return $error;
    }
    
    function editDiscounts()
    {
        $this->title = TITLE . " | Admin-Custom Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id']   = $this->UrlArray[2];
        $model        = $this->model;
        $catagoryData = $model->get_discount_row(API_URL, $data);
        $catagoryData = json_decode($catagoryData, true);
        
        $date = strtotime($catagoryData['Data'][0]['expiration']);
        $date = date("D, d M Y", $date);
        $this->smarty->assign('date', $date);
        $this->smarty->assign('data', $catagoryData['Data'][0]);
        $this->smarty->assign('content', "edit_discounts.tpl");
        
        
    }
    
    function updateDiscounts()
    {
        $data  = $_POST;
        $error = $this->validateData($data);
        
        if ($error != null) {
            $this->title = TITLE . " | Admin-New Discount";
            $this->smarty->assign('SITE_URL', SITE_URL);
            $this->smarty->assign('data', $data);
            $this->smarty->assign('error', $error);
            
            $this->smarty->assign('content', "edit_discounts.tpl");
        } else {
            
            if (!isset($data['minimum_order']))
                $data['minimum_order'] = 0;
            if (!isset($data['maximum_order']))
                $data['maximum_order'] = 0;
            $model               = $this->model;
            $test                = $model->update_discount(API_URL, $data);
            $_SESSION['success'] = "Updated Successfully";
            header("Location:" . SITE_URL . "discounts/");
            exit;
        }
    }
    
    function deleteItems()
    {
        $this->title = TITLE . " | Admin-Custom Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id']          = $this->UrlArray[2];
        $model               = $this->model;
        $catagoryData        = $model->delete_items(API_URL, $data);
        $catagoryData        = json_decode($catagoryData, true);
        $_SESSION['success'] = "Deleted Successfully";
        header("Location:" . SITE_URL . "discounts/");
        exit;
    }
    
    function updateIsLive()
    {
        $idlist              = implode("---", @$_POST['live']);
        $data                = array(
            'id' => $idlist
        );
        $model               = $this->model;
        $test                = $model->update_is_live(API_URL, $data);
        $_SESSION['success'] = "Updated Successfully";
        header("Location:" . SITE_URL . "discounts/");
        exit;
        
    }
}