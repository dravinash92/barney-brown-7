<?php
ini_set('display_errors',0);
class NewOrder extends Base
{
    public $model;
    
    public function __construct()
    {
        $this->model = $this->load_model('newOrderModel');
    }
    
    function index()
    {
        
        $this->title = TITLE . " | Admin-New Order";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $name = $email = $lastname = "";
        if (isset($_GET['name']))
            $name = $_GET['name'];
        if (isset($_GET['email']))
            $email = $_GET['email'];
        if (isset($_GET['lastname']))
            $lastname = $_GET['lastname'];
        
        $this->smarty->assign('name', $name);
        $this->smarty->assign('email', $email);
        $this->smarty->assign('lastname', $lastname);
        
        if ($name !== "" or $email != "" or $lastname != "") {
            $response_users = $this->model->get_customer_data($_GET);
            
            $users_data = $this->model->processCustomerdata($response_users->Data);
        } else {
            $users_data = "";
            
        }
        $this->smarty->assign('users_data', $users_data);
        
        $this->smarty->assign('content', "start_order.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'neworder/',
                SITE_URL . 'neworder/orderSearch/'
            ),
            'text' => array(
                'NEW ORDER',
                'ORDER SEARCH'
            ),
            'active_text' => 'NEW ORDER'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'neworder');
        
    }
    
    function addToCart()
    {
        
        $user_id = $this->UrlArray[2];
        if (!intval($user_id)) {
            echo "<script>window.location='" . SITE_URL . "neworder';</script>";
            exit;
        }
        
        $user      = $this->model->getUserDetails($user_id);
        $orders    = $this->model->getUserOrdersInfo($user_id);
        $addresses = $this->model->getUserAddresses($user_id);
        $gallery   = $this->model->gallery_load_customer_data($user_id);
        
        $gallery   = $this->model->process_user_data($gallery);
        
        if ($orders) 
        {
            $user->total_orders = $orders->orders;
            $user->total_sum    = $orders->sales_sum;
        } 
        else 
        {
            $user->total_orders = 0;
            $user->total_sum    = 0;
        }
        
        $user->addresses = $addresses;
        $this->smarty->assign('GALLARY_DATA', $gallery);

        if (!isset($_SESSION['temp_sandwiches'])) { //in createsandwich 
            $this->smarty->assign('temp_sandwiches', array());
        } else {
            $this->smarty->assign('temp_sandwiches', $_SESSION['temp_sandwiches']);
        }
        
        $stdCategoryItems = $this->model->getStandardCategories();
        foreach ($stdCategoryItems as $row)
        {
            $row->categoryProducts = $this->model->getStandardCategoryProducts($row->id);
        }
        //echo '<pre>';print_r($stdCategoryItems);exit;

        $saladsResult = array();

        foreach ($stdCategoryItems as $row)
        {
            if ($row->category_identifier == "SALADS") 
            {
                $saladsResult = $this->model->getStandardCategoryProducts($row->id);
            }
        }
        
        $salad_data = json_decode(json_encode($saladsResult), true);
        $sal = array();
        $modifier_options = array();
        foreach ($salad_data as $value)
        {
            $sal[] = $value;
        }
        foreach($sal as $key=>$val)
        {
            $modifier_options[]=json_decode($val['modifier_options'],true);
        }
        //echo '<pre>';print_r($modifier_options);exit;
        $this->smarty->assign('stdCategoryItems', $stdCategoryItems);
        $this->smarty->assign('modifier_options',$modifier_options);
        $data["user_id"] = $user->uid;
        $this->smarty->assign('uid', $user->uid);
        //get Customer notes
        $notes           = $this->getCustomerNotes($data);
        $this->smarty->assign('notes', $notes);
        
        //Get all cart items related to user
        $shopitems = $this->model->cartItems($data);
        
        //Optimize result and get output of cart
        $shopitems = $this->getCartItemList($shopitems);
        $this->smarty->assign('shopitems', $shopitems);
        
        
        $data             = array();
        $cateringData     = $this->model->get_catering_catgory_items(API_URL, $data);
        //Category Item
        $count            = 0;
        $cateringdataList = Array();
        
        foreach ($cateringData as $key => $catagory) {
            $cateringdataList[$count]['id']       = $catagory->id;
            $cateringdataList[$count]['name']     = $catagory->category_identifier;
            $cateringdataList[$count]['cat_name'] = $catagory->standard_cat_name;
            $cateringdataList[$count]['data']     = $this->standardCatagoryItem($catagory);
            //for adding item	 
            $count++;
        }
        //echo '<pre>';print_r($cateringdataList);exit;
        $this->smarty->assign('cateringdataList', $cateringdataList);
        
        $billing_data = $this->model->get_Billinginfo(array(
            'uid' => $user->uid
        ));
        $this->smarty->assign('billingInfo', $billing_data->Data);
		
		// added for date
            $currenthour = date("10"); //10 AM to 4 PM down
            $currentmin  = date("00");
            if ($currentmin < 15)
                $currentmin = 0;
            else if ($currentmin >= 15 && $currentmin < 30)
                $currentmin = 15;
            else if ($currentmin >= 30 && $currentmin < 45)
                $currentmin = 30;
            else if ($currentmin >= 45 && $currentmin < 60)
                $currentmin = 45;
            $currentdate = $currenthour . ":" . $currentmin;
		
			$from        = date('H:i:s', strtotime($currentdate));
            $todate      = date('H:i:s', strtotime('4pm'));
            
            
            sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
            $lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $lower = $lower + 900;
            sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
            $upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $upper = $upper - 900;
            
            
            $times = "<select name='time' class='select'>";
            if (empty($format)) {
                $format = 'g:i a';
            }
            $step = 900;
            foreach (range($lower, $upper, $step) as $increment) {
                $increment = gmdate('H:i', $increment);
                
                list($hour, $minutes) = explode(':', $increment);
                $date = new DateTime($hour . ':' . $minutes);
                $date = $date->format($format);
                
                $times .= "<option value=" . $hour . ':' . $minutes . ">" . $date . "</option>";
            }
            $times .= "</select>";
            $this->smarty->assign('times', $times);
		
        
        $this->title = TITLE . " | Admin-Place Order";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('CMS_URL', CMS_URL);
        $this->smarty->assign('user', $user);
        $this->smarty->assign('TO_USER_ID', $user_id);
        
        $this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
        $this->smarty->assign('content', "add_to_cart.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'neworder/',
                SITE_URL . 'neworder/orderSearch/'
            ),
            'text' => array(
                'NEW ORDER',
                'ORDER SEARCH'
            ),
            'active_text' => 'NEW ORDER'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'neworder');
        
    }

    function getCurrentprice(){
        $desc = $_POST['pdata']['sandwich_desc'];
        $desc_id = $_POST['pdata']['sandwich_desc_id'];
        $brd = $_POST['pdata']['bread'];
        $pid = $_POST['pdata']['id'];
        $price = $this->model->get_sandwich_current_price($pid, $brd,$desc,$desc_id);

        echo $price;exit;
    }

    function updateToastmenu()
    {
        $model  = $this->model;
        $data   = array('item_id'=>$_POST['item_id'],'toast'=>$_POST['toast']);
        echo $model->updateToastmenu($data);
        
        exit;
    }
    
    
    
    function cartConfirm()
    {
        
        $this->title = TITLE . " | Admin-Place Order Confirm";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('content', "cart_confirm.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'neworder/',
                SITE_URL . 'neworder/orderSearch/'
            ),
            'text' => array(
                'NEW ORDER',
                'ORDER SEARCH'
            ),
            'active_text' => 'NEW ORDER'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'neworder');
        
    }
    
    
    
    function cartModify()
    {
        $this->title = TITLE . " | Admin-New order modify";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('content', "modify_order.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'neworder/',
                SITE_URL . 'neworder/orderSearch/'
            ),
            'text' => array(
                'NEW ORDER',
                'ORDER SEARCH'
            ),
            'active_text' => 'NEW ORDER'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'neworder');
    }
    
    
    function orderSearch()
    {
        $model       = $this->model;
        $this->title = TITLE . " | Admin-Order search";
        $orders      = $model->getOrdersList($_GET);
        $search      = "";
        if (isset($_GET['search']))
            $search = $_GET['search'];
        
        $this->smarty->assign('search', $search);
        $this->smarty->assign('orders', $orders);
        
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('content', "order_search.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'neworder/',
                SITE_URL . 'neworder/orderSearch/'
            ),
            'text' => array(
                'NEW ORDER',
                'ORDER SEARCH'
            ),
            'active_text' => 'ORDER SEARCH'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'neworder');
        
    }
    
    function editAddress()
    {
        
        $result  = $this->model->editAddress($_POST);
        $address = $result[0];
        $this->smarty->assign('SITE_URL', SITE_URL);
        if ($address->address_id) {
            $phone = explode("-", $address->phone);
            $this->smarty->assign('phone1', $phone[0]);
            $this->smarty->assign('phone2', $phone[1]);
            $this->smarty->assign('phone3', $phone[2]);
            $this->smarty->assign('address', $address);
            $output = $this->smarty->fetch("edit-address.tpl");
        } else {
            $output = "No address found";
        }
        echo $output;
        exit;
    }
    
    function savingAddress()
    {
        $result = $this->model->savingUserAddress($_POST);
        echo $result;
        exit;
    }
    
    function addItemToCart()
    {
        //echo "<pre>";print_r($_POST);exit;
        $result = $this->model->addItemToCart($_POST);
        //echo "<pre>";print_r($result);exit;
        $output = $this->getCartItemList($result);
        print_r($output);
        exit;
    }
    
    public function updateItemToCart()
    {
        $result = $this->model->updateItemToCart($_POST);
        $output = $this->getCartItemList($result);
        print_r($output);
        exit;
    }
    
    public function removeCartItem()
    {
        $result = $this->model->removeCartItem($_POST);
        $output = $this->getCartItemList($result);
        print_r($output);
        exit;
    }
    
    public function updateCartCouponDetails()
    {
        
        $coupon          = $this->model->geDiscountDetails($_POST);
        $data['user_id'] = $_POST['user_id'];
        //Get all cart items related to user
        $shopitems       = $this->model->cartItems($data);
        //Optimize result and get output of cart
        $shopitems       = $this->getCartItemList($shopitems, $coupon);
        
        print_r($shopitems);
        exit;
    }
    
    public function updateCartDetails()
    {
        $data['user_id'] = $_POST['user_id'];
        $data['code']    = $_POST['coupon_code'];
       
        $coupon = $this->model->geDiscountDetails($data);
     
        //Get all cart items related to user
        $shopitems = $this->model->cartItems($data);
        //Optimize result and get output of cart
        $shopitems = $this->getCartItemList($shopitems, $coupon, $_POST);
        
        print_r($shopitems);
        exit;
        
    }
    
    
    public function getCartItemList($result, $coupon = "", $params = "")
    {
        
        if (count($result->sandwiches) > 0) {
            $sandwiches = $this->model->processSandwiches($result->sandwiches);
        } else {
            $sandwiches = array();
        }
        
        $total = 0;
        $o     = '';
        $in=0;
        foreach ($sandwiches as $sum) {
            $sandwiches[$in]['data_string']=str_replace(',',', ',$sandwiches[$in]['data_string']);
            $total = $total + ($sum['item_price'] * $sum['item_qty']);
            $o .= $sum['order_item_id'] . ' ';

            $in++;
        }

        foreach ($sandwiches as &$one_sandwich_line) {
            $one_sandwich_line['TEMP_TALLY_FOR_HTML'] = $one_sandwich_line['item_price'] * $one_sandwich_line['item_qty'] ;
        }

   

        $products = $result->products;
        //echo "<pre>";print_r($result);exit;
        foreach ($products as $product) {
            $total += $product->product_price * $product->item_qty;
            if($product->extra_cost)
            {
                $total += $product->extra_cost * $product->item_qty;
                $product->item_price = ($product->product_price + $product->extra_cost) * $product->item_qty;
            }
            
            $o .= $product->order_item_id . ' ';
        }
        
        $o         = trim($o);
        $ordString = str_replace(' ', ':', $o);
        
        $subtotal = $total;
        
        if (isset($params['tips_selected']))
            $tips = $params['tips_selected'];
        else
            $tips = 3;
        
        $coupon_id   = 0;
        $coupon_code = " ";
        $tax = $subtotal * TAX;
        $tax =number_format($tax, 2);
        
        if ($coupon) {
            $coupon_id   = $coupon->id;
            $coupon_code = $coupon->code;
            if ($coupon->applicable_to) { //Reduce from Total 				
                if ($coupon->type) { // In amount					
                    $total_owed = $total - $coupon->discount_amount;
                    $off_type   = "$" . $coupon->discount_amount;
                    $off_value  = number_format((float) $coupon->discount_amount, 2, '.', '');
                } else { // In %					
                    $amount     = $total * ($coupon->discount_amount / 100);
                    $off_type   = $coupon->discount_amount . "%";
                    $off_value  = number_format((float) $amount, 2, '.', '');
                    $total_owed = $total - $amount;
                }
                
                $total      = number_format((float) $total, 2, '.', '') + $tips+$tax; //Tip added
                $total_owed = number_format((float) $total_owed, 2, '.', '') + $tips+$tax; //Tip added
                
            } else { // Reduce from Subtotal
                if ($coupon->type) { // In amount
                    $total_owed = $subtotal - $coupon->discount_amount;
                    $off_type   = "$" . $coupon->discount_amount;
                    $off_value  = number_format((float) $coupon->discount_amount, 2, '.', '');
                } else { // In %
                    $amount     = $subtotal * ($coupon->discount_amount / 100);
                    $off_type   = $coupon->discount_amount . "%";
                    $off_value  = number_format((float) $amount, 2, '.', '');
                    $total_owed = $subtotal - $amount;
                }
                
                $subtotal   = number_format((float) $subtotal, 2, '.', '');
                $total      = number_format((float) $subtotal, 2, '.', '') + $tips+$tax; //Tip added
                $total_owed = number_format((float) $total_owed, 2, '.', '') + $tips+$tax; //Tip added
            }
        } else {
            $off_type   = "0%";
            $off_value  = "0.00";
            $subtotal   = number_format((float) $subtotal, 2, '.', '');
            $total      = $tips + number_format((float) $subtotal, 2, '.', '')+ $tax; //Tip added
            $total_owed = $tips + number_format((float) $subtotal, 2, '.', '')+ $tax; //Tip added
        }
        
        if (isset($params['manual_discount_val']))
            $manual_discount = number_format((float) $params['manual_discount_val'], 2, '.', '');
        else
            $manual_discount = 0;
        
       
        
        $total      = number_format((float) $total, 2, '.', '');
        $total_owed = number_format((float) $total_owed, 2, '.', '');
        
        $grand_total = $total_owed - $manual_discount;
        
        $grand_total = number_format((float) $grand_total, 2, '.', '');
        
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('sandwiches', $sandwiches);
        $this->smarty->assign('products', $products);
        $this->smarty->assign('itemsCount', count($products) + count($sandwiches));
        $this->smarty->assign('order_string', $ordString);
        $this->smarty->assign('total_price', $total);
        $this->smarty->assign('subtotal', $subtotal);
        $this->smarty->assign('grand_total', $grand_total);
        $this->smarty->assign('off_type', $off_type);
        $this->smarty->assign('off_value', $off_value);
        $this->smarty->assign('coupon_id', $coupon_id);
        $this->smarty->assign('coupon_code', $coupon_code);
        $this->smarty->assign('manual_discount', $manual_discount);
        $this->smarty->assign('tips', $tips);
        $this->smarty->assign('tax', $tax);
        $this->smarty->assign('total_owed', $total_owed);
        
        $output = $this->smarty->fetch("add_to_cart_shopping_details.tpl");
        
        return $output;
    }
    
    public function getCustomerNotes($data) //user_id in array
    {
        
        $notes = $this->model->getCustomerNotes($data);
        return $notes;
    }
    
    function getAddressForSelection()
    {
        if ($_POST['address_type'] == "delivery") {
            $addresses = $this->model->getUserAddresses($_POST['user_id']);
        } else if ($_POST['address_type'] == "pickup") {
            $addresses = $this->model->getPickupStores();
        }
        
        $this->smarty->assign('addresses', $addresses);
        
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        if ($_POST['address_type'] == "delivery") {
            $output = $this->smarty->fetch("cart-user-address-load.tpl");
        } else if ($_POST['address_type'] == "pickup") {
            $output = $this->smarty->fetch("cart-pickup-address-load.tpl");
        }
        echo $output;
        exit;
    }
    
    function saveBillinginfo()
    {
        $checkout_items = $this->model->save_billing_info($_POST);
        exit;
    }
    
    function get_billing()
    {
        $id = @$_POST['id'];
        $this->model->get_Billinginfo(array(
            'id' => $id,
            'uid' => $_POST['user_id']
        ), true);

        exit;
    }
    
    function place_order()
    {
        $tax = $_POST['tax'];
        $tax = str_replace( "$","" ,$tax);
        $_POST['tax'] = $tax;
		$json = $this->model->confirm_cart($_POST);
		setcookie('date_val', null, -1, '/neworder/addToCart/');
        setcookie('now_or_specific', null, -1, '/neworder/addToCart/');
        setcookie('time_val', null, -1, '/neworder/addToCart/');
        
        print $json;
        exit;
    }
    
    public function newUserAccount()
    {
        
        $this->title = TITLE . " | Admin-New Order";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('content', "newOrderUserAccount.tpl");
        
    }
    
    function createAccount()
    {
        $model  = $this->model;
        $result = $model->createAccount($_POST);
        
        echo json_encode($result);
        exit;
    }
    
    function standardCatagoryItem($data)
    {
        $model    = $this->model;
        $itemList = $model->get_standard_catgory_items_list(API_URL, (array) $data);
        return $itemList;
    }
    
    
    public function getStoreFomZip()
    {
        $model = $this->model;
        echo json_encode($model->getStoreFomZip());
        exit;
    }
    
    public function getCurrentHour()
    {
    
    	$response                 = array();
    	$response['hour']         = date("H:i");
    	$response['day']          = date("w");
    	$response['date_time']    = date("d-m-y H:i:s");
    	$response['date_time_js'] = date("Y,m,d,H,i,s");
    	$response['timezone']     = date_default_timezone_get();
    	echo json_encode($response);
    	exit;
    
    }
    public function getCurrentStoreTimeSlot()
    {
    	$response = $this->model->getCurrentStoreTimeSlot($_POST);
    	echo json_encode($response);
    	exit;
    }
	
}
