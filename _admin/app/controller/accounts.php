<?php

class Accounts extends Base
{
    public $model;
    
    public function __construct()
    {
        $this->model = $this->load_model('accountsModel');
    }
    
    function index()
    {
        //ini_set("display_errors", 1);        
        $model = $this->model;
        
        $this->title = TITLE . " | Admin-Store Account";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data = '';
        
        $store_accounts = $model->get_store_accounts_data(API_URL, $data);
        //echo "<pre>";print_r($store_accounts);exit;
        $this->smarty->assign('store_accounts', $store_accounts);
        
        
        
        $this->smarty->assign('content', "store_accounts.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'accounts',
                SITE_URL . 'accounts/users',
                SITE_URL . 'accounts/deliveryUsersList',
                SITE_URL . 'accounts/sociallinks'
            ),
            'text' => array(
                'STORE ACCOUNTS',
                'USER ACCOUNTS',
                'DELIVERY ACCOUNTS',
                'SOCIAL LINKS'
            ),
            'active_text' => 'STORE ACCOUNTS'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'admin');
    }
    
    function storeUsernameExists()
    {
        $model = $this->model;
        $data  = $model->storeUsernameExists($_POST);
        
        echo json_encode($data);
        exit;
    }
    
    function cmsUsernameExists()
    {
        $model = $this->model;
        $data  = $model->cmsUsernameExists($_POST);
        
        echo json_encode($data);
        exit;
    }
    
    
    function newStoreUser()
    {
        
        $model        = $this->model;
        $timesections = array();
        if (empty($format)) {
            $format = 'g:i a';
        }
        $step   = 900;
        $from   = date('H:i:s', strtotime('8am'));
        $todate = date('H:i:s', strtotime('10pm'));
        
        
        sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
        $lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
        
        
        sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
        $upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
        
        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i', $increment);
            
            list($hour, $minutes) = explode(':', $increment);
            
            $date = new DateTime($hour . ':' . $minutes);
            $date = $date->format($format);
            
            $timesections[] .= $date;
        }
        
        $this->smarty->assign('timesections', $timesections);
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->title = TITLE . " | Admin-New Store Account";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data           = '';
        $store_accounts = $model->get_store_accounts_data(API_URL, $data);
        $this->smarty->assign('store_accounts', $store_accounts);
        
        
        $this->smarty->assign('content', "new_store_account.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'accounts',
                SITE_URL . 'accounts/users',
                SITE_URL . 'accounts/deliveryUsersList',
                SITE_URL . 'accounts/sociallinks'
            ),
            'text' => array(
                'STORE ACCOUNTS',
                'USER ACCOUNTS',
                'DELIVERY ACCOUNTS',
                'SOCIAL LINKS'
            ),
            'active_text' => 'STORE ACCOUNTS'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'admin');
    }
    
    
    function users()
    {
        $model       = $this->model;
        $this->title = TITLE . " | Admin-Users";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data = '';
        
        $admin_users = $model->get_admin_user_data(API_URL, $data);
        
        $this->smarty->assign('admin_users', $admin_users);
        
        $this->smarty->assign('content', "users_list.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'accounts',
                SITE_URL . 'accounts/users',
                SITE_URL . 'accounts/deliveryUsersList',
                SITE_URL . 'accounts/sociallinks'
            ),
            'text' => array(
                'STORE ACCOUNTS',
                'USER ACCOUNTS',
                'DELIVERY ACCOUNTS',
                'SOCIAL LINKS'
            ),
            'active_text' => 'USER ACCOUNTS'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'admin');
        
    }
    
    
    function newUser()
    {
        $model = $this->model;
        
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->title = TITLE . "| Admin-New User Account";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data = '';
        
        $admin_user_category = $model->get_admin_user_category(API_URL, $data);
        $this->smarty->assign('admin_user_category', $admin_user_category);
        
        $this->smarty->assign('content', "new_user.tpl");
        
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'accounts',
                SITE_URL . 'accounts/users',
                SITE_URL . 'accounts/deliveryUsersList',
                SITE_URL . 'accounts/sociallinks'
            ),
            'text' => array(
                'STORE ACCOUNTS',
                'USER ACCOUNTS',
                'DELIVERY ACCOUNTS',
                'SOCIAL LINKS'
            ),
            'active_text' => 'USER ACCOUNTS'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'admin');
        
    }
    
    public function sociallinks()
    {
        $model = $this->model;
        
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->title = TITLE . " | Admin-New User Account";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $social = $model->getSocialLinks(array());
        $this->smarty->assign('social', $social);
        
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'accounts',
                SITE_URL . 'accounts/users',
                SITE_URL . 'accounts/deliveryUsersList',
                SITE_URL . 'accounts/sociallinks'
            ),
            'text' => array(
                'STORE ACCOUNTS',
                'USER ACCOUNTS',
                'DELIVERY ACCOUNTS',
                'SOCIAL LINKS'
            ),
            'active_text' => 'USER ACCOUNTS'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'admin');
        $this->smarty->assign('content', "social_links.tpl");
    }
    
    public function saveSocialLinks()
    {
        $model = $this->model;
        $model->saveSocialLinks($_POST);
        header("Location:" . SITE_URL . "accounts/sociallinks");
    }
    
    function addNewUser()
    {
        
        $model = $this->model;
        
        $data = array(
            'name' => $_POST['admin_user_name'],
            'username' => $_POST['admin_user_uname'],
            'password' => $_POST['admin_user_pwd'],
            'admin_cat_id' => $_POST['access_level']
        );
        
        $admin_user_category = $model->add_new_admin_user(API_URL, $data);
        header("Location:" . SITE_URL . "accounts/users");
    }
    
    
    function editAdminUser()
    {
        
        $model       = $this->model;
        $this->title = TITLE . " | Edit Admin-User Account";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id'] = $this->UrlArray[2];
        
        $adminData = $model->get_admin_user(API_URL, $data);
        
        
        $admin_user_category = $model->get_admin_user_category(API_URL, $data);
        $this->smarty->assign('admin_user_category', $admin_user_category);
        
        $this->smarty->assign('adminData', $adminData);
        $this->smarty->assign('content', "edit_admin_user.tpl");
        
    }
    
    function updateAdminUser()
    {
        
        if (isset($_POST['admin_user_pwd']) && $_POST['admin_user_pwd'] !== $_POST['admin_user_cpwd']) 
        {
            echo "<script> alert('Passwords do not match');
            window.location.href = '". SITE_URL ."accounts/editAdminUser/".$_POST['hidden_uid']."';
            </script>";
            //header("Location:" . SITE_URL . "accounts/editAdminUser/".$_POST['hidden_uid']);
        }
        else
        {
            $model = $this->model;
            $data  = array(
                'name' => $_POST['admin_user_name'],
                'admin_cat_id' => $_POST['access_level'],
                'uid' => $_POST['hidden_uid'],
                'password' => $_POST['admin_user_pwd']
            );
            
            $admin_user_category = $model->update_admin_user(API_URL, $data);
            header("Location:" . SITE_URL . "accounts/users");
            
        }
    }
    
    
    function deleteAdminUser()
    {
        
        $model               = $this->model;
        $data['id']          = $this->UrlArray[2];
        $admin_user_category = $model->delete_admin_user(API_URL, $data);
        header("Location:" . SITE_URL . "accounts/users");
        
    }
    
    function addNewStoreAccount()
    {
        
        $model               = $this->model;
        $data                = array();
        $admin_user_category = $model->add_new_store_account(API_URL, $_POST);
        header("Location:" . SITE_URL . "accounts");
        
    }
    
    
    function editStoreAccount()
    {
        
        //echo "<pre>";print_r($_SESSION);exit;
        $model       = $this->model;
        $this->title = TITLE . " | Edit Admin-Store Account";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id'] = $this->UrlArray[2];
        
        $storeData       = $model->get_store_account(API_URL, $data);
        //echo "<pre>";print_r($storeData);exit;
        $store_timing_id = array();
        $days            = array();
        $open            = array();
        $close           = array();
        $store_active    = array();
        
        
        
        foreach ($storeData->store_timings as $c) {
            $store_timing_id[] = $c->id;
            $days[]            = $c->day;
            $open[]            = $c->open;
            $close[]           = $c->close;
            $delivery[]        = $c->delivery_active;
            $pickup[]          = $c->pickup_active;
            $store_active[]	   = $c->store_active;
        }
        $timesections = array();
        if (empty($format)) {
            $format = 'g:i a';
        }
        $step   = 900;
        $from   = date('H:i:s', strtotime('8am'));
        $todate = date('H:i:s', strtotime('10pm'));
        
        
        sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
        $lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
        
        
        sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
        $upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
        
        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i', $increment);
            
            list($hour, $minutes) = explode(':', $increment);
            $date = new DateTime($hour . ':' . $minutes);
            $date = $date->format($format);
            $timesections[] .= $date;
        }
        
        $this->smarty->assign('timesections', $timesections);
        $this->smarty->assign('store_timings_id', $store_timing_id);
        $this->smarty->assign('store_timings_day', $days);
        $this->smarty->assign('store_timings_open', $open);
        $this->smarty->assign('store_timings_close', $close);
        $this->smarty->assign('delivery_active', $delivery);
        $this->smarty->assign('pickup_active', $pickup);
        $this->smarty->assign('store_active', $store_active);
        $this->smarty->assign('CMS_URL', CMS_URL);
        $this->smarty->assign('TO_USER_ID', '');
        $this->smarty->assign('DATA_ID', '');
        
         $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'accounts',
                SITE_URL . 'accounts/users',
                SITE_URL . 'accounts/deliveryUsersList',
                SITE_URL . 'accounts/sociallinks'
            ),
            'text' => array(
                'STORE ACCOUNTS',
                'USER ACCOUNTS',
                'DELIVERY ACCOUNTS',
                'SOCIAL LINKS'
            ),
            'active_text' => 'USER ACCOUNTS'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', '');
        $this->smarty->assign('isStore', '');
        
        $store_accounts = $model->get_store_accounts_data(API_URL, $data);
        
        $this->smarty->assign('store_accounts', $store_accounts);
        $this->smarty->assign('store_accounts_data', $storeData);
        $this->smarty->assign('content', "edit_store_account_data.tpl");
        
    }
    
    
    function updateStoreAccount()
    {
        $model               = $this->model;
        $admin_user_category = $model->update_store_account(API_URL, $_POST);
        exit;
        
    }
    
    function deleteZip()
    {
        echo $this->model->deleteZipcode();
        exit;
    }
    
    function deleteStoreAccount()
    {
        
        $model       = $this->model;
        $data['id']  = $this->UrlArray[2];
        $data['uid'] = $this->UrlArray[3];
        
        $admin_user_category = $model->delete_store_account(API_URL, $data);
        header("Location:" . SITE_URL . "accounts");
    }
    
    function updateStoreOrder()
    {
        $model = $this->model;
        $model->updateStoreOrder($_POST);
        header("Location:" . SITE_URL . "accounts");
        exit;
    }
    
    function updateUsersLive()
    {
        $model = $this->model;
        $model->updateUsersLive($_POST);
        header("Location:" . SITE_URL . "accounts/users");
        exit;
    }

    function updateDeliveryUsersLive()
    {
        $model = $this->model;
        $model->updateDeliveryUsersLive($_POST);
        header("Location:" . SITE_URL . "accounts/deliveryUsersList");
        exit;
    }
    
    public function getAllzipcodes()
    {
        $model  = $this->model;
        $result = $model->getAllzipcodes($_POST);
        $result = json_decode($result);
        $arr    = array();
        if (count($result->Data > 0)) {
            
            foreach ($result->Data as $data) {
                $arr[] = $data->zipcode;
            }
            
            $arr = array_unique($arr);
            
        }
        
        echo json_encode($arr);
        
        exit;
    }
    
    /*
    * Delivery Accounts
    */
    function delivery_users()
    {
        $model       = $this->model;
        $this->title = TITLE . " |  ADD Delivery Account";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $pickupstores = $model->getPickupStores();
        $finalArray = array();
        foreach($pickupstores as $pickupstore){
            $finalArray[] = get_object_vars($pickupstore);
        }

        $this->smarty->assign('pickupstores', $finalArray);
        $this->smarty->assign('content', "add_delivery_account.tpl");
    }

    function addDeliveryUser()
    {
        $model       = $this->model;
        $store_id = implode(',', $_POST['store_id']);
        $data  = array(
            'name' => ucwords($_POST['delivery_name']),
            'user_name' =>  $_POST['delivery_user_uname'],
            'password' =>  $_POST['delivery_user_pwd'],
            'store_id' =>  $store_id,
            'live' => 1
        );
        $admin_user_category = $model->add_new_delivery_user(API_URL, $data);
        header("Location:" . SITE_URL . "accounts/deliveryUsersList");
    }

    function deliveryUsersList()
    {
        $model       = $this->model;
        $this->title = TITLE . " | Admin-Users";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data = '';

        $from_date   = date("d M Y", strtotime("-30 days"));
        $to_date   = date("d M Y");
        $this->smarty->assign('from_date', $from_date);
        $this->smarty->assign('to_date', $to_date);
        $from_time ="00:00";
        $to_time ="23:59";

        $from   = date('H:i:s', strtotime('12am'));
        $todate = date('H:i:s', strtotime('11pm'));
        sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
        $lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
         
        
        sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
        $upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
         
        
         
        $fromTimes = "";
        $toTimes   = "";
        if (empty($format)) {
            $format = 'g:i a';
        }
        $step = 3600;
        
        $range = range($lower, $upper, $step);
        
        foreach ($range as $key => $increment) {
            $increment = gmdate('H:i', $increment);
            
            list($hour, $minutes) = explode(':', $increment);
            
            $date = new DateTime($hour . ':' . $minutes);
            $date = $date->format($format);
            
            //Date for to Time
            $date1 = new DateTime($hour . ':' . ($minutes+29));
            $date1 = $date1->format($format);
            
            $fromTimes .= "<option value=" . $hour . ":".$minutes.">" . $date . "</option>";
            
            if (count($range) == $key + 1)
                $selected = " ";
            else
                $selected = "";
            
        }
        
        $step = 3600;
        foreach ($range as $key => $increment) {
            $increment = gmdate('H:i', $increment);
            
            list($hour, $minutes) = explode(':', $increment);
            
            $date = new DateTime($hour . ':' . $minutes);
            $date = $date->format($format);
            
            //Date for to Time
            $date1 = new DateTime($hour . ':' . ($minutes+59));
            $date1 = $date1->format($format);            
            
            $selected = "";
            if($key == count($range)-1) $selected = "selected";
            
            $toTimes .= "<option $selected value=" . $hour . ":".($minutes+59)." $selected>" . $date1 . "</option>";
        }

        $this->smarty->assign('toTimes', $toTimes);
        $this->smarty->assign('fromTimes', $fromTimes);
        
        $delivery_users = $model->get_delivery_user_data(API_URL, $data);


        $this->smarty->assign('delivery_users', $delivery_users);
        
        $this->smarty->assign('content', "delivery_users_list.tpl");
        $nav_ops = array(
            'show' => true,
            'url' => array(
                SITE_URL . 'accounts',
                SITE_URL . 'accounts/users',
                SITE_URL . 'accounts/deliveryUsersList',
                SITE_URL . 'accounts/sociallinks'
            ),
            'text' => array(
                'STORE ACCOUNTS',
                'USER ACCOUNTS',
                'DELIVERY ACCOUNTS',
                'SOCIAL LINKS'
            ),
            'active_text' => 'DELIVERY ACCOUNTS'
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'admin');
        
    }

    function editDeliveryUser()
    {
        $model       = $this->model;
        $this->title = TITLE . " | Edit Delivery-User Account";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id'] = $this->UrlArray[2];
        
        $delivery_user = $model->get_delivery_user(API_URL, $data);
        $sid = explode(',',  $delivery_user[0]['store_id']);
        $pickupstores = $model->getPickupStores();
        $finalArray = array();
        foreach($pickupstores as $pickupstore){
            $finalArray[] = get_object_vars($pickupstore);
        }

        $this->smarty->assign('pickupstores', $finalArray);
        $this->smarty->assign('sid_ary', $sid);
       
        $this->smarty->assign('delivery_user', $delivery_user);
        $this->smarty->assign('content', "edit_delivery_user.tpl");
    }

    function updateDeliveryUser()
    {
        $model       = $this->model;
        $store_id = implode(',', $_POST['store_id']);
        $data  = array(
            'uid' => $_POST['hidden_uid'],
            'name' => ucwords($_POST['delivery_name']),
            'user_name' =>  $_POST['delivery_user_uname'],
            'password' =>  $_POST['delivery_user_pwd'],
            'store_id' =>  $store_id
        );
        //echo "<pre>";print_r($data);exit;
        $admin_user_category = $model->update_delivery_user(API_URL, $data);
        header("Location:" . SITE_URL . "accounts/deliveryUsersList");
    }

    function deleteDeliveryUser()
    {
        $model               = $this->model;
        $data['id']          = $this->UrlArray[2];
        $admin_user_category = $model->delete_delivery_user(API_URL, $data);
        header("Location:" . SITE_URL . "accounts/deliveryUsersList");
        
    }
    function deliveriesByDate()
    {
        $model  = $this->model;
        $delivery_users = $model->get_delivery_user_by_date($_POST);
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('delivery_users', $delivery_users);
        $this->smarty->assign('content', "delivery_users_list_ajax.tpl");
        $output = $this->smarty->fetch("delivery_users_list_ajax.tpl");
        print_r($output);exit;

    }
    
}
