<?php

class Menu extends Base
{
    public $model;
    
    public function __construct()
    {
        $this->model = $this->load_model('menuModel');
        $this->model->fill_unique_slugs();
    }
    
    function index()
    {
        
        $model       = $this->model;
        $this->title = TITLE . " | Admin-Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data = "";
        $catagoryData = $model->get_standard_catgory_items(API_URL, $data);
        $cateringData = $model->get_catering_catgory_items(API_URL, $data);
        $catagoryDataArray = json_decode($catagoryData, true);
        $cateringDataArray = json_decode($cateringData, true);
        
        $count    = 0;
        $dataList = Array();
        
        $counts = json_decode($model->get_individual_item_count());
        $counts = get_object_vars($counts->Data);
        
        foreach ($catagoryDataArray['Data'] as $catagory) {
            $dataList[$count]['id']       = $catagory['id'];
            $dataList[$count]['name']     = $catagory['category_identifier'];
            $dataList[$count]['cat_name'] = $catagory['standard_cat_name'];
            $dataList[$count]['data']     = $this->standardCatagoryItem($catagory);
            //for adding item
            $count++;
        }

        $this->smarty->assign('counts', $counts);
        $this->smarty->assign('dataList', $dataList);
        
        
        $count            = 0;
        $cateringdataList = Array();
        
        
        foreach ($cateringDataArray['Data'] as $catagory) {
            $cateringdataList[$count]['id']       = $catagory['id'];
            $cateringdataList[$count]['name']     = $catagory['category_identifier'];
            $cateringdataList[$count]['cat_name'] = $catagory['standard_cat_name'];
            $cateringdataList[$count]['data']     = $this->standardCatagoryItem($catagory);
            $count++;
        }
        
        
        $this->smarty->assign('cateringdataList', $cateringdataList);
        
        $this->smarty->assign('content', "menu_items.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
        
    }
    
    
    function edit_options()
    {
        
        $model       = $this->model;
        $this->title = TITLE . " | Admin-Edit Options";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $optiondata = $model->edit_options();
        $this->smarty->assign('OPTION_DATA', $optiondata);
        $this->smarty->assign('content', "edit_options.tpl");
        
    }
    
    function createOption()
    {
        
        $model       = $this->model;
        $id          = $this->UrlArray[2];
        $this->title = TITLE . " | Admin-Edit Options";
        $this->smarty->assign('SITE_URL', SITE_URL);
        if ($id) {
            $this->smarty->assign('id', $id);
            $optiondata = $model->edit_options($id);
            $this->smarty->assign('OPTION_DATA', $optiondata);
        }
        $this->smarty->assign('content', "createOption.tpl");
    }
    
    function addoptions()
    {
        $model      = $this->model;
        $optiondata = $model->addoptions($_POST);
        header("Location:" . SITE_URL . "menu/edit_options");
    }
    
    function deleteOption()
    {
        $model      = $this->model;
        $id         = $this->UrlArray[2];
        $optiondata = $model->deleteOption($id);
        header("Location:" . SITE_URL . "menu/edit_options");
        exit;
    }
    
    
    function standardCatagoryItem($data)
    {
        
        $model    = $this->model;
        $itemList = $model->get_standard_catgory_items_list(API_URL, $data);
        $itemList = json_decode($itemList, true);
        
        return $itemList['Data'];
    }
    
    
    function updateoptionstatus()
    {
        $model = $this->model;
        $model->updateoptionstatus();
        header("Location:" . SITE_URL . "menu/edit_options");
        exit;
    }
    
    function standard()
    {
        
        $model       = $this->model;
        $this->title = TITLE . " | Admin-Standard Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('content', "standard_menu_items.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
        
    }
    
    function custom()
    {
        
        $this->title = TITLE . " | Admin-Custom Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data         = "";
        $model        = $this->model;
        $catagoryData = $model->get_custom_catgory_items(API_URL, $data);
        
        $catagoryDataArray = json_decode($catagoryData, true);
        
        $count    = 0;
        $dataList = Array();
        
        foreach ($catagoryDataArray['Data'] as $catagory) {
            $dataList[$count]['id']       = $catagory['id'];
            $dataList[$count]['name']     = $catagory['category_identifier'];
            $dataList[$count]['cat_name'] = $catagory['category_name'];
            $dataList[$count]['data']     = $this->catagoryItem($catagory);
            $count++;
        }
        
        $this->smarty->assign('dataList', $dataList);
        
        $this->smarty->assign('content', "custom_menu_items.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
    }
    
    function catagoryItem($data)
    {
        $model    = $this->model;
        $itemList = $model->get_custom_catgory_items_list(API_URL, $data);
        $itemList = json_decode($itemList, true);
        return $itemList['Data'];
    }
    
    function editItems()
    {

        $this->title = TITLE . " | Admin-Custom Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id']   = $this->UrlArray[2];
        
        $model        = $this->model;
        $catagoryData = $model->get_sandwich_custom_category_items(API_URL, $data);
        
        array_walk($catagoryData, function(&$val, $key)
        {
            $val = get_object_vars($val);
        });

        
        
        $sandwichCatagoryData = $model->get_sandwich_category_items(API_URL, $data);

        $sandwichCatagoryData = json_decode($sandwichCatagoryData); 
        $sandwichCatagoryData = get_object_vars($sandwichCatagoryData->Data[0]);
        
        $this->smarty->assign('sandwichCatagoryData', $catagoryData);
        $this->smarty->assign('data', $sandwichCatagoryData);
       
        if($sandwichCatagoryData['category_id'] == 1)
        {
            $this->smarty->assign('content', "edit_custom_item.tpl");
        }
        else
        {
            if (isset($sandwichCatagoryData['option_images'])) 
            {
                $optionImgs = get_object_vars(json_decode($sandwichCatagoryData['option_images']));
                foreach ($optionImgs as $key => $value) 
                {
                    $optionImgs[$key] = get_object_vars($value);
                } 
                $this->smarty->assign('optionImgs', $optionImgs);
            }
            $this->smarty->assign('content', "edit_custom_item_new.tpl");
        }
        
    }
    
    function updateItems()
    {
        $oldImgs              = array();
        $image_array          = array();
        $unlink          = array();
        $long_img             = "";
        $round_img            = "";
        $trapezoid_img        = "";
        $final_file_long       ="";
        $final_file_round      ="";
        $final_file_trapezoid      ="";
        $target_dir           = "upload/";
        foreach ($_POST['oldOptionImgs'] as $key => $value) 
        {
            $img = explode('_',$key);
            if ($img[2] == 'long') 
            {
                $oldImgs[$value]['lImg']= $key;
            }
            if ($img[2] == 'round') 
            {
                $oldImgs[$value]['rImg']= $key;
            }
            if ($img[2] == 'trapezoid') 
            {
                $oldImgs[$value]['tImg']= $key;
            }
        }

        foreach($_POST['select_sandwich_option'] as $item_id) 
        {
            foreach($_FILES as $key => $value) 
            {
                if ("lImg-".$item_id == $key && $_FILES[$key]['name'])
                {
                    $target_file_long  = $target_dir . basename($_FILES[$key]['name']);
                    $uploadOk          = 1;
                    $imageFileTypelong = pathinfo($target_file_long, PATHINFO_EXTENSION);
                    $long_img   = date("Ymd_his")  ."_long_".$item_id. "." . $imageFileTypelong;
                    
                    if ($_FILES[$key]["size"] > 500000) {
                        echo "Sorry, your file is too large.";
                        $uploadOk = 0;
                    }
                    if ($imageFileTypelong != "png") {
                        echo "Sorry, only PNG files are allowed.";
                        $uploadOk = 0;
                    }
                    if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                    } 
                    else 
                    {
                        if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_dir . $long_img)) {
                            "The file " . basename($_FILES[$key]["name"]) . " has been uploaded.";
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                            exit;
                        }
                    }

                    $image_array[$item_id]['lImg'] = $long_img;
                }
                if ("rImg-".$item_id == $key && $_FILES[$key]['name'])
                {
                    $target_file_round  = $target_dir . basename($_FILES[$key]["name"]);
                    $uploadOk           = 1;
                    $imageFileTyperound = pathinfo($target_file_round, PATHINFO_EXTENSION);
                    $round_img   = date("Ymd_his") ."_round_".$item_id. "." . $imageFileTyperound;
                    
                    if ($_FILES[$key]["size"] > 500000) {
                        echo "Sorry, your file is too large.";
                        $uploadOk = 0;
                    }
                    
                   
                    if ($imageFileTyperound != "png") {
                        echo "Sorry, only PNG files are allowed.";
                    
                        $uploadOk = 0;
                    }
                    
                    if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                        
                    } 
                    else 
                    {
                        if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_dir . $round_img)) 
                        {
                            "The file " . basename($_FILES[$key]["name"]) . " has been uploaded.";
                        } else 
                        {
                            echo "Sorry, there was an error uploading your file.";
                            exit;
                        }
                    }
                    $image_array[$item_id]['rImg'] = $round_img;
                }
                if ("tImg-".$item_id == $key && $_FILES[$key]['name'])
                {
                    $target_file_trapezoid  = $target_dir . basename($_FILES[$key]["name"]);
                    $uploadOk               = 1;
                    $imageFileTypetrapezoid = pathinfo($target_file_trapezoid, PATHINFO_EXTENSION);
                    $trapezoid_img   = date("Ymd_his")  ."_trapezoid_".$item_id. "." . $imageFileTypetrapezoid;
                    
                    if ($_FILES[$key]["size"] > 500000) 
                    {
                        echo "Sorry, your file is too large.";
                        $uploadOk = 0;
                    }
                   
                    if ($imageFileTypetrapezoid != "png") 
                    {
                        echo "Sorry, only PNG files are allowed.";
                    
                        $uploadOk = 0;
                    }
                    
                    if ($uploadOk == 0) 
                    {
                        echo "Sorry, your file was not uploaded.";
                        
                    } 
                    else 
                    {
                        if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_dir . $trapezoid_img)) 
                        {
                            "The file " . basename($_FILES[$key]["name"]) . " has been uploaded.";
                        } 
                        else 
                        {
                            echo "Sorry, there was an error uploading your file.";
                            exit;
                        }
                    }
                    $image_array[$item_id]['tImg'] = $trapezoid_img;
                }
            }
        }

        foreach ($image_array as $bKey => $bValue)
        {
            if(in_array($bKey, $_POST['select_sandwich_option']))
            {
                if(isset($bValue['lImg']))
                {
                    $unlink[] = $oldImgs[$bKey]['lImg'];
                    $oldImgs[$bKey]['lImg'] = $bValue['lImg'];
                } 
                if(isset($bValue['rImg']))
                {
                    $unlink[] = $oldImgs[$bKey]['rImg'];
                    $oldImgs[$bKey]['rImg'] = $bValue['rImg'];
                }
                if(isset($bValue['tImg']))
                {
                    $unlink[] = $oldImgs[$bKey]['tImg'];
                    $oldImgs[$bKey]['tImg'] = $bValue['tImg'];            
                }
            }
        }
        if (isset($unlink)) 
        {
            foreach ($unlink as $key) 
            {
                $filePath = $_SERVER['DOCUMENT_ROOT']. 'upload/' . @$key;
                if (file_exists($filePath)) 
                {
                    unlink($filePath);
                } 
            }
        }
        
        $option_images = json_encode($oldImgs);
        foreach ($oldImgs as $key => $value) 
        {
            if(!in_array($key, $_POST['select_sandwich_option'])) 
            {
                foreach ($value as $img) 
                {
                    $filePath = $_SERVER['DOCUMENT_ROOT']. 'upload/' . @$img;
                    if (file_exists($filePath)) 
                    {
                        unlink($filePath);
                    } 
                }
                unset($oldImgs[$key]);
            }
        }

        if ($_FILES["item_image"]["name"]) {
            $final_file = "";
        } else {
            $final_file = $_POST['old_image'];
        }
        if ($_FILES["item_image_sliced"]["name"]) {
            $final_file_sliced = "";
        } else {
            $final_file_sliced = $_POST['old_image_sliced'];
        }
        
        if (isset($_POST['select_sandwich_option']))
            $data_implode = implode(':', $_POST['select_sandwich_option']);
        else
            $data_implode = "";
        
        $target_dir = "upload/";
        if ($_FILES["item_image"]["name"]) 
        {
            
            $target_dir    = "upload/";
            $target_file   = $target_dir . basename($_FILES["item_image"]["name"]);
            $uploadOk      = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $final_file    = date("Ymd_his") . "." . $imageFileType;
            
            if ($_FILES["item_image"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            
            if ($imageFileType != "png") {
                echo "Sorry, only PNG files are allowed.";
                
                $uploadOk = 0;
            }
            
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                
            } else {
                if (move_uploaded_file($_FILES["item_image"]["tmp_name"], $target_dir . $final_file)) {
                    $filePath = SITE_URL . '/upload/' . @$_POST['old_image'];
                    if (file_exists($filePath))
                        unlink($filePath);
                    "The file " . basename($_FILES["item_image"]["name"]) . " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                    exit;
                }
            }
        }

        if ($_FILES["item_image_sliced"]["name"]) 
        {
            $target_filesliced   = $target_dir . basename($_FILES["item_image_sliced"]["name"]);
            $uploadOk            = 1;
            $imageFileTypesliced = pathinfo($target_filesliced, PATHINFO_EXTENSION);
            $final_file_sliced   = date("Ymd_his") . "sliced." . $imageFileTypesliced;
            
            if ($_FILES["item_image_sliced"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            
            if ($imageFileTypesliced != "png") {
            	echo "Sorry, only PNG files are allowed.";
            
                $uploadOk = 0;
            }
            
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                
            } else {
                if (move_uploaded_file($_FILES["item_image_sliced"]["tmp_name"], $target_dir . $final_file_sliced)) {
                    $filePath = SITE_URL . '/upload/' . @$_POST['old_image_sliced'];
                    if (file_exists($filePath))
                        unlink($filePath);
                    "The file " . basename($_FILES["item_image_sliced"]["name"]) . " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                    exit;
                }
            }
        }

        
        $data                = array(
            'id' => @$_POST['id'],
            'item_name' => @$_POST['item_name'],
            'item_price' => @$_POST['item_price'],
            'category_id' => @$_POST['category_id'],
            'old_price' => @$_POST['old_price'],
            'item_image' => $final_file,
            'item_image_sliced' => $final_file_sliced,
            'image_long' => $final_file_long,
            'image_round' => $final_file_round,
            'image_trapezoid' => $final_file_trapezoid,
            'bread_type' => @$_POST['bread_type'],
            'bread_shape' => @$_POST['bread_shape'],
            'select_sandwich_option' => $data_implode,
            'taxable' => @$_POST['taxable'],
        	'premium' => @$_POST['premium'],
            'base_fare' => BASE_FARE,
            'option_images' =>$option_images
        );
        
        $model               = $this->model;
        $test                = $model->update_sandwich_category(API_URL, $data);
        $_SESSION['success'] = "Updated Successfully";
        header("Location:" . SITE_URL . "menu/custom");
        exit;
        
    }
    
    function deleteItems()
    {
        $this->title = TITLE . " | Admin-Custom Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id']   = $this->UrlArray[2];
        
        $model        = $this->model;
        $catagoryData = $model->delete_sandwich_category_items(API_URL, $data);
        //$catagoryData = $model->delete_category_items($data);

        $catagoryData        = json_decode($catagoryData, true);
        $_SESSION['success'] = "Deleted Successfully";
        header("Location:" . SITE_URL . "menu/custom");
        exit;
    }
    
    function newCheese()
    {
        
        $this->title = TITLE . " | Admin-New Cheese";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data  = "";
        $model = $this->model;
        
        $option_data = $model->get_sandwich_custom_category_items(API_URL, array());
        $option_data = $model->process_data($option_data);
        $this->smarty->assign('option_data', $option_data);
        //$this->smarty->assign('content', "new_cheese.tpl");
        $this->smarty->assign('content', "new_cheese_new.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
    }
    
    
    function newtoppings()
    {
        
        $this->title = TITLE . " | Admin-New Topping";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data  = "";
        $model = $this->model;
        
        $option_data = $model->get_sandwich_custom_category_items(API_URL, array());
        $option_data = $model->process_data($option_data);
        $this->smarty->assign('option_data', $option_data);
        
        //$this->smarty->assign('content', "new_topping.tpl");
        $this->smarty->assign('content', "new_topping_new.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
        
    }
    
    
    function newcondiments()
    {
        
        $this->title = TITLE . " | Admin-New Condiment";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data        = "";
        $model       = $this->model;
        $option_data = $model->get_sandwich_custom_category_items(API_URL, array());
        $option_data = $model->process_data($option_data);
        $this->smarty->assign('option_data', $option_data);
        
        //$this->smarty->assign('content', "new_condiment.tpl");
        $this->smarty->assign('content', "new_condiment_new.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
        
    }
    
    
    function newProtein()
    {
        
        $this->title = TITLE . " | Admin-New Protein";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $data        = "";
        $model       = $this->model;
        $option_data = $model->get_sandwich_custom_category_items(API_URL, array());
        $option_data = $model->process_data($option_data);
        $this->smarty->assign('option_data', $option_data);
        
       //$this->smarty->assign('content', "new_protein.tpl");
         $this->smarty->assign('content', "new_protein_new.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
        
    }
    
    
    function newBread()
    {
        $this->title = TITLE . " | Admin-New Bread";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('content', "new_bread.tpl");
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
        
    }
    
    function addCategoryItems()
    {
        // echo "<pre>";print_r($_POST);
        // echo "<pre>";print_r($_FILES);exit;
        $image_array = array();
        $model = $this->model;
        $target_dir    = "upload/";
        $data_implode         = "";
        $final_file_sliced    = "";
        $final_file_long      = "";
        $final_file_round     = "";
        $final_file_trapezoid = "";
        $final_file           = "";
        $long_img             = "";
        $round_img            = "";
        $trapezoid_img        = "";

        foreach($_POST['select_sandwich_option'] as $item_id) 
        {
            foreach($_FILES as $key => $value) 
            {
                if ("lImg-".$item_id == $key && $_FILES[$key]['name'])
                {
                    $target_file_long  = $target_dir . basename($_FILES[$key]['name']);
                    $uploadOk          = 1;
                    $imageFileTypelong = pathinfo($target_file_long, PATHINFO_EXTENSION);
                    $long_img   = date("Ymd_his")  ."_long_".$item_id. "." . $imageFileTypelong;
                    
                    if ($_FILES[$key]["size"] > 500000) {
                        echo "Sorry, your file is too large.";
                        $uploadOk = 0;
                    }
                    if ($imageFileTypelong != "png") {
                        echo "Sorry, only PNG files are allowed.";
                        $uploadOk = 0;
                    }
                    if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                    } 
                    else 
                    {
                        if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_dir . $long_img)) {
                            "The file " . basename($_FILES[$key]["name"]) . " has been uploaded.";
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                            exit;
                        }
                    }

                    $image_array[$item_id]['lImg'] = $long_img;
                }
                if ("rImg-".$item_id == $key && $_FILES[$key]['name'])
                {
                    $target_file_round  = $target_dir . basename($_FILES[$key]["name"]);
                    $uploadOk           = 1;
                    $imageFileTyperound = pathinfo($target_file_round, PATHINFO_EXTENSION);
                    $round_img   = date("Ymd_his") ."_round_".$item_id. "." . $imageFileTyperound;
                    
                    if ($_FILES[$key]["size"] > 500000) {
                        echo "Sorry, your file is too large.";
                        $uploadOk = 0;
                    }
                    
                   
                    if ($imageFileTyperound != "png") {
                        echo "Sorry, only PNG files are allowed.";
                    
                        $uploadOk = 0;
                    }
                    
                    if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                        
                    } 
                    else 
                    {
                        if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_dir . $round_img)) 
                        {
                            "The file " . basename($_FILES[$key]["name"]) . " has been uploaded.";
                        } else 
                        {
                            echo "Sorry, there was an error uploading your file.";
                            exit;
                        }
                    }
                    $image_array[$item_id]['rImg'] = $round_img;
                }
                if ("tImg-".$item_id == $key && $_FILES[$key]['name'])
                {
                    $target_file_trapezoid  = $target_dir . basename($_FILES[$key]["name"]);
                    $uploadOk               = 1;
                    $imageFileTypetrapezoid = pathinfo($target_file_trapezoid, PATHINFO_EXTENSION);
                    $trapezoid_img   = date("Ymd_his")  ."_trapezoid_".$item_id. "." . $imageFileTypetrapezoid;
                    
                    if ($_FILES[$key]["size"] > 500000) 
                    {
                        echo "Sorry, your file is too large.";
                        $uploadOk = 0;
                    }
                   
                    if ($imageFileTypetrapezoid != "png") 
                    {
                        echo "Sorry, only PNG files are allowed.";
                    
                        $uploadOk = 0;
                    }
                    
                    if ($uploadOk == 0) 
                    {
                        echo "Sorry, your file was not uploaded.";
                        
                    } 
                    else 
                    {
                        if (move_uploaded_file($_FILES[$key]["tmp_name"], $target_dir . $trapezoid_img)) 
                        {
                            "The file " . basename($_FILES[$key]["name"]) . " has been uploaded.";
                        } 
                        else 
                        {
                            echo "Sorry, there was an error uploading your file.";
                            exit;
                        }
                    }
                    $image_array[$item_id]['tImg'] = $trapezoid_img;
                }
            }
        }

        $option_images = json_encode($image_array);
        if (isset($_POST['select_sandwich_option'])) 
        {
            $data_implode = implode(':', $_POST['select_sandwich_option']);
        }
        
        if (isset($_FILES["item_image"]["size"])) 
        {
            $target_file   = $target_dir . basename($_FILES["item_image"]["name"]);
            $uploadOk      = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $final_file    = date("Ymd_his") . "." . $imageFileType;
            
            if ($_FILES["item_image"]["size"] > 500000) 
            {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            
            //if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
              //  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            if ($imageFileType != "png") 
            {
                echo "Sorry, only PNG files are allowed.";
                $uploadOk = 0;
            }
            
            if ($uploadOk == 0) 
            {
                echo "Sorry, your file was not uploaded.";  
            }
            else 
            {
                if (move_uploaded_file($_FILES["item_image"]["tmp_name"], $target_dir . $final_file)) 
                {
                    "The file " . basename($_FILES["item_image"]["name"]) . " has been uploaded.";
                } 
                else 
                {
                    echo "Sorry, there was an error uploading your file.";
                    exit;
                }
            }
        }
       

        if (isset($_FILES["item_image_sliced"]["name"])) 
        {
            $target_file_sliced  = $target_dir . basename($_FILES["item_image_sliced"]["name"]);
            $uploadOk            = 1;
            $imageFileTypesliced = pathinfo($target_file_sliced, PATHINFO_EXTENSION);
            $final_file_sliced   = date("Ymd_his") . "sliced." . $imageFileTypesliced;
            
            if ($_FILES["item_image_sliced"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            
           
            if ($imageFileTypesliced != "png") {
            	echo "Sorry, only PNG files are allowed.";
            
                $uploadOk = 0;
            }
 
            
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                
            } else {
                if (move_uploaded_file($_FILES["item_image_sliced"]["tmp_name"], $target_dir . $final_file_sliced)) {
                    "The file " . basename($_FILES["item_image_sliced"]["name"]) . " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                    exit;
                }
            }
        }

        $data = array(
            'item_name' => @$_POST['item_name'],
            'item_price' => @$_POST['item_price'],
            'category_id' => @$_POST['category_id'],
            'bread_type' => @$_POST['bread_type'],
            'bread_type' => @$_POST['bread_shape'],
            'item_image' => $final_file,
            'item_image_sliced' => $final_file_sliced,
            'image_long' => $final_file_long,
            'image_round' => $final_file_round,
            'image_trapezoid' => $final_file_trapezoid,
            'select_sandwich_option' => $data_implode,
            'taxable' => @$_POST['taxable'],
            'premium' => @$_POST['premium'],
        	'option_images' =>$option_images
        );

        $model->add_catgory_items(API_URL, $data);
        $_SESSION['success'] = "Added Successfully";
        header("Location:" . SITE_URL . "menu/custom");
        exit;
    }
    
    
    function newStandardCategory()
    {
        $this->title = TITLE . " | Admin-New standard Category";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('content', "new_standard_cat.tpl");
        $nav_ops = array(
            'show' => false
        );
        
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'menu');
        // echo "klfdgjkdfngsdfsdfsdfsdf";die;

    }
    
    function addStandardCategoryItems()
    {
        
        $model = $this->model;
        $data  = array(
            'standard_cat_name' => @$_POST['category_name'],
            'type' => @$_POST['type']
        );
        
        $model->insert(API_URL, $data);
        $_SESSION['success'] = "Added Successfully";
        header("Location:" . SITE_URL . "menu");
        exit;
    }
    
    function newStandardProducts()
    {
        $this->title = TITLE . " | Admin-Standard Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $id = $this->UrlArray[2];
        $this->smarty->assign('id', $id);
        $this->smarty->assign('content', "standard_menu_items.tpl");
        
    }
    
    function addStandardCategoryProducts()
    {
        $productImage = "";
        $final_file_sliced    = "";
        if ($_FILES["product_image"]["name"]) {
            $target_dir    = "upload/products/";

            $target_filesliced   = $target_dir . basename($_FILES["product_image"]["name"]);
            $uploadOk            = 1;
            $productImage = pathinfo($target_filesliced, PATHINFO_EXTENSION);
            $final_file_sliced   = date("Ymd_his") . "." . $productImage;
            
            if ($_FILES["product_image"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                
            } else {
                if (move_uploaded_file($_FILES["product_image"]["tmp_name"], $target_dir . $final_file_sliced)) {
                } else {
                    echo "Sorry, there was an error uploading your file.";
                    exit;
                }
            }
        }

        $model = $this->model;
        if(!empty($_POST['option'])){
            $re=array();$result=array();
            $opt = $_POST['option'];
            $prc = $_POST['price'];
            $tot=count($_POST['option']);
            for($i=0;$i<$tot;$i++){
                $re['option']=$opt[$i];
                $re['price']=$prc[$i];
                $result[]=$re;
            }
        }
        $extra_names = isset($_POST['extra_name']) ? implode(',', $_POST['extra_name']) : '';
        $extra_cost  = isset($_POST['extra_cost']) ? implode(',', $_POST['extra_cost']) : '';
        $extra_id    = isset($_POST['extra_id']) ?  implode(',', $_POST['extra_id']) : '';
        $descs       = isset($_POST['descriptor']) ?  implode(',', $_POST['descriptor']) : '';
        $desc_ids    = isset($_POST['descriptor_id']) ?  implode(',', $_POST['descriptor_id']) : '';
        
        $data = array(
            'product_name' => @$_POST['product_name'],
            'description' => @$_POST['description'],
            'product_image' => $final_file_sliced,
            'product_price' => @$_POST['product_price'],
            'standard_category_id' => $_POST['hidden_id'],
            'taxable' => @$_POST['taxable'],
            'allow_spcl_instruction' => @$_POST['allow_spcl_instruction'],
            'add_modifier' => @$_POST['add_modifier'],
            'modifier_desc' => @$_POST['modifier_desc'],
            'modifier_isoptional' => @$_POST['modifier_isoptional'],
            'modifier_is_single' => @$_POST['modifier_is_single'],
            'modifier_options' => !empty($result) ? json_encode($result) : '',
            'is_extra' => @$_POST['is_extra'],
            'descriptor' => $descs,
            'descriptor_ids' => $desc_ids,
            'extra_name' => $extra_names,
            'extra_cost' => $extra_cost,
            'extra_id' => $extra_id,
            'extra_default' => @$_POST['extra_default']
        );

        $model->insert_standard_products(API_URL, $data);
         
        $this->index();
    }
    
    function editStandardProducts()
    {
        $this->title = TITLE . " | Admin-Custom Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id'] = $this->UrlArray[2];
        
        $model        = $this->model;
        $catagoryData = $model->get_standard_product_items(API_URL, $data);
        $extra_data   = $model->get_standard_product_extraData(API_URL, array(
            'pid' => $data['id']
        ));
        $catagoryData = json_decode($catagoryData, true);
        $modifier_options = json_decode($catagoryData['Data'][0]['modifier_options'],true);
        $this->smarty->assign('extras_data', $extra_data);
        $this->smarty->assign('modifier_options', $modifier_options);
        $this->smarty->assign('data', $catagoryData['Data'][0]);
        $this->smarty->assign('content', "edit_standard_product.tpl");
    }
    
    function updateStandardCategoryProducts()
    {
        
        $final_file_sliced    = "";
        if ($_FILES["product_image"]["name"]) {

            $target_dir    = "upload/products/";

            $target_filesliced   = $target_dir . basename($_FILES["product_image"]["name"]);
            $uploadOk            = 1;
            $productImage = pathinfo($target_filesliced, PATHINFO_EXTENSION);
            $final_file_sliced   = date("Ymd_his") . "." . $productImage;
            
            if ($_FILES["product_image"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                
            } else {
                if (move_uploaded_file($_FILES["product_image"]["tmp_name"], $target_dir . $final_file_sliced)) {
                    $filePath = SITE_URL . '/upload/' . $_POST['old_product_image'];
                    if (file_exists($filePath))
                        unlink($filePath);
                    "The file " . basename($_FILES["product_image"]["name"]) . " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                    exit;
                }
            }
        }else{
            $final_file_sliced = $_POST['old_product_image'];
        }
        if(!empty($_POST['option'])){
            $re=array();$result=array();
            $opt = $_POST['option'];
            $prc = $_POST['price'];
            $tot=count($_POST['option']);
            for($i=0;$i<$tot;$i++){
                $re['option']=$opt[$i];
                $re['price']=$prc[$i];
                $result[]=$re;
            }
        }
        
        $extra_names = implode(',', $_POST['extra_name']);
        $extra_cost  = implode(',', $_POST['extra_cost']);
        $extra_id    = implode(',', $_POST['extra_id']);
        $descs       = implode(',', $_POST['descriptor']);
        $desc_ids    = implode(',', $_POST['descriptor_id']);
        $data        = array(
            
            'id' => @$_POST['hidden_id'],
            'description' => @$_POST['description'],
            'product_name' => @$_POST['product_name'],
            'product_image' => $final_file_sliced,
            'product_price' => @$_POST['product_price'],
            'taxable' => @$_POST['taxable'],
            'allow_spcl_instruction' => @$_POST['allow_spcl_instruction'],
            'add_modifier' => @$_POST['add_modifier'],
            'modifier_desc' => @$_POST['modifier_desc'],
            'modifier_isoptional' => @$_POST['modifier_isoptional'],
            'modifier_is_single' => @$_POST['modifier_is_single'],
            'modifier_options' => !empty($result) ? json_encode($result) : '',
            'is_extra' => @$_POST['is_extra'],
            'descriptor' => $descs,
            'descriptor_ids' => $desc_ids,
            'extra_name' => $extra_names,
            'extra_cost' => $extra_cost,
            'extra_id' => $extra_id,
            'extra_default' => @$_POST['extra_default']
        );
                
        $model = $this->model;
        $test  = $model->update_standard_category_products(API_URL, $data);
        
        $_SESSION['success'] = "Updated Successfully";
        header("Location:" . SITE_URL . "menu");
    }
    
    function deleteStandardCategoryProducts()
    {
        $this->title = TITLE . " | Admin-Custom Menu items";
        $this->smarty->assign('SITE_URL', SITE_URL);
        $data['id'] = $this->UrlArray[2];
        
        $model        = $this->model;
        $catagoryData = $model->delete_standard_category_products(API_URL, $data);
        
        $catagoryData        = json_decode($catagoryData, true);
        $_SESSION['success'] = "Deleted Successfully";
        header("Location:" . SITE_URL . "menu");
        exit;
    }
    
    function updateStandardCategoryPriority()
    {
        $livecheckbox = array();
        foreach ($_POST['hidden_standard_ids'] as $ids) {
            
            if (isset($_POST['livecheckbox' . $ids])) {
                if ($_POST['livecheckbox' . $ids] == "on") {
                    $livecheckbox[$ids] = 1;
                } else {
                    
                    $livecheckbox[$ids] = 0;
                }
                
            } else {
                
                $livecheckbox[$ids] = 0;
            }
        }
         
        $a    = implode(',', $_POST['hidden_standard_ids']);
        $b    = implode(',', $_POST['priority']);
        $c    = implode(',', $livecheckbox);
        $data = array(
            'id' => $a,
            'priority' => $b,
            'livecheckbox' => $c
        );
        
        $model               = $this->model;
        $test                = $model->update_standard_category_priority(API_URL, $data);
        $_SESSION['success'] = "Updated Successfully";
        header("Location:" . SITE_URL . "menu/");
        
    }
    
    function updateSandwichCategoryItems()
    {
        // echo "<pre>";print_r($_POST);exit;
        $customlivecheckbox = array();
        foreach ($_POST['sandwich_category_items'] as $ids) {
            
            if (isset($_POST['customlivecheckbox' . $ids])) {
                if ($_POST['customlivecheckbox' . $ids] == "on") {
                    $customlivecheckbox[$ids] = 1;
                } else {
                    
                    $customlivecheckbox[$ids] = 0;
                }
                
            } else {
                
                $customlivecheckbox[$ids] = 0;
            }
        }
        $a                   = implode(',', $_POST['sandwich_category_items']);
        $b                   = implode(',', $_POST['sandwich_category_items_order']);
        $c                   = implode(',', $customlivecheckbox);
        $d                   = implode(',', $_POST['sandwich_category_image_priority']);
        $e                   = implode(',', $_POST['sandwich_category_sheet_priority']);
        $data                = array(
            'id' => $a,
            'priority' => $b,
            'image_priority' => $d,
            'sheet_priority' => $e,
            'livecheckbox' => $c
        );
        //echo "<pre>";print_r($data);exit;
        $model               = $this->model;
        $test                = $model->updateStandardCategoryPriority(API_URL, $data);
        $_SESSION['success'] = "Updated Successfully";
        header("Location:" . SITE_URL . "menu/custom");
        
    }
    function deleteStandardCategory()
    {
        
        $data['categoryId'] = $_GET['id'];
        
        $model        = $this->model;
        $catagoryData = $model->deleteStandardCategory(API_URL, $data);
        
        $catagoryData        = json_decode($catagoryData, true);
        $_SESSION['success'] = "Deleted Successfully";
        header("Location:" . SITE_URL . "menu");
        exit;
    }
    
}