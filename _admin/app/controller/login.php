<?php

class login extends Base
{
    function index()
    {
        
        $this->title = TITLE . " - Admin";
        $this->smarty->assign('SITE_URL', SITE_URL);
        if (isset($_COOKIE['remember_me'])) {
            
            $cookiearray = json_decode($_COOKIE['remember_me']);
            $email       = $this->clean_for_sql($cookiearray[0]);
            $password    = $this->clean_for_sql($cookiearray[1]);
            $query       = "select *from admin_user where user_name='" . $email . "' AND password='" . $password . "'";
            
            $result = $this->db->Query($query);
            
            $count = $this->db->Rows($result);
            if ($count == 1) {
                $userData = "SELECT * FROM admin_user WHERE user_name='" . $email . "'";
                $result   = $this->db->query($userData);
                $data     = $this->db->FetchArray($result);
                
                
                $_SESSION['admin_uid'] = $data['uid'];
                $_SESSION['uname']     = $data['first_name'];
                header("Location:" . SITE_URL . "neworder/");
            }
        }
         
        $this->smarty->assign('content', "login.tpl");
         
    }
    
    
    function loginCheck()
    {
         
        $email    = $this->db->clean_for_sql($_POST['email']);
        $password = md5($this->db->clean_for_sql($_POST['password']));
        
        //Check if User is admin
        $query      = "select *from admin_user where user_name='" . $email . "' AND password='" . $password . "'";
        $result     = $this->db->Query($query);
        $adminCount = $this->db->Rows($result);
        
        //check if user is Store 
        $query      = "select *from pickup_stores where username='" . $email . "' AND password='" . $password . "'";
        $result     = $this->db->Query($query);
        $storeCount = $this->db->Rows($result);
        
        if ($adminCount == 1) {
            $userData    = "SELECT * FROM admin_user WHERE user_name='" . $email . "'";
            $result      = $this->db->query($userData);
            $data        = $this->db->FetchArray($result);
            $expiretime  = time() + 60 * 60 * 24 * 60;
            $cookiearray = array(
                $data['user_name'],
                $data['password']
            );
            $cookiearray = json_encode($cookiearray);
            if ($_POST['rememberme']) {
                setcookie('remember_me', $cookiearray, $expiretime, '/', NULL);
            } elseif (!$_POST['rememberme']) {
                if (isset($_COOKIE['remember_me'])) {
                    $past = time() - 100;
                    setcookie(remember_me, gone, $past);
                    unset($_COOKIE['remember_me']);
                }
            }
            
            $_SESSION['admin_uid'] = $data['uid'];
            $_SESSION['uname']     = $data['first_name'];
            $_SESSION['user_type'] = 'admin';
            header("Location:" . SITE_URL . "neworder/");
            
        } else if ($storeCount == 1) {
            
            $userData    = "SELECT * FROM pickup_stores WHERE username='" . $email . "'";
            $result      = $this->db->query($userData);
            $data        = $this->db->FetchArray($result);
            $expiretime  = time() + 60 * 60 * 24 * 60;
            $cookiearray = array(
                $data['user_name'],
                $data['password']
            );
            $cookiearray = json_encode($cookiearray);
            if ($_POST['rememberme']) {
                setcookie('remember_me', $cookiearray, $expiretime, '/', NULL);
            } elseif (!$_POST['rememberme']) {
                if (isset($_COOKIE['remember_me'])) {
                    $past = time() - 100;
                    setcookie(remember_me, gone, $past);
                    unset($_COOKIE['remember_me']);
                }
            }
            
            $_SESSION['admin_uid'] = $data['id'];
            $_SESSION['uname']     = $data['store_name'];
            $_SESSION['user_type'] = 'store';
            header("Location:" . SITE_URL . "deliveries/");
            
        } else {
            $_SESSION['error'] = "Invalid Username or Password";
            header("Location:" . SITE_URL . "login/");
        }
        exit;
    }
    
    function signout()
    {
        session_destroy();
        
        if (isset($_COOKIE['remember_me'])) {
            $past = time() - 100;
            setcookie(remember_me, "", $past, '/', NULL);
            unset($_COOKIE['remember_me']);
        }
        
        header("Location:" . SITE_URL);
    }
}