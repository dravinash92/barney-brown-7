<?php 


class StoreReportsModel extends Model
{
	//database connection object
	public $db; 

	public function __construct()
	{
		$this->db = parent::__construct();
	}	
	
	public function getPickupStores(){ 
		$get_url     = API_URL.'cart/listPickupAddress/';	
		$json          = $this->receive_data($get_url,array());
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function getOrders($data){		
		$get_url     = API_URL.'reports/getOrders';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;	    
	}	
	
	public function getDiscountsList(){
		$get_url     = API_URL.'reports/getDiscountsList';	
		$json          = $this->receive_data($get_url,array());
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function getSystemUsers(){
		$get_url     = API_URL.'reports/getSystemUsers';	
		$json          = $this->receive_data($get_url,array());
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function doTotalSalesFilterReports($data){
		$get_url     = API_URL.'reports/doTotalSalesFilterReports';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function getSandwichItems($data=array()){
		$get_url     = API_URL.'reports/sandwichitems';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getStandaredItems($data=array()){
		$get_url     = API_URL.'reports/standareditems';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getStandardCategory($data=array()){
		$get_url     = API_URL.'reports/getStandardCategory';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getStandaredItemsQty($data=array()){
		$get_url     = API_URL.'reports/standareditemsqty';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getCustomItems($data=array()){
		$get_url     = API_URL.'reports/customitems';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function doTotalItemsFilterReports($data){
		$get_url     = API_URL.'reports/doTotalItemsFilterReports';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function doTotalCustomItemsFilterReports($data){
		$get_url     = API_URL.'reports/doTotalCustomItemsFilterReports';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	public function getOrderedItems($data){		
		$get_url     = API_URL.'reports/getOrderedItems';
		$json          = $this->receive_data($get_url,$data);		
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getHourlyOrderReport($post)
	{		
		$get_url     = API_URL.'reports/getHourlyOrderReport';
		$json          = $this->receive_data($get_url,$post);	
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function getOrderDetails($post){
		$get_url     = API_URL.'reports/getOrderDetails';
		$json          = $this->receive_data($get_url,$post);	
		$finalData = @json_decode($json);
		return $finalData->Data;	
	}
	
	public function checkReportPin($post){
		$get_url     = API_URL.'reports/checkReportPin';
		$json          = $this->receive_data($get_url,$post);	
		$finalData = @json_decode($json);
		return $finalData->Data;	
	}
	
	public function getTotalSaleCount($data){
		$get_url     = API_URL.'reports/getTotalSaleCount';
		$json          = $this->receive_data($get_url,array('ids'=>$data));
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
}