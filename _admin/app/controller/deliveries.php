<?php
ini_set("display_errors", 0);
class Deliveries extends Base
{
    public $model;
    
    public function __construct()
    {
        
        $this->model = $this->load_model('deliveriesModel');
    }
    
    function index()
    {
        
        $this->title = TITLE . " | Admin-Delivery";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $date  = date("d M Y");
        $data  = array(
            "search_date" => $date
        );
        
        

        $model = $this->model;
        

        //Pickup stores
        $pickupstores = $model->getPickupStores();
        $this->smarty->assign('pickupstores', $pickupstores);
        $getDeliveryDetails = $sortOrder = $model->get_all_delivery_items(API_URL, $data);
        //echo "<pre>";print_r($getDeliveryDetails);exit;
            $del_assigned = '';
            $del_deliverd = '';
         $newDD = array(  );
         foreach( $getDeliveryDetails as $result )
        {
            
            if($result->delivery_assigned_to != 0)
            {
                $result->delivery_assigned_time = '('.date('h:i A', strtotime($result->delivery_assigned_time)).')';
            }
            if($result->order_delivered != '0000-00-00 00:00:00')
            {
                $result->order_delivered = '('.date('h:i A', strtotime($result->order_delivered)).')';
            }
             $deliveryDate = new DateTime( $result->date );
             $deliveryDate->setTime( 0,1,0 );
            
             $todayDate = new DateTime( "now" );
             $todayDate->setTime( 0,1,0 );
            
            if( $deliveryDate == $todayDate )
            {
                $newDD[] = $result;
            };
            
        }

        $getDeliveryDetails = $newDD;
        //echo "<pre>";print_r($getDeliveryDetails);exit;
        $this->smarty->assign('DeliveryDataList', $getDeliveryDetails);
        $this->smarty->assign('date', $date);

        foreach ($sortOrder as $key => $part) 
        {
            $sort[$key] = strtotime($part->order_date);
        }
        array_multisort($sort, SORT_DESC, $sortOrder);

        
        $this->smarty->assign('DeliverySortDataList', $sortOrder);
        
        
        $isStore = 0;
        if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'store') {
            $isStore = 1;
        }
        $this->smarty->assign('isStore', $isStore);
        
        
        $data['status'] = '1';
        $getOrderSatus  = $model->get_all_order_status(API_URL, $data);
        $this->smarty->assign('getOrderSatusList', $getOrderSatus);
        
        
        foreach ($getDeliveryDetails as $result) {
            
            if ($result->order_status == 0) {
                $count_new[] = $result->order_status;
            }
            
            if ($result->order_status == 5) {
                $count_prc[] = $result->order_status;
            }
            
            if ($result->order_status == 6) {
                $count_bag[] = $result->order_status;
            }
            
            if ($result->order_status == 7) {
                $count_pick[] = $result->order_status;
            }
            
        }
        
        $orderAdd = 'Order';
        
        $del_status = '';
        $this->smarty->assign('del_status', $del_status);
        
        
        $this->smarty->assign('count_new', count(@$count_new));
        $this->smarty->assign('count_prc', count(@$count_prc));
        $this->smarty->assign('count_bag', count(@$count_bag));
        $this->smarty->assign('count_pick', count(@$count_pick));
        $total_delivery = count(@$count_new) + count(@$count_prc) + count(@$count_bag) + count(@$count_pick);
        $this->smarty->assign('total_delivery', $total_delivery);
        $this->smarty->assign('content', "deliveries.tpl");
        
        $nav_ops = array(
            'show' => false
        );
        $this->smarty->assign('SEC_NAV_OPS', $nav_ops);
        $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'deliveries');
        
    }
    
    
    function get_order_popup_data()
    {
        
        $order_id  = @$_POST['order_id'];
        $order_det = $this->model->get_order_popup_data($order_id);
               
        
        $this->smarty->assign('authorizenet_transaction_id', @$order_det->Data->authorizenet_transaction_id);
        $this->smarty->assign('order_total_count', @$order_det->Data->orderQty);
        $this->smarty->assign('address_non_modified', @$order_det->Data->address_non_modified);
        $this->smarty->assign('order_date_time', @$order_det->Data->order_date);
        $this->smarty->assign('date', @$order_det->Data->date);
        $this->smarty->assign('time', @$order_det->Data->time);
        $this->smarty->assign('billing_type', @$order_det->Data->billing_type);
        $this->smarty->assign('billing_card_no', @$order_det->Data->billing_card_no);
        $this->smarty->assign('address', @$order_det->Data->address);
        $this->smarty->assign('delivery_instructions', @$order_det->Data->delivery_instructions);
        $this->smarty->assign('total', @$order_det->Data->total);
        $this->smarty->assign('sub_total', @$order_det->Data->sub_total);
        $this->smarty->assign('tip', @$order_det->Data->tip);
        $this->smarty->assign('order_item_detail', @$order_det->Data->order_item_detail);
        
        $this->smarty->assign('specific_time', @$order_det->Data->specific_time);
        $this->smarty->assign('discount', @$order_det->Data->discount);
        $this->smarty->assign('orderid', $order_id);
        $this->smarty->assign('is_delivery', @$order_det->Data->is_delivery);
        
        
        
        $sum = 0;
        foreach (@$order_det->Data->order_item_detail as $cdata) {
            
            if (isset($cdata->qty)) {
                $sum = $sum + $cdata->qty;
            } else {
                $sum = $sum + 1;
            }
            
        }
        
        $this->smarty->assign('count_order', $sum);
        echo $this->smarty->fetch("order_detail_popup.tpl");
        exit;
        
    }
    
     
    function managedeliveryData()
    {
        $order_id    = $_POST['order_id'];
        $status_code = $_POST['id'];
        
        $data       = array(
            'status_code' => $status_code,
            'order_id' => $order_id
        );
        $model      = $this->model;
        $getAddress = $model->update_order_status(API_URL, $data);
        exit;
    }

    function searchDeliveryDataAjax()
    {
       
        $date = $_POST['search_date'];
        
        if ($date) {
            $date_replace = str_replace(',', ' ', $date);
            $str_to_time  = date('Y-m-d', strtotime($date_replace));
            $date = date("d M Y", strtotime($date_replace));
        } else{
            $str_to_time = '';
            $date = date("d M Y");
        }
        
        
        
        $this->title = TITLE . " | Admin-Delivery";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        $isStore = 0;
        if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'store') {
            $isStore = 1;
        }
        $this->smarty->assign('isStore', $isStore);
        
        $data = array(
            'order_status' => $_POST['delivery_status'],
            'store_id' => @$_POST['store_id'],
            'date' => $str_to_time,
            'status_flag' => @$_POST['delivery_status'],
            'delivery' => '1'
        );
        $this->smarty->assign('data', $data);
        $model        = $this->model;
        $pickupstores = $model->getPickupStores();
        $this->smarty->assign('pickupstores', $pickupstores);
        
        $getDeliveryDetails = $sortOrder = $model->get_search_delivery_items(API_URL, $data);

        // $del_user_details = $model->get_this_del_users($getDeliveryDetails[0]->delivery_assigned_to);
        
        // $del_user = '';
        // $del_assigned = '';
        // $del_deliverd = '';
        // if(!empty($del_user_details->Data))
        // {
        //     $del_user = json_decode(json_encode($del_user_details->Data[0]), true);
        //     $del_user = $del_user['name'];
        //     $del_assigned = '('.date('h:i A', strtotime($getDeliveryDetails[0]->delivery_assigned_time)).')';
        //     $del_deliverd = '('.date('h:i A', strtotime($getDeliveryDetails[0]->order_delivered)).')';
            
        // }
        // $this->smarty->assign('del_user', $del_user);
        // $this->smarty->assign('del_assigned', $del_assigned);
        // $this->smarty->assign('del_deliverd', $del_deliverd);
         $newArr = array(  );
        foreach( $getDeliveryDetails as $result )
        {
             $deliveryDate = new DateTime( $result->date );
             $deliveryDate->setTime( 0,1,0 );
            
            $todayDate = new DateTime( $date );
             $todayDate->setTime( 0,1,0 );

             // print "DD: ". $deliveryDate;
             // print "this: ". $todayDate;

             // print "MATCH: ".$deliveryDate == $todayDate;
            if($result->delivery_assigned_to != 0)
            {
                $result->delivery_assigned_time = '('.date('h:i A', strtotime($result->delivery_assigned_time)).')';
            }
            if($result->order_delivered != '0000-00-00 00:00:00')
            {
                $result->order_delivered = '('.date('h:i A', strtotime($result->order_delivered)).')';
            }
             if( $deliveryDate == $todayDate )
            {
                $newArr [] = $result;
            }
        }
        $getDeliveryDetails = $newArr;
        $del_status = $_POST['delivery_status'];
        $this->smarty->assign('del_status', $del_status);
        $this->smarty->assign('date', $date);
        $this->smarty->assign('DeliveryDataList', $getDeliveryDetails);
        foreach ($sortOrder as $key => $part) 
        {
            $sort[$key] = strtotime($part->order_date);
        }
        array_multisort($sort, SORT_DESC, $sortOrder);

        
        $this->smarty->assign('DeliverySortDataList', $sortOrder);
        
        $data['status'] = '1';
        $getOrderSatus  = $model->get_all_order_status(API_URL, $data);

        $this->smarty->assign('getOrderSatusList', $getOrderSatus);
         $count_new = array();
         $count_prc = array();
         $count_bag = array();
         $count_pick = array();
        
        foreach ($getDeliveryDetails as $result) {
            
            if ($result->order_status == 0) {
                $count_new[] = $result->order_status;
            }
            
            if ($result->order_status == 5) {
                $count_prc[] = $result->order_status;
            }
            
            if ($result->order_status == 6) {
                $count_bag[] = $result->order_status;
            }
            
            if ($result->order_status == 7) {
                $count_pick[] = $result->order_status;
            }
            
        }
        
        $orderAdd = 'Order';
        
        $this->smarty->assign('count_new', count(@$count_new));
        $this->smarty->assign('count_prc', count(@$count_prc));
        $this->smarty->assign('count_bag', count(@$count_bag));
        $this->smarty->assign('count_pick', count(@$count_pick));
        
        $total_delivery = count(@$count_new) + count(@$count_prc) + count(@$count_bag) + count(@$count_pick);
        
        $this->smarty->assign('total_delivery', $total_delivery);
        $this->smarty->assign('content', "deliveriesajax.tpl");
        $data                     = $this->smarty->fetch('deliveriesajax.tpl');
        $response                 = new stdClass;
        $response->html           = $data;
        $response->count_new      = count(@$count_new);
        $response->count_prc      = count(@$count_prc);
        $response->count_bag      = count(@$count_bag);
        $response->count_pick     = count(@$count_pick);
        $response->total_delivery = $total_delivery;
        echo json_encode($response);
        exit;
        
    }
    
    function searchDeliveryData()
    {
        $date = $_POST['search_date'];
        
        if( !isset($date) || !$date )   
        {
            $date_replace = date( "D M j, Y" );     //Wed Dec 31, 1969
        }   
        else
        {
            $date_replace=str_replace(',',' ',$date);
        }
        

        $str_to_time  = date('Y-m-d', strtotime($date_replace));
        
        $date = date("d M Y", strtotime($date_replace));
        
        $this->title = TITLE . " | Admin-Delivery";
        $this->smarty->assign('SITE_URL', SITE_URL);
        
        
        $data = array(
            'order_status' => $_POST['delivery_status'],
            'store_id' => @$_POST['store_id'],
            'date' => $str_to_time,
            'status_flag' => @$_POST['delivery_status'],
            'delivery' => '1'
        );
        
        $this->smarty->assign('data', $data);
        $model        = $this->model;
        //Pickup stores
        $pickupstores = $model->getPickupStores();
        $this->smarty->assign('pickupstores', $pickupstores);
        
        $getDeliveryDetails = $sortOrder = $model->get_search_delivery_items(API_URL, $data);

        $del_user_details = $model->get_this_del_users($getDeliveryDetails[0]->delivery_assigned_to);
        //echo "<pre>";print_r($del_user_details);exit;
        $del_user = '';
        $del_assigned = '';
        $del_deliverd = '';
        if(!empty($del_user_details->Data))
        {
            $del_user = json_decode(json_encode($del_user_details->Data[0]), true);
            $del_user = $del_user['name'];
            $del_assigned = date('h:i A', strtotime($getDeliveryDetails[0]->delivery_assigned_time));
            $del_deliverd = date('h:i A', strtotime($getDeliveryDetails[0]->order_delivered));
            
        }
        $this->smarty->assign('del_user', $del_user);
        $this->smarty->assign('del_assigned', $del_assigned);
        $this->smarty->assign('del_deliverd', $del_deliverd);
        $newArr = array(  );
        foreach( $getDeliveryDetails as $result )
        {
            if($result->delivery_assigned_to != 0)
            {
                $result->delivery_assigned_time = '('.date('h:i A', strtotime($result->delivery_assigned_time)).')';
            }
            if($result->order_delivered != '0000-00-00 00:00:00')
            {
                $result->order_delivered = '('.date('h:i A', strtotime($result->order_delivered)).')';
            }
             $deliveryDate = new DateTime( $result->date );
             $deliveryDate->setTime( 0,1,0 );
            
            $todayDate = new DateTime( $date );
             $todayDate->setTime( 0,1,0 );

             // print "DD: ". $deliveryDate;
             // print "this: ". $todayDate;

             // print "MATCH: ".$deliveryDate == $todayDate;
            
             if( $deliveryDate == $todayDate )
            {
                $newArr [] = $result;
            }
        }
        $getDeliveryDetails = $newArr;
        
        $del_status = $_POST['delivery_status'];
        $this->smarty->assign('del_status', $del_status);
        $this->smarty->assign('date', $date);
        $this->smarty->assign('DeliveryDataList', $getDeliveryDetails);
        foreach ($sortOrder as $key => $part) 
        {
            $sort[$key] = strtotime($part->order_date);
        }
        array_multisort($sort, SORT_DESC, $sortOrder);

        
        $this->smarty->assign('DeliverySortDataList', $sortOrder);
        //echo "<pre>";print_r($getDeliveryDetails);exit;
        $data['status'] = '1';
        $getOrderSatus  = $model->get_all_order_status(API_URL, $data);
        $this->smarty->assign('getOrderSatusList', $getOrderSatus);
        
        
        foreach ($getDeliveryDetails as $result) {
            
            if ($result->order_status == 0) {
                $count_new[] = $result->order_status;
            }
            
            if ($result->order_status == 5) {
                $count_prc[] = $result->order_status;
            }
            
            if ($result->order_status == 6) {
                $count_bag[] = $result->order_status;
            }
            
            if ($result->order_status == 7) {
                $count_pick[] = $result->order_status;
            }
            
        }
        
        $orderAdd = 'Order';
        
        $this->smarty->assign('count_new', count(@$count_new));
        $this->smarty->assign('count_prc', count(@$count_prc));
        $this->smarty->assign('count_bag', count(@$count_bag));
        $this->smarty->assign('count_pick', count(@$count_pick));
        $total_delivery = count(@$count_new) + count(@$count_prc) + count(@$count_bag) + count(@$count_pick);
        $this->smarty->assign('total_delivery', $total_delivery);
        $this->smarty->assign('content', "deliveries.tpl");
        
    }

    private function amp_fix( $txt )
    {
        $txt = html_entity_decode($txt);
        return str_Replace( '&amp;', '&', $txt );
    }
    
    /*
     * printOrder()
     * To print the order details in pdf fromat
     *
     */
    function printOrder() 
    {
    
        $order_id = $this->UrlArray [2];
        $order_det = $this->model->get_order_popup_data ( $order_id );
        
        $spc_date = strtoupper(date('D, M dS, Y', strtotime($order_det->Data->specified_date)));
        $spc_time =  strtoupper(date('g:iA', strtotime($order_det->Data->specified_date)));
        $ord_date = strtoupper(date('D, M dS, Y', strtotime($order_det->Data->order_date)));
        $ord_time =  strtoupper(date('g:iA', strtotime($order_det->Data->order_date)));
        //echo '<pre>';print_r($spc_date);exit;
        include_once ('app/base/fpdf/fpdf.php');
        
        $spc_time = ($order_det->Data->specific_time == 1)?$spc_time:'';
        
        $pdf = new FPDF ( "p", 'in', 'letter' );
        $pdf->SetMargins ( 0.15625, 0.5 );
        $pdf->cMargin = 0;
        $pdf->SetAutoPageBreak ( true, 0.5 );
        $pdf->AddPage ();
        $pdf->AddFont ( 'Lobster', '', 'lobster_1.4-webfont.php' );
        $pdf->AddFont ( 'OpenSans-Regular', '', 'OpenSans-Regular-webfont.php' );
        $pdf->AddFont ( 'OpenSans-Bold', '', 'OpenSans-Bold.php' );
        $pdf->AddFont ( 'Oswald-Regular', '', 'Oswald-Regular.php' );
    
        $leftRowOffset = 0.5;
        $rightRowOffset = 0.5;
        $row_height = 1;
        
        // 10 right column items fit on a page, but only 7 left column items do, so we'll need to keep different counters for each side.
        $leftItem = 0;
        $rightItem = 0;
    
        $sum = 0;
        foreach ( @$order_det->Data->order_item_detail as $cdata ) {
    
            if (isset ( $cdata->qty )) {
                $sum = $sum + $cdata->qty;
            } else {
                $sum = $sum + 1;
            }
        }
        $itemcountcheck = 0;
        $pagecountcheck = 0;
        $rowCount=0;
        //Main Loop

        //  $logThis = $order_det;
        //file_put_contents( "app/log.log", var_export( $logThis ,true ) );
        //die;
        
        //create a copy of the order details to use on the right side,
        //because sometimes more sandwiches fit on one side than the other so the same loop can't be used for both.
        $rightOrdersArray=array();
        //echo '<pre>';print_r($order_det->Data->order_item_detail);
        foreach ( $order_det->Data->order_item_detail as $row ) {
            if ($row->type  == 'user_sandwich') {
                for($i=0;$i<$row->qty;$i++){
                    $rightOrdersArray[] = $row;
                }
            }
        }

        foreach ( $order_det->Data->order_item_detail as $row ) {
            
            $rowCount++;
            
            // add page header if this is a new page
            if ($leftItem == 0) {
                $pdf->SetX ( 0.7 );
                $pdf->SetFont ( 'Oswald-Regular', '', 10 );
                $pdf->MultiCell ( 4.9365, $row_height * .225, strtoupper ( 'DELIVERY #' . $order_id . '
CUSTOMER: ' . $order_det->Data->first_name. ' ' . $order_det->Data->last_name . '
' . $ord_date .' - '.$ord_time), 0, 'L', false );

                $pdf->SetX ( 0.7 );
                $pdf->SetFontSize(12);

                if($order_det->Data->specific_time == 1)
                {
                    $pdf->SetTextColor(255,255,255);
                    $pdf->cell( 2, $row_height * .3, strtoupper ($spc_date .' - '.$spc_time), 0, 0,'C', true ); 
                    $pdf->SetTextColor('');
                }
                else
                {   
                    $pdf->SetLineWidth ( 0.01);
                    $pdf->SetDrawColor (0, 0, 0 );
                    $pdf->cell( 1, $row_height * .3, strtoupper ("DUE NOW"), 1, 0,'C', false );
                }
                $pdf->SetX ( 3.1 );
                $pdf->SetFont ( 'OpenSans-Bold', '', 9 );
                $pdf->MultiCell ( 4.9365, - 0.89, 'DELIVERY INSTRUCTIONS', 0, 'L', false );
                $pdf->SetFont ( 'OpenSans-Regular', '', 6 );
                $pdf->SetXY(3.1,.83);
                $pdf->MultiCell ( 2.4, .15, $order_det->Data->delivery_instructions, 0, 'J', false );
                
                $leftRowOffset += 1.3;
               
            }//Ends page header
             
            $pdf->SetY ( $leftRowOffset );
            $pdf->SetDrawColor ( 255, 255, 255 );
            $pdf->SetLineWidth ( 0.015 );
            $pdf->SetX ( 0.7 );
            $pdf->MultiCell ( .2, .2, '', 1, 'L', false );
   
            $pdf->SetY ( $leftRowOffset );
            $pdf->SetX ( 1 );
            $pdf->SetFont ( 'Oswald-Regular', '', 12 );

            $pdf->MultiCell ( 4.5, $row_height * .225, $row->qty, 0, 'L', false );
    
            $pdf->SetY ( $leftRowOffset + .04 );
            $pdf->SetX ( 1.20 );
            $pdf->SetFont ( 'OpenSans-Regular', '', 8 );
            if ($row->type == "product") {
                $pdf->MultiCell ( 4.5, $row_height * .13, $this->amp_fix( $row->product_name ), 0, 'L', false );
                // Modifiers
                if(!empty($row->extra_id)){
                    $currY = $pdf->GetY ();
                    $pdf->SetX ( 1.20 );
                    $pdf->MultiCell ( 3.75, $row_height * .13, 'Modifiers:', 0, 'L', false );
                    $pdf->SetY ( $currY );
                    $pdf->SetX ( 1.75 );
                    $pdf->MultiCell ( 3.75, $row_height * .13, $row->extra_id, 0, 'L', false );
                }
                
                // Special instructions
                if(!empty($row->spcl_instructions)){
                    $currY = $pdf->GetY ();
                    $pdf->SetX ( 1.20 );
                    $pdf->MultiCell ( 3.75, $row_height * .13, 'Special Instructions:', 0, 'L', false );
                    $pdf->SetY ( $currY );
                    $pdf->SetX ( 2.25 );
                    $pdf->MultiCell ( 3.75, $row_height * .13, $row->spcl_instructions, 0, 'L', false );
                }
                
                $leftRowOffset += 0.5;
                $leftItem += 0.5;
            }
            else {
                $pdf->MultiCell ( 4.5, $row_height * .13, $this->amp_fix(  $row->sandwich_name ), 0, 'L', false );
    
                // have to split this cell into several parts to do the indenting...
                // bread
                $currY = $pdf->GetY ();
                $pdf->SetX ( 1.20 );
                $pdf->MultiCell ( 3.75, $row_height * .13, 'BRD:', 0, 'L', false );
                $pdf->SetY ( $currY );
                $pdf->SetX ( 1.507 );
                $pdf->MultiCell ( 3.75, $row_height * .13, $row->sandwich_bread, 0, 'L', false );
                // condiments
                $currY = $pdf->GetY ();
                $pdf->SetX ( 1.20 );
                $pdf->MultiCell ( 3.75, $row_height * .13, 'CON:', 0, 'L', false );
                $pdf->SetY ( $currY );
                $pdf->SetX ( 1.507 );
                $pdf->MultiCell ( 3.75, $row_height * .13, $row->sandwich_condiments, 0, 'L', false );
                // protein
                $currY = $pdf->GetY ();
                $pdf->SetX ( 1.20 );
                $pdf->MultiCell ( 3.75, $row_height * .13, 'PRO:', 0, 'L', false );
                $pdf->SetY ( $currY );
                $pdf->SetX ( 1.507 );
                $pdf->MultiCell ( 3.75, $row_height * .13, $row->sandwich_protein, 0, 'L', false );
                // cheese
                $currY = $pdf->GetY ();
                $pdf->SetX ( 1.20 );
                $pdf->MultiCell ( 3.75, $row_height * .13, 'CHE:', 0, 'L', false );
                $pdf->SetY ( $currY );
                $pdf->SetX ( 1.507 );
                $pdf->MultiCell ( 3.75, $row_height * .13, $row->sandwich_cheese, 0, 'L', false );
                // toppings
                $currY = $pdf->GetY ();
                $pdf->SetX ( 1.20 );
                $pdf->MultiCell ( 3.75, $row_height * .13, 'TOP:', 0, 'L', false );
                $pdf->SetY ( $currY );
                $pdf->SetX ( 1.507 );
                $pdf->MultiCell ( 3.75, $row_height * .13, $row->sandwich_topping, 0, 'L', false );
    
                if ($row->toasted == "1") {
                    $pdf->SetX ( 1.20 );
                    $pdf->SetFont ( 'Lobster', '', 10 );
                    if($toppingscount > 66)
                        $pdf->MultiCell ( 3, $row_height * .3, 'Toasted', 0, 'L', false );
                    else
                        $pdf->MultiCell ( 2.4, $row_height * .12, 'Toasted', 0, 'L', false );
                }


    
                if ( isset($detailscount) && $detailscount > 350) {
                    $leftRowOffset += 2;
                    $leftItem += 2;
                }
                else if (  isset($detailscount) && $detailscount > 220) {
                    $leftRowOffset += 1.5;
                    $leftItem += 1.5;
                }
                else if (  isset($toppingscount) && $toppingscount > 66 && $row->toasted == "1") {
                    $leftRowOffset += 1.5;
                    $leftItem += 1.5;
                }
                else {
                    $leftRowOffset += 1;
                    $leftItem += 1;
                }
            }
    
            // after adding the first left item to the page, add ten right items if they exist with two address at top
            $leftItemcheck = 0;
    
            if ( isset($detailscount) &&  $detailscount > 350) {
                $leftItemcheck = 2;
            } else if ( isset($detailscount) && $detailscount > 220) {
                $leftItemcheck = 1.5;
            } 
            else if ( isset($toppingscount) && $toppingscount > 66 && $row->toasted == "1") {
                    $leftItemcheck = 1.5;
                }
                else {
                $leftItemcheck = 1;
            }

           /*print "leftItem $leftItem <br> ";
           print "leftItemcheck $leftItemcheck<br> ";
           print "pagecountcheck $pagecountcheck<br> ";
            */
            
            if ($leftItem == $leftItemcheck || $leftItem == 0.5) {
    
                // add borders for testing
                $pdf->SetY ( 0.5 );
                for($i = 0; $i < 10; $i ++) {
                    $pdf->SetLineWidth ( 0.005 );
                    $pdf->Cell ( 2.625, $row_height, '', 1, 0, false );
                    $pdf->Cell ( 0.15625, $row_height, '' );
                    $pdf->Cell ( 2.625, $row_height, '', 1, 0, false );
                    $pdf->Cell ( 0.15625, $row_height );
                    $pdf->Cell ( 2.625, $row_height, '', 1, 0, false );
                    $pdf->Ln ();
                }
    
                
                //Add two Address rows on right-top
                if($pagecountcheck==0)
                {
                    $debug_string = "LI: $leftItem ; LIC:  $leftItemcheck ; PC: $pagecountcheck ";
                    for($i = 0; $i < 2; $i ++) {
                    
        
                        $pdf->SetFont ( 'Oswald-Regular', '', 10 );
    
                        $pdf->SetY ( $rightRowOffset + 0.1 );
                        $pdf->SetX ( 5.875 );
                        $pdf->cell (1.9, $row_height * .175, strtoupper ( $order_det->Data->address->name ), 0, 'L', false );
                        $pdf->cell ( 0.7, $row_height * .175, $spc_time, 0, 'L', false );
    
                        $pdf->SetFont ( 'OpenSans-Regular', '', 7 );
    
                        $pdf->SetY ( $rightRowOffset + 0.275 );
                        $pdf->SetX ( 5.875 );
                        $pdf->MultiCell ( 2.46875, 0.11, $order_det->Data->address->company . "\n" . $order_det->Data->address->addline1 . " " . $order_det->Data->address->street . "\n" . "New York, NY " . $order_det->Data->address->zip . "\n" . $order_det->Data->address->cross_streets . "\n" . $order_det->Data->address->phone." ".$order_det->Data->address->extn. "\n" . "Order Placed: " . $order_det->Data->order_date, 0, 'L', false );
                        $pdf->SetY ( $rightRowOffset + 0.28 );
                        $pdf->SetX ( 5.875 );
                        $pdf->MultiCell ( 2.375, 0, $order_det->Data->address->zipAbbr . '-' . $sum, 0, 'R', false );

                        
                        $pdf->SetY ( $rightRowOffset + 0.35 );
                        $pdf->SetX ( 5.875 );
                        //$pdf->MultiCell ( 2.375, 0,  $debug_string, 0, 'R', false );
    
                        
    
                        $rightRowOffset ++;
                    }
                }
            }
            
            //if the page is full, or all left items have been added, add items to the right side
            $newPageNeeded = false;
            $detailscount = strlen ( $row->sandwich_bread . $row->sandwich_condiments . $row->sandwich_protein . $row->sandwich_cheese . $row->sandwich_topping );
            $toppingscount = strlen ($row->sandwich_topping );
            if (
                ($leftItem >= 8.5 && $detailscount!=0) ||
                ($leftItem >= 7.5 && $toppingscount > 64 && $row->toasted == "1") ||
                ($leftItem >= 7.5 && $detailscount > 200) ||
                ($leftItem >= 8)
            ) {
                $newPageNeeded = true;
            }
            
            if (($newPageNeeded == true || count($order_det->Data->order_item_detail) == $rowCount) && count($rightOrdersArray) > 0) {
              //  echo '<pre>';print_r($rightOrdersArray[0]->sandwich_details);
                $tmp_object = $rightOrdersArray[0];

                if($pagecountcheck > 0){
                    $rightItemCount=10;
                }
                if($pagecountcheck == 0) {
                    $rightItemCount=8;
                }
                for($i=0;$i<$rightItemCount;$i++){   
                    $pdf->SetFont ( 'Oswald-Regular', '', 10 );

                    $pdf->SetY ( $rightRowOffset + 0.2 );

                    $pdf->SetX ( 5.875 );
                    $pdf->MultiCell ( 2.46875, $row_height * .175,  $this->amp_fix( $rightOrdersArray[0]->sandwich_name ), 0, 'L', false );
                   // echo '<pre>';print_r($rightOrdersArray[0]->sandwich_details);

                    $pdf->SetFont ( 'OpenSans-Regular', '', 7 );

                    $pdf->SetY ( $rightRowOffset + 0.375 );
                    $pdf->SetX ( 5.875 );
                    $extra = '';
                    if (strlen ( $rightOrdersArray[0]->sandwich_details ) > 127) {
                        $extra = '...';
                    }

                    $pdf->MultiCell ( 2.35, 0.11, str_replace ( ', , ',', ', str_replace ( ', , ',', ', substr ( $rightOrdersArray[0]->sandwich_details , 0, 128 ))) . $extra, 0, 'L', false );

                    if ($rightOrdersArray[0]->toasted == "1") {
                        $pdf->SetY ( $rightRowOffset + 0.75 );
                        $pdf->SetX ( 5.875 );
                        $pdf->SetFont ( 'Lobster', '', 10 );
                        $pdf->MultiCell ( 2.35, $row_height * .175, 'Toasted', 0, 'L', false );
                    }
                    $pdf->Ln ();
                    $rightRowOffset++;
                    $rightItem++;
                    
                    array_shift($rightOrdersArray);
                }
                
                //add a new page if there are still more left items to show
                if(count($order_det->Data->order_item_detail) > $rowCount){
                    $pdf->AddPage ();
                    $leftRowOffset = 0.5;
                    $rightRowOffset = 0.5;
                    $leftItem = 0;
                    $pagecountcheck++;
                    $rightItem = 0;
                }
            }
    
        }//ends main loop
        
        //if there are still more sandwiches to list on the right side, continue making more pages as necessary
       /* if (count($rightOrdersArray)>0) {
            $rightItem = 0;
            for($i=0; $i<count($rightOrdersArray); $i++){
                if($rightItem == 0){
                    // add new page header and reset positioning
                    $pdf->AddPage ();
                    $rightRowOffset = 0.5;
                    $pdf->SetX ( 0.7 );
                    $pdf->SetFont ( 'Oswald-Regular', '', 10 );
                   $pdf->MultiCell ( 4.9365, $row_height * .225, strtoupper ( 'DELIVERY #' . $order_id . '
CUSTOMER: ' . $order_det->Data->first_name. ' ' . $order_det->Data->last_name . '
' . $ord_date .' - '.$ord_time), 0, 'L', false );

                $pdf->SetX ( 0.7 );
                $pdf->SetFontSize(12);

                if($order_det->Data->specific_time == 1)
                {
                    $pdf->SetTextColor(255,255,255);
                    $pdf->cell( 2, $row_height * .3, strtoupper ($spc_date .' - '.$spc_time), 0, 0,'C', true ); 
                    $pdf->SetTextColor('');
                }
                else
                {
                    $pdf->cell( 1, $row_height * .3, strtoupper ("DUE NOW"), 1, 0,'C', false );
                }
                $pdf->SetX ( 3.1 );
                $pdf->SetFont ( 'OpenSans-Bold', '', 9 );
                $pdf->MultiCell ( 4.9365, - 0.89, 'DELIVERY INSTRUCTIONS', 0, 'L', false );
                $pdf->SetFont ( 'OpenSans-Regular', '', 6 );
                $pdf->SetXY(3.1,.83);
                $pdf->MultiCell ( 2.4, .15, $order_det->Data->delivery_instructions, 0, 'J', false );
                }
                
                $pdf->SetFont ( 'Oswald-Regular', '', 10 );
                $pdf->SetY ( $rightRowOffset + 0.2 );
                $pdf->SetX ( 5.875 );
                $pdf->MultiCell ( 2.46875, $row_height * .175,  $this->amp_fix( $rightOrdersArray[$i]->sandwich_name ), 0, 'L', false );

                $pdf->SetFont ( 'OpenSans-Regular', '', 7 );
                $pdf->SetY ( $rightRowOffset + 0.375 );
                $pdf->SetX ( 5.875 );
                $extra = '';
                if (strlen ( $rightOrdersArray[$i]->sandwich_details ) > 127) {
                    $extra = '...';
                }

                $pdf->MultiCell ( 2.35, 0.11, str_replace ( ', , ',', ', str_replace ( ', , ',', ', substr ( $rightOrdersArray[$i]->sandwich_details , 0, 128 ))) . $extra, 0, 'L', false );

                if ($rightOrdersArray[$i]->toasted == "1") {
                    $pdf->SetY ( $rightRowOffset + 0.75 );
                    $pdf->SetX ( 5.875 );
                    $pdf->SetFont ( 'Lobster', '', 10 );
                    $pdf->MultiCell ( 2.35, $row_height * .175, 'Toasted', 0, 'L', false );
                }
                $pdf->Ln ();
                $rightRowOffset++;
                $rightItem++;
                if($rightItem >= 10){ $rightItem = 0; }
            }
        }*/
    
        $pdf->AutoPrint();
        $pdf->Output ();
    
        exit ();
    
        $this->smarty->assign ( 'authorizenet_transaction_id', @$order_det->Data->authorizenet_transaction_id );
        $this->smarty->assign ( 'order_total_count', @$order_det->Data->orderQty );
        $this->smarty->assign ( 'address_non_modified', @$order_det->Data->address_non_modified );
        $this->smarty->assign ( 'order_date_time', @$order_det->Data->order_date );
        $this->smarty->assign ( 'date', @$order_det->Data->date );
        $this->smarty->assign ( 'time', @$order_det->Data->time );
        $this->smarty->assign ( 'billing_type', @$order_det->Data->billing_type );
        $this->smarty->assign ( 'billing_card_no', @$order_det->Data->billing_card_no );
        $this->smarty->assign ( 'address', @$order_det->Data->address );
        $this->smarty->assign ( 'delivery_instructions', @$order_det->Data->delivery_instructions );
        $this->smarty->assign ( 'total', @$order_det->Data->total );
        $this->smarty->assign ( 'sub_total', @$order_det->Data->sub_total );
        $this->smarty->assign ( 'tip', @$order_det->Data->tip );
        $this->smarty->assign ( 'order_item_detail', @$order_det->Data->order_item_detail );
    
        $this->smarty->assign ( 'specific_time', @$order_det->Data->specific_time );
        $this->smarty->assign ( 'discount', @$order_det->Data->discount );
        $this->smarty->assign ( 'orderid', $order_id );
        $this->smarty->assign ( 'is_delivery', @$order_det->Data->is_delivery );
    
        $sum = 0;
        foreach ( @$order_det->Data->order_item_detail as $cdata ) {
    
            if (isset ( $cdata->qty )) {
                $sum = $sum + $cdata->qty;
            } else {
                $sum = $sum + 1;
            }
        }
    }

    public function getDeliveryUsers()
    {
        $order_id = $_POST['order_id'];
        $ord_details = $this->model->get_this_order_details($order_id);
        $del_users =  $this->model->get_del_users($ord_details[0]);
        echo json_encode($del_users->Data);exit;
    }

    public function assignDeliveryUser()
    { 
        $data       = array('uid' => $_POST['delUser'], 'order_id'=>$_POST['order_id'] );
        $model      = $this->model;
        $getAddress = $model->assign_delivery_user(API_URL, $data);
        return json_encode(1);exit;
    }
    public function removeDeliveryUser()
    {
        $data       = array('order_id'=>$_POST['order_id']);
        $model      = $this->model;
        $getAddress = $model->remove_delivery_user(API_URL, $data);
        return json_encode(1);exit;
    }
    
}
