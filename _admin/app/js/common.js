var admin_ha = {};


window.addEventListener('load',function(){
	if( window.location.href.indexOf('deliveries') != -1 ) { 
		setInterval(function(){  
			if(window.location.href.indexOf('storedeliveries')!= -1)
				 var postUrl = SITE_URL + "storedeliveries/searchDeliveryDataAjax/";
			else
				var postUrl = SITE_URL + "deliveries/searchDeliveryDataAjax/"; 
			
           
            $.ajax({
                type: "POST",
                url: postUrl,
                data: {
                    'search_date': $('#search_delivery_date').val(),
                    'delivery_status': $('select[name="delivery_status"]').val(),
                    'store_id': $('#store_id').val()
                },
                success: function(data) {
                    data = JSON.parse(data);
                    
                    if( data.html.indexOf("No Result Found") == -1) { 
                     
                    if( $(".deliveries-table-detail-wrapper").length == 0 ){
                    	$('.top-deliveries-wrapper').next().remove();
                    	$('.top-deliveries-wrapper').after('<div class="deliveries-table-detail-wrapper"></div>');
                    }  } else {
                    	$('.top-deliveries-wrapper').next().remove();
                    	$('.top-deliveries-wrapper').after('<div style="margin-top: 110px;margin-left: 335px;font-size:200%">No Result Found!</div>');
                    }
                    
                    $(".deliveries-table-detail-wrapper").html("");
                    $(".deliveries-table-detail-wrapper").html(data.html);
                   
                    $(".count_new").html(data.count_new);
                    $(".count_prc").html(data.count_prc);
                    $(".count_bag").html(data.count_bag);
                    $(".count_pick").html(data.count_pick);
                    $(".total_delivery").html(data.total_delivery);

                }
            });
		
		},45000);
	}
});
	var locationUrl = window.location.href;
	
			$(document).on(
			'keydown',
			function(e) {
				
				if (e.ctrlKey && e.keyCode == 80 && parseInt($("#popuporderid").val())) {
					if(locationUrl.indexOf("deliveries") != -1 ){
						e.preventDefault();
						w = window.open(SITE_URL + "deliveries/printOrder/"+ $("#popuporderid").val(), '_blank');
						w.onunload = function() {
						}
						w.focus();
						w.print();
					}else{
						e.preventDefault();
						return false;
					}
				}
			}); 
	
	
admin_ha.common = window.admin_ha.common || {
    json_obj: null,
    zipcodes: [],
    product_extras: null,
    init: function() {
        var self = this;
        self.show_date_picker();
        self.enable_spin_arrows();
        self.enable_label_selections();
        self.sandwich_gallery_search();
        self.toggle_gallery_public();
        self.delete_gallery_item();
        self.flagg_gallery_item();
        self.customer_search();
        self.edit_banner_details();
        self.get_all_zip_codes();
        self.add_extras();
        self.extras_default();
        self.remove_zipCode();
        self.add_card();
		self.setCookie_forcalendar();
        self.search_accounts();
        if (self.json_obj != null) {
            Object.keys(self.json_obj).forEach(function(e) {

                $name = self.json_obj[e].item_name;
                Qty = self.json_obj[e].item_qty;

                if (e != 'BREAD') {
                    $name.forEach(function(t, i) {
                        if (Qty[t]) {
                            itm = Qty[t][0];
                            $ops = $('span:contains(' + t + ')').next();
                            $ops.val(itm);
                        }
                    });
                }

            });

        }

        if ($(".close-button").length > 0) {
            $(".close-button").click(function() {
                var form = $("form#edit-billForm");
                $(form).find("input[type=text], textarea").val("");
                $('.error_msg').text('');
                $(".popup-wrapper").hide();
            });
        }


        $(document).on("click", '.viewOrdDet', function(e) {
            e.preventDefault();
            $id = $(this).attr('id');
	    w = window.open(SITE_URL + "deliveries/printOrder/" +  $id, '_blank');
            w.onunload = function() {
                 w.focus();
                 w.print();
            }
            if(navigator.userAgent.search("Firefox")>-1)
            {         
                                 
            }else{
            }            
            
        });
        $(document).on("click", '.viewOrdDetStore', function(e) {
            e.preventDefault();
            $id = $(this).attr('id');
        w = window.open(SITE_URL + "storedeliveries/printOrder/" +  $id, '_blank');
            w.onunload = function() {
                 w.focus();
                 w.print();
            }
        });
        $(document).on("click", '.viewOrdDetst', function(e) {
            e.preventDefault();
            $id = $(this).attr('id');
        w = window.open(SITE_URL + "storedeliveries/printOrder/" +  $id, '_blank');
            w.onunload = function() {
                 w.focus();
                 w.print();
            }
        });
        self.loadNewOrderEvents();
        self.remove_cart();
        self.edit_customer_info();
        self.customer_notes();
        self.place_order_events();
        self.edit_gallery_options();
        self.updateCartCouponDetails();
        self.totals_sales_report_filter_events();
        self.customer_tabs();
        self.totals_items_report_filter_events();
        self.store_accounts();
        self.orders_search();
        self.newOrderNewUserAccount();
        self.edit_standard_products();
        self.edit_custom_item();
        self.save_webpage();
        self.get_all_product_extra();

        $('.closeOrd').unbind('click').on('click', function() {
            self.hide_order_detail_popup();
        });




        self.SOTD_and_TRND();
        self.item_reports_events();
        self.checkoutStoreSelectionEvent();
        
        
        
        $("input[name=uses_type]").click(function(){
			var num = $(this).val();
			
			if(num == 1){
				$("input[name=uses_amount]").css("background","grey");
				$("input[name=uses_amount]").val(0);
			}  else {
				$("input[name=uses_amount]").css("background","");
			}

		});
		
		$("input[name=uses_amount]").keyup(function(){
			var num = $("input[name=uses_type]:checked").val();
			
			if(num == 1){
				$(this).val(0);
				return false;
			}
		});

    },

    item_reports_events: function() {

        $("span.hide_subtottal").replaceWith($("span.tottal_subtottal"));
        $("span.hide_totalqty").replaceWith($("span.tottal_qty"));
        


        var total_subtotal = $("span.tottal_subtottal").text();
        var totalpercentage = 0;
        var tottal_qty = 0;

        
        $("span.hide_salades_subtottal").each(function(index, obj) {

            var data_id = $(obj).data("id");
            var cssclass = data_id.replace("#", "");

            $("table.products tr." + cssclass).each(function(index) {

                var subtotal = $(this).find("td:nth-child(4)").text();
                var itemqty = $(this).find("td:nth-child(3)").text();

                if (parseInt(itemqty)) {
                    tottal_qty = parseInt(tottal_qty) + parseInt(itemqty);
                }

                subtotal = subtotal.trim();
                if (subtotal.length) {
                    subtotal = subtotal.replace("$", "");
                    subtotal = parseFloat(subtotal);
                    total_subtotal = parseFloat(total_subtotal);
                    var percent = (subtotal * 100) / total_subtotal;

                    totalpercentage = totalpercentage + percent;
                    percent = percent.toFixed(2);

                    $(this).find("td:nth-child(5)").text(percent + "%");
                }

            });


        });



        $("table.sandwiches tr.datarow").each(function(index) {

            var subtotal = $(this).find("td:nth-child(4)").text();

            var itemqty = $(this).find("td:nth-child(3)").text();

            if (parseInt(itemqty)) {
                tottal_qty = parseInt(tottal_qty) + parseInt(itemqty);
            }

            subtotal = subtotal.trim();
            if (subtotal.length) {
                subtotal = subtotal.replace("$", "");
                subtotal = parseFloat(subtotal);
                total_subtotal = parseFloat(total_subtotal);
                var percent = (subtotal * 100) / total_subtotal;

                totalpercentage = totalpercentage + percent;
                percent = percent.toFixed(2);

                $(this).find("td:nth-child(5)").text(percent + "%");
            }

        });

        $("span.tottal_qty").text(tottal_qty);

    },

    get_all_product_extra: function() {
        var self = this;


        $.ajax({
            type: "POST",
            url: SITE_URL + 'sandwich/getAllproductsExtras/',
            async: false,
            dataType: 'json',
            success: function(data) {
                self.product_extras = data;
            }
        });

    },


    show_order_detail_popup: function(id, print) {
        $('.popupOrderDetailsBg').fadeIn(200, function() {
            $('.popupOrderDetails,.popup_order_detail').fadeIn(300);
        });
        $('#popOut .loader').show();

        $.ajax({
            type: "POST",
            data: {
                'order_id': id
            },
            url: SITE_URL + 'deliveries/get_order_popup_data/',
            success: function(data) {
                $('#popOut .loader').hide();
                $('#popOut').html(data);
                setTimeout(function() {
                    if (print == true) window.print();
                }, 500);
            }
        });

    },
    show_order_detail_popup_store: function(id, print) {
        $('.popupOrderDetailsBg').fadeIn(200, function() {
            $('.popupOrderDetails,.popup_order_detail').fadeIn(300);
        });
        $('#popOut .loader').show();

        $.ajax({
            type: "POST",
            data: {
                'order_id': id
            },
            url: SITE_URL + 'storedeliveries/get_order_popup_data/',
            success: function(data) {
                $('#popOut .loader').hide();
                $('#popOut').html(data);
                setTimeout(function() {
                    if (print == true) window.print();
                }, 500);
            }
        });

    },


    hide_order_detail_popup: function() {
        $('.popupOrderDetails,.popup_order_detail').fadeOut(300, function() {
            $('.popupOrderDetailsBg').fadeOut(300);
        });
    },


    setCookie: function(cname, cvalue, exdays, path) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        var path = "path=" + path;
        document.cookie = cname + "=" + cvalue + "; " + expires;
    },

    getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    },

    check_session_state: function() {
        var result;
        $.ajax({
            async: false,
            type: "POST",
            url: SITE_URL + "myaccount/is_session",
            dataType: 'json',
            success: function(e) {
                result = e.session_state;
            }
        });
        return result;
    },


    extras_default: function() {

        var self = this;
        $("input[name='extra_default']").unbind("click").on("click", function() {
            $("input[name='extra_default']").attr('value', '');
            parent = $(this).parent().parent();
            rd_val = $(parent).find("input[name='extra_name[]']").val();
            $(this).val(rd_val);
        });

        $("input[name='extra_name[]']").unbind("change").on("change", function() {
            parent = $(this).parent();
            input = $(parent).find("input[name='extra_default']:checked");
            $(input).val($(this).val());
        });

    },

    add_extras: function() {
        var self = this;
        $('.extras_more').unbind('click').on('click', function(e) {
            e.preventDefault();
            current = $(this);
            prev = current.prev();
            elem = $(prev).find('li:last');
            html = $(elem).html();

            count = $(prev).find('li').length;
            rand = Math.round(Math.random() * 10) + 10;
            html = html.replace(/dafault1/g, "dafault" + rand + count);
            $(elem).after("<li>" + html + "</li>");
            inputs = $(elem).next().find('input');
            radio = $(elem).next().find('.radio-section input');
            radio_label = $(elem).next().find('.radio-section label');
            $(radio).attr('id', "dafault" + rand + count);
            $(radio_label).attr('for', "dafault" + rand + count);
            $(inputs).val('');
            self.extras_default();
        });
    },


    get_all_zip_codes: function() {
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'accounts/getAllzipcodes/',
            async: false,
            dataType: 'json',
            success: function(data) {
                self.zipcodes = [];
                Object.keys(data).forEach(function(e) {
                    self.zipcodes.push(data[e]);
                });


            }
        });

    },


    customer_search: function() {
        $('#phonesearch').unbind('click').bind('click', function(e) {
            e.preventDefault();
            $ph1 = $('input[name="ph1"]').val();
            $ph2 = $('input[name="ph2"]').val();
            $ph3 = $('input[name="ph3"]').val();
            $ph4 = $('input[name="ph4"]').val();
            phone = $ph1 + $ph2 + $ph3 + $ph4;
            url = SITE_URL + 'customer/index/?searchphone=' + phone;
            window.open(url, '_self');
        });

        $('#customeremail').unbind('click').bind('click', function(e) {
            e.preventDefault();
            email = $('input[name="customeremail"]').val();
            url = SITE_URL + 'customer/index/?searchemail=' + email;
            window.open(url, '_self');
        });

        $('#customercompany').unbind('click').bind('click', function(e) {
            e.preventDefault();
            company = $('input[name="company"]').val();
            url = SITE_URL + 'customer/index/?searchcompany=' + company;
            window.open(url, '_self');
        });

        $('#customername').unbind('click').bind('click', function(e) {
            e.preventDefault();
            customername = $('input[name="customername"]').val();
            url = SITE_URL + 'customer/index/?searchbyname=' + customername;
            window.open(url, '_self');
        });
        $('#lastname').unbind('click').bind('click', function(e) {

            e.preventDefault();
            customername = $('input[name="lastname"]').val();
            url = SITE_URL + 'customer/index/?searchbylastname=' + customername;
            window.open(url, '_self');
        });

    },
    equalheight: function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
},

    //customers tabbed list control and ajax options
    customer_tabs: function() {
        var self = this;
        var type = 0;
        var $uid = $('input[name="hiduid"]').val();
        $('.admin_customer_tabs li a').unbind('click').on('click', function(e) {
            $('.admin_customer_tabs li a').removeClass('active');
            $(this).addClass('active');
            $txt = $(this).text();
            e.preventDefault();

            switch ($txt) {

                case 'PROFILE':
                    type = 1;
                    break;

                case 'ORDER HISTORY':
                    type = 2;
                    break;

                case 'SAVED MEALS':
                    type = 3;
                    break;

                case 'SAVED ADDRESSES':
                    type = 4;
                    break;

                case 'SAVED BILLING':
                    type = 5;
                    break;

                case 'MEAL REMINDER':
                    type = 6;
                    break;

            }

            self.makeCustomerPageRequest(type, $uid);

        });
        self.loadProfileEvents();

    },

    makeCustomerPageRequest: function(type, $uid) {
        var self = this;

        $.ajax({
            type: "POST",
            data: {
                'type': type,
                'uid': $uid
            },
            url: SITE_URL + 'customer/ajax_tab_load',
            success: function(e) {
                self.customer_tabs_change(e);
                

                if (type == 1) {  
                    self.loadProfileEvents();
                }

                if (type == 4) {  
                    self.equalheight('.saved-address-inner li');
                    self.loadCustomerSavedAddressEvents();
                }

                if (type == 5) {  
                    self.loadCustomerSavedBillingEvents();
                }
            }
        });
    },

    customer_tabs_change: function(HTMLdata) {
        $('.ajxwrp').html(HTMLdata);
    },

    loadCustomerSavedBillingEvents: function() {
        var self = this;
        var $uid = $('input[name="hiduid"]').val();
        $("input[name=uid]").val($uid);
        
        $('a.remove_card').unbind('click');
        $('a.remove_card').click(function () {
        var value = $(this).attr("data-id");
        $.ajax({
            type: "POST",
            data: {
                'id': value
            },
            url: SITE_URL + 'customer/cardRemoveEvent/',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                self.makeCustomerPageRequest(5, $uid);
            }
        });
    });




        $('a.add-credit-card, a.edit_card').unbind('click');
        $('a.add-credit-card, a.edit_card').click(function() {
            $('.error_msg').empty();
            var value = $(this).attr("data-id");
            if (value == 0) {
                document.getElementById("edit-billForm").reset();
                $('#credit-card-edit-details input[name="id"]').val(value);
                $('#credit-card-edit-details h1').text('ADD CREDIT CARD');
                $('#credit-card-edit-details .add-address').text('Add');
                $('#credit-card-edit-details input[name="uid"]').val($uid);
                $('#credit-card-edit-details').show();
            } else if (value > 0) {

                $.ajax({
                    type: "POST",
                    data: {
                        'id': value,
                        "uid": $uid
                    },
                    dataType: 'json',
                    url: SITE_URL + 'customer/get_billing/',
                    success: function(e) {
                        $('#credit-card-edit-details input[name="id"]').val(e.Data[0].id);
                        $('#credit-card-edit-details input[name="uid"]').val($uid);
                        $('#credit-card-edit-details input[name="card_number"]').val(e.Data[0].card_number);
                        $('#credit-card-edit-details input[name="card_zip"]').val(e.Data[0].card_zip);
                        $('#credit-card-edit-details select[name="card_type"] option[value="' + e.Data[0].card_type + '"]').prop('selected', true);
                        $('#credit-card-edit-details select[name="expire_month"] option[value="' + e.Data[0].expire_month + '"]').prop('selected', true);
                        $('#credit-card-edit-details select[name="expire_year"] option:contains("' + e.Data[0].expire_year + '")').prop('selected', true);

                        $('#credit-card-edit-details .add-address').text('Save');

                        $('#credit-card-edit-details').show();
                    }
                });

            }
        });

        $("#credit-card-edit-details a.save-card-detail").unbind("click");
        $("#credit-card-edit-details a.save-card-detail").click(function() {
            var form = $("form#edit-billForm");
            
            var card_number = $('#credit-card-edit-details input[name="card_number"]').val();
            var card_zip = $('#credit-card-edit-details input[name="card_zip"]').val();
            var card_type = $('#credit-card-edit-details select[name="card_type"]').val();
            var expire_month = $('#credit-card-edit-details select[name="expire_month"]').val();
            var expire_year = $('#credit-card-edit-details select[name="expire_year"]').val();
            var card_cvv = $('#credit-card-edit-details input[name="card_cvv"]').val();

             function serializeRemove(thisArray, thisName) {
               "use strict";
                return thisArray.filter( function( item ) {
               return item.name != thisName;
                });
            };


            var card_data = form.serialize();


            
            $.ajax({
                type: "POST",
                data: card_data,
                dataType: 'json',
                url: SITE_URL + 'customer/cardAddEditEvent/',
                success: function(e) {
                    if(typeof(e)==="string" && isNaN(e)){
						
                        $('.error_msg').text('Please check your credit card details');
                        return false;
                    }
                    else{
                        //var dat=JSON.parse(e);
                        if( e !== null ){
                         if(e.data!=true && e.data!=null && isNaN(e.data)){
                            $('.error_msg').text('Please check your credit card details');
                            return false; 
                        }
                        }
                    }
                    $(form).find("input[type=text], textarea").val("");
                    $('#credit-card-edit-details').hide();
                    self.makeCustomerPageRequest(5, $uid);
                }
            });

        });

    },

    loadCustomerSavedAddressEvents: function() {
        var self = this;
        var $uid = $('input[name="hiduid"]').val();
        $("input[name=uid]").val($uid);

        if ($(".change-address").length > 0) {
            $(".change-address").click(function() {
                $(".popup-wrapper").hide();
                $("#add-new-address").show();
            });
        }

        $("a.save_address").unbind("click");
        $("a.save_address").click(function() {
            var form = $(this).parent("li").parent("ul").parent("form.add_address");
            var recipient = $(form).find("input[name=recipient]").val();
            var company = $(form).find("input[name=company]").val();
            var address1 = $(form).find("input[name=address1]").val();
            var address2 = $(form).find("input[name=address2]").val();
            var zip = $(form).find("input[name=zip]").val();
            var phone1 = $(form).find("input[name=phone1]").val();
            var phone2 = $(form).find("input[name=phone2]").val();
            var phone3 = $(form).find("input[name=phone3]").val();
            var crossStrt = $(form).find("input[name=cross_streets]").val(); 

             

            if (!recipient || recipient.length < 1) {
                alert("Recipient name can't be empty.");
                return false;
            }
            
            if(  !crossStrt || crossStrt.length < 1 ){
                alert("Cross Streets can't be empty.");
                return false;
            }
            
           


            if (!phone1 || phone1.length < 1 || phone1.length > 3 || !phone2 || phone2.length < 1 || phone2.length > 3 || !phone3 || phone3.length < 1 || phone3.length > 4) {
                alert("Phone number should be valid.");
                return false;
            }
            if (!zip || zip.length < 1) {
                alert("Zip Code can't be empty.");
                return false;
            }

            var data = form.serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'customer/savingAddress',
                success: function(e) {
                    $(form).find("input[type=text], textarea").val("");
                    $(".popup-wrapper").hide();
                    self.makeCustomerPageRequest(4, $uid);
                }
            });
        });

        $("a.remove_address").unbind("click");
        $("a.remove_address").click(function() {
            var address_id = $(this).attr("data-target");

            $.ajax({
                type: "POST",
                data: {
                    "address_id": address_id,
                    "uid": $uid
                },
                url: SITE_URL + 'customer/removeAddress',
                success: function(e) {
                    self.makeCustomerPageRequest(4, $uid);
                }
            });

        });

        $("a.edit_address").unbind("click");
        $("a.edit_address").click(function() {
            var address_id = $(this).attr("data-target");

            $.ajax({
                type: "POST",
                data: {
                    "address_id": address_id
                },
                url: SITE_URL + 'customer/editAddress',
                success: function(e) {
                    $("#edit-address div.add-new-address-inner").html(e);
                    $("#edit-address").show();
                    $("#edit-address div.add-new-address-inner .close-button").click(function() {
                        $("#edit-address").hide();
                    });

                    self.makeCustomerPageRequest(4, $uid);
                }
            });

        });
    },


    loadProfileEvents: function() {

        var self = this;

        var $uid = $('input[name="hiduid"]').val();
        

        $("input.savereview").unbind('click').click(function() {
            var form = $("form#user_review");
            var how_know = $("form#user_review [name=how_know]").val();
            var rating_order = $("form#user_review [name=rating_order]").val();
            var rating_customer_service = $("form#user_review [name=rating_customer_service]").val();
            var rating_food = $("form#user_review [name=rating_food]").val();
            var message = $("form#user_review [name=message]").val();

            

            var userdata = form.serialize();
            $.ajax({
                type: "POST",
                data: userdata,
                url: SITE_URL + 'customer/addUserReview/',
                success: function(data) {

                    if (data) {
                        alert("Your review saved. Thanks.");
                        document.getElementById("user_review").reset();
                        self.makeCustomerPageRequest(1, $uid);
                    }
                }
            });


        });

        $('.ajxwrp .edit').unbind('click').on('click', function(e) {
            e.preventDefault();
            $ptxt = '';
            $label = $(this).prev();
            $selc = $label.find('span');
            $ptxt = $selc.text();


            if ($label.find('a').length > 0) $label.find('a').removeAttr('href');

            if ($label.text().indexOf('Password') != -1) {
                $selc.html('<input type="password" name="editpass"  class="editxttransp" />');
            } else if ($label.text().indexOf('Email') != -1) {
                $selc.html('<input type="text" name="editemail" value="' + $ptxt + '" class="editxttransp" />');
            } else if ($label.text().indexOf('Name') != -1) {
                $selc.html('<input type="text" name="editname" value="' + $ptxt + '" class="editxttransp" />');
            } else if ($label.text().indexOf('Phone') != -1) {
                $selc.html('<input type="text" name="editphone" value="' + $ptxt + '" class="editxttransp" />');
            } else if ($label.text().indexOf('Company') != -1) {
                $selc.html('<input type="text" name="editcompany" value="' + $ptxt + '" class="editxttransp" />');
            }

            self.edit_account_info();

        });


    },

    edit_account_info: function() {

        var $uid = $('input[name="hiduid"]').val();

        $('input[name="editpass"],input[name="editemail"],input[name="editname"],input[name="editphone"],input[name="editcompany"]').change(function() {

            $name = $(this).attr('name');
            $value = $(this).val();


            switch ($name) {

                case 'editpass':
                    $(this).parent().html('xxxxxxxxx');
                    $(this).remove();
                    $.ajax({
                        type: "POST",
                        data: {
                            'password': $value,
                            'uid': $uid,
                            'type': 'password'
                        },
                        url: SITE_URL + 'customer/update_customer',
                        success: function(e) {
                            if (e.indexOf('Error') != -1) {
                                alert("Your password must be at least 6 characters long");
                            }
                        }
                    });

                    break;

                case 'editemail':
                    $(this).parent().html($value);
                    $(this).remove();
                    var $this = $(this);
                    $.ajax({
                        type: "POST",
                        data: {
                            'email': $value,
                            'uid': $uid,
                            'type': 'email'
                        },
                        url: SITE_URL + 'customer/update_customer',
                        success: function(e) {
                            if (e.indexOf('Error') != -1) {
                                alert("Invalid Email");
                            }
                            $this.after('<span></span>');
                        }
                    });
                    break;

                case 'editname':
                    $(this).parent().html($value);
                    $(this).remove();

                    $.ajax({
                        type: "POST",
                        data: {
                            'name': $value,
                            'uid': $uid,
                            'type': 'name'
                        },
                        url: SITE_URL + 'customer/update_customer',
                        success: function(e) {
                        }
                    });

                    break;

                case 'editphone':
                    $(this).parent().html($value);
                    $(this).remove();

                    $.ajax({
                        type: "POST",
                        data: {
                            'name': $value,
                            'uid': $uid,
                            'type': 'phone'
                        },
                        url: SITE_URL + 'customer/update_customer',
                        success: function(e) {
                        }
                    });

                    break;

                case 'editcompany':
                    $(this).parent().html($value);
                    $(this).remove();

                    $.ajax({
                        type: "POST",
                        data: {
                            'name': $value,
                            'uid': $uid,
                            'type': 'company'
                        },
                        url: SITE_URL + 'customer/update_customer',
                        success: function(e) {
                        }
                    });

                    break;
            }

        });

    },

	refresh_dropdown : function(){
            var self = this;
            var user_id = $("input.user_id").val();
            data = {
                    "user_id": user_id,
                    "address_type": 'delivery'
                };

                 $.ajax({
                    type: "POST",
                    data: data,
                    url: SITE_URL + 'neworder/getAddressForSelection/',
                    success: function(e) {
                        $("div.address-container").html(e);
                        self.loadNewOrderEvents();
                    }
                });

               
        },

    getCookie_forcalendar : function(){

            var now_or_specific = $("input.now_or_specific:checked").val();
            var date_val = $('.hasDatepicker').val();
            var time_val = $('select[name=time]').val();
            admin_ha.common.setCookie("now_or_specific",now_or_specific,1);
            admin_ha.common.setCookie("date_val",date_val,1);
            admin_ha.common.setCookie("time_val",time_val,1);

    },
    setCookie_forcalendar : function(){

            var now_or_specific = admin_ha.common.getCookie('now_or_specific');
            var date_val = admin_ha.common.getCookie('date_val');
            var time_val = admin_ha.common.getCookie('time_val');

            if( now_or_specific === 'now' && now_or_specific != 'undefined')
                $('#radio1').prop("checked", true);
            if( now_or_specific === 'specific' && now_or_specific != 'undefined')
                $('#radio2').prop("checked", true);
            if( time_val != 'undefined')
                $('select.select').val(time_val);

    },
    loadNewOrderEvents: function() {
        var self = this;
        self.add_product_descriptor_to_cart();
        $("a.add-address").unbind("click").on('click', function() {
            $("div#add-new-address").show();
        });

        $("a.save_address").unbind("click");
        $("a.save_address").click(function() {
            var address_type = $("input[name=address_type]").val();

            if (address_type == "delivery") {
                var form = $(this).parent("li").parent("ul").parent("form.add_address");
                
                var recipient = $(form).find("input[name=recipient]").val();
                var company = $(form).find("input[name=company]").val();
                var address1 = $(form).find("input[name=address1]").val();
                var address2 = $(form).find("input[name=address2]").val();
                var zip = $(form).find("input[name=zip]").val();
                var phone1 = $(form).find("input[name=phone1]").val();
                var phone2 = $(form).find("input[name=phone2]").val();
                var phone3 = $(form).find("input[name=phone3]").val();
                var crossStrt = $(form).find("input[name=cross_streets]").val();
                
                
                if (!recipient || recipient.length < 1) {
                    alert("Recipient name can't be empty.");
                    return false;
                }
                
                if(  !crossStrt || crossStrt.length < 1 ){
                    alert("Cross Streets can't be empty.");
                    return false;
                }
                
                if (!phone1 || phone1.length < 1 || phone1.length > 3 || !phone2 || phone2.length < 1 || phone2.length > 3 || !phone3 || phone3.length < 1 || phone3.length > 4) {
                    alert("Phone number should be valid.");
                    return false;
                }

                if (!zip) {
                    alert("Zip Code required! ");
                    return false;
                }  
				
				$("input[name=uid]").each(function(){ 
					if($(this).val() != ""){
						$("input[name=uid]").val($(this).val());
					}
				})
				
                var data = form.serialize();

                $.ajax({
                    type: "POST",
                    data: data,
                    url: SITE_URL + 'neworder/savingAddress',
                    success: function(e) {
                        if( !$('.place-order-coustomer-info').next().hasClass('addrs-response'))
                            $('.place-order-coustomer-info').after('<h2 class="addrs-response">Address has been saved succesfully</h2>');
                        self.refresh_dropdown();
                        $('#edit-address').hide('fast');
                        $('.close-button').trigger('click');
                    }
                });
            }
        });
		
        $("a.edit-address").unbind("click");
        $("a.edit-address").click(function() {
            var address_type = $("input[name=address_type]").val();

            if (address_type == "delivery") {
                var address_id = $("select[name=user_address]").val();
                if (address_id) {
                    $.ajax({
                        type: "POST",
                        data: {
                            "address_id": address_id
                        },
                        url: SITE_URL + 'neworder/editAddress',
                        success: function(e) {
                            $("#edit-address div.add-new-address-inner").html(e);
                            $("#edit-address").show();
                            $("#edit-address div.add-new-address-inner .close-button").click(function() {
                                $("#edit-address").hide();
                            });
                            self.loadNewOrderEvents();
                        }
                    });
                } else {
                    alert("Select Address first.");
                    return;
                }
            }
        });
		$('.hasDatepicker').change(function(){ 

                self.getCookie_forcalendar();
        });

        $('select.select').change(function(){

            self.getCookie_forcalendar();
        });
        $("a.add-to-cart").unbind("click");
        $("a.add-to-cart").click(function() 
        {
            var data_id = $(this).attr("data-id");
            var data_type = $(this).attr("data-type");
            var user_id = $("input.user_id").val();
            var qty = $(this).prev().find("input[name=quantity]").val();
            if (!qty) qty = 1;
            var toast = 0;

            if (data_type == "product") 
            {

                var prod_descriptor = self.product_extras.Data[data_id];

                if (typeof(prod_descriptor) == "undefined")
                {} 
                else 
                {  
                    $("#product-descriptor ul.from-holder").html("");

                    for (i = 0; i < prod_descriptor.length; i++) 
                    {
                        var prod_desc = prod_descriptor[i];

                        var options = "";

                        for (j = 0; j < prod_desc.extra.length; j++) {

                            var option = prod_desc.extra[j];

                            options += "<option value='" + option.id + "'>" + option.name + "</option>";
                        }

                        $("#product-descriptor .title-holder h1").html(prod_desc.desc);

                        var content = '<li><span class="text-box-holder"><select name="extra_id" class="extra_id listextra">' + options + '</select></span> </li><input type="hidden" name="descriptor_id" value="' + prod_desc.id + '"/>';


                        $("#product-descriptor ul.from-holder").append(content);
                    }

                    var hidden_fields = '<input type="hidden" name="data_id" value="' + data_id + '" />';
                    hidden_fields += '<input type="hidden" name="data_type" value="' + data_type + '" />';
                    hidden_fields += '<input type="hidden" name="user_id" value="' + user_id + '" />';
                    hidden_fields += '<input type="hidden" name="qty" value="' + qty + '" />';
                    hidden_fields += '<input type="hidden" name="toast" value="' + toast + '" />';


                    $("#product-descriptor form .hidden_fields").html(hidden_fields);
                    $("#product-descriptor").show();
                    return false;
                }
            }


            var data = {
                "data_id": data_id,
                "data_type": data_type,
                "user_id": user_id,
                "qty": qty,
                "toast": 0
            };

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'neworder/addItemToCart',
                success: function(e) {
                    $('#sandwich-gallery').hide();
                    $("div.shopping-cart").html(e);
                    self.enable_spin_arrows();
                    self.remove_cart();
                    self.updateCartCouponDetails();
                }
            });

        });

        $("a.salad-listing").unbind('click').on('click', function(e) 
        {
            pdata = $(this).data();
            //console.log("pdata ",pdata);return;
            $("input[name='itemQuatity']").val("01");
            
            
            $("#salad_popup").find('#hidden_sandwich_id').val(pdata.id);
            $("#salad_popup").find('#hidden_sandwich_price').val(pdata.product_price);
            $("#salad_popup").find('h4').text(pdata.product_name);
            //$($("#salad_popup p")[0]).text(pdata.description);
            $($("#salad_popup p")[0]).html($.parseHTML(pdata.description));
            if(pdata.add_modifier != 1 || pdata.add_modifier == 0){
                $("#salad_popup").find('.add_modifier').hide();
            }else{
                $("#salad_popup").find('.add_modifier').show();
                $(".salad-protien--sec").find('div').remove();
                var chklen = pdata.modifier_options.length;
                var html = '';
                $.each( pdata.modifier_options, function( key, value ) {
                    //console.log("value", value);
                  /*$.each( value, function( ke, val ) {*/
                    //console.log(ke +':'+ val);
                    html += '<div class="checkbox-holder">';
                    html += '<input id="check_p_'+key+'" type="checkbox" class="menucheck savetousertoastnew" name="checkoptions" value="'+value.option+','+value.price+'">';
                    html += '<label for="check_p_'+key+'">'+value.option + ' ($' + value.price + ')</label>';
                    html += '</div>';   
                  /*});*/
                });

                $(".salad-protien--sec").append(html);
            }
            if(pdata.spcl_instr != 1 || pdata.spcl_instr == 0){
                $("#salad_popup").find('.spcl_instruction').hide();
            }else{
                $("#salad_popup").find('.spcl_instruction').show();
            }
            
            $("#salad_popup").find('.modifier_desc').html(pdata.modifier_desc);
            $("#salad_popup").show();
            $('.popup_spinner .left_spinner').unbind('click').on('click', function() 
            {
                input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            $('.popup_spinner .right_spinner').unbind('click').on('click', function() 
            {
                input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });
            
            $(".add-salad-to-cart").unbind('click').on('click', function()
            {

                var atLeastOneIsChecked = $('input:checkbox').is(':checked');
                var chkboxlen = $(":checkbox:checked").length;
                //console.log($('input[name="checkoptions"]:checked').serialize());
                var clickedItems = [];
                i = 0;
                $('input[name="checkoptions"]:checked').each(function() {
                   clickedItems[i++] = this.value;
                });
        
                if(pdata.add_modifier == 1 && pdata.modifier_isoptional == 'no' && atLeastOneIsChecked==false){
                    alert('Please select one '+pdata.modifier_desc);
                }else if(pdata.add_modifier == 1 && pdata.modifier_is_single == 'yes' && chkboxlen > 1){
                    alert('You can select only one item');
                }else{
                    qty = $('input[name="itemQuatity"]').val();
                    $(".hidden-values").find('input').remove();
                    var hidden_fields = '<input type="hidden" name="data_id" value="' + pdata.id + '" />';
                    hidden_fields += '<input type="hidden" name="data_type" value="' + pdata.product + '" />';
                    hidden_fields += '<input type="hidden" name="user_id" value="' + pdata.uid + '" />';
                    hidden_fields += '<input type="hidden" name="qty" value="' + qty + '" />';
                    hidden_fields += '<input type="hidden" name="clickedItems" value="' + clickedItems + '" />';
                    hidden_fields += '<input type="hidden" name="toast" value="0" />';
                    $('.hidden-values').append(hidden_fields);

                    var data = $(this).parent().parent().serialize();
                    
                    var data_id = pdata.id;
                    var data_type = pdata.type;
                    var user_id = pdata.uid;
                    var qty = $('input[name="itemQuatity"]').val();
                    if (!qty) qty = 1;
                    var toast = 0;

                    if (data_type == "product") 
                    {
                        var prod_descriptor = self.product_extras.Data[data_id];

                        if (typeof(prod_descriptor) == "undefined")
                        {} 
                        else 
                        {  
                            $("#product-descriptor ul.from-holder").html("");

                            for (i = 0; i < prod_descriptor.length; i++) 
                            {
                                var prod_desc = prod_descriptor[i];
                                var options = "";
                                for (j = 0; j < prod_desc.extra.length; j++) 
                                {
                                    var option = prod_desc.extra[j];
                                    options += "<option value='" + option.id + "'>" + option.name + "</option>";
                                }

                                $("#product-descriptor .title-holder h1").html(prod_desc.desc);

                                var content = '<li><span class="text-box-holder"><select name="extra_id" class="extra_id listextra">' + options + '</select></span> </li><input type="hidden" name="descriptor_id" value="' + prod_desc.id + '"/>';


                                $("#product-descriptor ul.from-holder").append(content);
                            }

                            var hidden_fields = '<input type="hidden" name="data_id" value="' + data_id + '" />';
                            hidden_fields += '<input type="hidden" name="data_type" value="' + data_type + '" />';
                            hidden_fields += '<input type="hidden" name="user_id" value="' + user_id + '" />';
                            hidden_fields += '<input type="hidden" name="qty" value="' + qty + '" />';
                            hidden_fields += '<input type="hidden" name="toast" value="' + toast + '" />';


                            $("#product-descriptor form .hidden_fields").html(hidden_fields);
                            $("#product-descriptor").show();
                            return false;
                        }
                    }


                    // var data = {
                    //     "data_id": data_id,
                    //     "data_type": data_type,
                    //     "user_id": user_id,
                    //     "qty": qty,
                    //     "toast": 0
                    // };

                    $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'neworder/addItemToCart',
                        success: function(e) {
                            $("#salad_popup").hide();
                            $('#sandwich-gallery').hide();
                            $("div.shopping-cart").html(e);
                            self.enable_spin_arrows();
                            self.remove_cart();
                            self.updateCartCouponDetails();
                        }
                    });
                }
                
            });
            
            $("#salad_popup").find('.close-button').click(function(){
                $(".salad-protien--sec").find('div').remove();
                $("#salad_popup").hide();
            });
        });
        
        $("a.view-sandwich").unbind('click').on('click', function(e)
        {
            var parent = $(this).parent().parent();
            var pdata = parent.data();
            src = $("#sandImg_"+pdata.id).val();

            $ptxt = ""
            breadTxt = pdata.bread;
            breadTxt = breadTxt.trim();
            breadTxt = breadTxt.replace(/##/g, " ");
            $ptxt = breadTxt;

            $('.popup_spinner .left_spinner').unbind('click').on('click', function() 
            {
                input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            $('.popup_spinner .right_spinner').unbind('click').on('click', function() 
            {
                input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            if (pdata.protien != "") 
            {
                protiens = pdata.protien.replace(/ /g, ', ')
                protiens = protiens.trim();
                protiens = protiens.replace(/##/g, " ");
                if ($ptxt) $dl = ', ';
                else $dl = '';
                $ptxt += $dl + protiens;
            }
            if (pdata.cheese != "") 
            {
                chesses = pdata.cheese.replace(/ /g, ', ')
                chesses = chesses.trim();
                chesses = chesses.replace(/##/g, " ");
                $ptxt += chesses;
            }
            if (pdata.topping != "") 
            {
                toppings = pdata.topping.replace(/ /g, ', ')
                toppings = toppings.trim();
                toppings = toppings.replace(/##/g, " ");
                if ($ptxt) $dl = ', ';
                else $dl = '';
                $ptxt += toppings;
            }
            if (pdata.cond != "") 
            {
                conds = pdata.cond.replace(/ /g, ', ')
                conds = conds.trim();
                conds = conds.replace(/##/g, " ");
                $ptxt += conds;
            }

            txt = $(this).text();
            if ($(this).hasClass("view-sandwich") )
            {
                txt = 'VIEW';
            }
            if (txt === 'VIEW') 
            {
                if (pdata.flag == 0) 
                {
                    $('.flagthis').css('text-decoration', 'underline');
                } 
                else 
                {
                    $('.flagthis').css('text-decoration', 'none');
                }

                $("#sandwiches-popup").find('.savetousertoastnew').prop('checked', false);
                $("#sandwiches-popup").find('#hidden_uid').val(pdata.id);
                $("#sandwiches-popup").find('#hidden_sandwich_id').val(pdata.id);

                if ((pdata.toast == 1)) 
                {
                    $("#sandwiches-popup").find('.savetousertoastnew').prop('checked', true);
                }

                $("#sandwiches-popup").find('.text-box').val('01');


                image = $("#sandwiches-popup").find('img:nth-child(2)');
                $("#sandwiches-popup").find('h1').text(pdata.name);

                $.ajax({
                    type: "POST",
                    url: SITE_URL + 'newOrder/getCurrentprice',
                    data: {'pdata': pdata},
                    success: function(data) {
                        $("#sandwiches-popup").find('h2').text('$' + data);
                    }
                });

                $($("#sandwiches-popup p")[0]).text(pdata.sandwich_desc);
                $(image).attr({
                    'src': src,
                    'width': '350',
                    'height': '194'
                });
                $("#sandwiches-popup .add-to-cart").text($(parent).attr('rel'));
                $("#sandwiches-popup .edit-sandwich-popup").attr('rel', pdata.id);

                sType = 'SS';
                $("#sandwiches-popup").show();
                var toast_sandwich = pdata.toast;
                $(".menucheck").unbind('change').on('change', function(e)
                {
                   toast_sandwich = $('.menucheck').is(':checked')?1:0;
                   if(sType == "SS")
                   {
                        var sandwich_ = $('li[data-id="' + pdata.id + '"]');
                        $.ajax({
                            type: "POST",
                            data: {
                                "toast": toast_sandwich,
                                "item_id": pdata.id
                            },
                            url: SITE_URL + 'newOrder/updateToastmenu',
                            dataType: "json",
                            success: function(e) 
                            {
                                $("#toastID_"+pdata.id).data('toast',toast_sandwich);
                            }

                        });
                   }
                 
                });

                $("#sandwiches-popup").find('.add-to-cart').unbind('click').on('click', function(e) 
                {
                    var toast = 0;
                    if ($(".menucheck").is(":checked")) 
                    {
                        toast = 1;
                    }
                    e.preventDefault();
                    var data_id = pdata.id;
                    var data_type = pdata.type;
                    var user_id = pdata.userid;
                    var qty = $('input[name="itemQuty"]').val();
                    if (!qty) qty = 1;
                    
                    var data = {
                        "data_id": data_id,
                        "data_type": data_type,
                        "user_id": user_id,
                        "qty": qty,
                        "toast": toast
                    };

                   $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'neworder/addItemToCart',
                        success: function(e) {
                            $('#sandwich-gallery').hide();
                            $("#sandwiches-popup").hide();
                            $("div.shopping-cart").html(e);
                            self.enable_spin_arrows();
                            self.remove_cart();
                            self.updateCartCouponDetails();
                        }
                    });
                    
                    
                    
                });
            }
        });

    },


    add_product_descriptor_to_cart: function() {

        var self = this;

        $("#product-descriptor input.add_desc").click(function() {

            var data = $(this).parent().serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'neworder/addItemToCart',
                beforeSend: function() {
                },
                complete: function() {
                },
                success: function(e) {
                    $(".popup-wrapper").hide();
                    $("div.shopping-cart").html(e);
                    self.enable_spin_arrows();
                    self.remove_cart();
                }
            });
        });

    },

    update_cart_arraow_events: function(obj) {

        var self = this;

        var qty = $(obj).parent().find("input.qty").val();
        var data_id = $(obj).parent().find("input[name=data_id]").val();
        var data_type = $(obj).parent().find("input[name=data_type]").val();
        var order_item_id = $(obj).parent().find("input[name=data_order_id]").val();
        var user_id = $("input.user_id").val();

        var data = {
            "qty": qty,
            "data_id": data_id,
            "data_type": data_type,
            "user_id": user_id,
            "order_item_id": order_item_id
        };

        $.ajax({
            type: "POST",
            data: data,
            url: SITE_URL + 'neworder/updateItemToCart/',
            success: function(e) {
                $("div.shopping-cart").html(e);
                self.enable_spin_arrows();
                self.remove_cart();
                self.updateCartCouponDetails();
            }
        });

    },

    updateCartCouponDetails: function() {
        var self = this;
        $("input.coupon_code").keyup(function(event) {
            if (event.keyCode == 13) {
                var user_id = $("input.user_id").val();
                var coupon_code = $(this).val();

                var data = {
                    "user_id": user_id,
                    "code": coupon_code
                };

                if (coupon_code) {
                    $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'neworder/updateCartCouponDetails/',
                        success: function(e) {
                            $("div.shopping-cart").html(e);
                            self.enable_spin_arrows();
                            self.remove_cart();
                            self.updateCartCouponDetails();
                        }
                    });

                } else {
                    alert("Please enter coupon code.");
                    return false;
                }
            }
        });
		
        $("a.apply_coupon").unbind("click").click(function(event) {

            var coupon_code = $("input.coupon_code").val();
            if (coupon_code) {
                var user_id = $("input.user_id").val();

                var data = {
                    "user_id": user_id,
                    "code": coupon_code
                };

                if (coupon_code) {
                    $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'neworder/updateCartCouponDetails/',
                        success: function(e) {
                            
                            $("div.shopping-cart").html(e);
                            self.enable_spin_arrows();
                            self.remove_cart();
                            self.updateCartCouponDetails();
                        }
                    });

                } else {
                    alert("Please enter coupon code.");
                    return false;
                }
            } else {
                alert("Please enter coupon code.");
                return false;
            }
        });


		        
        $("a.apply_manual_discount").unbind("click").click(function(event) {
		
			
            var manual_discount = $("input.manual_discount").val();

            if (manual_discount) {
                var user_id = $("input.user_id").val();


                 
                var data = $("form#cartdetails").serializeArray();
                data.push({
                    name: 'user_id',
                    value: user_id
                });

                if (manual_discount) {
                    $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'neworder/updateCartDetails/',
                        success: function(e) {
                             
                            $("div.shopping-cart").html(e);
                            self.enable_spin_arrows();
                            self.remove_cart();
                            self.updateCartCouponDetails();
                        }
                    });

                } else {
                    alert("Please enter Manual discount.");
                    return false;
                }
            }
        });

        $("input.manual_discount").keyup(function(event) {
            if (event.keyCode == 13) {
                var user_id = $("input.user_id").val();
                var coupon_code = $(this).val();

               
                var data = $("form#cartdetails").serializeArray();
                data.push({
                    name: 'user_id',
                    value: user_id
                });

                if (coupon_code) {
                    $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'neworder/updateCartDetails/',
                        success: function(e) {
                           
                            $("div.shopping-cart").html(e);
                            self.enable_spin_arrows();
                            self.remove_cart();
                            self.updateCartCouponDetails();
                        }
                    });

                } else {
                    alert("Please enter Manual discount.");
                    return false;
                }
            }
        });

        $("select[name=tips_selected]").unbind("click");
        $("select[name=tips_selected]").change(function(event) {
            var user_id = $("input.user_id").val();
            var data = $("form#cartdetails").serializeArray();
            data.push({
                name: 'user_id',
                value: user_id
            });


            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'neworder/updateCartDetails/',
                success: function(e) {
                     
                    $("div.shopping-cart").html(e);
                    self.enable_spin_arrows();
                    self.remove_cart();
                    self.updateCartCouponDetails();
                }
            });


             
        });

    },

    remove_cart: function() {
        $("a.remove_cart_item").click(function() {
            var qty = $(this).parent().parent().find("input.qty").val();
            var data_id = $(this).parent().parent().find("input[name=data_id]").val();
            var data_order_id = $(this).parent().parent().find("input[name=data_order_id]").val();
            var data_type = $(this).parent().parent().find("input[name=data_type]").val();
            var user_id = $("input.user_id").val();

            var data = {
                "qty": qty,
                "data_id": data_id,
                "data_type": data_type,
                "user_id": user_id,
                "order_item_id": data_order_id
            };

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'neworder/removeCartItem',
                success: function(e) {
                    window.location.reload();
                }
            });

        });
    },

    show_date_picker: function() {
        $('.date-icon,.calendar').prev().datepicker({
            "dateFormat": "d M yy"
        });
        $('.date-icon,.calendar').unbind('click').on('click', function() {
            $(this).prev().focus();
            return false;
        });
    },

    enable_spin_arrows: function() {

        var self = this;

        $('.arrow-left,.arrow-right').unbind('click').on('click', function(e) {
            e.preventDefault();
            if ($(e.target).attr('class').includes('arrow-left')) {
                $val = $(this).next().val();
                if ($val > 1) {
                    $val--;
                } else {
                    $val = 1;
                }
                $(this).next().val($val);


                if ($(this).data("event") == "noupdate")
                    return;

                 
                self.update_cart_arraow_events(this);

            }

            if ($(e.target).attr('class') == 'arrow-right') {
                $valinc = $(this).prev().val();
                if ($valinc < 999) {
                    $valinc++;
                } else {
                    $valinc = 1;
                }
                $(this).prev().val($valinc);

                if ($(this).data("event") == "noupdate")
                    return;

                self.update_cart_arraow_events(this);
            }
        });

    },


    enable_label_selections: function() {
        $('span').on('click', function() {
            var chk = $(this).prev().find('input[type="radio"],input[type="checkbox"]');
            if (chk) {
                chk.trigger('click');
            }
            var chk1 = $(this).prev().prev();
            if (chk1) {
                chk1.trigger('click');
            }
        });

    },


    sandwich_gallery_search: function() {
        var self = this;
        var Array = [];
        var self = this;
        var $sel;
        $('.leftside-bar input[type="checkbox"]').unbind('click').on('click', function() {

            $('.adminGalleryList tr').show();
            $txt = $(this).val().toUpperCase();

            id_index = $(this).attr('id');
            id_index = id_index.split('-');


            if ($(this).prop('checked') == true) {
                if ($.inArray($txt, Array) == -1) {
                    Array.push($txt);
                }
            } else {
                if ($.inArray($txt, Array) != -1) {
                    Array.splice($.inArray($txt, Array), 1);
                }
            }

            $sel = '';

            if (id_index[1] == 1) {

                Array.forEach(function(e, i) {

                    if (i > 0) {
                        $del = ',';
                    } else {
                        $del = '';
                    }
                    $sel += $del + 'tr[data-bread~="' + e + '"]';

                });

            } else if (id_index[1] == 2) {

                Array.forEach(function(e, i) {

                    if (i > 0) {
                        $del = ',';
                    } else {
                        $del = '';
                    }
                    $sel += $del + 'tr[data-protien~="' + e + '"]';

                });

            } else if (id_index[1] == 3) {

                Array.forEach(function(e, i) {

                    if (i > 0) {
                        $del = ',';
                    } else {
                        $del = '';
                    }
                    $sel += $del + 'tr[data-cheese~="' + e + '"]';

                });

            } else if (id_index[1] == 4) {

                Array.forEach(function(e, i) {

                    if (i > 0) {
                        $del = ',';
                    } else {
                        $del = '';
                    }
                    $sel += $del + 'tr[data-topping~="' + e + '"]';

                });

            } else if (id_index[1] == 5) {


                Array.forEach(function(e, i) {

                    if (i > 0) {
                        $del = ',';
                    } else {
                        $del = '';
                    }
                    $sel += $del + 'tr[data-cond~="' + e + '"]';

                });

            }

            if ($sel == '') $('.adminGalleryList tr').show();
            else $('.adminGalleryList tr').not($sel).not('.static').hide();

        });

        $('input[name="search_ingallery"]').unbind('keyup').bind('keyup', function() {

            
            newhrf = '';
            anchor = $($(this).next());
            newhrf = SITE_URL + 'sandwich/index/?search=' + $(this).val();
            anchor.attr('href', newhrf);

        });

    },

    toggle_gallery_public: function() {

        $('.adminGalleryList tr').find('input[type="checkbox"]').unbind('click').on('click', function() {
            var $parent, state;
            state = 0;
            if ($(this).prop('checked') == true) {
                state = 1;
            }
            $parent = $(this).parent().parent().parent();
            $parentData = $parent.data();

            $.ajax({
                type: "POST",
                data: {
                    'public': state,
                    'id': $parentData.id
                },
                url: SITE_URL + 'sandwich/set_public/',
                success: function(e) {   }
            });

        });

    },

    delete_gallery_item: function() {

        $('.sandwich-gallery' ).on( 'click', '.remove_gallery_item', function (e) {
            e.preventDefault();
            $parent = $(this).parent().parent();
            $parentData = $parent.data();

            $.ajax({
                type: "POST",
                data: {
                    'id': $parentData.id
                },
                url: SITE_URL + 'sandwich/delete_gallery/',
                success: function(e) {
                }
            });

            $parent.remove();

        });

    },

    flagg_gallery_item: function() {
        var self = this;
        $('.flag').unbind('click').on('click', function(e) {
            e.preventDefault();
            $txt = $(this).text();
            $txt
            $(this).text('');
            self.set_flag_state(0, $(this));
            
        });

    },


    set_flag_state: function(state, obj) {

        $parent = obj.parent().parent();
        $data = $parent.data();

        $.ajax({
            type: "POST",
            data: {
                'id': $data.id,
                'status': state
            },
            url: SITE_URL + 'sandwich/set_flag/',
            success: function(e) {
            }
        });
    },

    edit_customer_info: function() {

        $("a.edit_info").click(function() {
            $(this).hide();
            $("div.address-details input.edit_customer_info, div.address-details a.save_info").show();
        });
        $("a.save_info").click(function() {
           
            var uid = $("input.user_id").val();
            var first_name = $("input[name=first_name]").val();
            var last_name = $("input[name=last_name]").val();
            var username = $("input[name=username]").val();
            var phone = $("input[name=phone]").val();


            if (!first_name) {
                alert("Please fill the First Name");
                return false;
            }

            if (!last_name) {
                alert("Please fill the Last Name");
                return false;
            }

            var user_data = {
                "first_name": first_name,
                "last_name": last_name,
                "uid": uid,
                'username': username,
                "phone": phone
            };

            if (username) {
            	 $(this).hide();
                 $("div.address-details input.edit_customer_info, div.address-details a.save_info").hide();

                $.post(SITE_URL + "accounts/cmsUsernameExists", user_data,
                    function(data, status) {
                        data = JSON.parse(data);

                        if (data.state) {
                            alert("Username / Email already exists.");
                            return false;
                        } else {

                            $(this).hide();
                            $("div.address-details input.edit_customer_info, div.address-details a.save_info").hide();


                            $.ajax({
                                type: "POST",
                                data: user_data,
                                url: SITE_URL + 'customer/saveCustomerInfo/',
                                success: function(e) {
                                   // console.log(e);
                                    window.location.reload();
                                }
                            });

                        }
                    });
            } else {
                $(this).hide();
                $("div.address-details input.edit_customer_info, div.address-details a.save_info").hide();

                var user_data = {
                    "first_name": first_name,
                    "last_name": last_name,
                    "uid": uid,
                    "phone": phone
                };

                $.ajax({
                    type: "POST",
                    data: user_data,
                    url: SITE_URL + 'customer/saveCustomerInfo/',
                    success: function(e) {
                        window.location.reload();
                    }
                });
            }
        });

    },

    customer_notes: function() {

        var self = this;

        $("a.add-new-note").unbind("click").click(function() {
            $("div.customer-notes-list").slideUp("slow", function() {
                $("div.add-customer-note").slideDown("slow");
            });
        });

        $("a.cancel_customer_note").unbind("click").click(function() {
            $("div.add-customer-note").slideUp("slow", function() {
                $("div.customer-notes-list").slideDown("slow");
                $(this).parent().find("textarea").val("");
            });
        });

        $("a.add_customer_note").unbind("click").click(function() {
            var note = $(this).parent().find("textarea").val();
            var user_id = $("input.user_id").val();
            if (note.length) {
                var data = {
                    "user_id": user_id,
                    "notes": note
                };
                $.ajax({
                    type: "POST",
                    data: data,
                    url: SITE_URL + 'customer/addCustomerNote/',
                    success: function(e) {
                        $("div.customer-notes-list").html(e);
                        self.customer_notes();
                        $("div.add-customer-note").slideUp("slow", function() {
                            $("div.customer-notes-list").slideDown("slow");
                            $(this).parent().find("textarea").val("");
                        });
                    }
                });
                
            } else {
                alert("Please add notes first.");
                return false;
            }
        });

        $("a.remove_customer_note").unbind("click").click(function() {
            var data_id = $(this).attr("data-id");
            if (data_id) {
                var data = {
                    "id": data_id
                };
                $(this).parent().remove();
                $.ajax({
                    type: "POST",
                    data: data,
                    url: SITE_URL + 'customer/removeCustomerNote/',
                    success: function(e) {
                    }
                });
            }
        });
    },

    getStore_id_using_zip: function(zip) {
        var self = this;
        var store_id;
        $.ajax({
            type: "POST",
            data: {
                'zip': zip
            },
            url: SITE_URL + 'neworder/getStoreFomZip/',
            async: false,
            dataType: 'json',
            success: function(data) {
                if (data[0]) {
                    store_id = data[0].store_id;
                } else {
                    store_id = 0;
                }
            }
        });

        return store_id;
    },

    place_order_events: function() {

        var self = this;

        $("label.address_type_label").click(function() {
            var user_id = $("input.user_id").val();
            var radioid = $(this).attr("for");
            $(this).prev().prop("checked", true);
            var address_type = $(this).prev().val();

            if (address_type) {
                var data = {
                    "user_id": user_id,
                    "address_type": address_type
                };

                $.ajax({
                    type: "POST",
                    data: data,
                    url: SITE_URL + 'neworder/getAddressForSelection/',
                    success: function(e) {
                        $("div.address-container").html(e);
                        self.loadNewOrderEvents();
                    }
                });
            }

        });

        $("label.time_type_label").click(function() {
            $(this).prev().prop("checked", true);
			self.getCookie_forcalendar();
        });


        $("a.place-order").unbind("click").click(function(e) {
            e.preventDefault();

            var user_id = $("input.user_id").val();
            var delivery = -1;
            var order_string = $('input[name="order_string"]').val();
            if (typeof order_string == "undefined" || !order_string) {
                alert("Please check your cart items. It can't be empty.");
                return false;
            }

            var address_type = $('input[name="address_type"]').val();
            if (typeof address_type == "undefined" || !address_type) {
                alert("Please select address type.");
                return false;
            } else {
                if (address_type == "delivery") delivery = 1;
                if (address_type == "pickup") delivery = 0;
            }

            var address_id = $("select.user_address").val();
            if (typeof address_id == "undefined" || !address_id) {
                alert("Please select address.");
                return false;
            }


            var now_or_specific = $("input.now_or_specific:checked").val()
            var date_val = $('.hasDatepicker').val();
            var time_val = $('select[name=time]').val();

            if (typeof now_or_specific == "undefined" || !now_or_specific) {
                alert("Please select time and date.");
                return false;
            }

            if (now_or_specific == "specific") {

                if (typeof date_val == "undefined" || !date_val) {
                    alert("Please select date for order.");
                    return false;
                }
                if (typeof time_val == "undefined" || !time_val) {
                    alert("Please select time for order.");
                    return false;
                }
                var time_only = time_val.replace(" PM", ":00");
                time_only = time_val.replace(" AM", ":00");
                var date_combined = date_val + " " + time_only;

                var select_date = new Date(date_combined);
                var current_date = new Date();

                if (select_date < current_date) {
                    alert("Select future date time.");
                    return false;
                }
            } else {
                date = 0;
                time = 0;
            }

            var coupon_code = $('input[name="coupon_id"]').val();
            var tips = $('input[name="tips"]').val();
            var tax = $('#taxvalue').text();
            var tax = Number( tax.replace(/[^0-9\.-]+/g,"") );

            var grand_total = $('input[name="grand_total"]').val();
            
            var sub_total = $('#addToCart_Subtotal').text(); 
            var sub_total = Number( sub_total.replace(/[^0-9\.-]+/g,"") );


            var credit_card_id = $('input[name="billing_card"]').val();
            var off_amount = $('input[name="off_amount"]').val();
            var off_type = $('input[name="off_type"]').val();
            var manual_discount = $('input[name="manual_discount"]').val();
			var payment_Form = new Array();
			if($("select.billing-select").val() == "no"){
				
				credit_card_id = "no";
				payment_Form[1] = {"value": ""};
				payment_Form[2] = {"value": ""};
				payment_Form[5] = {"value": ""};
				payment_Form[6] = {"value": ""};
								
			}else{
			
				payment_Form = $('#_payment_form').serializeArray();
				if (payment_Form[1].value == "" && payment_Form[2].value == "" && payment_Form[5].value == "" && payment_Form[6].value == "") {
					alert("Please select billing");
					return false;
				}
			}

            var address = $("select.user_address option:selected").text();
            address = address.split(",");
            var zip = (address[address.length - 1]).trim();

            if (address_type == "delivery") {
                var store_id = self.getStore_id_using_zip(zip);
            }
            if (address_type == "pickup") {
                var store_id = address_id;
            }
            var _card_number_auth = $('input[name="_card_number_auth"]').val();
            var _expiry_month_auth = $('input[name="_expiry_month_auth"]').val();
            var _expiry_year_auth = $('input[name="_expiry_year_auth"]').val();
            var data = {
                'store_id': store_id,
                'address_id': address_id,
                'order_item_id': order_string,
                'total': grand_total,
                'tax': tax,
                'delivery': delivery,
                'date': date_val,
                'time': time_val,
                'now_or_specific': now_or_specific,
                "credit_card_id": credit_card_id,
                'coupon_code': coupon_code,
                'tip': tips,
                'sub_total': sub_total,
                "user_id": user_id,
                "by_admin": 1,
                "off_type": off_type,
                "off_amount": off_amount,
                "manual_discount": manual_discount,
                'credit_card': payment_Form[1].value,
                'cvv': payment_Form[2].value,
                'exp_mth': payment_Form[5].value,
                'exp_yr': payment_Form[6].value,
                '_card_number_auth': _card_number_auth,
                '_expiry_month_auth': _expiry_month_auth,
                '_expiry_year_auth': _expiry_year_auth
            };


            $.ajax({
                type: "POST",
                data: data,
                dataType: 'json',
                url: SITE_URL + 'neworder/place_order/',
                success: function(e) 
                {
                    if ( e.response == undefined || e.response.status_code  !== '200' ) 
                    {
                            alert("Transaction Failed");
                                return false;
                    }
                    else
                    {
                        alert("Checkout Successful!");
                        window.location.reload();
                    }
                }
            });
        });

        /*Placing cart order above*/

        $('select.billing-select').change(function() {
            var parent_sel = $(this).parent();
            var value = $(this).val();
            var value_text = $(this).find("option:selected").text();

            var user_id = $("input.user_id").val();

            if (value == 0) {
                $('#billForm')[0].reset();

                $('#credit-card-details h1').text('ADD NEW CREDIT CARD');
                $('#credit-card-details .checkbox-save-bill').show();
                $('#credit-card-details .add-address').text('ADD');
                $('#credit-card-details').show();
                $('.billingDetails').attr('rel', '');
            } else if (value > 0) {
                $.ajax({
                    type: "POST",
                    data: {
                        'id': value,
                        "user_id": user_id
                    },
                    dataType: 'json',
                    url: SITE_URL + 'neworder/get_billing/',
                    success: function(e) {

                        $('.billingDetails').attr('rel', e.Data[0].id);

                        self.set_billing_values({
                            'id': e.Data[0].id,
                            'card_type': e.Data[0].card_type,
                            'card_no': e.Data[0].card_number,
                            'card_cvv': e.Data[0].card_cvv,
                            'card_name': e.Data[0].card_name,
                            'expiry_month': e.Data[0].expire_month,
                            'expiry_year': e.Data[0].expire_year,
                            'card_zip': e.Data[0].card_zip,
                        });



                        $('#credit-card-details input[name="cardNo"]').val(e.Data[0].card_number);
                        $('#credit-card-details input[name="cardZip"]').val(e.Data[0].card_zip);
                        $('#credit-card-details select[name="cardMonth"] option[value="' + e.Data[0].expire_month + '"]').prop('selected', true);
                        $('#credit-card-details select[name="cardYear"] option[value="' + e.Data[0].expire_year + '"]').prop('selected', true);
                        $('#credit-card-details .checkbox-save-bill').hide();
                        $('#credit-card-details input[name="cardCvv"]').val(e.Data[0].card_cvv);

                        var card_no = e.Data[0].card_number;
                        var card_type = e.Data[0].card_type;
                        var expiry_month = e.Data[0].expire_month;
                        var expiry_year = e.Data[0].expire_year;
                        var card_zip = e.Data[0].card_zip;
                        var id = e.Data[0].id;
                        var card_cvv = e.Data[0].card_cvv;
                        var card_name = e.Data[0].card_name;


                      
                        $("div.billing-card-holder").hide();
                        $("div.billing-info-show").html("<h3>" + card_type + " card number ending with " + card_no.substr(-4) + " selected.</h3>").show("slow");
                        $("input[name=billing_card]").val(id);
                        $(".popup-wrapper").hide();
                        $('.billingDetails, .change-billing').show();
                        self.edit_change_billing($("input[name=billing_card]").val());
                        $('.billinglist').parent().hide();
                      
                    }
                });
            }
        });
    },

    set_billing_values: function(data) {
        $('#_payment_form input[name="_card_type"]').val(data.card_type);
        $('#_payment_form input[name="_card_number"]').val(data.card_no);
        $('#_payment_form input[name="_card_cvv"]').val(data.card_cvv);
        $('#_payment_form input[name="_card_name"]').val(data.card_name);
        $('#_payment_form input[name="_expiry_month"]').val(data.expiry_month);
        $('#_payment_form input[name="_expiry_year"]').val(data.expiry_year);
        $('#_payment_form input[name="_card_zip"]').val(data.card_zip);
        $('.billingDetails .card_no').text(data.card_no.substr(-4));
        $('.billingDetails .card_month').text(data.expiry_month);
        $('.billingDetails .card_year').text(data.expiry_year);
        $('.billingDetails .card_typ').text(data.card_type);
        $('input[name="card_id"]').val(data.id);
    },



    add_card: function(value_text, id) {

        var self = this;

        if (!id) id = 0;

        $('.credit-card-button').unbind('click').on('click', function(e) {

            card_name = $('input[name="cardNickname"]').val();
            card_no = $('input[name="cardNo"]').val();
            card_cvv = $('input[name="cardCvv"]').val();
            card_type = $('select[name="cardType"]').val();
            card_zip = $('input[name="cardZip"]').val();
            expiry_month = $('select[name="cardMonth"]').val();
            expiry_year = $('select[name="cardYear"]').val();
            is_save = $('input[name="save_billing"]').prop("checked");      // This 
			address1 = $('#credit-card-details input[name="address1"]').val();
            street = $('#credit-card-details input[name="street"]').val();
            if(is_save){
            	is_save=1;
            }
            else{
            	is_save=0;
            }
            
			if (isNaN(card_no) == true ||  (card_no.length < 13 || card_no.length > 16 || card_no.length == 14 )) {
                alert("Invalid Card number!");
                return false;
            } else if (isNaN(card_cvv) == true) {
                alert("Invalid card sec.no");
                
                return false;

            }
			
			if(card_type == "American Express"  ){
            		
            	if( card_cvv.length != 4 ) { 
            		alert("Invalid card sec.no");
            		return false;
            	}
            		
            } else if(card_type != "American Express"  ){
            		
            	if( card_cvv.length != 3 ) { 
            		alert("Invalid card sec.no");
            		return false; 
            	}

            } 
			
			jsTime = self.getCurrentTimJs();
            currentMonth = parseInt(jsTime.getMonth()) + 1;
			currentYear  = parseInt(jsTime.getFullYear());
			selectedMonth  = parseInt(expiry_month);
			selectedYear   = parseInt(expiry_year);
			
			if(selectedYear < currentYear)
			{
				alert("The expiration date is invalid");
				return false;
			}
			else if(selectedYear == currentYear && selectedMonth  <= currentMonth){
			
				alert("The expiration date is invalid");
				return false;
			}		
			
			
			if (!card_no || !card_cvv || !card_zip || !card_type || !expiry_month || !expiry_year ) {
				alert("All fields are important.");
				return false;
			}
              
			var uid = 0;
			$("input[name=uid]").each(function(){ 
			
				if($(this).val() != "") 
					$("input[name=uid]").val($(this).val())	;
			})
            var hide_box=true;
			var billing;
			uid = $("input[name=uid]").val();
                $.ajax({
                    type: "POST",
                    data: {
                        'card_name': '',
                        'card_no': card_no,
                        'card_type': card_type,
                        'expiry_month': expiry_month,
                        'expiry_year': expiry_year,
                        'card_zip': card_zip,
                        'uid': uid,
                        'crd_id': id,
                        'display_in_menu': is_save,
                        'cvv': card_cvv,
                        'address1':address1,
                        'street':street,
                    },
                    dataType: 'json',
                    url: SITE_URL + 'neworder/saveBillinginfo/',
                    success: function(e) {
                        if(typeof(e)==="string" && isNaN(e)){
                        $('.error_msg').text('Please check your credit card details');
                        hide_box=false;
                        return false;
                        }
                        else{
                            var dat=e;
                             if(dat.Data!=true && dat.Data!=null && isNaN(dat.Data)){
                                $('.error_msg').text('Please check your credit card details');
                                hide_box=false;
                                return false; 
                            }
                        }
                        if(hide_box==true){
                            // this also sets the three auth boxes...On neworder/saveBillinginfo/!!!
                    	$('input[name="_card_number_auth"]').val(card_no);
                    	$('input[name="_expiry_month_auth"]').val(expiry_month);
                    	$('input[name="_expiry_year_auth"]').val(expiry_year);
                        $("input[name=billing_card]").val(e.Data);

                        if ($('.billing-select option').length >= 2) {
                            
                                $('.billing-select').append('<option value="' + e.Data + '">visa card number ending with ' + card_no.substr(-4) + ' </option>');
                          
                            billing=e.Data;

                        }
                        }
                        
                 if(hide_box==true){
                self.set_billing_values({
                    'id': billing,
                    'card_type': card_type,
                    'card_no': card_no,
                    'card_cvv': card_cvv,
                    'card_name': card_name,
                    'expiry_month': expiry_month,
                    'expiry_year': expiry_year,
                    'card_zip': card_zip
                });


                $("div.billing-card-holder").hide();
                $("div.billing-info-show").html("<h3>" + card_type + " card number ending with " + card_no.substr(-4) + " selected.</h3>").show("slow");
                $("input[name=billing_card]").val(id);
                $(".popup-wrapper").hide();
                $('.billingDetails, .change-billing').show();
                self.edit_change_billing($("input[name=billing_card]").val());
                $('.billinglist').parent().hide();
                }
                    }
                });  
                 
            
        });
    },
	
	getCurrentTimJs : function(){
		var self = this;
		servtime  = self.getServerTime();
        servtime  = servtime.split(",")
		servtime[1] = parseInt(servtime[1]-1);
        return new Date(servtime[0],servtime[1],servtime[2],servtime[3],servtime[4],servtime[5] );
	},
	
	//~getting server time using async Ajax call.
	getServerTime : function(){
		var dateTime;
		var self = this;
        var currentDayHour = 0;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'neworder/getCurrentHour/',
            async: false,
            success: function(data) {
            	 $json =  JSON.parse(data);
        		
            	if($json.date_time_js){
            		dateTime = $json.date_time_js;
            	}
            	
            }
        });
        return dateTime;
	},
	
    edit_change_billing: function(editId) {

        var self = this;
        $('.change-billing, .edit-billing').unbind('click').on('click', function(e) {

            if ($(e.target).hasClass('edit-billing')) {

                $('#credit-card-details h1').text(' EDIT CREDIT CARD');
                $('#credit-card-details .checkbox-save-bill').show();
                $('#credit-card-details .add-address').text('EDIT');
                $('#credit-card-details input[name="cardCvv"]').val('');
                $('#credit-card-details').show();


            } else if ($(e.target).hasClass('change-billing')) {

                $('.billingDetails').hide();
                $('.change-billing , .edit-billing').hide();
                $('.billing-select').parent().parent().show();
            }

        });

       

    },
   

    edit_gallery_options: function() {

        var self = this;
        self.calculate_price();
        $('.custom-sandwich select , .custom-sandwich input').change(function(e) {

            data = $(this).data();
            propChk = $(this).prop('checked');



            if (e.target.localName == 'select') {

                sel_data = $(this).find('option:selected').data();
                parent_data = $(this).parent().find('input').data();

            } else {

                if (data.category == 'BREAD') {

                    self.json_obj[data.category].item_name = [data.item_name];
                    self.json_obj[data.category].item_id = [data.id];
                    self.json_obj[data.category].item_image = [data.image];
                    self.json_obj[data.category].item_price = [data.item_price];
                } else {

                    $pos_name = $.inArray(data.item_name, self.json_obj[data.category].item_name);
                    $pos_id = $.inArray(data.id, self.json_obj[data.category].item_id);
                    $pos_image = $.inArray(data.image, self.json_obj[data.category].item_image);
                    $pos_price = $.inArray(data.item_price, self.json_obj[data.category].item_price);

                    if (propChk == false && $pos_name != -1) {
                        self.json_obj[data.category].item_name.splice($pos_name, 1)
                    }

                    if (propChk == true && $pos_name == -1) {
                        self.json_obj[data.category].item_name.push(data.item_name)
                    }

                    if (propChk == false && $pos_id != -1) {
                        self.json_obj[data.category].item_id.splice($pos_id, 1)
                    }

                    if (propChk == true && $pos_id == -1) {
                        self.json_obj[data.category].item_id.push(data.id)
                    }


                    if (propChk == false && $pos_image != -1) {
                        self.json_obj[data.category].item_image.splice($pos_image, 1)
                    }

                    if (propChk == true && $pos_image == -1) {
                        self.json_obj[data.category].item_image.push(data.image)
                    }


                    if (propChk == false && $pos_price != -1) {
                        self.json_obj[data.category].item_price.splice($pos_price, 1)
                    }

                    if (propChk == true && $pos_price == -1) {

                        self.json_obj[data.category].item_price.push(data.item_price)
                    }


                    self.calculate_price();
                }

            }
        });



        $('.gallery_save').unbind('click').on('click', function(e) {

            e.preventDefault();
            if ($('#checkbox-2-1').prop('checked') == true) {
                pub = 1;
            } else {
                pub = 0;
            }

            var name = $('input[name="sandwichname"]').val()

            if (!name) {
                alert("Please enter Name of sandwich.");
                return false;
            }

            var description = $("#description").val();

            $data = {
                'data': JSON.stringify(self.json_obj),
                'name': $('input[name="sandwichname"]').val(),
                'is_pub': pub,
                'price': self.price_tmp,
                'menu_active': $('input[name="sandwich_menustate"]').val(),
                'id': $('input[name="sandwich_id"]').val(),
                'uid': $('input[name="sandwich_uid"]').val(),
                'description': description
            }

            $.ajax({
                type: "POST",
                data: $data,
                url: SITE_URL + 'sandwich/update/',
                dataType: 'json',
                success: function(e) { 

                    alert("Sandwich saved succcessfully.")
                }

            });

        });



    },

    price_tmp: 0,
    calculate_price: function() {
        if (typeof(admin_ha.common.json_obj) == "undefined") {
            return;
        }
        var final_price = 0;
        var self = this;
        $data = self.json_obj;
        if ($data != null) {
            Object.keys($data).forEach(function(e) {
                $price = $data[e].item_price;
                temp = 0;
                $price.forEach(function(t) {
                    if (t !== undefined && isNaN(t) != true) {
                        final_price += parseFloat(t);
                        temp += parseFloat(t);
                        
                    }
                });
            });
            self.price_tmp = final_price.toFixed(2);
        }
    },

    totals_sales_report_filter_events: function() {
        var self = this;
        $("a.do_total_sales_filter").click(function() {
            var data = $("form[name=totals_sales_report_filter]").serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'reports/doTotalSalesFilterReports/',
                success: function(e) {
                    $("div.reports_table").html(e);
                }
            });
        });
        $("a.do_store_total_sales_filter").click(function() {
            var data = $("form[name=totals_sales_report_filter]").serialize();
            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'storereports/doTotalSalesFilterReports/',
                success: function(e) {
                    $("div.reports_table").html(e);
                }
            });
        });
    },

    totals_items_report_filter_events: function() {
        var self = this;
        $("a.do_item_report_filter").click(function() {
            var data = $("form[name=items_sales_report_filter]").serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'reports/doTotalItemsFilterReports/',
                success: function(e) {
                    $("div.reports_table").html(e);
                    self.item_reports_events();
                }
            });
        });
        $("a.do_store_item_report_filter").click(function() {
            var data = $("form[name=items_sales_report_filter]").serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'storereports/doTotalItemsFilterReports/',
                success: function(e) {
                    $("div.reports_table").html(e);
                    self.item_reports_events();
                }
            });
        });
    },




    store_accounts: function() {
        var self = this;
        $("button.save_store_account").unbind('click');
        $("button.save_store_account").click(function() {
            var form = $('form[name="maincontents"]');
            var account_data = form.serialize();

            var store_name = $(form).find("input[name=store_name]").val();
            var address1 = $(form).find("[name=address1]").val();
            var address2 = $(form).find("[name=address2]").val();
            var zip = $(form).find("[name=zip]").val();
            var username = $(form).find("[name=username]").val();
            var password = $(form).find("[name=password]").val();
            var con_password = $(form).find("[name=con_password]").val();
            var report_pin = $(form).find("[name=report_pin]").val();
            var restrict_ip = $(form).find("[name=restrict_ip]").val();
            var id = $(form).find("[name=id]").val();

            if (id) {

                if (!store_name || store_name == "") {
                    alert("Please enter the Store Name.");
                    return false;
                }

                if (!address1 || address1 == "") {
                    alert("Please enter the Address Line One.");
                    return false;
                }

                if (!zip || zip == "") {
                    alert("Please enter the Zip Code.");
                    return false;
                }

                if (password && password != con_password) {
                    alert("Password and confirm password not matched.")
                    return false;
                }

            } else {

                if (!store_name || store_name == "") {
                    alert("Please enter the Store Name.");
                    return false;
                }

                if (!address1 || address1 == "") {
                    alert("Please enter the Address Line One.");
                    return false;
                }

                if (!zip || zip == "") {
                    alert("Please enter the Zip Code.");
                    return false;
                }

                if (!username || username == "") {
                    alert("Please enter user name");
                    return false;
                }

                if (password && password != con_password) {
                    alert("Password and confirm password not matched.")
                    return false;
                }


                $.post(SITE_URL + "accounts/storeUsernameExists", account_data,
                    function(data, status) {
                        data = JSON.parse(data);

                        if (data.state == "true") {
                            alert("Username / Email already exists.");
                            return false;
                        } else {

                        }
                    });
            }



            $.ajax({
                type: "POST",
                data: account_data,
                url: SITE_URL + 'accounts/updateStoreAccount/',
                success: function(e) {
                    window.location = SITE_URL + "accounts";
                }
            });

        });
        $("a.zip_add").click(function() {
        	
            var zipcount = $("input[name=zipcount]").val();
            var minamount =$(this).attr('data-minamount');
            $('.ten').find("input[name=minAmount]").val(minamount);
            zipcount = parseInt(zipcount);
            zipcount++;

            var field = '<ul><li class="zip-code"><input type="text"  value="" name="zipcode' + zipcount + '"></li><li class="abbr"><input type="text" value="" maxlength="3" name="abbreviation' + zipcount + '"> <input type="hidden" value="10" name="minAmount' + zipcount + '"></li><li class="delivery-fee"><input type="text" value="" name="deliveryfee' + zipcount + '"></li><li class="zipItemrmv"><a class="removeZip" >Remove</a></li></ul>';


            $(".ten div.zip_wrap").append(field);
            self.remove_zipCode();
            $("input[name=zipcount]").val(zipcount);

        });
        $("a.zip_add_hundred").click(function() {
        	
            var zipcount = $("input[name=zipcount]").val();
            var minamount =$(this).attr('data-minamount');
            $('.hundred').find("input[name=minAmount]").val(minamount);
            zipcount = parseInt(zipcount);
            zipcount++;

            var field = '<ul><li class="zip-code"><input type="text"  value="" name="zipcode' + zipcount + '"></li><li class="abbr"><input type="text" maxlength="3" value="" name="abbreviation' + zipcount + '"><input type="hidden" value="100" name="minAmount' + zipcount + '"></li><li class="delivery-fee"><input type="text" value="" name="deliveryfee' + zipcount + '"></li><li class="zipItemrmv"><a class="removeZip" >Remove</a></li></ul>';

            $(".hundred div.zip_wrap").append(field);
            self.remove_zipCode();
            $("input[name=zipcount]").val(zipcount);

        });


    },

    remove_zipCode: function() {
        var self = this;
        $('.removeZip').unbind('click').on('click', function() {

            parent = $(this).parent().parent();
            parent = $(parent);

            zipId = parent.find('input[name="zipCodeId"]').val();
            if (zipId > 0) {
                $.ajax({
                    type: "POST",
                    data: {
                        'zipid': zipId
                    },
                    url: SITE_URL + 'accounts/deleteZip/',
                    success: function(e) {
                    }
                });
            }
            parent.remove();
        });
    },

    orders_search: function() {
        $("div.start-order a.arrow").click(function() {
            $("form.new-order-form").submit();
        });
    },

    newOrderNewUserAccount: function() {
        $("input.newUserAccount").click(function() {
            var form = $(this).parent();
            var userdata = $(form).serialize();
            var first_name = $(form).find("[name=firstname]").val();
            var last_name = $(form).find("[name=lastname]").val();
            var username = $(form).find("[name=username]").val();
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!first_name) {
                alert("Please fill the First Name");
                return;
            }

            if (!last_name) {
                alert("Please fill the Last Name");
                return;
            }

            if (!username) {
                alert("Please fill the Email/Username");
                return;
            }

            if (!filter.test(username)) {
                alert("Please enter a valid email address");
                return false;
            }

           

            if (username) {

                $.post(SITE_URL + "accounts/cmsUsernameExists", userdata,
                    function(data, status) {
                        data = JSON.parse(data);

                        if (data.state) {
                            alert("Username / Email already exists.");
                            return false;
                        } else {

                            $.ajax({
                                type: "POST",
                                url: SITE_URL + "neworder/createAccount",
                                data: userdata,
                                success: function(data, status) {
                                    alert("New user created.");
                                    data = JSON.parse(data);
                                    if (data.msg.uid) {
                                        window.location = SITE_URL + "neworder/addToCart/" + data.msg.uid;
                                    }
                                }

                            });

                        }
                    });
            }



        });

    },

    edit_standard_products: function() {
        $("input.save_standard_product").click(function() {
            var product_name = $("input[name=product_name]").val();
            var description = $("textarea[name=description]").val();
            var product_price = $("input[name=product_price]").val();

            if (!product_name) {
                alert("Please enter Product name.");
                return false;
            }

           

            if (!product_price) {
                alert("Please enter Product price.");
                return false;
            }

            if (isNaN(product_price)) {
                alert("Please enter valid Product price.");
                return false;
            }


            $("form[name=newbread]").submit();

        });
    },

    edit_custom_item: function() {
        $("input.save_custom_item").click(function() {
            var item_name = $("input[name=item_name]").val();

            var item_price = $("input[name=item_price]").val();

            if (!item_name) {
                alert("Please enter " + $("input[name=item_name]").prev().text() + ".");
                return false;
            }

            if (!item_price) {
                alert("Please enter " + $("input[name=item_price]").prev().text() + ".");
                return false;
            }

            if (isNaN(item_price)) {
                alert("Please enter valid Product price.");
                return false;
            }

            $("form").submit();

        });
    },

    edit_banner_details: function() {

        $("input.save_banner").click(function() {

            var banner_name = $("input[name=banner_name]").val();
            var banner = $("input[name=banner]").val();

            if (!banner_name) {
                alert("Please enter banner name.");
                return false;
            }

            if (!banner) {
                alert("Please select type of banner.");
                return false;
            }



            $("input[type=submit]").click();

        });

    },

    save_webpage: function() {

        $("input.save_webpage").click(function() {
            var webpage_name = $("input[name=webpage_name]").val();
            if (!webpage_name) {
                alert("Please enter webpage name.");
                return false;
            }

            $("input[type=submit]").click();

        });

    },


    SOTD_and_TRND: function() {
        $("div.sotd.black").click(function() {
            var sandwich = $(this).data("sandwich");
            $('#sotd_sandwich_id').val(sandwich);
            $("#SOTD").show();
        });

        $('input.add_sotd').click(function() {

            var date = $("input#sotd_date").val();
            if (!date) {
                alert("Please select date.");
            }

            var data = $("form.sotd").serialize();

            $.ajax({
                type: "POST",
                url: SITE_URL + "sandwich/addSandwichToSOTD",
                data: data,
                success: function(data, status) {
                    window.location.reload();
                }

            });
        });

        $("a.remove_sotd").click(function() {

            var sandwich = $(this).data("sandwich");
            $.ajax({
                type: "POST",
                url: SITE_URL + "sandwich/removeSandwichToSOTD",
                data: {
                    'id': sandwich
                },
                success: function(data, status) {
                    window.location.reload();
                }

            });

        });

        $("div.trnd.black").click(function() {
            var sandwich = $(this).data("sandwich");
            $.ajax({
                type: "POST",
                url: SITE_URL + "sandwich/addSandwichToTRND",
                data: {
                    'id': sandwich
                },
                success: function(data, status) {
                    window.location.reload();
                }
            });
        });

        $("a.remove_trnd").click(function() {

            var sandwich = $(this).data("sandwich");
            $.ajax({
                type: "POST",
                url: SITE_URL + "sandwich/removeSandwichToTRND",
                data: {
                    'id': sandwich
                },
                success: function(data, status) {
                 window.location.reload();
                }

            });

        });

		
		 $(".priority_class").keypress(function (e) {
		 //if the letter is not digit then display error and don't type anything
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
				//$("#errmsg").html("Digits Only").show().fadeOut("slow");
				   return false;
			}
	   });
	   
			   
	   $("a.set_priority").click(function() {			 
            var sandwich = $(this).data("sandwich");
			var priority = $('#'+sandwich).val();  
            $.ajax({
                type: "POST",
                url: SITE_URL + "sandwich/setPriority",
                data: {
                    'id': sandwich,
					'priority': priority
                },
                success: function(data, status) { 
                   window.location.reload();
                }

            });

        });
		
        $("table.sandwich-trending tr td input.do-trnd").click(function() {
            var $parent, state, id;
            state = 0;
            if ($(this).prop('checked') == true) {
                state = 1;
            }
            id = $(this).data("id");

            $.ajax({
                type: "POST",
                data: {
                    'state': state,
                    'id': id
                },
                url: SITE_URL + 'sandwich/setTrndLive/',
                success: function(e) {  }
            });
        });
    },
    
    checkoutStoreSelectionEvent: function(){
		$(document).on("change", 'select.user_address', function(e){
				var self = this;
		        var currentDayHour = 0;
		        $.ajax({
		            type: "POST",
		            url: SITE_URL + 'neworder/getCurrentHour/',
		            success: function(data) {
		                currentDayHour = JSON.parse(data);
		            }
		        });

		        //10 to 4 PM
		        setTimeout(function() {
			        var currentHour = parseInt(currentDayHour.hour);
			        var currentDay = parseInt(currentDayHour.day);
			        var currentDate = currentDayHour.date_time_js;
			        currentDate = currentDate.split(",");
			        currentDate = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);

					$(".checkout-msg-red").html("");
			       
			        var delvry = -1;
			     
			        if ($('input:radio[name=radio_address_type]:checked').val() == "delivery") {
			            delvry = 1;
			        } else if ($('input:radio[name=radio_address_type]:checked').val() == "pickup") {
			            delvry = 0;
			        }

			        var deliveryOrPickup = "";
			        var address_id = 0;

		            if (delvry) {
		
		                deliveryOrPickup = "delivery";
		                var deliveryAddressId = $("input.address_selected").val();
		
		            } else {
		                deliveryOrPickup = "pickup";
		                var pickupStoreId = $("select.user_address").val();
		            }
		            var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		
		            if (address_id == 0 && deliveryOrPickup == "pickup") {
		
		                address_id = pickupStoreId;
		
		            } else {
		
		                address_id = deliveryAddressId;
		            }
		
		
		
		            if (deliveryOrPickup && address_id && currentDay) {

		                $.ajax({
		                    type: "POST",
		                    url: SITE_URL + 'neworder/getCurrentStoreTimeSlot/',
		                    data: {
		                        deliveryOrPickup: deliveryOrPickup,
		                        id: address_id,
		                        day: days[currentDay]
		                    },
		                    success: function(response) {
		                    	
		                        store_timing = JSON.parse(response);
		                        var open = new Date(currentDate);
		                        var close = new Date(currentDate);
		                        var open_time = store_timing.open.split(":");
		                        var close_time = store_timing.close.split(":");
		
		        		

		                        if (store_timing.store_active) {
		                            var open_time = store_timing.open.split(":");
		                            var close_time = store_timing.close.split(":");
		                            open.setHours(open_time[0]);
		                            open.setMinutes(open_time[1]);
		
		                            close.setHours(close_time[0]);
		                            close.setMinutes(close_time[1]);
		                            currentDate = new Date(currentDate);
		
		
		
		
		                            if (store_timing.pickup_active == 1 && deliveryOrPickup == "pickup") {
		                                //if pickup deactivated
		
		                                if (currentDate < close && currentDate > open) {
		
		                                    return;
		                                } else {
		
		                                }
		
		                                $("#radio1").attr("disabled", true);
		                                $("#radio1").attr("checked", false);
		                                $("#radio2").attr("checked", true);
		                                $("#radio2").trigger("click");
		
		
		                                var msg = "<span class='checkout-msg-red'>APOLOGIES, DUE TO HIGH ORDER VOLUME, WE CANNOT ACCEPT ADDITIONAL PICK-UP ORDERS AT THIS TIME.</span>";
		                                $("div.date-time-order").after(msg);
		
		
		                                var current_day = (currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear();
		       
		                                var currentTime = currentDate.getHours() + ":" + currentDate.getMinutes();
		
		                                var store_id = store_timing.store_id;
		                                var selectedDay = days[currentDay];
		                                var isCurrentDay = 0;
		
		                                var params = {
		                                    "current_day": current_day,
		                                    "current_time": currentTime,
		                                    "store_id": store_id,
		                                    "selectedDay": selectedDay,
		                                    "isCurrentDay": isCurrentDay
		                                };
		
		                                
		
		                                return;
		                            }
		
		                            if (store_timing.delivery_active == 1 && deliveryOrPickup == "delivery") {
		
		
		                                if (currentDate < close && currentDate > open) {
		
		                                    return;
		                                } else {
		
		                                }
		
		
		                                $("#radio1").attr("disabled", true);
		                                $("#radio1").attr("checked", false);
		                                $("#radio2").attr("checked", true);
		                                $("#radio2").trigger("click");
		
		
		                                var msg = "<span class='checkout-msg-red'>APOLOGIES, DUE TO HIGH ORDER VOLUME, WE CANNOT ACCEPT ADDITIONAL DELIVERY ORDERS AT THIS TIME.</span>";
		
		                                $("div.date-time-order").after(msg);
		
		
		                                var current_day = (currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear();
		
		                                var currentTime = currentDate.getHours() + ":" + currentDate.getMinutes();
		
		                                var store_id = store_timing.store_id;
		                                var selectedDay = days[currentDay];
		                                var isCurrentDay = 0;
		
		                                var params = {
		                                    "current_day": current_day,
		                                    "current_time": currentTime,
		                                    "store_id": store_id,
		                                    "selectedDay": selectedDay,
		                                    "isCurrentDay": isCurrentDay
		                                };
		
		                                return;
		
		                            }
		
		
		
		                            if (currentDate < open) {
		
		
		                                var openhour = open.getHours();
		                                var open_ampm = openhour >= 12 ? 'pm' : 'am';
		                                openhour = openhour % 12;
		                                openhour = openhour ? openhour : 12; // the hour '0' should be '12'
		                                openhour = ("0" + openhour).slice(-2);
		
		                                var closehour = close.getHours();
		                                var close_ampm = closehour >= 12 ? 'pm' : 'am';
		                                closehour = closehour % 12;
		                                closehour = closehour ? closehour : 12; // the hour '0' should be '12'
		                                closehour = ("0" + closehour).slice(-2);
		
		                                var msg = "<span class='checkout-msg-red'>SORRY, WE’RE CURRENTLY CLOSED. <br/>HOURS: " + openhour + ":" + ("0" + open.getMinutes()).slice(-2) + " " + open_ampm + " - " + closehour + ":" + ("0" + close.getMinutes()).slice(-2) + " " + close_ampm + " <br/>SELECT A FUTURE ORDER TIME BELOW.</span>";
		
		                                $("div.date-time-order > p").after(msg);
		                                $("#radio1").attr("disabled", true);
		                                $("#radio1").attr("checked", false);
		                                $("#radio2").attr("checked", true);
		                                $("#radio2").trigger("click");
		                                $("div.timechange select > option[value='12:00']").attr("selected", "selected");
		
		                            } else if (currentDate > close) {
		
		
		                                $("#radio1").attr("disabled", true);
		                                $("#radio1").attr("checked", false);
		                                $("#radio2").attr("checked", true);
		                                $("#radio2").trigger("click");
		
		                                var openhour = open.getHours();
		                                var open_ampm = openhour >= 12 ? 'pm' : 'am';
		                                openhour = openhour % 12;
		                                openhour = openhour ? openhour : 12; // the hour '0' should be '12'
		                                openhour = ("0" + openhour).slice(-2);
		
		                                var closehour = close.getHours();
		                                var close_ampm = closehour >= 12 ? 'pm' : 'am';
		                                closehour = closehour % 12;
		                                closehour = closehour ? closehour : 12; // the hour '0' should be '12'
		                                closehour = ("0" + closehour).slice(-2);
		
		
		                                var msg = "<span class='checkout-msg-red'>SORRY, WE’RE CURRENTLY CLOSED. <br/>HOURS: " + openhour + ":" + ("0" + open.getMinutes()).slice(-2) + " " + open_ampm + " - " + closehour + ":" + ("0" + close.getMinutes()).slice(-2) + " " + close_ampm + " <br/>SELECT A FUTURE ORDER TIME BELOW.</span>";
		
		                                $("div.date-time-order").after(msg);
		
		                                var current_day = (currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear();
		
		                                var currentTime = currentDate.getHours() + ":" + currentDate.getMinutes();
		
		                                var store_id = store_timing.store_id;
		                                var selectedDay = days[currentDay];
		                                var isCurrentDay = 0;
		
		                                var params = {
		                                    "current_day": current_day,
		                                    "current_time": currentTime,
		                                    "store_id": store_id,
		                                    "selectedDay": selectedDay,
		                                    "isCurrentDay": isCurrentDay
		                                };
		
		
		                            } else if (currentDate < close && currentDate > open) {
		
		                                return;
		                            } else {
		
		
		                                $("#radio1").attr("disabled", true);
		                                $("#radio1").attr("checked", false);
		                                $("#radio2").attr("checked", true);
		                                $("#radio2").trigger("click");
		
		
		                                var openhour = open.getHours();
		                                var open_ampm = openhour >= 12 ? 'pm' : 'am';
		                                openhour = openhour % 12;
		                                openhour = openhour ? openhour : 12; // the hour '0' should be '12'
		                                openhour = ("0" + openhour).slice(-2);
		
		                                var closehour = close.getHours();
		                                var close_ampm = closehour >= 12 ? 'pm' : 'am';
		                                closehour = closehour % 12;
		                                closehour = closehour ? closehour : 12; // the hour '0' should be '12'
		                                closehour = ("0" + closehour).slice(-2);
		
		
		                                if (open.getMinutes()) optimez = open.getMinutes();
		                                else optimez = null;
		                                if (close.getMinutes()) closetimez = close.getMinutes();
		                                else closetimez = null;
		                                var msg = "<span class='checkout-msg-red'>SORRY, WE’RE CURRENTLY CLOSED. <br/>HOURS: " + openhour + ":" + ("0" + optimez).slice(-2) + " " + open_ampm + " - " + closehour + ":" + ("0" + closetimez).slice(-2) + " " + close_ampm + " <br/>SELECT A FUTURE ORDER TIME BELOW.</span>";
		

		
		                                $("div.date-time-order").after(msg);
		                                $("div.timechange select > option[value='12:00']").attr("selected", "selected");
		
		                                var current_day = (currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear();
		
		                                var currentTime = currentDate.getHours() + ":" + currentDate.getMinutes();
		
		                                var store_id = store_timing.store_id;
		                                var selectedDay = days[currentDay];
		                                var isCurrentDay = 0;
		
		                                var params = {
		                                    "current_day": current_day,
		                                    "current_time": currentTime,
		                                    "store_id": store_id,
		                                    "selectedDay": selectedDay,
		                                    "isCurrentDay": isCurrentDay
		                                };
		
		                            }
		                        }
		                    }
		                });
		            }
		
		        }, 2100);
		});
	
	},
    search_accounts: function() 
    {
        $("#searchDelUser").unbind('click').on('click', function()
        {
            var data = {
                "from_date": $("#from_date").val(),
                "from_time":  $("#from_time").val(),
                "to_date":  $("#to_date").val(),
                "to_time":  $("#to_time").val()
            };
            $.ajax({
                    type: "POST",
                    data: data,
                    url: SITE_URL + 'accounts/deliveriesByDate/',
                    success: function(data) 
                    {
                        $("#delUserList").hide();
                        $("#ajaxList").html(data);
                    }
                });
        });

        $("#searchOrderDetails").unbind('click').on('click', function()
        {
            var data = {
                "uid": $("#uid").val(),
                "from_date": $("#from_date").val(),
                "from_time":  $("#from_time").val(),
                "to_date":  $("#to_date").val(),
                "to_time":  $("#to_time").val()
            };
            $.ajax({
                    type: "POST",
                    data: data,
                    url: SITE_URL + 'reports/showDeliveriesDetailsByDate/',
                    success: function(data) 
                    {
                        $("#orderDetails").hide();
                        $("#orderDetailsAjaxResult").html(data);
                    }
                });
        });
    }




}

$(document).ready(function() {
    $('.add_more').click(function(){
      
          $('.add-more-opt').append('<input type="text" name="option[]" class="descriptor" placeholder="" value="">');
          $('.add-more-prc').append('<input type="text" name="price[]" class="descriptor" placeholder="" value="">');
          $('.add-more-rem').append('<p class="rem_add_more">remove</p>');
        
    });
});
$(document).ready(function() {
    $('.rem_add_more').click(function(){
        var inputs = $(".description-right").find($("input") );
        if(inputs.length > 2){
            $(".add-more-opt input" ).last().remove();
            $(".add-more-prc input" ).last().remove();
            $(".add-more-rem p" ).last().remove();
        }else{
            $('.option_price').val('');
        }
    });
});

$(function() {
    admin_ha.common.init();
});


$(document).ready(function() 
{
    $("a.update_priority").click(function() 
    {
        $(this).parent("form").submit();
    });
    /* ------ edit items starts --------- */
    var url = window.location.pathname.split("/");
    var method = url[2];
    if(method == "editItems")
    {
        $('.file-choose-pro input:checked').each(function() 
        {
            var id =this.id;
            $("#upload-"+id).show();
            $("#lImg-"+id).change(function()
            {
                 $("#lImgName-"+id).html($("#lImg-"+id).val().split('\\').pop());
            });
            $("#rImg-"+id).change(function()
            {
                 $("#rImgName-"+id).html($("#rImg-"+id).val().split('\\').pop());
            });
            $("#tImg-"+id).change(function()
            {
                 $("#tImgName-"+id).html($("#tImg-"+id).val().split('\\').pop());
            });
        });
    }
    /* ------ edit items starts --------- */
    $(".collapse-expand").click(function()
         {
            $(this).find('span').text(($(this).find('span').text() == '+')?'-':'+');
            $(this).next('.collapse').toggle();
        })
});

function showUploadImage(id)
{
    var id = id.id;
    if($("#"+id).is(":checked"))
    {
        $("#upload-"+id).show();
        $("#lImg-"+id).change(function()
        {
             $("#lImgName-"+id).html($("#lImg-"+id).val().split('\\').pop());
        });
        $("#rImg-"+id).change(function()
        {
             $("#rImgName-"+id).html($("#rImg-"+id).val().split('\\').pop());
        });
        $("#tImg-"+id).change(function()
        {
             $("#tImgName-"+id).html($("#tImg-"+id).val().split('\\').pop());
        });
    }   
    else
    {
        $("#upload-"+id).hide();
    }  
}

function newFunction(code, url, id) {
    $.ajax({
        type: "POST",
        data: {
            order_id: id,
            status_code: code
        },
        url: url + 'pickups/managePickupData/',
        success: function(e) {
            $("div.right-side").html(e);

        }
    });
    return false;
}
$(document).ready(function() {
    $('select#tohour, #datehourreport').change(function() {

        var to = $('select#tohour').val();
        var from = $("#fromhour").val();
        var datehourreport = $("#datehourreport").val();
        var store = $("select.store").val();

        if (!datehourreport) {

            alert("Please select the date.")
            return false;
        }

        $.ajax({
            type: "POST",
            data: {
                'to': to,
                'from': from,
                'datehourreport': datehourreport,
                'store': store
            },
            url: SITE_URL + 'reports/getHourlyOrderReport',
            success: function(e) {
                $(".reporttable").html(e);

            }
        });

    });
    $('select#storetohour, #storedatehourreport').change(function() {
        var to = $('select#storetohour').val();
        var from = $("#fromhour").val();
        var datehourreport = $("#storedatehourreport").val();

        if (!datehourreport) {

            alert("Please select the date.")
            return false;
        }

        $.ajax({
            type: "POST",
            data: {
                'to': to,
                'from': from,
                'datehourreport': datehourreport
            },
            url: SITE_URL + 'storereports/getHourlyOrderReport',
            success: function(e) {
                $(".reporttable").html(e);
            }
        });

    });
    $(".createaccount").click(function(e) {
        e.preventDefault();
        $(".register").removeClass('register');
        $(".rerror_msg").html('');
        var rfname = $("#customernameinput").val();
        var rlname = $("#lastnameinput").val();
        var remail = $("#customeremailinput").val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
        if (rfname == "") {

            $("#customernameinput").addClass('register').focus();
        } else if (rlname == "") {

            $("#lastnameinput").addClass('register').focus();
        } else if (remail == "") {

            $("#customeremailinput").addClass('register').focus();


        } else if (!filter.test(remail)) {
            $("#customeremailinput").addClass('register').focus();
            $(".rerror_msg").html("Please enter a valid email address");
        } else {
            $.ajax({
                type: "POST",
                url: SITE_URL + "customer/newUserAccount",
                data: $("#new-order-form").serialize(),
                success: function(data) {

                    data = JSON.parse(data);

                    if (data.state == false) {
                        $(".rerror_msg").html(data.msg);
                    } else {
                        alert("New user created.");
                        window.location = SITE_URL + "customer";
                    }
                }
            });

        }

    });
    $(document).on("click", "a.view-details", function() {

        
            var orderId = $(this).data("order");
            var order = $(this).data("order-id");
            $('.popupOrderDetailsBg').fadeIn(200, function() {
                $('.popupOrderDetails,.popup_order_detail').fadeIn(300);
            });
            $('#popOut .loader').show();            
            $.ajax({
                type: "POST",
                data: {
                    'order_id': orderId
                },
                url: SITE_URL + 'deliveries/get_order_popup_data/',
                beforeSend: function() {
                    //$("#ajax-loader").show();
                    $("#order-detail-pop").removeAttr("style");
                },
                complete: function() {

                },
                success: function(data) {

                	   $('#popOut .loader').hide();
                       $('#popOut').html(data);  
                       $(".print_button").hide();
                }
            });
    });
    $(document).on("click", "a.reorder", function() {
    	var orderId = $(this).data("order");
        var order = $(this).data("order-id");	    
        var user_id = $(this).data("user-id");
	    $.ajax({
	        type: "POST",
	        data: {
	            'order_id': order,
	            'user_id':user_id
	        },
	        dataType: 'json',
	        url: SITE_URL + 'customer/reorder/',
	        beforeSend: function() {
	            $("#ajax-loader").show();
	        },
	        complete: function() {
	            $("#ajax-loader").hide();
	        },
	        success: function(e) {
	            window.location = SITE_URL + "neworder/addtocart/"+user_id;
	        }
	    });
    });
});

// function manageDelivereis(order_id, id, ordrno, order_status) 
// {

//     if(order_status == 5 && id == 6)
//     {
//         $.ajax({
//                 type: "POST",
//                 url: SITE_URL + "deliveries/getDeliveryUsers/",
//                 data: {
//                     'order_id': order_id,
//                 }, 
//                 success: function(res) 
//                 {
//                     var data = JSON.parse(res);

//                     var selOpts = "<option value='0'>SELECT</option>";
//                     for (var i = 0; i < data.length; i++) {
//                          selOpts += "<option value='"+data[i].uid+"'>"+data[i].name+"</option>";
//                     }
//                     $('.deliveryUserOptions').empty().append(selOpts);
//                     $("#delivery_user_assign_"+order_id).show();
//                 }
//         });
//         $("#assign_del_user").bind("click").on("click", function()
//         {
//             var delUser = $(".deliveryUserOptions").val();
//             if(delUser == 0)
//             {
//                 return;
//             }
//             $.ajax({
//                     type:"POST",
//                     url: SITE_URL + "deliveries/assignDeliveryUser/",
//                     data: {
//                         'delUser': delUser,
//                         'order_id': order_id
//                     }, 
//                     success: function(res) 
//                     {
//                         $("#delivery_user_assign").hide();
//                         if (ordrno > 0)
//                         { 
//                             $id = ordrno;
//                         }
//                         var status = $("#1").val();
//                         var store_id = $("#store_id").val();
//                         var date;

//                         if (window.location.href.indexOf("searchDeliveryData") != -1) date = $("#search_delivery_date").val();

//                         $.ajax({
//                             type: "POST",
//                             url: SITE_URL + "deliveries/managedeliveryData/",
//                             data: {
//                                 'order_id': order_id,
//                                 'id': id
//                             },
//                             success: function(data) 
//                             {
//                                 $.ajax({
//                                     type: "POST",
//                                     url: SITE_URL + "deliveries/searchDeliveryDataAjax/",
//                                     data: {
//                                         'search_date': date,
//                                         'delivery_status': status,
//                                         'store_id': store_id
//                                     },
//                                     success: function(data) {
//                                         data = JSON.parse(data);

//                                         $(".deliveries-table-detail-wrapper").html("");
//                                         $(".deliveries-table-detail-wrapper").html(data.html);
//                                         $(".count_new").html(data.count_new);
//                                         $(".count_prc").html(data.count_prc);
//                                         $(".count_bag").html(data.count_bag);
//                                         $(".count_pick").html(data.count_pick);
//                                         $(".total_delivery").html(data.total_delivery);

//                                     }
//                                 });
//                             }
//                         });
//                     }
//                 });
//         });
//         $("#close-assign-user").click(function() 
//         {
//             $("#delivery_user_assign").hide();
//         }); 
//     }
//     else
//     {

//         if (ordrno > 0)
//         { 
        
//             $id = ordrno;
//         }
//         var status = $("#1").val();
//         var store_id = $("#store_id").val();
//         var date;

//         if (window.location.href.indexOf("searchDeliveryData") != -1) date = $("#search_delivery_date").val();
//         console.log('id '+ id)
//         $.ajax({
//             type: "POST",
//             url: SITE_URL + "deliveries/managedeliveryData/",
//             data: {
//                 'order_id': order_id,
//                 'id': id
//             },
//             success: function(data) 
//             {

//                 $.ajax({
//                     type: "POST",
//                     url: SITE_URL + "deliveries/searchDeliveryDataAjax/",
//                     data: {
//                         'search_date': date,
//                         'delivery_status': status,
//                         'store_id': store_id
//                     },
//                     success: function(data) 
//                     {
//                         data = JSON.parse(data);
//                          console.log("data",data);
//                         console.log("data.htmk",data.html);
//                         $(".deliveries-table-detail-wrapper").html("");
//                         $(".deliveries-table-detail-wrapper").html(data.html);
//                         $(".count_new").html(data.count_new);
//                         $(".count_prc").html(data.count_prc);
//                         $(".count_bag").html(data.count_bag);
//                         $(".count_pick").html(data.count_pick);
//                         $(".total_delivery").html(data.total_delivery);

//                     }
//                 });
//             }
//         });
//     }

    
// }

function manageDelivereis(order_id, id, ordrno, order_status) 
{
    console.log('order_status',order_status);
    if(order_status == 5 && id == 6)
    {
        $.ajax({
                type: "POST",
                url: SITE_URL + "deliveries/getDeliveryUsers/",
                data: {
                    'order_id': order_id,
                }, 
                success: function(res) 
                {
                    var data = JSON.parse(res);

                    var selOpts = "<option value='0'>SELECT</option>";
                    for (var i = 0; i < data.length; i++) {
                         selOpts += "<option value='"+data[i].uid+"'>"+data[i].name+"</option>";
                    }
                    $('.deliveryUserOptions_'+order_id).empty().append(selOpts);
                    $("#delivery_user_assign_"+order_id).show();
                }
        });

        $(".assign_del_user").on("click", function()
        {
            console.log("clicked")
            var delUser = $(".deliveryUserOptions_"+order_id).val();
            if(delUser == 0)
            {
                return;
            }
            console.log("delUser",delUser)
            $.ajax({
                    type:"POST",
                    url: SITE_URL + "deliveries/assignDeliveryUser/",
                    data: {
                        'delUser': delUser,
                        'order_id': order_id
                    }, 
                    success: function(res) 
                    {
                        $("#delivery_user_assign_"+order_id).hide();
                        if (ordrno > 0)
                        { 
                            $id = ordrno;
                        }
                        var status = $("#1").val();
                        var store_id = $("#store_id").val();
                        var date;

                        if (window.location.href.indexOf("searchDeliveryData") != -1) date = $("#search_delivery_date").val();

                        $.ajax({
                            type: "POST",
                            url: SITE_URL + "deliveries/managedeliveryData/",
                            data: {
                                'order_id': order_id,
                                'id': id
                            },
                            success: function(data) 
                            {
                                $.ajax({
                                    type: "POST",
                                    url: SITE_URL + "deliveries/searchDeliveryDataAjax/",
                                    data: {
                                        'search_date': date,
                                        'delivery_status': status,
                                        'store_id': store_id
                                    },
                                    success: function(data) {
                                        data = JSON.parse(data);

                                        $(".deliveries-table-detail-wrapper").html("");
                                        $(".deliveries-table-detail-wrapper").html(data.html);
                                        $(".count_new").html(data.count_new);
                                        $(".count_prc").html(data.count_prc);
                                        $(".count_bag").html(data.count_bag);
                                        $(".count_pick").html(data.count_pick);
                                        $(".total_delivery").html(data.total_delivery);

                                    }
                                });
                            }
                        });
                    }
                });
        });
        $("#close-assign-user_"+order_id).click(function() 
        {
            $("#delivery_user_assign_"+order_id).hide();
        }); 
    }
    else
    {

        if (ordrno > 0)
        { 
        
            $id = ordrno;
        }
        var status = $("#1").val();
        var store_id = $("#store_id").val();
        var date;

        if (window.location.href.indexOf("searchDeliveryData") != -1) date = $("#search_delivery_date").val();
        console.log('id '+ id)
        $.ajax({
            type: "POST",
            url: SITE_URL + "deliveries/managedeliveryData/",
            data: {
                'order_id': order_id,
                'id': id
            },
            success: function(data) 
            {

                $.ajax({
                    type: "POST",
                    url: SITE_URL + "deliveries/searchDeliveryDataAjax/",
                    data: {
                        'search_date': date,
                        'delivery_status': status,
                        'store_id': store_id
                    },
                    success: function(data) 
                    {
                        data = JSON.parse(data);
                         console.log("data",data);
                        console.log("data.htmk",data.html);
                        $(".deliveries-table-detail-wrapper").html("");
                        $(".deliveries-table-detail-wrapper").html(data.html);
                        $(".count_new").html(data.count_new);
                        $(".count_prc").html(data.count_prc);
                        $(".count_bag").html(data.count_bag);
                        $(".count_pick").html(data.count_pick);
                        $(".total_delivery").html(data.total_delivery);

                    }
                });
            }
        });
    }

    
}

function managePickupData(order_id, id, ordrno) {
	if (ordrno > 0) admin_ha.common.show_order_detail_popup(ordrno, true);
    var status = $("#1").val();
    var store_id = $("#store_id").val();
    var date = $("#search_pickup_date").val();
    $.ajax({
        type: "POST",
        url: SITE_URL + "pickups/managePickupData/",
        data: {
            'order_id': order_id,
            'id': id
        },
        success: function(data) {

            $.ajax({
                type: "POST",
                url: SITE_URL + "pickups/searchPickupsDataAjax/",
                data: {
                    'search_date': date,
                    'delivery_status': status,
                    'store_id': store_id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    $(".deliveries-table-detail-wrapper").html("");
                    $(".deliveries-table-detail-wrapper").html(data.html);
                    $(".count_new").html(data.count_new);
                    $(".count_prc").html(data.count_prc);
                    $(".count_bag").html(data.count_bag);
                    $(".count_pick").html(data.count_pick);
                    $(".total_pickUps").html(data.total_pickUps);

                }
            });
        }
    });
}


// function manageStoreDelivereis(order_id, id, ordrno) {
    
//     var status = $("#1").val();
//     var store_id = $("#store_id").val();
//     var date;

//     if (window.location.href.indexOf("searchDeliveryData") != -1) date = $("#search_delivery_date").val();
    
//     $.ajax({
//         type: "POST",
//         url: SITE_URL + "storedeliveries/managedeliveryData/",
//         data: {
//             'order_id': order_id,
//             'id': id
//         },
//         success: function(data) {

//             $.ajax({
//                 type: "POST",
//                 url: SITE_URL + "storedeliveries/searchDeliveryDataAjax/",
//                 data: {
//                     'search_date': date,
//                     'delivery_status': status,
//                     'store_id': store_id
//                 },
//                 success: function(data) {
//                     data = JSON.parse(data);
//                     $(".deliveries-table-detail-wrapper").html("");
//                     $(".deliveries-table-detail-wrapper").html(data.html);
//                     $(".count_new").html(data.count_new);
//                     $(".count_prc").html(data.count_prc);
//                     $(".count_bag").html(data.count_bag);
//                     $(".count_pick").html(data.count_pick);
//                     $(".total_delivery").html(data.total_delivery);

//                 }
//             });
//         }
//     });
   
// }

function manageStoreDelivereis(order_id, id, ordrno,order_status) 
{
    
    if(order_status == 5 && id == 6)
    {
        $.ajax({
                type: "POST",
                url: SITE_URL + "storedeliveries/getDeliveryUsers/",
                data: {
                    'order_id': order_id,
                }, 
                success: function(res) 
                {
                    var data = JSON.parse(res);

                    var selOpts = "<option value='0'>SELECT</option>";
                    for (var i = 0; i < data.length; i++) {
                         selOpts += "<option value='"+data[i].uid+"'>"+data[i].name+"</option>";
                    }
                    console.log(selOpts);
                    $('.storedeliveryUserOptions_'+order_id).empty().append(selOpts);
                    $("#storedelivery_user_assign_"+order_id).show();
                }
        });
        $(".assign_storedel_user").on("click", function()
        {
            var delUser = $('.storedeliveryUserOptions_'+order_id).val();
            if(delUser == 0)
            {
                return;
            }
            $.ajax({
                    type:"POST",
                    url: SITE_URL + "storedeliveries/assignDeliveryUser/",
                    data: {
                        'delUser': delUser,
                        'order_id': order_id
                    }, 
                    success: function(res) 
                    {
                        $("#delivery_user_assign_"+order_id).hide();
                        if (ordrno > 0)
                        { 
                        
                            $id = ordrno;
                        }

                        var status = $("#1").val();
                        var store_id = $("#store_id").val();
                        var date;

                        if (window.location.href.indexOf("searchDeliveryData") != -1) {
                            date = $("#search_delivery_date").val();
                        }
                        else
                        {
                            date = $("#search_delivery_date").val();
                        }
                        
                        $.ajax({
                            type: "POST",
                            url: SITE_URL + "storedeliveries/managedeliveryData/",
                            data: {
                                'order_id': order_id,
                                'id': id
                            },
                            success: function(data) {
                                console.log('search_date',date)
                                $.ajax({
                                    type: "POST",
                                    url: SITE_URL + "storedeliveries/searchDeliveryDataAjax/",
                                    data: {
                                        'search_date': date,
                                        'delivery_status': status,
                                        'store_id': store_id
                                    },
                                    success: function(data) {
                                        data = JSON.parse(data);
                                        $(".deliveries-table-detail-wrapper").html("");
                                        $(".deliveries-table-detail-wrapper").html(data.html);
                                        $(".count_new").html(data.count_new);
                                        $(".count_prc").html(data.count_prc);
                                        $(".count_bag").html(data.count_bag);
                                        $(".count_pick").html(data.count_pick);
                                        $(".total_delivery").html(data.total_delivery);

                                    }
                                });
                            }
                        });
                    }
                });
        });
        $("#close-storeassign-user_"+order_id).click(function() 
        {
            $("#storedelivery_user_assign_"+order_id).hide();
        }); 
    }
    else
    {
        if (ordrno > 0)
        { 
        
            $id = ordrno;
        }

        var status = $("#1").val();
        var store_id = $("#store_id").val();
        var date;

        if (window.location.href.indexOf("searchDeliveryData") != -1) {
            date = $("#search_delivery_date").val();
        }
        else
        {
            date = $("#search_delivery_date").val();
        }
        
        $.ajax({
            type: "POST",
            url: SITE_URL + "storedeliveries/managedeliveryData/",
            data: {
                'order_id': order_id,
                'id': id
            },
            success: function(data) {
                console.log('search_date',date)
                $.ajax({
                    type: "POST",
                    url: SITE_URL + "storedeliveries/searchDeliveryDataAjax/",
                    data: {
                        'search_date': date,
                        'delivery_status': status,
                        'store_id': store_id
                    },
                    success: function(data) {
                        data = JSON.parse(data);
                        $(".deliveries-table-detail-wrapper").html("");
                        $(".deliveries-table-detail-wrapper").html(data.html);
                        $(".count_new").html(data.count_new);
                        $(".count_prc").html(data.count_prc);
                        $(".count_bag").html(data.count_bag);
                        $(".count_pick").html(data.count_pick);
                        $(".total_delivery").html(data.total_delivery);

                    }
                });
            }
        });
    }
   
}

function manageStorePickupData(order_id, id, ordrno) {
	if (ordrno > 0) admin_ha.common.show_order_detail_popup_store(ordrno, true);
    var status = $("#1").val();
    var store_id = $("#store_id").val();
    var date = $("#search_pickup_date").val();
    $.ajax({
        type: "POST",
        url: SITE_URL + "storepickups/managePickupData/",
        data: {
            'order_id': order_id,
            'id': id
        },
        success: function(data) {

            $.ajax({
                type: "POST",
                url: SITE_URL + "storepickups/searchPickupsDataAjax/",
                data: {
                    'search_date': date,
                    'delivery_status': status,
                    'store_id': store_id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    $(".deliveries-table-detail-wrapper").html("");
                    $(".deliveries-table-detail-wrapper").html(data.html);
                    $(".count_new").html(data.count_new);
                    $(".count_prc").html(data.count_prc);
                    $(".count_bag").html(data.count_bag);
                    $(".count_pick").html(data.count_pick);
                    $(".total_pickUps").html(data.total_pickUps);

                }
            });
        }
    });
}

