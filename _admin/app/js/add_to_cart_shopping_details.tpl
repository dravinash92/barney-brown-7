<h3>SHOPPING CART</h3>
        <form  id="cartdetails">
         <table class="shopping-cart-table-new">
			<tbody>
				<tr>
				 <th>ORDER DETAILS</th>
				 <th>&nbsp;</th>
				 <th>QTY</th>
				 <th>TOTAL</th>
				 <th>&nbsp;</th>
				</tr>
				{foreach from=$sandwiches key=myId item=sandwich}
				<tr>
				 <td colspan="2">
					 <p>{$sandwich.sandwich_name}_________________________________</p>
					<p>{$sandwich.data_string}</p>
				 </td>
				 <td>
					<span class="arrow-holder">
				   <a href="#" class="left arrow-left"></a>
				   <input type="text" class="qty" name="qty[]" value="{$sandwich.item_qty}">
				   <a href="#" class="arrow-right"></a>
				   <input type="hidden" name="data_id" value="{$sandwich.id}" />
				   <input type="hidden" name="data_type" value="user_sandwich" />
				   <input type="hidden" name="data_order_id" value="{$sandwich.order_item_id}" />
				 </span>
				 </td>
				 <td>${$sandwich.item_price}</td>
				 <td><a href="javascript:void(0)" class="remove remove_cart_item">REMOVE</a></td>
				</tr>
				{/foreach}
				
				{foreach from=$products key=myId item=product}
				<tr>
				 <td colspan="2"><p>{$product->product_name} ______________________________{if $product->extra_name }<br/> {$product->extra_name}{/if}</p></td>
				 <td>
					<span class="arrow-holder">
				   <a href="#" class="arrow-left"></a>
				   <input type="text" class="qty" name="qty[]" value="{$product->item_qty}">
				   <a href="#" class="arrow-right"></a>
				   <input type="hidden" name="data_id" value="{$product->id}" />
				   <input type="hidden" name="data_order_id" value="{$product->order_item_id}" />
				   <input type="hidden" name="data_type" value="product" />
				 </span>
				 </td>
				 <td>${$product->item_price}</td>
				 <td><a href="javascript:void(0)" class="remove remove_cart_item">REMOVE</a></td>
				</tr>
				{/foreach}
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">SUBTOTAL</td>
            <td>${$subtotal} </td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">TIP</td>
            <td colspan="2">
              <select name="tips_selected" class="select-tip">
                <option {if $tips eq 3} selected {/if} value="3">$3.00</option>
                <option {if $tips eq 3.50} selected {/if} value="3.50">$3.50</option>
                <option {if $tips eq 4} selected {/if} value="4">$4.00</option>
                <option {if $tips eq 4.50} selected {/if} value="4.50">$4.50</option>
                <option {if $tips eq 5} selected {/if} value="5">$5.00</option>
                <option {if $tips eq 5.50} selected {/if} value="5">$5.50</option>
                <option {if $tips eq 6} selected {/if} value="6">$6.00</option>
                <option {if $tips eq 6.50} selected {/if} value="6.50">$6.50</option>
                <option {if $tips eq 7} selected {/if} value="7">$7.00</option>
                <option {if $tips eq 7.50} selected {/if} value="7.50">$7.50</option>
                <option {if $tips eq 8} selected {/if} value="8">$8.00</option>
                <option {if $tips eq 8.50} selected {/if} value="8.50">$8.50</option>
                <option {if $tips eq 9} selected {/if} value="9">$9.00</option>
                <option {if $tips eq 9} selected {/if} value="9.50">$9.50</option>
                <option {if $tips eq 10} selected {/if} value="10">$10.00</option>                
                <option {if $tips eq 10.50} selected {/if} value="10.50">$10.50</option>                
              </select>
            </td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TAX</h4></td>
            <td><h4>${$tax}</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL VALUE</h4></td>
            <td><h4>${$total_price}</h4></td>
            <td>&nbsp;</td>
           </tr>
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">DISCOUNT:  {$off_type} OFF SPECIAL</td>
            <td>-${$off_value}</td>
            <td>&nbsp;</td>
           </tr>
		   
		   {if $manual_discount}
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2">MANUAL DISCOUNT: </td>
            <td>-${$manual_discount}</td>
            <td>&nbsp;</td>
           </tr>
		   {/if}
           
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">DISCOUNT / GIFT CARD</p>
            	<span class="text-box-holder">
              	<input name="" type="text" class="text-box coupon_code">
                <a href="javascript:void(0)" class="shopping-cart-select apply_coupon">Apply</a>
              </span>
            </td>
           </tr>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="4">
            <p class="tag-contorl">MANUAL DISCOUNT</p>
            	<span class="text-box-holder">
              	<input name="manual_discount_val" type="text" class="text-box manual_discount" value="">
                <a href="javascript:void(0)" class="shopping-cart-select apply_manual_discount">Apply</a>              
              </span>
            </td>
           </tr>
           <tr>
           	<td>&nbsp;</td>
            <td colspan="2"><h4>TOTAL OWED</h4></td>
            <td><h4>${$grand_total}</h4></td>
            <td>&nbsp;</td>
           </tr>
         </tbody>
        </table>
		
		<input	type="hidden" name="order_string" value="{$order_string}" />
		<input	type="hidden" name="grand_total" value="{$grand_total}" />
		<input	type="hidden" name="sub_total" value="{$subtotal}" />
		<input	type="hidden" name="coupon_id" value="{$coupon_id}" />
		<input	type="hidden" name="coupon_code" value="{$coupon_code}" />
		<input	type="hidden" name="manual_discount" value="{$manual_discount}" />
		<input	type="hidden" name="tips" value="{$tips}" />
		<input	type="hidden" name="tax" value="{$tax}" />
		<input	type="hidden" name="off_type" value="{$off_type}" />
		<input	type="hidden" name="off_amount" value="{$off_value}" />
		
		</form>			
    
