var ha = {};
ha.sandwichcreator = window.ha.sandwichcreator || {
    vars: {
        temp_id: 0,
        cheese_min: 0,
        AjaxURL: {
            BREAD: "createsandwich/load_bread_data/",
            PROTEIN: "createsandwich/load_protien_data/",
            CHEESE: "createsandwich/load_cheese_data/",
            TOPPINGS: "createsandwich/load_toppings_data/",
            CONDIMENTS: "createsandwich/load_condiments_data/"
        },
        manual_mode: false,
        current_sliced_image: null,
        ACTIVE_MENU: $('.create-sandwich-menu ul li a[class="active"]').text(),
        selection_data: null,
        validators: ["BREAD", "PROTEIN", "CHEESE"],
        bread_type: null,


        image_path: SITE_URL + 'upload/',
        current_price: 0,
        individual_price: {},
        filterArray : [],

        ingredient_details: {
            BREAD: {
                item_name: [],
                item_price: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                type: 0, // either 3 foot or 6 foot or normal [0-normal,1-3-foot,2-6-foot]
                shape: 0, // either long,round,trapezoid [0-long,1-3-round,2-6-trapezoid]
                actual_price: {}
            },
            PROTEIN: {
                manual_mode: false,
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            CHEESE: {
                manual_mode: false,
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            TOPPINGS: {
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            CONDIMENTS: {
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            }
        }
    },


    init: function() {
        var e = this;
        e.vars.bread_type = e.get_url_params('view');
        if (e.get_url_params('view')) $('.landing-page').remove();
        $(window).load(function() {
            e.retrive_data_onload();
        });
        e.ingredient_enable();
        e.slide_function();
        $("#finished").unbind("click").on("click", function() {



            error = e.validate($(this).text());
            if (error == true) {
                return false
            } else {

                if (SESSION_ID == "" || SESSION_ID == null) {}  
                else
                    e.show_end()

            }
        });

        e.gallery_search();
        e.select_from_gallery();
        e.lazy_load_sandwiches();

    },

    Ajax_busy: false,
    
    lazy_load_sandwiches: function() {

        var e = this;
        var set = 0;
        $(window).load(function() {
            if (window.location.href.indexOf("sandwich") != -1) {
            	
                $(window).scroll(e.bind_scrolls);
            }
        });
    },
   bind_scrolls: function() {
        var $current_count = parseInt($('.adminGalleryList .sandwichlist').length);
        var $total_items = parseInt($('input[name="total_item_count"]').val());
       
        if ($(window).scrollTop() > 1000) {
                if (ha.sandwichcreator.Ajax_busy == false && $('input[name="search"]').length == 0) {
                    ha.sandwichcreator.load_more_sandwiches();
                }
            }

       /*
        if ($('.adminGalleryList .sandwichlist').length < 20) {
            if ($(window).scrollTop() > 500) {
                if (ha.sandwichcreator.Ajax_busy == false && $('input[name="search"]').length == 0) {
                    ha.sandwichcreator.load_more_sandwiches();
                }
            }
        } else {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                setTimeout(function() {
                    if (ha.sandwichcreator.Ajax_busy == false && ($current_count < $total_items) && $('input[name="search"]').length == 0) {
                    	ha.sandwichcreator.load_more_sandwiches();
                    }
                }, 300);
            }
        }*/
    },

load_more_sandwiches: function() {

        var self = this;
        var Array = {};

        Array['BREAD'] = [];
        Array['PROTEIN'] = [];
        Array['CHEESE'] = [];
        Array['TOPPINGS'] = [];
        Array['CONDIMENTS'] = [];
        $txt = $('.leftside-bar input[type="checkbox"]').parent().next().text();
        $txt = $txt.trim();
        parent = $('.leftside-bar input[type="checkbox"]').parent().parent().parent();
        $key = $(parent).prev('h3').text()
        $key = $key.trim();
        $key = $key.toUpperCase();

        // switch ($key) {

        //     case 'BREAD':

        //         if ($(this).prop('checked') == true) {
        //             if ($.inArray($txt, Array['BREAD']) == -1) {
        //                 Array['BREAD'].push($txt);
        //             }
        //         } else {
        //             if ($.inArray($txt, Array['BREAD']) != -1) {
        //                 Array['BREAD'].splice($.inArray($txt, Array['BREAD']), 1);
        //             }
        //         }
        //         break;

        //     case 'PROTEIN':
        //         if ($(this).prop('checked') == true) {
        //             if ($.inArray($txt, Array['PROTEIN']) == -1) {
        //                 Array['PROTEIN'].push($txt);
        //             }
        //         } else {
        //             if ($.inArray($txt, Array['PROTEIN']) != -1) {
        //                 Array['PROTEIN'].splice($.inArray($txt, Array['PROTEIN']), 1);
        //             }
        //         }
        //         break;

        //     case 'CHEESE':
        //         if ($(this).prop('checked') == true) {
        //             if ($.inArray($txt, Array['CHEESE']) == -1) {
        //                 Array['CHEESE'].push($txt);
        //             }
        //         } else {
        //             if ($.inArray($txt, Array['CHEESE']) != -1) {
        //                 Array['CHEESE'].splice($.inArray($txt, Array['CHEESE']), 1);
        //             }
        //         }
        //         break;

        //     case 'TOPPINGS':
        //         if ($(this).prop('checked') == true) {
        //             if ($.inArray($txt, Array['TOPPINGS']) == -1) {
        //                 Array['TOPPINGS'].push($txt);
        //             }
        //         } else {
        //             if ($.inArray($txt, Array['TOPPINGS']) != -1) {
        //                 Array['TOPPINGS'].splice($.inArray($txt, Array['TOPPINGS']), 1);
        //             }
        //         }
        //         break;

        //     case 'CONDIMENTS':
        //         if ($(this).prop('checked') == true) {

        //             if ($.inArray($txt, Array['CONDIMENTS']) == -1) {
        //                 Array['CONDIMENTS'].push($txt);
        //             }
        //         } else {
        //             if ($.inArray($txt, Array['CONDIMENTS']) != -1) {
        //                 Array['CONDIMENTS'].splice($.inArray($txt, Array['CONDIMENTS']), 1);
        //             }
        //         }
        //         break;

        // }
        
        Array  = self.filterArray;
        var sortBy = $('.sortby').val();
        var $string = JSON.stringify(Array);
        
        var searchTerm = $("#searchTerm").val();
      
        $('.loadMoreGallery').show();
        $current_count = $('.adminGalleryList .sandwichlist').length;
        $total_items = $('input[name="total_item_count"]').val();
        sort_id = $('input[name="sort_type"]').val();

        //if (parseInt($current_count) < parseInt($total_items)) {
        //
        if( $(document).scrollTop() > $(document).height(  ) * 0.75 ){
             $current_count = $current_count + 50;
              console.log( "Fire GET: " + $current_count );
                 

            $.ajax({
                type: "POST",
                data: {
                	"count": $current_count,
                    'filter' : $string ,
                    'sortBy' : sortBy,
                    'searchTerm' : searchTerm,
                    "sort_id": sort_id
                },
                url: SITE_URL + 'sandwich/get_more_gallery',
                beforeSend: function( e )
                {
                      self.Ajax_busy = true;
                  },
                success: function(e) {
                    $('.adminGalleryList tr:last').after(e);
                    self.Ajax_busy = false;
                    $('.loadMoreGallery').hide();
                }
            });
        } else {
            $('.loadMoreGallery').hide();
        }

    },

gallery_search: function() {
        var Array = {};

        Array['BREAD'] = [];
        Array['PROTEIN'] = [];
        Array['CHEESE'] = [];
        Array['TOPPINGS'] = [];
        Array['CONDIMENTS'] = [];

        var $protien, $condiments, $toppings, $cheese;


        var self = this;
        var $sel;
        var parent;
        $(".sortby").change(function () {
            var sort_id = $(this).val();
            window.location.assign("" + SITE_URL + "sandwich/index/?sortid=" + sort_id + "");
        });
        
        $('.leftside-bar input[type="checkbox"]').unbind('click').on('click', function() {

            $('.adminGalleryList .sandwichlist').show();
            $txt = $(this).parent().next().text();
            $txt = $txt.trim();
            parent = $(this).parent().parent().parent();
            $key = $(parent).prev('h3').text()
            $key = $key.trim();
            $key = $key.toUpperCase();

            switch ($key) {

                case 'BREAD':

                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['BREAD']) == -1) {
                            Array['BREAD'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['BREAD']) != -1) {
                            Array['BREAD'].splice($.inArray($txt, Array['BREAD']), 1);
                        }
                    }
                    break;

                case 'PROTEIN':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['PROTEIN']) == -1) {
                            Array['PROTEIN'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['PROTEIN']) != -1) {
                            Array['PROTEIN'].splice($.inArray($txt, Array['PROTEIN']), 1);
                        }
                    }
                    break;

                case 'CHEESE':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['CHEESE']) == -1) {
                            Array['CHEESE'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['CHEESE']) != -1) {
                            Array['CHEESE'].splice($.inArray($txt, Array['CHEESE']), 1);
                        }
                    }
                    break;

                case 'TOPPINGS':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['TOPPINGS']) == -1) {
                            Array['TOPPINGS'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['TOPPINGS']) != -1) {
                            Array['TOPPINGS'].splice($.inArray($txt, Array['TOPPINGS']), 1);
                        }
                    }
                    break;

                case 'CONDIMENTS':
                    if ($(this).prop('checked') == true) {

                        if ($.inArray($txt, Array['CONDIMENTS']) == -1) {
                            Array['CONDIMENTS'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['CONDIMENTS']) != -1) {
                            Array['CONDIMENTS'].splice($.inArray($txt, Array['CONDIMENTS']), 1);
                        }
                    }
                    break;

            }
            var searchTerm = $("#searchTerm").val();
            // check to make sure, if no filters selected, then the massive data of all sandwiches is in play, which takes special consideration, so pop back to our base page. 
            if( Array['BREAD'].length == 0 && Array['TOPPINGS'].length == 0 && Array['CONDIMENTS'].length == 0 &&  Array['CHEESE'].length == 0 &&  Array['PROTEIN'].length == 0 )
            {

                window.location.href = 'sandwich/index/?search='+searchTerm;
                window.location.reload ( true );
                return;
            }
            
            self.filterArray = Array;
            var sortBy = $('.sortby').val();
            
            var $string = JSON.stringify(Array);
            $.ajax({
	               type: "POST",
	               data: {   'filter' : $string , 'sortBy' : sortBy, 'searchTerm':searchTerm  },
	               url: SITE_URL + 'sandwich/filters/',
	               beforeSend: function() {
	                    $("#ajax-loader").show();
	                },
	                complete: function() {
	                    $("#ajax-loader").hide();
	                },
	               success: function(e) { 
	            	   $('.adminGalleryList tr:not(:first)').remove();
	            	   $( ".adminGalleryList tr.static").after(e);
	            	   $("#ajax-loader").hide();
	            	   self.Ajax_busy = false;
	            	   self.bind_scrolls();
	               }
	           });
            
        });

    },
    
    /**/
    
    get_url_params: function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        return results == null ? null : results[1];
    },


 
    gb: 0,

    destroy_endscreen_var: function() {
        admin_ha.common.setCookie('sandwich_end_screen', false, 1);
    },



    select_from_gallery: function() {

        $(".choose-from-gallery").click(function() {
            $("#sandwich-gallery").show();
        });

        $(".selected-gallery-item").click(function() {
            $id = $(this).attr("data-id");
            $.ajax({
                type: "POST",
                data: {
                    'sandwich_id': $id
                },
                url: SITE_URL + 'createsandwich/setTempSandwichesToShow/',
                success: function(e) {
                    window.open(SITE_URL + 'neworder/addToCart/' + SESSION_ID, '_self');
                }
            });
        });

    },

    add_sandwich_to_cart: function(data_id, user_id, data_type, qty) {

        if (!qty) qty = 1;

        var state = false;

        var data = {
            "data_id": data_id,
            "data_type": data_type,
            "user_id": user_id,
            "qty": qty
        };

        $.ajax({
            type: "POST",
            data: data,
            url: SITE_URL + 'neworder/addItemToCart',
            async: false,
            success: function(e) {
                 state = true;
            }
        });

        return state;

    },


     
    retrive_data_onload: function() {
        var self = this;


        $.ajax({
            type: "POST",
            data: {
                'id': DATA_ID,
                'uid': SESSION_ID
            },
            url: SITE_URL + 'createsandwich/get_sandwich_data/',
            dataType: 'json',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
                if (self.vars.ingredient_details["BREAD"].item_name.length == 0) self.auto_selection();
                if (self.vars.ingredient_details["BREAD"].item_name.length > 0) {
                    $('.landing-page').remove();
                    $('.button-holder').show();
                }
            },
            success: function(e) {

                self.auto_selection();
                if (e.Response['status-code'] == 200) {
                    if (e.Data[0].id) {
                        self.temp_id = e.Data[0].id;
                        self.temp_name = e.Data[0].sandwich_name;



                    } else {
                        self.temp_id = '';
                    }
                    $sanwch = e.Data[0].sandwich_data;
                    price = parseFloat(e.Data[0].sandwich_price);
                    if ($sanwch) {
                        json_data = JSON.parse($sanwch);

                        if (json_data) {
                            self.vars.ingredient_details = json_data;

                            self.load_all_data(json_data);
                            self.reload_state();
                        }
                    }

                    if (admin_ha.common.getCookie('sandwich_end_screen') == 'true') {
                        self.show_end();
                    }

                }


            }
        });



    },


    auto_selection: function() {
        var self = this;
        if (self.get_url_params('view') != null) {
            switch (self.get_url_params('view')) {

                case '3foot':
                    $3foot = $('#optionList input[data-bread_type="1"]');
                    if ($3foot.length == 0) {
                        alert("Please add a 3 foot bread!");
                        window.location.href = SITE_URL + 'catering/';
                    } else {
                        $3foot.trigger('click');
                        self.vars.ingredient_details["BREAD"].type = 1;
                        $('.landing-page').remove();
                    }
                    break;
                case '6foot':
                    $6foot = $('#optionList input[data-bread_type="2"]');

                    if ($6foot.length == 0) {
                        alert("Please add a 6 foot bread!");
                        window.location.href = SITE_URL + 'catering/';
                    } else {
                        $6foot.trigger('click');
                        self.vars.ingredient_details["BREAD"].type = 2;
                        $('.landing-page').remove();
                    }
                    break;
            }
        }
        
    },

    load_all_data: function($data) {
        var $html = '';
        var self = this;
        var itms = {};
        var htmlELm = {}
        Object.keys($data).forEach(function(e) {
            var priorityOb = {};
            if ($data[e].item_name != "NULL") {
                $images = $data[e].item_image;
                $names = $data[e].item_name;
                $priority = $data[e].item_priority;

                $len = $names.length;
                for (i = 0; i < $len; i++) {
                    if (e != 'BREAD') {
                        $rel = $names[i];

                    } else {
                        $rel = '';
                    }

                    $img = self.vars.image_path + $images[i];
                    if (e != 'BREAD') {
                        priorityOb[$priority[i]] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    } else {

                        priorityOb[0] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    }



                }

                htmlELm[e] = priorityOb;
                $html += self.sandwich_image_wrapping(e, htmlELm);
            }
        });



        $(".bread_images").html("" + $html + "");
        $(".bread_images .image-holder img").fadeIn(300);
    },

    sandwich_image_wrapping: function(type, html) {

        var finalData = {
            'BREAD': '',
            'PROTEIN': '',
            'CHEESE': '',
            'TOPPINGS': '',
            'CONDIMENTS': ''
        };
        $data = html[type];
        $keys = Object.keys($data);
        $keys.forEach(function(keys) {
            finalData[type] += $data[keys];
        });


        switch (type) {
            case 'BREAD':
                $data = '<span class="bread_image">' + finalData[type] + '</span>';
                break;
            case 'PROTEIN':
                $data = '<span class="protein_image">' + finalData[type] + '</span>';
                break;
            case 'CHEESE':
                $data = '<span class="cheese_image">' + finalData[type] + '</span>';
                break;
            case 'TOPPINGS':
                $data = '<span class="topping_image">' + finalData[type] + '</span>';
                break;
            case 'CONDIMENTS':
                $data = '<span class="condiments_image">' + finalData[type] + '</span>';
                break;
        }



        return $data;
    },




    //price calculator
    calculate_price: function() {
        var final_price = 0;
        var self = this;
        self.get_min_cheese_price();
        $data = self.vars.ingredient_details;
        Object.keys($data).forEach(function(e) {
            $price = $data[e].item_price;
            temp = 0;
            $name = $data[e].item_name;


            $name.forEach(function(t, i) {
                 

                if ($price[i] !== undefined && isNaN($price[i]) != true) {
                    temp += parseFloat($price[i]);
                    self.vars.individual_price[e] = temp.toFixed(2);
                }


            });
            if ($name.length == 0) self.vars.individual_price[e] = 0;
           

        });

        if (self.vars.ingredient_details["BREAD"].type != 0) {
            self.sum_individual();
            self.vars.current_price = self.vars.ingredient_details["BREAD"].item_price[0];
        } else {
            self.vars.current_price = self.sum_individual();
            self.vars.current_price = parseFloat(self.vars.current_price)+parseFloat(BASE_FARE);
            self.vars.current_price=self.vars.current_price.toFixed(2);
        }

        $(".price").text("$" + self.vars.current_price);
         
    },


    adjust_cheese_price: function() {
        var self = this;
        var $sum = 0;
        if (self.vars.ingredient_details.CHEESE.item_qty) {
            $keys = Object.keys(self.vars.ingredient_details.CHEESE.item_qty)
            $len = $keys.length;
            if ($len > 0) {
                $keys.forEach(function(ei) {
                    $sum += self.vars.ingredient_details.CHEESE.item_qty[ei][2];
                });
            }
        }

        if ($sum > 1) {
            cheese_price = parseFloat(self.vars.individual_price.CHEESE);
            cheese_price = cheese_price - self.vars.cheese_min;
        } else {
            cheese_price = 0;
        }
        return cheese_price;
    },

    get_min_cheese_price: function() {
        var self = this;
        var array = [];
        self.vars.ingredient_details.CHEESE.item_name.forEach(function(e) {
            if (self.vars.ingredient_details.CHEESE.actual_price[e] > 0) array.push(self.vars.ingredient_details.CHEESE.actual_price[e]);
        });
        min = Math.min.apply(Math, array);
        if (min) {
            self.vars.cheese_min = min;
        }

    },

    sum_individual: function() {
        $data = this.vars.individual_price;
        var finalprice = 0;
        Object.keys($data).forEach(function(e) {
            finalprice += parseFloat($data[e]);
        });

        return finalprice.toFixed(2);
    },

    sync_data_to_db: function(is_pub, sandwichname, menu_active, tempname, returnid) {

        var self = this;
        var toast = 0;
        if ($("#ov_check1").is(':checked')) {
            toast = 1;
        }

        var state;
        //screening userinputs.

        if (!sandwichname) {
            alert("Name your sandwich and continue!");
            return false;

        } else {

            $.ajax({
                type: "POST",
                data: {
                    'word': sandwichname,
                },
                async: false,
                url: SITE_URL + 'createsandwich/filter_words/',
                dataType: 'json',
                success: function(e) {
                    jso = JSON.parse(e);
                    state = jso.Response["status-code"];
                }
            });
            
            if (state == 500) {
                alert("Sorry, this name is not allowed due to inappropriate content.  Please try again.");
                return false;
            }

        }
        user_data = JSON.stringify(self.vars.ingredient_details);
        if (tempname != sandwichname) {
            tempname = 1;
        } else {
            tempname = 0;
        }



        $.ajax({
            type: "POST",
            data: {
                'tempname': tempname,
                'menu_active': menu_active,
                'id': self.temp_id,
                'uid': SESSION_ID,
                'data': user_data,
                'price': self.vars.current_price,
                'is_pub': is_pub,
                'name': sandwichname,
                'toast': toast
            },
            async: false,
            url: SITE_URL + 'createsandwich/user_input/',
            dataType: 'json',
           
            success: function(e) {
                if (e.Response['status-code'] == 200 || e.Response['status-code'] == 201) {
                    self.destroy_endscreen_var();

                    if (!DATA_ID) {
                        $id = e.Data;
                    } else if (tempname == 1) {
                        $id = e.Data;
                    } else {
                        $id = DATA_ID;
                    }

                    imgStore = self.create_sandwitch_image(463, 344, $id);


                    if (imgStore == true) {

                        if (returnid != true) {
                             
                            window.open(SITE_URL + 'neworder/addToCart/' + SESSION_ID, '_self');
                        } else {

                            state = self.add_sandwich_to_cart($id, SESSION_ID, "user_sandwich");
                            if (state == true) window.open(SITE_URL + 'neworder/addToCart/' + SESSION_ID, '_self');

                        }
                    }

                }


                $.ajax({
                    type: "POST",
                    data: {
                        'sandwich_id': $id
                    },
                    url: SITE_URL + 'createsandwich/setTempSandwichesToShow/',
                    success: function(e) {
                    },
                });

            }




        });

    },



    create_sandwitch_image: function(width, height, id) {
        var _imgs = [];
        var $final;
        var ret = false;
        $('.image-holder img').each(function() {

            src = $(this).attr('src');
            Arr = src.split('/');
            img = Arr[Arr.length - 1];
            _imgs.push(img);

        });

        $final = JSON.stringify({
            images: _imgs
        });



        $.ajax({
            type: "POST",
            data: {
                'json_string': $final,
                'width': width,
                'height': height,
                'file_name': 'sandwich_' + id + '_' + SESSION_ID + '.png',
                'path': SESSION_ID + '/'
            },
            url: SITE_URL + 'createsandwich/save_image/',
            async: false,
            success: function(e) {
                ret = true;
            }
        });

        
        return ret;
    },


    ingredient_enable: function() {
        var e = this;
        $selector = $(".create-sandwich-menu ul li a");
        $selector.unbind("click").on("click", function() {
            error = e.validate($(this).text());
            if (error == true) {
                return false
            }

            if ($(this).text() != 'BREAD') e.turn_bread_image();

            e.vars.ACTIVE_MENU = $(this).text();
            e.load_side_menu();
            $selector.removeClass("active");
            $(this).addClass("active")
        });
        this.enable_nav_buttons();
        this.user_selections()
    },


    slide_function: function() {
        var e = this;
        $("#optionList .left, #optionList .right").unbind("click").on("click", function(t) {
            t.preventDefault();
            $parent = $(this).parent();
            e.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode = true;
            $ptext = $parent.parent().find('input[type="checkbox"]').data();
            $total = $parent.find("input").length - 1;
            $inputs = $parent.find("input");
            $curr_index = $parent.find("input:visible").index();
            $curr_index_t = $curr_index + 1;
            if ($curr_index > 0) {
                $prv_index = $curr_index - 1
            } else {
                $prv_index = 0
            }
            if ($curr_index < $total) {
                $nxt_index = $curr_index + 1
            } else {
                $nxt_index = $total
            }

            if (t.target.className.indexOf("right") != -1) {
                $parent.find("input:visible").hide();
                $($parent.find("input")[$nxt_index]).show();
                $ind = $parent.find("input:visible").index();

                if ($ind == $total) {

                    $(this).removeClass("right-hover");
                    $(this).parent().find('.left').addClass("left-hover");
                }
                if ($ind > 0) {
                    $(this).parent().find('.left').addClass("left-hover");
                }


            } else {
                $parent.find("input:visible").hide();
                $($parent.find("input")[$prv_index]).show();
                $ind = $parent.find("input:visible").index();

                if ($ind == 0) {

                    $(this).removeClass("left-hover");
                    $(this).parent().find('.right').addClass("right-hover");
                }

                if ($ind < $total) {
                    $(this).parent().find('.right').addClass("right-hover");
                }

            }

            $arr = [];
            $value = $parent.find("input:visible").val();
            $data = $parent.find("input:visible").data();
            $arr[0] = $value;
            $arr[1] = $data.unit;
            $arr[2] = $data.price_mul;
            $arr[3] = $data.image;
            e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$ptext.itemname] = $arr
            $itn_id = $ptext.id;
            new_price = parseFloat($ptext.price) * parseFloat($arr[2]);
            $p_index = $.inArray($itn_id, e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_id);
            if ($p_index != -1) {
                e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_price[$p_index] = new_price;
                e.calculate_price();
            }

        });

        e.slider_button_fix();
    },



    load_side_menu: function() {
        var e;
        var t = this;
        $.ajax({
            type: "GET",
            url: SITE_URL + t.vars.AjaxURL[t.vars.ACTIVE_MENU],
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },

            success: function(e) {
                $datax = $(e).find('input').not('.sub-value input');
                

                $datax.each(function() {
                    itmDat = $(this).data();
                    if (itmDat.itemname != "NULL") {
                        t.vars.ingredient_details[t.vars.ACTIVE_MENU].actual_price[itmDat.itemname] = itmDat.price;
                    }
                });

                $("#optionList").html(e);
                t.user_selections();
                t.enable_scrolbar();
                t.reload_state();
                t.slide_function()
            }
        })
    },


    reload_state: function() {
        var e = this;
        var t;
        var n;
        var r;
        if (!e.vars.ACTIVE_MENU) {
            e.vars.ACTIVE_MENU = 'BREAD';

        }
        t = e.vars.ingredient_details[e.vars.ACTIVE_MENU];

        if (e.vars.ACTIVE_MENU == 'BREAD' && t.item_id) {
            $data = $('.main-category-list ul li input[data-id="' + t.item_id + '"]').data();
            if ($data) e.vars.current_sliced_image = $data.item_image_sliced;
        }


        t.item_name.forEach(function(e) {
            $('input[data-itemname="' + e + '"]').prop("checked", true);
            n = e;

            $(".sub-value").each(function() {
                if ($(this).parent().find("input:checked").length > 0) {
                    $(this).show();


                    r = t.item_qty[n];

                    if (r != undefined && $(this).parent().find("input[data-itemname='" + n + "']").length > 0) {
                        $(this).find("input").hide();
                        $(this).find('input[value="' + r[0] + '"]').show()
                    }
                }
            });
        });

        e.calculate_price();
        e.slider_button_fix();
    },


    enable_nav_buttons: function() {
        var e = this;
        $(".back,.next").unbind("click").on("click", function(t) {
            e.turn_bread_image();
            $total = $(".create-sandwich-menu ul li").length - 1;
            $slector = $('.create-sandwich-menu ul li a[class="active"]').parent();
            $length = $slector.index();
            if ($(t.target).attr("class") == "back") {
                if ($length != 0) $slector.prev().find("a").trigger("click")
            } else {
                if ($length < $total) $slector.next().find("a").trigger("click");
                if ($length == $total) {
                    e.show_end()
                }
            }
        })
    },

    randomBetween: function(min, max) {
        if (min < 0) rand = min + Math.random() * (Math.abs(min) + max);
        else rand = min + Math.random() * max;
        return rand = Math.floor(rand);
    },



    show_end: function() {
        var self = this;


        var funWord;
        $.ajax({
            type: "POST",
            async: false,
            url: SITE_URL + 'createsandwich/getRandom_sandwich_name/',
            success: function(e) {
                funWord = e;
            }
        });

 
        $rand = self.randomBetween(0, self.vars.ingredient_details.PROTEIN.item_name.length);
        var $prot;

        proteins = self.vars.ingredient_details.PROTEIN.item_name;
        cheeses = self.vars.ingredient_details.CHEESE.item_name;

        $noFirstName = false;

        if (proteins.length > 2) {
            $noFirstName = false;
            $sandwh = 'Protein'
        } else if (proteins.length == 2) {
            $noFirstName = true;
            $sandwh = proteins[0] + ' & ' + proteins[1];

        } else if (proteins.length == 1) {
            $noFirstName = false;
            $sandwh = proteins[0];
        } else if (proteins.length == 0) {

            if (cheeses.length > 2) {
                $sandwh = 'Cheesy';
                $noFirstName = false;
            } else if (cheeses.length == 2) {
                $sandwh = cheeses[0] + ' & ' + cheeses[1];
                $noFirstName = true;
            } else if (cheeses.length == 1) {
                $noFirstName = false;
                $sandwh = cheeses[0];
            }
        }

        if (proteins.length == 0 && cheeses.length == 0) {
            $sandwh = 'Veggie';
        }



        $sandwichName = $('#namecreation').val();
        $sandwichName_split = $sandwichName.split(' ');
        $ln = $sandwichName_split.length - 1;

        $last = $sandwichName_split[$ln];

        $first = $('input[name="user_fname"]').val();
        if ($first[0]) $first = $first[0].toUpperCase() + $first.slice(1) + "'s";
        else $first = '';




        if (funWord) $last = funWord;
        $sandwichNamePart2 = $sandwh + " " + $last;
        $nameLen = parseInt($first.length) + parseInt($sandwichNamePart2.length);


        if ($noFirstName == false && $nameLen < 35 && $first) $sandwichName = $first + " " + $sandwichNamePart2
        else $sandwichName = $sandwichNamePart2;

        $('#namecreation').val($sandwichName);


        $data = this.prepare_endScreen();

        if(self.vars.ingredient_details["BREAD"].type == 0)
        	$("#final_out").html('<li style="margin-top:0px"></li> <li style="border-bottom: 1px solid;margin-bottom: 8px;margin-top:8px;"><h4 style="text-decoration: none !important;">BASE PRICE <span>+$'+BASE_FARE+'.00</span></h4></li>' + $data);
        else
            $("#final_out").html('<li style="margin-top:0px"></li>' + $data);
        

        $('#totalPrice').html('$' + self.vars.current_price);
        $(".create-sandwich-menu").hide();


        $(".bread_images").css("margin", "36px 0 0 0");
        $(".create-sandwich-right-wrapper").hide();

        $("#ovrlayx").show();
        $("#ovrlayx").find(".create-sandwich-right-wrapper").show();
        $(".back").parent().parent().parent().hide();

        $(".edit-sandwich").unbind("click").on("click", function() {
            $("#ovrlayx").hide();
            $(".create-sandwich-menu").show();
            $(".bread_images").css("margin", "");
            $(".create-sandwich-right-wrapper").show();
            $(".back").parent().parent().parent().show();

        });

        $('#namecreation').focus();


        $('.save-to-my-menu,.am-done').not('#finished').unbind('click').on('click', function(e) {
            e.preventDefault();



            var active;
 
            if ($(e.target).attr('class') == 'save-to-my-menu') {
                active = 1;
                retchk = false;
            } else if ($(e.target).attr('class') == 'am-done') {
                active = 1;
                retchk = true;
            } else {
                active = 1;
                retchk = true;
            }

            is_pub = $('#ov_check2:checked').length;
            name_cr = $('#namecreation').val();

            self.sync_data_to_db(is_pub, name_cr, active, self.temp_name, retchk);

        });


    },




    prepare_endScreen: function() {
        $out = "";
        $i = 0;
        var self = this;
        self.calculate_price();
        $data = self.vars.ingredient_details;
        Object.keys($data).forEach(function(e) {

           

            if (self.vars.individual_price[e] < 0) sign = '-';
            else sign = '+';
            vals = Math.abs(self.vars.individual_price[e]);
            $out += "<li>";
            $out += "<h4>" + e + " <span>" + sign + "$" + vals.toFixed(2) + "</span></h4>";
            if (e == "BREAD") {
                $out += "<p>" + $data[e].item_name[0] + "</p>"
            } else {
                $sub = "";
                $en_ln = $data[e].item_name.length;
                $qty = $data[e].item_qty;
                $data[e].item_name.forEach(function(e) {
                    $i++;
                    if ($qty[e] != undefined) {
                        if (e != "NULL") $final_name = e + "(" + $qty[e][1] + ")";
                        else $final_name = "None"
                    } else {
                        if (e != "NULL") $final_name = e + "(1)";
                        else $final_name = "None"
                    }
                    if ($i < $en_ln) {
                        delimit = ","
                    } else {
                        delimit = ""
                    }
                    $sub += $final_name + delimit + " "
                });

                if ($data[e].item_name.length > 0) {
                    $out += "<p>" + $sub + "</p>";
                } else {
                    $out += "<p>" + "None" + "</p>";
                }


            }
            $out += "</li>"
        });
        return $out
    },



    ing_availaility_check: function() {
        var self = this;
        count = 0;
        Object.keys(self.vars.ingredient_details).forEach(function(e) {
            if (e != 'BREAD') count += self.vars.ingredient_details[e].item_id.length;
        });
        return count;
    },

    dynamic_option_selection: function(selector, dataAdding) {
         
        var self = this;
        ACTIVE_TAB = self.vars.ACTIVE_MENU;
        if (dataAdding == true) inc = 1;
        else inc = 0
        len = self.vars.ingredient_details[ACTIVE_TAB].item_name.length + inc;

        if (self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode != undefined) {

            parent = $(selector).parent();


            sel = $(parent).find('.sub-value');

            if (self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode != true) {

               

                inputs = $(sel).find('input');
               


                if (len > 1) {
                    if ($(inputs[0]).length > 0) {
                        $('.sub-value').find('input').hide();
                        $('.sub-value').each(function(e, j) {
                            $($(j).find('input')[self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_second]).show();
                        });
                        $(inputs[self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_second]).show();

                    }

                } else {
                    if ($(inputs[self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_first]).length > 0) {
                        $(sel).find('input').hide();
                        $(inputs[self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_first]).show();

                    }

                }
            }
        }

        self.slider_button_fix();
    },

    user_selections: function() {
        var e = this;
        $selectors = $("#optionList input");
        $selectors.unbind("click").not('.sub-value input').on("click", function(t) {

            if ($("#optionList #null:checked").length) {
                 
            } else {
                if ($(this).not("#null").length == 0) e.calculate_price();
            }

            if ($(this).parent("li").find(".sub-value:visible").length > 0) {
                $(this).parent("li").find(".sub-value").find('input').hide();
                $(this).parent("li").find(".sub-value").hide();
            } else {
                sub_index = $('.sub-value').find('input[value~="NORMAL"]').index();
                if (sub_index == -1) sub_index = 0;
                $(this).parent("li").find(".sub-value").find('input').hide();
                $($(this).parent("li").find(".sub-value").find('input')[sub_index]).show();
                $(this).parent("li").find(".sub-value").show();
              
            }

            data = $(this).data();

            $num = e.ing_availaility_check();
            if (e.vars.ACTIVE_MENU == 'BREAD' || e.vars.ACTIVE_MENU == "") {
                e.vars.ingredient_details["BREAD"].type = data.bread_type;
                e.vars.ingredient_details["BREAD"].shape = data.shape;

                if ($num > 0) {
                    e.vars.ingredient_details["BREAD"].item_image[0] = data.item_image_sliced;
                }
                e.update_json_data();

            }
            $('.landing-page').remove();
            $('.button-holder').show();

            $image = e.select_image_by_shape(data, $num);
            image_url = e.vars.image_path + $image;
            $name = data.itemname;
            $price = data.price;
            $sliced_image = data.item_image_sliced;
            $id = data.id;
            $priority = data.priority;
            className = e.Get_imageWrapperClassName();

            if (data.type == "replace") {
                itm_price = parseFloat(data.price);
                e.vars.current_sliced_image = $sliced_image;
                $($(className + " .image-holder")[0]).find("img").hide(0);
                $($(className + " .image-holder")[0]).find("img").attr("src", image_url);
                $($(className + " .image-holder")[0]).find("img").fadeIn(300);
                e.on_complete_actions(true, $name, $price, $image, $id)
            } else {
                if (t.target.checked == true) {
                    if (data.empty == true) {

                        e.on_complete_actions(false, "NULL", "0", '', '', '');
                        if ($("#optionList").find("input:checked").not("#null").length > 0) {


                            $("#optionList").find("input:checked").not("#null").each(function() {
                                data_fe = $(this).data();


                                $(className + '.image-holder[rel="' + data_fe.itemname + '"]').find('img').fadeOut(300, function() {
                                    $(className + '.image-holder[rel="' + data_fe.itemname + '"]').remove();
                                });


                            });
                            $("#optionList").find("input:checked").not("#null").prop("checked", false);
                            ACTIVE_TAB = e.vars.ACTIVE_MENU;
                            e.vars.ingredient_details[ACTIVE_TAB].item_name = ["NULL"];
                            e.vars.ingredient_details[ACTIVE_TAB].item_price = [0];
                            if (e.vars.ingredient_details[ACTIVE_TAB].item_priority) e.vars.ingredient_details[ACTIVE_TAB].item_priority = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_id = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_image = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_qty = {}
                            e.calculate_price();

                        }
                    } else {

                         
                        if ($("#optionList #null:checked").length > 0) {
                            $("#optionList #null").prop("checked", false);
                            ACTIVE_TAB = e.vars.ACTIVE_MENU;
                            $name_index = $.inArray("NULL", e.vars.ingredient_details[ACTIVE_TAB].item_name);
                            $price_index = $.inArray(0, e.vars.ingredient_details[ACTIVE_TAB].item_price);
                            e.vars.ingredient_details[ACTIVE_TAB].item_name.splice($name_index, 1);
                            e.vars.ingredient_details[ACTIVE_TAB].item_price.splice($price_index, 1);
                        }

                        e.dynamic_option_selection($(this), true);

                        im = [e.vars.current_sliced_image];
                        if (im != null && im != '' && e.vars.ACTIVE_MENU != "BREAD") {
                            if (e.vars.ingredient_details["BREAD"].item_image[0] != im) {
                                e.vars.ingredient_details["BREAD"].item_image = im;
                                e.turn_bread_image();
                            }
                        }

                        $select = $(this).parent().find(".sub-value");
                        if ($select.length > 0) {
                            $dat = $select.find("input:visible").data();
                            if ($dat) {
                                $arr = [];
                                $arr[0] = $select.find("input:visible").val();
                                $arr[1] = $dat.unit;
                                $arr[2] = $dat.price_mul;
                                $arr[3] = $dat.image;
                                e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$name] = $arr;
                            }
                        }

                        $pv = $priority;
                        e.set_images_in_position($priority, image_url, $(this));
                        e.on_complete_actions(false, $name, $price, $image, $id, $pv);
                        e.auto_select_option_fix();
                    }
                } else {
                    e.remove_items($id);
                    e.dynamic_option_selection($(this), false);
                    e.auto_select_option_fix();
                }
            }
        })
    },


    turn_bread_image: function() {
        var self = this;
        var $img = self.vars.image_path + self.vars.current_sliced_image;
        current = $(".bread_images .bread_image div:first").find('img').attr('src');
        if (self.vars.current_sliced_image && current.indexOf($img) == -1) {
            $(".bread_images .bread_image div:first").find('img').fadeOut(300, function() {
                if ($img) $(".bread_images .bread_image div:first").find('img').attr('src', $img);
                $(".bread_images .bread_image div:first").find('img').fadeIn(300);
            });
        }
    },


    slider_button_fix: function() {
        var self = this;

        $('.sub-value').each(function(e) {
            count = $(this).find('input').length;
            count = count - 1;
            index = $(this).find('input:visible').index();
            if (index == 0) {
                $(this).find('.left').removeClass('left-hover');
                $(this).find('.right').addClass('right-hover');
            } else if ((index > 0) && (index < count)) {
                $(this).find('.left').addClass('left-hover');
                $(this).find('.right').addClass('right-hover');
            } else if (index == count) {
                $(this).find('.right').removeClass('right-hover');
                $(this).find('.left').addClass('left-hover');
            }

        });

    },

    auto_select_option_fix: function() {
        var e = this;

        $('.sub-value').find('input:visible').each(function() {
            $ptext = $(this).parent().parent().parent().find('input[type="checkbox"]').data();
            $arr = [];
            $value = $(this).val();
            $data = $(this).data();
            $arr[0] = $value;
            $arr[1] = $data.unit;
            $arr[2] = $data.price_mul;
            $arr[3] = $data.image;

            e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$ptext.itemname] = $arr;
            $itn_id = $ptext.id;
            new_price = parseFloat($ptext.price) * parseFloat($data.price_mul);
            $p_index = $.inArray($itn_id, e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_id);
            if ($p_index != -1) {
                if (new_price > 0) e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_price[$p_index] = new_price;
            }


        });

        e.calculate_price();

    },

    set_images_in_position: function(thisPos, image, currentObj) {
        var self = this;
        var $pos = -1;
        var prev, next;
        var priorityArry = [];

        $('#optionList li input:checked').each(function() {
            $data = $(this).data();
            if ($data) priorityArry.push($data.priority);
        });

        priorityArry = priorityArry.sort(function(a, b) {
            return a - b;
        });


        if (priorityArry.length > 0) length = priorityArry.length - 1;
        else length = 0;
        mypos = $.inArray(thisPos, priorityArry);

        if (length == 0) {
            $meth = 'append';
            $priority = priorityArry[mypos];

        } else {

            if (mypos > 0) {
                $meth = 'after';
                $priority = priorityArry[mypos - 1];

            } else {
                $meth = 'before';
                $priority = priorityArry[mypos + 1];
            }


        }




        itemDat = $('#optionList li input[data-priority="' + $priority + '"]').data();
        currentDat = $('#optionList li input[data-priority="' + thisPos + '"]').data();

        current_data = $(currentObj).data();

        currentName = currentDat.itemname;
        $Itemname = current_data.itemname;
        prev_name = itemDat.itemname;

         
        className = self.Get_imageWrapperClassName();

        switch ($meth) {
            case 'append':
                $(".bread_images " + className).append('<div  rel="' + $Itemname + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
                break;
            case 'after':
                $(".bread_images " + className + " .image-holder[rel='" + prev_name + "']").after('<div  rel="' + currentName + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
                break;
            case 'before':
                $(".bread_images " + className + " .image-holder[rel='" + prev_name + "']").before('<div  rel="' + currentName + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
        }
        $(".bread_images " + className + " .image-holder[rel='" + currentDat.itemname + "']").find('img').fadeIn(300);

    },

    Get_imageWrapperClassName: function() {
        var self = this;

        if (self.vars.ACTIVE_MENU == "BREAD") className = '.bread_image';
        else if (self.vars.ACTIVE_MENU == "PROTEIN") className = '.protein_image';
        else if (self.vars.ACTIVE_MENU == "CHEESE") className = '.cheese_image';
        else if (self.vars.ACTIVE_MENU == "TOPPINGS") className = '.topping_image';
        else if (self.vars.ACTIVE_MENU == "CONDIMENTS") className = '.condiments_image';
        else className = '.bread_image';
        return className;
    },

    update_json_data: function() {

        var self = this;
        var $data;
        var tmp = [];
        $data = self.vars.ingredient_details;
        Object.keys($data).forEach(function(e) {

            if (e != "BREAD") {

                if ($data[e].item_id.length > 0) {
                    $data[e].item_id.forEach(function(item_ids) {
                        tmp.push(item_ids);
                    });
                }
            }

        });

        $ids = tmp.join(",");

        $.ajax({
            type: "POST",
            data: {
                'ids': $ids
            },
            dataType: 'json',
            url: SITE_URL + 'createsandwich/get_category_items_data/',
            success: function(e) {

                if (e.length > 0) {
                    e.forEach(function(iData) {

                        Object.keys(self.vars.ingredient_details).forEach(function(key) {


                            if (self.vars.ingredient_details[key].item_id.length > 0) {
                                self.vars.ingredient_details[key].item_id.forEach(function(iKey, iIndex) {

                                    if (iData.id == iKey) {

                                        switch (self.vars.ingredient_details["BREAD"].shape) {
                                            case 0:
                                                $image = iData.image_long;
                                                break;

                                            case 1:
                                                $image = iData.image_round;
                                                break;

                                            case 2:
                                                $image = iData.image_trapezoid;
                                                break;

                                        }

                                        self.vars.ingredient_details[key].item_image[iIndex] = $image;
                                    }

                                });
                            }

                        });
                    });
                }

                self.load_all_data(self.vars.ingredient_details);
            }
        });



    },

    select_image_by_shape: function(data, num) {
        self = this;
        $img = '';
        if (self.vars.ACTIVE_MENU == "BREAD" || self.vars.ACTIVE_MENU == "") {

            if (num > 0) return data.item_image_sliced;
            else return data.image;

        }


        switch (self.vars.ingredient_details["BREAD"].shape) {

            case 0:
                $img = data.image_long;
                break;
            case 1:
                $img = data.image_round;
                break;
            case 2:
                $img = data.image_trapezoid;
                break;

        }

        return $img;
    },


    remove_items: function($id) {
        var r = this;
        ACTIVE_TAB = r.vars.ACTIVE_MENU;
        var $item_name;
        if ($id) {
            $id_index = $.inArray($id, r.vars.ingredient_details[ACTIVE_TAB].item_id);
            if ($id_index != -1) {
                $item_name = r.vars.ingredient_details[ACTIVE_TAB].item_name[$id_index];
                r.vars.ingredient_details[ACTIVE_TAB].item_name.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_price.splice($id_index, 1);
                if (r.vars.ingredient_details[ACTIVE_TAB].item_priority) r.vars.ingredient_details[ACTIVE_TAB].item_priority.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_id.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_image.splice($id_index, 1);
                delete r.vars.ingredient_details[ACTIVE_TAB].item_qty[$item_name];

                className = r.Get_imageWrapperClassName();

                $(className + ' .image-holder[rel="' + $item_name + '"]').find('img').fadeOut(500, function() {
                    $(className + ' .image-holder[rel="' + $item_name + '"]').remove();
                });

                r.calculate_price();
            }
        } else if (r.vars.ingredient_details[ACTIVE_TAB].item_name.length > 0) {
            if (r.vars.ingredient_details[ACTIVE_TAB].item_name[0] == "NULL") {
                r.vars.ingredient_details[ACTIVE_TAB].item_name.splice(0, 1);
            }
        }
    },

    on_complete_actions: function(e, t, n, w, z, s) {
        var i = this;
        $sel = $(".create-sandwich-menu ul li").find("a:contains(" + i.vars.ACTIVE_MENU + ")").parent().next().find("a");
        $txt = $($sel).text();


        if (e == true) {
            if (z) {
                i.vars.ingredient_details.BREAD.item_name[0] = t;
                i.vars.ingredient_details.BREAD.item_price[0] = n;
                i.vars.ingredient_details.BREAD.item_image[0] = w;
                i.vars.ingredient_details.BREAD.item_id[0] = z;
            }
        } else {
            if (z) {


                if (i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name.length == 0 && i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price.length == 1) {
                    i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price = [];
                }

                if ($.inArray(z, i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_id) == -1) {
                    id_index = i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_id.push(z)
                    id_index = id_index - 1;
                    if (t) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name[id_index] = t;
                    if (n) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price[id_index] = n;
                    if (s) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_priority[id_index] = s;
                    if (w) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_image[id_index] = w;
                     
                }



            } else if (t == "NULL") {
                if ($.inArray(t, i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name) == -1) {
                    i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name.push(t)
                }
            }
        }
        i.calculate_price();
    },

    validate: function(e) {
        var t = this;
        var n = [];
        var r = false;
        if (e) {
            $active = e
        } else {
            $active = t.vars.ACTIVE_MENU
        }
        t.vars.validators.forEach(function(e) {
            selections = t.vars.ingredient_details[e].item_name.length;


            switch (e) {
                case "BREAD":
                    if (selections == 0) {
                        n.push("Select a bread to continue");
                    }
                    break;
                case "PROTEIN":

                    if (selections == 0) {
                    }

                    break;
                case "CHEESE":

                    if (selections == 0) {

                    } else {
                    }
                    break
            }
        });
        switch (n.length) {
            case 3:
                if ($active != t.vars.validators[0]) {
                    alert(n[0]);
                    r = true
                }
                break;
            case 2:
                if ($active != t.vars.validators[0] && $active != t.vars.validators[1]) {
                    alert(n[0]);
                    r = true;
                }
                break;
            case 1:
                if (t.vars.ingredient_details["BREAD"].item_name.length == 0) {
                    alert("Select a bread to continue");
                    r = true;
                } else {
                }
                break;
            default:
                r = false;
                break;
        }
        return r;
    },

    enable_scrolbar: function() {
        if ($(".scroll-bar").length > 0) {
            $(".scroll-bar").mCustomScrollbar({
                theme: "minimal",
                scrollInertia: 50
            });
        }
    }
};

$(function() {
    ha.sandwichcreator.init();
    loc = window.location.href;
    arr = loc.split('/');


    if ($.inArray('menu', arr) != -1 && !SESSION_ID) {
        $("#login").show(0, function() {
            $('.close-button').unbind('click').on('click', function() {
                window.open(SITE_URL, '_self');
            });
        });
    }

    if ($.inArray('createsandwich', arr) != -1 && !SESSION_ID) {
        $("#login").show(0, function() {
            $('.close-button').unbind('click').on('click', function() {
                window.open(SITE_URL, '_self');
            });
        });
    }


    $('.sandwich_like').on('change', function(e) {

        var sandwich_id = $(this).attr('id');
        var count = $(this).val();

        $.ajax({
            type: "POST",
            data: {
                'id': sandwich_id,
                "count": count
            },
            url: SITE_URL + 'sandwich/addLike/',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) {

                if (e) {
                    alert("Like count updated");

                }
            }
        });

    });
    $('.sandwich_purchase').on('change', function(e) {

        var sandwich_id = $(this).attr('id');
        var count = $(this).val();

        $.ajax({
            type: "POST",
            data: {
                'id': sandwich_id,
                "count": count
            },
            url: SITE_URL + 'sandwich/addPuchase/',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) {

                if (e) {

                    alert("Purchase count updated");
                }
            }
        });

    });
});
