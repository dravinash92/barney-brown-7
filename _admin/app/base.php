<?php

class Base extends INSPIRE
{
    
    var $controller;
    var $action;
    var $postdata;
    var $title;
    var $navigation;
    var $db;
    var $languages;
    var $vehicle_class;
    var $body_types;
    var $devices;
    var $apps;
    
    
    function init()
    {
        date_default_timezone_set('US/Eastern');
        $this->SessionStart();
        $this->ProcessUrl();
        $this->db = $this->db();
        $this->create_template_dir();
        $userType = "";
        
        
        if(!isset($_SESSION['admin_uid']) || !$_SESSION['admin_uid'] ){
        	if($this->UrlArray[0] != 'login'  ) header('Location:' . SITE_URL . 'login/');
        }
        
        if ($this->UrlArray[0] != '') {
            $this->controller = $this->UrlArray[0];
        } else {
            
            if (isset($_SESSION['admin_uid']) && $_SESSION['admin_uid'] > 0 && $_SESSION['user_type'] == 'admin') {
                header('Location:' . SITE_URL . 'neworder/');
                $userType = "admin";
                
            }
            if (isset($_SESSION['admin_uid']) && $_SESSION['admin_uid'] > 0 && $_SESSION['user_type'] == 'store') {
                header('Location:' . SITE_URL . 'storedeliveries/');
                $userType = "store";
            } else
                $this->controller = 'login';
        } 
        
        if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'store') {
            
            $storeURLS = array(
                "storedeliveries",
                "storepickups",
                "storereports",
                "storemanage"
            );
            $userType  = "store";
            
            if (!in_array($this->controller, $storeURLS)) {
                header('Location:' . SITE_URL . 'storedeliveries/');
            }
            
        } else if (isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'admin') {
            $userType = "admin";
        }
        
        
        if ($this->UrlArray[1] != '') {
            $this->action = $this->UrlArray[1];
        } else {
            
            $this->action = 'index';
        }
       
        
        $params = $this->process();
        $this->smarty->assign('API_URL', API_URL);
        $this->smarty->assign('User_Type', $userType);
        $this->smarty->assign('Title', $params->title);
        $this->smarty->assign('title_text', TITLE);
        $this->smarty->assign('isIpad', $this->isIpad());
        $this->smarty->assign('BASE_FARE',BASE_FARE);
        $this->smarty->display('index.tpl');
        
        
    }
    
    function isIpad() {
    	$userAgent = $_SERVER["HTTP_USER_AGENT"];
    	return preg_match("/(iPhone|iPad)/i",$userAgent);
    }
    
    function create_template_dir()
    {
        if (!is_dir('app/theme/templates_c'))
            mkdir('app/theme/templates_c', 0777);
    }
    

    
    function process()
    {
        if(!class_exists($this->controller) ){
        if(isset($_SESSION['admin_uid']) && $_SESSION['admin_uid'] )	header('Location: '.SITE_URL.'error/_404/');
    		exit();
    	} else $this->LoadSite();
    	
        $obj           = new $this->controller;
        $obj->postdata = $this->postdata;
        $obj->smarty   = $this->smarty;
        $obj->db       = $this->db;
        $obj->UrlArray = $this->UrlArray;
        
        if(!method_exists($obj,$this->action)){
        if(isset($_SESSION['admin_uid']) && $_SESSION['admin_uid'] ) header('Location: '.SITE_URL.'error/_404/');
        	exit();
        }
        
        $obj->{$this->action}();
        return $obj;
    }
    
    
    function br2nl($string)
    {
        return str_replace(array(
            "\r\n",
            "\r",
            "\n"
        ), "\\n", $string);
    }
    
    public function load_model($modelname)
    {
        
        spl_autoload_register(function($class)
        {
            require_once DIR_MODEL . $class . '.php';
        });
        
        return new $modelname;
    }
    
}
