<?php

//Directories
define('DIR_BASE', 'app/base/');
define('DIR_CONF', 'app/conf/');
define('DIR_CONTROLLER', 'app/controller/');
define('DIR_DB', 'app/db/');
define('DIR_MODEL', 'app/model/');
define('DIR_SMARTY', 'app/smarty/');
define('DIR_THEME', 'app/theme/');
define('PROJECT_BASE', '_admin/');
define('SITE_URL', 'http://admin.ha/');
define('CMS_URL','http://cms.ha/');
define('API_URL','http://api.ha/');
define('ADMIN_URL', 'http://admin.ha/');
define('API_USERNAME','hashburyuser');
define('API_PASSWORD','hashburypass');
define('SANDWICH_UPLOAD_DIR','../sandwich_uploads/');
define('SANDWICH_IMAGE_PATH','http://admin.ha/sandwich_uploads/');
define('TITLE','BARNEY BROWN');
define("ENVIRONMENT", "STAGING");

define("TAX",  0.08875);define("BASE_FARE",7);

