<?php

class Sandwich extends Base
{
	public $model;
	
	public function __construct(){
		$this->model = $this->load_model('sandwichModel');
	}	
	
	function index(){
		$model                = $this->model;
		$limit = 100;
		$page = empty($_GET['page']) ? 1 : (int)$_GET['page'];
		$start = $page == 1 ? 0 : (($page - 1) * $limit - 1);
		if(isset($_GET['sortid']))
			$sort_id=$_GET['sortid'];
			else
				$sort_id=0;

		$data['sort_id'] =  $sort_id;
		$data['start'] =  $start;
		$data['limit'] =  $limit; 
		$totalCount = $model->get_gallery_data_count($data);
		$this->smarty->assign('totalCount',$totalCount);
		$lastPage = (int)($totalCount/$limit);
		$remain = $totalCount%$limit;
		if($remain) $lastPage = $lastPage+1;
		$this->smarty->assign('lastPage',$lastPage);
		$serch_term           = @$_GET['search'];
		
		if($serch_term){ 
			$param = array( 'name'=>$serch_term, 
							'limit'=>$limit,
							'start'=>$start,
							'sort_id'=>$sort_id); 
		}
		else
		{
			$param = array( 'limit'=>10,'start'=>0,'sort_id'=>$sort_id );
		}
		$response_sandwich    = $model->get_all_sandwich_data($param);
		/*print '<pre>';
		print_r($response_sandwich);
		die('asdas');*/
		$sandwich_data        = $response_sandwich->Data;


		$_SESSION['sandwich_id_list'] = array(  );
		$_SESSION['sandwich_list_mode'] = 'simple';
		$_SESSION['sandwich_id_list_bookmark'] = 100;
		if(count($sandwich_data)>0){
			foreach( $sandwich_data as $sandwich )
			{
				array_push( $_SESSION['sandwich_id_list'], $sandwich->id);
			}
		}
		//print_r( $_SESSION['sandwich_id_list'] );die;
		$side_menu_bread      = $model->process_user_data($model->load_data(1)->Data);
		$side_menu_protein    = $model->process_user_data($model->load_data(2)->Data);
		$side_menu_cheese     = $model->process_user_data($model->load_data(3)->Data);
		$side_menu_topping    = $model->process_user_data($model->load_data(4)->Data);
		$side_menu_condiments = $model->process_user_data($model->load_data(5)->Data);
		if(count($sandwich_data)>0){
			$gallery_data = $model->_pre_process_user_data($sandwich_data);
		} else {
			$gallery_data = array();
		}
		$categories=$model->getSandwichCategories();
		$categoryItems=$model->getSandwichCategoryItems();
		$this->smarty->assign('categoryItems',$categoryItems);
		$this->smarty->assign('categories',$categories);
		$this->smarty->assign('total_item_count',$totalCount);
		$this->smarty->assign('GALLARY_DATA',$gallery_data);
		$this->smarty->assign('BREAD_DATA',$side_menu_bread);
		$this->smarty->assign('PROTIEN_DATA',$side_menu_protein);
		$this->smarty->assign('CHEESE_DATA',$side_menu_cheese);
		$this->smarty->assign('TOPPING_DATA',$side_menu_topping);
		$this->smarty->assign('CONDIMENTS_DATA',$side_menu_condiments);
		$this->smarty->assign('sort_id',$sort_id);
		$this->title = TITLE." | Admin - Sandwich Gallery";
		$this->smarty->assign('SITE_URL',SITE_URL);
		
		$this->smarty->assign('content',"sandwich_gallery.tpl");	
		
		$nav_ops  = array('show'  => true , 
						'url'  => array(SITE_URL.'sandwich',SITE_URL.'sandwich/sotd',SITE_URL.'sandwich/trnd'), 
					'text' => array('SANDWICH GALLERY','SANDWICH OF THE DAY','TRENDING SANDWICHES'),
								'active_text' => 'SANDWICH GALLERY');
		$this->smarty->assign('SEC_NAV_OPS',$nav_ops);
		$this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','sandwich');	
		
	}
	
	function filters(){
		$limit = 100;
		$start = 0;
		$_POST['start'] =  $start;
		$_POST['limit'] =  $limit;
		$filter = @$_POST['filter'];
		$filter = json_decode($filter);

		$filter->sort_id = 0;
		if( isset( $_GET['sortid'] ) ){
			$filter->sort_id = $_GET['sortid'];
		}

		if( isset( $_POST['sortBy'] ) ){
			$filter->sort_id = $_POST['sortBy'];
		}


		$sandwich_data = $this->model->filters( $filter );
		$sortBy        = @$_POST['sortBy'];
		$self = $this;
		$this->no_items = array();
		$final = array();



		foreach($sandwich_data as $data){
			if(count($data->sandwich_data['PROTEIN']) == 0) $data->sandwich_data['PROTEIN'][0] = 'No protein';
			if(count($data->sandwich_data['CHEESE']) == 0) $data->sandwich_data['CHEESE'][0] = 'No cheese';
			if(count($data->sandwich_data['TOPPINGS']) == 0) $data->sandwich_data['TOPPINGS'][0] = 'No toppings';
			if(count($data->sandwich_data['CONDIMENTS']) == 0) $data->sandwich_data['CONDIMENTS'][0] = 'No condiments';
			$is_bread_in = in_array( $data->sandwich_data['BREAD'][0] ,  $filter->BREAD  );
			$bread_diff   = array_diff($filter->BREAD,$data->sandwich_data['BREAD']);
			$prot_diff   = array_diff($filter->PROTEIN,$data->sandwich_data['PROTEIN']);
			$chs_diff    = array_diff($filter->CHEESE,$data->sandwich_data['CHEESE']);
			$top_diff    = array_diff($filter->TOPPINGS,$data->sandwich_data['TOPPINGS']);
			$con_diff    = array_diff($filter->CONDIMENTS,$data->sandwich_data['CONDIMENTS']);
			if((count($bread_diff) == 0 && count($prot_diff) == 0 &&  count($chs_diff) == 0 &&  count($top_diff) == 0 && count($con_diff) == 0 )  )
			{
				if(isset($data->id))$final[] = $data->id;
			}
		}

		if( isset(  $datas['start'])  )
			$start = $datas['start'];

		$_SESSION['sandwich_id_list'] = $final;
		$_SESSION['sandwich_list_mode'] = 'filter';
		$_SESSION['sandwich_id_list_bookmark'] = 100;

		$new_final = array_slice( $final, 0, 100 );

		//print_r( $_SESSION['sandwich_id_list'] );die;
		$data = $this->model->filter_seacrh_ajax_admin($new_final, $sortBy, 100, $start );

		if (isset($data->Data) && count(@$data->Data) > 0) {
			$gallery_datas = $this->model->_pre_process_user_data($data->Data);
		} else {
			$gallery_datas = array();
		}
		$this->smarty->assign('GALLARY_DATA',$gallery_datas);
		$this->smarty->assign('sandwich_count', count($gallery_datas));
		$this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
		echo $this->smarty->fetch("sandwich-gallery-ajax-search.tpl");
		exit;
	
	}
	
	function get_more_gallery(){

		// No filters varient
		if( $_SESSION['sandwich_list_mode'] == 'simple' )
		{
			$final_sandwich_id_list = array( );
			$cursor = $_SESSION['sandwich_id_list_bookmark'];
			$search_term           = @$_GET['search'];
			if($search_term)
			{ 
				$param = array( 'name'=>$search_term, 
							'limit'=>100,
							'start'=>$cursor,
							'sort_id'=>$sort_id); 
			}
			else
			{
					$param = array( 'limit'=>100,'start'=>$cursor,'sort_id'=>$sort_id );
			}
			$model                = $this->model;
			$response_sandwich    = $model->get_all_sandwich_data($param);			
			$sandwich_data        = $response_sandwich->Data;
			
			// Clear and refill with new stack of sammies
			$_SESSION['sandwich_id_list'] = array(  );
			foreach( $sandwich_data as $sandwich )
			{
				array_push( $_SESSION['sandwich_id_list'], $sandwich->id);
			}
			$final_sandwich_id_list = $_SESSION['sandwich_id_list'];
			$_SESSION['sandwich_id_list_bookmark'] = $cursor + $_SESSION['sandwich_id_list_bookmark'];
		}
		else
		{
			// Filter mode
			$sortBy        = @$_POST['sortBy'];
			$limit            =  100;
			$start = 0;
			if( isset(  $_POST['count'])  )
				$start = $_POST['count'];
			
			$final_sandwich_id_list = array_slice(  $_SESSION['sandwich_id_list'], $_SESSION['sandwich_id_list_bookmark'] , 100 );
			$_SESSION['sandwich_id_list_bookmark'] = $_SESSION['sandwich_id_list_bookmark'] + $limit;
		}
		
		$data = $this->model->filter_seacrh_ajax_admin( $final_sandwich_id_list, $sortBy, $limit, 0 );

		if ( isset( $data->Data ) && count( @$data->Data ) > 0 ) {
			$gallery_datas = $this->model->_pre_process_user_data( $data->Data );
		} else {
			$gallery_datas = array( );
		}
		$this->smarty->assign('GALLARY_DATA',$gallery_datas);
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('ADMIN_URL',ADMIN_URL);
		$this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
		echo $this->smarty->fetch("sandwich-gallery-more.tpl");
		exit;
	}
	
	function sotd(){
		$model = $this->model;		
		
		$this->title = TITLE." | Admin - Sandwich Gallery";
		$this->smarty->assign('SITE_URL',SITE_URL);
		
		$sandwichOfTheDay = $model->getSandwichOfTheDay();
		$this->smarty->assign('sandwiches',$sandwichOfTheDay);
		
		$this->smarty->assign('content',"sandwich_of_the_day.tpl");	
		
		$nav_ops  = array('show'  => true , 
						'url'  => array(SITE_URL.'sandwich',SITE_URL.'sandwich/sotd',SITE_URL.'sandwich/trnd'), 
					'text' => array('SANDWICH GALLERY','SANDWICH OF THE DAY','TRENDING SANDWICHES'),
								'active_text' => 'SANDWICH OF THE DAY');
		$this->smarty->assign('SEC_NAV_OPS',$nav_ops);
		$this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','sandwich');	
	
	}
	
	function trnd(){
		$model = $this->model;		
		
		$this->title = TITLE." | Admin - Sandwich Gallery";
		$this->smarty->assign('SITE_URL',SITE_URL);
		
		$getSandwichOfTrending = $model->getSandwichOfTrending();
		$this->smarty->assign('sandwiches',$getSandwichOfTrending);
		
		$this->smarty->assign('content',"sandwich_trending.tpl");	
		
		$nav_ops  = array('show'  => true , 
						'url'  => array(SITE_URL.'sandwich',SITE_URL.'sandwich/sotd',SITE_URL.'sandwich/trnd'), 
					'text' => array('SANDWICH GALLERY','SANDWICH OF THE DAY','TRENDING SANDWICHES'),
								'active_text' => 'TRENDING SANDWICHES');
		$this->smarty->assign('SEC_NAV_OPS',$nav_ops);
		$this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','sandwich');	
	
	}
	
	function set_public(){
		$model                = $this->model;
		$model->set_public();
		exit;
	}
	
	function delete_gallery(){
		$model                = $this->model;
		$model->delete_gallery();
		exit;
	}
	

	
	
     function edit(){

     $this->title = TITLE." | Admin-Edit Sandwhich";
	 $this->smarty->assign('SITE_URL',SITE_URL);
	 $editid = $this->UrlArray[2];
	 $model                = $this->model;
	 if($editid){ $param = array('id'=>$editid); }  else { $param = array();  }
	 $response_sandwich    = $model->get_all_sandwich_data($param);
	  

	 
	 $edit_data = $model->_pre_process_user_data($response_sandwich->Data); 
	 
	 //getting ingredients
	 
	 $side_menu_bread      = $model->input_compatible_data($model->load_data(1)->Data);
	 $side_menu_protein    = $model->input_compatible_data($model->load_data(2)->Data);
	 $side_menu_cheese     = $model->input_compatible_data($model->load_data(3)->Data);
	 $side_menu_topping    = $model->input_compatible_data($model->load_data(4)->Data);
	 $side_menu_condiments = $model->input_compatible_data($model->load_data(5)->Data);
	
	
	 $this->smarty->assign('EDIT_DATA',$edit_data);
	 
	 $this->smarty->assign('BREAD_DATA',$side_menu_bread);
	 $this->smarty->assign('PROTEIN_DATA',$side_menu_protein);
	 $this->smarty->assign('CHEESE_DATA',$side_menu_cheese);
	 $this->smarty->assign('TOPPING_DATA',$side_menu_topping);
	 $this->smarty->assign('CONDIMENTS_DATA',$side_menu_condiments);
	 
	 
	
	 $nav_ops  = array('show'  => false);	
     $this->smarty->assign('SEC_NAV_OPS',$nav_ops);
     $this->smarty->assign('ACTIVE_MAIN_MENU_TEXT','sandwich');
     $this->smarty->assign('content',"edit_sandwich.tpl");
     }
     
     function set_flag(){
     	$model                = $this->model;
        echo $model->toggle_flag();
     	exit;
     }
     
     function update(){     	
     	$model                = $this->model;
     	echo $model->input_user_sanwich_data($_POST);
     	exit;
     } 
     function addLike(){
     	$model  = $this->model;
     	$data   = array('id'=>$_POST['id'],'count'=>$_POST['count']);
     	echo $model->addLike($data);
     	exit;
     
     } 
     function addPuchase(){
     	$model  = $this->model;
     	$data   = array('id'=>$_POST['id'],'count'=>$_POST['count']);
     	echo $model->addPuchase($data);
     	exit;
     	 
     }
     
    public function addSandwichToSOTD(){
		$date=$_POST['sotd_date'];
		
		$date_replace=str_replace(',',' ',$date);		
		$_POST['sotd_date'] = date('Y-m-d',strtotime($date_replace));
		
		$model  = $this->model;
		$response = $model->addSandwichToSOTD($_POST);	
		exit;
	} 
	
    public function removeSandwichToSOTD(){
		$model  = $this->model;
		$response = $model->removeSandwichToSOTD($_POST);	
		exit;
	} 
	
   public function setPriority(){	
   
		$model  = $this->model;
		$response = $model->setSandwichPriority($_POST);	
		exit;
	} 
	
    public function addSandwichToTRND(){				
		$model  = $this->model;
		$response = $model->addSandwichToTRND($_POST);	
		exit;
	} 
	
    public function removeSandwichToTRND(){
		$model  = $this->model;
		$response = $model->removeSandwichToTRND($_POST);	
		exit;
	} 
	
	function setTrndLive(){
		$model = $this->model;
		$model->setTrndLive($_POST);
		exit;
	}
	
	function getAllproductsExtras(){
		$model        = $this->model;
		$model->getAllproductsExtras();
		exit;
	}
}