<?php 

class Model extends INSPIRE {


  public function __construct(){
  //setting up db object
  return $this->create_db();
  }

 //create db connection
  public function create_db(){
	require(DIR_CONF.'conf.my_db.php');
        return $db = new BaseClass($b_type,$Cfg_host,$Cfg_user,$Cfg_password,$Cfg_db);
  }
  
  
  public function receive_data( $url, $params=array(), $test = 0 ){
  
  	$params['api_username']  = API_USERNAME;
  	$params['api_password']  = API_PASSWORD;
	 $params['ENVIRONMENT']  = ENVIRONMENT; 

  	$params                  = $this->process_input($params);
  
  	//invoking curl
  	$ch = curl_init();
    
  	//set the url, number of POST vars, POST data
  	curl_setopt($ch,CURLOPT_URL,$url);
  	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  	curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'BB_DATA: app'
      ));
    
    if(  $test == 1 )
      curl_setopt($ch, CURLINFO_HEADER_OUT, true);


  	//execute post
  	$result = curl_exec($ch);
  
    if(  $test == 1 )
    {
        $headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT );
        echo "<pre>";
        echo "URL: url\n";
        echo "HEADER: $headerSent\n";
        echo "PARAMS: \n";
        var_dump( $params );
        echo "\n\nRESULT: \n\n";
        echo $result;
        echo "</pre>\n\n\n";
    } 
  	//close connection
  	curl_close($ch);
    return $result;
  }
  

  public function sendMail($params){
  
  	if(!isset($params['from'])) $params['from'] = 'noreply@cms.ha.allthingsmedia.com';
  
  	$to      = $params['to'];
  	$subject = $params['subject'];
  	$message = $params['message'];
  	$headers  = array(
  
  			"From: ".TITLE." <".$params['from'].">",
  			//"Reply-To: ". $params['from'],
  			"X-Mailer: PHP/" . phpversion(),
  
  			//"X-Originating-IP: ". $_SERVER['SERVER_ADDR'],
  			"MIME-Version: 1.0",
  			//"Content-Transfer-Encoding: 8bit",
  			// "Content-Type: text/html; charset=\"iso-8859-1\" ",
  			//"X-Priority: 1 (Higuest)",
  			//"X-MSMail-Priority: High",
  			//"Importance: High",
  			//"X-Sender: ".TITLE." < noreply@cms.ha.allthingsmedia.com >",
  			//"Date: " . date('r', $_SERVER['REQUEST_TIME']),
  			//"Message-ID: <" . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . "@" . $_SERVER['SERVER_NAME'] . ">"
  	);
  	// Send
  	if(	mail($to, $subject, $message, implode("\r\n", $headers) ) ) return true; else  return false;
  
  
  
  }
  
  
  private function process_input($data){
  	array_walk($data,function(&$val,$key){
  		$val = urlencode($val);
  	});
  	return $data;
  }
}