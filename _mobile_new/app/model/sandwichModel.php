<?php

class SandwichModel extends Model
{

	public $db; //database connection object
	/**
	* invoke database connection object
	*/
	public function __construct()
	{
		$this->db = parent::__construct();
	}


	public function get_gallery_data_count_api($post)
	{


		if (isset($post['sort_id']) && ($post['sort_id'] == 1)) {
			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1'  GROUP BY sandwich_name,sandwich_data ORDER BY date_of_creation DESC   ";
		} else if (isset($post['sort_id']) && ($post['sort_id'] == 2)) {
			$query = "SELECT user.*,slike.id as likeid,user.like_count as count FROM `user_sandwich_data` as user LEFT JOIN sandwich_like as slike  on (user.id=slike.sandwich_id) WHERE  user.is_public = '1'   GROUP BY user.sandwich_name,user.sandwich_data ORDER BY count DESC   ";
		} else if (isset($post['sort_id']) && ($post['sort_id'] == 3)) {
			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1' GROUP BY sandwich_name,sandwich_data  ORDER BY menu_add_count DESC   ";
		} else {
			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1'  GROUP BY sandwich_name,sandwich_data ORDER BY date_of_creation DESC   ";
		}



		$result = $this->db->Query($query);
		$num_rows = $this->db->Rows( );
		return $num_rows;
	}

	/**
	 * List gallery data count
	 */
	public function get_gallery_data_count($data = array())
	{	
		$get_url     = API_URL.'sandwich/get_gallery_data_count/';	
		$count          = $this->get_gallery_data_count_api( $data );		
		
	    return $count;
	}
	/**
	 * List gallery load data
	 */
	public function gallery_load_data($data){	
		  
		    $res          = $this->get_gallery_data_api( $data );
		    
		return $res;
	}

	/**
	 * List user gallery data
	 */
	public function get_gallery_data_api($post)
	{
		//print_r($post);exit;
		$post = $this->process_input($post);
		$start = $limit = 0;
		if (isset($post['start']) && ($post['start'] >= 0)) {
			$start = $post['start'];
		}

		if (isset($post['limit']) && ($post['limit'] >= 0)) {
			$limit = $post['limit'];
		}
		if (isset($post['name']) && ($post['name'] != '')) 
		{
			$name = $this->db->clean_for_sql($post['name']);
		}

		$query = 'SELECT * FROM `user_sandwich_data` WHERE is_public = "1"';
		if (isset($name) && ($name != '')) 
		{
			$name = preg_replace('/\x{2019}/u', "'", $name);
			$query .= 'AND sandwich_name  LIKE "%'.$name.'%" GROUP BY sandwich_name,sandwich_data ORDER BY date_of_creation DESC'; 
		}
		else
		{
			$query .= ' GROUP BY sandwich_name,sandwich_data ORDER BY date_of_creation DESC';
		}
		

		$query .= ' LIMIT '.$start.','. $limit;
		$result = $this->db->Query($query);

		return $this->db->FetchAllArray($result);
	}
	/**
	 * List user
	 */
	public function get_user($id)
	{
		$get_url = API_URL . 'user/get/';
		$json    = $this->receive_data($get_url, array(
				'user_id' => $id
		));
		$data    = json_decode($json);
		if (count($data->Data) > 0)
			return $name = $data->Data[0]->first_name . ' ' . $data->Data[0]->last_name;
		else
			return '';
	}
	
	
	public function get_user_limit_lname($id)
	{
		$get_url = API_URL . 'user/get/';
		$json    = $this->receive_data($get_url, array(
				'user_id' => $id
		));
		$data    = json_decode($json);
		if (count($data->Data) > 0){ 
			$lanme = $data->Data[0]->last_name;
		    $lanme = strtoupper($lanme[0]);
			return $name = $data->Data[0]->first_name . ' ' .$lanme; }
		else
			return '';
	}
	
	/**
	 * List product extra
	 */
	public function getAllproductsExtras()
	{

		$get_url = API_URL . 'categoryitems/getAllproductsExtras/';
		$json    = $this->receive_data($get_url, array());
		echo $json;
	}
	/**
	 * Remove menu
	 */
	public function remove_menu($data)
	{

		$get_url = API_URL . 'sandwich/remove_menu_item/';
		$json    = $this->receive_data($get_url, $data);

	}
	/**
	 * List sandwich like
	 */
	public function get_sandwich_like($user_id = null, $sandwich_id = null)
	{
		if ( $user_id && $sandwich_id ) {
			$query  = "SELECT id FROM `sandwich_like` WHERE sandwich_id=" . $sandwich_id . " AND u_id=" . $user_id . "";
			$result = $this->db->Query($query);
			return $this->db->FetchAllArray($result);
		}
	
	}
	/**
	 * List sandwich like count
	 */
	public function get_sandwich_like_count($id)
	{

		$query  = "SELECT id,sandwich_id,COUNT(*) AS count FROM `sandwich_like` WHERE sandwich_id= $id  GROUP BY `sandwich_id` ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Process user data
	 */
	function  process_user_data(&$data){
	
		$num_tot =  count($data); 
		
		$self  = $this;

		#	debug_print_backtrace();die;
		$ind = 0;
		$bread = $protein = $cheese = $topping = $condiment = $desc = $descDate = "";
		array_walk($data,function(&$value,$key) use($self){
			$value = get_object_vars((object)$value);
			
			$array = get_object_vars(json_decode($value['sandwich_data']));
			


			array_walk($array,function(&$val,$key){
				$val = get_object_vars($val);
			});
			
			
			$bread     = $array['BREAD']['item_name'][0];
			$protein   = $self->getCombinedString($array['PROTEIN']['item_qty']); 
			$cheese    = $self->getCombinedString($array['CHEESE']['item_qty']);  
			$topping   = $self->getCombinedString($array['TOPPINGS']['item_qty']); 
			$condiment = $self->getCombinedString($array['CONDIMENTS']['item_qty']);  

			// item ids 
			$bread_id     = $array['BREAD']['item_id'][0];
			$protein_id   = implode(', ',$array['PROTEIN']['item_id']); 
			$cheese_id    = implode(', ',$array['CHEESE']['item_id']);  
			$topping_id   = implode(', ',$array['TOPPINGS']['item_id']); 
			$condiment_id = implode(', ',$array['CONDIMENTS']['item_id']);
			   
		    $desc      = $bread.", ".$protein.", ".$cheese.", ".$topping.", ".$condiment; 
			$descData  = explode(', ',$desc);
			$descData  = array_filter($descData);
		    $desc      = implode(', ',$descData);

		    $desc_id = $bread_id.", ".$protein_id.", ".$cheese_id.", ".$topping_id.", ".$condiment_id;
		    $descDataId  = explode(', ',$desc_id);
			$descDataId  = array_filter($descDataId);
		    $desc_id      = implode(', ',$descDataId);

		    $brd = $bread;
			$total = $this->get_sandwich_current_price_menu($brd,$desc,$desc_id);
		 
			$value['sandwich_data'] = $array;
			$value['sandwich_desc'] = $desc;
			$value['sandwich_desc_id'] = $desc_id;
			$value['current_price'] = $total;
			$value['user_name']     = $self->get_user_limit_lname($value['uid']);

			$value['formated_date'] = date('m/d/y',strtotime($value['date_of_creation']));
			
		});
		
		return array('data'=>$data,'count'=>$num_tot);
	}

	public function get_sandwich_current_price_menu($bread,$desc,$desc_id)
	{
		$params      = array('bread'=>$bread,'desc'=>$desc,'desc_id'=>$desc_id);
		$get_url     = API_URL.'sandwich/get_sandwich_current_price_menu/';
		 $json    = $this->receive_data($get_url,$params);
		$finalData = json_decode($json,true);
        return $finalData['Data'];
	}
	/**
	 * List combined string
	 */
	public function getCombinedString($data)
	{
		$arr = array();
		
		if(is_object($data)){
			$data = get_object_vars($data);
		} else $data = array();
		
		foreach ($data as $key => $data) {
			$arr[] = $key . " (" . $data[1] . ")";
		}
		return implode(', ', $arr);
	}
	/**
	 * Place order
	 */
	function create_order($item_id, $user_id, $data_type, $qty, $toast, $spcl_instructions, $extra_id)
	{
		error_reporting(0);
		if ($data_type == 'user_sandwich') {
			$price_data = @json_decode($this->receive_data(API_URL . 'sandwich/get_sandwich_data/', array(
					'id' => $item_id
			)));
		} else {

			$price_data = @json_decode($this->receive_data(API_URL . 'sandwich/get_product_data/', array(
					'id' => $item_id
			)));
		}



		if (isset($price_data)) {

			if ($data_type == 'user_sandwich') {
				$price = $price_data->Data[0]->sandwich_price;
			} else {
				$price = $price_data->Data[0]->product_price;
			}


			$data      = array(
					'itemid' => $item_id,
					'uid' => $user_id,
					'price' => $price,
					'data_type' => $data_type,
					'qty' => $qty,
					'toast' => $toast,
					'spcl_instructions' => $spcl_instructions,
					'extra_id' => $extra_id
			);
			$url       = API_URL . 'sandwich/add_order_items/';
			$json      = $this->receive_data($url, $data);
			$finalData = json_decode($json);



			return @$finalData->Data;
		}
	}
	/**
	 * Place order
	 */
	function remove_order($id, $uid)
	{
		$url       = API_URL . 'sandwich/remove_cart_order/';
		$json      = $this->receive_data($url, array(
				'order_item_id' => $id,
				'uid' => $uid
		));
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * Resume order
	 */
	function resume_order()
	{

		$url        = API_URL . 'sandwich/get_checkout_data/';
		$json       = $this->receive_data($url, array(
				'uid' => @$_SESSION['uid']
		));
		$data       = json_decode($json);
		$final      = array();
		$item_count = 0;
		if (isset($data)) {
			foreach ($data->Data as $dat) {
				$array_data               = get_object_vars($dat);
				$final['item_id'][]       = $array_data['order_item_id'];
				$final['order_item_id'][] = $array_data['item_id'];
				$item_count += $array_data['item_qty'];

			}

			$final['item_qty'] = $item_count;
			return $final;

		} else {

			return null;

		}
	}
	/**
	 * List order data
	 */
	function get_order_data()
	{

		$url  = API_URL . 'sandwich/get_checkout_data/';
		$json = $this->receive_data($url, array(
				'uid' => @$_SESSION['uid']
		));
		$data = json_decode($json);
		if (is_object($data)) {

			$final_data = $this->process_data($data->Data);

			$result_ids = array();
			foreach ($final_data as $final_data) {

				$result_ids[$final_data['item_type']]['order_item_id'][]                 = $final_data['order_item_id'];
				$result_ids[$final_data['item_type']]['item_id'][]                       = $final_data['item_id'];
				$result_ids[$final_data['item_type']]['quatity'][$final_data['item_id']] = $final_data['item_qty'];

			}


			return $result_ids;

		}
		return null;

	}
	/**
	 * Add menu
	 */
	function add_menu_multi($post)
	{

		$url  = API_URL . 'sandwich/add_menu_multi/';
		$json = $this->receive_data($url, $post);
		$dt   = json_decode($json);


		$out = get_object_vars($dt->Data);


		$nwid     = $out['id'];
		$oldimage = $out['image'];
		$olduid   = $out['uid'];

		$folder    = SANDWICH_UPLOAD_DIR . $post["uid"] . '/';
		$oldfolder = SANDWICH_UPLOAD_DIR . $olduid . '/';
		if (!is_dir($folder)) {
			mkdir($folder, '0777', true);
		}
		$imgname = 'sandwich_' . $nwid . '_' . $post["uid"] . '.png';

		$cp1  = $oldfolder . $oldimage;
		$cpt2 = $folder . $imgname;
  
	    copy($cp1, $cpt2);

	}
	/**
	 * Process data
	 */
	function process_data($data)
	{
		if (is_array($data)) {
			array_walk($data, function(&$val, $key)
			{
				$val = get_object_vars($val);
			});
		}
		return $data;
	}
	/**
	 * List Sandwich Category Items
	 */
	function getSandwichCategoryItems()
	{

		$url  = API_URL . 'sandwich/getSandwichCategoryItems/';
		$json = $this->receive_data($url, array(
				'uid' => @$_SESSION['uid']
		));
		$data = json_decode($json);


		return $data;
	}
	/**
	 * List Sandwich Category
	 */
	function getSandwichCategories()
	{

		$url  = API_URL . 'sandwich/getSandwichCategories/';
		$json = $this->receive_data($url, array(
				'uid' => @$_SESSION['uid']
		));
		$data = json_decode($json);


		return $data;
	}
	/**
	 * Add likes
	 */
	function addLike($data)
	{
		$url  = API_URL . 'sandwich/addLike/';
		$json = $this->receive_data($url, array(
				'id' => $data['id'],
				'uid' => $data['uid']
		));
		
		return $json;
	}
	/**
	 * Toggle flag
	 */
	public function toggle_flag()
	{
		$params  = array(
				'id' => $_POST['id'],
				'state' => $_POST['status']
		);
		$get_url = API_URL . 'sandwich/toggle_flag/';
		return $json = $this->receive_data($get_url, $params);
	}
	/**
	 * Update Toast menu
	 */
	public function updateToastmenu($data)
	{
		$params  = array(
				'item_id' => $data['item_id'],
				'toast' => $data['toast']
		);
		$get_url = API_URL . 'sandwich/updateToastmenu/';
		$json    = $this->receive_data($get_url, $params);
		return $json;
	}
	/**
	 * Send flag mail
	 */
	public function send_flag_mail()
	{

		$uid      = @$_SESSION['uid'];
		$id       = $_POST['id'];
		$userName = $this->get_user($uid);

		$params   = array(
				'id' => $id
		);
		$get_url  = API_URL . 'sandwich/get_sandwich_name/';
		$sandwich = $this->receive_data($get_url, $params);
		$sandwich = json_decode($sandwich);
		$sandwich = $sandwich->Data->sandwich_name;
		$uri      = API_URL . 'sandwich/sendFlagMail/';
		$this->receive_data($uri, array(
				'user' => $userName,
				'sandwich' => $sandwich
		));

	}
	
	public function arraYprocessfilter($v){
	array_walk($v,function(&$v,$k){ 
		$v = $v->item_name;
	});
    return $v;
	}
	
	// THIS IS THE DEV MODLE
	public function filters($filter){
		$get_url  = API_URL . 'sandwich/getAllsandwichDetailsSearch/';
		
		$data = $this->getAllsandwichDetailsSearch(  );

		#OK, here got a list of sanwiches, with appropriate info
		$_self = $this;
    	foreach( $data as &$item )
    	{
    		if( $item['sandwich_data'] )
    		{
    			$vals = $item['sandwich_data'] ;
    			$vals = json_decode($vals);
    			$vals = get_object_vars($vals);
    			$item['sandwich_data'] =  $_self->arraYprocessfilter($vals);
    		}	
    	}
		return $data;
	}
	
	// once we have all valid sandwiches, get by id list.
	public function filter_seacrh_ajax($ids,$sort,$limit=2500){
		$ids = implode(",",$ids);

		$sandwich = $this->filter_search_ajax_api( array('id'=>$ids,'sortBy'=>$sort,'limit'=>$limit ) );
	 	return $sandwich;
	}

	public function filter_search_ajax_api( $post )
    {
		
		
		$start = $limit = 0;
		if (isset($post['limit']) && ($post['limit'] >= 0)) {
			$limit = $post['limit'];
		}
		$limit = 2500;

		
		if (isset($post['sortBy']) && ($post['sortBy'] == 1)) {
			$orderQuery = "ORDER BY date_of_creation DESC";
		} else if (isset($post['sortBy']) && ($post['sortBy'] == 2)) {
			$orderQuery = "ORDER BY count DESC";
		} else if (isset($post['sortBy']) && ($post['sortBy'] == 3)) {
			$orderQuery = "ORDER BY menu_add_count DESC";
		} else {
			$orderQuery = "ORDER BY date_of_creation DESC";
		}
		
		$query = "SELECT user.*,slike.id as likeid,user.like_count as count FROM `user_sandwich_data` as user LEFT JOIN sandwich_like as slike  on (user.id=slike.sandwich_id) WHERE  user.is_public = '1' AND user.id IN (".$post['id'].")  GROUP BY user.id ".$orderQuery;
		$query .= " LIMIT $start, $limit";    // THID WAS CAUSING IT TO FAIL
		 
		 $result = $this->db->Query($query);
		 return $this->db->FetchAllArray($result);
	}


	public  function getAllsandwichDetailsSearch(){

		$query = "SELECT id,sandwich_data FROM `user_sandwich_data` WHERE is_public = '1'";
		$noDuplicates = "GROUP BY sandwich_name  DESC LIMIT 2500  "; //for avoiding duplicate sandwiches 
		$query .= $noDuplicates;
		// TODO - Why not distinct?
		
		$this->db->Query($query);
		$result = $this->db->FetchAllArray();
		
		

		return $result;
		
	}
    
	
	public function get_individual_sandwich_data($id,$uid){
		$data        = array('id'=>$id,'uid'=>$uid);
		$get_url     = API_URL.'sandwich/get_individual_sandwich_data/';
		$json        = $this->receive_data($get_url,$data);
		return $json;
	}
	
	function  process_individual_sandwich_data($data){
	
		$num_tot =  count($data);
	
		$self  = $this;
		if(!isset($data->sandwich_data) ) {
			return  array("state"=>false);
		}
		else {
			$sandwich = $data->sandwich_data;
			$sandwich = json_decode($sandwich);
			$array = get_object_vars($sandwich);
			array_walk($array,function(&$val,$key){
				$val = get_object_vars($val);
			});
	
	
	
				$like = $self->get_sandwich_like($data->uid,$data->id);
				$like_counts  = $self->get_sandwich_like_count($data->id);
					
					
				if(isset($like->Data[0])) $like_id = $like->Data[0]->id; else $like_id = 0;
					
					
				$bread     = $array['BREAD']['item_name'][0];
				$protein   = $self->getCombinedString($array['PROTEIN']['item_qty']);
				$cheese    = $self->getCombinedString($array['CHEESE']['item_qty']);
				$topping   = $self->getCombinedString($array['TOPPINGS']['item_qty']);
				$condiment = $self->getCombinedString($array['CONDIMENTS']['item_qty']);
					
				$desc      = $bread.", ".$protein.", ".$cheese.", ".$topping.", ".$condiment;
				$descData  = explode(', ',$desc);
				$descData  = array_filter($descData);
				$desc      = implode(', ',$descData);

				// item ids 
				$bread_id     = $array['BREAD']['item_id'][0];
				$protein_id   = implode(', ',$array['PROTEIN']['item_id']); 
				$cheese_id    = implode(', ',$array['CHEESE']['item_id']);  
				$topping_id   = implode(', ',$array['TOPPINGS']['item_id']); 
				$condiment_id = implode(', ',$array['CONDIMENTS']['item_id']);

				$desc_id = $bread_id.", ".$protein_id.", ".$cheese_id.", ".$topping_id.", ".$condiment_id;
			    $descDataId  = explode(', ',$desc_id);
				$descDataId  = array_filter($descDataId);
			    $desc_id      = implode(', ',$descDataId);

			    $brd = $bread;
				$total = $this->get_sandwich_current_price_menu($brd,$desc,$desc_id);
					
				$data->like_id          = $like_id;
				$data->sandwich_data    = $array;
				$data->sandwich_desc    = $desc;
				$data->sandwich_desc_id    = $desc_id;
				$data->sandwich_price    = $total;
				$data->user_name        = $self->get_user($data->uid);
				$data->formated_date    = date('m/d/y',strtotime($data->date_of_creation));
				$data->imagepath        = SANDWICH_IMAGE_PATH.$data->uid.'/'.'sandwich_'.$data->id.'_'.$data->uid.'.png';
				return $data;
		}
	}

	//to check FB user
	    public function isFbOnlyUser($uid){
    	$url       = API_URL.'sandwich/isFbOnlyUser/';
    	$json      = $this->receive_data($url,array('uid'=>$uid));
    	$finalData = json_decode($json);
        return $finalData;
    }

    /* Featured Sandwiches */
    public function getSandwichOfTrending($data = array())
    {
        $url  = API_URL . 'sandwich/getSandwichOfTrending/';
        $json = $this->receive_data($url, $data);
        $json = json_decode($json);
        return $json->Data;
    }
	 /* Saved Sandwiches */
    public function get_user_sanwich_data($data)
    {
		 $get_url     = API_URL.'sandwich/get_menu_data/';
		$json        = $this->receive_data($get_url,$data);
		$json = json_decode($json);
        return $json->Data;
	}

	public function  remove_saved($data)
	{
		$data = $this->process_input($data);
		if( @$data['sType'] == "FS")
		{
			$query = "UPDATE user_sandwich_data SET menu_is_active = '0' WHERE menu_added_from = '" . @$data['id'] . "' AND uid = '" . @$data['uid'] . "' ";
		}
		else
		{
			$query = "UPDATE user_sandwich_data SET menu_is_active = '0' WHERE id = '" . @$data['id'] . "' AND uid = '" . @$data['uid'] . "' ";
		}
		$this->db->Query($query);
		return @$data['sType'];
		
	}
	public function  make_sandwich_private($data)
	{
		$data = $this->process_input($data);
		$query = '';
		if( @$data['sType'] == "FS")
		{
			$sw_query = "SELECT menu_added_from FROM user_sandwich_data WHERE id = '" . @$data['id'] . "' AND uid = '" . @$data['uid'] . "' ";
			$res = $this->db->Query($sw_query);
			$check = $this->db->Rows($res);
			if($check == 1)
			{
				$query = "UPDATE user_sandwich_data SET is_public = '".@$data['is_public']."' WHERE id = '" . @$data['id'] . "' AND uid = '" . @$data['uid'] . "' ";
			}
			else
			{
				$query = "UPDATE user_sandwich_data SET is_public = '".@$data['is_public']."' WHERE menu_added_from = '" . @$data['id'] . "' AND uid = '" . @$data['uid'] . "' ";
			}
			
		}
		else
		{
			$query = "UPDATE user_sandwich_data SET is_public = '".@$data['is_public']."' WHERE id = '" . @$data['id'] . "' AND uid = '" . @$data['uid'] . "' ";
		}
		return $this->db->Query($query);
		
	}
	/*saved sandwiches ids */
	public function get_user_sandwich_ids($uid){
		$data     = $this->get_my_sandwich_ids($uid);
		
        return $data;
	}

	/**
	 * List user sandwich ids
	 */
	public function get_my_sandwich_ids($uid)
	{
		
		
		$query  = "SELECT id,menu_added_from, is_public FROM user_sandwich_data WHERE  uid  = '" . $uid . "' AND menu_is_active = 1";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}

	public function get_sandwich_shared_creations_count(){
    	$query = "SELECT id FROM `user_sandwich_data` WHERE  is_public = '1' GROUP BY sandwich_name,sandwich_data";

		$result = $this->db->Query($query);

		$num_rows = $this->db->Rows( );
		return $num_rows;
    }

    function getFBfriendsMenu()
	{
		$get_url     = API_URL.'menu/getFBfriendsMenu/';
		if(isset($_SESSION['uid'])) $id = $_SESSION['uid']; else $id = 0;
		$json    = $this->receive_data($get_url,array('id'=>$id));
		
		return  json_decode($json);
	}

	function getfriendSandwichCount($uid){

		$get_url     = API_URL.'menu/getfriendSandwichCount/';
		 $json       = $this->receive_data($get_url,array('uid'=>$uid));  
	     $count      = json_decode($json);
	     if(isset($count->Data[0]->COUNT)) return $count->Data[0]->COUNT;
	     else return 0;
	     
	}

	public function _process($data)
	{
		
		$_this = $this; 
		array_walk($data,function(&$val,$key) use ($_this)
		{ 
			$val->sandwich_count = $_this->getfriendSandwichCount($val->uid);
		});
	 	return $data;
	}

	public function get_sandwich_current_price($pid,$bread,$desc,$desc_id)
	{
		$params      = array('id'=> $pid, 'bread'=>$bread,'desc'=>$desc,'desc_id'=>$desc_id);
		$get_url     = API_URL.'sandwich/get_sandwich_current_price/';
		 $json    = $this->receive_data($get_url,$params);
		$finalData = json_decode($json,true);
        return $finalData['Data'];
	}

	function get_searchItem_count($data)
	{
		$data = $this->process_input($data);

		$name = preg_replace('/\x{2019}/u', "'", $data["name"]);
		$query = 'SELECT id FROM user_sandwich_data WHERE  is_public = "1"  AND sandwich_name LIKE "%'.$name.'%" GROUP BY sandwich_name,sandwich_data';

		$countRes = $this->db->Query($query);
		$res = $this->db->Rows($countRes);
		return $res;
	}
	function sandwich_filter($data)
    {
        $url       = API_URL.'sandwich/sandwich_filter/';
        $json      = $this->receive_data($url,$data);
        $result      = json_decode($json);
        return  $result->Data;
    }
    function sandwich_filter_count($data)
    {
        $url       = API_URL.'sandwich/sandwich_filter_count/';
        $json      = $this->receive_data($url,$data);
        $result      = json_decode($json);
        return  $result->Data;
    }
    /**
     * Insert process input
     */
    private function process_input($data) {
        array_walk($data, function(&$val, $key) {
            $val = urldecode($val);
            $val = $this->clean_for_sql($val);
        });
        return $data;
    }
}