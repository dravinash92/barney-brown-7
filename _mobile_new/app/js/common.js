// mobile new which is mobile web
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};


console.dir = function(  ){}; 


(function() {
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    Date.prototype.getMonthName = function() {
        return months[ this.getMonth() ];
    };
    Date.prototype.getDayName = function() {
        return days[ this.getDay() ];
    };

    //session code
    $.session = {
        _id: null,
        _cookieCache: undefined,
        _init: function () {
            if (!window.name) {
                window.name = Math.random();
            }
            this._id = window.name;
            this._initCache();
 
            // See if we've changed protcols

            var matches = (new RegExp(this._generatePrefix() + "=([^;]+);")).exec(document.cookie);
            if (matches && document.location.protocol !== matches[1]) {
                this._clearSession();
                for (var key in this._cookieCache) {
                    try {
                        window.sessionStorage.setItem(key, this._cookieCache[key]);
                    } catch (e) {
                    }
                    ;
                }
            }

            document.cookie = this._generatePrefix() + "=" + document.location.protocol + ';path=/;expires=' + (new Date((new Date).getTime() + 120000)).toUTCString();

        },
        _generatePrefix: function () {
            return '__session:' + this._id + ':';
        },
        _initCache: function () {
            var cookies = document.cookie.split(';');
            this._cookieCache = {};
            for (var i in cookies) {
                var kv = cookies[i].split('=');
                if ((new RegExp(this._generatePrefix() + '.+')).test(kv[0]) && kv[1]) {
                    this._cookieCache[kv[0].split(':', 3)[2]] = kv[1];
                }
            }
        },
        _setFallback: function (key, value, onceOnly) {
            var cookie = this._generatePrefix() + key + "=" + value + "; path=/";
            if (onceOnly) {
                cookie += "; expires=" + (new Date(Date.now() + 120000)).toUTCString();
            }
            document.cookie = cookie;
            this._cookieCache[key] = value;
            return this;
        },
        _getFallback: function (key) {
            if (!this._cookieCache) {
                this._initCache();
            }
            return this._cookieCache[key];
        },
        _clearFallback: function () {
            for (var i in this._cookieCache) {
                document.cookie = this._generatePrefix() + i + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            }
            this._cookieCache = {};
        },
        _deleteFallback: function (key) {
            document.cookie = this._generatePrefix() + key + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            delete this._cookieCache[key];
        },
        get: function (key) {
            return window.sessionStorage.getItem(key) || this._getFallback(key);
        },
        set: function (key, value, onceOnly) {
            try {
                window.sessionStorage.setItem(key, value);
            } catch (e) {
            }
            this._setFallback(key, value, onceOnly || false);
            return this;
        },
        'delete': function (key) {
            return this.remove(key);
        },
        remove: function (key) {
            try {
                window.sessionStorage.removeItem(key);
            } catch (e) {
            }
            ;
            this._deleteFallback(key);
            return this;
        },
        _clearSession: function () {
            try {
                window.sessionStorage.clear();
            } catch (e) {
                for (var i in window.sessionStorage) {
                    window.sessionStorage.removeItem(i);
                }
            }
        },
        clear: function () {
            this._clearSession();
            this._clearFallback();
            return this;
        }

    };

    $.session._init();

    //end of session code
})();

$.session.remove('deliveryFee');
mobile_ha = {};
mobile_ha.common = window.mobile_ha.common || {
    
    product_extras: null,
    zipPopUpFlag: 0,
    zipcodes: [],
    fbStatus : null,
    activeAjaxConnections : null,
    time_state_array : [],
    validation_blocked: 0,
    checkoutSlectionData : { 'orderType' : null , 'addressId' : null , 'cardId' : null , 'storeId' : null , 'isNotTimeNow' : null, 'specificTime' : null },
    specific_date_exist: null,
    specific_date_selected: null,
    specific_time_selected: null,
    specific_time_selected_tm: null,
    newDate: null,
    newTime: null,
    closed_time: null, 
    opened_time: null,
    open_hour: null,
    close_hour: null,
    open_date_json: null,
    close_date_json:null,
    _current_day_hour:null,
    init : function(){ 
        var self = this;
        onPageLoad = true;
        self.menuAction();
        self.loginAction();
        self.createAccountAction();
        self.forgotPasswordAction(); 
        self.sandwichGalleryAction();
        self.menu_page_activity();
        self.addMenuAction();
        self.cookieLoginAction();
        self.facebookLoginAction();
        self.addToCartAction();
        self.removeMenuAction();
        self.qtySpinnerAction();
        self.add_standard_menu_item_cart();
        if(window.location.href.indexOf('createsandwich') == -1 && window.location.href.indexOf('sandwich/gallery') == -1)
        self.checkoutPageAction();
        self.sandwichToastAction();
        self.gallery_search();
        self.lazy_load_sandwiches();
        self.myaccount_reorder();
        self.enable_friend_sandwiches();
        self.show_friends_popup_onload();
        self.removeCartItems();
        self.showSandwichSummaryCheckout();
        if(window.location.href.indexOf('createsandwich') == -1 && window.location.href.indexOf('sandwich/gallery') == -1)
        self.get_all_product_extra();
        self.add_product_descriptor_to_cart();
        self.preload_images();
        if(window.location.href.indexOf('createsandwich') == -1 && window.location.href.indexOf('sandwich/gallery') == -1)
        self.get_all_zip_codes();
        self.pageBackEvent();
        self.loadProductDescriptorPopup();
        
        if(window.location.href.indexOf('createsandwich') == -1 && window.location.href.indexOf('sandwich/gallery') == -1)
        self.checkTimingsRealtedConditions(  );     // this basically gets our open and closed times...

        self.lazyLoadMenuSandwiches();
        self.show_sandwich_detail_onload();
        self.process_url(); //to check the url to remove the session and the cookie variables
        
        
        //on clicking back to menu button on friends menu pop-up
        cookieAddCartData = self.getCookie('addCartData');
        isFbOnlyUser      = mobile_ha.common.getCookie('facebookOnlyUser');
   
        if(isFbOnlyUser == "1"){ 
         if(cookieAddCartData != "" && cookieAddCartData != "false"){
            
             session = mobile_ha.common.check_session_state();
            if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                  mobile_ha.common.login_popup($(this));
                 } else {
                   mobile_ha.common.facebook_login($(this));
                 }
                return;
            }
            outJSon = JSON.parse(cookieAddCartData);
            if(outJSon.type == 'reorder'){
                $(window).load(function(){ 
                    $("a[data-order='"+outJSon.reorderId+"']").trigger("click");    
                });
            } 
            else if(outJSon.type == 'likesandwich'){
                $(window).load(function(){ 
                    $("img[data-sand-pop='"+outJSon.reorderId+"']").trigger("click");   
                    $("a[data-sand-like-id='"+outJSon.reorderId+"']").trigger("click"); 
                });
            }
            else {
                $(window).load(function(){ 
                    if(outJSon.type == 'product') ftype = 'product'; else ftype = 'user_sandwich';
                    if(!outJSon.uid) uid = SESSION_ID; else uid = outJSon.uid;
                    if(outJSon.reorderId){                          
                    mobile_ha.sandwichcreator.add_sandwich_to_cart(outJSon.reorderId,uid,ftype, outJSon.qty); 
                }
                });
            }  
            self.setCookie("addCartData",'false',1);
         } }

       
            
        $('.close-button').on('click touchstart',function(e){
            e.preventDefault();
            
            if(window.location.href.indexOf("privacypolicy") != -1 || window.location.href.indexOf("termsofuse") != -1){ 
                window.location.replace(SITE_URL);
                setTimeout(function(){ $('.sandwich-popup').hide(); }, 3000);
            } 
            else { 
                $('.sandwich-popup').hide();
            }
            $('.error_msg').hide();
            self.body_scroll_ops(true);
        });
        
        $('.cancel-quick-edit').click(function(){
                $("#mobile-sandwiches-popup").hide();
                $("#sandwiches_quick_edit").hide();
                $(".edit_sandwich-popup").hide();
                $("#salad_popup").hide();
            });
        
        
        $("#cal-holder").click(function(){ 
          $('.delivery_date').datepicker('show');
        });
        
        $('.sandwich .container .salads-wrapper ul li .left p').css('letter-spacing','-1px')
        
        if (window.devicePixelRatio == 2) {
            var images = $("img.retina");
              // loop through the images and make them hi-res
            for(var i = 0; i < images.length; i++) {
                // create new image name
                var imageType = images[i].src.substr(-4);
                var imageName = images[i].src.substr(0, images[i].src.length - 4);
                imageName += "@2x" + imageType;
                //rename image
                images[i].src = imageName;
            }
        }
        
        
        
        if(window.location.href.indexOf("checkout") != -1){ 
            mobile_ha.common.getTempUserSelection();
        }

           if (window.location.href.indexOf("checkout") !== -1 )
       {
             self.getSpecificDayForStore( $("#hidden_default_store_id").val( ) );

                console.log( "SPECCIFICS!" );
             console.log( $("#hidden_default_store_id").val( ) );
              console.log( self.specific_day );
            console.log( self.specific_message );
            console.log(  new Date( self.specific_day ) );
       }
     


        if(window.location.href.indexOf('createsandwich') != -1){ 
        self.pull_to_refresh();
        }
        
        self.enableAllScrolls(); 
        self.home_page_bar_click();
        self.look_for_date_change();
       // if(window.location.href.indexOf('createsandwich') == -1 && window.location.href.indexOf('sandwich/gallery') == -1)
        if(window.location.href.indexOf('checkout') != -1)
        self.check_time_and_change_if_needed(  );
        
        
    },
    
    pull_to_refresh : function(){ //alert("hi");
            // Function to disable "pull-to-refresh" effect present in some webviews.
            // Especially Crosswalk 12 and above (Chromium 41+) runtimes.

            window.addEventListener('load', function() {
                var lastTouchY = 0 ;
                var maybePreventPullToRefresh = false ;

                // Pull-to-refresh will only trigger if the scroll begins when the
                // document's Y offset is zero.

                var touchstartHandler = function(e) {
                    if( e.touches.length != 1 ) {             
                        return ;
                    }
                    lastTouchY = e.touches[0].clientY ;
                    // maybePreventPullToRefresh = (preventPullToRefreshCheckbox.checked) && (window.pageYOffset == 0) ;
                    maybePreventPullToRefresh = (window.pageYOffset === 0) ;
                    //document.getElementById('txtLog').textContent = "maybePreventPullToRefresh: " + maybePreventPullToRefresh;
                };

                // To suppress pull-to-refresh it is sufficient to preventDefault the
                // first overscrolling touchmove.

                var touchmoveHandler = function(e) {
                    var touchY = e.touches[0].clientY ;
                    var touchYDelta = touchY - lastTouchY ;
                    lastTouchY = touchY ;

                    if (maybePreventPullToRefresh) {
                        maybePreventPullToRefresh = false ;
                            e.preventDefault() ;
                            return ;
                       
                    }
                };

                document.addEventListener('touchstart', touchstartHandler, false) ;
                document.addEventListener('touchmove', touchmoveHandler, false) ;
            }) ;
        },
    //for clearing share enable cookie.
    shareCookieClear : function(){
        
        var self = this;
        


        
          if(window.location.href.indexOf("createsandwich") != -1){
              
              try{
               FB.getLoginStatus(function(response){ 
                if(response.status != "connected"){
                     self.setCookie('share_active',0,1);  
                     self.setCookie('share_id',0,1);
                 } else {
                     if( parseInt(self.getCookie('share_active')) == 1) {
                     $('.share-sandwich').trigger('click');
                     }
                 }
                
              });
            }
            catch( e )
            {
                if( parseInt(self.getCookie('share_active')) == 1) {
                    $('.share-sandwich').trigger('click');
                }
            }
                 
              } else if(window.location.href.indexOf("menu") != -1){
              
                FB.getLoginStatus(function(response){ 
                if(response.status != "connected"){
                     self.setCookie('share_active',0,1);  
                     self.setCookie('share_id',0,1);
                 } else {
                     
                    if( parseInt(self.getCookie('share_active')) == 1) {
                        shid = parseInt(self.getCookie('share_id'));
                        if(shid > 0){ 
                        $('.menu-listing li[data-id="'+shid+'"] .sandwich_gallery_view').trigger('click');
                        $('.share-sandwich').trigger('click');
                        }
                    }
                 }
              });
              
              } else if(window.location.href.indexOf("gallery") != -1){
              
                FB.getLoginStatus(function(response){ 
                if(response.status != "connected"){
                     self.setCookie('share_active',0,1);  
                     self.setCookie('share_id',0,1);
                 } else {
                    
                        if( parseInt(self.getCookie('share_active')) == 1) {
                            shid = parseInt(self.getCookie('share_id'));
                            if(shid > 0){ 
                         $('.menu-listing li[data-id="'+shid+'"] .sandwich_gallery_view').trigger('click');
                         $('.share-sandwich').trigger('click');
                            }
                        }
                     
                 }
               });
              
              } else {
                 self.setCookie('share_active',0,1);  
                 self.setCookie('share_id',0,1);
                
              }
        
        
    },
    
    windowScrollPreventWhenPopup: function(){
        var self = this;
        
         $('.extra_id').on('blur',function(){    self.enableScroll(); });
         $('.extra_id').on('focus',function(){    self.disableScroll(); });
            
    },
    
    get_url_params: function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        return results == null ? null : results[1];
    },
    
    
    //checkout page value change check.
    on_chekckout_quantity_value_change : function(){
       
        var optval = $('input[name="itemQuty"]:focus').val()
       var self  = this; 
       $('input[name="itemQuty"]').on('keyup',function(e){
           
           if( e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode ==40 || e.keyCode == 8){
              return false;
          }
           
          
         valu = $(this).val(); 
         valu = valu.replace(/\D/g,'');
         valu = parseInt(valu);
         if(isNaN(valu) == true) { 
            if(isNaN(optval) == true)   valu = 0;  else  valu = parseInt(optval); 
          }
         
         if(valu <= 9) valu = '0'+valu;
        
         $(this).val(valu);
         self.allBarsNotRed();
       });
    },

    show_sandwich_detail_onload: function() {
        
        var self = this;
        
        $(window).load(function() {
            sandwichId = self.get_url_params("galleryItem");
          
            if (window.location.href.indexOf('gallery') != -1 && sandwichId) {
              
                sandwichId = parseInt(sandwichId);
                // $(document).find('.menu-listing').find("[data-id='" + sandwichId + "']").find('#view-sandwich-popup').trigger('click');
                if($('.menu-listing').find("[data-id='" + sandwichId + "']").length > 0)
                {
                    $('.menu-listing').find("[data-id='" + sandwichId + "']").find('#view-sandwich-popup').trigger('click');
                }
                else
                {
                    var user_id = (mobile_ha.common.getCookie('user_id'))?ha.common.getCookie('user_id'):0;
                    $.ajax({
                        type: "POST",
                        data: {
                            'id': sandwichId,
                            'user_id':user_id
                        },
                        url: SITE_URL + 'sandwich/get_individual_sandwich_data/',
                        async: false,
                        success: function(data) {
                            json = JSON.parse(data);
                           
                            if (json.state == false) {
                                alert("Sorry sandwich you are looking for has been removed!")
                            } else 
                            {
                                var append = '';
                                append +='<div class="p" class="right-rgt" data-sandwich_desc="'+json.sandwich_desc+'" data-flag="'+json.flag+'" data-menuadds="'+json.menu_add_count+'" data-userid ="'+json.uid+'" rel="ADD TO CART" data-cheese="" data-topping="" data-cond="" data-toast="'+json.menu_toast+'" data-formatdate="'+json.formated_date+'" data-username="'+json.user_name+'" data-protien="" data-bread=""  data-date="'+json.date_of_creation+'" data-price="'+json.sandwich_price+'" data-id="'+json.id+'" data-name="'+json.sandwich_name+'" data-likeid="'+json.like_id+'" data-likecount="'+json.like_count+'" data-typeSandwich="FS">';
                                append += '<li><img data-href="'+SITE_URL+'createsandwich/index/'+json.id+'" src="'+json.image_path+'" alt="sandwitchimageview" class="sandwich_gallery_view">';
                                append += '<input type="hidden" id="sandwichGalleryImg_'+json.id+'" value="'+json.imagepath+'"><input type="hidden" name="chkmenuactice'+json.id+'" value="'+json.menu_is_active+'" /><h4 class="sandwich_gallery_view">{'+json.sandwich_name+'|upper|truncate:20:"..":true}</h4><a href="#" class="add-cart" id="view-sandwich-popup">VIEW</a><input type="hidden" name="saved_tgl'+json.id+'" value="'+json.saved+'">';
                                append += '</li><input type="hidden" name="hidden_uid" id="hidden_uid" value="{$smarty.session.uid}" ></div>';
                                $('.menu-listing').append(append);
                                $('.menu-listing').find("[data-id='" + sandwichId + "']").find('#view-sandwich-popup').trigger('click');
                                
                            }

                        }
                    });
                }
                // $.ajax({
                //     type: "POST",
                //     data: {
                //         'id': sandwichId
                //     },
                //     url: SITE_URL + 'sandwich/get_individual_sandwich_data/',
                //     async: false,
                //     success: function(data) {
                //         json = JSON.parse(data);
                       
                //         if (json.state == false) {
                //             alert("Sorry sandwich you are looking for has been removed!")
                //         } else {
                //             $("#sandwich-popup-gallery").show();
                //             $("#sandwich-popup-gallery .sandwich_name").text(json.sandwich_name);
                //             $("#sandwich-popup-gallery .view-popup-scroll").text(json.sandwich_desc);
                //             $("#sandwich-popup-gallery img:first").attr('src', json.imagepath);
                //             $("#sandwich-popup-gallery img:first").attr('width', 350);
                //             $("#sandwich-popup-gallery img:first").attr('height', 194);
                //             $("#sandwich-popup-gallery .user_name").text(json.user_name);
                //             $("#sandwich-popup-gallery .created_date").text(json.date_of_creation);
                //             $("#sandwich-popup-gallery .likes_cnt").text(json.like_count);
                //             $("#sandwich-popup-gallery .menu_adds").text(json.menu_add_count);
                //             $("#sandwich-popup-gallery .save-menu").attr('rel', json.id);
                //         }

                //     }
                // });
            }
        });

    },
    
    home_page_bar_click: function(){
        
        $("ul.home_page_menu li").click(function(){
            var href = $(this).find(".right a").attr("href");
            window.location = href;
        })
    },  
    
    
    delete_credit_card : function(){
        var self = this;
        
        $('.remove-card').unbind('click').on('click',function(){
            
            id   = $(this).attr('rel');
            var pitem  = $(this).parent().parent()
            if(id) {
                chkConfirm = confirm("Are you sure you want to remove this card!");
                if(chkConfirm == true){
                    $.ajax({
                         type: "POST",
                         data: {
                             'id': id
                         },
                         url: SITE_URL + 'checkout/removeCard',
                         success:function(e){ 
                            pitem.remove(); 
                         }
                         
                    });
                } else return;
            }
            
        });
        
        
    },
    
    
    pageBackEvent: function(){
        
        $(".pages .back-register").click(function(){            
            $("#register-block").show();
        });
        
        $(".pages .friend-button").click(function(){            
            window.location.href = siteurl;
        });
    
    },  
    enableAllScrolls: function(){
        setTimeout(function(){
            $(".common-scrollbar").mCustomScrollbar({
                theme:"minimal"
            });
        }, 1500);
        
        setTimeout(function(){          
            $(".common-scrollbar").removeClass("mCS-autoHide");
        }, 1600);
    },
    
    //~checking if any 3-foot of 6-foot is existing in order if any order will be available after 24 hr only. if everything fine this function will return true
    checkBreadType : function(){
        
        var self = this;
        var bradTypeAvilable = false
        
        $('.bread_type').each(function(){  
            
            if($(this).val() > 0) {  
                
                bradTypeAvilable = true
            } 
        });
    
        
       if(bradTypeAvilable == true){
            
           if( $('#radio1:checked').length > 0 ){
                
               alert("Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you!");    
               $('.place-order-checkout').addClass('disabled');
               return 'error';
                
            } else { 
                $('.place-order-checkout').removeClass('disabled');
            currentTime     = self.getServerTime(); 
            //~calculate hover diffence here.
            currentTime     = currentTime.split(',');
                        $date = self.getCookie('specific_date_selected');
                        $time  = self.getCookie('specific_time_selected');
            $date           = $('.delivery_date').val();            
            $time           = $('.timechange select').val();
            
            $date = new Date($date);
            
            
            $timeSplits     = $time.split(':');
                
            
            $dateSelectd    = new Date($date.getFullYear(),$date.getMonth(),$date.getDate(),$timeSplits[0],$timeSplits[1]); 
            $currentTime    = new Date(currentTime[0],parseInt(currentTime[1])-1,currentTime[2],currentTime[3],currentTime[4]);
            
    
            diffMs          =  $dateSelectd - $currentTime;
                                    
            hours           =  diffMs/3600000;
            
            if(hours<24){
                
                alert("Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you!");
                $('.place-order-checkout').addClass('disabled');
                return 'error';
            }           
          }         
        }       
    },
    
    //~getting server time using async Ajax call.
    getServerTime : function(){
                
        var dateTime;
        var self = this;
        var currentDayHour = 0;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentHour/',
            async: false,
            success: function(data) {
                 $json =  JSON.parse(data);
                
                if($json.date_time_js){
                    dateTime = $json.date_time_js;
                }
                
            }
        });
        return dateTime;
    },
        
    get_all_zip_codes: function() {
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'myaccount/getAllzipcodes/',
            async: false,
            dataType: 'json',
            success: function(data) {
                self.zipcodes = [];
                Object.keys(data).forEach(function(e) {
                    self.zipcodes.push(data[e]);
                });


            }
        });

    },
    
    get_all_product_extra: function() {
        var self = this;

        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'sandwich/getAllproductsExtras/',
            async: false,
            dataType: 'json',
            success: function(data) {
                self.product_extras = data;
            }
        });

    },
    
    body_scroll_ops  : function(state){
        
        if(navigator.userAgent.match(/(Android)/)){
            if(state == false) $("body").attr("style","overflow:hidden !important");
            else if(state == true) $("body").attr("style","overflow:auto !important")
        }
        
        if($("body").hasClass("scrollDisabled")){
            y_offsetWhenScrollDisabled= $(window).scrollTop();
            $('body').removeClass('scrollDisabled').css('margin-top', 0);
            $(window).scrollTop(y_offsetWhenScrollDisabled);
        }
    },
    
    
    disableScroll : function() {
         
        if (window.addEventListener) // older FF
              window.addEventListener('DOMMouseScroll', preventDefault, false);
          window.onwheel = preventDefault; // modern standard
          window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
          window.ontouchmove  = preventDefault; // mobile
          document.onkeydown  = preventDefaultForScrollKeys;
    },

    enableScroll : function() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null; 
        window.onwheel = null; 
        window.ontouchmove = null;  
        document.onkeydown = null;  
    },

    
    
    //Late loading Sandwich Gallery
    
    lazy_load_sandwiches : function(){
 
     var self = this; var set = 0;
     $(window).load(function(){
        if(window.location.href.indexOf("gallery") != -1){
    
             $(window).scroll(self.bind_scrolls);
        }
     });
    },
    
    Ajax_busy : false,
    
    bind_scrolls         : function(){
            
    var $current_count = parseInt($('.menu-listing li').length);
    var $total_items   = parseInt($('input[name="total_item_count"]').val());
 
    //filter Scroll Prevention
   var sandwichFilter = $('#sandwich-popup-filter').css('display');
    if(sandwichFilter == 'none'){
        if($('.menu-listing li').length < 20){ 
            
            if( $(window).scrollTop() > 50 ){ //prev value 500 Revision Point
                            
                 if(mobile_ha.common.Ajax_busy == false) mobile_ha.common.load_more_sandwiches();
            }
        } else { 
            
        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
             setTimeout(function(){   if(mobile_ha.common.Ajax_busy == false && ($current_count <  $total_items) ) {  
                    mobile_ha.common.load_more_sandwiches(); 
                  }  },300);
             } 
        }
    }
     
    },

    load_more_sandwiches : function(){
        
    var self = this;
    self.Ajax_busy = true;
    var noScroll = false;
     
     
     //disable scroll events for filter
    $filterData = self.gallery_search_array();
    $filterKeys = Object.keys($filterData);
    $filterKeys.forEach(function(i,j){
        if($filterData[i].length > 0){
            noScroll = true;
        }
    });
    
    if(noScroll == true){ return false; } 
     
     $('.loadMoreGallery').show();
     $current_count = $('.menu-listing li').length;
     $total_items   = $('input[name="total_item_count"]').val();
     sort_id        = $('input[name="sort_type"]').val(); 
     var searchTerm = $("#searchTerm").val();
     if( parseInt($current_count) < parseInt($total_items)){ 
     $.ajax({
            type: "POST",
            data:{ "count" : $current_count , "sort_id" : sort_id, "searchTerm":searchTerm },
            url: SITE_URL+'sandwich/get_more_gallery',
            success: function(e){
                $('.menu-listing li:last').after(e);
                //mobile_ha.sandwichcreator.listing_click(); //Revision Point
                self.Ajax_busy = false;
                   $('.loadMoreGallery').hide();
            }
        }); 
     } else {   $('.loadMoreGallery').hide(); } 
         
    },
    
    
    menuAction : function(){
        $('#menu').slicknav({
            'afterOpen': function(trigger){
                FB.XFBML.parse();
            }
        });
        
         $('.banner, .container, .create-sandwhich-menu-wrapper').click (function(){
                    $('#menu').slicknav('close');                
        });
         
    },
    
    //Lazy loading for User Menu Sandwiches
    lazyLoadMenuSandwiches : function(){
 
        var self = this; var set = 0;
        $(window).load(function(){
            if(window.location.href.indexOf("menu") != -1 && window.location.href.indexOf("friends_menu") == -1){       
                $(window).scroll(self.menu_bind_scrolls);
            }
        });
    },
    
    Menu_More_Item : true,
    
    menu_bind_scrolls         : function(){         
        
        //filter Scroll Prevention
        if(mobile_ha.common.Ajax_busy == false && mobile_ha.common.Menu_More_Item == true) mobile_ha.common.load_more_menu_sandwiches();        
     
    },
    
    load_more_menu_sandwiches : function(){
        
        var self = this;
        self.Ajax_busy = true;
        var noScroll = false;
        totalMenuitems = $('input[name="countMenuItems"]').val();
         
        $('.loadMoreGallery').show();
        $current_count = $('.user-menu-sandwiches li[data-id]').length; 
        
        if(totalMenuitems == $current_count){ $(".loadMoreGallery").hide(); return false; } 
             
        if( parseInt($current_count)){ 
            $.ajax({
                    type: "POST",
                    data:{ "count" : $current_count-1 },
                    url: SITE_URL+'menu/lazyLoadMoreUserSandwich',
                    success: function(e){
                        if(e.trim() != "None"){
                            $('.user-menu-sandwiches li:last').after(e);                        
                            self.Ajax_busy = false;
                            $('.loadMoreGallery').hide();
                        }else{
                            mobile_ha.common.Menu_More_Item = false;
                            $('.loadMoreGallery').hide();
                        }   
                    }
            }); 
            
        } else {   $('.loadMoreGallery').hide(); } 
         
    },
    
    loginAction : function(){
        var self = this;
        
        $('.login-button').click(function(){
            if($(this).text() == 'LOGIN'){ 
            $('#login-block-mail').show(); 
            self.body_scroll_ops(false);
            }
        });
        
        $('.login-button-cart').click(function(){
            if($(this).text() == 'LOGIN'){ 
            $('#login-block').show(); 
            self.body_scroll_ops(false);
            }
        });
        
        $('.connect-login').on('click',function(){
            $('#login-block-mail').show();
            self.body_scroll_ops(false);
        });
        
        
        $('.connect-facebook').on('click',function(e){
            e.preventDefault();
            $hrf = $(this).attr('href');
            self.facbook_auth_window($hrf);
        });
        
        $(document).on('click',"#login_btn", function(e) { 
            
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                
                $("div.login-mail-wrapper span.error_msg").html('');
                var lemail = $("#lemail").val();
                var lpwd = $("#lpwd").val();

                if (lemail == "") {
                      $("div.login-mail-wrapper span.error_msg").html("Please enter a valid email address").show();
                } else if (!filter.test(lemail)) {
                      $("div.login-mail-wrapper span.error_msg").html("Please enter a valid email address").show();

                } else if (lpwd == "") {
                      $("div.login-mail-wrapper span.error_msg").html("Please enter a password").show();
                } else if (lpwd.length < 6) {
                      $("div.login-mail-wrapper span.error_msg").html("Your password must be at least 6 characters long").show();
                } else {
                    $.post(SITE_URL+"myaccount/loginCheck", {'email':lemail,'password':lpwd},
                        function(data, status) {
                            data = JSON.parse(data);

                           if (data.state == "true") {
                                var accounturl;
                                loc = window.location.href;
                                arr = loc.split(SITE_URL);
                                accounturl = SITE_URL + arr[1].replace('#', '');
                                randno  = Math.round( Math.random() * 10000000 );
                                if(siteurl == accounturl){
                                    accounturl = accounturl+"home/index/?pid="+randno;
                                } else {
                                 
                                    if(accounturl.indexOf('pid') != -1){
                                        array = accounturl.split('pid');
                                        accounturl = array[0]+"pid="+randno;
                                    } else {
                                        
                                        if(accounturl.indexOf('%3f') != -1){
                                            accounturl = SITE_URL+"home/index/?pid="+randno;
                                        }else{
                                            accounturl = accounturl+"index/?pid="+randno;
                                        }
                                        
                                    }
                                    
                                }
                                
                                
                                
                                if(self.tempSlector != null){
                                    $(self.tempSlector).trigger('click');
                                    self.tempSlector = null; 
                                    
                                    var intV = setInterval(function(){ 
                                        
                                         if( self.tmpTransAnim == 1){ 
                                          window.location = accounturl;
                                          clearInterval(intV);
                                         }
                                    },10);
                                                                    
                                } else {
                                    $(".sandwich-popup").hide();
                                     window.location = accounturl;
                                }
                                
                                
                           
                               
                                
                                
                            } else {
                                
                                $("div.login-mail-wrapper span.error_msg").html(data.msg).show();
                            }
                        }
                    );

                }
        });
    },
    cookieLoginAction : function(){
        
        var self = this;
          $("#cookie_login_btn").on('click', function(e) { 
              
               var lemail = $("#cemail").val();
                var lpwd = $("#cpwd").val();
                
                
                if (lpwd == "") {

                    $("#lpwd_cookie").addClass('register').focus();
                    $(".error_msg").html("Please enter a password");
                } else if (lpwd.length < 6) {
                    $("#lpwd_cookie").addClass('register').focus();
                    $(".error_msg").html("Your password must be at least 6 characters long");
                } else {
                    $("#cookie_login_btn").text('PLEASE WAIT...');
                    $("#cookie_login_btn").addClass('disabled_login');
                    $.post(SITE_URL + "myaccount/loginCheck", {'email':lemail,'password':lpwd},
                            function(data, status) {
                                data = JSON.parse(data);
                                if (data.state == "true") {
                                    if (self.tempSlector != null) {
                                        var accounturl;
                                        loc = window.location.href;
                                        arr = loc.split(SITE_URL);
                                        accounturl = SITE_URL + arr[1].replace('#', '');
                                        window.location = accounturl;
                                        $(self.tempSlector).trigger('click');
                                        self.tempSlector = null;
                                    }
                                    else if(mobile_ha.common.getCookie('doneSandwich')==='true'){
                                        $('#cookie-login-block').hide();
                                        mobile_ha.common.setCookie('doneSandwich', false, 1)
                                        mobile_ha.sandwichcreator.show_end();
                                    } else{
                                        var accounturl;
                                        loc = window.location.href;
                                        arr = loc.split(SITE_URL);
                                        accounturl = SITE_URL + arr[1].replace('#', '');
                                        window.location = accounturl;
                                    } 
                                } else {
                                    $(".error_msg").html(data.msg);
                                    $("#cookie_login_btn").text('LOG IN');
                                    $("#cookie_login_btn").removeClass('disabled_login');
                                }
                            }
                        );
                    
                }
          });
    
    },

    facebookLoginAction:function(){
        $("#fbLoginCookie").on('click', function(e) { 
            $("#fbLoginCookie").text('PLEASE WAIT...');
            $("#fbLoginCookie").addClass('disabled_login');
        });
    },
    createAccountAction : function(){
        var self = this;
        $('.connect-register').on('click',function(){
            $('#register-block').show();
            self.body_scroll_ops(false);
        });
        $(document).on("click","#create_account", function(e) {
             $("div.register-wrapper div.form-holder span.error_msg").html('');
              var rfname = $("#rfname").val();
              var rlname = $("#rlname").val();
              var remail = $("#remail").val();
              var rpwd = $("#rpwd").val();
              var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
              var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
              if (rfname == "") {

                  $("#rfname").addClass('register').focus();
              } else if (rlname == "") {

                  $("#rlname").addClass('register').focus();
              } else if (remail == "") {

                  $("#remail").addClass('register').focus();


              } else if (!filter.test(remail)) {
                  $("#remail").addClass('register').focus();
                  $("div.register-wrapper div.form-holder span.error_msg").html("Please enter a valid email address").show();
              } else if (rpwd == "") {

                  $("#rpwd").addClass('register').focus();
              } else if (rpwd.length < 6) {
                  $("#rpwd").addClass('register').focus();
                  $("div.register-wrapper div.form-holder span.error_msg").html("Your password must be at least 6 characters long").show();
              }  else {
                  $("div.register-wrapper div.form-holder span.error_msg").html('');

                  $.ajax({
                          type: "POST",
                          url: SITE_URL + "myaccount/createAccount",
                          data: {'email':remail,'password':rpwd,'firstname':rfname,'lastname':rlname},
                          success: function(data) {
                          data = JSON.parse(data);
                            
                            var accounturl;
                            var tempClick = 0;
                                if (data.state == false) {
                                      $("div.register-wrapper div.form-holder span.error_msg").html(data.msg).show();                                    
                                } else {
                                    $(".sandwich-popup").hide();
                                
                                    if (window.location.href.indexOf("createsandwich") != -1) {
                                        accounturl = SITE_URL + "createsandwich";
                                        window.location = accounturl;
                                    } else {
                                                                                
                                        loc = window.location.href;
                                        arr = loc.split(SITE_URL);
                                        accounturl = SITE_URL + arr[1].replace('#', '');
                                      
                                        if(self.tempSlector != null){
                                            $(self.tempSlector).trigger('click');
                                            self.tempSlector = null; 
                                            
                                            var intV = setInterval(function(){ 
                                                
                                                 if( self.tmpTransAnim == 1){ 
                                                  window.location = accounturl;
                                                  clearInterval(intV);
                                                 }
                                            },10);
                                                                            
                                        } else {
                                            
                                            window.location = accounturl;
                                        }
                                       
                                    }
                                    
                                }
                            }  
                      });

            }
        });
    },
    forgotPasswordAction : function(){
        var self = this;
        $('.show-forgot-block').on('click',function(){
            $('#forgot-block').show();
            self.body_scroll_ops(false);
        });

        $('.connect-login').on('click',function(){
            
            $('#forgot-block').hide(); 
            $('#login-block-mail').show(); 
            self.body_scroll_ops(false);
            
        });
        
         $("#forgot_btn").on('click', function(e) {
            var fbtn = $(this);
             $(".error_msg").html('');
             var femail = $("#femail").val();
             var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
             if (femail == "") {

                 $("#femail").addClass('register').focus();


             } else if (!filter.test(femail)) {
                 $("#femail").addClass('register').focus();
                 $(".error_msg").html("Please enter a valid email address").show();
             } else {
                fbtn.addClass('disabled');
                 $.ajax({
                         type: "POST",
                         url: SITE_URL + "myaccount/forgotPassword",
                         data:  {'email':femail}
                     })
                     .done(function(data) {
                         data = JSON.parse(data);
                         
                         if (data.state == false) {
                             $(".error_msg").html(data.msg).show();
                             fbtn.removeClass('disabled');
                         } else {
                             $(".error_msg").html("A link has been sent to your email ID to reset the password.").show();                            
                            fbtn.removeClass('disabled');
                         }

                     });
             }
         });
      
    },
    
    showPopupAtScreenPosition: function(id){
        var self = this; 
        $(id).show();
        self.windowScrollPreventWhenPopup();
    },
    
     
        
    sandwichGalleryAction : function(){
        var self = this;
         $(".sortby").change(function(){
                var sort_id=$(this).val();
                var no_search = $('input[name="search"]').length;
                if(no_search == 0){
                window.location.assign(""+SITE_URL+"sandwich/gallery/?sortid="+sort_id+"");
                } else {
                    $(".apply-filter").trigger("click");
                }
         });
         
         $(document).on("click", '.sandwich_gallery_view', function(e){
           $(this).siblings('#view-sandwich-popup').trigger('click');
                    //e.preventDefault();
                    //  self.body_scroll_ops(false);
                //  self.showPopupAtScreenPosition('#sandwich-popup-gallery');
                 //    var parent;
           //          parent = $(this).parent();
           //          pdata = parent.data();
                 //    src = parent.find('img').attr('src');
                    // $("#sandwich-popup-gallery .banner").find('img').attr({'src':src });
                    // $("#sandwich-popup-gallery").find('p').text(pdata.sandwich_desc);
           //          $("#sandwich-popup-gallery").find('.sandwich_name').text(pdata.name);
           //          $("#sandwich-popup-gallery").find('.user_name').text(pdata.username);
           //          $("#sandwich-popup-gallery").find('.created_date').text(pdata.formatdate);
           //          $("#sandwich-popup-gallery").find('.likes_cnt').text(pdata.likecount);
           //          $("#sandwich-popup-gallery").find('.menu_adds').text(pdata.menuadds);
           //          $("#resetQty").val('01');

           //          if (pdata.likeid)
     //               $(".Like_sandwich").text("Liked");
     //               else $(".Like_sandwich").text("Like");
                    
           //          $("#sandwich-popup-gallery").find('.amount').text('$'+pdata.price);
           //          $("#sandwich-popup-gallery .save-menu").attr('rel', pdata.id);
                    // $("#sandwich-popup-gallery").find("#hidden_sandwich_id").val(pdata.id);

           //          if ((pdata.toast==1)) {
        //                 $("#sandwich-popup-gallery").find('.toast').prop('checked', true);
        //             }else{
        //                 $("#sandwich-popup-gallery").find('.toast').prop('checked', false);

        //          }

         });
         $(document).on("click", '.view_sandwich', function(e){
            
           $(this).parentsUntil('.save-sandwich--wrap').next().find('#view-sandwich-popup').trigger('click');
         });

         $(document).on("click", '.editSandwich', function(e){
            e.preventDefault();
             mobile_ha.sandwichcreator.clear_temp_session();
            session = mobile_ha.common.check_session_state();

            if (session.session_state == false) 
            {
                $("#mobile-sandwiches-popup").hide();
                mobile_ha.common.login_popup();
                return;
            }
            var pdata = $(this).parent().parent().data();
            var id = $(this).attr("id");
            var isSaved = $(this).next('a').text();

           mobile_ha.sandwichcreator.quickEditLoadEvents();
           $('.edit-sandwich-popup').attr("rel",id);
           $("#mobile-sandwiches-popup .final-list-wrapper .view-popup-scroll").text(pdata.bread);
           $("#mobile-sandwiches-popup .final-list-wrapper a.save-sandwich-popup").text(isSaved);
           $('.edit-sandwich-popup').trigger('click');
         });

         $('.menu-listing a:contains(Edit)').unbind('click');
        $(document).on('click','.menu-listing a:contains(Edit)',function(x) 
        {
            x.preventDefault();
            var parent;
            parent = $(this).parent().parent();
            src = parent.find('img').attr('src');
            sType = parent.find('.typeSandwich').val();
            
            pdata = parent.data();
            $.ajax({
                type: "POST",
                url: SITE_URL + 'sandwich/getCurrentprice',
                data: {'pdata': pdata},
                success: function(price) {
                   //window.location.href = SITE_URL+"createsandwich/index/"+pdata.id;
                   pdata.price =  price;
                }
            });
            mobile_ha.sandwichcreator.quickEditLoadEvents();
            $('.edit-sandwich-popup').attr("rel", pdata.id);
            $('.edit-sandwich-popup').trigger('click');
        });

        /* Sandwich popups sandwichimg_*/
        $(document).on("click", '#view-sandwich-popup', function(e)
        {
            e.preventDefault();
            var parent;

            parent = $(this).parent().parent();

            sType = parent.find('.typeSandwich').val();
            pdata = parent.data();

            if(sType == undefined)
            {
                sType = pdata.typesandwich;
            }
            //console.log(sType);return;
            
            $ptxt = "";
            breadTxt = pdata.bread;
            breadTxt = breadTxt.trim();
            breadTxt = breadTxt.replace(/##/g, " ");
            $ptxt = breadTxt;
            src = $("#sandwichimg_"+pdata.id).attr('src');
            if($("#sandwichGalleryImg_"+pdata.id).val() != undefined)
            {
                src = $("#sandwichGalleryImg_"+pdata.id).val();
            }
            //console.log(src);return;
            // session = mobile_ha.common.check_session_state();
            // if (session.session_state == false) 
            // {
            //    mobile_ha.common.login_popup();
            //    return;
            // }

            if (pdata.protien != "") 
            {

                protiens = pdata.protien.replace(/ /g, ', ')
                protiens = protiens.trim();
                protiens = protiens.replace(/##/g, " ");
                if ($ptxt) $dl = ', ';
                else $dl = '';
                $ptxt += $dl + protiens;
            }
            if (pdata.cheese != "") 
            {
                chesses = pdata.cheese.replace(/ /g, ', ')
                chesses = chesses.trim();
                chesses = chesses.replace(/##/g, " ");
                $ptxt += chesses;
            }
            if (pdata.topping != "") 
            {
                toppings = pdata.topping.replace(/ /g, ', ')
                toppings = toppings.trim();
                toppings = toppings.replace(/##/g, " ");
                if ($ptxt) $dl = ', ';
                else $dl = '';
                $ptxt += toppings;
            }
            if (pdata.cond != "") 
            {
                conds = pdata.cond.replace(/ /g, ', ')
                conds = conds.trim();
                conds = conds.replace(/##/g, " ");
                $ptxt += conds;
            }

            txt = $(this).text();

            if ($(this).attr("class") == "view_sandwich") 
            {
                txt = 'VIEW';
            }

            if (txt === 'ADD TO CART') 
            {
                return;
            }
            $('.leftSpinner').unbind('click').on('click', function() 
            {
                input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            $('.rightSpinner').unbind('click').on('click', function() 
            {
                input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            if (txt === 'VIEW' || txt === 'ADD') 
            {
                randno  = Math.round( Math.random() * 10000000 );
                refreshurl = window.location.href;
                array = refreshurl.split('pid');
                refreshurl  = array[0]+"pid="+randno;
                $('.share-sandwich-popup').unbind('click').on('click', function() 
                {
                    session = mobile_ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        $("#mobile-sandwiches-popup").hide();
                        mobile_ha.common.login_popup();
                        return;
                    }
                    main = $(this).parent().parent();
                    main = $(main);
                    sandwichname = main.find('.sandwich_name').text();
                    sandwichname = sandwichname.toUpperCase();
                    sandwichDesc = main.find('p:first').text()

                    $img = src;
                    sandwichID = pdata.id;
                    var feed = {
                        method: 'feed',
                        app_id: FB_APP_ID,

                   
                        link: SITE_URL + 'sandwich/gallery/?galleryItem=' + sandwichID,
                 
                        display: 'popup'
                    };

                    FB.ui(feed,  function ( response ){ }  );
                });

                $("#mobile-sandwiches-popup").find('.menucheck').prop('checked', false);
                $("#mobile-sandwiches-popup").find('#hidden_sandwich_id').val(pdata.id);

                if ((pdata.toast == 1)) 
                {
                    $("#mobile-sandwiches-popup").find('.menucheck').prop('checked', true);
                }

                $("#mobile-sandwiches-popup").find('.username').text(pdata.username);
                $("#mobile-sandwiches-popup").find('.text-box').val('01');
                
                if (pdata.flag == 1)
                {
                    $('.gold-flag').removeClass('gold-flag-op');
                }
                else
                {
                     $('.gold-flag').addClass('gold-flag-op');
                }
                // if (pdata.flag == 1) 
                // {
                //     $('.flagthis').text('This sandwich has been flagged');
                //     $('.flag-sandwich').text('This sandwich has been flagged');
                //     $('.flag-sandwich').css('text-decoration', 'none');
                //     $('.flag-sandwich').css('cursor', 'auto')
                // } 
                // else 
                // {
                //     $('.flagthis').text('flag this sandwich');
                //     $('.flag-sandwich').text('flag this sandwich');
                //     $('.flag-sandwich').css('text-decoration', 'underline');
                //     $('.flag-sandwich').css('cursor', 'pointer')
                // }

                // $('.flagthis').unbind('click').on('click', function(e) 
                // {
                //     e.preventDefault();
                //     $parent = $(this).parent().parent().parent();
                //     $txt = $(this).text();
                //     id = $($parent).find('input[name="hidden_sandwich_id"]');

                //     if ($txt == 'flagged') {

                //     } else {
                //         $(this).text('This sandwich has been flagged');
                //         $(this).css('text-decoration', 'none');
                //         $(this).css('cursor', 'auto');
                //         self.set_flag_state({
                //             'state': 1,
                //             'id': id.val()
                //         });
                //     }
                // });

                // if (pdata.likeid)
                //     $(".like-sandwich").text("Liked");
                // else $(".like-sandwich").text("Like");

                $("#mobile-sandwiches-popup").find('.datefromat').text(pdata.formatdate);

                image = $("#mobile-sandwiches-popup").find('img:nth-child(2)');
                
                $(".sandwich_name").text(pdata.name);
                $.ajax({
                    type: "POST",
                    url: SITE_URL + 'sandwich/getCurrentprice',
                    data: {'pdata': pdata},
                    success: function(data) {
                        $("#mobile-sandwiches-popup").find('.amount').text('$' + data);
                    }
                })
                /*$("#mobile-sandwiches-popup").find('.amount').text('$' + pdata.price);*/
                $("#mobile-sandwiches-popup").find('.view-popup-scroll').text(pdata.sandwich_desc);
                
                $(image).attr({
                    'src': src,
                    'width': '350',
                    'height': '194'
                });
                
                $("#mobile-sandwiches-popup .add-to-cart").text($(parent).attr('rel'));
                $("#mobile-sandwiches-popup .edit-sandwich-popup").attr('rel', pdata.id);
                $("#mobile-sandwiches-popup .saveMenu").attr({rel: pdata.id, sType: sType});

                // if (SESSION_ID) session = SESSION_ID;
                // else session = ha.common.getCookie("user_id");
                // session = parseInt(session);

                if ($('input[name="chkmenuactice' + pdata.id + '"]').length > 0) 
                {
                    if ($('input[name="chkmenuactice' + pdata.id + '"]').val() == 1)
                    {
                        $('.saveMenu').text('SAVED');
                        $('.saveMenu').addClass('removeMenu');
                    } 
                    else 
                    {
                        $('.saveMenu').text('SAVE');
                        $('.saveMenu').removeClass('removeMenu');
                    }
                }


                if ($('input[name="saved_tgl' + pdata.id + '"]').val() != "undefined" && $('input[name="saved_tgl' + pdata.id + '"]').val() > 0)
                {
                        $('.saveMenu').text('SAVED');
                        $('.saveMenu').addClass('removeMenu');
                        $('.check-sandwich-private').show();
                } 
                else 
                {
                    $('.saveMenu').text('SAVE');
                    $('.saveMenu').removeClass('removeMenu');
                    $('.check-sandwich-private').hide();

                }
                

                var isPub = $("#isPublic" + pdata.id).val();
                var isPublicFs = $("#isPublicFs"+ pdata.id).val();
                if(isPublicFs != undefined)
                {
                    isPub = isPublicFs;
                }
                if(isPub == 0)
                {
                    $('.make_private_check').prop('checked', true);
                }
                else
                {
                     $('.make_private_check').prop('checked', false);
                }


                $("#mobile-sandwiches-popup").show();
                var toast_sandwich = pdata.toast;
                $(".menucheck").unbind('change').on('change', function(e)
                {
                   toast_sandwich = $('.menucheck').is(':checked')?1:0;
                   if(pdata.typesandwich == "SS")
                   {
                        session = mobile_ha.common.check_session_state();
                        if (session.session_state == false) 
                        {
                            $("#mobile-sandwiches-popup").hide();
                            mobile_ha.common.login_popup();
                            return;
                        }
                        var sandwich_ = $('div[data-id="' + pdata.id + '"]');
                        $.ajax({
                            type: "POST",
                            data: {
                                "toast": toast_sandwich,
                                "item_id": pdata.id
                            },
                            url: SITE_URL + 'sandwich/updateToastmenu',
                            dataType: "json",
                            success: function(e) 
                            {
                                $("#toastID_"+pdata.id).data('toast',toast_sandwich);
                                if (toast_sandwich == 1) 
                                {
                                    $(sandwich_).find('.toasted').css('display', 'block');
                                } 
                                else 
                                {
                                    $(sandwich_).find('.toasted').css('display', 'none');
                                }
                            }

                        });
                   }
                 
                });

                $(".gold-flag").unbind('click').on('click', function(e)
                {
                    if($(".gold-flag").hasClass("gold-flag-op"))
                    {
                        session = mobile_ha.common.check_session_state();
                        if (session.session_state == false) 
                        {
                            $("#mobile-sandwiches-popup").hide();
                            mobile_ha.common.login_popup();
                            return;
                        }
                       $.ajax({
                                type: "POST",
                                data: {
                                    'id': pdata.id,
                                    'status': 1
                                },
                                url: SITE_URL + 'sandwich/set_flag/',

                                success: function(e) {
                                    $("#toastID_"+pdata.id).data('flag',1);
                                    $('.gold-flag').removeClass('gold-flag-op');
                                    
                                }
                            });
                    }
                    
                    
                });
                $(".saveMenu").unbind('click').on('click', function(e)
                {

                    e.preventDefault();
                    session = mobile_ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        $("#mobile-sandwiches-popup").hide();
                        mobile_ha.common.login_popup();
                        return;
                    }
                    $('input[name="chkmenuactice' + pdata.id + '"]').val('1');
                    $('#save_toggle'+pdata.id).text("SAVED");
                    $('#save_toggle'+pdata.id).addClass('saved');
                    

                    // var toast_sandwich=0;
                    
                    // if($("#check18").prop('checked'))
                    //     toast_sandwich =1;
                    session = mobile_ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        if(session.facebook_only_user == false)
                        {
                           mobile_ha.common.login_popup($(this));
                        } 
                        else 
                        {
                           mobile_ha.common.login_popup($(this));
                        }
                        return;
                    }

                    id = $(this).attr('rel');
                    var element = this;
                    //console.log($(".saveMenu").hasClass('removeMenu'));return;
                    if ($(".saveMenu").hasClass('removeMenu') == false) 
                    {
                        $.ajax({
                            type: "POST",
                            data: {
                                'id': id,
                                "uid": SESSION_ID,
                                "toast_sandwich":toast_sandwich
                            },
                            url: SITE_URL + 'sandwich/add_menu_multi/',
                            beforeSend: function() {
                                $("#ajax-loader").show();
                            },
                            complete: function() {
                                $("#ajax-loader").hide();
                            },
                            success: function(e) {
                                if(window.location.href.indexOf('friends_menu')>-1){
                                    // $("#mobile-sandwiches-popup").hide(); 
                                }else{
                                    $(element).text("SAVED");
                                    // $("#mobile-sandwiches-popup").hide(); 
                                    $(element).addClass('saved');
                                }
                                // window.location = refreshurl;
                            }
                        });
                    }
                });

                $(".removeMenu").unbind('click').on('click', function(e)
                {
                    $("#salad_popup").hide();
                    session = mobile_ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        $("#mobile-sandwiches-popup").hide();
                        mobile_ha.common.login_popup();
                        return;
                    }
                    e.preventDefault();
                    id = $(this).attr('rel');
                    //sType = $(".typeSandwich").val();
                    sType = pdata.typesandwich;
                    
                    //console.log("id "+id+" sType "+sType);return;
                    $.ajax({
                        type: "POST",
                        data: {
                            'id': id,
                            "uid": SESSION_ID,
                            "sType": sType
                        },
                        url: SITE_URL + 'sandwich/remove_saved/',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete: function() {
                            $("#ajax-loader").hide();
                        },
                        success: function(e) {
                            // window.location = refreshurl;
                        }
                    });

                    if ($('input[name="chkmenuactice' + pdata.id + '"]').length == 0) 
                    {
                        $("#mobile-sandwiches-popup").hide();
                        if(sType == "SS"){ $('.save-sandwich--wrap[data-id="' + id + '"]').remove();} 
                    } 
                    else 
                    {
                        $('.saveMenu').text('SAVE');
                        $('.saveMenu').removeClass('remove_menu');
                        $("#mobile-sandwiches-popup .save-menu").removeAttr('rel', pdata.id);
                        $('input[name="chkmenuactice' + pdata.id + '"]').remove();
                        $("#mobile-sandwiches-popup").hide();
                        if(sType == "SS"){$('.save-sandwich--wrap[data-id="' + id + '"]').remove();}
                    }
                });

                $(".make_private_check").unbind('change').on('change', function(e)
                {
                    session = mobile_ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        $("#mobile-sandwiches-popup").hide();
                        mobile_ha.common.login_popup();
                        return;
                    }
                    var ck_val = (this.checked)?0:1;
                    var id = pdata.id;
                    var sType = $(".typeSandwich").val();
                    if(sType == undefined)
                    {
                        sType = pdata.typesandwich;
                    }
                    $.ajax({
                            type: "POST",
                            data: {
                                "id": id,
                                "uid": SESSION_ID,
                                "is_public":ck_val,
                                "sType":sType
                            },
                            url: SITE_URL + 'sandwich/make_sandwich_private/',
                            success: function(e) 
                            {
                            }
                        });
                })

                $("#mobile-sandwiches-popup").find('.addToCart').unbind('click').on('click', function(e) 
                {
                    
                    var toast = 0;
                    if ($(".menucheck").is(":checked")) 
                    {
                        toast = 1;
                    }
                    e.preventDefault();

                    qty = $('input[name="itemCount"]').val();
                    if (toast) 
                    {
                        self.add_toast_sandwich_to_cart(pdata.id, toast, null, null, qty, $(this));
                    } 
                    else 
                    {
                         $.ajax({
                            type: "POST",
                            data: {
                                    "id": pdata.id,
                                    'pdata': pdata
                                  },
                            url: SITE_URL + 'sandwich/Updatecurprice/',                        
                            success: function(e) 
                            {
                            }
                        });
                        self.add_sandwich_to_cart(pdata.id, null, null, qty, $(this));
                    }
                    $("#mobile-sandwiches-popup").hide(  );
                });
            }

            $('.close-button').click(function(){
                $("#mobile-sandwiches-popup").hide();
            });
        });
    $('#Share-BarneyBrown').unbind('click').on('click', function() 
        {
            session = mobile_ha.common.check_session_state();
            if (session.session_state == false) 
            {
                mobile_ha.common.login_popup();
                return;
            }
           
            var feed = {
                method: 'feed',
                app_id: FB_APP_ID,
                name: "Barney Brown",
                link: CMS_URL
            };

            FB.ui(feed,  function ( response ){ }  );
        });
        
        $('.menu-listing a:contains(SAVE),.menu-listing a:contains(SAVED), .menu-saved_list a:contains(SAVE), .menu-saved_list a:contains(SAVED)').unbind('click');
        $(document).on('click','.menu-listing a:contains(SAVE),.menu-listing a:contains(SAVED), .menu-saved_list a:contains(SAVE), .menu-saved_list a:contains(SAVED)',function(x) 
        {

            x.preventDefault();
            session = mobile_ha.common.check_session_state();
            if (session.session_state == false) 
            {
                $("#mobile-sandwiches-popup").hide();
                mobile_ha.common.login_popup();
                return;
            }
            var parent;
            parent = $(this).parent().parent(); 

            pdata = parent.data(); 
            
            session = mobile_ha.common.check_session_state();
            if (session.session_state == false) 
            {
               mobile_ha.common.login_popup();
               return;
            }
            randno  = Math.round( Math.random() * 10000000 );
            refreshurl = window.location.href;
            array = refreshurl.split('pid');
            refreshurl  = array[0]+"pid="+randno;
            
            id = pdata.id;
            var toast_sandwich=0;
            var txt = $(this).text();
            var element = this;
            //var sType = $(".typeSandwich").val();
            var sType = pdata.typesandwich;
            // if(sType == undefined)
            // {
            //     sType = pdata.typesandwich;
            // }

            
           if(txt == "SAVE")
           {
               $.ajax({
                            type: "POST",
                            data: {
                                    "id": id,
                                    "uid": SESSION_ID,
                                    "toast_sandwich":toast_sandwich
                                },
                            url: SITE_URL + 'sandwich/add_menu_multi/',
                            beforeSend: function() {
                                $("#ajax-loader").show();
                            },
                            complete: function() {
                                    $("#ajax-loader").hide();
                                },
                                success: function(e) {
                                    if(window.location.href.indexOf('friends_menu')>-1){
                                    }else{
                                        $(element).text("SAVED");
                                        $(element).addClass('saved');
                                    }
                                    //window.location = refreshurl;
                                }
                        });
           }
           if(txt == "SAVED")
           {

                    $.ajax({
                        type: "POST",
                        data: {
                            "id": id,
                            "uid": SESSION_ID,
                            "sType":sType
                        },
                        url: SITE_URL + 'sandwich/remove_saved/',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete: function() {
                            $("#ajax-loader").hide();
                        },
                        success: function(e) 
                        {
                            if(e == "SS")
                            {
                                $('input[name="chkmenuactice' + pdata.id + '"]').remove();
                                $('.menu-listing li[data-id="' + id + '"]').remove();
                            }
                            
                            $(element).text("SAVE");
                            $(element).removeClass('saved');
                            $("#lockid_"+id).hide();
                            if(sType == "SS")
                            {
                                $('.save-sandwich--wrap[data-id="' + id + '"]').remove();
                            }

                            // window.location = refreshurl;
                        }
                    });
           }
        });

        $(".quickAddToCart").unbind('click').on('click', function(e) 
        {
            e.preventDefault();
            var parent;
            parent = $(this).parent().parent().parent();            
            pdata = parent.data();
            
            qty = $('input[id="sandwichQty'+ pdata.id + '"]').val();

            $.ajax({
                type: "POST",
                url: SITE_URL + 'sandwich/getCurrentprice',
                data: {'pdata': pdata},
                success: function(data) {
                    console.log(data);
                }
            })

            self.add_sandwich_to_cart(pdata.id, null, null, qty, $(this));
   
        });
        
        $('.sandwich_filter_view').click(function(){              
                $('#sandwich-popup-filter').show(); 
                $(".filterToggle").hide();
                $(".FilterLable").show();
                $("span.filter").show();
                self.body_scroll_ops(false);    
        });
        
        $('.close-button-new').click(function(){
            var anyBoxesChecked = false;
            $('input:checkbox').each(function() {
            if ($(this).is(":checked")) {
                anyBoxesChecked = true;
            }
            });

            if (anyBoxesChecked == false) {
                $('.menu-listing li').show();
                $('.FilterLable').html('Select');
            }             
            $('.sandwich-popup').hide();
            self.body_scroll_ops(true); 
        });
        
        $('.clear-all-filter').click(function(){
             $('.FilterLable').html('Select');       
            $('.menu-listing li').show();
            $('input:checkbox').removeAttr('checked');              
            $('div.FilterLable').html('Select');    
            location.reload();
        });
        
        $('.cancel-button').click(function(){
            $('.menu-listing li').show();
            $('.sandwich-popup').hide();                
        });
        
        $('.apply-filter').click(function(){
            
            $(".filterToggle").hide();
            $(".FilterLable").show();
            $("span.filter").show();
            
            dataObj = self.gallery_search_array();
            $string = JSON.stringify(dataObj);
            
          
            if(dataObj.BREAD.length==0 && dataObj.PROTEIN.length==0 && dataObj.CHEESE.length==0 && dataObj.TOPPINGS.length==0 && dataObj.CONDIMENTS.length==0){
            
                alert("Please select any filter.");
                return false;
            }          
            
             sortBy  = $(".sortby").val();
             var searchTerm = $("#searchTerm").val();
               $.ajax({
                   type: "POST",
                   data: {   'filter' : $string , 'sortBy' : sortBy, "searchTerm":searchTerm  },
                   url: SITE_URL + 'sandwich/filters/',
                   beforeSend: function() {
                          $('.sandwich-popup').hide();
                        $("#ajax-loader").show();
                        $('.pagelogo').fadeIn(500);
                    },
                    complete: function() {
                        $("#ajax-loader").hide();
                        $('.pagelogo').fadeOut(500);
                        
                    },
                   success: function(e) { 
                       $('.menu-listing').html(e);
                       var sandwich_count = $("#sandwich_count").val();
                       $("p.sandwich_count").html(sandwich_count);
                   }
               });
               
           //    $('.sandwich-popup').hide();
        });
        
        
         $('.FilterLable').click(function(){
              $('.FilterLable').show();
              $('.filter').show();
              $('.filterToggle').hide();
              
                var Id = $(this).attr('id');
                $('#fit-'+Id).toggle();
                
              $('#'+Id).toggle();
              $(this).parent().hide();
         });
         
        //Like 
        
             $('.sandwich-popup a:contains(Like)').unbind('click').on('click', function(e) {
           var u_id = $("#hidden_uid").val();
           e.preventDefault();
           // update like count here...
           var sandwich_id;
           sandwich_id = $('.like-sandwich').parent().find('#hidden_sandwich_id');
           sandwich_id = $(sandwich_id).val();



           session = mobile_ha.common.check_session_state();
           if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                  mobile_ha.common.login_popup();
                 } else {
                   mobile_ha.common.facebook_login($(this));
                 }
                return;
            }

           var sandwich_ = $('li[data-id="' + sandwich_id + '"]');
           var like_count;


           if ($(this).text() == 'Liked') return;

           current_like_count = $('.like-sandwich').parent().parent().find('.likes_cnt').text();
           current_like_count = parseInt(current_like_count);
           //updating like count
           like_count = current_like_count + 1;
           $('.like-sandwich').parent().parent().find('.likes_cnt').text(like_count);
          

           $.ajax({
               type: "POST",
               data: {
                   'id': sandwich_id,
                   "uid": SESSION_ID
               },
               url: SITE_URL + 'sandwich/addLike/',
               beforeSend: function() {
                   $("#ajax-loader").show();
               },
               complete: function() {
                   $("#ajax-loader").hide();
               },
               success: function(e) {
                   $(".Like_sandwich").text("Liked");
                   if (e) {
                       $data = JSON.parse(e);
                       id = $data.Data;
                       sandwich_.data('likeid', id);
                       sandwich_.data('likecount', like_count);
                   }
               }
           });

       });
       
       
       //facebook share for menu and shared creations page.
        $('.share-sandwich').unbind('click').on('click', function(e) {
            
             e.preventDefault();
             
            
            
             FB.getLoginStatus(function(response){ 
                 self.fbStatus = response;
                 //console.log(response);
             });
             

      
        if(!navigator.userAgent.match(/(Android)/)){             
             
             if(self.fbStatus.status != "connected"){
                
                 if($('.signout-account').length > 0 ){
                     alert('Please logout the current user and login with facebook to continue..');
                     $('.share-sandwich').removeClass('shareOnGoing');
                     if(window.location.href.indexOf("createsandwich") != -1){ 
                      $('.share-sandwich').attr('style','background:transparent url("../app/images/share-sandwich.jpg") no-repeat scroll 0 0/34px 34px');
                     } else {
                      sloader = $('.share-sandwich').find('img');
                      $(sloader).attr('src',SITE_URL+'app/images/share-img.png');  

                    }
                     return;
                 }
                 
                 
                 if( window.location.href.indexOf("createsandwich") == -1 ) {
                     mainitem = $(this).parent().parent().parent();
                     sanwdid = $(mainitem.find('input[name="hidden_sandwich_id"]')).val();
                     self.setCookie('share_id',sanwdid,1);
                 }
                 
                 self.setCookie('share_active',1,1);
                 
                 self.facbook_auth_window(decodeURIComponent(self.getCookie('fbloginurl')));
                 return;
             } 
            
        } else {
            
            if($('.signout-account').length > 0 && $('.signout-account').attr('href').indexOf('facebook') == -1){
                
             if($('.signout-account').length > 0 ){
                 alert('Please logout the current user and login with facebook to continue..');
                 $('.share-sandwich').removeClass('shareOnGoing');
                 if(window.location.href.indexOf("createsandwich") != -1){ 
                  $('.share-sandwich').attr('style','background:transparent url("../app/images/share-sandwich.jpg") no-repeat scroll 0 0/34px 34px');
                 } else {
                  sloader = $('.share-sandwich').find('img');
                  $(sloader).attr('src',SITE_URL+'app/images/share-img.png');  

                }
                 return;
             }
             
             
             if( window.location.href.indexOf("createsandwich") == -1 ) {
                 mainitem = $(this).parent().parent().parent();
                 sanwdid = $(mainitem.find('input[name="hidden_sandwich_id"]')).val();
                 self.setCookie('share_id',sanwdid,1);
             }
             
             self.setCookie('share_active',1,1);
             
             self.facbook_auth_window(decodeURIComponent(self.getCookie('fbloginurl')));
             return;
            }
            
        }
           
             
            //change selections based on the page
             
             
            if( window.location.href.indexOf("createsandwich") != -1 ) {
                
                is_pub = $('#check2:checked').length;
                sandwichname = $('#namecreation').val();
                sandwichname = sandwichname.toUpperCase();
                $('.sharefbloader').show();
                if ($(this).attr("rel")) {
                    sanwid = $(this).attr("rel");
                } else {
                    sanwid = mobile_ha.sandwichcreator.sync_data_to_db(is_pub, sandwichname, 0,null, 'share');
                }
                
                sandwichDesc = mobile_ha.sandwichcreator.get_desc_from_sandwich();
                $img = $sandwich_img_live_uri + SESSION_ID + '/sandwich_' + sanwid + '_' + SESSION_ID + '.png';
                
                if(sanwid){
                    
                    mobile_ha.sandwichcreator.add_to_sandwich_menu(sanwid);
                    $('.sharefbloader').hide();
                    $(this).attr('rel', sanwid);
                }  

                
            } else {
                
                
                main = $(this).parent().parent().parent();
                main = $(main);
                sandwichname = main.find('.final-list-wrapper h2:first').text();
                sandwichname = sandwichname.toUpperCase();
                sandwichDesc = main.find('.final-list-wrapper .view-popup-scroll').text()
                $img = main.find('.banner img').attr('src');
                sanwid = $(main.find('input[name="hidden_sandwich_id"]')).val();
                
            }
             

            
            

            if (sanwid) {  
                
                 
                self.faceBookFeed(sandwichname,sandwichDesc,sanwid,$img);
                

            }
     
                
          });


        
    }, 
    
    faceBookFeed : function(sandwichname,sandwichDesc,sanwid,$img){
      var self = this;
      
        var feed = {
                method: 'feed',
                name: sandwichname,
                caption: sandwichname,
                description: sandwichDesc,
                link: CMS_URL + 'sandwich/gallery/?galleryItem=' + sanwid,
                picture: $img,
                display: 'popup'
        };

        FB.ui(feed,function(response) {  
            
            self.setCookie('share_active',0,1);  
            self.setCookie('share_id',0,1); 
            $('.share-sandwich').removeClass('shareOnGoing');

            
            
        
        });
        
    },
     
    menu_page_activity: function(){
         
        var self = this;
        
        if(window.location.href.indexOf("menu") != -1 ){ 
        selector = ".sandwich .container .sandwich-wrapper ul li h4, .sandwich .container .sandwich-wrapper ul li .sand-pop"
        //$(selector).attr("")
        } else selector = "a.menu-view-sandwich";
        
        
         
        
        $(document).on("click",selector,function(e){
            var parent;
            e.preventDefault();
            
            self.body_scroll_ops(false);
            
            parent = $(this).parent();
            pdata = parent.data();
            var id= $(this).parent().data("id");
            var name = $(this).parent().data("sandwich-name");
            var desc = $(this).parent().data("sandwich_desc");
            var username = $(this).parent().data("username");
            var likes = $(this).parent().data("likecount");
            var created = $(this).parent().data("date");
            var image = $(this).parent().data("image");
            var menuadds = $(this).parent().data("menuadds");
            var toast = $(this).parent().data("toast");
            var likeid = $(this).parent().data("likeid");
            var amount = "$"+$(this).parent().data("price");
            
            
            $("#view-sandwich").find("div.banner img").attr("src", image);
            $("#view-sandwich").find("h2.name").html(name);
            $("#view-sandwich").find("p.desc").html(desc);
            $("#view-sandwich").find("li.user span").html(username);
            $("#view-sandwich").find("li.created span").html(created);
            $("#view-sandwich").find("li.likes span").html(likes);
            $("#view-sandwich").find("li.menu-adds span").html(menuadds);
            $("#view-sandwich").find(".like-sandwich").attr('data-sand-like-id',id);
            
            if($("#view-sandwich").find(".remove_menu").length > 0){ 
              $("#view-sandwich").find(".remove_menu").attr('rel',id);
            } 
            
            if($("#view-sandwich").find(".save-menu").length > 0){ 
              $("#view-sandwich").find(".save-menu").attr('rel',id);    
            }
            
            $("#view-sandwich").find("#hidden_sandwich_id").val(id);
            $("#view-sandwich").find("h2.amount").text(amount);
            $("#resetQty").val('01');
                    if (likeid)
                    $(".Like_sandwich").text("Liked");
                    else $(".Like_sandwich").text("Like");
                    
            
            if ((toast==1)) {
                $("#view-sandwich").find('.toast').prop('checked', true);
            }
            else{
                $("#view-sandwich").find('.toast').prop('checked', false);
            }
            
            self.showPopupAtScreenPosition("#view-sandwich");
            
        }); 
     
    },
    

    setCookie: function(cname, cvalue, exdays, path) {
        var d = new Date();
        if(!path) path = "/";
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        var path    =  "path="+path; 
        document.cookie = cname + "=" + cvalue + "; " + expires+";  "+path+";"; 
       
    },
    
    getCookie : function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0){ 
                var string = c.substring(name.length, c.length);
                string = decodeURI(string);
                string = string.replace("+", " ");
                return string;
            }
        }
        return "";
    },
  
    check_session_state : function(){
      var result;
      var out;
      $.ajax({async : false , type: "POST", url: SITE_URL + "myaccount/is_session", dataType : 'json' , 
          success: function(e){  
              result = e; 
              out    = e;
         }    
      }); 
      
     
      if(out && out.userAvail == 0){
         alert("User has been removed!");
         if($('.signout-account').attr('href')){ 
         window.location.href = $('.signout-account').attr('href');
        }
        return
      }
      
     return result;
    },
    
    addMenuAction : function() {
        var self = this;
        
        $(document).on("click", ".save-menu", function(){
            
            var toast_sandwich=0;
            
            if($("#check1").prop('checked'))
                toast_sandwich =1;

            if ($(this).text().indexOf('ADDED TO MENU') != -1) return;

            session = mobile_ha.common.check_session_state();
            if (session.session_state == false) {
                mobile_ha.common.login_popup($(this));
                self.body_scroll_ops(true);
                return;
            }

            id = $(this).attr('rel');
            var element = this;
            if ($(".save-menu").hasClass('removemenu') == false) {
                
                $.ajax({
                    type: "POST",
                    data: {
                        'id': id,
                        "uid": SESSION_ID,
                        "toast_sandwich":toast_sandwich
                    },
                    url: SITE_URL + 'sandwich/add_menu_multi/',
                    beforeSend: function() {
                        $("#ajax-loader").show();
                    },
                    complete: function() {
                        $("#ajax-loader").hide();
                    },
                    success: function(e) {
                        setTimeout(function(){                      
                            if(window.location.href.indexOf('friends_menu')>-1){
                                $(".sandwich-popup").hide(); 
                                self.body_scroll_ops(true);
                            }else{
                                $(element).text("ADDED TO MENU");
                                $(".sandwich-popup").hide();
                                self.body_scroll_ops(true); 
                                $(element).text("SAVE TO MY MENU");
                            }                                               
                        },300);
                        
                        self.body_scroll_ops(true);
                    }
                });
            }
            
            self.body_scroll_ops(true);

        });
    },
    removeMenuAction : function() {

        $(document).on("click", ".remove_menu", function(){
            
         session = mobile_ha.common.check_session_state();
         if (session.session_state == false) {
            mobile_ha.common.login_popup($(this));
             return;
         }

            id = $(this).attr('rel');
            $.ajax({
                type: "POST",
                data: {
                    'id': id,
                    "uid": SESSION_ID
                },
                url: SITE_URL + 'sandwich/remove_menu/',
                beforeSend: function() {
                    $("#ajax-loader").show();
                },
                complete: function() {
                    $("#ajax-loader").hide();
                },
                success: function(e) {
                      window.open(SITE_URL + 'menu/', '_self');
                }
            });
         
        });
    },
    addToCartAction : function() {
        var self = this;
        
        $(document).on("click", ".add-to-cart", function(e){
            //var pdata;
             e.preventDefault();
             session = mobile_ha.common.check_session_state();
            if (session.session_state == false) {
              if(session.facebook_only_user == false){
                   self.login_popup($(this));
                 } else {
                   self.facebook_login($(this));
                 }
                return;
            }   
             var toast = 0;
             qty = $('input[name="itemQuty"]').val();
             if( pdata != undefined && pdata ){
                self.add_sandwich_to_cart(pdata.id, null, null, qty,$(this));
             }
             $(".sandwich-popup").hide();
        });
    },
    
    login_popup : function(clkitem){
          self = this;
          self.tempSlector = clkitem;
          if($('#cookie-login-block').length>0){ $('#cookie-login-block').show(); 
          self.body_scroll_ops(false);
          }
          else { $("#login-block").show(); self.body_scroll_ops(false);  }
          if(typeof clkitem !== "undefined")
          {

            var qty = 1;
            if(clkitem.hasClass('save-menu')) return;
            isProd = clkitem.hasClass('common_add_item_cart');
            data   = clkitem.data();

            if(data.sandwich == "product") prdChk = true;  else prdChk = false;
            if(isProd && prdChk){
                type       = 'product'; 
                reorderId = data.sandwich_id;
            }
            else if(clkitem.hasClass('reorder'))
              {
                type       = 'reorder'; 
                reorderId = data.order;
              }
              else{
                type       = 'sandwich';
                if(data.sandwich_id){
                reorderId = data.sandwich_id;
                } else {
                reorderId = $('input[name="hidden_sandwich_id"]').val();
                }
                input=$('input[name="itemQuty"]').val();
                if (input) qty = input;
                else qty = 1;
                 
            }
                uid=self.getCookie("user_id");
                finalDobject = { 'uid' : uid , 'type' : type , 'reorderId' : reorderId, "qty" : qty  }
                self.setCookie("addCartData",JSON.stringify(finalDobject),1);
              }
    },

    facebook_login : function(clkitem){
        self = this;
          if($('#cookie-fblogin-block').length>0){ $('#cookie-fblogin-block').show(); 
          self.body_scroll_ops(false);
          }
          if(typeof clkitem !== "undefined")
          {
            var qty = 1;
            if(clkitem.hasClass('save-menu')) return;
            isProd = clkitem.hasClass('common_add_item_cart');
            data   = clkitem.data();

            if(data.sandwich == "product") prdChk = true;  else prdChk = false;
            if(isProd && prdChk){
                type       = 'product'; 
                reorderId = data.sandwich_id;
            }
            else if(clkitem.hasClass('reorder'))
            {
             type       = 'reorder'; 
             reorderId = data.order;
            }
            else if(clkitem.hasClass('like-sandwich')){
            type       = 'likesandwich';
            reorderId = $('input[name="hidden_sandwich_id"]').val();
            qty=0;
            }
            else{
                type       = 'sandwich';
                if(data.sandwich_id){
                reorderId = data.sandwich_id;
                } else {
                reorderId = $('input[name="hidden_sandwich_id"]').val();
                }
                input=$('input[name="itemQuty"]').val();
                if (input) qty = input;
                else qty = 1;
                 
            }
                uid=self.getCookie("user_id");
                finalDobject = { 'uid' : uid , 'type' : type , 'reorderId' : reorderId, "qty" : qty  }
                self.setCookie("addCartData",JSON.stringify(finalDobject),1);
                
          }
    },
    
    createCookie: function(name,value,days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    },
    
    readCookie: function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    },
    eraseCookie: function(name) {
        var self = this;
        self.createCookie(name,"",-1);
    },
    
    loadProductDescriptorPopup: function(){
        var self = this;
        $(document).ready(function(){                       
            var elementId = self.readCookie("productClicked");
            setTimeout(function(){              
                $("#"+elementId).trigger("click");
                self.eraseCookie("productClicked");
            }, 300)
        });
        
    },
        
    add_sandwich_to_cart: function(id,uid,datatype,Qty,ClickedItem) {
        var self = this;
        session = mobile_ha.common.check_session_state();
       if (session.session_state == false) {
          if(session.facebook_only_user == false){
               mobile_ha.common.login_popup(ClickedItem);
             } else {
               mobile_ha.common.facebook_login(ClickedItem);
             }
       

            if (datatype == 'product') {
                var prod_descriptor = self.product_extras.Data[id];
                if (typeof(prod_descriptor) != "undefined") {
                    var elementId = $(ClickedItem).attr('id');
                    self.createCookie("productClicked", elementId, 1);
                }   
                    
            }   
            
            return;
        }

        var user_id = $("#hidden_uid").val();
        if (uid) {
            user_id = uid;
        }
        var data_type = $("#hidden_sandwich_data").val();
        if (datatype) {
            data_type = datatype
        }
       
        var toast = 0;

        if($(ClickedItem).hasClass("quickAddToCart"))
        {
            parent = $(ClickedItem).parent().parent().parent();
            pmaindata = parent.data();
            toast = pmaindata.toast;
        }
        else
        {
            if ($(".menucheck").is(':checked')) 
            {
                toast = 1;
            }
        }

        var qty;
        if (Qty) qty = Qty;
        else qty = 1;
        
        if (datatype == 'product') {

            var prod_descriptor = self.product_extras.Data[id];
            
            if (typeof(prod_descriptor) == "undefined") {


            } else {
                
                var amount = $(ClickedItem).prev().val();

                $("#product-descriptor ul.from-holder").html("");

                for (i = 0; i < prod_descriptor.length; i++) {
                    var prod_desc = prod_descriptor[i];
                    var options = "";
                    for (j = 0; j < prod_desc.extra.length; j++) {
                        var option = prod_desc.extra[j];
                        options += "<option value='" + option.id + "'>" + option.name + "</option>";
                    }

                    
                    $("#product-descriptor h2").html(prod_desc.desc);
                    $("#product-descriptor h2").attr('style','padding-top:40px;')
                    var content = '<li><div class="date_time_wrapper filter" style="background: none repeat scroll 0% 0% transparent;"><select name="extra_id" class="extra_id listextra">' + options + '</select></div> </li><input type="hidden" name="descriptor_id" value="' + prod_desc.id + '"/>';
                    content += '<input type="hidden" name="descriptor_id" value="' + prod_desc.id + '"/>';
                    
                    
                    $("#product-descriptor ul.from-holder").append(content);
                }

                var hidden_fields = '<input type="hidden" name="itemid" value="' + id + '" />';
                hidden_fields += '<input type="hidden" name="data_type" value="' + data_type + '" />';
                hidden_fields += '<input type="hidden" name="user_id" value="' + user_id + '" />';
                hidden_fields += '<input type="hidden" name="toast" value="' + toast + '" />';


                $("#product-descriptor form .hidden_fields").html(hidden_fields);
                $("#product-descriptor input.add_desc").val('ADD TO CART');
                $("#product-descriptor h2.amount").text('$'+amount);
                self.showPopupAtScreenPosition('#product-descriptor');
                return false;
            }

        }
        
        $.ajax({
            type: "POST",
            data: {
                'itemid': id,
                "data_type": data_type,
                "user_id": user_id,
                "qty": qty,
                "toast": toast
            },
            async : false,
            url: SITE_URL + 'sandwich/add_to_cart',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) {
                
                self.added_to_cart_transition(e);
            }
        });
    },
    
    add_toast_sandwich_to_cart: function(id, toast, uid, datatype, Qty, ClickedItem) {
        var self = this;
        session = self.check_session_state();
       if (session.session_state == false) {
          if(session.facebook_only_user == false){
               self.login_popup(ClickedItem);
             } else {
               self.facebook_login(ClickedItem);
             }
            return;
        }   

        var user_id = $("#hidden_uid").val();
        if (uid) {
            user_id = uid;
        }
        var data_type = $("#hidden_sandwich_data").val();
        if (datatype) {
            data_type = datatype
        }

        var qty;
        if (Qty) qty = Qty;
        else qty = 1;

        $.ajax({
            type: "POST",
            data: {
                'itemid': id,
                "data_type": data_type,
                "user_id": user_id,
                "qty": qty,
                "toast": toast
            },
            url: SITE_URL + 'sandwich/add_to_cart',
            beforeSend: function() {
               $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
                $("#sandwich-view").hide();
            },
            success: function(e) {
                self.added_to_cart_transition(e);
                 $('.shopping-cart span').text(e);
                $("#sandwich-view").hide();
            }
        });


    },
    
   tmpTransAnim : 0, 
   added_to_cart_transition: function(e){
       var self = this;
        var url = window.location.href;   
        
         if($(".sandwich-popup:visible").length > 0){ 
             $(".close-button").trigger('click');
         }
        
        if(url.indexOf('createsandwich') > -1){         
            return;
        }   
        
        var divTop = "0px"
        
        //Filter is fixed to so need to put top
        if(url.indexOf('gallery') > -1){
            divTop = "60px"
        }       
            
        var currentCount = $('.cart-top h1').text();
        $('.cart-top h1').text(e);
        var difference = parseInt(e) - parseInt(currentCount);              
        var scrollHeight = $(window).scrollTop();                
        if(scrollHeight < 0){ // > 100
            $("div.dynamic_updates_fixed div.left-block").html("<p>"+difference+" item(s) added to cart.</p>" );
            
            
            setTimeout(function(){ $("div.dynamic_updates_fixed").slideDown("slow"); }, 100);
                    
         
            setTimeout(function(){ $("div.dynamic_updates_fixed").slideUp(250,function(){ self.tmpTransAnim = 1; });  }, 4000);
        }else{
            $("div.dynamic_updates div.left-block").html("<p>"+difference+" item(s) added to cart.</p>" );
            $("div.dynamic_updates").css("top", divTop );
            setTimeout(function(){ $("div.dynamic_updates").slideDown("slow"); }, 100);
            $('.shopping-cart span').text(e);
            
            setTimeout(function(){ $("div.dynamic_updates").slideUp(250,function(){ self.tmpTransAnim = 1; });  }, 4000);   
        }
        
        $("input.text-box.qty").val("01");
    },
    
    cartChangeFlag :  0,
    
    qtySpinnerAction: function(e){
        var self = this;
        $(document).on("click", ".popup_spinner .left_spinner, .left_spinner_checkout", function()
        {
            
            var sandwich_id = $(this).parent().find(".sandwich_id").val();
            var order_item_id = $(this).parent().find(".order_item_id").val();
            var data_type = $(this).parent().find(".data_type").val();  
                    
            input = $(this).next();
                span = $(this).next().next();
            value = parseInt($(input).val());
            if($(this).hasClass("sandwich_gallery_view_popup")){
                value = parseInt($("#resetQty").val());
            }
            if(isNaN(value) == true) value = 1;
            if(value > 1){  value = value - 1;  }
            
            if($(this).hasClass("left_spinner_checkout")){
                if(self.cartChangeFlag == 0){
                    self.cartChangeFlag= 1;
                    self.updateCartContent(this, sandwich_id, value, order_item_id, data_type, "left");
                    self.allBarsNotRed();
                }else{
                    self.allBarsNotRed();
                    return; 
                }   
            }
            
            if(value<10) txt = "0"+value; else txt = value;
            $(input).val(txt);
                if($(this).hasClass("sandwich_gallery_view_popup")){
                    $("#resetQty").val(txt);
                }
                else
                    {
                    $("#resetQty").val(txt);
                    }
            var zipPopUpFlag = 1;
            var zip_hidden = $('#zip_hidden_selected').val();
                if(zip_hidden)
                {
            self.checkZip(zip_hidden,zipPopUpFlag);
            self.allBarsNotRed();
                }

             setTimeout(function(){
                        self.setCookie_forcalendar();  
                    }, 1000);               
            
        
         });
         
         //salad-popup
         $(".salad-listing, .view_sandwich").unbind('click').on('click', function(e) 
        {
            var parent;
            parent = $(this).parent().parent().parent(); 
            if($(this).hasClass("salad-listing"))
            {
               parent = $(this).parent().parent().parent().parent().parent();
            }           
            pdata = parent.data();
            var des = pdata.description;
            $("input[name='itemQuatity']").val("01");
            if (!pdata.product_image) 
            {
                image = $(".salad_item_image").find('img:nth-child(1)').hide();
            }
            else
            {
                image = $(".salad_item_image").find('img:nth-child(1)');
                $(image).attr({
                    'src':pdata.image_path+pdata.product_image
                    /*'width': '350',
                    'height': '194'*/
                });
            }
            session = mobile_ha.common.check_session_state();
              if(session.session_state == false){ 
                if(session.facebook_only_user == false){
                  mobile_ha.common.login_popup($(this));
                }
                else {
                  mobile_ha.common.facebook_login($(this));
                }
                  return;  
              }


           /* $("#salad_popup").find('#hidden_sandwich_id').val(pdata.id);
            $("#salad_popup").find('#hidden_sandwich_price').val(pdata.product_price);*/
            $("#salad_popup").find('h4').text(pdata.product_name);
            $("#salad_popup").find('h2').text('$'+pdata.product_price);
            $($("#salad_popup p")[0]).html($.parseHTML(des));
            if(pdata.add_modifier != 1 || pdata.add_modifier == 0){
                $("#salad_popup").find('.add_modifier').hide();
            }else{
                $("#salad_popup").find('.add_modifier').show();
                $(".salad-item-wrap").find('div').remove();
                var chklen = pdata.modifier_options.length;
                var html = '';
                $.each( pdata.modifier_options, function( key, value ) {
                    html += '<div class="salad-checkbox">';
                    html += '<input id="check_p_'+key+'" type="checkbox" class="menucheck savetousertoastnew" name="checkoptions" value="'+value.option+','+value.price+'">';
                    html += '<label for="check_p_'+key+'">'+value.option + ' ($ ' + value.price + ')</label>';
                    html += '</div>';   
                });

                $(".salad-item-wrap").append(html);
            }
            if(pdata.spcl_instr != 1 || pdata.spcl_instr == 0){
                $("#salad_popup").find('.spcl_instruction').hide();
            }else{
                $("#salad_popup").find('.spcl_instruction').show();
            }
            
            $("#salad_popup").find('.modifier_desc').html(pdata.modifier_desc);
            $("#salad_popup").show();
            /*$('.popup_spinner .left_spinner').unbind('click').on('click', function() 
            {
                input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            $('.popup_spinner .right_spinner').unbind('click').on('click', function() 
            {
                input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });*/
            
            $(".add-salad-to-cart").unbind('click').on('click', function()
            {
                var atLeastOneIsChecked = $('input:checkbox').is(':checked');
                var chkboxlen = $(":checkbox:checked").length;
                //console.log($('input[name="checkoptions"]:checked').serialize());
                var clickedItems = [];
                i = 0;
                $('input[name="checkoptions"]:checked').each(function() {
                   clickedItems[i++] = this.value;
                });
        
                if(pdata.add_modifier == 1 && pdata.modifier_isoptional == 'no' && atLeastOneIsChecked==false){
                    alert('Please select one '+pdata.modifier_desc);
                }else if(pdata.add_modifier == 1 && pdata.modifier_is_single == 'yes' && chkboxlen > 1){
                    alert('You can select only one item');
                }else{
                    qty = $('input[name="itemQuatity"]').val();
                    $(".hidden-values").find('input').remove();
                    var hidden_fields = '<input type="hidden" name="itemid" value="' + pdata.id + '" />';
                    hidden_fields += '<input type="hidden" name="data_type" value="' + pdata.product + '" />';
                    hidden_fields += '<input type="hidden" name="user_id" value="' + pdata.uid + '" />';
                    hidden_fields += '<input type="hidden" name="qty" value="' + qty + '" />';
                    hidden_fields += '<input type="hidden" name="clickedItems" value="' + clickedItems + '" />';
                    hidden_fields += '<input type="hidden" name="toast" value="0" />';
                    $('.hidden-values').append(hidden_fields);

                    var data = $('#popup-form').serialize();
                    console.log(data);
                    $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'sandwich/add_to_cart',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete: function() {
                            $("#ajax-loader").hide();
                            $("#salad_popup").hide();
                        },
                        success: function(e) {
                            self.added_to_cart_transition(e);
                            $('.shopping-cart span').text(e);
                            $("#product-descriptor").hide();
                            $("#salad_popup").hide();

                        }
                    });
                    //self.add_sandwich_to_cart(pdata.id, pdata.uid, pdata.product, qty, $(this));
                }
                
            })
        });
         
        $(document).on("click", ".popup_spinner .right_spinner, .right_spinner_checkout", function()
        {
            
            var sandwich_id = $(this).parent().find(".sandwich_id").val();
            var order_item_id = $(this).parent().find(".order_item_id").val();
            var data_type = $(this).parent().find(".data_type").val();  
            
            input = $(this).prev();
            value = parseInt($(input).val()); 
            
            if($(this).hasClass("sandwich_gallery_view_popup"))
            {
                value = parseInt($("#resetQty").val());
            }

            if(isNaN(value) == true) value =  parseInt($(input).text());
            if(isNaN(value) == true) value = 1;
            if(value < 999){  value = value + 1;  }

            if($(this).hasClass("right_spinner_checkout"))
            {
                
                if(self.cartChangeFlag == 0)
                {                   
                    self.cartChangeFlag= 1;
                    self.updateCartContent(this, sandwich_id, value, order_item_id, data_type, "right");
                    self.allBarsNotRed();
                }else{
                    self.allBarsNotRed();
                    return ;    
                }   
            }
            
            if(value<9) txt = "0"+value; else txt = value;
            
            $(input).val(txt);
            if($(this).hasClass("sandwich_gallery_view_popup"))
            {
                $("#resetQty").val(txt);
            }
            else
            {
                $("#resetQty").val(txt);
            }

            var zipPopUpFlag = 1;
            var zip_hidden = $('#zip_hidden_selected').val();
            if(zip_hidden)
            {
                self.checkZip(zip_hidden,zipPopUpFlag);
            
                self.allBarsNotRed();
            }
            setTimeout(function()
            {
                self.setCookie_forcalendar();
            }, 1000);                
            
        });

        $('.leftSpinner').unbind('click').on('click', function() 
            {
                input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            $('.rightSpinner').unbind('click').on('click', function() 
            {
                input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });
    },
     add_standard_menu_item_cart : function(){
            var self = this;
              $(document).on("click", ".common_add_item_cart", function(e){
                  e.preventDefault(); 
                  $data = $(this).data();
                  ob = $(this);
                  parent = ob.parent();
                  prev   = $(parent).prev();
                  input = $(prev).find('input[name="itemQuty"]');
                  qtyInp = $(input).val();
                  if (qtyInp) qty = qtyInp;
                  else qty = 1;
                  
                    parent = ob.parent();
                    li = $(parent).parent();                    
                    liData = $(li).data();
                  
                if($data.sandwich == 'product')
                    self.add_sandwich_to_cart($data.sandwich_id,$data.uid,$data.sandwich,qty,$(this));
                else if (liData.toast == 0) 
                    self.add_sandwich_to_cart($data.sandwich_id,$data.uid,$data.sandwich,qty,$(this));
                else
                    self.add_toast_sandwich_to_cart($data.sandwich_id, liData.toast, $data.uid, $data.sandwich, qty, $(this));  
                            
              });
    },
    
    //reorder myaccount 
    myaccount_reorder : function(){
        var self = this;
        $("a.reorder-button, .reorder-item").unbind('click').on('click',function(e){
              session = mobile_ha.common.check_session_state();
              if(session.session_state == false){ 
                if(session.facebook_only_user == false){
                  mobile_ha.common.login_popup($(this));
                }
                else {
                  mobile_ha.common.facebook_login($(this));
                }
                  return;

              } else { 
            
            var order_id = $(this).data("order");
            var reorder_from = $(this).data("from");
                pdataArray = [];
                if(reorder_from =='order_history'){
                    $(this).parent().next('div').find(".item_details").each(function(){
                        pdataArray.push($(this).data());
                    });
                }else if(reorder_from =='home_page'){
                    $(this).parent().parent().find(".item_details").each(function(){
                        pdataArray.push($(this).data());
                    });
                }
                //console.log('data',pdataArray);return;
                if(pdataArray != 'undefined'){
                    $.each(pdataArray,function(index, value){
                        $.ajax({
                            type: "POST",
                            url: SITE_URL + 'sandwich/getCurrentprice',
                            data: {'pdata': value},
                            success: function(data) {
                                console.log(data);
                            }
                        })
                    });    
                }
            $.ajax({
                type: "POST",
                data: { 'order_id' : order_id },
                dataType : 'json',
                url: SITE_URL+'myaccount/myOrderReorder/',
                beforeSend: function() {
                    $("#ajax-loader").show();
                },
                complete:function() {
                    $("#ajax-loader").hide();
                },
                success: function(e) {
                    window.location = SITE_URL+"checkout/index/";   // /choosedeliveryorpickup
                }
            }); 
            
            
              } 
        }); 
    },
    
    checkoutPageAction : function(){
        
        var self = this;
        
              $(document).on("click", "#select_address_div", function(){  
                 $('.place-order-checkout').removeClass('disabled');
                var order_type = $("#order-type").val();
                order_type =  1; //DM #615 //parseInt(order_type);
                
                if(order_type == 1 || order_type == 0 ) 
                {
                    $("#address_list").show();
                    self.body_scroll_ops(false);
                }
                else{   
                    alert("Please select Delivery or Pickup.");
                    $('.place-order-checkout').addClass('disabled');
                    return false;
                }                   
                
              });
              
              $(document).on("click", "#select_delivery_or_pickup_div", function(){  
                  $("#select_delivery_or_pickup").show();
                  $('#select_address_div').addClass("first-time");
                  self.body_scroll_ops(false);
              });
              
              $(document).on("click", "#enter_payment_div", function(){  
                    
                  $("#credit_card_list").show();
                  self.delete_credit_card();
                  self.body_scroll_ops(false);
              });
              
              $(document).on("click", ".add-credit-card", function(){  
                    
                  $("#new_credit_card").show();
                  self.body_scroll_ops(false);
              });
             
              $(document).on("click", "#add-address", function(){  
                    
                  $("#add_new_address").show();
                  var name = $('input[name="recipientName"]').val();
                    $('input[name="recipient"]').val(name);
                  self.body_scroll_ops(false);
              });
              
              $(document).on("click", "#add_new_address .close-button", function(){  
                    
                  $("#add_new_address").find("input[type=text], textarea").val("");
                 
              });
              $(".enter_zip,.enter_delivery").on('keypress', function(e) {

                    if(e.keyCode==13)
                    {

                    $('.save_address').click();

                }
              });
            $(document).on("click", ".save_address", function(){  
                    
                    var form = $(this).parent("ul").parent("form.add_address");

                    var recipient = $(form).find("input[name=recipient]").val();
                    var company = $(form).find("input[name=company]").val();
                    var address1 = $(form).find("input[name=address1]").val();
                    var address2 = $(form).find("input[name=address2]").val();
                    var zip = $(form).find("input[name=zip]").val();
                    var phone1 = $(form).find("input[name=phone1]").val();
                    var phone2 = $(form).find("input[name=phone2]").val();
                    var phone3 = $(form).find("input[name=phone3]").val();
                    var crossStrt = $(form).find("input[name=cross_streets]").val();
                    
                    if( !recipient || recipient.length < 1){
                        alert("Recipient name can't be empty.");
                        $('.place-order-checkout').addClass('disabled');
                        return false;
                    }   
                    
                       if(  !crossStrt || crossStrt.length < 1 ){
                            alert("Cross Streets can't be empty.");
                            return false;
                        }
                    
                
                    if( !phone1 || phone1.length < 1 || phone1.length > 3 || !phone2 || phone2.length < 1 || phone2.length > 3 || !phone3 || phone3.length < 1 || phone3.length > 4 ){
                        alert("Phone number should be valid.");
                        $('.place-order-checkout').addClass('disabled');
                        return false;
                    }   
                    

                    if(!zip){
                        alert("Zip Code required! ");
                        $('.place-order-checkout').addClass('disabled');
                        return false;
                    }else {
                        if ($.inArray(zip, self.zipcodes) == -1) {
                            console.log( "4" );
                            alert("Sorry, delivery is not available in this area! Barney Brown currently delivers in Manhattan from Battery Park to 100th St.");
                            $('.place-order-checkout').addClass('disabled');
                            return false;
                        }
                    }
                    $('.place-order-checkout').removeClass('disabled');
                    var data = form.serialize();
                    $.ajax({
                        type: "POST",
                        data:data,
                        url: SITE_URL+'myaccount/savingAddress',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete:function() {
                            $("#ajax-loader").hide();
                        },
                        success: function(res){ 
                             
                            $(form).find("input[type=text], textarea").val("");
                            $(".popup-wrapper").hide();
                            $('.sandwich-popup').hide();
                            $(".popup-wrapper").hide();
                            $.ajax({
                                type: "POST",
                                data:  {
                                    "zip": zip
                                },
                                url: SITE_URL + 'checkout/checkZip',
                                success: function(data){
                                    data = JSON.parse(data);

                                    //console.log("data", data);
                                     
                                    $('.hundred-min-red-bar').html('');
                                     if(data.min_order==100 && subtotal<100){
                                         if(self.zipPopUpFlag==0){
                                             $('#warningcheckout').show();
                                         }
                                         
                                         var subtotal = $("input[name=hidden_sub_total]").val();
                                         var remain = parseFloat(100.00-subtotal).toFixed(2);
                                        
                                        var msg = '<p>$100.00 SUBTOTAL MINIMUM.'+remain+'  TO GO!</p>';
                                          $('.hundred-min-red-bar').html(msg);
                                         $('#warningOk').on('click',function(e){
                                            e.preventDefault();
                                                $('#warningcheckout').hide();
                                            $('.error_msg').hide();
                                            self.body_scroll_ops(true);
                                            $("#address_list .address-select:last").trigger("click");
                                            if(res){
                                                self.changeCheckoutoption("deliverychooseaddress");
                                                
                                                setTimeout(function(){
                                                    $("#address_list .address-select:last").trigger("click")                                    
                                                }, 300);
                                                
                                            }
                                        });
                                     }
                                     else if(data.min_order<=10 && subtotal<10){
                                         var remain = parseFloat(10.00-subtotal).toFixed(2);
                                         var msg = '<p>$10.00 SUBTOTAL MINIMUM.'+remain+'  TO GO!</p>';
                                             $('.hundred-min-red-bar').html(msg);
                                             self.allBarsNotRed(); 

                                       }


                                     else{
                                         $("#address_list .address-select:last").trigger("click");
                                        if(res){
                                            self.changeCheckoutoption("deliverychooseaddress");
                                            
                                            setTimeout(function(){
                                                                                                $('.place-order-checkout').removeClass('disabled');
                                                $("#address_list .address-select:last").trigger("click")                                    
                                            }, 300);
                                            
                                        }
                                     }

                                     if(data.delivery_fee != ''){

                                        $.session.remove('deliveryFee');
                                        $.session.set("deliveryFee",data.delivery_fee);

                                        $('.tax-total-wrapper .delivery_fee_in_total').remove();
                                        $('.tax-total-wrapper .total:nth-child(1) p:nth-child(2)').after('<p class="delivery_fee_in_total">DELIVERY</p><input type="hidden" name="hidden_delivery_fee" id="hidden_delivery_fee" value="'+ data.delivery_fee +'">');
                                        $('.tax-total-wrapper .total:nth-child(2) p:nth-child(2)').after('<p class="delivery_fee_in_total">$'+ parseFloat(data.delivery_fee).toFixed(2) +'</p><input type="hidden" name="hidden_delivery_fee" id="hidden_delivery_fee" value="'+ data.delivery_fee +'">');
                                        
                                       // var grand_tot = $("input[name=hidden_grand_total]").val();

                                        var discount =0;
                                        var subtotal = $("#hidden_sub_total").val();
                                         var tax = $("#hidden_tax").val();
                                        
                                        if($("#hidden_discount_amount").val()){
                                         discount = $("#hidden_discount_amount").val();
                                        }
                                        var tip = $("#hidden_tip").val(); 
                                        
                                        var grand_tot = parseFloat(parseFloat(subtotal) + parseFloat(tax) + parseFloat(discount) + parseFloat(tip));

                                        var tot_with_delivery_fee = parseFloat(parseFloat(grand_tot) + parseFloat(data.delivery_fee)).toFixed(2);
                                        
                                        $("input[name=hidden_grand_total]").val(tot_with_delivery_fee);
                                        $(".grand-total h2, .amount-done-wrapper h3").text('$' + tot_with_delivery_fee);
                                    }
                                     
                                }
                            });
                            
                            self.checkTimingsRealtedConditions( new Date() ); 
                            self.check_time_and_change_if_needed(  );
                            
                        }
                    });
            });
            
            $(document).on("click", "#select_delivery_or_pickup .order-type", function(){     
                $('.place-order-checkout').removeClass('disabled'); 
                var order_type = 1; //$(this).data("order-type");       // DM fix for #615
                var text = $(this).text();
                
                self.checkoutSlectionData.orderType = order_type;
                self.saveTempUserSelection();
                
                $('.pop-delivery-to-manhatten-only').remove();
                
                $("#order-type").val(order_type);
                $("#select_delivery_or_pickup_div h2").text(text);
                $(".sandwich-popup").hide();    
                
                var order_type = $("#order-type").val();
                            
                if(order_type == 1){
                    self.changeCheckoutoption("deliverychooseaddress");
                    $('#select_delivery_or_pickup_div').removeClass("first-time");
                    $("#select_address_div h2").html("CHOOSE A LOCATION");
                    console.log( "5" );
                    $('.place-order-checkout').addClass('disabled');
                    $("#address_list div.add-address-wrapper h2").text("Select Address");
                    $("#address_list div.add-address-wrapper a.add-address").after('<p class="pop-delivery-to-manhatten-only" style="padding-top:90px;font:600 11px open sans;color:#dab37f !important;">Barney Brown delivers in Manhattan from Battery Park to 100th Street.</p>');
                    $("#address_list #add-address").show();
                    
                    self.updatePickupOrDeliveryInCart(order_type);
                }
                else if(order_type == 0){
                    $('#select_delivery_or_pickup_div').removeClass("first-time");
                    $("#select_address_div h2").html("CHOOSE A LOCATION");
                    self.changeCheckoutoption("pickupstorelocation");
                    console.log( "6" );
                    $('.place-order-checkout').addClass('disabled');
                    $("#address_list div.add-address-wrapper h2").text("Select Store Location");
                    $("#address_list #add-address").hide();
                    
                    self.updatePickupOrDeliveryInCart(order_type);
                }
                else{
                    $('#select_delivery_or_pickup_div').addClass("first-time");
                    $("#select_address_div h2").html("CHOOSE A LOCATION");
                    alert("Please select Order Type first.");
                    
                    console.log( "7" );
                    $('.place-order-checkout').addClass('disabled');
                    return;
                }
                            
            });
            
            $(document).on("click", ".select-address-button",function(){    
                var address_id = $(this).data("address-id");
                $("#address-id").val(address_id);
                var address_text = $(this).parent().parent().find("h2").text(); 
                address_text = address_text.length >= 30 ? address_text.substring(0, 30)+'...' : address_text;          
                $("#select_address_div h2").text(address_text);
                $(".sandwich-popup").hide();    
                
                 setTimeout(function(){
                                    if(self.specific_date_exist != undefined && self.specific_date_exist != '' )
                                    {
                                        var selected_date_time = self.specific_date_selected +' '+self.specific_time_selected_tm;
                                        var selected_date_time = self.specific_date_selected +' '+self.specific_time_selected_tm;
                                        $("#enter_datetime_seelction").removeClass("first-time");
                                        $("#enter_datetime_seelction h2").text(selected_date_time);
                                    }
                                   
                                }, 2500);
                                
                var order_type = $("#order-type").val();
                
                
                
                $("#select_address_div").removeClass("first-time");
                
                var objData = {};
        
                //If Delivery then chk the store id with ZIP
                if(order_type == 1){
                    var zip_hidden = $(this).parent().parent().find("input.zip_hidden").val();
                    $('#zip_hidden_selected').val(zip_hidden);
                    
                     
                    // checking store closed or not.
                    if( $.inArray(zip_hidden,self.zipcodes) == -1 ){
                        alert("Zip code invalid or store closed");
                        return false;
                    }
                    
                    var zipPopUpFlag=0;
                    self.checkZip(zip_hidden,zipPopUpFlag);
                    var storeid = self.getStore_id_using_zip(zip_hidden);
                    self.checkoutSlectionData.addressId = address_id;
                    self.setCookie("addressId",address_id,1);
                    self.checkoutSlectionData.storeId = storeid;
                    self.saveTempUserSelection();
                    
                    $('input[name="delivery_store_id"]').val(storeid);
                    $("#hidden_store_id").val(storeid);
                    $("#hidden_default_store_id").val(storeid);
                                        
                    objData = {"store_id": storeid, "address_id" : address_id };
                    
                }else{
                    
                    objData = {"store_id": address_id, "address_id" : 0 };
                    
                    $('input[name="delivery_store_id"]').val(address_id);
                    $("#hidden_store_id").val(address_id);
                    $("#hidden_default_store_id").val(address_id);
                    
                    self.checkoutSlectionData.addressId = address_id;
                    self.setCookie("addressId",address_id,1);
                    self.checkoutSlectionData.storeId = address_id;
                    self.saveTempUserSelection();
                }
                
                
                
                //After new address select remove disable state if there
                $(".switch_date").removeAttr("disabled");
                self.specificdayCheck(objData.store_id);                        
                self.checkTimingsRealtedConditions( ); 
                self.check_time_and_change_if_needed(  );           
                self.storeTimingsEvents(order_type, objData);
            });
            
            $(document).on("click", ".select-card-button",function(){       
                var card_id = $(this).data("card-id");
                $("#card-id").val(card_id);
                var card_text = $(this).parent().parent().find("h2").text();
                $("#enter_payment_div h2").text(card_text);             
                $("#enter_payment_div").removeClass("first-time");              
                $(".sandwich-popup").hide();    
                
                self.checkoutSlectionData.cardId = card_id;
                self.setCookie("cardId",card_id,1);
                self.saveTempUserSelection();
                self.allBarsNotRed();
                self.storeTimingsEvents(1, '');
                
            });
            
            $(document).on("click", ".save-credit-card", function(){
                
                
                jsTime = self.getCurrentTimJs();
                currentMonth = parseInt(jsTime.getMonth()) + 1;
                currentYear  = parseInt(jsTime.getFullYear());
                
                card_name      = $('input[name="cardNickname"]').val();
                card_no        = $('input[name="cardNo"]').val();
                card_cvv       = $('input[name="cardCvv"]').val();
                card_type      = $('select[name="cardType"]').val();
                card_zip       = $('input[name="cardZip"]').val();
                expiry_month   = $('select[name="cardMonth"]').val();
                expiry_year    = $('select[name="cardYear"]').val();
                is_save        = $('input[name="save_billing"]').prop("checked");
                $num_cvv       = 1;
                
                address1 = $('#new_credit_card input[name="address1"]').val();
                        street = $('input[name="street"]').val();
                selectedMonth  = parseInt(expiry_month);
                selectedYear   = parseInt(expiry_year);
                
                if(is_save){
                    is_save=1;
                }
                else{
                    is_save=0;
                }

                
                if (isNaN(card_no) == true || (card_no.length < 13 || card_no.length > 16 || card_no.length == 14 )) {
                    alert("Invalid Card number!");
                    console.log( "8" );
                    $('.place-order-checkout').addClass('disabled');
                    return false;
                }
                    else if ( isNaN(card_cvv) == true ) {
                    alert("Invalid card sec.no");
                    return false;
                }
                
                    else {


                        if(card_type == "American Express"  ){
                            
                            if( card_cvv.length != 4 ) { 
                                alert("Invalid card sec.no");
                                return false;
                            }
                            
                        }  else if(card_type != "American Express"  ){
                            
                            if( card_cvv.length != 3 ) { 
                                alert("Invalid card sec.no");
                                return false; 
                            }

                        }

                    }
                
                if(selectedYear < currentYear)
                {
                    alert("The expiration date is invalid");
                    $('.place-order-checkout').addClass('disabled');
                    return false;
                }
                else if(selectedYear == currentYear && selectedMonth  <= currentMonth){
                
                    alert("The expiration date is invalid");
                    $('.place-order-checkout').addClass('disabled');
                    return false;
                }        
                else {
                        $('.place-order-checkout').removeClass('disabled');
                        $.ajax({
                        type: "POST",
                        data: {
                            'card_name': '',
                            'card_no': card_no,
                            'card_type': card_type,
                            'expiry_month': expiry_month,
                            'expiry_year': expiry_year,
                            'card_zip': card_zip,
                            'cvv' : card_cvv,
                            'uid': SESSION_ID,
                            'crd_id': $('.billingDetails').attr('rel'),
                            'display_in_menu' : is_save,
                            'address1':'',
                                        'street':'',
                                        //'cross_streets':cross_streets,
                        },
                        async : false,
                        dataType : 'json',
                        url: SITE_URL+'checkout/saveBillinginfo/',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete:function() {
                            $("#ajax-loader").hide(); 
                        },
                        success: function(e) {  
                            if( e.response == undefined || e.response.status_code != '200' )
                            {
                                return false;
                            }
                            $('input[name="_card_number_auth"]').val(card_no);
                            $('input[name="_expiry_month_auth"]').val(expiry_month);
                            $('input[name="_expiry_year_auth"]').val(expiry_year);
                            
                            $("#card-id").val(e.data);
                            self.checkoutSlectionData.cardId = e.data;


                            self.saveTempUserSelection();                           
                            if(card_type == "American Express") card_type = "AMEX";
                            var card_text = card_type.toUpperCase()+" ENDING WITH "+card_no.substr(-4);                                         
                            
                            $("#enter_payment_div h2").text(card_text);             
                            $(".sandwich-popup").hide();
                            
                            var newHtml =  '<div class="address"><h2>'+card_text+'</h2><div class="left"><p>    XXXX-XXXX-XXXX-'+card_no.substr(-4)+'<br>'+card_type+'<br><br></p></div><div class="right"><a rel="'+e.data+'" class="remove-card">Remove</a><a class="view-button select-card-button" data-card-id="'+e.data+'" href="#">select</a></div></div>';
                            if(is_save==1) $("#credit_card_list div.addresslist").append(newHtml);
                            
                            $("#enter_payment_div").removeClass("first-time");
                            self.delete_credit_card();
                            self.storeTimingsEvents(1,'');
                        }                                       
                              
                    });  
                    
                }       
                
            });
            
            $(document).on("click", "#enter_datetime_seelction", function(){            
                $("#select_date_time").show();
            });
            
            $(document).on("click", ".apply-time-selection", function(){    
                check = self.checkBreadType();
                
                console.log( "CLICKED ON SOURCE" );

                if(check == 'error') { 
                    return;
                }
                
                var now_or_specific = $('input.switch_date:checked').val();
                
                if(now_or_specific == "now"){
                    
                    $("#enter_datetime_seelction").removeClass("first-time");
                    self.checkoutSlectionData.isNotTimeNow = 0;
                    self.saveTempUserSelection();
                    self.clear_the_specific_date_time(  );
                    console.log( "Clearing to now..." );
                    self.set_checkout_front_to_say_now(  );
                    console.log( "DONE" );


                } else if(now_or_specific == "specific"){
                    console.log( "SET USER PICK" );
                    self.setTimepicked( 1 );
                    self.setSpecificDateTimeFromElements(  );
                    //self.check_time_and_change_if_needed(  );

                    var date_text = $(".delivery_date").val();
                    var time = $("div.timechange select option:selected").text();
                    var time_val = $("div.timechange select option:selected").val();
                    self.specific_time_selected = time_val;
                    self.specific_time_selected_tm = time;
                    date_text = new Date(date_text);
                    date_text = date_text.toDateString();
                    date_text = date_text+" "+time; 
                    self.checkoutSlectionData.isNotTimeNow = 1;
                    self.checkoutSlectionData.specificTime = date_text;
                    $("#enter_datetime_seelction").removeClass("first-time");
                    $("#enter_datetime_seelction h2").text(date_text);
                }
                var zipPopUpFlag = 0;
                var zip_hidden = $('#zip_hidden_selected').val();
                //self.checkZip(zip_hidden,zipPopUpFlag);
                $(".sandwich-popup").hide();
                storeId = $('input[name="delivery_store_id"]').val();
                self.specificdayCheck(storeId);
                self.allBarsNotRed();
                self.storeTimingsEvents(1, '');

                //self.check_time_and_change_if_needed(  );
                
            });
                    
            $(document).on("click", ".switch_date", function(){     
                var elementid = $(this).attr("id"); 
                console.log( self.time_state_array.nowDisabled );
                if(elementid == "radio1"){
                    if( self.time_state_array.nowDisabled == true )
                    {
                        // dm change to fix ticket 613
                        self.show_date_radio(  );

                      
                        var tmp = document.createElement("DIV");
                        tmp.innerHTML =self.time_state_array.msg;
                        //alert( tmp.innerText  );
                        // SHOW MESSAGE IF YOU WANT!
                        return;
                    }
                    else
                    {
                        self.show_now_radio(  );
                        $(".date_time_wrapper").hide();
                        return;
                    }
                }
                else{ 
                    
                    //call only if store is selected.
                    var delvry = $("#order-type").val(); 
                    if (delvry == "1") {
                        deliveryORPickup = "delivery";
                        var deliveryAddressId = $("#address-id").val();
                    } else {
                        deliveryORPickup = "pickup";
                        var pickupStoreId = $("#hidden_store_id").val();
                    }
                    if(deliveryORPickup = "pickup" && pickupStoreId)
                        self.onSpecificDateCallavailTime();
                    
                    $(".date_time_wrapper").show(); 
                }
                
                
            });
            
            
        $(document).on("change", "#select_tip_amount", function(){ 
            if($.session.get("deliveryFee"))
            {
                //alert($.session.get("deliveryFee"));
                var delivery_fee_amount = $.session.get("deliveryFee");
            }
            else
            {
                var delivery_fee_amount = 0;
            }

            var discount =0;
            var subtotal = $("#hidden_sub_total").val();
             var tax = $("#hidden_tax").val();
            
            if($("#hidden_discount_amount").val()){
             discount = $("#hidden_discount_amount").val();
            }
            var tip = $(this).val();    
            $("#hidden_tip").val(tip); 
            tip = parseFloat(tip); 
            
            var net_tot = tip+parseFloat(subtotal)-parseFloat(discount)+ parseFloat(tax) + parseFloat(delivery_fee_amount);
            $(".grand-total h2").html("$"+net_tot.toFixed(2));  
            $(".amount-done-wrapper h3").html("$"+net_tot.toFixed(2));
            
               
            net_tot = net_tot.toFixed(2);
            $('#hidden_grand_total').val(net_tot);
            var tipamount = tip.toFixed(2);
            $("div.tax-total-wrapper div.total p.p-tips").html('$'+tipamount);    
        });
            
            self.look_for_date_change();
            self.doCheckoutEvents();
            self.applyDiscountEvents();
    },
    
    checkZip:function(zip_hidden,flag){
                $("#select_address_div").removeClass("first-time");

        var self = this;
        var address_id = $("#address-id").val();
        var order_type = $("#order-type").val();
        $.ajax({
            type: "POST",
            data:  {
                "zip": zip_hidden
            },
            url: SITE_URL + 'checkout/checkZip',
            success: function(data){

                data = JSON.parse(data);

                var subtotal = $("input[name=hidden_sub_total]").val();
                var html = '';
                    $('.hundred-min-red-bar').html('');
                   
                 if(data.min_order ==100 && subtotal<100){
                     if(flag==0){
                         $('#warningcheckout').show();
                     }
                     
                     var remain = parseFloat(100.00-subtotal).toFixed(2);
                     var msg = '<p>$100.00 SUBTOTAL MINIMUM.'+remain+'  TO GO!</p>';
                     
                         $('.hundred-min-red-bar').html(msg);
                         self.allBarsNotRed(); 
                         $('#warningOk').on('click',function(e){
                            e.preventDefault();
                                $('#warningcheckout').hide();
                            $('.error_msg').hide();
                            self.body_scroll_ops(true);
                            
                        });
                 }
                 else if(data.min_order <=10 && subtotal<10){
                  
                     var remain = parseFloat(10.00-subtotal).toFixed(2);
                     var msg = '<p>$10.00 SUBTOTAL MINIMUM. $'+remain+'  TO GO!</p>';
                     
                         $('.hundred-min-red-bar').html(msg);
                         self.allBarsNotRed(); 
                        

                   }


                 else{
                     
                        if( self.validation_blocked == "" ||  self.validation_blocked == 0 ){
                         setTimeout(function(){
                             $('.place-order-checkout').removeClass('disabled');
                         }, 300);
                     }
                }


                    if(flag!=0){
                 var storeid = self.getStore_id_using_zip(zip_hidden);
                 objData = {"store_id": storeid, "address_id" : address_id };
                 self.specificdayCheck(storeid);
                 self.storeTimingsEvents(order_type, objData);
             }

             if(data.delivery_fee != ''){

                $.session.remove('deliveryFee');
                $.session.set("deliveryFee",data.delivery_fee);

                $('.tax-total-wrapper .delivery_fee_in_total').remove();
                $('.tax-total-wrapper .total:nth-child(1) p:nth-child(2)').after('<p class="delivery_fee_in_total">DELIVERY</p><input type="hidden" name="hidden_delivery_fee" id="hidden_delivery_fee" value="'+ data.delivery_fee +'">');
                $('.tax-total-wrapper .total:nth-child(2) p:nth-child(2)').after('<p class="delivery_fee_in_total">$'+ parseFloat(data.delivery_fee).toFixed(2) +'</p><input type="hidden" name="hidden_delivery_fee" id="hidden_delivery_fee" value="'+ data.delivery_fee +'">');
                
                //var grand_tot = $("input[name=hidden_grand_total]").val();

                var discount =0;
                var subtotal = $("#hidden_sub_total").val();
                 var tax = $("#hidden_tax").val();
                
                if($("#hidden_discount_amount").val()){
                 discount = $("#hidden_discount_amount").val();
                }
                var tip = $("#hidden_tip").val(); 
                
                var grand_tot = parseFloat(parseFloat(subtotal) + parseFloat(tax) + parseFloat(discount) + parseFloat(tip));
                
                var tot_with_delivery_fee = parseFloat(parseFloat(grand_tot) + parseFloat(data.delivery_fee)).toFixed(2);
                
                $("input[name=hidden_grand_total]").val(tot_with_delivery_fee);
               $(".grand-total h2, .amount-done-wrapper h3").text('$' + tot_with_delivery_fee);
            }
          }
        });
        
    },
    
    specificdayCheck: function(objData)
    {
    },
    //saves user selections in a temp sessions
    saveTempUserSelection : function(){
        var self =  this;
         data = JSON.stringify(self.checkoutSlectionData);
         $.ajax({
             type: "POST",
             data:{'data':data},
             url: SITE_URL+'checkout/saveTempUserSelectionData',
             success:function() {
                 
                self.allBarsNotRed(); 
                 
            } });
    },
    
    getTempUserSelection: function(){
        var self = this;
         $.ajax({
             type: "POST",
             url: SITE_URL+'checkout/getTempUserSelectionData',
             async:false,
             success:function(e) {
                self.checkoutSlectionData = JSON.parse(e);
            } });
        
    },
    
    getCurrentTimJs : function(){
        var self = this;
        servtime  = self.getServerTime();
        servtime  = servtime.split(",")
        servtime[1] = parseInt(servtime[1]-1);
        return new Date(servtime[0],servtime[1],servtime[2],servtime[3],servtime[4],servtime[5] );
    },
    
    onSpecificDateCallavailTime : function(){
        return;
        var self = this;
        currentDate  = self.getCurrentTimJs();
     
        
        
        
        datepickerDate = new Date( $( '.delivery_date' ).datepicker( "getDate" ) );
        
        var current_day = self.getAlphaDay(datepickerDate.getDay())+", "+self.getAlphaMonth(datepickerDate.getMonth())+" "+datepickerDate.getDate()+", "+datepickerDate.getFullYear();
        
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

                        
        dateSel          = days[currentDate.getDay()];
        
        $("#selected_day").val(selectedDay);
        var add_id = $("#hidden_store_id").val();
        if(add_id=="")
        {
            add_id = $("#hidden_default_store_id").val();
        }
        
        
        var delvry = -1;
        var order_type = $("#order-type").val();
        if(order_type){
            delvry  = 1;
        } else {
            delvry  = 0;
        }
        
     
         var isCurrentDay = 1;
         var selectedDay  = $("#selected_day").val();
         var currentTime;
         currentTime = currentDate.getHours() + ":" + currentDate.getMinutes();
        
        $obj = { "isDelivery": delvry , "current_time" : currentTime , "current_day" : current_day , "store_id" : add_id , "selectedDay" : dateSel , "isCurrentDay" : isCurrentDay  };
        
     
        
        
         self.getTimeAvilableSlots($obj);
         self.limit_select_times_by_date(  );
        
    },
    
    updatePickupOrDeliveryInCart : function(deliveryOrPickup){
        
        var data = {'deliveryOrPickup': deliveryOrPickup};
        
        var last_tip = $("#select_tip_amount").val();       
         var grandTotal= $("#hidden_grand_total").val();
        $.ajax({
            type: "POST",
            data:data,
            url: SITE_URL+'checkout/updatePickupOrDelivery',
            beforeSend: function() {
                
            },
            complete:function() {
                
            },
            success: function(data){                
                data = JSON.parse(data);
                if(data.status == true){                    
                    $(".check-out-left").html(data.message);
                    $.ajax({
                        type: "POST",
                        data:data,
                        url: SITE_URL+'checkout/getCartItemQtyCount/',
                        success: function(e){
                            $("div.cart-top h1").text(e);
                            var subtotal = $("#hidden_sub_total").val();
                            var tip = $("#select_tip_amount").val();                                
                        }
                    });
                    
                    //cart item list total value to Bottom Total 
                    $("div.amount-done-wrapper h3").text($("div.grand-total h2").text());       

                }
            }
        });      
        
    },
        
    getStore_id_using_zip : function(zip){
        var self = this;
        var store_id;
        $.ajax({
                type: "POST",
                data: { 'zip' : zip }, 
                url: SITE_URL + 'myaccount/getStoreFomZip/',
                async:false,
                dataType : 'json',
                success: function(data) { 
                    if(data[0]){
                        store_id = data[0].store_id;
                    } else {  store_id = 0; }
              }
         });
         
        return store_id;
    },

 clearUserTimeTemp : function(  )
    {
//        console.log( "_____ CLEAR TIME TEMP" );
        this.setTimepicked( 0 );
        this.setSpecificDateTime( 0 );
    },

    clearUserTimeValidated : function(  )
    {
//        console.log( "_____ CLEAR TIME VALIDATED" );
        this.setUserTimeValidated( 0 );
    },

    setUserTimeValidated: function( user_date )
    {
        console.log( "_____ SET USER VALIDATED TIME: " );
        //console.log(  user_date );
        if( user_date != 0  )
            date_pack = JSON.stringify(  user_date );
        else
            date_pack = 0;
        this.setCookie( "user_date_validated", date_pack , 1);
    },

    getUserTimeValidated: function(  )
    {
        if( this.getCookie("user_date_validated") ){
            return JSON.parse( this.getCookie("user_date_validated") );
        }
        else
            return 0;
    },



    setSpecificDateTimeFromElements: function(  )
    {
         console.trace(  );
        //console.log( "______SETTING DATE TIME FROM ELEMENETS" );
        this.setTimepicked( 1 );
        console.log( "-!" + $( ".delivery_date" ).val(  ) );
        console.log( "-!" + $('.timedropdown').val(  ) );
        this.newDate =  $(".delivery_date").val();
        this.newTime = $('.timedropdown').val(  );
    },

    setSpecificDateTime: function( newDate, newTime )
    {
          if( newDate == 0 ){
            this.newDate = 0 ;
            this.newTime = 0;
        }
        else
           this.setSpecificDateTimeFromElements(  );
       
       
    },

    getSpecificDate: function(  )
    {
       if( this.newDate )
            return this.newDate;
        else
            return 0;
    },

    getSpecificTime: function(  )
    {
         if( this.newTime )
            return this.newTime;
        else
            return 0;
    },

    setOpen: function ( date )
    {
        date_save = JSON.stringify( date );
        console.log( "___Saving - OPEN - " + date_save );
        this.open_date_json = date_save;
    },

    getOpen: function(  )
    {
        date_json = this.open_date_json;
        if( this.is_empty( date_json ) == 0)
            return JSON.parse( date_json );
        else
            return 0;
    },

    setClose: function ( date )
    {
        date_save = JSON.stringify( date );
        console.log( "___Saving - CLOSE - " + date_save );
        this.close_date_json = date_save;
    },

    getClose: function(  )
    {
        date_json = this.close_date_json;
        if( this.is_empty( date_json ) == 0)
            return JSON.parse( date_json );
        else
            return 0;
    },


    clear_the_specific_date_time : function(  )
    {
        var self = this;
        console.log( "%c___Clearing the specific time etc", "color:red" );
        /*self.setCookie('specific_date_exist', '', 1 );
        self.setCookie('specific_date_selected', '', 1 );
        self.setCookie('specific_time_exist', '', 1 );
        self.setCookie('specific_time_selected', '', 1 );
*/
        self.specific_date_exist   = '';
        self.specific_date_selected   = '';
        self.specific_time_exist   = '';
        self.specific_time_selected   = '';

    },


    set_checkout_front_to_say_now : function(  )
    {
        console.log( "CLEARING FRONT PAGE THING" );
        $( "#front_page_delivery_time" ).val( 'NOW' );
        $( "h2#front_page_delivery_time" ).val( 'NOW' );
        $("#enter_datetime_seelction").removeClass("first-time");
        $("div#enter_datetime_seelction h2").html("NOW");
    },

    setCurrentDayHour: function ( date )
    {
        return;
        date_save = JSON.stringify( date );
        console.log( "___Saving - CURRENT - " + date_save );
        this._current_day_hour = date_save;
    },

    getCurrentDayHour: function(  )
    {
        date = this.getCurrentDateFromServer(  );
        return date;
    },

    format_date: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var date_string = self.getAlphaDay(date.getDay())+", "+self.getAlphaMonth(date.getMonth())+" "+date.getDate()+", "+date.getFullYear();
        return date_string;
    },

    format_time: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        display_hours = date.getHours();
    
        var time_string = this.pad(  date.getHours() , 2) + ":" + this.pad( date.getMinutes(), 2);
        return time_string;
    },

    military_hours_to_standard:function( display_hours )
    {
        //console.log( "MILITARY FIX" );
        display_hours = parseInt(  display_hours ) ;
        if( display_hours > 12 )
            display_hours = display_hours - 12;
        return display_hours;
    },


    format_date_time: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date_string = this.format_date( d );
        time_string = this.format_time( d );

        return date_string + " " + time_string;
    },

    compare_times: function( d1, d2 )
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date1 = new Date( d1 );
        date2 = new Date( d2 );

        date_comp_1 = new Date( date1 );
        date_comp_2 = new Date( date1 );

        date_comp_1.setSeconds( 0 );
        date_comp_1.setMilliseconds( 0 );

        date_comp_2.setSeconds( 0 );
        date_comp_2.setMilliseconds( 0 );

        date_comp_1.setHours( date1.getHours(  ) );
        date_comp_1.setMinutes( date1.getMinutes(  ) );

        date_comp_2.setHours( date2.getHours(  ) );
        date_comp_2.setMinutes( date2.getMinutes(  ) );

        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

        return result;
    },

    blank_time: function( d )
    {
        d = new Date(  d );
        d.setHours( 0 );
        d.setMinutes( 0 );
        d.setSeconds( 0 );
        d.setMilliseconds( 0 );
        return d;
    },

    compare_dates: function( d1, d2 )
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2  = this.blank_time( date_comp_2 );


        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

        return result;
    },

    set_date_time : function( date, time  )
    {
        var day = new Date( date );
        var time_split = {};
        console.log( "TIME" + time );
        if( typeof( time ) == 'object' )
        {
            time_split[0] = time.getHours(  );
            time_split[1] = time.getMinutes(  );
        }
        else //string
        {
            time_split = time.split(/\:|\-/g);
        }
        
        day.setHours(time_split[0]);
        day.setMinutes(time_split[1]);
        return day;
    },


    pad : function (num, size) 
    {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    },


    is_empty: function( thing )
    {
        if( thing == 'undefined'    ||
            thing == undefined      ||
            thing == null           ||
            thing == ''             ||
            thing === false )
            return 1;
        else
            return 0;
    },
    date_plus_1 : function( d )
    {
        var date = new Date( d );
        date.setDate( date.getDate() + 1 );
        return date;
    },

    get_next_valid_date : function(  )
    {
        //console.log( "%cGetting Valid Date", "color:teal" );
        date_open       = this.getOpen(  );
        date_closed     = this.getClose(  );
        date_current    = this.getCurrentDayHour(  );

        vs_open_time = this.compare_times( date_current, date_open );
        vs_closed_time = this.compare_times( date_current, date_closed );

        next_valid = date_current;
        if( vs_open_time == "<" )
            next_valid = date_open;
        if( vs_closed_time == ">" )
        {
            next_valid = this.date_plus_1( date_open );
        }
        return next_valid;

    },
    // DM Changed cuz throwing error IE 3-15
    show_date_radio : function(  )
    {
        $("#radio1").prop( "checked", false );
        $("#radio2").prop( "checked", true );
        $(".date_time_wrapper").show(); 
    },
    // DM Changed cuz throwing error IE 3-15

    show_now_radio : function(  )
    {
        console.log( "Checked" );
        $("#radio2").prop( "checked", false );
        $("#radio1").prop( "checked", true );
        $(".date_time_wrapper").hide(); 
        console.log( "done" );
    },

    validate_or_clear_date_cookie : function(  )
    {   
        console.log( "%cVALIDATING", "color:teal" );
        cookie_date = new Date( this.specific_date_selected );
        time = this.specific_time_selected;
        cookie_date_final = this.set_date_time( cookie_date, time );
        this.checkTimingsRealtedConditions( cookie_date_final );


        date_cookie     = cookie_date_final;
        date_open       = this.getOpen(  );
        date_closed     = this.getClose(  );
        date_current    = this.getCurrentDayHour(  );


        // first check if this is today or a future day...
        vs_current_date = this.compare_dates( date_cookie, date_current );
        vs_open_time = this.compare_times( date_cookie, date_open );
        vs_closed_time = this.compare_times( date_cookie, date_closed );

        if( vs_current_date == "<" )        // This is before today, so not works. 
            return 0;
        if( vs_open_time == "<" || vs_open_time == "=")
            return 0;
        if( vs_closed_time == ">" || vs_closed_time == "=" )
            return 0;

        var vs_current_time = '';
        if( vs_current_date == "=" )
        {
            vs_current_time = this.compare_times( date_cookie, date_current );
            if( vs_current_time == "<" ){   // same day but before current time, so invalid. 
                return 0;}
        }

        if( vs_current_date == "?" || vs_open_time == "?" || vs_closed_time == "?" || vs_current_time == "?")
        {
            console.warn( "%cERROR in date validation", "color:teal; bakcground:black"  );
            return 0;
        }

        // If we get here, we are ok.
        return 1;
    },
    tester: function( x )
    {
        console.log( x );
    },
    disableSpecificDates: function( date )
    {
       // console.log( "TEST - " + date );
        self = this;
        if(  self.specific_day  == "" )
        {
            return [1];
        }

        var bad_date = new Date(  mobile_ha.common.specific_day );
    
        var m_bad = bad_date.getMonth();
         var d_bad = bad_date.getDate();
         var y_bad = bad_date.getFullYear();
         

        var m = date.getMonth();
         var d = date.getDate();
         var y = date.getFullYear();
         
         // First convert the date in to the mm-dd-yyyy format 
         // Take note that we will increment the month count by 1 
         var currentdate = (m + 1) + '-' + d + '-' + y ;
         var bad_date_formatted =  (m_bad + 1) + '-' + d_bad + '-' + y_bad ;
          // We will now check if the date belongs to disableddates array 
         if( currentdate == bad_date_formatted  )
         {
            return [false];
         }
         else
         {
            return [true];
         }
    }, 
    look_for_date_change : function()
    {
        var self = this;        
        $(".delivery_date").datepicker({
            setDate: new Date(),
            minDate: 0,
            dateFormat: 'D, M d, yy',
            onSelect: function(selectedDate) 
            {
                self.getTimeAvilableSlots(  );
                self.limit_select_times_by_date(  );
            }
        });

        $('.delivery_date').datepicker('option', 'onSelect', function(selectedDate) 
        { 
            self.specific_date_exist  = 1;
            self.specific_date_selected  = selectedDate;
            self.specific_time_selected  = '12:00';
            self.specific_time_selected_tm  = '12:00 PM';

            $(".apply-time-selection").css({"pointer-events": "none","opacity":"0.5","cursor":"default"});
            $(".apply-time-selection").html("Processing...");
            
            currentdate      =  new Date().setHours(0, 0, 0, 0);
            var storedate    =  new Date(selectedDate);
            if(currentdate   == storedate.setHours(0, 0, 0, 0)){ $("#selected_date").val(1); }
            else { $("#selected_date").val(0); }
                                                    
            var isCurrentDay =  $("#selected_date").val();
            var dayNames     =  new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
            var selectedDay  =  dayNames[storedate.getDay()];
            
            
            var options      =  self.getTimings(selectedDate);
            dateSel          = selectedDate;
                    
            $("#selected_day").val(selectedDay);
            var add_id = $("#hidden_store_id").val();
            if(add_id=="")
            {
                add_id=$("#hidden_default_store_id").val();
            }
            
            var delvry = -1;
            var order_type = $("#order-type").val();
            if(order_type)
            {
                delvry  = 1;
            } 
            else 
            {
                delvry  = 0;
            }
                    
                 
            var isCurrentDay = $("#selected_date").val();
            var selectedDay  = $("#selected_day").val();
           
            ctimeDt  = self.getCurrentTimJs();
             

            if(  (storedate.getDate() == ctimeDt.getDate()) &&  (storedate.getMonth() == ctimeDt.getMonth()) && (storedate.getFullYear() == ctimeDt.getFullYear()) ) {
                 currentTime = ctimeDt.getHours() + ":" + ctimeDt.getMinutes(); 
            } else {
                currentTime =  $('.timechange select').val();
            }
                    
            self.checkTimingsRealtedConditions(selectedDate);
            setTimeout(function(){
                $(".apply-time-selection").css({"pointer-events": "all","opacity":"1","cursor":"pointer"});
                $(".apply-time-selection").html("SELECT");

            }, 500);    
                
            self.getTimeAvilableSlots(  );
            self.limit_select_times_by_date(  );
                     
                    
        });
            
            $(".delivery_date").attr("readonly","readonly");
            $('.date_wrapper').click(function()
            {
                self.limit_select_times_by_date(  );
                $('.delivery_date').datepicker('show');
            });
            
    },
    
    
    getTimings: function(selectedDate){
        if(selectedDate)
            var date_selected = new Date(selectedDate);
        else
            var date_selected = new Date();
            
        var date_today = new Date();
                    
                    if(date_today.getDate() == date_selected.getDate()){
                        var cur_hour = new Date().getHours();
                        var cur_min = new Date().getMinutes();
                        var options = "";
                        
                        for(temp_hr = cur_hour; temp_hr<=21; temp_hr++){
                                                        
                                var h_in  = temp_hr;
                                var am_pm = " AM";
                                if(h_in > 12){
                                     h_in = temp_hr - 12;
                                     am_pm = " PM";
                                }
                                                                                        
                                if(cur_min+15 > 59) cur_min = (cur_min+15)-59;
                                
                                if(cur_min > 0 && cur_min <=15){
                                 cur_min = 30;
                                } else if(cur_min > 15 && cur_min <=30){ 
                                    cur_min = 45; 
                                } else if(cur_min >= 45 && cur_min <=59) cur_min = 0;
                                
                                for(temp_min = cur_min; temp_min <= 59; temp_min = temp_min + 15){
                                    options  = options +"<option value='"+temp_hr+":"+temp_min+"'>"+h_in+":"+temp_min+am_pm+"</option>";
                                }
                                        
                        }
                    }else if(date_today.getDate() < date_selected.getDate()){
                                                            
                        var options = "";
                        var open_time = 10;                 
                        for(temp_hr = open_time; temp_hr<=21; temp_hr++){   
                                                                        
                                var h_in  = temp_hr;                                            
                                var am_pm = " AM";
                                if(h_in > 12){
                                     h_in = temp_hr - 12;
                                     am_pm = " PM";
                                }    
                                
                                for(temp_min = 15; temp_min <= 59; temp_min = temp_min + 15){
                                    options = options +"<option value='"+temp_hr+":"+temp_min+"'>"+h_in+":"+temp_min+am_pm+"</option>";
                                }                                   
                        }
                    }
                        
            return options;         
    },
    
    getTimeAvilableSlots  : function(oBj)
    {
        time_list = this.get_all_possible_times(  );
        
         $(".timechange").html("");
         current_value = $(".timechange").val();
        $(".timechange").html(
            '<select class="timedropdown">' +
            time_list +
            "</select>"
            );
        this.set_time_html( current_value );

        return;

        var self = this;
         $.ajax({
                type: "POST",
                data:{ "isDelivery" : oBj.isDelivery , "current_day" : oBj.current_day , "current_time" : oBj.current_time , "store_id" : oBj.store_id, "selectedDay" : oBj.selectedDay , "isCurrentDay" : oBj.isCurrentDay },
                url: SITE_URL+'checkout/changeTimeOnStoreSelection',
                success: function(e){
                    
                    if(e=="Closed"){
                        
                        e="<div class='shopclosemsg' style='padding: 5px 0px 5px 10px'>Close</div>";
                    }
                    $(".timechange").html("");
                    $(".timechange").html(e);
                    
                    date_string = new Date($('input[name="updatedDate"]').val());
                
                    $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', date_string));    
                    
                    
                    
                }
            });
         
    },
    
    selected_billing : function(){
    
            var self = this;
              
                card_name      = $('input[name="cardNickname"]').val();
                card_no        = $('input[name="cardNo"]').val();
                card_cvv       = $('input[name="cardCvv"]').val();
                card_type      = $('select[name="cardType"]').val();
                card_zip       = $('input[name="cardZip"]').val();
                expiry_month   = $('select[name="cardMonth"]').val();
                expiry_year    = $('select[name="cardYear"]').val();
                is_save        = $('input[name="save_billing"]').prop("checked");
            
                var billing = $('.billingDetails').attr('rel');
                self.set_billing_values({
                    'id' : billing,
                    'card_type' : card_type,
                    'card_no' : card_no,
                    'card_cvv' : card_cvv,
                    'card_name' : card_name,
                    'expiry_month' : expiry_month,
                    'expiry_year': expiry_year,
                    'card_zip' : card_zip
                });     
                
                $(".popup-wrapper").hide(); 
                $('.delivery-address').hide();
                
                $('.billingDetails, .edit-billing').show();
                if($('.billinglist option').length>=2){ $('.change-billing').show(); }
                 self.edit_change_billing(billing);
                $('.billinglist').parent().hide();
                    
    },
    
    
    
    changeCheckoutoption : function(checkoutoption)
    {
        $.ajax({
        type: "POST",
        url: SITE_URL+"checkout/"+checkoutoption,
        beforeSend: function() {
            
        },
        complete:function() {
            
        }
        })
        .done(function( msg ) {         
            $("#address_list div.addresslist").html(msg);
        });
    },
    
    updateCartContent :  function(obj, sandwich_id, count, order_item_id, data_type, whichArrow){
        
        var self = this;
         
            var order_type = $("#order-type").val(); // check id pickup 0 or delivery 1
            var last_tip = $("#select_tip_amount").val();           
            
            var qty = count;
            var data_id = sandwich_id;          
                    
            var data = {"qty" : qty, "data_id" : data_id, "data_type" : data_type, "order_item_id": order_item_id, "deliveryOrPickup" : order_type, "last_tip": last_tip, "whichArrow" : whichArrow};
            
            $.ajax({
                type: "POST",
                data:data,
                async:false,
                url: SITE_URL+'checkout/updateCartItems/',
                beforeSend: function() {
                    
                },
                complete:function() {
                    
                },
                success: function(e){           
                    
                    if(!e) return;
                    
                    var data = JSON.parse(e);
                    
                    
                    if(data.status){
                        $(".check-out-left").html(data.message);
                        
                        var subtotal = $("#hidden_sub_total").val();
                        var tip = $("#select_tip_amount").val();
                        
                        
                        $.ajax({
                            type: "POST",
                            data:data,
                            url: SITE_URL+'checkout/getCartItemQtyCount/',
                            success: function(e){
                                $("div.cart-top h1").text(e);
                                                                
                                //applying discount if any.
                                self.applyDiscount();   
                                self.on_chekckout_quantity_value_change();
                            }
                        });
                        
                        //cart item list total value to Bottom Total 
                        $("div.amount-done-wrapper h3").text($("div.grand-total h2").text());
                        
                        
                        
                        self.cartChangeFlag= 0;                         
                        self.allBarsNotRed();
                    }else{
                        
                        alert("Session has been expired. Please login again.");
                        window.location = SITE_URL;
                    }
                }
            }); 

        
    
    },
    
    sandwichToastAction :  function(){  
        
        $(document).on("change", ".savetousertoast", function(){  
         session = mobile_ha.common.check_session_state();
         if (session.session_state == false) {
             mobile_ha.common.login_popup();
             return;
         }


         var item_id = $("#hidden_sandwich_id").val();
         var sandwich_ = $('li[data-id="' + item_id + '"]');
         var toast = 0;
         
         
         
         if ($('.savetousertoast').is(':checked')) {
             var toast = 1;
         }

         if (toast == 1) {
             $(sandwich_).find('.toasted').css('display', 'block');
         } else $(sandwich_).find('.toasted').css('display', 'none');

         $.ajax({
             type: "POST",
             data: {
                 "toast": toast,
                 "item_id": item_id
             },
             url: SITE_URL + 'sandwich/updateToastmenu',
             dataType: "json",
             success: function(e) {
                 if (e) {
                     sandwich_.data('toast', e.Data);
                 }
             }

         });
        
     });
    
    },
    
    
    gallery_search_array: function(){
        // ajax filter instead normal jQuery filter 
        $data = {'BREAD':[],'PROTEIN':[],'CHEESE':[],'TOPPINGS':[],'CONDIMENTS':[]};
         
        $('.mobile-popup-filter input[type="checkbox"]:checked').each(function(e){ 
          $selectionsKey = $(this).attr('name');    
          $selectionsKey = $selectionsKey.toUpperCase();
          $value         = $(this).val();
          $value         = $value.trim();
          $data[$selectionsKey].push($value);
        });
         
        return $data;
    },
    
    
    gallery_search: function() {
        var Array = {};

        Array['BREAD'] = [];
        Array['PROTEIN'] = [];
        Array['CHEESE'] = [];
        Array['TOPPINGS'] = [];
        Array['CONDIMENTS'] = [];

        var $protien, $condiments, $toppings, $cheese;


        var self = this;
        var $sel;
        var parent;
       $('.mobile-popup-filter input[type="checkbox"]').on('click', function() {
           
  var check = $('.mobile-popup-filter input[type="checkbox"]:checked').length;

           
            $('.menu-listing li').show();
            $txt = $(this).next().text();
            $txt = $txt.trim();
            parent = $(this).parent().parent().parent();
            $key = $(parent).prev('p').text()
            $key = $key.trim();
            $key = $key.toUpperCase();
    
            
            switch ($key) {

                case 'BREAD':

                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['BREAD']) == -1) {
                            Array['BREAD'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['BREAD']) != -1) {
                            Array['BREAD'].splice($.inArray($txt, Array['BREAD']), 1);
                        }
                    }
                    if(Array['BREAD'].length != 0)
                        $("#bread").html(Array['BREAD']);
                    else
                         $("#bread").html("Select");    

                    break;

                case 'PROTEIN':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['PROTEIN']) == -1) {
                            Array['PROTEIN'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['PROTEIN']) != -1) {
                            Array['PROTEIN'].splice($.inArray($txt, Array['PROTEIN']), 1);
                        }
                    }

                    if(Array['PROTEIN'].length != 0)
                        $("#protein").html(Array['PROTEIN']);
                    else
                         $("#protein").html("Select");  

                    break;

                case 'CHEESE':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['CHEESE']) == -1) {
                            Array['CHEESE'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['CHEESE']) != -1) {
                            Array['CHEESE'].splice($.inArray($txt, Array['CHEESE']), 1);
                        }
                    }

                    if(Array['CHEESE'].length != 0)
                        $("#cheese").html(Array['CHEESE']);
                    else
                         $("#cheese").html("Select");   

                    break;

                case 'TOPPINGS':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['TOPPINGS']) == -1) {
                            Array['TOPPINGS'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['TOPPINGS']) != -1) {
                            Array['TOPPINGS'].splice($.inArray($txt, Array['TOPPINGS']), 1);
                        }
                    }

                    if(Array['TOPPINGS'].length != 0)
                        $("#toppings").html(Array['TOPPINGS']);
                    else
                         $("#toppings").html("Select"); 

                    break;

                case 'CONDIMENTS':
                    if ($(this).prop('checked') == true) {

                        if ($.inArray($txt, Array['CONDIMENTS']) == -1) {
                            Array['CONDIMENTS'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['CONDIMENTS']) != -1) {
                            Array['CONDIMENTS'].splice($.inArray($txt, Array['CONDIMENTS']), 1);
                        }
                    }
                    if(Array['CONDIMENTS'].length != 0)
                        $("#condiments").html(Array['CONDIMENTS']);
                    else
                         $("#condiments").html("Select");   

                    break;

            }

            $condiments = '';
            $cheese = '';
            $protien = '';
            $toppings = '';
            
            $condiments_f = '';
            $cheese_f = '';
            $protien_f = '';
            $toppings_f = '';
            $bread_f  = '';

            if (Array['PROTEIN'].length > 0) {          
                
                Array['PROTEIN'].forEach(function(dat, count) {
                    dat = dat.replace("##", " ");
                    if(dat == "No protein") 
                    {
                        $protien += '[data-protien=""]';
                    }
                    
                    else {
                     $protien += '[data-protien~="' + dat + '"]';
                    if (Array['PROTEIN'].length > 1) {
                        
                        var $end = "";
                        if (count < Array['PROTEIN'].length - 1) {
                            $end = ', '
                        }
                        
                        $protien_f += dat+$end;
                        $('#protein').html($protien_f); 
                    }
                    else
                    {
                        $('#protein').html(dat); 
                    }
                    
                    }
                });
            } 
           

            if (Array['CHEESE'].length > 0) {
                Array['CHEESE'].forEach(function(dat, count) {
                    dat = dat.replace("##", " ");
                    if(dat == "No cheese")
                    { 
                        $cheese += '[data-cheese=""]'; 
                        
                    }
                    else 
                    {
                        $cheese += '[data-cheese~="' + dat + '"]';
                        if (Array['CHEESE'].length > 1) {
                            
                            var $end = "";
                            if (count < Array['CHEESE'].length - 1) {
                                $end = ', '
                            }
                            
                            $cheese_f += dat+$end;
                            $('#cheese').html($cheese_f); 
                        }
                        else
                        {
                            $('#cheese').html(dat); 
                        }
                     }
                });
            }


            if (Array['TOPPINGS'].length > 0) {
                Array['TOPPINGS'].forEach(function(dat, count) {
                    dat = dat.replace("##", " ");
                    if(dat == "No toppings")
                    {
                         $toppings += '[data-topping=""]';
                    }
                    else
                    {
                         $toppings += '[data-topping~="' + dat + '"]';
                         
                        if (Array['TOPPINGS'].length > 1) {
                            
                            var $end = "";
                            if (count < Array['TOPPINGS'].length - 1) {
                                $end = ', '
                            }
                            
                            $toppings_f += dat+$end;
                            $('#toppings').html($toppings_f); 
                        }
                        else
                        {
                            $('#toppings').html(dat); 
                        }

                    }    
                });
            }

            if (Array['CONDIMENTS'].length > 0) {
                Array['CONDIMENTS'].forEach(function(dat, count) {
                    dat = dat.replace("##", " ");
                     if(dat == "No condiments") 
                     {
                        $condiments += '[data-cond=""]';
                     }
                    else
                    {
                         $condiments += '[data-cond~="' + dat + '"]';
                        if (Array['CONDIMENTS'].length > 1) {
                            
                            var $end = "";
                            if (count < Array['CONDIMENTS'].length - 1) {
                                $end = ', '
                            }
                            
                            $condiments_f += dat+$end;
                            $('#condiments').html($condiments_f); 
                        }
                        else
                        {
                            $('#condiments').html(dat); 
                        }

                    }
                });
            }

            $data = $protien + $cheese + $toppings + $condiments;
            $li = '';

            var $bread = "";
            if (Array['BREAD'].length > 0) {
                var $bread = " ";
                if (Array['BREAD'].length > 0) {
                    Array['BREAD'].forEach(function(dat, count) {
                        dat = dat.replace("##", " ");
                        var $end = "";
                        if (count < Array['BREAD'].length - 1) {
                            $end = ', '
                        }
                            $li += '.menu-listing li[data-bread~="' + dat + '"]' + $data + $end;                            
                            $bread+= dat+$end;
                    });
                    
                    $('#bread').html($bread); 
                }


            } else {

                $li = '.menu-listing li' + $data;

            }
          
        });

    },
   
   set_validation_blocker: function ( msg )
    {
        console.log( "*** Blocking validation" );
        this.validation_blocked = msg;
    },
    clear_validation_blocker : function (  )
    {
        console.log( "*** Cleared validation" );
        this.validation_blocked = 0;
    },


    placeOrderDisabled: function (  )
    {
        $(".place-order").addClass("disabled");
    },

    placeOrderEnabled: function (  )
    {
        $(".place-order").removeClass("disabled");
    },
    
    doCheckoutEvents: function()
    {
        var self = this;
        $(document).on("click", ".place-order-checkout", function()
        {
            
            session = mobile_ha.common.check_session_state();
            if(session.session_state == false){ 
                alert("Sorry sessions are not active please login again!")
                window.location.reload();
                return;
            } 
            
            if($(this).attr('disabled')!='disabled'){
                
                if(self.checkBreadType() == 'error'){
                    return;
                }
            
            var order_string = $('input[name="odstring"]').val();
            if(typeof order_string == "undefined" || !order_string){
                alert("Please check your cart items. It can't be empty.");
                return false;
            }
            if ($(this).hasClass("disabled")) return false;
            var order_type  = 1;//$("#order-type").val();  //DM #615
            var store_id    = $('input[name="delivery_store_id"]').val();
            var address_id  = $('input#address-id').val();
            
            var sub_total         = $('#hidden_sub_total').val();
            
            if(sub_total < 10 && order_type == 1){
                alert("$10.00 SUBTOTAL MINIMUM AMOUNT. Please add more items.");
                 $('.place-order-checkout').addClass('disabled');
                return false;
            }
            if(order_type == 1){                
                
                if(!store_id){                  
                    alert("No store near to your delivery place.");
                     
                    return false;
                }
            
            }else if(order_type == 0){
                store_id = address_id;
            }else{
                alert("Please select Delivery or Pickup.");
                
                return false;
            }   
            
            var address_id = $('input#address-id').val();
            if(!address_id){
                alert("Please select Address.");                
                return false;
            }

            // TODO BLOCKER MESSAge...
            //  this.validation_blocked = MESSAGE
            if( self.validation_blocked != 0 )
            {
                console.log( "!!!Here" );
                alert( self.validation_blocked );
                return false;
            }

            var now_or_specific     = $('input.switch_date:checked').val();
            
            var date_val            = $(".delivery_date").val();
            var time_val            = $('div.timechange select').val();
            var coupon_code         = $('#hidden_discount_id').val();
            var discount_amount     = $('#hidden_discount_amount').val();
            var tip                 = $('#select_tip_amount').val();
            var tax                 = $('#hidden_tax').val();
            
            
            var total        = $('#hidden_grand_total').val();
            var delivery_fee = 0;
            if($('#hidden_delivery_fee').val()){
                var delivery_fee = $('#hidden_delivery_fee').val();
            }
            
            if($('#enter_datetime_seelction').hasClass('first-time')){
                alert("Please Select Date/Time for your order.");
                 $('.place-order-checkout').addClass('disabled');
                return false;
            }   
            
            if(now_or_specific == "specific"){
                    
                if(typeof date_val == "undefined" || !date_val ){
                    alert("Please select date for order.");                 
                    return false;
                }
                if(typeof time_val == "undefined" || !time_val ){
                    alert("Please select date for order.");                 
                    return false;
                }
                var time_only = time_val.replace(" PM", ":00");
                time_only = time_val.replace(" AM", ":00");
                var date_combined = date_val+" " +time_only;
                
                var select_date = new Date(date_combined);
                var currentDayHour;
                $.ajax({
                    type: "POST",
                    url: SITE_URL + 'checkout/getCurrentHour/',
                    async: false,                   
                    success: function(data) {
                        currentDayHour = JSON.parse(data);

                        // self.clearUserTimeValidated(  );
                        // self.clearUserTimeTemp(  );
                    }
                });
                
                
                var currentHour = parseInt(currentDayHour.hour);
                var currentDay = parseInt(currentDayHour.day);
                var currentDate = currentDayHour.date_time_js;
                

                currentDate = currentDate.split(",");
                var current_date = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);
                    
                //self.setCurrentDayHour( current_date );
                
                
                
                if( select_date <  current_date ){
                    alert("Select future date time.");                  
                    return false;
                }
                
                date_val = select_date.toLocaleDateString();
                
                date_val = select_date.getFullYear()+"-"+(select_date.getMonth()+1)+"-"+select_date.getDate();
                                
            }
            
            var card_id = $("#card-id").val();
            if(!card_id){
                alert("Please select billing");
                return false;           
            }
            
            var meal_name = "";
             var _card_number_auth = $('input[name="_card_number_auth"]').val();
                var _expiry_month_auth = $('input[name="_expiry_month_auth"]').val();
                var _expiry_year_auth = $('input[name="_expiry_year_auth"]').val(); 
                    var credit_card = $('input[name="cardNo"]').val();
                var cvv = $('input[name="cardCvv"]').val();
                var exp_mth = $('select[name="cardMonth"]').val();
                var exp_yr = $('select[name="cardYear"]').val();
            
            var data = { 'mealname' : meal_name,  'store_id' : store_id , 'address_id' : address_id , 'order_item_id' : order_string , 'total' : total , 'tax' : tax , 'delivery_fee': delivery_fee,  'delivery' : order_type, 'date' : date_val, 'time' : time_val ,  'now_or_specific' : now_or_specific , credit_card_id : card_id,
                            'coupon_code':coupon_code,'tip':tip,'sub_total':sub_total , "off_amount":discount_amount,
                                                'credit_card': credit_card,
                            'cvv': cvv,
                            'exp_mth': exp_mth,
                            'exp_yr': exp_yr,
                            '_card_number_auth': _card_number_auth,
                            '_expiry_month_auth': _expiry_month_auth,
                            '_expiry_year_auth': _expiry_year_auth};
            
            
            
            var currentItemthis  = $(this);
            if(currentItemthis.hasClass('processBusy')) return;
            currentItemthis.addClass('processBusy');
            currentItemthis.text('PROCESSING...'); 
                    
            $.ajax({
                type: "POST",
                data:data,              
                dataType:'json',
                url: SITE_URL+'checkout/place_order/',
                success: function(e) {

                            if(e.response.status_code == '204' && e.response.message == "FAIL_TO_GET_ORDER_ITEMS"){
                                alert("Some of the items you are trying to place order are already removed from the cart, So please order again");
                                location.reload();
                                return false;
                            }else if(e.response.status_code == '204' && e.response.message == "FAIL_TO_MATCH_TOTAL"){
                                alert("Something went wrong, please try again");
                                location.reload();
                                return false;
                            }
                    
                            if ( e.response == undefined ||  e.response.status_code != '200' ) 
                            {                               
                                
                                    alert("There was an error processing your order, please ensure your billing information is correct and try again.");
                                    currentItemthis.text('PLACE ORDER'); 
                                    currentItemthis.removeClass('processBusy');
                            
                                
                                return false;
                            }
                            else                
                            {
                                $('.shopping-cart span').text(0);
                                self.setCookie("discount_code","",1);                               
                                window.location.href = SITE_URL+"checkout/checkout_confirm/"+e.data;
                            }
                } 
            });             
            
            }
        });
        
    },
        
    applyDiscountEvents: function(){
        
        var self = this;
        
        $(document).on("click", "div.apply-discount-code", function(){          
            $("#apply_discount_popup").show();                      
        });
        
        //Click event Ending of place order
        $(document).on("click","a.check-discount-code",function(){
           
            var discount_id = $(this).prev().val();
            if(discount_id){
                //applying discount by calling applyDiscount();
                self.applyDiscount(discount_id);
                
            }
                
        });
    
    },
    
    // applies valid discount code if any. Input: discount code. Output: Set discountcode if valid else show error.
    applyDiscount : function(discount_id){
        
      var self = this;
     
      $cookieDisCode = self.getCookie("discount_code");
      
      if(!discount_id) {
          
          discount_id = $cookieDisCode;
      } 
      
      var disCode = discount_id;
      
      if(discount_id){ 
      
        $.ajax({
            type: "POST",
            data: {
                "code": discount_id,
                "user_id": SESSION_ID
            },
            url: SITE_URL + 'checkout/applyDiscount',
            dataType: "json",
            success: function(e) {

                if (e == "") {
                    //showing error message if invalid coupon code entered.
                    alert("Sorry!  This discount code is not valid.  Please try again.");
                    return false;
                }

                if(e.status=="error" && e.message == "single use per user"){
                    alert("Woops! Looks like this code was already used.");
                    $("#apply_discount").val("");
                    return false;
                }
                if(e.status=="error" && e.message == "not alive"){
                    alert("This promo code is not valid. Please try again!");
                    $("#apply_discount").val("");
                    return false;
                }

                var discount = e;
                var discount_id = discount.id;

                var newtextbox = '<input id="hidden_discount_id" type="hidden" value="' + discount_id + '"/>';
                newtextbox += '<input id="hidden_discount_amount" type="hidden" value="0"/>';
                $("div.discount_details").append(newtextbox);

                 var tax = $("#hidden_tax").val();
                if (discount.applicable_to == 1) {

                    var select_tip_amount = $("#select_tip_amount").val();

                    if (discount.type == 0) {
                        var per = parseFloat(discount.discount_amount) / 100;
                        var hidden_grand_total = $("#hidden_grand_total").val().replace(",", "");
                        hidden_grand_total = parseFloat(hidden_grand_total);

                        if (discount.minimum_order == 1) {

                            if (hidden_grand_total < parseFloat(discount.minimum_order_amount)) {

                                $("input#discount_code").val("");
                                alert("Sorry, this discount code can only be used for orders of $" + discount.minimum_order_amount + " or more.");
                                return false;
                            }
                        }
                        if (discount.maximum_order == 1) {

                            if (hidden_grand_total > parseFloat(discount.maximum_order_amount)) {

                                $("input#discount_code").val("");
                                alert("Sorry, this discount code can only be used for orders up to $" + discount.maximum_order_amount + ".");
                                return false;

                            }

                        }

                        var amount = hidden_grand_total * per;
                        amount = parseFloat(amount.toFixed(2));
                        
                        var value_remain = hidden_grand_total - amount;
                        $(".tax-total-wrapper .total p.p-discount").show();
                        $(".tax-total-wrapper .total p.p-discount-title").show();
                        $(".tax-total-wrapper .total p.p-discount").text("-$"+amount);                          

                        /*var final_cal = parseFloat(value_remain) + parseFloat(select_tip_amount);*/
                        var final_cal = parseFloat(value_remain);
                        final_cal = parseFloat(final_cal) + parseFloat(tax);

                        final_cal = final_cal.toFixed(2);
                        $(".grand-total h2").text("$"+final_cal);
                        
                        $(".amount-done-wrapper h3").html("$"+final_cal);  
                        $('#hidden_grand_total').val(final_cal);
                        $('#hidden_discount_id').val(discount.id);
                        $('#hidden_discount_amount').val(amount);                           

                    } else {
                        var hidden_grand_total = $("#hidden_grand_total").val().replace(",", "");
                        hidden_grand_total = parseFloat(hidden_grand_total);

                        if (discount.minimum_order == 1) {

                            if (hidden_grand_total < parseFloat(discount.minimum_order_amount)) {
                                $("input#discount_code").val("");
                                alert("Sorry, this discount code can only be used for orders of $" + discount.minimum_order_amount + " or more.");
                                return false;
                            }
                        }
                        if (discount.maximum_order == 1) {

                            if (hidden_grand_total > parseFloat(discount.maximum_order_amount)) {

                                $("input#discount_code").val("");
                                alert("Sorry, this discount code can only be used for orders up to $" + discount.maximum_order_amount+ ".") ;
                                return false;

                            }

                        }

                        var value_remain = hidden_grand_total - parseFloat(discount.discount_amount);

                        var amount = parseFloat(discount.discount_amount);
                        amount = amount.toFixed(2);
                        $(".tax-total-wrapper .total p.p-discount").show();
                        $(".tax-total-wrapper .total p.p-discount-title").show();
                        $(".tax-total-wrapper .total p.p-discount").text("-$"+amount);
                        
                        var final_cal = parseFloat(value_remain);
                        final_cal = parseFloat(final_cal) + parseFloat(tax);

                        final_cal = final_cal.toFixed(2);
                        $(".grand-total h2").text("$"+final_cal);
                        $(".amount-done-wrapper h3").html("$"+final_cal);  
                        $('#hidden_grand_total').val(final_cal);
                        $('#hidden_discount_id').val(discount.id);
                        $('#hidden_discount_amount').val(value_remain);
                    }
                    $(".sandwich-popup").hide();    
                }

                if (discount.applicable_to == 0) {

                    if (discount.type == 0) {
                        var select_tip_amount = $("#select_tip_amount").val();
                        var hidden_sub_total = $("#hidden_sub_total").val().replace(",", "");
                        hidden_sub_total = parseFloat(hidden_sub_total);

                        if (discount.minimum_order == 1) {
                            if (hidden_sub_total < parseFloat(discount.minimum_order_amount)) {
                                $("input#discount_code").val("");
                                alert("Sorry, this discount code can only be used for orders of $" + discount.minimum_order_amount + " or more.");
                                return false;
                            }
                        }
                        if (discount.maximum_order == 1) {

                            if (hidden_sub_total > parseFloat(discount.maximum_order_amount)) {

                                $("input#discount_code").val("");
                                alert("Sorry, this discount code can only be used for orders up to $" + discount.maximum_order_amount+ ".") ;
                                return false;

                            }

                        }

                        var per = parseFloat(discount.discount_amount) / 100;

                        var amount = hidden_sub_total * per;
                        var value_remain = (hidden_sub_total - amount).toFixed(2);
                        amount = amount.toFixed(2);
                        $(".tax-total-wrapper .total p.p-discount").show();
                        $(".tax-total-wrapper .total p.p-discount-title").show();
                        $(".tax-total-wrapper .total p.p-discount").text("-$"+amount);                  

                        var final_cal = parseFloat(value_remain) + parseFloat(select_tip_amount);
                        final_cal = parseFloat(final_cal) + parseFloat(tax);

                        final_cal = final_cal.toFixed(2);
                        $(".grand-total h2").text("$"+final_cal);
                        $(".amount-done-wrapper h3").html("$"+final_cal);  
                        $('#hidden_grand_total').val(final_cal);
                        $('#hidden_discount_id').val(discount.id);
                        $('#hidden_discount_amount').val(amount);

                    } else {
                        var select_tip_amount = $("#select_tip_amount").val();
                        var hidden_sub_total = $("#hidden_sub_total").val().replace(",", "");

                        hidden_sub_total = parseFloat(hidden_sub_total);

                        if (discount.minimum_order == 1) {
                            if (hidden_sub_total < parseFloat(discount.minimum_order_amount)) {
                                $("input#discount_code").val("");
                                alert("Sorry, this discount code can only be used for orders of $" + discount.minimum_order_amount + " or more.");
                                return false;
                            }
                        }
                        if (discount.maximum_order == 1) {

                            if (hidden_sub_total > parseFloat(discount.maximum_order_amount)) {

                                $("input#discount_code").val("");
                                alert("Sorry, this discount code can only be used for orders up to $" + discount.maximum_order_amount+ ".") ;
                                return false;

                            }

                        }
                        
                        
                         
                        
                        var discount_amount = discount.discount_amount;
                        discount_amount = parseFloat(discount_amount).toFixed(2);
                        var amount = hidden_sub_total - discount.discount_amount;
                        var value_remain = (hidden_sub_total - amount).toFixed(2);
                        $(".tax-total-wrapper .total p.p-discount").show();
                        $(".tax-total-wrapper .total p.p-discount-title").show();
                        $(".tax-total-wrapper .total p.p-discount").text("-$"+discount_amount);                         

                        var final_cal = parseFloat(hidden_sub_total) + parseFloat(select_tip_amount) - discount.discount_amount;
                        final_cal = parseFloat(final_cal) + parseFloat(tax);

                        final_cal = final_cal.toFixed(2);
                        $(".grand-total h2").text("$"+final_cal);
                        $(".amount-done-wrapper h3").html("$"+final_cal);  
                        $('#hidden_grand_total').val(final_cal);
                        $('#hidden_discount_id').val(discount.id);
                        $('#hidden_discount_amount').val(discount_amount);
                    }
                    
                    $(".sandwich-popup").hide();    
                    //storing code to temporary cookie if it is getting applied. 
                    self.setCookie("discount_code",disCode,1);
                }

                return false;
            }
        });
        
      }
    },
    
    facbook_auth_window : function(href){
        
         wnd = window.open(href,'_blank','width=480,height=640'); 
         wnd.onunload = function(){
          
             
            if(window.location.href.indexOf("createsandwich") != -1){ 
                $('.share-sandwich').attr('style','background:transparent url("../app/images/share-sandwich.jpg") no-repeat scroll 0 0/34px 34px');
            } else {
                sloader = $('.share-sandwich').find('img');
                $(sloader).attr('src',SITE_URL+'app/images/share-img.png');  

            }
        };
    },
    
    //friend sandwiches 
    enable_friend_sandwiches : function(){
        var self = this; var href;
        $('#show_friends_sandwich').unbind('click').on('click',function(e){
            e.preventDefault();

             
            href = $(this).attr('href');
            text = $(this).text();
            sessionState =  $(this).attr('rel');
            sessionFBState =  $(this).data('fbrel');
             
            if(text.indexOf("FRIENDS'SANDWICHES") != -1) {
                
                if( sessionFBState == "false")
                {
                    self.set_friends_popup_data();
                }
                else
                {
                    if (sessionState == "false") {
                        mobile_ha.common.setCookie("reopen_fb_pop",true,1);  self.facbook_auth_window(href);
                        return;
                    } else {
                        self.show_friends_popup(href); 
                    }
                }
                
            }
        });
    },
    
    show_friends_popup : function(href){
        var self = this; 
        
        $("#frnDpopmenuz").show(10,function(){
        session = self.check_session_state();
        if(session.session_state == true){ 
         
            self.set_friends_popup_data();
            mobile_ha.common.setCookie("reopen_fb_pop",false,1);
        } else { mobile_ha.common.setCookie("reopen_fb_pop",true,1);   self.facbook_auth_window(href);   }
            
        });
        
        
        self.body_scroll_ops(false);
        $("#frnDpopmenuz .close-button").unbind('click').on('click',function(){
            $("#frnDpopmenuz").fadeOut(100);
            self.body_scroll_ops(true);
        });
    },
    
    set_friends_popup_data : function(){
        var self = this;
     
          $.ajax({
                type: "POST",
                url: SITE_URL + 'menu/getFBfriendsMenu',
                success: function(data) {  
                    $(".frndpopcont").html(data);
                     $('.back-to-menu').on('click',function(e){
                        e.preventDefault();
                        $('.close-button').trigger('click');
                    });
                }
         });
         
    },
    
    
    show_friends_popup_onload : function(){
        var self = this;
        if(window.location.href.indexOf("menu") != -1){ 
        $(document).ready(function(){
            
         if( self.getCookie("reopen_fb_pop")  == "true" ){
             self.show_friends_popup();
             self.set_friends_popup_data();
             
             self.setCookie("reopen_fb_pop",false,1);
         }   
        }); }
    },
    
    
    check_valid_User_AccessToken : function(){
        var self = this;
         
        var out;
          $.ajax({
                type: "POST",
                url: SITE_URL + 'myaccount/getUserAccessToken',
                async:false,
                dataType : 'json',             
                success: function(data) { out = data;  }
         });
        return out;
    },
    
    showSandwichSummaryCheckout: function(){
        
        $(document).on("click", ".cart-item-name", function(){
            
            var $data = $(this).data("sandwich-ingredients");
            var toast = $(this).data("toast");
            if($data){
                
                var $out = "";
                Object.keys($data).forEach(function(e) {
                    
                    if (e != "BREAD") {
                    }
                    if(e == "BREAD" && $data[e].item_price[0] == "0.00")
                    {
                        $out += '<li><h4 style="text-decoration: none !important;border-bottom: 2px solid;">BASE PRICE <span>+$'+BASE_FARE+'.00</span></h4></li>';
                    }

                    if ($data[e].item_price[0] < 0) sign = '-';
                    else sign = '+';
                    
                    var vals;
                    if(typeof($data[e].item_price[0]) == "undefined"){
                        vals = 0;
                    }else{
                        if(e == "BREAD")
                        {
                            vals = Math.abs($data[e].item_price[0]);
                        }
                        else
                        {
                            var sum = 0;
                            for (var i = 0; i < $data[e].item_price.length; i++) 
                            {
                                sum += Math.abs($data[e].item_price[i]);
                            }
                            vals = sum;
                        }
                    }
                    
                    $out += "<li>";
                    $out += "<h4>" + e + " <span>" + sign + "$" + vals.toFixed(2) + "</span></h4>";

                    if (e == "BREAD") {
                        $out += "<p>" + $data[e].item_name[0] + "</p>"
                    } else {
                        $sub = "";
                        $en_ln = $data[e].item_name.length;
                        $qty = $data[e].item_qty;


                        $data[e].item_name.forEach(function(e, i) {

                            if (!e) {
                                $final_name = "None"
                            }

                            if ($qty[e] != undefined) {
                                if (e != "NULL") $final_name = e + " (" + $qty[e][1] + ")";
                                else $final_name = "None"
                            } else {
                                if (e != "NULL") $final_name = e + " (1)";
                                else $final_name = "None"
                            }

                            if (i < $en_ln - 1) {
                                delimit = ", "
                            } else {
                                delimit = ""
                            }

                            $sub += $final_name + delimit + " "
                        });

                        if ($data[e].item_name.length > 0) {
                            $out += "<p>" + $sub + "</p>";
                        } else {
                            $out += "<p>" + "None" + "</p>";
                        }

                    }

                    $out += "</li>"
                });
                //return $out
                if(toast == 1)
                {
                    $out += "<li><h4>" + "Toasted" + "</h4></li>";
                }
                $('#summary .summary_details').html($out);
                $('#summary').show();
                mobile_ha.common.body_scroll_ops(false);
            }
            
        });
        
    },
    
    removeCartItems: function(){
        
        $(document).on("click","a.remove_cart_item", function(){
            
            var order_item_id = $(this).attr("data-target");
            var item_id = $(this).attr("data-item");
            
            $.ajax({
                type: "POST",
                data:{ "order_item_id" : order_item_id,'itemid' : item_id  },
                url: SITE_URL+'sandwich/remove_cart_item',
                beforeSend: function() {
                    
                },
                complete:function() {
                    
                },
                success: function(e){ 
                    
                    window.location.reload();
                }
            });
            
        }); 
            
    },
    
    add_product_descriptor_to_cart: function() {

        var self = this;

        $(document).on("click","#product-descriptor .add_desc",function(e){              
            
            var data = $(this).parent().parent().serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'sandwich/add_to_cart',
                beforeSend: function() {
                   
                },
                complete: function() {
                   
                },
                success: function(e) {
                    self.added_to_cart_transition(e);                   
                    $(".sandwich-popup").hide();
                }
            });
        });

    },

preload_images : function(){
         var self = this;
          fl      = self.getCookie("first_load");
          if(fl != 'true'){ 
          array   = self.getXMLdata(SITE_URL + 'app/manifest.xml');
          strinf  = JSON.stringify(array);
          self.setCookie("common_images",strinf,1);
          } else{ 
              
             if(self.getCookie("common_images" )){ 
              array = JSON.parse(self.getCookie("common_images" ));
             } array = [];
          
          }
         
         
          var x = [];
          
          if( this.is_empty( array ) )
            return;
          else{}
            //console.log( array );

          array.forEach(function(e,j){
               x[j] = new Image();
               x[j].setAttribute('src',e);
          });
          self.setCookie("first_load",true);
    },
    
    getXMLdata     : function(file){
        //console.log( "Get XML for " + file );
        var imagearray = [];
        try
        {
            var request = new XMLHttpRequest();
            request.open("GET", file, false);
            request.send();
            var xml = request.responseXML;
    
            if( !xml )
                return;
            images = xml.getElementsByTagName("images");
             $img   =  $(images).find('img');
              $img.each(function(e,i){
                  $image = $(i).text();
                  imagearray[e] = SITE_URL+'app/images/'+$image;
              }); 
        }
        catch( e )
        {

        }
        return imagearray;
    },
    
    ResumeFromOrderState : function(){
 
        var self = this;
        var ajaxUrl,intervel,intervel2,intervel3,succreate;
        succreate = 0;
        //for launch only.
        $($("#select_delivery_or_pickup .order-type")[0]).trigger("click");
        //end.

        if(mobile_ha.common.getCookie('addressId') != undefined && mobile_ha.common.getCookie('addressId') != '' ){
            self.checkoutSlectionData.addressId=parseInt(mobile_ha.common.getCookie('addressId'));
        }

        if(mobile_ha.common.getCookie('cardId') != undefined && mobile_ha.common.getCookie('cardId') != '' ){
            self.checkoutSlectionData.cardId=parseInt(mobile_ha.common.getCookie('cardId'));
        }

        
        if(self.checkoutSlectionData.addressId != null  || 
            self.checkoutSlectionData.cardId != null    || 
            self.checkoutSlectionData.storeId != null   || 
            self.checkoutSlectionData.isNotTimeNow != null)
        {
             
            
            if(self.checkoutSlectionData.orderType == 1){
                delivery = 1; 
                itemIndex = 0                    
            } else { 
                delivery = 0;
                itemIndex = 1
            }    
            
             $($("#select_delivery_or_pickup .order-type")[itemIndex]).trigger("click");
                 
            if( $('#select_delivery_or_pickup_div h2').text() == "Delivery" || $('#select_delivery_or_pickup_div h2').text() == "Pickup" ){
                
                
                intervel = setInterval(function(){ 
                
                if( $(".select-address-button[data-address-id='"+self.checkoutSlectionData.addressId+"']").length > 0){ 
                    self.zipPopUpFlag = 0;
                    //console.log('3');
                    $(".select-address-button[data-address-id='"+self.checkoutSlectionData.addressId+"']").trigger('click');
                    
                }
                
                $(".select_address_div").hasClass('first_time')

                
                
                if(($("#select_address_div").hasClass('first-time') == false && self.checkoutSlectionData.addressId != null) )
                 { 
                    
                    clearInterval(intervel);
                 } 
                
                 },500);
             
            }
            
    
            
            
            
                
                if( $('#select_delivery_or_pickup_div h2').text() == "Delivery" || $('#select_delivery_or_pickup_div h2').text() == "Pickup" ){
                    
                    if( $(".select-card-button[data-card-id='"+self.checkoutSlectionData.cardId+"']").length > 0){ 
                        $(".select-card-button[data-card-id='"+self.checkoutSlectionData.cardId+"']").trigger('click');
                    }
                    
                    
                    if( ( $("#enter_payment_div").hasClass('first-time') == false && self.checkoutSlectionData.cardId != null) ){
                        
                        
                    }
                }
                
        
            
        
            setTimeout(function(){
                self.allBarsNotRed();
            }, 1100);   
             
        } else { 
            
        ajaxUrl  = SITE_URL+'home/get_last_order_detail';
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            dataType : 'json', 
            success: function(e){ 
                //console.log(e)
            addid  = parseInt(e.address_id);
            billid = parseInt(e.billing_id);
            del    = parseInt(e.is_delivery);
            
            
        
                if(isNaN(del)==false){
                    var itemIndex = 0;
                if(del == 1){
                    delivery = 1; 
                    itemIndex = 0                    
                }else{ 
                    delivery = 0;
                    itemIndex = 1
                }   
                    
                
                
                
                
                    if( $('#select_delivery_or_pickup_div h2').text() == "Delivery" || $('#select_delivery_or_pickup_div h2').text() == "Pickup" ){
                        
                    var k  = 0; 
                    intervel = setInterval(function()
                    {
                        if( $(".select-address-button[data-address-id='"+addid+"']").length > 0){
                            self.zipPopUpFlag = 0;
                            $(".select-address-button[data-address-id='"+addid+"']").trigger('click');
                        }
                        if($('#select_address_div .tax h2').text() != "CHOOSE A LOCATION"){ 
                         clearInterval(intervel);
                        }
                        k++;
                        if(k > 10) clearInterval(intervel);
                     },500);
                      
                        if( $(".select-card-button[data-card-id='"+billid+"']").length > 0){ 
                            $(".select-card-button[data-card-id='"+billid+"']").trigger('click');
                        }
                        self.storeTimingsEvents(delivery, e);
                    }
            }else{
                self.allBarsNotRed();
            }
            }
        });
        
        }
         
    },
    
    
    getAlphaDay : function(day){
        var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        return days[day];
    },
    
    getAlphaMonth : function(month){
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return monthNames[month];
    },
    
    get_store_timeslot_ajax: function( dayOfWeekName, address_id, delivery_pickup )
    {
       return  $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentStoreTimeSlot/',
            async: false,
            data: {
                deliveryOrPickup: delivery_pickup,
                id: address_id,
                day: dayOfWeekName
            },
            success: function( ajax_response ) {
                if(ajax_response == "false")
                {
                    alert("Delivery not available in this area");
                    return false; 

                }
                else
                {
                    store_timing = setTimeout( function( )
                    {
                        store_timing = JSON.parse(ajax_response); 
                        return store_timing; 
                    } , 200);
                    
                    self.timeslot_information = store_timing ;
                    return   store_timing;
                }
                
            }
        });
        
    },

    get_active_address: function ()
    {
        var self = this;
        if ( !$("#address-id").val() ) {
            return setTimeout(self.get_active_address, 1000);
        }
        return $("#address-id").val();
    // do work here
    },


    checkTimingsRealtedConditions:  function( selectedDate )
    {
        var self = this;

        if( selectedDate == undefined )
        {
            selectedDate = $( ".delivery_date" ).val(  );
        }

        var today_Date;
        today_Date = new Date ( self.getCurrentDateFromServer_NOW(  ) );

        var today_DayOfWeek = today_Date.getDayName(  );
      
        var is_delivery = -1;
        is_delivery = $("#order-type").val();      
        
        var address_id;
        var deliveryOrPickup;

        var is_delivery = 1;    // DEFAULT ONLY DELIVERY IN THIS VERSION
     
            if ( is_delivery == 0 )
            {
                 deliveryOrPickup = "pickup";
                 var pickupStoreId = $("#hidden_store_id").val()
                address_id = pickupStoreId;
            }
            else
            {
                deliveryOrPickup = "delivery";
                var deliveryAddressId =  self.get_active_address(  ); //$("#address-id").val(); ;
                address_id = deliveryAddressId;
            }
            
            var store_timeslot_response = self.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
            
            store_timeslot_response_text = JSON.parse( store_timeslot_response.responseText  );

            if( store_timeslot_response_text == null  ||  store_timeslot_response_text === false   ){
                return;
            }
              
            this.setOpenTime( this.set_date_time(  today_Date,  store_timeslot_response_text['open'] ) );
            this.setClosedTime( this.set_date_time(  today_Date,  store_timeslot_response_text['close'] ) );
    
            this.setOpenHour( store_timeslot_response_text['open'] );
            this.setCloseHour( store_timeslot_response_text['close'] );
            
            if( store_timeslot_response_text == false || store_timeslot_response_text == null ){
                return false;
            };

            $("#hidden_store_id").val( store_timeslot_response.store_id ); 
            self.getSpecificDayForStore( $("#hidden_store_id").val(  ) );
            


    }, 
    
    noon_or_greater:function ( day_time )
    {
       
        user_time = new Date( day_time );
        noon_time = new Date( day_time );
        return_time = new Date( day_time );

        noon_time = this.set_date_time( noon_time, "12:00" );

        user_vs_noon = this.compare_times( user_time, noon_time );
        

        if( user_vs_noon == ">" ){
            return_time = this.set_date_time( day_time, user_time );
        }
        else{
           return_time = this.set_date_time( day_time, "12:00" );
        }

        return return_time;
    },

    setCookie_forcalendar : function(){
        var self = this;
            if(mobile_ha.common.getCookie('specific_date_exist') != undefined && mobile_ha.common.getCookie('specific_date_exist') != '' )
            {
                //console.log( "Specific time, so chenge the dateitme_selection thing" );
                var selected_date_time = mobile_ha.common.getCookie('specific_date_selected') +' '+mobile_ha.common.getCookie('specific_time_selected_tm');
                $("#enter_datetime_seelction").removeClass("first-time");
                $("#enter_datetime_seelction h2").text(selected_date_time);
                $('#radio2').prop('checked',true);
            }
    },
    
    storeTimingsEvents: function(delvry, e) 
    {
        
    },

    checkNowStoreIsOpened: function()
    {
        $('.place-order-checkout').removeClass('disabled');                 
        var self = this;
        
        self.activeAjaxConnections++;
        var currentDayHour = 0;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentHour/',
            async: false,
            
            success: function(data) {
                currentDayHour = JSON.parse(data);
               
                self.activeAjaxConnections--;
            }
        });

        //10 to 4 PM
        var currentHour = parseInt(currentDayHour.hour);
        var currentDay = parseInt(currentDayHour.day);
        var currentDate = currentDayHour.date_time_js;
        currentDate = currentDate.split(",");
        currentDate = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);


        var delvry = -1;

        if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
            delvry = 1;
        } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
            delvry = 0;
        }

        var deliveryORPickup = "";
        var address_id = 0;

        setTimeout(function() {
            if (delvry) {
                deliveryORPickup = "delivery";
                var deliveryAddressId =$("input#address-id").val();
            } else {
                deliveryORPickup = "pickup";
                var pickupStoreId = $("input.pickup_address_id").val();
            }

            var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                                
            if (address_id == 0 && deliveryORPickup == "pickup") {
                
                address_id = pickupStoreId;
                
            } else {

                address_id = deliveryAddressId;
            }
                        
            if (deliveryORPickup && address_id && currentDay) {

                $.ajax({
                    type: "POST",
                    url: SITE_URL + 'checkout/getCurrentStoreTimeSlot/',
                    data: {
                        deliveryORPickup: deliveryORPickup,
                        id: address_id,
                        day: days[currentDay]
                    },
                    async: false,
                    success: function(response) {
                 
                        if(response != "false"){ 
                             
                            store_timing = JSON.parse(response);
                            
                            var open = new Date(currentDate);
                            var close = new Date(currentDate);
                            var open_time = store_timing.open.split(":");
                            var close_time = store_timing.close.split(":");
                            
                            open.setHours(open_time[0]);
                            open.setMinutes(open_time[1]);

                            close.setHours(close_time[0]);
                            close.setMinutes(close_time[1]);
                            
                            
                            if (currentDate < close && currentDate > open) {
                                                            
                                return;                                 
                            }
                        }
                        
                        self.allBarsNotRed();
                    }
                }); 
            }
            
            self.allBarsNotRed();
            
        },300); 
        
        
        

    },
    
    allBarsNotRed: function(){
        
        //console.log("This is getting called.");
            
        if($(".personal-detail .details-1").hasClass("first-time")){            
             $('.place-order-checkout').addClass('disabled');
        }else if($("table.cart_list_items tr").hasClass("first-time")){
            $('.place-order-checkout').addClass('disabled');
        }else if($(".hundred-min-red-bar p").length == 1){
            $('.place-order-checkout').addClass('disabled');
        }else{
            if( self.validation_blocked == "" ||  self.validation_blocked == 0 ){
                $('.place-order-checkout').removeClass('disabled');
            }
        } 
        
    },
    process_url:function(){
        var resetVar=false;
        var wlh=window.location.href;
        if(  wlh.indexOf('/createsandwich') == -1  && wlh.indexOf('/facebook/') == -1  && wlh.indexOf('/sandwich/') == -1  && wlh.indexOf('/myaccount/') == -1 )
        {
            resetVar=true;
        }
        else if(wlh.indexOf('/sandwich/')!=-1 && wlh.indexOf('/gallery/')!=-1){
            resetVar=true;
        }
        

        if(resetVar){
            $.ajax({
            type: "POST",
            url: SITE_URL + 'CreateSandwich/unsetSessionCookie/',
            async: false,
            
            success: function(data) {
                return;
            }
        });
        }
        },


    setOpenHour: function( hour )
    {
       this.open_hour = hour;
    },

    setCloseHour: function( hour )
    {
        this.close_hour = hour;
    },

     getCloseHour: function ( )    
    {
         if( this.close_hour )
            return this.close_hour;
        else
            return 0;
    },

    getOpenHour: function ( )    
    {
        if( this.open_hour )
            return this.open_hour;
        else
            return 0;
    },


    setOpenTime: function ( openTime )
    {
        //console.log( "_____ SET OPENED TIME: " + openTime );
               this.opened_time =openTime;

    },

    setClosedTime: function ( closedTime )
    {
        //console.log( "_____ SET CLOSED TIME: " + closedTime );
        this.closed_time = closedTime;
    },


    getClosedTime: function (  )
    {
        if( this.closed_time )
            return this.closed_time;
        else
        {
            this.checkTimingsRealtedConditions(  );
            return this.closed_time;
        }
        
    },

    clear_checkout_message: function(  )
    {
        $('.checkout-msg-red').remove();
    },
    set_checkout_message: function( txt )
    {
        var msg = "<span class='checkout-msg-red'>" + txt + "</span>";
        $("div#enter_datetime_seelction").addClass("first-time");
        $("div#enter_datetime_seelction h2").html(msg);

         if (navigator.userAgent.match(/(ipod|iPhone|iPad)/)) {
            $('#front_page_delivery_time').text( msg );
            $('#front_page_delivery_time').html( msg );
        }

    },

    set_checkout_message_clean: function( txt )
    {
        var msg = txt;
        $("div#enter_datetime_seelction").addClass("first-time");
        $("div#enter_datetime_seelction h2").html(msg);
    },

    getSpecificDayForStore: function( storeId )
    {
    
        self = this;
        $.ajax({
            type: "POST",
            dataType: 'json',
            async: false,
            data: {
                "storeId": storeId
            },
            url: SITE_URL + 'checkout/getSpecificDayForStore',
            success: function(e) {
                console.log( "Checked out specific days.Success" );
                var message  = e.message;
                if(message !='')
                { 
                    self.specific_day = e.specific_day;
                    self.specific_message = e.specific_message;

                }
            }
        });

    },


    getOpenCloseFromDate: function( date )
    {
        self = this;
        date = new Date( date );

        var today_DayOfWeek = date.getDayName(  );
    
        var deliveryOrPickup = "delivery";
        var deliveryAddressId = $("input#address-id").val();
        var address_id = deliveryAddressId;
        var store_timeslot_response = self.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
        store_timeslot_response = JSON.parse( store_timeslot_response.responseText);
       

        var open_time =  store_timeslot_response['open'] ;
        var close_time =  store_timeslot_response['close'];

        var open_d = self.set_date_time(  date,  open_time );
        var close_d = self.set_date_time(  date,  close_time );


        if(  store_timeslot_response['delivery_active'] == 1 ){     // active 1 means it's off 
            store_delivery_off_message =  1;
        }
        else{
            store_delivery_off_message = 0;
        }

         ret = {
                "day_of_week" :store_timeslot_response['day'],
                "store_closed_on_day": store_timeslot_response['store_active'],
                "open_time": open_d,
                "close_time": close_d,
                "store_id":store_timeslot_response['store_id']
            };

        return ret;
    },


    getOpenTime: function( ) 
    {
         
         if( this.opened_time )
            return this.opened_time;
        else
        {
            this.checkTimingsRealtedConditions(  );
            return this.opened_time;
        }
    },

    check_catered: function( )
    {},

    setTimeStateArray: function ( timestate )
    {

        this.setCookie( "time_state_array", timestate );
    },

     setTimepicked: function ( timePicked )
    {
        this.setCookie( "time_picked",timePicked );
    },

    getTimepicked: function ( )
    {
        if( this.getCookie("time_picked") )
            return this.getCookie("time_picked");
        else
            return 0;
    },
    getTimeStateArray: function (  )
    {
         if( this.getCookie("time_state_array") )
            {
                tsa = this.getCookie("time_state_array");
               return tsa;
            }   
        else{
            default_ts_array = {'state':'None', 'timeFloor':new Date(  ), 'nowDisabled':false, 'nowHidden':false};
            return default_ts_array;
        }
    },

    format_date: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var date_string = self.getAlphaDay(date.getDay())+", "+self.getAlphaMonth(date.getMonth())+" "+date.getDate()+", "+date.getFullYear();
        return date_string;
    },

    format_time: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var time_string = this.pad( date.getHours(), 2) + ":" + this.pad( date.getMinutes(), 2);
        return time_string;
    },

    format_date_time: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date_string = this.format_date( d );
        time_string = this.format_time( d );

        return date_string + " " + time_string;
    },

 get_min_early_entry: function(  date_time )
{
    self = this;
    var open_time = this.getOpenTime(  );
    var now = new Date(  date_time );

    if( this.is_empty( date_time ) )
        return open_time;
    else
    {
        vs_open = this.compare_times( now, open_time  );
        if( vs_open == "<" )
            return open_time;
        else
            return now;
    }
},

 handle_user_time_pick_first_run: function( tsa )
{
    self = this;
    // check done
    // validate done
    // store doen
    // delete
    // set params
    console.log( "USER PICK HANDLER: " + this.getTimepicked(  ) );
    if( this.getTimepicked(  ) == 0 )
        return;

    console.log( "Checking validation of user time pick. " );
    valid_time_or_zero = this.get_valid_user_time_pick( tsa );

 //   console.log( "Checking validation of user time pick. " + valid_time_or_zero );
    this.validation_to_cookies( valid_time_or_zero );        // clear cookies, etc
    this.validation_to_global( valid_time_or_zero );
    return valid_time_or_zero;
},

 validation_to_global: function( valid_time_or_zero )
{
    if(  valid_time_or_zero != 0 )
        VALID_USER_PICKED_RECENTLY = valid_time_or_zero;
    else
        VALID_USER_PICKED_RECENTLY = 0;
},


 pick_the_actual_time_to_set_in_datetime_picker : function( time_state_array )
{
    self = this;
    floor_time = time_state_array.timeFloor;
    user_time = this.getUserTimeValidated(  );
    now_time = this.getCurrentDateTime(  );
    
 //   console.log( "Picking..." );
 //   console.log( user_time );

    // FOR DEBUG::
  //  user_time = 0;

    if( user_time != 0 ){
   //     console.log( "User time" );
        return user_time;
    }
    else
    {
        if( floor_time != 0 )
        {
            return this.noon_or_greater( floor_time );
        }
        else 
        {
     
            return this.noon_or_greater( now_time );
        }
            //return 0;    // no user time set. no floor time set.  send zero ( which will mean NOW )
    }
},
limit_datepicker_min_date: function ( time_state_mode, floorDate )
{
    self = this;
    if( time_state_mode =='Cater' || time_state_mode == 'ClosedCater' ||  time_state_mode == 'Closed') 
    {
        $(".delivery_date").datepicker('option', {minDate: new Date(floorDate)});
    }
    else
    {
        $(".delivery_date").datepicker('option', {minDate: 0});   
    }
},

limit_datepicker_on_closed_days: function(timeStateArray)
{
    self = this;
    var store_id = timeStateArray.store_id;
    self.getSpecificDayForStore(store_id);
    $.ajax({
        type: "POST",
        data: {
            "store_id": store_id
        },
        dataType:'json',
        url: SITE_URL + 'checkout/get_storeClosed_days',
        beforeSend: function() {
            $("#ajax-loader").show();
        },
        complete: function() {
            $("#ajax-loader").hide();
        },
        success: function(e) 
        {
            console.log(e)
            if(e != false)
            {
                console.log(e)
                $('.delivery_date').datepicker('option',
                {
                    beforeShowDay: function(d) 
                    {
                        var is_specficHoliday = self.disable_specific_dates(d);
                        if(is_specficHoliday){
                            var day = d.getDay();
                            return [e.indexOf(day) == -1];
                        }else{
                            return [false];
                        }
                        
                    }
                });
            }
        }
    });
},

disable_specific_dates: function( date )
{
   // console.log( "TEST - " + date );
    self = this;
    console.log(self.specific_day)
    if(  self.specific_day  == "" )
    {
        return 1;
    }

    var bad_date = new Date(  mobile_ha.common.specific_day );

    var m_bad = bad_date.getMonth();
     var d_bad = bad_date.getDate();
     var y_bad = bad_date.getFullYear();
     

    var m = date.getMonth();
     var d = date.getDate();
     var y = date.getFullYear();
     
     // First convert the date in to the mm-dd-yyyy format 
     // Take note that we will increment the month count by 1 
     var currentdate = (m + 1) + '-' + d + '-' + y ;
     var bad_date_formatted =  (m_bad + 1) + '-' + d_bad + '-' + y_bad ;
      // We will now check if the date belongs to disableddates array 
     if( currentdate == bad_date_formatted  )
     {
        return 0;
     }
     else
     {
        return 1;
     }
}, 



//// FIGURE OUT THE DATE AND TIME FUNCTIONS
// Entry function...

check_time_and_change_if_needed:  function ( force_show )
{
    self = this;
    var day_time_minumum = 0;
    
    this.getTimeAvilableSlots(  );
    var timeStateArray = this.pick_time_state( );
    var timeStateArray_now = this.pick_time_state_now();
    console.log("f1",timeStateArray)

    mode = timeStateArray['state'];
    //last_mode = LAST_TIME_PICK;
    
    this.setTimeStateArray( timeStateArray );      // This saves our state array that we built.

    this.handle_user_time_pick_first_run( timeStateArray  );                // This is first run.  Sets appropriate stuff if it works.

    // So now we have the valid user info, we have the floor.  Now what?  
    // Get the actual time to use.
    timeStateArray = this.last_chance_specific_day( timeStateArray );
    console.log("f2",timeStateArray)

    var mesg;
    if (timeStateArray.msg) 
    {
        mesg = timeStateArray.msg;
    }
    while(timeStateArray.closedTodayOverride) // if the store is closed for next days, add days to it.
    {

        timeStateArray = this.last_chance_specific_day( timeStateArray );

    }

    this.limit_datepicker_min_date( mode, timeStateArray.timeFloor );                  // Sets datepicker minimum date - either 0 for now or 1 for tomorrow.
    this.limit_datepicker_on_closed_days(timeStateArray);
    timeStateArray.msg = mesg;

    set_date_time = this.pick_the_actual_time_to_set_in_datetime_picker( timeStateArray );
    console.log("f3",set_date_time)

    if( this.getUserTimeValidated(  ) == 0 )
    {
        set_date_time = this.noon_or_greater( this.get_min_early_entry(  set_date_time ) );   //this makes sure our pick is not too early. 
    }
    else
    {
        timeStateArray['user_validated_time'] =this.getUserTimeValidated(  );
    }

    timeStateArray['set_date_time'] = set_date_time;
    timeStateArray_now['set_date_time'] = set_date_time;
    console.log("f4",timeStateArray)

    timeStateArray = this.figure_out_element_states( timeStateArray_now );

    timeStateArray = this.set_open_floor( timeStateArray );

    console.log( "%cTimestate: ", "color:red"  );
    //console.log( timeStateArray );
    console.log( "%cTime to set: " + this.format_date_time( set_date_time ) , "color:red" );

    if( force_show == 1)
    {
        timeStateArray['nowHidden'] = true;
    }
    timeStateArray = self.no_now_on_specific_day( timeStateArray  );
    this.set_time_in_html_element( timeStateArray );
    this.set_date_in_html_element( timeStateArray );
  
    console.log( "%cTime state: " , "color:red" );
    console.dir( timeStateArray );

    this.change_html_elements_for_time(  timeStateArray );
    if( timeStateArray.state == 'None' )
        this.limit_select_times_by_date(  this.getOpenTime(  ) );
    else
        this.limit_select_times_by_date(  timeStateArray.timeFloor );
    
    if (timeStateArray.msg) 
    {
        timeStateArray.msg = mesg;
    }
     //this.set_cater_time_estimate( timeStateArray );
    this.time_state_array = timeStateArray;
  
    setTimeout(function(){
        self.set_time_in_html_element( timeStateArray );
        self.set_date_in_html_element( timeStateArray );
    },500);  
},

 get_hours_regular : function(  )
{
    
    return new Date(  );
},


    last_chance_specific_day : function( timeStateArray )
    {
        self = this;
        timeStateArray.closedTodayOverride = 0;

        picked_date_vs_bad_date = self.compareDates( timeStateArray.timeFloor, self.specific_day  );
        
        
        if( picked_date_vs_bad_date == "=" )
        {
            timeStateArray.closedTodayOverride = 1;
            timeStateArray.timeFloor = self.add24Hours( timeStateArray.timeFloor );
        }

        var times = self.getOpenCloseFromDate( timeStateArray.timeFloor );
        var open_time = times.open_time;
        var close_time = times.close_time;
        var store_open_today = times.store_closed_on_day;
        

        // This is all basically to test if the store is closed today, and if the user picked today or not picked at all therefore default to today.
        var user_time_validated = this.getUserTimeValidated(  );
        var test_time_against = 0;
        if (  user_time_validated == 0 )
            test_time_against = 0;
        else
            test_time_against = times.open_time;


        if(  store_open_today == 0 &&
        ( user_time_validated == 0 ? 1 :  ( this.compareDates( user_time_validated , test_time_against ) == "=" ) ) )
        {
            timeStateArray.closedTodayOverride = 1;
            console.log( "Marking today as closed" );
            timeStateArray.timeFloor = self.add24Hours( timeStateArray.timeFloor );
            self.set_checkout_message( "Sorry, we're currently closed. Please choose a future order time." );
            timeStateArray.day_closed = times.day_of_week;
            timeStateArray.state = "Closed";
            timeStateArray.msg = "Sorry, we're currently closed. Please choose a future order time.";
            timeStateArray.nowDisabled = true;
        }

        // and do one now.
        var times = self.getOpenCloseFromDate( new Date(  ) );
        store_open_today = times.store_closed_on_day;
        if(  store_open_today == 0 &&
        ( this.compareDates( this.getUserTimeValidated(  ) , times.open_time ) == "="  ) )
        {
            timeStateArray.closedTodayOverride = 1;
            timeStateArray.msg = "Sorry, we're currently closed. Please choose a future order time.";
            timeStateArray.nowDisabled = true;
          }
       
        return timeStateArray;
    },
    no_now_on_specific_day: function( timeStateArray )
    {
        today_date_vs_bad_date = self.compareDates( new Date( ), self.specific_day  );
        console.log( "Comparison today: " + today_date_vs_bad_date );
         if(  today_date_vs_bad_date == "=" )
        {
            console.log( "Disabling NOW!" );
            timeStateArray.nowDisabled = true;
            timeStateArray.nowHidden = true;
        }
        return timeStateArray;
    },





 get_valid_user_time_pick: function ( timeStateArray )
{
    self = this;
    user_date = self.getSpecificDate(  );
    user_time = self.getSpecificTime(  );

    console.log( "UD: " + user_date);
    console.log( "UT: " + user_time );
    console.log( "Next..." )    ;

    if( self.is_empty( user_date ) == 1 || self.is_empty( user_time ) == 1 )
    {
       console.log( "VALIDATION FAILURE default" );
        validation = 0;
    }
    else
    {
        validation = self.validate_user_pick( user_date, user_time, timeStateArray );
        console.log( "VALIDATION SUCCESS: " + validation );
    }

    return validation;
},

 validation_to_cookies : function( valid_time_or_zero )
{
    self = this;
    // If we have a valid date then save it.
    self.setUserTimeValidated( valid_time_or_zero );
    
    // If it's not valid or it's time to clear this information.
    self.clearUserTimeTemp(  );
},



 validate_user_pick: function( date, time, timeStateArray )
{   
    var self = this;
    cookie_date = self.set_date_time( date, time );
   console.log( "THE USER DATE: " + self.format_date_time(  cookie_date  ) );
    
    date_cookie     = cookie_date;
    date_cookie     = cookie_date;
    var now_date = self.getCurrentDateTime( );
    var date_open;
    var date_closed;
    if( self.compareDates( date_cookie, new Date(  ) ) == '=' )
    {
        date_open       = self.getOpenTime(  );
        date_closed     = self.getClosedTime(  );
    }
    else
    {
        times = self.getOpenCloseFromDate( date_cookie );
        date_open = times.open_time;
        date_closed = times.close_time;
    }
    date_current    = self.getCurrentDayHour(  );
    date_floor      = timeStateArray.timeFloor;
    
    if(  self.is_empty( date_floor ) == 1 )
        date_floor = self.getOpenTime(  );

    vs_date_floor  =  self.compareDates( date_cookie, date_floor );
    vs_time_floor  =  self.compareTimes( date_cookie, date_floor );
    

    if( vs_date_floor == "<" )
        return 0;
    if( vs_date_floor == "=" )
    {
        if( vs_time_floor == "<" )
            return 0;
    }

    // first check if this is today or a future day...
    vs_current_date = self.compare_dates( date_cookie, date_current );
    vs_open_time = self.compareTimes( date_cookie, date_open );
    vs_closed_time = self.compareTimes( date_cookie, date_closed );

//    console.log( "%cTest current date ", "color:blue" );
    if( vs_current_date == "<" )        // This is before today, so not works. 
        return 0;
    if( vs_open_time == "<"/* || vs_open_time == "="*/)
        return 0;
    if( vs_closed_time == ">"/* || vs_closed_time == "="*/)
        return 0;

 
    var vs_current_time = '';
    if( vs_current_date == "=" )
    {
        vs_current_time = self.compareTimes( date_cookie, date_current );
        if( vs_current_time == "<" ){   // same day but before current time, so invalid. 
            return 0;}
    }

    if( vs_current_date == "?" || vs_open_time == "?" || vs_closed_time == "?" || vs_current_time == "?")
    {
        console.warn( "%cERROR in date validation", "color:teal; bakcground:black"  );
        return 0;
    }

    // If we get here, we are ok.
//    console.log( "%cWe are validated: " + self.format_date_time( cookie_date ) , "color:teal"  );
    return cookie_date;
},


 account_for_user_time_pick: function(  )
{
self = this;
    is_user_pick_valid  = 0;
    if( self.getTimepicked(  ) == 1 ) 
    {
        is_user_pick_valid = self.validate_user_pick( timeFloor );
        //is_user_pick_valid = 1;
    }
    //console.log( "Testing user time pick: " +is_user_pick_valid );

    adduserpick =0;
    if(  is_user_pick_valid == 1 )
    {
        nowHidden = true;
        adduserpick = 'UserPick';
    }
    
    if( adduserpick == 'UserPick' )
        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":adduserpick, "userTime": set_date_time( self.getSpecificDate(  ), self.getSpecificTime(  ) ) };
    else
        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0 };
},






    limit_select_times_by_date: function( floor_date_time, onLoad = false )
    {
        var self = this;
        //self.getTimeAvilableSlots(  );

        var picked_date = new Date( $( ".delivery_date" ).val(  ) );
        var changeToCurrentday = false;
        
        if(onLoad == true && self.compareDates( picked_date, new Date(  ) ) != '='){
            changeToCurrentday = true;
        }
        if( $('.timechange select').val()  == 'undefined' || $('.timechange select').val()  == undefined )
        {
            picked_date = this.set_date_time( picked_date, '08:00' );
        }
         else
         {
            picked_date = this.set_date_time( picked_date, $('.timechange select').val() );
         }
        
        var now_date = this.getCurrentDateTime( );
        var open_time;
        var close_time;

        var store_open_today = undefined;
        
        // if( this.is_empty( floor_date_time ) )
        //     floor_date_time = this.getOpenTime( );
        // var floor_date_time = new Date( floor_date_time );
        // console.log( "FLOOR: " + floor_date_time );
 

        if( self.compareDates( picked_date, new Date(  ) ) == '=' )
        {
            // TODAY
             open_time = self.getOpenTime(  );
             close_time = self.getClosedTime(  );
             if( self.compareTimes(  now_date, open_time ) == ">" )
             {
                var hour = now_date.getHours();
                var newDate = now_date.setHours(hour + 1);
                open_time = newDate;
             }
        }
        else
        {
             console.log( "The picked date is NOT actually today.  Get times for a specific date." );
             console.trace(  );
            times = self.getOpenCloseFromDate( picked_date );
              open_time = times.open_time;
              close_time = times.close_time;
              store_open_today = times.store_closed_on_day;
        }
        
        if(  store_open_today == 0 )
        {
            var curr_dt = new Date();
            var weekdays = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

            if(changeToCurrentday == true){
                times.day_of_week = weekdays[curr_dt.getDay()];
            }
            console.log( "FAIL THIS STORE IS CLOSED TODAY" );
            console.log( "setting the val blocker" );
            self.set_validation_blocker( "Sorry, we're currently closed. Please choose a future order time."  );
            self.set_checkout_message( "Sorry, we're currently closed. Please choose a future order time." );
            console.log( self.validation_blocked );
            return;
        }
        else
        {
            self.clear_validation_blocker(  );
        }
        
        console.log( "%cPICKED DATE: " + this.format_date_time( picked_date ), "color:red" );

        var timeStateArray  = this.time_state_array;

        if( floor_date_time == undefined ||  floor_date_time == 'undefined'  ||  floor_date_time == ''  )
        {
            if( self.compare_dates( picked_date, timeStateArray.timeFloor ) == "=" )
            {
                floor_date_time =  timeStateArray.timeFloor;   
            }
            else
            {
                floor_date_time = self.set_date_time( picked_date, self.format_time( open_time )  );
            }
        }
        else
        {
            if( timeStateArray.state == 'Cater' ||  timeStateArray.state == 'ClosedCater' )
            {
                // because if we are using catering, then we can't use the "getTimeStateArray" open time as a floor.  But we have already calc'd a floor so...
             
            }
            else{
                floor_date_time = self.set_date_time( floor_date_time, self.format_time( open_time )  );
            }
        }



        var floor_date_time = new Date( floor_date_time );
        console.log( "FLOOR: " + floor_date_time );
        //  console.log( "%cPICKED DATE: " + this.format_date_time( picked_date ), "color:red" );
        time_state_array_datetime = floor_date_time;

        if( picked_date.getHours(  ) == 0 )
        {
            picked_date = this.set_date_time( picked_date, this.format_time( floor_date_time )  );
        }
       
        if(  this.is_empty(floor_date_time) == 1 || floor_date_time == 'Invalid Date' )
        {
            console.log( "%cNo floor, return", "color:red" );
             picked_date = this.set_date_time( picked_date, '08:00' );
        }
        var self = this;

        if( this.compareDates( picked_date , floor_date_time ) == "<" )
        {
            this.set_html_date( floor_date_time );
            this.limit_select_times_by_date( floor_date_time  );
            picked_date = $( ".delivery_date" ).val(  );
        }
        else if( this.compareDates( picked_date , floor_date_time ) == "=" )
        {
            
            $(".timedropdown option").each(function()
            {
                var index_time = $(this).val();
                var index_date_time = self.set_date_time( picked_date, index_time );
                var vs_index_time_and_floor_time = self.compareTimes( index_date_time, floor_date_time );

                console.log( "%cTime in question: " + index_time +" Compared with floor: " + self.format_time( floor_date_time ) +
                             " Result: " + vs_index_time_and_floor_time + " Del on: < ", "color:red" );


                if( vs_index_time_and_floor_time == "<" )
                {
                    console.log( "Remove A - " + index_time + " is less than " + self.format_time( floor_date_time ));
                    $( this ).remove(  );
                }


                var vs_index_time_and_closed_time = self.compareTimes( index_date_time, close_time );
                 console.log( "%cTime in question: " + index_time +" Compared with close_time: " + self.format_time( close_time ) +
              " Result: " + vs_index_time_and_closed_time + " Del on: > ", "color:red" );

                if( vs_index_time_and_closed_time == ">" )
                {
                     console.log( "Remove B - " + index_time + " is less than " + self.format_time( close_time ));
                    $( this ).remove(  );
                }


                var vs_index_time_and_open_time = self.compareTimes( index_date_time, open_time );              
                 console.log( "%cTime in question: " + index_time +" Compared with open time: " + self.format_time( open_time ) +
                             " Result: " + vs_index_time_and_open_time + " Del on: < ", "color:red" );
                if( vs_index_time_and_open_time == "<" )
                {
                    console.log( "Remove B.2 - " + index_time + " is less than " + self.format_time( open_time ));
                     $( this ).remove(  );
                }
            });
        }

        else if( this.compareDates( picked_date , floor_date_time ) == ">" )
        {
            console.log( "%cPicked time is greater than floor date.", "color:red" );
            $(".timedropdown option").each(function()
            {
                var index_time = $(this).val();
                var index_date_time = self.set_date_time( picked_date, index_time );
               
                vs_index_time_and_open_time = self.compareTimes( index_date_time, open_time );
                console.log( vs_index_time_and_open_time )
                if( vs_index_time_and_open_time == "<" )
                {
                   console.log( "Remove C - " + index_time + " is < than " + self.format_time( open_time ) +  + " Del on: < ");
                  $( this ).remove(  );
                }

                vs_index_time_and_closed_time = self.compareTimes( index_date_time, close_time );
                console.log( vs_index_time_and_closed_time )
                if( vs_index_time_and_closed_time == ">" )
                    {
                  console.log( "Remove D - " + index_time + " is GREATER than " + self.format_time( close_time ));
                   $( this ).remove(  );
                }
            });
            
        }
        if( self.compareTimes(  close_time, now_date  ) == ">" ||  self.compareTimes(  close_time, now_date ) == "=")
        {
            newDateMin = this.date_plus_1( now_date );
            this.set_date_time( newDateMin, this.format_time( this.getOpenTime( ) ) );
        }
         $( 'select.timedropdown option' ).each( function(  )
        {
            if( $($(this)).val(  ) == "12:00" )
                $($(this)).prop( 'selected',true );
        } );
        
    },

    

// Figure out the base time. KEY FUNCTION.  This is the state manager function.  
pick_time_state : function (  )
{
    var store_closed = this.check_if_closed(  );
    var store_catered = this.checkBreadSimple(  );

    var currentTime = this.getCurrentDateTime(  );
    var storeOpenToday = this.getOpenCloseFromDate( currentTime );
    var storeID = storeOpenToday.store_id;

   if( store_closed  == 1 &&   store_catered == 1  )
    {
        timeFloor = this.get_hours_cater_and_closed(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'ClosedCater';
    }

    else if(  store_closed == 1 &&  store_catered  == 0  )
    {
        timeFloor = this.get_hours_closed(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'Closed';
    }

    else if( store_closed == 0 &&  store_catered == 1  )
    {
        timeFloor = this.get_hours_cater(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'Cater';
    }
    else
    {
        state = 'None';
        timeFloor = this.get_hours_regular(  );
        nowDisabled = false;
        nowHidden = false;
    }
    ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0, "store_id": storeID };
    return ret;
},
pick_time_state_now : function (  )
{
    var store_closed = this.check_if_closed_now(  );
    var store_catered = this.checkBreadSimple(  );

    var currentTime = this.getCurrentDateTime(  );
    var storeOpenToday = this.getOpenCloseFromDate( currentTime );
    var storeID = storeOpenToday.store_id;

   if( store_closed  == 1 &&   store_catered == 1  )
    {
        timeFloor = this.get_hours_cater_and_closed(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'ClosedCater';
    }

    else if(  store_closed == 1 &&  store_catered  == 0  )
    {
        timeFloor = this.get_hours_closed(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'Closed';
    }

    else if( store_closed == 0 &&  store_catered == 1  )
    {
        timeFloor = this.get_hours_cater(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'Cater';
    }
    else
    {
        state = 'None';
        timeFloor = this.get_hours_regular(  );
        nowDisabled = false;
        nowHidden = false;
    }
    ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0, "store_id": storeID };
    return ret;
},

   
pick_the_actual_time_to_set_in_datetime_picker: function( time_state_array )
{
    floor_time = time_state_array.timeFloor;
    user_time = this.getUserTimeValidated(  );
    now_time = this.getCurrentDateTime(  );
    
 //   console.log( "Picking..." );
 //   console.log( user_time );

    // FOR DEBUG::
  //  user_time = 0;

    if( user_time != 0 ){
   //     console.log( "User time" );
        return user_time;
    }
    else
    {
        if( floor_time != 0 )
        {
      //      console.log( "FLOOR TIME" );
            return this.noon_or_greater( floor_time );
        }
        else 
        {
     //          console.log( "NOW TIME" );
            return this.noon_or_greater( now_time );
        }
            //return 0;    // no user time set. no floor time set.  send zero ( which will mean NOW )
    }
},


 change_html_elements_for_time : function( time_state_array )
{
    //console.log( 'NOW HIDDEN: ' + time_state_array.nowHidden );
    //console.log( 'NOW DISABLED: ' + time_state_array.nowDisabled );
    var self = this;
    if( time_state_array.nowHidden == true )   // HIDE now.
    {
        this.show_date_radio(  );
        $('#front_page_delivery_time').text( this.format_date_time_for_display( time_state_array.set_date_time ) );
        if (navigator.userAgent.match(/(ipod|iPhone|iPad)/)) {
            $('#front_page_delivery_time').html( this.format_date_time_for_display( time_state_array.set_date_time ) );
        }

    }
    else        // SHOW now
    {
        this.show_now_radio(  );
        $("#enter_datetime_seelction").removeClass("first-time");
         $('#front_page_delivery_time').text( 'NOW' );
         if (navigator.userAgent.match(/(ipod|iPhone|iPad)/)) {
            $('#front_page_delivery_time').html( 'NOW' );
        }

    }

    if( time_state_array.nowDisabled == false )   // is active
    {
        $('#radio1').data('err-msg', '' ); //setter
        $('#radio1').data('data-off', 'ENABLE' ); //setter
    }
    else
    {

        $('#radio1').data('err-msg', time_state_array.msg ); //setter
        $('#radio1').data('data-off', 'DISABLE' ); //setter
    }
    //console.log( "DONE" );
    //
     console.log( "Testing message: " + time_state_array.msg );
     if( !time_state_array.user_validated_time  && ( self.specific_message && self.compareDates( self.specific_day, new Date(  ) ) == "="  ))
    {
        console.log( "Bad day vs today" );
        self.set_checkout_message( self.specific_message );
        $("#radio2").data( 'err-msg', self.specific_message  );
    }
    else if( time_state_array.user_validated_time  && ( self.specific_message && self.compareDates( self.specific_day, time_state_array.user_validated_time ) == "="  ))
    {
        console.log( "Bad day vs validated user" );
        self.set_checkout_message( self.specific_message );
        $("#radio2").data( 'err-msg', self.specific_message  );
    }
    else
    {
        if ( time_state_array.msg )
        {

            // possible messages: closed today, deliveries off, specific day ( these are hit earlier ), closed cater.
            if( self.compareDates( time_state_array.user_validated_time, time_state_array.timeFloor ) == ">"    )
            {

            }
            else
            {
                console.log( "BBB" );
                $("#radio2").data( 'err-msg', time_state_array.msg  ) ;
                self.set_checkout_message( time_state_array.msg );
             }
        }
        else
        {
            console.log( "CCC" );
            self.clear_checkout_message(  );
        }
    }
},

set_front_initial_run: function ( time_state_array )
{
    
    if( $('#time_select_front_page').text(  ) == 'NOW' ){
        if( this.is_empty( time_state_array.user_validated_time ) == 0 )
        {
            $('.checkout-msg-red').remove(); 
            $("div#enter_datetime_seelction h2").html( this.format_date_time_for_display( time_state_array.user_validated_time ) );
        }
        else
        {
            if( ( time_state_array.state == 'Closed' || time_state_array.state == 'ClosedCater' )  )
            {
                $('.checkout-msg-red').remove(); 
                var msg = "<span class='checkout-msg-red'>"+time_state_array.msg+"</span>";
                
                $("div#enter_datetime_seelction").addClass("first-time");
                $("div#enter_datetime_seelction h2").html(msg);
                setTimeout(function(){
                    $("div#enter_datetime_seelction").addClass("first-time")
                    $('.place-order-checkout').addClass('disabled');
                }, 500); 
            }
            
            else
            {
                $('.checkout-msg-red').remove(); 
                $("#enter_datetime_seelction").removeClass("first-time");
            }

    }
    
        }

        
},

set_open_floor : function( time_state_array )
{
    if(  this.is_empty( time_state_array.timeFloor ) )
        time_state_array.timeFloor = this.getOpenTime(  );
    return time_state_array;
},


 set_time_in_html_element:function(  time_state_array )
{
    this.set_time_html( time_state_array.set_date_time );
},


 set_date_in_html_element: function( timeStateArray )
{
    this.set_date_html(   timeStateArray.set_date_time );
},

set_date_html: function( newDate )
{
    d = new Date( newDate );
      $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', d ));
},


set_time_html : function( newTime )
{
    console.log(newTime)
    var d = new Date( newTime );
    var new_time = this.get_time_from_date(d)
    console.log( "%cSet new time: " + new_time , "color:green" );
    
    $('.timechange >select option[value="' + new_time + '"]').prop('selected', true);
    
    if( $(".timedropdown").val(  )  == null || $(".timedropdown").val(  )  == "null"){
    }

},

 get_time_from_date: function( date_time )
{
    if( typeof( date_time ) == "object" )
    {


        result = ( this.pad( date_time.getHours(  ), 2 ) + ":" + this.pad(date_time.getMinutes(  ),2) );
        return result;
    }
    else
    {
        var timeRegex = /([01]\d|2[0-3]):([0-5]\d)/;
        var match = timeRegex.exec( date_time );
        time = match[0];
        return time;
    }
},


 set_open_floor: function( time_state_array )
{
    if(  this.is_empty( time_state_array.timeFloor ) )
        time_state_array.timeFloor = this.getOpenTime(  );
    return time_state_array;
},

 figure_out_element_states:function( time_state_array )
{
    console.log("time state array ",time_state_array);
    var self = this;
    if(  time_state_array.state == "None" )
    {
        time_state_array.nowDisabled = false;
        if( this.getUserTimeValidated(  ) != 0 )
        {
            time_state_array.nowHidden = true;
        }
        else
        {
            time_state_array.nowHidden = false;
        }
    }
    else
    {
        time_state_array.nowDisabled = true;
        time_state_array.nowHidden = true;

        if( time_state_array.state == 'Cater' || time_state_array.state == 'ClosedCater'  )
            time_state_array.msg = 'Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you! ';

        if( time_state_array.state == 'Closed'  )
        {
            var msg = '';
            if( time_state_array.msg ){
                msg =  time_state_array.msg;
            }
            else{
                 var formated_open_time=this.format_time( this.getOpenTime(  ) );
                var formated_close_time=this.format_time( this.getClosedTime(  ) );
                
                if( this.compareTimes(  this.format_date_time( time_state_array.user_validated_time ), this.format_date_time( this.getOpenTime(  ) ) ) == ">" )
                {
                    // validated its early and not open yet.
                }
                else
                {
                    //var msg = "SORRY, WE&apos;RE CURRENTLY CLOSED. HOURS: " + this.tConvert(formated_open_time)  + " - " + this.tConvert(formated_close_time) + " <br/>SELECT A FUTURE ORDER TIME BELOW.</span>";
                    var msg = "Sorry, we&apos;re currently closed. <br/>Please choose a future order time.";
                }
            }
            time_state_array.msg = msg;
        }

        if( self.specific_message && self.compareDates( self.specific_day, new Date(  ) ) == "=" )
        {
               var msg = self.specific_message;
               time_state_array.msg =  self.specific_message;
        }

    }
    return time_state_array;

},

    //~checking if any 3-foot of 6-foot is existing in order. if there is then you get a TRUE else a FALSE.
    checkBreadSimple : function(){
        
        var self = this;
        var cateredBread = false

        $('.bread_type').each(function(){ 
        
            if( $(this).val() > 0) {  
                cateredBread = 1;
            } 
        });
        if( cateredBread == 1 )
            return 1;
        else
            return 0;
    },


    format_date: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var date_string = self.getAlphaDay(date.getDay())+", "+self.getAlphaMonth(date.getMonth())+" "+date.getDate()+", "+date.getFullYear();
        return date_string;
    },

    format_time: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 
        if(typeof d=="string")
        {
            if(d.indexOf("GMT ")){
                d=d.replace("GMT ", "GMT+");
                d=new Date(d);
            } 
        }
        date = new Date(  d  );
        var time_string = this.pad( date.getHours(), 2) + ":" + this.pad( date.getMinutes(), 2);
        return time_string;
    },

    get_am_pm_from_military_hour: function( hours )
    {
        if( hours >= 12 )
            return "PM";
        else
            return "AM";
    },


    format_date_time_for_display: function( d )
    {
         self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date_string = this.format_date( d );
        d = new Date( d );
        hours = d.getHours(  );
        min = d.getMinutes( );
        std_hours = this.military_hours_to_standard(  hours );
    
        ampm = this.get_am_pm_from_military_hour( hours );

            

                
        time_string = std_hours + ":" + this.pad( min, 2 ) + ampm;


        return date_string + " " + time_string;
    },

    format_date_time: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date_string = this.format_date( d );
        time_string = this.format_time( d );

        return date_string + " " + time_string;
    },

    compare_times: function( d1, d2 )
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date1 = new Date( d1 );
        date2 = new Date( d2 );

        date_comp_1 = new Date( date1 );
        date_comp_2 = new Date( date1 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2 = this.blank_time( date_comp_2 );

        date_comp_1.setHours( date1.getHours(  ) );
        date_comp_1.setMinutes( date1.getMinutes(  ) );
        date_comp_1.setSeconds( date1.getMinutes(  ) );
        date_comp_1.setMilliseconds( date1.getMinutes(  ) );

        date_comp_2.setHours( date2.getHours(  ) );
        date_comp_2.setMinutes( date2.getMinutes(  ) );
        date_comp_2.setSeconds( date2.getMinutes(  ) );
        date_comp_2.setMilliseconds( date2.getMinutes(  ) );

        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) == date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

//        console.log( "%cComparing TIMES - " + this.format_date_time( date_comp_1 ) + " vs " + this.format_date_time( date_comp_2 ) + " Result: " + result, "color:teal" );
        return result;
    },

    get_date_difference : function( start_date, end_date )
    {
        var date1 = new Date( start_date );
        var date2 = new Date( end_date ); //less than 1
        var start = Math.floor(date1.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var end = Math.floor(date2.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var daysDiff = end - start; // exact dates
        return daysDiff;
    },


    blank_time: function( d )
    {
        d = new Date(  d );
        d.setHours( 0 );
        d.setMinutes( 0 );
        d.setSeconds( 0 );
        d.setMilliseconds( 0 );
        return d;
    },

    compare_dates: function( d1, d2 )
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2  = this.blank_time( date_comp_2 );


        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

//        console.log( "%cComparing DATES - " + this.format_date( date_comp_1 ) + " vs " + this.format_date( date_comp_1 ) + " Result: " + result, "color:teal" );
        return result;
    },

    
    pad : function (num, size) 
    {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    },


    is_empty: function( thing )
    {
        if( thing == 'undefined'    ||
            thing == undefined      ||
            thing == null           ||
            thing == ''             ||
            thing === false )
            return 1;
        else
            return 0;
    },
    date_plus_1 : function( d )
    {
        var date = new Date( d );
        date.setDate( date.getDate() + 1 );
        return date;
    },

    getCurrentDateTime: function(  )
    {
        date = this.getCurrentDayHour(  );
        return date;
    },

     getCurrentDayHour: function(  )
    {
        date = this.getCurrentDateFromServer(  );
        date = new Date( date );
        return date;
    },

    getCurrentDateFromServer_NOW: function(  )
    {   
        $.ajax({
                    type: "POST",
                    url: SITE_URL + 'checkout/getCurrentHour/',
                    async: false,                   
                    success: function(data) {
                        currentDayHour = JSON.parse(data);
                    }
                });
                

                
                var currentHour = parseInt(currentDayHour.hour);
                var currentDay = parseInt(currentDayHour.day);
                var currentDate = currentDayHour.date_time_js;
                //console.log( "%cGot current date: " + currentDate, "color:green" );

                currentDate = currentDate.split(",");
                var current_date = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);
               // console.log( "%RETURNING current date: " + current_date, "color:green" );
                return current_date;
    },

    
    getCurrentDateFromServer: function(  )
    {   
        $.ajax({
                    type: "POST",
                    url: SITE_URL + 'checkout/getCurrentHour/',
                    async: false,                   
                    success: function(data) {
                        currentDayHour = JSON.parse(data);
                    }
                });
                
 
                var currentHour = parseInt(currentDayHour.hour);
                var currentDay = parseInt(currentDayHour.day);
                var currentDate = currentDayHour.date_time_js;

                currentDate = currentDate.split(",");
                var current_date = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);
                return current_date;
    }, 

    getCurrentTime: function (  )
    {
        if( this.getCookie("current_time") ){
            return JSON.parse(  this.getCookie("current_time") );
        }
        else
        {
            return this.getServerTime(  );
        }
        
    },

    getCurrentDayHour: function(  )
    {
        date = this.getCurrentDateFromServer(  );

        date = new Date( date );
        

//        console.log( "IMPORTANT!!! OVERRIDE HERE TO TEST STORE CLOSING!!" );
        //date.setHours( 20 );

        return date;
    },

    getCurrentDateTime: function(  )
    {
        date = this.getCurrentDayHour(  );
        return date;
    },
    
    double_check_if_closed_same_day_before_open: function(  )
    {
         openTime = this.getOpenTime(  );
         currentTime = this.getCurrentDateTime(  );
         if( this.compareDates( openTime, currentTime ) == '=' )
         {
            if ( this.compareTimes( currentTime, openTime ) == "<" )
            {
                return 1;
            }
         }
         return 0;
    },

    check_if_closed : function (  )
    {
        openTime = this.getOpenTime(  );
        closedTime = this.getClosedTime(  );
        currentTime = this.getCurrentDateTime(  );
        var hour = currentTime.getHours();
        var newDate = currentTime.setHours(hour + 1);
        // IMPORTANT SET LOCAL TIME OVERRIDE
        //currentTime.setHours( 11 );
        var storeOpenToday = this.getOpenCloseFromDate( currentTime );
        if(typeof openTime=="string"){
            if(openTime.indexOf("GMT ")){
                openTime=openTime.replace("GMT ", "GMT+");
                openTime=new Date(openTime);
            } 
        }
        if(typeof closedTime=="string"){
            if(closedTime.indexOf("GMT ")){
                closedTime=closedTime.replace("GMT ", "GMT+");
                closedTime=new Date(closedTime);
            } 
        }

        if(storeOpenToday.store_closed_on_day == 1)
        {
            if( this.compareTimes( new Date(newDate), openTime ) == '<' )
            {
                return 1;
            }

            if( this.compareTimes( new Date(newDate), closedTime ) == '>' || this.compareTimes( new Date(newDate), closedTime ) == "=" )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }
    },

    check_if_closed_now : function (  )
    {
        openTime = this.getOpenTime(  );
        closedTime = this.getClosedTime(  );
        currentTime = this.getCurrentDateTime(  );

        // IMPORTANT SET LOCAL TIME OVERRIDE
        //currentTime.setHours( 11 );
        var storeOpenToday = this.getOpenCloseFromDate( currentTime );
        if(typeof openTime=="string"){
            if(openTime.indexOf("GMT ")){
                openTime=openTime.replace("GMT ", "GMT+");
                openTime=new Date(openTime);
            } 
        }
        if(typeof closedTime=="string"){
            if(closedTime.indexOf("GMT ")){
                closedTime=closedTime.replace("GMT ", "GMT+");
                closedTime=new Date(closedTime);
            } 
        }
        if(storeOpenToday.store_closed_on_day == 1)
        {
            if( this.compareTimes( currentTime, openTime ) == '<' )
            {
                return 1;
            }

            if( this.compareTimes( currentTime, closedTime ) == '>' || this.compareTimes( currentTime, closedTime ) == "=" )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }
    },


 compareTimes: function( t1, t2 )
{
   if( this.is_empty( t1 ) == 1 )
        return undefined;

    if( this.is_empty( t2 ) == 1 )
        return undefined;

    date1 = new Date( t1 );
    date2 = new Date( t2 );

    date_comp_1 = new Date( date1 );
    date_comp_2 = new Date( date1 );

    date_comp_1 = this.blank_time( date_comp_1 );
    date_comp_2 = this.blank_time( date_comp_2 );

    date_comp_1.setHours( date1.getHours(  ) );
    date_comp_1.setMinutes( date1.getMinutes(  ) );

    date_comp_2.setHours( date2.getHours(  ) );
    date_comp_2.setMinutes( date2.getMinutes(  ) );

    var result = "";
    if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
        result = ">";

    else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
        result = "<";
    
    else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
        result = "=";
    
    else
        result = "?";

    return result;
},


compareDates: function( d1, d2 )
{
    if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2  = this.blank_time( date_comp_2 );

        date_comp_1.setHours( 0 );
        date_comp_1.setMinutes( 0 );
        date_comp_1.setSeconds( 0 );
        date_comp_1.setMilliseconds( 0 );

        date_comp_2.setHours( 0 );
        date_comp_2.setMinutes( 0 );
        date_comp_2.setSeconds( 0 );
        date_comp_2.setMilliseconds( 0 );


        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";
        
     
        return result;
},

get_hours_cater :function (  )
{
    floor = this.getTimeFloor_cater_only(  );
    return floor;
},

get_hours_closed :function (  )
{
    floor = this.getTimeFloor_closed_only(  );
    return floor;
},

get_hours_cater_and_closed :function (  )
{
    floor = this.getTimeFloor_closed_and_cater(  );
    return floor;
},


calcFloor: function ( type_closed )
    {

        current = this.getCurrentDayHour(  );
        console.log( "  ! CURRENT: " +current );
        
        if( type_closed =='closed_only' )
        {
            if( this.compareTimes(  current, this.getOpenTime( ) ) == "<" )
                    newDateMin =current ;
                else
                    newDateMin = this.date_plus_1( current );
        }
        else
        {
            newDateMin = this.date_plus_1( current );
        }
        console.log( "  ! PLUS 1: " +newDateMin );
        console.log( "OPEN TIME: " + this.getOpenTime( ) );
        console.log( "JUST THE TIME: " + this.format_time( this.getOpenTime( ) ) );
        newDateMin = this.set_date_time( newDateMin, this.format_time( this.getOpenTime( ) ) );
        console.log( "  ! NEXT OPEN TIME 1: " + newDateMin );
       
         return newDateMin;
},


test24HoursTooLate: function (  )
{
    closedTime = this.getClosedTime(  );
    currentTime = this.getCurrentDateTime(  );

 // IMPORTANT SET LOCAL TIME OVERRIDE
   // currentTime.setHours( 11 );
   
    //console.log(  compareTimes( currentTime, closedTime )  );
    // if it's closed, basically, then it's too late to set it 24 hours, need 2 days.
    if( this.compareTimes( currentTime, closedTime ) == '>' )
        return 1;
    else
        return 0;
},

getTimeFloor_closed_and_cater : function (  )
{
    // ADD SAT SUN CLOSED HERE.
    now = this.getCurrentDayHour(  );
    newDateMin  = now;
    
    if(  this.check_if_closed(  ) == 1 )
    {
        newDateMin = this.calcFloor(  );
    }
    
    var check_24_hours_too_late = this.test24HoursTooLate(  );
    if( check_24_hours_too_late == 1 )
    {
        newDateMin = this.date_plus_1( newDateMin );
    }
    else
    {
        // 24 hours from now is ok. since we are already tomorrow, set the time then for now, which would be 24 hours. 
         newDateMin = this.set_date_time( newDateMin, now );
    }
    
    newDateMin = this.roundMinutes( newDateMin  );
    return newDateMin;
},


getTimeFloor_closed_only: function (  )
{
    // ADD SAT SUN CLOSED HERE.
    newDateMin = this.getCurrentDayHour(  );
    //console.log( "New date min: " + newDateMin );
    if(  this.check_if_closed(  ) == 1 )
    {
        if( this.double_check_if_closed_same_day_before_open(  ) == 1 )
        {
            newDateMin = this.getOpenTime(  );
        }
        else
        {
            newDateMin = this.calcFloor(  );
        }
        //console.log( "New min: " + newDateMin );
    }
       
    newDateMin = this.roundMinutes( newDateMin  );
    //console.log( "Rounded: " + newDateMin );
    return newDateMin;
},


getTimeFloor_cater_only: function (  )
{
    newDateMin = this.getCurrentDayHour(  );
    
    if(  this.check_if_closed(  ) == 0 )
    {
        newDateMin = this.add24Hours( newDateMin );
    }
    else
    {
        return this.getTimeFloor_closed_and_cater(  )
    }
       
    newDateMin = this.roundMinutes( newDateMin  );
    return newDateMin;
},


 roundMinutes: function( dateObject )
{
    now = dateObject;
    var mins = now.getMinutes();
    var quarterHours = Math.round(mins/15);
    if (quarterHours == 4)
    {
        now.setHours(now.getHours()+1);
    }   
    var rounded = (quarterHours*15)%60;
    now.setMinutes(rounded);
    return now;
},

add24Hours : function( date )
{
    twenty_four_hours_later = new Date( date.getTime() + 60 * 60 * 24 * 1000 );   
    twenty_four_hours_later = this.roundMinutes( twenty_four_hours_later  );
    return twenty_four_hours_later 
},

 dateAdd: function( d, daysToAdd )
{
    if( !daysToAdd )
        daysToAdd = 1;
            
    result = new Date( d );
    result.setDate( result.getDate() + daysToAdd );
    return result;
},
tConvert: function (time)
      {
     
       // Check correct time format and split into components
       time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) 
        { // If time format correct
            
          time = time.slice (1);  // Remove full string match value
          time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
          time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        //return time; // return adjusted time or original string
          return time[0] + ":" + time[2] + time[5];
      },
 get_all_possible_times :function(  )
    {
        buffer = '';
       for( hour = 8; hour < 24; hour ++ )
       {
            for( minute = 0; minute <60; minute = minute + 15  )
            {
                time_text = this.pad( hour, 2 ) + ":" + this.pad( minute, 2);
                time_new = this.tConvert( time_text );
                buffer = buffer + '<option value="' + time_text  + '">' +  time_new +  '</option>';
            }
       }
       return buffer;
     }

}

$(document).ready(function(){ 
    mobile_ha.common.init();
     if(window.location.href.indexOf('sandwich/gallery') != -1 ){
        var el = document.querySelectorAll('ul.menu-listing li img');
        var observer = lozad(el);
        observer.observe();
    }
});

$(window).load(function(){
    $('.pagelogo').fadeOut(500,function(){ 
        $('.page-contents').fadeIn(500);
        $('.js .slicknav_menu').fadeIn(500);
    }); 
    
    //on page refresh on checkout page if a valid coupon code in cookie it will be reapplied
    $discode = mobile_ha.common.getCookie("discount_code");
    if(window.location.href.indexOf('checkout') != -1 && $discode){
         mobile_ha.common.applyDiscount($discode)
    }
    
});

var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

 

$(document).ready(function(){
     var itemid;
    
        $( '.close-button' ).click( function( e  )
                {
                     $('.sandwich-popup').hide(  ); 
                });

        if (navigator.userAgent.match(/(ipod|iPhone|iPad)/)) {
            $('.sand-pop').click(function(e) {
                itemid = $(this).parent().attr('id');           
            });

            $('.common_add_item_cart').click(function(e) {
                e.preventDefault();  
                itemid = $(this).parent().parent().attr('id');                  
            });

            $('.summary-details-add-menu .close-button, .add-to-cart, .add_desc, .add-cart').click(function(e) {
                e.preventDefault();
                var data = $(this).data();
                
                
                
                $('body').removeClass('posfix');
                if($("#"+itemid ).length){
                    offsetItm = $("#"+itemid ).offset();                
                }
                     
            });
        }
    
});





$(window).on('load',function(){ 
    
    setTimeout(function(){  
    mobile_ha.common.shareCookieClear();
    },600);
    if(window.location.href.indexOf("checkout") != -1){ 
        mobile_ha.common.ResumeFromOrderState();  
    }
});
    
        Date.prototype.withoutTime = function () {
            var d = new Date(this);
            d.setHours(0, 0, 0, 0, 0);
            return d
        }
          $(document)
    .on('focus', 'select', function() {
        
          $('.slicknav_menu').addClass('slicknav_menu_sub');
        $('.header_wrapper').addClass('header_wrapper_sub');
         $('.create-sandwhich-menu').addClass('create-sandwhich-menu_sub');
        
    })
    .on('blur', 'select', function() {
        
        $('.slicknav_menu').removeClass('slicknav_menu_sub');
        $('.header_wrapper').removeClass('header_wrapper_sub');
         $('.create-sandwhich-menu').removeClass('create-sandwhich-menu_sub');
         
    });

