// mobile new which is mobile web
mobile_ha.sandwichcreator = window.mobile_ha.sandwichcreator || {
    vars: {
        temp_id: 0,
        cheese_min: 0,
        AjaxURL: {
            BREAD: "createsandwich/load_bread_data/",
            PROTEIN: "createsandwich/load_protien_data/",
            CHEESE: "createsandwich/load_cheese_data/",
            TOPPINGS: "createsandwich/load_toppings_data/",
            CONDIMENTS: "createsandwich/load_condiments_data/"
        },
        scenes: ['BREAD', 'PROTEIN', 'CHEESE', 'TOPPINGS', 'CONDIMENTS'],
        manual_mode: false,
        current_sliced_image: null,
        ACTIVE_MENU: $($('.sandwhich-menu li a')[1]).text().replace("Choose ", "").toUpperCase(),
        selection_data: null,
        validators: ["BREAD", "PROTEIN", "CHEESE"],
        bread_type: null,
        image_path: ADMIN_URL + 'upload/',
        current_price: 0,
        individual_price: {},

        ingredient_details: {
            BREAD: {
                item_name: [],
                item_price: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                type: 0, // either 3 foot or 6 foot or normal [0-normal,1-3-foot,2-6-foot]
                shape: 0, // either long,round,trapezoid [0-long,1-3-round,2-6-trapezoid]
                actual_price: {}
            },
            PROTEIN: {
                // manual_mode: false,
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            CHEESE: {
                // manual_mode: false,
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            TOPPINGS: {
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            CONDIMENTS: {
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            }
        }
    },

    init: function() {
        var self = this;
        self.enable_nav_buttons();
        self.user_selections();

        // DM This fixes #617
        $("#optionList input").each( function( )
        {
            this.checked=false;
        });

        if (window.location.href.indexOf("createsandwich") != -1) {            
            self.pageLoadEvents();          
        }else{
            self.quickEditLoadEvents();
            return false;
        }
        self.getRandomSandwichName();
    },
    createSandwichPageLoaded : 0,
    pageLoadEvents: function(){
        var self = this;
            $(window).load(function() {
                
                var response = self.retrive_data_onload();
                
                if(response != true) window.location.reload();
                    
            });
            
            $(".doneSandwich").unbind("click").on("click", function() {
                mobile_ha.common.setCookie('doneSandwich', true, 1);
                error = self.validate($(this).text());
                if (error == true) {
                    return false
                } else {
                    if (SESSION_ID == "" || SESSION_ID == null) {
                        //Set cookie value if not logged in. After login it helps to go to create sandwich last page.
                        mobile_ha.common.setCookie('sandwich_end_screen', true, 1);

                        session = mobile_ha.common.check_session_state();
                        if (session.session_state == false) {
                             if(session.facebook_only_user == false){
                               mobile_ha.common.login_popup();
                             } else {
                               mobile_ha.common.facebook_login();
                             }
                        }
                    } else
                        self.show_end();
                }
            });
            
            //Start Over will clear the temp session inputs of created sandwich
            $(document).on("click",".start-over", function(){
                $(this).text('restarting...');
                $.ajax({
                    type: "POST",                               
                    url: SITE_URL + 'createsandwich/delete_temp_session_input/',
                    success: function(e) {  
                        
                        randno  = Math.round( Math.random() * 10000000 );
                        accounturl = window.location.href;
                        if(accounturl.indexOf('pid') != -1){
                            array = accounturl.split('pid');
                            accounturl = array[0]+"pid="+randno;
                        } else {
                            
                            accounturl = accounturl+"index/?pid="+randno;
                            //alert(accounturl);
                        }
                        
                            window.location = accounturl;
                    }
                });
                

                
            });
    },

    /*
     * Clear all temp session data for  the sandwich, including bread, protien selections. prices etc. 
     */
    
    clear_temp_session : function(){
        
        var self = this;
        var ret  = false; 
        $.ajax({
            type: "POST",
            async:false,
            url: SITE_URL + 'createsandwich/delete_temp_session_input/',
            success:function(){  ret = true; },
            });
        return ret;
    },
    
    //~sandwich sider on home page
    set_sandwich_maker_slider: function() {
        var i           = 0; 
        var j           = 0;
        var self        = this;
        var sliderSpans = $('.homepageAutoBanner span');
        var spanCount   = sliderSpans.length - 1;
        
        var interval    = setInterval(function() {
            
            if(i <= spanCount){             
                
                siders   = sliderSpans[i];
                imgs     = $(siders).find('img');
                imgs     = $(imgs);
                imgCount = imgs.length - 1;
                
                  if(j <= imgCount){
                      
                      $('.homeAnimationLoaderImg').hide(10,function(){
                          $(imgs[1]).delay(700).fadeIn(500);
                      });
                      
                      $(imgs[j]).fadeIn(500);
                      j++;
                  } else {
                      j=0;
                      $(siders).find('img').delay( 1800 ).fadeOut(500);
                      i++;
                  }
                  
                  
                 
            } else {
                i = 0;
                $(sliderSpans.find('img')[0]).fadeIn(500)
            }
            
            
        },2000);
        
        
        

    },
    //This method loads the temp session selection once reload or come to page
    retrive_data_onload: function() {
        var self = this;
        /*var ID = 0;
        urlArray = window.location.href.split('/');
        if(urlArray.length >= 6){
            if(urlArray[5]) ID = urlArray[5]; else ID = DATA_ID;
        }  else {
            ID = DATA_ID;
        }*/
        $.ajax({
            type: "POST",
            data: {
                'id': DATA_ID,
                'uid': SESSION_ID,
                'source': 'edit'
            },
            url: SITE_URL + 'createsandwich/get_sandwich_data/',
            dataType: 'json',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
                if (self.vars.ingredient_details["BREAD"].item_name.length == 0) self.auto_selection();
                if (self.vars.ingredient_details["BREAD"].item_name.length > 0) {                     
                    $('.button-holder').show();
                    $('.homecontent').remove();                    
                }
            },
            success: function(e) {
                                
                self.auto_selection();
                if (e.Response['status-code'] == 200) {
                    if (e.Data[0].id) {
                        self.temp_id = e.Data[0].id;
                        self.temp_name = e.Data[0].sandwich_name;

                    } else {
                        self.temp_id = '';
                    }
                    $sanwch = e.Data[0].sandwich_data;
                    price = parseFloat(e.Data[0].sandwich_price);
                    if ($sanwch) {
                        json_data = JSON.parse($sanwch);

                        if (json_data) {
                            self.vars.ingredient_details = json_data;

                            self.load_all_data(json_data,true);
                            self.reload_state();
                        }
                    }
                    
                    if (mobile_ha.common.getCookie('sandwich_end_screen')=='true') {
                        self.show_end();
                    }

                }


            }
        });

        self.enable_li_click();
        return true;
    },

    retrive_data_on_quickEdit: function(ID) {

        mobile_ha.sandwichcreator.vars = {
            temp_id: 0,
            cheese_min: 0,
            AjaxURL: {
                BREAD: "createsandwich/load_bread_data/",
                PROTEIN: "createsandwich/load_protien_data/",
                CHEESE: "createsandwich/load_cheese_data/",
                TOPPINGS: "createsandwich/load_toppings_data/",
                CONDIMENTS: "createsandwich/load_condiments_data/"
            },
            manual_mode: false,
            current_sliced_image: null,
            ACTIVE_MENU: $('.create-sandwich-menu ul li a[class="active"]').text(),
            selection_data: null,
            validators: ["BREAD", "PROTEIN", "CHEESE"],
            bread_type: null,
            image_path: ADMIN_URL + 'upload/',
            current_price: 0,
            individual_price: {},

            ingredient_details: {
                BREAD: {
                    item_name: [],
                    item_price: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    type: 0, // either 3 foot or 6 foot or normal [0-normal,1-3-foot,2-6-foot]
                    shape: 0, // either long,round,trapezoid [0-long,1-3-round,2-6-trapezoid]
                    actual_price: {}
                },
                PROTEIN: {
                    // manual_mode: false,
                    item_name: [],
                    item_price: [],
                    item_priority: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    actual_price: {},
                    option_default_Choice_first: 1,
                    option_default_Choice_second: 0,
                },
                CHEESE: {
                    // manual_mode: false,
                    item_name: [],
                    item_price: [],
                    item_priority: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    actual_price: {},
                    option_default_Choice_first: 1,
                    option_default_Choice_second: 0,
                },
                TOPPINGS: {
                    item_name: [],
                    item_price: [],
                    item_priority: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    actual_price: {},
                    option_default_Choice_first: 1,
                    option_default_Choice_second: 0,
                },
                CONDIMENTS: {
                    item_name: [],
                    item_price: [],
                    item_priority: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    actual_price: {},
                    option_default_Choice_first: 1,
                    option_default_Choice_second: 0,
                }
            },
            
            appliedDiscount: 0
        }

        console.log("ID", ID);
        DATA_ID = ID;
        var self = this;

        $.ajax({
            type: "POST",
            data: {
                'id': ID,
                'uid': SESSION_ID,
                'source': 'quick_edit'
            },
            url: SITE_URL + 'createsandwich/get_sandwich_data/',
            dataType: 'json',
            beforeSend: function() {
                
            },
            complete: function() {
                $("#ajax-loader").hide();
                if (self.vars.ingredient_details["BREAD"].item_name.length == 0) self.auto_selection();
                if (self.vars.ingredient_details["BREAD"].item_name.length > 0) {
                     $("#sandwiches-popup").hide();
                }
            },
            success: function(e) {

              //  console.log("e", e);
                self.auto_selection();
                if (e.Response['status-code'] == 200) {
                    if (e.Data[0].id) {
                        self.temp_id = e.Data[0].id;
                        self.temp_name = e.Data[0].sandwich_name;

                    } else {
                        self.temp_id = '';
                    }
                    $sanwch = e.Data[0].sandwich_data;
                    price = parseFloat(e.Data[0].sandwich_price);
                    if ($sanwch) {
                        json_data = JSON.parse($sanwch);

                        if (json_data) {  
                            $keyz = Object.keys(json_data);
                            
                            $keyz.forEach(function(e){
                                $kys = Object.keys(json_data[e]);
                                $kys.forEach(function(k){ 
                                    self.vars.ingredient_details[e][k] = json_data[e][k];
                                 });
                            });
                            console.log("self.vars", self.vars);
                            
                            self.load_all_data(json_data,true);
                            self.reload_state_quick_edit();
                        }
                    }
                }
            }
        });
    },

    auto_selection: function() {
        var self = this;
        if (self.get_url_params('view') != null) {
            switch (self.get_url_params('view')) {

                case '3foot':
                    $3foot = $('#optionList input[data-bread_type="1"]');
                    if ($3foot.length == 0) {
                        alert("Please add a 3 foot bread!");
                        window.location.href = SITE_URL + 'catering/';
                    } else {
                        $3foot.trigger('click');
                        self.vars.ingredient_details["BREAD"].type = 1;
                        $('.homecontent').remove();
                    }
                    break;
                case '6foot':
                    $6foot = $('#optionList input[data-bread_type="2"]');

                    if ($6foot.length == 0) {
                        alert("Please add a 6 foot bread!");
                        window.location.href = SITE_URL + 'catering/';
                    } else {
                        $6foot.trigger('click');
                        self.vars.ingredient_details["BREAD"].type = 2;
                       
                        $('.homecontent').remove();
                    }
                    break;
            }
        }
    },

    get_url_params: function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        return results == null ? null : results[1];
    },

    cfunWord : null,
    getRandomSandwichName : function(){
        var self = this;
        if(window.location.href.indexOf("createsandwich") != -1){ 
        $.ajax({
            type: "POST",
            async: false,
            url: SITE_URL + 'createsandwich/getRandom_sandwich_name/',
            success: function(e) {
                self.cfunWord = e;
            }
        }); }
        
    },
     
    show_end: function() {

        var self = this;
        mobile_ha.common.setCookie('sandwich_end_screen', true, 1);
        var funWord = self.cfunWord;
   

        session = mobile_ha.common.check_session_state();
        if (session.session_state == false) {
             if(session.facebook_only_user == false){
               mobile_ha.common.login_popup();
             } else {
               mobile_ha.common.facebook_login();
             }
            return;
        }

        $rand = self.randomBetween(0, self.vars.ingredient_details.PROTEIN.item_name.length);


        var $prot;

        proteins = self.vars.ingredient_details.PROTEIN.item_name;
        cheeses = self.vars.ingredient_details.CHEESE.item_name;

        $noFirstName = false;

        if (proteins.length > 2) {
            $noFirstName = false;
            $sandwh = 'Protein'
        } else if (proteins.length == 2) {
            $noFirstName = true;
            $sandwh = proteins[0] + ' & ' + proteins[1];

        } else if (proteins.length == 1) {
            $noFirstName = false;
            $sandwh = proteins[0];
        } else if (proteins.length == 0) {

            if (cheeses.length > 2) {
                $sandwh = 'Cheesy';
                $noFirstName = false;
            } else if (cheeses.length == 2) {
                $sandwh = cheeses[0] + ' & ' + cheeses[1];
                $noFirstName = true;
            } else if (cheeses.length == 1) {
                $noFirstName = false;
                $sandwh = cheeses[0];
            }
        }

        if (proteins.length == 0 && cheeses.length == 0) {
            $sandwh = 'Veggie';
        }

        $sandwichName = $('#namecreation').val();
        $sandwichName_split = $sandwichName.split(' ');
        $ln = $sandwichName_split.length - 1;

        $last = $sandwichName_split[$ln];

        $first = mobile_ha.common.getCookie('user_fname');
        if ($first[0]) $first = $first[0].toUpperCase() + $first.slice(1) + "'s";
        else $first = '';



        if (funWord) $last = funWord;
        $sandwichNamePart2 = $sandwh + " " + $last;
        $nameLen = parseInt($first.length) + parseInt($sandwichNamePart2.length);


        if ($noFirstName == false && $nameLen < 35 && $first) $sandwichName = $first + " " + $sandwichNamePart2
        else $sandwichName = $sandwichNamePart2;
        
        $sandwichName = $sandwichName.toUpperCase();
        
        $('#namecreation').val($sandwichName);

        $('#namecreation').removeAttr('readonly');
        $('#namecreation').attr('style','background-color:#D8AE84');
        $('a.save-to-my-menu').focus();

        $(".create-navigation").hide();
        $(".create-screen").hide();
        $('.finalizeScreen').show();

        $(".edit-button").unbind("click").on("click", function(e) {
            e.preventDefault();
            $(".create-screen").show();
            $('.finalizeScreen').hide();
            $(".create-navigation").show();
        });

        $('.summary').unbind('click').on('click', function(e) {
            e.preventDefault();
            $('.summary_details').html(self.prepare_endScreen());
            $('#summary').show();
            mobile_ha.common.body_scroll_ops(false);
        });
        
        $('#namecreation').unbind("click").on("click",function(){
            $(this).removeAttr('readonly');
            $(this).attr('style','background-color:#D8AE84');
        });
        
        
        $('#namecreation').on("blur",function(){
            $(this).attr('style','background-color:transparent');
            
        }); 
        

        $('.save-to-my-menu,.addtoCart').not('#finished').unbind('click').on('click', function(e) {
            e.preventDefault();
            
            if ($(this).hasClass('disable') && $(this).hasClass('save-to-my-menu')) 
            {
                alert("This item is already saved!");
                return false;
            }
            if ($(this).hasClass('disable')) {
                alert("Adding to cart is in progress please wait!");
                return false;
            }



            var active;
           
            if ($(e.target).attr('class') == 'save-to-my-menu') {
                active = 1;
                retchk = false;
            } else if ($(e.target).attr('class') == 'addtoCart') {
                active = 1;
                retchk = true;
            } else {
                active = 1;
                retchk = true;
            }
            is_private = $('#check2:checked').length;

            is_pub = is_private == 1 ? 0 : 1;
            name_cr = $('#namecreation').val();
            name_cr = name_cr.toUpperCase();

            self.sync_data_to_db(is_pub, name_cr, active, self.temp_name, retchk,  $(this), '', '');

        });


    },
    
    
    get_desc_from_sandwich : function(){
        
        var self = this;
        itmArray = [];
        ings = self.vars.ingredient_details;
        kys  = Object.keys(ings);
        kys.forEach(function(e){
         if(e){
             
            items =  self.vars.ingredient_details[e].item_name
            
            items.forEach(function(i){
             
                if(self.vars.ingredient_details[e].item_qty[i] && self.vars.ingredient_details[e].item_qty[i].length){
                  qty = self.vars.ingredient_details[e].item_qty[i];    
                  itmArray.push(i+"("+qty[1]+")");
                } else {
                    itmArray.push(i);
                }
            });
            
         }      
            
        });
        
         
        return itmArray.join(", ");     
        
    },
    
    //Adding sandwich to My Menu
    add_to_sandwich_menu : function(id){
        
        var menuAdd = false;
        
        $.ajax({
            type: "POST",
            data: {
                'id': id,
                "uid": SESSION_ID
            },
            async : false,
            url: SITE_URL + 'sandwich/add_menu_multi/',
            success: function(e) {
               menuAdd = true;
            }
        });
        
        return menuAdd;
    },

    prepare_endScreen: function() {
        $out = "";
        $i = 0;
        var self = this;
        self.calculate_price();
        $data = self.vars.ingredient_details;

        Object.keys($data).forEach(function(e) {

        if (e != "BREAD") {
        }

            if (self.vars.individual_price[e] < 0) sign = '-';
            else sign = '+';
            vals = Math.abs(self.vars.individual_price[e]);
           $price = vals.toFixed(2);
            
            if (e == "BREAD") {
                if($price >= parseInt(BASE_FARE)){
                    $price = $price; 
                } else {
                        $price = $price; 
                }
                if(self.vars.ingredient_details["BREAD"].type == 0)
                $out += '<li style="border-bottom: 1px solid;margin-bottom: 8px;list-style: none;    display: block;height: 28px;"><h4 style="text-decoration: none !important;">BASE PRICE <span>+$'+BASE_FARE+'.00</span></h4></li>';
            }
            $price = parseFloat($price);
            $price = $price.toFixed(2);
                                    
            $out += "<li>";
            $out += "<h4>" + e + " <span>" + sign + "$" + $price + "</span></h4>";

            if (e == "BREAD") {
                $out += "<p>" + $data[e].item_name[0] + "</p>"
            } else {
                $sub = "";
                $en_ln = $data[e].item_name.length;
                $qty = $data[e].item_qty;


                $data[e].item_name.forEach(function(e, i) {

                    if (!e) {
                        $final_name = "None"
                    }

                    if ($qty[e] != undefined) {
                        if (e != "NULL") $final_name = e + " (" + $qty[e][1] + ")";
                        else $final_name = "None"
                    } else {
                        if (e != "NULL") $final_name = e + " (1)";
                        else $final_name = "None"
                    }

                    if (i < $en_ln - 1) {
                        delimit = ", "
                    } else {
                        delimit = ""
                    }

                    $sub += $final_name + delimit + " "
                });

                if ($data[e].item_name.length > 0) {
                    $out += "<p>" + $sub + "</p>";
                } else {
                    $out += "<p>" + "None" + "</p>";
                }

            }
            $out += "</li>"
        });
        return $out
    },

    sync_data_to_db: function(is_pub, sandwichname, menu_active, tempname, returnid, thisItm, from, is_saved) {
        
        var self = this;
        var toast = 0;
        if ($(".menucheck").is(':checked')) {
            toast = 1;
        }
        var self = this;
        var state;
       
       is_saved = is_saved != '' ? is_saved : "SAVE"; 

        if (!sandwichname) {
            alert("Name your sandwich and continue!");
            return false;

        } else {

            $.ajax({
                type: "POST",
                data: {
                    'word': sandwichname,
                },
                async: false,
                url: SITE_URL + 'createsandwich/filter_words/',
                dataType: 'json',
                success: function(e) {
                    jso = JSON.parse(e);
                    state = jso.Response["status-code"];
                }
            });

            if (state == 500) {
                alert("Sorry, this name is not allowed due to inappropriate content.  Please try again.");
                return false;
            }

        }        
        
        
        if (thisItm) {
            if(returnid == true){ 
                if(from == 'quick_edit') thisItm.text('Saving...');
                else thisItm.text('Adding to cart...');
            } else {
                thisItm.text('Adding to my menu...');
            }
            
            thisItm.addClass('disable');
        }
        var items_names = [];
        
        $.each(self.vars.ingredient_details, function(k, ingredient ) 
        {
            if(ingredient.item_name == '' && k == 'PROTEIN')
            {
                items_names = items_names.concat("No Protein");
            }
            else if(ingredient.item_name == '' && k == 'CHEESE')
            {
                items_names = items_names.concat("No Cheese");
            }
            else if(ingredient.item_name == '' && k == 'TOPPINGS')
            {
                items_names = items_names.concat("No Toppings");
            }
            else if(ingredient.item_name == '' && k == 'CONDIMENTS')
            {
                items_names = items_names.concat("No Condiments");
            }
            else
            {
                items_names = items_names.concat(ingredient.item_name);
            }
            
        })
        items_names = items_names.toString();
        
        user_data = JSON.stringify(self.vars.ingredient_details);
        if (tempname != sandwichname) {
            tempname = 1;
        } else {
            tempname = 0;
        }
        

        var $sandid = 0;
        
    if ($('.share-sandwich').attr('rel')) {
            
            if (returnid == "share") {
                $sandid = $('.share-sandwich').attr('rel');
            } else if (returnid == true) {
                
                $sandid  = $('.share-sandwich').attr('rel');        
                self.add_sandwich_to_cart($sandid, SESSION_ID, "user_sandwich");
                
                window.open(SITE_URL + 'checkout', '_self');

            } else {
                
                window.open(SITE_URL + 'menu', '_self');
            }

    } else {

        if (SESSION_ID == "" || SESSION_ID == null) {
            SESSION_ID=mobile_ha.common.getCookie('user_id');
        }
        
        $.ajax({
            type: "POST",
            data: {
                'tempname': tempname,
                'menu_active': menu_active,
                'id': self.temp_id,
                'uid': SESSION_ID,
                'data': user_data,
                'items_names': items_names,
                'price': self.vars.current_price,
                'is_pub': is_pub,
                'name': sandwichname,
                'toast': toast,
                'is_saved' : is_saved
            },
            async: false,
            url: SITE_URL + 'createsandwich/user_input/',
            dataType: 'json',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) {
                if (e.Response['status-code'] == 200 || e.Response['status-code'] == 201) {
                    if(from != 'quick_edit') self.destroy_endscreen_var();

                    if (e.Data) {
                        $id = e.Data;
                    }else {
                        $id = DATA_ID;
                    }

                    imgStore = self.create_sandwitch_image(463, 344, $id, from);

                    if (imgStore == true) {
                        
                        if (returnid == 'share') {

                            $sandid = $id;
                        } else if (returnid == true) {
                            if(from == 'quick_edit') {
                                location.reload(true);
                                // randno  = Math.round( Math.random() * 10000000 );
                                // refreshurl = window.location.href;
                                // array = refreshurl.split('pid');
                                // refreshurl  = array[0]+"pid="+randno;
                                // window.location = refreshurl;
                            }else{
                                self.add_sandwich_to_cart($id, SESSION_ID, "user_sandwich");
                                window.open(SITE_URL + 'checkout/', '_self');
                            } 
                        } else {

                             window.open(SITE_URL + 'sandwich/savedSandwiches', '_self');

                        }
                    }

                }
            }

        });
    }
        
        return $sandid;

    },
    
    enable_li_click : function(){       
         
        $("#optionList li").click(function(e){
          if(e.target == this){ 
             $($(this).find('input')[0]).trigger("click");
          }
        });
        
    },

    add_sandwich_to_cart: function(id, uid, datatype, Qty, ClickedItem) {

        var self = this;
        session = mobile_ha.common.check_session_state();
        if (session.session_state == false) {
             if(session.facebook_only_user == false){
               mobile_ha.common.login_popup(ClickedItem);
             } else {
               mobile_ha.common.facebook_login(ClickedItem);
             }
            return;
        }

        var user_id = $("#hidden_uid").val();
        if (uid) {
            user_id = uid;
        }
        var data_type = $("#hidden_sandwich_data").val();
        if (datatype) {
            data_type = datatype
        }

        var toast = 0;
        if ($(".menucheck").is(':checked')) {
            toast = 1;
        }

        var qty;
        if (Qty) qty = Qty;
        else qty = 1;

        if (datatype == 'product') {

                        
            var prod_descriptor = mobile_ha.common.product_extras.Data[id];


            if (typeof(prod_descriptor) == "undefined") {


            } else { 

                $("#product-descriptor ul.from-holder").html("");

                for (i = 0; i < prod_descriptor.length; i++) {

                    var prod_desc = prod_descriptor[i];

                    var options = "";

                    for (j = 0; j < prod_desc.extra.length; j++) {

                        var option = prod_desc.extra[j];

                        options += "<option value='" + option.id + "'>" + option.name + "</option>";
                    }

                    $("#product-descriptor .title-holder h1").html(prod_desc.desc);

                    var content = '<li><span class="text-box-holder"><select name="extra_id" class="extra_id listextra">' + options + '</select></span> </li><input type="hidden" name="descriptor_id" value="' + prod_desc.id + '"/>';


                    $("#product-descriptor ul.from-holder").append(content);
                }

                var hidden_fields = '<input type="hidden" name="itemid" value="' + id + '" />';
                hidden_fields += '<input type="hidden" name="data_type" value="' + data_type + '" />';
                hidden_fields += '<input type="hidden" name="user_id" value="' + user_id + '" />';
                hidden_fields += '<input type="hidden" name="qty" value="' + qty + '" />';
                hidden_fields += '<input type="hidden" name="toast" value="' + toast + '" />';


                $("#product-descriptor form .hidden_fields").html(hidden_fields);
                $("#product-descriptor input.add_desc").val('ADD TO CART');
                $("#product-descriptor").show();
                return false;
            }

        }


        $.ajax({
            type: "POST",
            data: {
                'itemid': id,
                "data_type": data_type,
                "user_id": user_id,
                "qty": qty,
                "toast": toast
            },
            async: false,
            url: SITE_URL + 'sandwich/add_to_cart',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
                $("#sandwich-view").hide();
            },
            success: function(e) {
                self.added_to_cart_transition(e);
                $(".popup-wrapper").hide();
                $("#sandwich-view").hide();
            }
        });


    },

    add_product_descriptor_to_cart: function() {

        var self = this;

        $("#product-descriptor input.add_desc").click(function() {

            var data = $(this).parent().serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'sandwich/add_to_cart',
                beforeSend: function() {
                    $("#ajax-loader").show();
                },
                complete: function() {
                    $("#ajax-loader").hide();
                    $("#sandwich-view").hide();
                },
                success: function(e) {
                    self.added_to_cart_transition(e);
                    $('.shopping-cart span').text(e);
                    $("#product-descriptor").hide();
                    $("#sandwich-view").hide();

                }
            });
        });

    },


    destroy_endscreen_var: function() {
        mobile_ha.common.setCookie('sandwich_end_screen', false, 1);
    },

    preload_sandwich_images: function() {
        $('.optionLoader').show();
        $.ajax({
            type: "POST",
            url: SITE_URL + 'createsandwich/preload_images/',
            success: function(e) {
                $('.appendImages').html();
                $('.optionLoader').hide();
            }
        });
    },


    added_to_cart_transition: function(e) {

       var url = window.location.href;      
        
        if(url.indexOf('createsandwich') > -1){         
            return;
        }   
        
        var divTop = "0px"
        
        
        if(url.indexOf('gallery') > -1){
            divTop = "60px"
        }       
            
        var currentCount = $('.cart-top h1').text();
        $('.cart-top h1').text(e);
        var difference = parseInt(e) - parseInt(currentCount);              
        var scrollHeight = $(window).scrollTop();                
        if(scrollHeight < 0){ 
            $("div.dynamic_updates_fixed div.left-block").html("<p>"+difference+" item(s) added to cart.</p>" );
            $("div.dynamic_updates_fixed").slideDown("slow");           
            setTimeout(function(){ $("div.dynamic_updates_fixed").slideUp("slow"); }, 4000);
        }else{
            $("div.dynamic_updates div.left-block").html("<p>"+difference+" item(s) added to cart.</p>" );
            $("div.dynamic_updates").css("top", divTop );
            $("div.dynamic_updates").slideDown("slow");
            $('.shopping-cart span').text(e);
            setTimeout(function(){ $("div.dynamic_updates").slideUp("slow"); }, 4000);  
        }
        

        $("input.text-box.qty").val("01");
    },
    //Creating sandwich image processes
    create_sandwitch_image: function(width, height, id, from) {
        var _imgs = [];
        var $final;
        var ret = false;
        if(from != 'quick_edit'){
            $('.image-holder img:visible').each(function() {

                src = $(this).attr('src');
                Arr = src.split('/');
                img = Arr[Arr.length - 1];
                _imgs.push(img);

            });
        }else if(from == 'quick_edit'){
            $('.image-holder img').each(function() {

                src = $(this).attr('src');
                Arr = src.split('/');
                img = Arr[Arr.length - 1];
                _imgs.push(img);

            });
        }


        $final = JSON.stringify({
            images: _imgs
        });

      
        $.ajax({
            type: "POST",
            data: {
                'json_string': $final,
                'width': width,
                'height': height,
                'file_name': 'sandwich_' + id + '_' + SESSION_ID + '.png',
                'path': SESSION_ID + '/'
            },
            url: SITE_URL + 'createsandwich/save_image/',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            async: false,
            success: function(e) {
                ret = true;
            }
        });


        return ret;
    },

    randomBetween: function(min, max) {
        if (min < 0) rand = min + Math.random() * (Math.abs(min) + max);
        else rand = min + Math.random() * max;
        return rand = Math.floor(rand);
    },

    enable_nav_buttons: function() {
        var self = this;
        $(".next-button,.back-button").unbind("click touchstart").on("click touchstart", function(t) {
         
            if(self.vars.ACTIVE_MENU == "BREAD"){ 
              error = self.validate(self.vars.ACTIVE_MENU);
              if (error == true) {
                return false
              } 
            }
            t.preventDefault();
            self.turn_bread_image();
            pagelength = parseInt(self.vars.scenes.length) - 1;
            pageIndex = self.vars.scenes.indexOf(self.vars.ACTIVE_MENU);
            classname = t.target.className;
            if (pageIndex == -1) pageIndex = 0;


            if ($($('.sandwhich-menu li')[2]).find('a').hasClass("doneSandwich") && (pageIndex == pagelength) && classname.indexOf('next-button') != -1) {
                 mobile_ha.common.setCookie('doneSandwich', true, 1);
                self.show_end();

            } else {

                if (classname == 'next-button') {

                    if (pageIndex < pagelength) pageIndex++;
                    else pageIndex = 0;

                } else if (classname == 'back-button') {
                    if (pageIndex > 0) pageIndex--;
                    else pageIndex = 0;
                }

                if (pageIndex == 0){ 
                    $($('.sandwhich-menu li')[1]).addClass('standard-item-head');
                    $($('.create-navigation span')[0]).find('a').css("visibility", "hidden"); 
                }
                else {  
                    $($('.create-navigation span')[0]).find('a').css("visibility", ""); 
                    $($('.sandwhich-menu li')[1]).removeClass('standard-item-head');  
                }
                if (pageIndex == pagelength) {
                    $($('.create-navigation span')[1]).find('a').text("DONE");
                    $($('.create-navigation span')[1]).find('a').addClass("doneSandwich");
                } else {
                    $($('.create-navigation span')[1]).find('a').text("NEXT");
                    $($('.create-navigation span')[1]).find('a').removeClass("doneSandwich");
                }

                self.goTopage(pageIndex);
            }
            
            
        });
    },

    goTopage: function(index) {
        var self = this;
        page = self.vars.scenes[index];

        error = self.validate(page);
        if (error == true) {
            return false
        }
        
        $menuHead = $('.sandwhich-menu li');
        $($menuHead[1]).find('a').text("Choose "+page.toLowerCase());
        $("h1.steps").text("STEP "+(index + 1)+" of 5");

        if (page != 'BREAD') self.turn_bread_image();
        self.vars.ACTIVE_MENU = page;
        self.load_side_menu(page);
        self.user_selections();
    },

    //Loading menu items
    load_side_menu: function(page) {
        var e;
        var t = this;

        $.ajax({
            type: "GET",
            url: SITE_URL + t.vars.AjaxURL[page],
            success: function(e) {
                $datax = $(e).find('input').not('.sub-value input');
                $datax.each(function() {
                    itmDat = $(this).data();
                    if (itmDat.itemname != "NULL") {
                        t.vars.ingredient_details[t.vars.ACTIVE_MENU].actual_price[itmDat.itemname] = itmDat.price;
                    }
                });
                $(".ajaxContent").html(e);
                t.user_selections();
                t.reload_state();
                t.slide_function();
                t.loadScrollBar();
                t.enable_li_click();
                $('.sub-value input[type="text"]').focus(function(){
                    $(this).blur();
                });
            }
        })
    },
    //Binding scroll event
    loadScrollBar: function(){
        $(".common-scrollbar").mCustomScrollbar({
            theme:"minimal"
        });
        
        setTimeout(function(){
            $(".common-scrollbar").removeClass("mCS-autoHide");
        }, 200);
    },
    //User Selection events of create sandwich
    user_selections: function() {
        var e = this;
        $selectors = $("#optionList input");

        $selectors.unbind("click").not('.sub-value input').on("change", function(t) {

            if ($("#optionList #null:checked").length) {

            } else {
                if ($(this).not("#null").length == 0) e.calculate_price();
            }
            var option_data;
            if ($(this).parent("li").find(".sub-value:visible").length > 0) {
                $(this).parent("li").find(".sub-value").find('input').hide();
                $(this).parent("li").find(".sub-value").hide();
            } else {
                sub_index = $('.sub-value').find('input[value~="NORMAL"]').index();
                if (sub_index == -1) sub_index = 0;
                $(this).parent("li").find(".sub-value").find('input').hide();
                $($(this).parent("li").find(".sub-value").find('input')[sub_index]).show();
                $(this).parent("li").find(".sub-value").show();
               
            }
            e.slider_button_fix();
            option_data = $(this).parent().find(".sub-value").find('input:visible').data();
            data = $(this).data();

            $num = e.ing_availaility_check();
            if (e.vars.ACTIVE_MENU == 'BREAD' || e.vars.ACTIVE_MENU == "") {
                e.vars.ingredient_details["BREAD"].type = data.bread_type;
                e.vars.ingredient_details["BREAD"].shape = data.shape;

                if ($num > 0) {
                    e.vars.ingredient_details["BREAD"].item_image[0] = data.item_image_sliced;
                }
                e.update_json_data('create_edit');

            }
             
            $('.button-holder').show();
            $('.homecontent').remove();

            $image = e.select_image_by_shape(data, $num, option_data);
            
            image_url = e.vars.image_path + $image;
            $name = data.itemname;
            $price = data.price;
            $sliced_image = data.item_image_sliced;
            $id = data.id;
            $priority = data.priority;
          
            className = e.Get_imageWrapperClassName();

            if (data.type == "replace") {
                
                itm_price = parseFloat(data.price);
                e.vars.current_sliced_image = $sliced_image;
              
                $($(className + " .image-holder")[0]).find("img").fadeOut(300, function(){
                    $($(className + " .image-holder")[0]).find("img").attr("src", image_url);   
                    
                    $($(className + " .image-holder")[0]).find("img").load(function(){
                         
                        $($(className + " .image-holder")[0]).find("img").fadeIn(300);
                    });
                });
                
                
                e.on_complete_actions(true, $name, $price, $image, $id)
            } else {
                if (t.target.checked == true) {
                    if (data.empty == true) {

                        e.on_complete_actions(false, "NULL", "0", '', '', '');
                        if ($("#optionList").find("input:checked").not("#null").length > 0) {


                            $("#optionList").find("input:checked").not("#null").each(function() {
                                data_fe = $(this).data();


                                $(className + '.image-holder[rel="' + data_fe.itemname + '"]').find('img').fadeOut(300, function() {
                                    $(className + '.image-holder[rel="' + data_fe.itemname + '"]').remove();
                                });


                            });
                            $("#optionList").find("input:checked").not("#null").prop("checked", false);
                            ACTIVE_TAB = e.vars.ACTIVE_MENU;
                            e.vars.ingredient_details[ACTIVE_TAB].item_name = ["NULL"];
                            e.vars.ingredient_details[ACTIVE_TAB].item_price = [0];
                            if (e.vars.ingredient_details[ACTIVE_TAB].item_priority) e.vars.ingredient_details[ACTIVE_TAB].item_priority = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_id = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_image = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_qty = {}
                            e.calculate_price();

                        }
                    } else {

                       
                        if ($("#optionList #null:checked").length > 0) {
                            $("#optionList #null").prop("checked", false);
                            ACTIVE_TAB = e.vars.ACTIVE_MENU;
                            $name_index = $.inArray("NULL", e.vars.ingredient_details[ACTIVE_TAB].item_name);
                            $price_index = $.inArray(0, e.vars.ingredient_details[ACTIVE_TAB].item_price);
                            e.vars.ingredient_details[ACTIVE_TAB].item_name.splice($name_index, 1);
                            e.vars.ingredient_details[ACTIVE_TAB].item_price.splice($price_index, 1);
                        }

                        e.dynamic_option_selection($(this), true);

                        im = [e.vars.current_sliced_image];
                        if (im != null && im != '' && e.vars.ACTIVE_MENU != "BREAD") {
                            if (e.vars.ingredient_details["BREAD"].item_image[0] != im) {
                                e.vars.ingredient_details["BREAD"].item_image = im;
                                e.turn_bread_image();
                            }
                        }

                        $select = $(this).parent().find(".sub-value");
                        if ($select.length > 0) {
                            $dat = $select.find("input:visible").data();
                            if ($dat) {
                                $arr = [];
                                $arr[0] = $select.find("input:visible").val();
                                $arr[1] = $dat.unit;
                                $arr[2] = $dat.price_mul;
                                $arr[3] = $dat.image;
                                $arr[4] = $dat.optionid;
                                e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$name] = $arr;
                            }
                        }

                        $pv = $priority;
                        e.set_images_in_position($priority, image_url, $(this));
                        e.on_complete_actions(false, $name, $price, $image, $id, $pv);
                        e.auto_select_option_fix();
                    }
                } else {
                    e.remove_items($id);
                    e.dynamic_option_selection($(this), false);
                    e.auto_select_option_fix();
                }
            }
        })
    },

    user_selections_on_quick_edit: function() {
        var e = this;
        
        $('#sandwiches_quick_edit_add_item a.add-item-to-sandwich').unbind("click").on("click", function(t) {
            t.stopPropagation();

            $selectors = $('#add-item-select option:selected');
            e.vars.ACTIVE_MENU = $('#add-item-category-select option:selected').text();
            
            data = $('#add-item-select option:selected').data();
            $select = $('#sandwiches_quick_edit_add_item .new_popup-spiner__edit');
            $dat = $select.find("input:visible").data();

            $num = e.ing_availaility_check();

            $image = e.select_image_by_shape(data, $num, $dat);

            image_url = e.vars.image_path + $image;
            $name = data.itemname;
            $price = data.price;
            $sliced_image = data.item_image_sliced;
            $id = data.id;
            $priority = data.priority;

            className = e.Get_imageWrapperClassName();
                
                if ($dat) {
                    $arr = [];
                    $arr[0] = $select.find("input:visible").val();
                    if(e.vars.ACTIVE_MENU == 'PROTEIN') $arr[0] = $select.find("input:visible").val().split(' (')[0];
                    else $arr[0] = $select.find("input:visible").val();
                    $arr[1] = $dat.unit;
                    $arr[2] = $dat.price_mul;
                    $arr[3] = $dat.image;
                    $arr[4] = $dat.optionid;
                    console.log("e.vars.ACTIVE_MENU", e.vars.ACTIVE_MENU)
                    console.log("e.vars.ingredient_details", e.vars.ingredient_details)
                    if(Object.keys(e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty).length == 0)
                    {
                        e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty = {};
                    }
                    e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$name] = $arr;
                }

            $pv = $priority;
            
            e.on_complete_actions(false, $name, $price, $image, $id, $pv);
            e.reload_state_quick_edit();
            setTimeout(function(){
                e.set_images_in_position($priority, image_url, $selectors);
            }, 3000);
            
            e.auto_select_option_fix_on_quickedit();

            $('#sandwiches_quick_edit_add_item').hide();
                  
        })
    },

    slider_button_fix: function() {
        var self = this;
         
        actLen    =  $('.sub-value:visible').length;
        itemIndex = $('.sub-value:visible .text-box input:visible').index();
        defaultChoice =  self.vars.ingredient_details[self.vars.ACTIVE_MENU].option_default_Choice_first;
        
        if( self.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode != undefined ) 
              modeManual = self.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode;
        else  modeManual = false;
        
        // fix for default half full protien on single protien selection
        if( actLen == 1 && modeManual == false && itemIndex != defaultChoice ){
               $inputs  =  $('.sub-value:visible').find('input');
               $($inputs).hide();
              
               $($inputs[defaultChoice]).show();
        }
        
        
        $('.sub-value').each(function(e) {
            count = $(this).find('input').length;
            count = count - 1;
            index = $(this).find('input:visible').index();
            if (index == 0) {
                $(this).find('.left').removeClass('left-hover');
                $(this).find('.right').addClass('right-hover');
            } else if ((index > 0) && (index < count)) {
                $(this).find('.left').addClass('left-hover');
                $(this).find('.right').addClass('right-hover');
            } else if (index == count) {
                $(this).find('.right').removeClass('right-hover');
                $(this).find('.left').addClass('left-hover');
            }

        });

    },

    ing_availaility_check: function() {
        var self = this;
        count = 0;
        Object.keys(self.vars.ingredient_details).forEach(function(e) {
            if (e != 'BREAD') count += self.vars.ingredient_details[e].item_id.length;
        });
        return count;
    },
    //Image processing events on selections
    select_image_by_shape: function(data, num, option_data) {
        console.log("data", data);
         console.log("num", num);
          console.log("option_data", option_data);
        self = this;
        $img = '';
        if (self.vars.ACTIVE_MENU == "BREAD" || self.vars.ACTIVE_MENU == "") {

            if (num > 0) return data.item_image_sliced;
            else return data.image;

        }

        if(option_data){
            switch (self.vars.ingredient_details["BREAD"].shape) {

                case 0:
                    $img = option_data.image_long;
                    break;
                case 1:
                    $img = option_data.image_round;
                    break;
                case 2:
                    $img = option_data.image_trapezoid;
                    break;

            }
        }

        return $img;
    },

    Get_imageWrapperClassName: function() {
        var self = this;

        if (self.vars.ACTIVE_MENU == "BREAD") className = '.bread_image';
        else if (self.vars.ACTIVE_MENU == "PROTEIN") className = '.protein_image';
        else if (self.vars.ACTIVE_MENU == "CHEESE") className = '.cheese_image';
        else if (self.vars.ACTIVE_MENU == "TOPPINGS") className = '.topping_image';
        else if (self.vars.ACTIVE_MENU == "CONDIMENTS") className = '.condiments_image';
        else className = '.bread_image';
        return className;
    },

    on_complete_actions: function(e, t, n, w, z, s) {
        var i = this;
        $sel = $(".create-sandwich-menu ul li").find("a:contains(" + i.vars.ACTIVE_MENU + ")").parent().next().find("a");
        $txt = $($sel).text();

        if (e == true) {
            if (z) {
                i.vars.ingredient_details.BREAD.item_name[0] = t;
                i.vars.ingredient_details.BREAD.item_price[0] = n;
                i.vars.ingredient_details.BREAD.item_image[0] = w;
                i.vars.ingredient_details.BREAD.item_id[0] = z;
            }
        } else {
            if (z) {

                if (i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name.length == 0 && i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price.length == 1) {
                    i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price = [];
                }

                if ($.inArray(z, i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_id) == -1) {
                    id_index = i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_id.push(z)
                    id_index = id_index - 1;
                    if (t) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name[id_index] = t;
                    if (n) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price[id_index] = n;
                    if (s) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_priority[id_index] = s;
                    if (w) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_image[id_index] = w;
                    
                }

            } else if (t == "NULL") {
                if ($.inArray(t, i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name) == -1) {
                    i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name.push(t)
                }
            }
        }
        i.calculate_price();
    },
    //Price calculation events
    calculate_price: function() {
        var final_price = 0;
        var self = this;
        self.get_min_cheese_price();
        $data = self.vars.ingredient_details;
        Object.keys($data).forEach(function(e) {
            $price = $data[e].item_price;
            temp = 0;
            $name = $data[e].item_name;

            $name.forEach(function(t, i) {

               

                if ($price[i] !== undefined && isNaN($price[i]) != true) {
                    temp += parseFloat($price[i]);
                    self.vars.individual_price[e] = temp.toFixed(2);
                }


            });

            if ($name.length == 0) self.vars.individual_price[e] = 0;

        });

        if (self.vars.ingredient_details["BREAD"].type != 0) {
            self.sum_individual();
            self.vars.current_price = self.vars.ingredient_details["BREAD"].item_price[0];
        } else {
            self.vars.current_price = self.sum_individual();
            self.vars.current_price = parseFloat(self.vars.current_price)+parseFloat(BASE_FARE);
            self.vars.current_price=self.vars.current_price.toFixed(2);
        }
    
        
        $(".amount-done-wrapper .price").text("$" + self.vars.current_price);
        if($('#sandwiches_quick_edit:visible').length == 0) self.save_to_temp_session();
        else self.clear_temp_session();
    },
    // Update JSON data for sandwich selections
    update_json_data: function(from) {

        var self = this;
        var $data;
        var tmp = [];
        $data = self.vars.ingredient_details;
        Object.keys($data).forEach(function(e) {
            if (e != "BREAD") {

                if ($data[e].item_id.length > 0) {
                    $data[e].item_id.forEach(function(item_ids) {
                        tmp.push(item_ids);
                    });
                }
            }
        });

        $ids = tmp.join(",");

        $.ajax({
            type: "POST",
            data: {
                'ids': $ids
            },
            dataType: 'json',
            url: SITE_URL + 'createsandwich/get_category_items_data/',
            success: function(e) {

                if (Object.keys(e) != null && Object.keys(e).length > 0 ) {
                    $.each(e, function(index, iData) {

                        Object.keys(self.vars.ingredient_details).forEach(function(key) {
                            
                            if (self.vars.ingredient_details[key].item_id.length > 0) {
                                self.vars.ingredient_details[key].item_id.forEach(function(iKey, iIndex) {

                                    if (iData && iData.id == iKey) {

                                        $option_id = self.vars.ingredient_details[key].item_qty[iData.item_name][4];
                                        switch (self.vars.ingredient_details["BREAD"].shape) {
                                            case 0:
                                                $image = typeof iData.option_images[$option_id] !== 'undefined' ? iData.option_images[$option_id].lImg : iData.image_long;
                                                break;

                                            case 1:
                                                $image = typeof iData.option_images[$option_id] !== 'undefined' ? iData.option_images[$option_id].rImg : iData.image_round;
                                                break;

                                            case 2:
                                                $image = typeof iData.option_images[$option_id] !== 'undefined' ? iData.option_images[$option_id].tImg : iData.image_trapezoid;
                                                break;

                                        }

                                        self.vars.ingredient_details[key].item_image[iIndex] = $image;
                                    }

                                });
                            }

                        });
                    });
                }

                if(from == 'create_edit'){
                    self.load_all_data(self.vars.ingredient_details);
                    self.save_to_temp_session();
                }else if(from == 'quick_edit'){
                    self.load_all_data_quickEdit(self.vars.ingredient_details);
                }
            }
        });

    },
    // Reloading sandwich selections
    reload_state: function() {
        var e = this;
        var t;
        var n;
        var r;
        
        setTimeout(function(){ 
        
        if (!e.vars.ACTIVE_MENU) {
            e.vars.ACTIVE_MENU = 'BREAD';

        }
        t = e.vars.ingredient_details[e.vars.ACTIVE_MENU];

        if (e.vars.ACTIVE_MENU == 'BREAD' && t.item_id) {
            $data = $('.main-category-list ul li input[data-id="' + t.item_id + '"]').data();
            if ($data) e.vars.current_sliced_image = $data.item_image_sliced;
        }
        
        

        t.item_name.forEach(function(e) {
            $('input[data-itemname="' + e + '"]').prop("checked", true);
            n = e;

            $(".sub-value").each(function() {
                if ($(this).parent().find("input:checked").length > 0) {
                    $(this).show();


                    r = t.item_qty[n];

                    if (r != undefined && $(this).parent().find("input[data-itemname='" + n + "']").length > 0) {
                        $(this).find("input").hide();
                        $(this).find('input[value="' + r[0] + '"]').show()
                    }
                }
            });
        });
        e.calculate_price();
        e.slider_button_fix();
        
         },500);
    },

    reload_state_quick_edit: function() {
        var e = this;
        var t;
        var n;
        var r;
        var html = '';
        var items_data = '';

        var items_array = [];

        $.each(e.vars.ingredient_details, function(k, ingredient ) {
            console.log("ingredient.item_id", ingredient.item_id)
            if(k != 'BREAD')
            items_array = items_array.concat(ingredient.item_id);
        })

        $item_ids = items_array.length > 0 ? items_array.join(',') : '';
        if(items_array.length > 0 )
        {
            $.ajax({
                    type: "POST",
                    data: {
                        'ids': $item_ids
                    },
                    dataType: 'json',
                    url: SITE_URL + 'createsandwich/get_category_items_data/',
                    success: function(data) {
                        
                        console.log("item_data", data);
                        console.log("data.length", Object.keys(data).length);
                        if(Object.keys(data).length > 0) items_data = data;
                        
                        $.each(e.vars.ingredient_details, function(k, ingredient ) {
                
                            console.log("ingredient", k);
                  
                            if(k != 'BREAD'){
                    

                                var t = e.vars.ingredient_details[k];
                                html += '<div class="spiner-edit--wrapper '+ k.toLowerCase() +'-items"><label>'+ k.charAt(0) + k.slice(1).toLowerCase() +'</label>';
                                t.item_id.forEach( function(v, i) {
                                    var item_name = t.item_name[i];
                                    var item_qty = t.item_qty[item_name];
                                    var item_id = t.item_id[i];

                                    var id = (k + '-check'+ (i+1)).toLowerCase();

                                    html += '<div class="new_spiner-edit"><input type="checkbox" class="cat-item" data-category_name="'+ k.toLowerCase() +'" data-empty="false" data-priority="'+ t.item_priority[i] +'" data-id="'+ t.item_id[i] +'" data-price="'+ t.actual_price[item_name] +'" data-image="'+ t.item_image[i] +'" data-itemname="'+ item_name +'" type="checkbox" value="" name="check" id="'+ id +'">';
                                    html += '<p>'+ item_name + '</p>';
                                        
                                    html += '<div class="new_popup-spiner__edit"> <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">';
                                    html += '<a href="javascript:void(0)" class="left left_spin" onclick="editQuantity_quickEdit(this)" data-from="edit-sandwich"></a>';
                                    console.log("items_data[item_id]", items_data[item_id]);
                                    $.each(items_data[item_id].options_id, function(index, option){
                                        var tImg = "", rImg = "", lImg = "";
                                        var option_id = option.id;
                                        if((typeof item_qty !== 'undefined') && (option.option_name.toLowerCase() == item_qty[0].toLowerCase() || item_qty[0].toLowerCase() == option.option_name.toLowerCase() + ' (' + option.option_unit +')' )){
                                            display = "block";
                                            current = "true";
                                        }else{
                                            display = "none";
                                            current = "false";
                                        }
                                        if(items_data[item_id].option_images.hasOwnProperty(option_id)){
                                            if(typeof items_data[item_id].option_images[option_id]['tImg'] !== "undefined"){
                                                tImg = items_data[item_id].option_images[option_id]['tImg'];
                                            }
                                            if(typeof items_data[item_id].option_images[option_id]['rImg'] !== "undefined"){
                                                rImg = items_data[item_id].option_images[option_id]['rImg'];
                                            }
                                            if(typeof items_data[item_id].option_images[option_id]['lImg'] !== "undefined"){
                                                lImg = items_data[item_id].option_images[option_id]['lImg'];
                                            }
                                        }
                                         
                                        if(k == "PROTEIN"){
                                            html += '<input rel="slide" readonly="" style="display: '+ display +';" data-unit="'+ option.option_unit +'" data-optionid="'+ option_id +'" data-current="'+ current +'" data-image="" data-image_trapezoid="'+ tImg +'" data-image_round="'+ rImg +'" data-image_long="'+ lImg +'" data-price_mul="'+ option.price_mult +'" type="text" name="" class="text-box" value="'+ option.option_name.toLowerCase() + ' (' + option.option_unit +')">';
                                        }else{
                                            html += '<input rel="slide" readonly="" style="display: '+ display +';" data-unit="'+ option.option_unit +'" data-optionid="'+ option_id +'" data-current="'+ current +'" data-image="" data-image_trapezoid="'+ tImg +'" data-image_round="'+ rImg +'" data-image_long="'+ lImg +'" data-price_mul="'+ option.price_mult +'" type="text" name="" class="text-box" value="'+ option.option_name.toLowerCase() +'">';
                                        }

                                        if(display == "block")  e.vars.ingredient_details[k].item_qty[item_name][4] = option_id;
                                     })
                        
                                    html += '<a href="javascript:void(0)" class="right right_spin" onclick="editQuantity_quickEdit(this)" data-from="edit-sandwich"></a>';
                                    html += '</h3><a href="javascript:void(0)" onclick="removeItems_quickedit(this)" class="delete-edit" data-id="'+ t.item_id[i] +'" data-category="'+ k +'"><img src="'+ SITE_URL +'app/images/delete_spiner.png"></a></div></div>';

                                    console.log(e);
                                });
                                html += '</div>';
                            }
                        })

                        $('#sandwiches_quick_edit .editable-sandwich-container').html(html); 
                        
                        $("#sandwiches_quick_edit").show();
                        e.calculate_price();
                        $('#sandwiches_quick_edit h2.amount').text("$" + e.vars.current_price);
                        //     e.slider_button_fix();
                    }
                });
        }
        else
        {
            $.each(e.vars.ingredient_details, function(k, ingredient ) {
                
                            console.log("ingredient", k);
                  
                            if(k != 'BREAD'){
                    

                                var t = e.vars.ingredient_details[k];
                                html += '<div class="spiner-edit--wrapper '+ k.toLowerCase() +'-items"><label>'+ k.charAt(0) + k.slice(1).toLowerCase() +'</label>';
                                html += '</div>';
                            }
                        })

                        $('#sandwiches_quick_edit .editable-sandwich-container').html(html); 
                       
                        $("#sandwiches_quick_edit").show();
                         e.calculate_price();
                        $('#sandwiches_quick_edit h2.amount').text("$" + e.vars.current_price);
        }

       // console.log("e.vars.ingredient_details", e.vars.ingredient_details)
        
 
    },

    slide_function: function() {
        var e = this;
        $("#optionList .left, #optionList .right").unbind("click").on("click", function(t) {
            t.preventDefault();
            $parent = $(this).parent();
            e.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode = true;
            $ptext = $parent.parent().find('input[type="checkbox"]').data();

            $pele = $parent.parent().find('input[type="checkbox"]');
            $p_priority = $ptext.priority;

            $total = $parent.find("input").length - 1;
            $inputs = $parent.find("input");
            $curr_index = $parent.find("input:visible").index();
            $curr_index_t = $curr_index + 1;
            if ($curr_index > 0) {
                $prv_index = $curr_index - 1
            } else {
                $prv_index = 0
            }
            if ($curr_index < $total) {
                $nxt_index = $curr_index + 1
            } else {
                $nxt_index = $total
            }

            if (t.target.className.indexOf("right") != -1) {
                $parent.find("input:visible").hide();
                $($parent.find("input")[$nxt_index]).show();
                $ind = $parent.find("input:visible").index();

                if ($ind == $total) {

                    $(this).removeClass("right-hover");
                    $(this).parent().find('.left').addClass("left-hover");
                }
                if ($ind > 0) {
                    $(this).parent().find('.left').addClass("left-hover");
                }


            } else {
                $parent.find("input:visible").hide();
                $($parent.find("input")[$prv_index]).show();
                $ind = $parent.find("input:visible").index();

                if ($ind == 0) {

                    $(this).removeClass("left-hover");
                    $(this).parent().find('.right').addClass("right-hover");
                }

                if ($ind < $total) {
                    $(this).parent().find('.right').addClass("right-hover");
                }

            }

            $arr = [];
            $value = $parent.find("input:visible").val();
            $data = $parent.find("input:visible").data();

            $num = e.ing_availaility_check();
            $image = e.select_image_by_shape($ptext, $num, $data);
            console.log("$image", $image);
            image_url = e.vars.image_path + $image;
            if($image == "")
            {
                image_url = ""; 
            }
            $arr[0] = $value;
            $arr[1] = $data.unit;
            $arr[2] = $data.price_mul;
            $arr[3] = $data.image;
            $arr[4] = $data.optionid;
            e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$ptext.itemname] = $arr;
            $itn_id = $ptext.id;
            new_price = parseFloat($ptext.price) * parseFloat($arr[2]);
            $p_index = $.inArray($itn_id, e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_id);
            if ($p_index != -1) {
                e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_price[$p_index] = new_price;
                e.calculate_price();
            }
            e.set_images_in_position($p_priority, image_url, $pele);
            e.on_complete_actions(false, $ptext.itemname, $ptext.price, $image, $ptext.id, $p_priority);
            e.auto_select_option_fix();
        });

        e.slider_button_fix();
    },

    slide_function_on_quick_edit: function() {
        var e = this;
        $(document).find("#sandwiches_quick_edit_add_item .left_spinner, #sandwiches_quick_edit_add_item .right_spinner, #sandwiches_quick_edit .left, #sandwiches_quick_edit .right").unbind("click").on("click", function(t) {
            t.stopPropagation();
            t.preventDefault();
            $parent = $(this).parent();
            var category = $('#add-item-category-select option:selected').text();
            e.vars.ingredient_details[category].manual_mode = true;
            $ptext = $('#add-item-select option:selected').data();
            $total = $parent.find("input").length - 1;
            $inputs = $parent.find("input");
            $curr_index = $parent.find("input:visible").index();
            console.log("curr_index", $curr_index);
            $curr_index_t = $curr_index + 1;
            $curr_index = $curr_index - 1;
            if ($curr_index > 0) {
                $prv_index = $curr_index - 1
            } else {
                $prv_index = 0
            }
            if ($curr_index < $total) {
                $nxt_index = $curr_index + 1
            } else {
                $nxt_index = $total
            }

            if (t.target.className.indexOf("right_spin") != -1) {
                $parent.find("input:visible").hide();
                $parent.find("input:eq("+ $nxt_index +")").show();
                $ind = $parent.find("input:visible").index();
            } else {
                $parent.find("input:visible").hide();
                $parent.find("input:eq("+ $prv_index +")").show();
                $ind = $parent.find("input:visible").index();
            }

        });

        //e.slider_button_fix();
    },

    get_min_cheese_price: function() {
        var self = this;
        var array = [];
        self.vars.ingredient_details.CHEESE.item_name.forEach(function(e) {
            if (self.vars.ingredient_details.CHEESE.actual_price[e] > 0) array.push(self.vars.ingredient_details.CHEESE.actual_price[e]);
        });
        min = Math.min.apply(Math, array);
        if (min) {
            self.vars.cheese_min = min;
        }

    },

    sum_individual: function() {
        $data = this.vars.individual_price;
        var finalprice = 0;
        Object.keys($data).forEach(function(e) {
            finalprice += parseFloat($data[e]);
        });
        return finalprice.toFixed(2);
    },
    //Saving sandwich selection data in temp session
    save_to_temp_session: function() {

        var self = this;
        user_data = JSON.stringify(self.vars.ingredient_details);

        $obj = {
            'Response': {
                'status-code': '200',
                'message': 'OK'
            },
            'Data': [{

                'id': '',
                'uid': '',
                'sandwich_name': '',
                'date_of_creation': '',
                'description': '',
                'sandwich_data': user_data,
            }]
        };

        $obj = JSON.stringify($obj)

        $.ajax({
            type: "POST",
            data: {
                'data': $obj
            },
            url: SITE_URL + 'createsandwich/temp_session_input/',
            dataType: 'json',
           
            beforeSend: function() {
                $(".optionLoader").show();
            },
            complete: function() {
                $(".optionLoader").hide();
            },
            success: function(e) {

            }

        });
    },

    load_all_data: function($data,pageload) {
        
        var $html = '';
        var self = this;
        var itms = {};
        var htmlELm = {}
        Object.keys($data).forEach(function(e) {
            var priorityOb = {};
            if ($data[e].item_name != "NULL") {
                $images = $data[e].item_image;
                $names = $data[e].item_name;
                $priority = $data[e].item_priority;

                $len = $names.length;
                for (i = 0; i < $len; i++) {
                    if (e != 'BREAD') {
                        $rel = $names[i];

                    } else {
                        $rel = '';
                    }

                    $img = self.vars.image_path + $images[i];
                    if (e != 'BREAD') {
                        priorityOb[$priority[i]] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    } else {

                        priorityOb[0] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    }
                }

                htmlELm[e] = priorityOb;
                $html += self.sandwich_image_wrapping(e, htmlELm);
            }
            
            
        });
        
        
        pcount  = self.vars.ingredient_details.PROTEIN.item_id.length;
        ccount  = self.vars.ingredient_details.CHEESE.item_id.length;
        cocount = self.vars.ingredient_details.CONDIMENTS.item_id.length;
        tcount  = self.vars.ingredient_details.TOPPINGS.item_id.length;

        
        if(pageload == true && self.vars.ACTIVE_MENU == "BREAD"){

            $(".bread_images").html("" + $html + "");
                $(".bread_images .image-holder img").load(function(){
                    $(".bread_images .image-holder img").fadeIn(300); 
                });
                
        }
        else if (pcount > 0 || ccount > 0 || cocount  > 0 || tcount > 0) {
            
            $(".bread_images").html("" + $html + "");        
            $(".bread_images .image-holder img").each(function(){
                
                $(this).load(function(){ 
                    $(this).fadeIn(400); 
                });
                
            });
        }
        
            
    },

    load_all_data_quickEdit: function($data,pageload) {
        var $html = '';
        var self = this;
        var itms = {};
        var htmlELm = {}
        Object.keys($data).forEach(function(e) {
            var priorityOb = {};
            if ($data[e].item_name != "NULL") {
                $images = $data[e].item_image;
                $names = $data[e].item_name;
                $priority = $data[e].item_priority;

                $len = $names.length;
                for (i = 0; i < $len; i++) {
                    if (e != 'BREAD') {
                        $rel = $names[i];

                    } else {
                        $rel = '';
                    }

                    $img = self.vars.image_path + $images[i];
                    console.log(e+" priority", $priority);
                    if (e != 'BREAD') {
                        if($priority != undefined)
                            priorityOb[$priority[i]] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                        else
                            priorityOb[i] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    } else {

                        priorityOb[0] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    }



                }

                htmlELm[e] = priorityOb;
                $html += self.sandwich_image_wrapping(e, htmlELm);
            }
        });
        
        
        pcount  = self.vars.ingredient_details.PROTEIN.item_id.length;
        ccount  = self.vars.ingredient_details.CHEESE.item_id.length;
        cocount = self.vars.ingredient_details.CONDIMENTS.item_id.length;
        tcount  = self.vars.ingredient_details.TOPPINGS.item_id.length;

 
        if(pageload == true && self.vars.ACTIVE_MENU == "BREAD"){
            $(".bread_images").html("" + $html + "");
                $(".bread_images .image-holder img").load(function(){
                    $(".bread_images .image-holder img").fadeIn(300); 
                });
                
        }
        else if (pcount > 0 || ccount > 0 || cocount  > 0 || tcount > 0) {
        $(".bread_images").html("" + $html + "");
        
        $(".bread_images .image-holder img").each(function(){
            
            $(this).load(function(){ 
                $(this).fadeIn(400); 
            });
            
        });
        
       }
    },
    
    //Sandwich image wrapping on priority
    sandwich_image_wrapping: function(type, html) {

        var finalData = {
            'BREAD': '',
            'PROTEIN': '',
            'CHEESE': '',
            'TOPPINGS': '',
            'CONDIMENTS': ''
        };
        $data = html[type];
        $keys = Object.keys($data);
        $keys.forEach(function(keys) {
            finalData[type] += $data[keys];
        });


        switch (type) {
            case 'BREAD':
                $data = '<span class="bread_image">' + finalData[type] + '</span>';
                break;
            case 'PROTEIN':
                $data = '<span class="protein_image">' + finalData[type] + '</span>';
                break;
            case 'CHEESE':
                $data = '<span class="cheese_image">' + finalData[type] + '</span>';
                break;
            case 'TOPPINGS':
                $data = '<span class="topping_image">' + finalData[type] + '</span>';
                break;
            case 'CONDIMENTS':
                $data = '<span class="condiments_image">' + finalData[type] + '</span>';
                break;
        }



        return $data;
    },

    turn_bread_image: function() {
        var self = this;
        var $img = self.vars.image_path + self.vars.current_sliced_image;
        current = $(".bread_images .bread_image div:first").find('img').attr('src');

        if (self.vars.current_sliced_image && current.indexOf($img) == -1) {
            $(".bread_images .bread_image div:first").find('img').fadeOut(300, function() {
                
                if ($img) $(".bread_images .bread_image div:first").find('img').attr('src', $img);
                
                    $(".bread_images .bread_image div:first").find('img').load(function(){ 
                    $(".bread_images .bread_image div:first").find('img').fadeIn(300);
                });
                 
            });
        }
    },

    dynamic_option_selection: function(selector, dataAdding) {
        
        var self = this;
        ACTIVE_TAB = self.vars.ACTIVE_MENU;
        if (dataAdding == true) inc = 1;
        else inc = 0
        len = self.vars.ingredient_details[ACTIVE_TAB].item_name.length + inc;

        if (self.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode != undefined) {

            parent = $(selector).parent();

            sel = $(parent).find('.sub-value');

            if (self.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode != true) {

                inputs = $(sel).find('input');
               
                if (len > 1) {
                    if ($(inputs[0]).length > 0) {
                        $('.sub-value').find('input').hide();
                        $('.sub-value').each(function(e, j) {
                            $($(j).find('input')[self.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_second]).show();
                        });
                        $(inputs[self.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_second]).show();

                    }

                } else {
                    if ($(inputs[self.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_first]).length > 0) {
                        $(sel).find('input').hide();
                        $(inputs[self.vars.ingredient_details[mobile_ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_first]).show();

                    }

                }
            }
        }

        self.slider_button_fix();
    },
    // Setting image layers for bread
    set_images_in_position: function(thisPos, image, currentObj) {
        var self = this;
        var $pos = -1;
        var prev, next;
        var priorityArry = [];

        if(document.getElementById("optionList")){
            $('#optionList li input:checked').each(function() {
                $data = $(this).data();
                if ($data) priorityArry.push($data.priority);
            });
        }else{ // for quick sandwich edit
            var cat_name = $(currentObj).data('category_name');
            $('#sandwiches_quick_edit .'+ cat_name +'-items input[class="cat-item"]').each(function() {
                $data = $(this).data();
                if ($data) priorityArry.push($data.priority);
            });
        }

        priorityArry = priorityArry.sort(function(a, b) {
            return a - b;
        });

        if (priorityArry.length > 0) length = priorityArry.length - 1;
        else length = 0;
        mypos = $.inArray(thisPos, priorityArry);

        if (length == 0) {
            $meth = 'append';
            $priority = priorityArry[mypos];

        } else {

            if (mypos > 0) {
                $meth = 'after';
                $priority = priorityArry[mypos - 1];

            } else {
                $meth = 'before';
                $priority = priorityArry[mypos + 1];
            }


        }

        if(document.getElementById("optionList")){
            itemDat = $('#optionList li input[data-priority="' + $priority + '"]').data();
            currentDat = $('#optionList li input[data-priority="' + thisPos + '"]').data();
        }else{
            itemDat = $('#sandwiches_quick_edit .'+ cat_name +'-items input[class="cat-item"][data-priority="' + $priority + '"]').data();
            currentDat = $('#sandwiches_quick_edit .'+ cat_name +'-items input[class="cat-item"][data-priority="' + thisPos + '"]').data();
        }

        current_data = $(currentObj).data();

        currentName = currentDat.itemname;
        $Itemname = current_data.itemname;
        prev_name = itemDat.itemname;
        
        className = self.Get_imageWrapperClassName();

       // $(".bread_images " + className + " .image-holder[rel='" + currentDat.itemname + "']").remove();
        var old_item_image = $(".bread_images " + className + " .image-holder[rel='" + currentDat.itemname + "']");
        old_item_image.attr("rel", "old_item_image");
        switch ($meth) {
            case 'append':
               
                $(".bread_images " + className).append('<div  rel="' + $Itemname + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
                break;
            case 'after':
               
                $(".bread_images " + className + " .image-holder[rel='" + prev_name + "']").after('<div  rel="' + currentName + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
                break;
            case 'before':
                
                $(".bread_images " + className + " .image-holder[rel='" + prev_name + "']").before('<div  rel="' + currentName + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
        }
        
        $images =  $(".bread_images " + className + " .image-holder[rel='" + currentDat.itemname + "']").find('img');
        
        $($images).load(function(){ 
          $($images).fadeIn(600);
          old_item_image.fadeOut(400, function(){
                old_item_image.remove();
            });
        });
        if(image == "")
        {
            old_item_image.fadeOut(400, function(){
                old_item_image.remove();
            }); 
        }
              
    },

    auto_select_option_fix: function() {
        var e = this;

        $('.sub-value').find('input:visible').each(function() {
            $ptext = $(this).parent().parent().parent().find('input[type="checkbox"]').data();
            $arr = [];
            $value = $(this).val();
            $data = $(this).data();

            $arr[0] = $value;
            $arr[1] = $data.unit;
            $arr[2] = $data.price_mul;
            $arr[3] = $data.image;
            $arr[4] = $data.optionid;

            e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$ptext.itemname] = $arr;

            $itn_id = $ptext.id;
            new_price = parseFloat($ptext.price) * parseFloat($data.price_mul);
            $p_index = $.inArray($itn_id, e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_id);
            if ($p_index != -1) {
               
                if (new_price > 0) e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_price[$p_index] = new_price;
                
            }


        });

        e.calculate_price();

    },

    auto_select_option_fix_on_quickedit: function(){
        var e = this;

            $optionEle = $('#sandwiches_quick_edit_add_item .new_popup-spiner__edit input:visible');
            $ptext = $('#add-item-select option:selected').data();
            $arr = [];
            $value = $optionEle.val();
            $data = $optionEle.data();

            $arr[0] = $value;
            $arr[1] = $data.unit;
            $arr[2] = $data.price_mul;
            $arr[3] = $data.image;
            $arr[4] = $data.optionid;

            e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$ptext.itemname] = $arr;

            $itn_id = $ptext.id;
            new_price = parseFloat($ptext.price) * parseFloat($data.price_mul);
            $p_index = $.inArray($itn_id, e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_id);
            if ($p_index != -1) {

                if (new_price > 0) e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_price[$p_index] = new_price;

            }

        e.calculate_price();
    },
    
    // Removing selection events
    remove_items: function($id) {
        var r = this;
        ACTIVE_TAB = r.vars.ACTIVE_MENU;
        var $item_name;
        if ($id) {
            $id_index = $.inArray($id, r.vars.ingredient_details[ACTIVE_TAB].item_id);
            if ($id_index != -1) {
                $item_name = r.vars.ingredient_details[ACTIVE_TAB].item_name[$id_index];
                r.vars.ingredient_details[ACTIVE_TAB].item_name.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_price.splice($id_index, 1);
                if (r.vars.ingredient_details[ACTIVE_TAB].item_priority) r.vars.ingredient_details[ACTIVE_TAB].item_priority.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_id.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_image.splice($id_index, 1);
                delete r.vars.ingredient_details[ACTIVE_TAB].item_qty[$item_name];

                className = r.Get_imageWrapperClassName();

                $(className + ' .image-holder[rel="' + $item_name + '"]').find('img').fadeOut(500, function() {
                    $(className + ' .image-holder[rel="' + $item_name + '"]').remove();
                    
                });

                r.calculate_price();
            }
        } else if (r.vars.ingredient_details[ACTIVE_TAB].item_name.length > 0) {
            if (r.vars.ingredient_details[ACTIVE_TAB].item_name[0] == "NULL") {
                r.vars.ingredient_details[ACTIVE_TAB].item_name.splice(0, 1);
            }
        }
    },
    //Validation sandwich saving.
    validate: function(e) {
        var t = this;
        var n = [];
        var r = false;
        if (e) {
            $active = e
        } else {
            $active = t.vars.ACTIVE_MENU
        }
        t.vars.validators.forEach(function(e) {
            selections = t.vars.ingredient_details[e].item_name.length;
            
            switch (e) {
                case "BREAD":
                    if (selections == 0) {
                        n.push("Select a bread to continue");
                    }
                    break;
                case "PROTEIN":

                    if (selections == 0) {
                        
                    }

                    break;
                case "CHEESE":

                    if (selections == 0) {
                       

                    } else {
                       
                    }
                    break
            }
        });
        switch (n.length) {
            case 3:
                if ($active != t.vars.validators[0]) {
                    alert(n[0]);
                    r = true
                }
                break;
            case 2:
                if ($active != t.vars.validators[0] && $active != t.vars.validators[1]) {
                    alert(n[0]);
                    r = true;
                }
                break;
            case 1:
                if (t.vars.ingredient_details["BREAD"].item_name.length == 0) {
                    alert("Select a bread to continue");
                    r = true;
                } else {
                    
                }
                break;
            default:
                r = false;
                break;
        }
        return r;
    },

    quickEditLoadEvents: function(){
        var self  = this;
        $('.edit-sandwich-popup').unbind('click').on('click', function(e) 
            {
                session = mobile_ha.common.check_session_state();
                if (session.session_state == false) 
                {
                    $("#mobile-sandwiches-popup").hide();
                    mobile_ha.common.login_popup();
                    return;
                }
                e.preventDefault();
                var id = $(this).attr('rel');

                var bread = $("#mobile-sandwiches-popup .final-list-wrapper .view-popup-scroll").text().split(',');
                var isSaved = $(this).next('a').text();

                console.log("bread", bread);
                $.ajax({
                    type: "POST",
                    url: SITE_URL + 'createsandwich/quickEditSandwich',
                    data: {'id': id},
                    success: function(data) {
                        
                        data = JSON.parse(data);
                        console.log("data", data);
                        var bread_options_html = '';

                        //console.log("sandwich_name", data.sandwich_name);
                        var sandwich_name = data.sandwich_name.replace("&amp;", "&");
                        var sandwich_name = sandwich_name.replace("&AMP;", "&");
                        console.log("sandwich_name", sandwich_name);

                        $.each(data.bread_data, function(index, bread_data){
                           var selected =  bread_data.item_name == bread[0] ? "selected" : "";

                            bread_options_html += '<option '+ selected +' data-shape="'+ bread_data.bread_shape +'" data-bread_type="'+ bread_data.bread_type +'" data-type="replace" data-id="'+ bread_data.id +'" data-itemname="'+ bread_data.item_name +'" value="'+ bread_data.id +'" data-price="'+ bread_data.item_price +'" data-categoryid="'+ bread_data.category_id +'" data-image="'+ bread_data.item_image +'" data-item_image_sliced="'+ bread_data.item_image_sliced
                         +'">'+ bread_data.item_name +'</option>';
                        })
                        $("#sandwiches_quick_edit #bread_drpdown").html(bread_options_html);
                        $("#sandwiches_quick_edit input[name='sandwich_name']").val(sandwich_name);
                        $("#sandwiches_quick_edit #save-quickedit-sandwich").attr("data-issaved", isSaved);
                    }
                })

                
                self.retrive_data_on_quickEdit(id);
                self.slide_function_on_quick_edit();
            })

        $('#sandwiches_quick_edit_add_item a.cancel-add-item').click(function(e){
            e.stopPropagation();
            $("#sandwiches_quick_edit_add_item").hide();
         })
    }
}

$(document).ready(function() {
    mobile_ha.sandwichcreator.init();
    
});
