<?php
session_start();

require_once 'graph_sdk/Facebook/autoload.php';


// use Facebook\FacebookSession;
use Facebook\Facebook;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\Authentication\AccessToken;
use Facebook\Authentication\OAuth2Client;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\eXCEPTIONS\FacebookAuthorizationException;



class FB_API 
{
    
    
    public  $scope      =  array('email,user_friends'); //publish_actions
    public  $redirURI   = FB_REDIR_URL;
    public  $helper;
    public  $appid      = FB_APP_ID; 
    public  $secret     = FB_APP_SECRET;
    public  $isValidTkn = false;
    public  $redirpaths = '';
    public  $fb;
    public  $fbApp;
    public  $fbClient;
    
    /*public function setSession(){
    	FacebookSession::setDefaultApplication($this->appid,$this->secret);
    	
    }*/
    public function FB_con(){
        $this->fb = new Facebook(array(
        'app_id' => $this->appid, // Replace {app-id} with your app id
        'app_secret' => $this->secret,
        'default_graph_version' => 'v2.9',
        ));
        $this->fbApp = $this->fb->getApp();
        $this->fbClient = $this->fb->getClient();

    }
    
    public function FB_check_login(){
        $this->FB_con();
        $this->helper = $this->fb->getRedirectLoginHelper();
        

        try {
            $accessToken = $this->helper->getAccessToken();
            
          } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
          } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
          }
             
        catch (Exception $ex) {
        // When validation fails or other local issues
        	print_r($ex->getMessage());
        }
        
        // see if we have a session
        if (!isset($accessToken)) {
          return false; 
            
        } else {
            $_SESSION['access_token'] = $accessToken->getValue();
            return true;
        }
        
       
    }
    
    
    function get_logout_URL(){
    	 
    	//FacebookSession::setDefaultApplication($this->appid,$this->secret);
    	if(isset($_SESSION['access_token']) && $_SESSION['access_token']){
        $this->FB_con();
        $this->helper = $this->fb->getRedirectLoginHelper();
    		//$session = new FacebookSession($_SESSION['access_token']);
    	
    	return $logoutUrl = $this->helper->getLogoutUrl($_SESSION['access_token'], SITE_URL.'myaccount/signout');
    	} else {
    		return SITE_URL.'myaccount/signout';
    	}
    	
    }
    
  
    
    
    function FB_get_url()    {
      
    	//$this->helper->disableSessionStatusCheck();
        $this->FB_con();
        $this->helper = $this->fb->getRedirectLoginHelper();
        return $this->helper->getLoginUrl($this->redirURI,$this->scope);
        
    }
    
    
    function FB_create_accessToken()
    {
        
        //$session = FacebookSession::newAppSession($this->appid, $this->secret);
        $this->FB_con();
        $this->helper = $this->fb->getRedirectLoginHelper();
        try {
            $accessToken = $this->helper->getAccessToken();
          } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
          } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
          } 

         $accessToken->getValue();
    }
    
    
    function FB_is_valid_token($token)
    {
        //$accessToken = new AccessToken($token);
        try {
            
            $Oauth  = new OAuth2Client($this->fbApp,$this->fbClient);
            
            $isvalid=$Oauth->debugToken($token);
        }
        catch (FacebookSDKException $e) {
            echo 'Error getting access token info: ' . $e->getMessage();
            exit;
        }
        return $isvalid['Valid'];
    }
    
    
     
    //for retriveing user data
    function FB_get_user($token)
    {

      //if($this->FB_is_valid_token($token) === true ){
     
        $this->FB_con();

        try {
        // Returns a `Facebook\FacebookResponse` object
        $response = $this->fb->get('/me?fields=first_name,last_name,email,id,friends',$token);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
        }

        $data = $response->getDecodedBody();
        $list=array();
        //echo "$data-->first_name".$data->first_name; die;
       foreach ($data['friends']['data'] as $fr) {
            
            
            $list['user_frnd_count'] = $data['friends']['summary']['total_count'];
                    if ($fr['name']) {
                        $list['name'][] = $fr['name'];
                    }
                    if ($fr['id']) {
                        $list['id'][] = $fr['id'];
                    }
            
        }
        
        $main['uid']               = $data['id'];
        $main['first_name']        = $data['first_name'];
        $main['last_name']         = $data['last_name'];
        $main['email']             = $data['email'];
        $main['friend_data'] = $list;
        
        return $main;
        
    //} else { return false; }
    }
    
    
   //for posting data to facebook 
   public  function FB_post_fb($token,$Array){
   	
   if($this->FB_is_valid_token($token) === true ){
  	    $session = new FacebookSession($token);
  	       if($session) {
   	             try {
   	                  $response = (new FacebookRequest(
   					   $session, 'POST', '/me/feed', array(
   							'link'        => $Array['link'],
   							'message'     => $Array['message'],
   					   		'caption'     => $Array['caption'],
   					   		'picture'     => $Array['picture'],
   					   		'name'        => $Array['name'],
   					   		'description' => $Array['description'],
   					    )
   			            ))->execute()->getGraphObject();
   	               //echo "Posted with id: " . $response->getProperty('id');
   	                } catch(FacebookRequestException $e) {
   	
   			      echo "Exception occured, code: " . $e->getCode();
   			      echo " with message: " . $e->getMessage();
   	           }
             } 
           }
           
           if(isset($response)){
           	
           if(is_object($response)){	
         return $response->getProperty('id');

           	} else { return false; }
           	
           } else return false; 
    	
   }
       
}
