<?php

//Directories
define('DIR_BASE', 'app/base/');
define('DIR_CONF', 'app/conf/');
define('DIR_CONTROLLER', 'app/controller/');
define('DIR_DB', 'app/db/');
define('DIR_MODEL', 'app/model/');
define('DIR_SMARTY', 'app/smarty/');
define('DIR_THEME', 'app/theme/');
define('PROJECT_BASE', '_mobile_new/');
define('SITE_URL', 'http://mobilenew.ha/');
define('LOG_IN_TIME', 86400); //in seconds 86400 = 24 hours (max inactive time user allowed to log in)
define('TITLE','BARNEY BROWN');


define('API_URL','http://api.ha/');
define('API_USERNAME','hashburyuser');
define('API_PASSWORD','hashburypass');

define('ADMIN_URL','http://admin.ha/');
define('CMS_URL','http://cms.ha/');
define('SALAD_IMAGE_PATH','http://admin.ha/upload/products/');
//facebook api config..

define('DIR_FBAPI','app/FB_API/');
define('FB_APP_ID','557060384864429'); //unque app id from Facebook app
define('FB_APP_SECRET','40b8c2c1383e883f4e660d3a07fec320'); //unque app secret key from Facebook app
define('FB_REDIR_URL','http://mobilenew.ha/facebook/data/'); //url to which facbook redirect after login

define('SANDWICH_UPLOAD_DIR','../sandwich_uploads/');
define('SANDWICH_IMAGE_PATH','http://mobilenew.ha/sandwich_uploads/');
define("ENVIRONMENT", "STAGING");

define('MAX_TIP', 100);
define("MIN_TIP", 2);
define("TAX",  0.08875);
define("BASE_FARE",7);

define('CACHE_MANIFEST','data.appcache');
define('CACHE_PATH','../_admin/upload/');