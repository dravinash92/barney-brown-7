<?php
class Base extends INSPIRE
{
    
    var $controller;
    var $action;
    var $postdata;
    var $title;
    var $navigation;
    var $db;
    var $languages;
    var $vehicle_class;
    var $body_types;
    var $devices;
    var $apps;
    
    
    function init()
    {        
        $this->SessionStart();
        $this->ProcessUrl();
        $this->db = $this->db();
        $this->create_template_dir();
        
        //clear session on signout
        if(isset($this->UrlArray[1]) && $this->UrlArray[1] == 'signout ' ){
        	session_destroy(); 
        }
        
        $this->resume_session();//Resume session if cookie exists within 30 days.
       
        if ($this->UrlArray[0] != '') {
            $this->controller = $this->UrlArray[0];
            
        } else {
            $this->controller = 'home';
        }
        
        if ($this->UrlArray[1] != '') {
            $this->action = $this->UrlArray[1];
        } else {
            $this->action = 'index';
        }
        
         
        
        $query  = "SELECT * FROM site_social_links WHERE id = 1 ";
        $result = $this->db->Query($query);
        $social = $this->db->FetchAllArray($result);
        if (isset($social[0])) {
            $social = $social[0];
        } else {
            $social = "";
        }
        $social = json_decode(json_encode($social));
        
        
        $params = $this->process();
        $this->resume_session();
		$this->cache_check();
        $this->smarty->assign('Title', @$params->title);
        $this->smarty->assign('title_text', TITLE);
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('API_URL', API_URL);
        $this->smarty->assign('ADMIN_URL', ADMIN_URL);
        $this->smarty->assign('CMS_URL',CMS_URL);
        $co = 0;
       
            
            $checkoutObj = new Checkout();
            $co          = $checkoutObj->getCartCount();
       
        
        $this->smarty->assign('signOutURL',$this->get_logout_URL());
        $this->smarty->assign('cart', $co);
        $this->smarty->assign('social', $social);
		
		$this->smarty->assign('BASE_FARE',BASE_FARE);
		
		$this->smarty->assign('deviceSpecificCss', $this->getUserAgent());
         $requestURI = $_SERVER['REQUEST_URI'];
        if ($requestURI) {
            $uriSegments = explode('/', $requestURI);
            if ($uriSegments[1] == 'checkout' && $uriSegments[2] == 'index') {
                $this->smarty->assign('CLASS_NAME', 'class="body-fixed"');
            } else {
                $this->smarty->assign('CLASS_NAME', 'class=""');
            }
        }
		
        $this->smarty->display('index.tpl');
		
        
    }
	
	public function getUserAgent(){
		
		$userAgent = $_SERVER["HTTP_USER_AGENT"];
		
		
		if(preg_match("/(iPhone)/i",$userAgent)){
			return "iphone.css?v=2.2";
		}else if(preg_match("/(Android)/i",$userAgent)){
			return "android.css?v=2.2";
		}else{
			return "screen.css?v=2.2";
		}
	}
	
	public function cache_check(){
		return;
		$data = file(CACHE_MANIFEST); 
		$data = str_replace("#",'',$data[1]);  
		$data  = explode(':',$data);
		$date  = $data[0];
		$version = $data[1]; 
		$version = trim($version, "a..zA..Z"); 
		
        $str = strtotime(date("M d Y ")) - (strtotime($date));
		//updating cache after a week or 7 days.
        if( floor($str/3600/24) > 7 ){
			$this->cache_update($version+1);
		}
		
	}
	
	public function cache_update($version){ 
	    $path  = CACHE_PATH;
        $files = scandir($path);
        
     
		$filesList  = "CACHE MANIFEST".PHP_EOL;
		$filesList .= "# ".date("Y-m-d").":v".$version;
		$filesList .= PHP_EOL;
		$filesList .= "# version ".$version;
		$filesList .= PHP_EOL;
		$filesList .= PHP_EOL;
		$filesList .= PHP_EOL;
		$filesList .= "CACHE:".PHP_EOL;
		$filesList .= SITE_URL."app/stylesheets/android.css".PHP_EOL;
		$filesList .= SITE_URL."app/stylesheets/backgrounds.css".PHP_EOL;
		$filesList .= SITE_URL."app/stylesheets/iphone.css"; 
		$filesList .= PHP_EOL;
		
		  foreach($files as $file){
			if(!is_dir($path.$file)) {
				$filesList .=  ADMIN_URL."upload/".$file.PHP_EOL;
		 	}
		  }
		  
		  $imagePath = 'app/images';
		  $imgList   = scandir($imagePath);
		  foreach($imgList as $imgLists){
		  	if(!is_dir($imagePath.$imgLists)) {
		  		$filesList .=  SITE_URL."app/images/".$imgLists.PHP_EOL;
		  	}
		  }
		  
		$filesList .= PHP_EOL;
		$filesList .= PHP_EOL;
		$filesList .= "NETWORK:";
		$filesList .= PHP_EOL;
		$filesList .= rtrim(SITE_URL, '/');
		$filesList .= PHP_EOL;
		$filesList .= SITE_URL;
		$filesList .= PHP_EOL;
		$filesList .= SITE_URL.'home/index';
		$filesList .= PHP_EOL;
		$filesList .= SITE_URL.'home';
		$filesList .= PHP_EOL;
		$filesList .= "*";
		 
		file_put_contents(CACHE_MANIFEST, $filesList);
		 
	}
    
    function create_template_dir()
    {
        if (!is_dir('app/theme/templates_c'))
            mkdir('app/theme/templates_c', 0777);
    }
    
    function resume_session()
    {
    	 
        if(isset($_COOKIE['user_id']) && isset($_COOKIE['user_mail']) && isset($_COOKIE['user_fname']) && isset($_COOKIE['user_lname'])){

		
		        // if (! isset ( $_SESSION ['uid'] ) && isset ( $_SESSION['uname']) && !in_array('createsandwich', $this->UrlArray)) 	{					

        		// 	unset($_SESSION['uid']);
        		// }
        		// else {
				
        		// }
            $_SESSION['uid']= $_COOKIE['user_id'];
			$_SESSION['start_time'] = strtotime("now");
			$_SESSION['uname']=$_COOKIE['user_fname'];
			
			$cookietime = time()+(86400 * 30 * 3); //90 days from now extended	
			setcookie("user_mail", $_COOKIE['user_mail'], $cookietime, '/', NULL);
			setcookie("user_fname", $_COOKIE['user_fname'], $cookietime, '/', NULL);
			setcookie("user_lname", $_COOKIE['user_lname'], $cookietime, '/', NULL);
			setcookie("user_id", $_COOKIE['user_id'], $cookietime, '/', NULL);

		}
    }
    
    function smarty_filter( $tpl_output, &$smarty )
    {
        $tpl_output = htmlspecialchars( $tpl_output ) ;
        return $tpl_output;
    }
    
    function process()
    {   
    	
        
        if(!in_array('menu', $this->UrlArray) && !in_array('facebook', $this->UrlArray)){
        	setcookie("reopen_fb_pop", 'false', time() + 60 * 60 * 24 * 60, "/");
        }
    	


    	
    	if(!class_exists($this->controller) ){
            $this->controller = "home";
            $this->LoadSite();
    		
    	} else {
            $this->LoadSite();
        }
    	
        $obj           = new $this->controller;
        $obj->postdata = $this->postdata;
        $obj->smarty   = $this->smarty;
        $obj->db       = $this->db;
        $obj->UrlArray = $this->UrlArray;
        
        if(!method_exists($obj,$this->action)){
        	header('Location: '.SITE_URL.'error/_404/');
        	exit();
        }
        
        $obj->{$this->action}();
         
        return $obj;
    }
    
    
    function br2nl($string)
    {
        return str_replace(array(
            "\r\n",
            "\r",
            "\n"
        ), "\\n", $string);
    }
    
    public function load_model($modelname)
    {
        spl_autoload_register(function($class)
        {
           if(file_exists(DIR_MODEL . $class . '.php')) require_once DIR_MODEL . $class . '.php';
        });
        return new $modelname;
    }
    
		
}
