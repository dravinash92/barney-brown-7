<?php 

class Model extends INSPIRE {

public $db;
  public function __construct(){
  //setting up db object
  return $this->create_db();
  }

 //create db connection
  public function create_db(){
	require(DIR_CONF.'conf.my_db.php');
        return $db = new BaseClass($b_type,$Cfg_host,$Cfg_user,$Cfg_password,$Cfg_db);
  }
  
  
  public function receive_data( $url, $params=array(), $test = 0 ){
  
  	$params['api_username']  = API_USERNAME;
  	$params['api_password']  = API_PASSWORD;
	 $params['ENVIRONMENT']  = ENVIRONMENT; 

  
    $params                  = $this->process_input($params);
    $params = http_build_query($params);
  	//invoking curl
  	$ch = curl_init();
    
    
  	//set the url, number of POST vars, POST data
  	curl_setopt($ch,CURLOPT_URL,$url);
  	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  	curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'BB_DATA: mobile'
      ));
    
    if(  $test == 1 )
      curl_setopt($ch, CURLINFO_HEADER_OUT, true);


  	//execute post
  	$result = curl_exec($ch);
  
    if(  $test == 1 )
    {
        $headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT );
        echo "<pre>";
        echo "URL: url\n";
        echo "HEADER: $headerSent\n";
        echo "PARAMS: \n";
        print $params ;
        echo "\n\nRESULT: \n\n";
        echo $result;
        echo "</pre>\n\n\n";
    } 
  	//close connection
  	curl_close($ch);
    return $result;
  }
  

  public function sendMail($params){
  
  	if(!isset($params['from'])) $params['from'] = 'noreply@cms.ha.allthingsmedia.com';
  
  	$to      = $params['to'];
  	$subject = $params['subject'];
  	$message = $params['message'];
  	$headers  = array(
  
  			"From: ".TITLE." <".$params['from'].">",
  			//"Reply-To: ". $params['from'],
  			"X-Mailer: PHP/" . phpversion(),
  
  			//"X-Originating-IP: ". $_SERVER['SERVER_ADDR'],
  			"MIME-Version: 1.0",
  			
  	);
  	// Send
  	if(	mail($to, $subject, $message, implode("\r\n", $headers) ) ) return true; else  return false;
  
  
  
  }
  
  
  private function process_input(&$data){
    foreach($data as $key => $value){
        //If $value is an array.
        if(is_array($value)){
            //We need to loop through it.
            $data[$key] = $this->process_input($value);
        } else{
            $data[$key] = urlencode($value);
        }
    }
    return $data;
  }
  public function clean_for_sql( $var )
    {
        return mysqli_real_escape_string($this->db->connection, $var );
    }
}