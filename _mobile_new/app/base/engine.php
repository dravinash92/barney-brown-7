<?php
class INSPIRE extends FB_API
{
    
    var $DirTree = 0; //url offset
    var $DirLimit = 10; //url variable limit 
    var $UpDirCounter;
    var $UrlArray;
    var $smarty; 
    var $ShortCuts = array(); 
    var $db;


    
    
  public function SessionStart() {
  	   if(!isset($_SESSION)){
         // server should keep session data for AT LEAST 30 days i.e 3600 * 24 * 30
        ini_set('session.gc_maxlifetime', 2592000);

        // each client should remember their session id for EXACTLY 30 days i.e 3600 * 24 * 30
        session_set_cookie_params(2592000);
         session_start();
       }

       if( ((strtotime("now") - @$_SESSION['start_time']) > LOG_IN_TIME ) && @$_SESSION['start_time'] && @$_SESSION['uid'] ){
        	//$this->curl_data(API_URL.'customer/delete_all_orders/',array("user_id"=>@$_SESSION['uid']));
        	//unset($_SESSION);
        	//session_destroy();
        	if($this->UrlArray[1] != 'is_session' ){
        		//header("Location:".SITE_URL);
        	}
        
        	
        }  
        $_SESSION['start_time'] = strtotime("now");      
 }  
 
 
 public function curl_data($url,$params=array()){
 
 	$params['api_username']  = API_USERNAME;
 	$params['api_password']  = API_PASSWORD;
 	array_walk($params,function(&$val,$key){
 		$val = urlencode($val);
 	});
 	//$params                  = $this->process_input($params);
 
 	//invoking curl
 	$ch = curl_init();
 	//set the url, number of POST vars, POST data
 	curl_setopt($ch,CURLOPT_URL,$url);
 	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
 	curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
 	//execute post
 	$result = curl_exec($ch);
 	//close connection
 	curl_close($ch);
 	return $result;
 }
    
	
	function db(){
		require(DIR_DB.'database.svc.php');
		return $db;
	}

    function ProcessUrl() {
    	$url = str_replace(PROJECT_BASE,'',$_SERVER['REQUEST_URI']);
        //$this->UrlArray = explode("/",$_SERVER['REQUEST_URI']);
    	$this->UrlArray = explode("/",$url);
        array_shift ($this->UrlArray);
        for($i=0; $i < $this->DirTree; $i++)
        {
            array_shift ($this->UrlArray);
        }

        $Counter=count($this->UrlArray);
        for($i=0; $i<$Counter; $i++) {  $this->UrlArray[$i]=str_replace('.html', '', $this->UrlArray[$i]); }
        for($i=1; $i<$Counter; $i++) {  $this->UpDirCounter.='../'; }

        for($i=0; $i<$this->DirLimit; $i++) {  if(!isset($this->UrlArray[$i])) $this->UrlArray[$i]=''; $this->UrlArray[$i]=trim($this->UrlArray[$i]);}

    }
    
    function PrepareLink($title, $ext='.html', $step=';') {
        $prepared_title=trim($title);
        $prepared_title = str_replace(" ", $step, $prepared_title);
        $prepared_title = ereg_replace("[^0-9a-zA-Z_,-]","", $prepared_title);
        $prepared_title.=$ext;
        return $prepared_title;
    }
   
    
    function LoadSite()
    {
    	
         $this->smarty = new SmartyBC();
        $this->smarty->setTemplateDir( DIR_THEME."templates");
        $this->smarty->setConfigDir( DIR_THEME."config");
        $this->smarty->setCacheDir(DIR_THEME."cache");
        $this->smarty->setCompileDir(DIR_THEME."templates_c");
        //$this->smarty->allow_php_tag=true;
        $this->smarty->assign('BasePath',$this->UpDirCounter);
        
        // generate random letter - CACHE (remove it in final version)
        $r = rand(0,1);
        $c = ($r==0)? rand(65,90) : rand(97,122);
        $this->smarty->assign('RandomLetter',chr($c));
        
        // Sections
        
       if(isset($_COOKIE['user_mail'])){ 
        $this->smarty->assign('user_mail',$_COOKIE['user_mail']);
        }
        if(isset($_COOKIE['user_fname'])){
        	$this->smarty->assign('user_fname',$_COOKIE['user_fname']);
        }
        if(isset($_COOKIE['user_lname'])){
        	$this->smarty->assign('user_lname',$_COOKIE['user_lname']);
        } 
        
        $this->smarty->assign("sandwich_img_live_uri",SANDWICH_IMAGE_PATH);
        $this->smarty->assign("Footer",'footer.tpl');
        $this->smarty->assign("Header",'header.tpl');
		
    }  
}