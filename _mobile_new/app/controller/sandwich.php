<?php
ini_set('display_errors',0);
class Sandwich extends Base
{
	public $model;
	/**
	 *
	 * Load model
	 *
	 */
	public function __construct()
	{
		$this->model = $this->load_model('sandwichModel');
	}
	/**
	 *
	 * Load sandwich page
	 *
	 */
	function index()
	{

		$this->title = TITLE;

	}
	/**
	 *
	 * View sandwich gallery
	 *
	 */
	
	function isMobile() {
		$userAgent = $_SERVER["HTTP_USER_AGENT"];
		return preg_match("/(iPhone|iPod|blackberry|Android|BlackBerry)/i",$userAgent);
	}
	
	function gallery()
	{
		
 		if(isset($_REQUEST['galleryItem']) && $_REQUEST['galleryItem']){ 
		if($this->isMobile() == false){
			header("Location:".CMS_URL."sandwich/gallery/?galleryItem=".$_REQUEST['galleryItem']);
			exit;
		}} 

		$this->title = TITLE;
		$this->smarty->assign('SITE_URL', SITE_URL);
		$this->smarty->assign('ADMIN_URL', ADMIN_URL);
		$this->smarty->assign('CMS_URL', CMS_URL);
		$this->smarty->assign('heading','SANDWICH GALLERY');

		$model = $this->model;

		$limit = 20;
		$page  = empty($_GET['page']) ? 1 : (int) $_GET['page'];
		$start = $page == 1 ? 0 : (($page - 1) * $limit - 1);

		if (isset($_GET['sortid']))
			$sort_id = $_GET['sortid'];
		else
			$sort_id = 0;

		$data['sort_id'] = $sort_id;
		$data['start']   = $start;
		$data['limit']   = $limit;

		$serch_term = @$_POST['search'];

		if(isset($serch_term)){ 
			$data = array('name'=>$serch_term, 
							'limit'=>$limit,
							'start'=>$start,
							'sort_id'=>$sort_id); 
			$totalCount = $model->get_searchItem_count($data);
		}
		else
		{
			$data = array( 'limit'=>50,'start'=>0,'sort_id'=>$sort_id );
			$totalCount = $model->get_gallery_data_count($data);
		}
		
		$link = SITE_URL . "sandwich/gallery/?sortid=" . $sort_id;
		$this->smarty->assign('link', $link);
		$this->smarty->assign('page', $page);



		
		if( $totalCount == '' || $totalCount == 'undefined' )
	 		$totalCount = 0;
		$this->smarty->assign('totalCount', $totalCount);

		$lastPage = (int) ($totalCount / $limit);
		$remain   = $totalCount % $limit;
		if ($remain)
			$lastPage = $lastPage + 1;
		$this->smarty->assign('lastPage', $lastPage);

		
		$sandwiches = $model->gallery_load_data($data);
	    
		if(count($sandwiches)>0){  
	     	$gallery_datas = $model->process_user_data($sandwiches);
			$gallery_data  = $gallery_datas['data'];
		} else {
			$gallery_data = array();
		}

		/* saved data */
		$uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
		if ($uid) 
		{ 
            $getSavedSandwiches = $this->model->get_user_sandwich_ids($uid);
        } else {
            $getSavedSandwiches = array();
        }

        $menu_added_from_array = array_column($getSavedSandwiches, 'menu_added_from');
        $id_array = array_column($getSavedSandwiches, 'id' );
        $is_public_array = array();
         foreach ($getSavedSandwiches as $key => $value) {
         	if($value['menu_added_from'] != 0)
        		$is_public_array[$value['menu_added_from']] = $value['is_public'];
        	else
        		$is_public_array[$value['id']] = $value['is_public'];
        }

        $getSavedSandwiches = array_merge($menu_added_from_array, $id_array);
		$this->smarty->assign('saved_data',$getSavedSandwiches);
	    $this->smarty->assign('is_public_array',$is_public_array);

		$order_data    = $this->model->get_order_data();
		$categories    = $model->getSandwichCategories();
		$categoryItems = $model->getSandwichCategoryItems();

		$this->smarty->assign('categoryItems', $categoryItems);
		$this->smarty->assign('categories', $categories);
		$this->smarty->assign('sort_id', $sort_id);
		$this->smarty->assign('serch_term',$serch_term);
		$this->smarty->assign('order_data', $order_data);
		$this->smarty->assign('GALLARY_DATA', $gallery_data);
		
		
		if (isset($gallery_datas['count']))
			$this->smarty->assign('gal_count', $gallery_datas['count']);
		$this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
		$this->smarty->assign('total_item_count', $totalCount);
		$this->smarty->assign('content', "sandwich-gallery.tpl");
	}
	
	public $no_items = array();
	
	
	function filters()
	{

		$filter = @$_POST['filter'];
		$sName = @$_POST['searchTerm'];
		$filter = json_decode($filter);
		$sandwich_items = array(); 
		foreach ($filter as $key => $value) 
		{ 
		    if (is_array($value) && !empty($value)) 
		    { 
		      foreach ($value as $val) 
		      {
		      	$sandwich_items[] = $val;
		      }
		    } 

		}

		$data = array("s_items"=>$sandwich_items, "sortBy"=> @$_POST['sortBy']);
		if(!empty($sName))
		{
			$data['name'] = $sName;
		}
		
		$sandwich_data = $this->model->sandwich_filter($data);
		$sandwich_data_count = $this->model->sandwich_filter_count($data);
		

		if (isset($sandwich_data) && count(@$sandwich_data) > 0) 
		{
		 	$gallery_datas = (array) $this->model->process_user_data($sandwich_data);
		 	
		 	$gallery_data  = (array)$gallery_datas['data'];
			
		} else {
		 	$gallery_data = array();
		 }
		$totalCount = $sandwich_data_count;
		if( $totalCount == '' || $totalCount == 'undefined' )
		 	$totalCount = 0;

		$uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
		if ($uid) 
		{ 
            $getSavedSandwiches = $this->model->get_user_sandwich_ids($uid);
        } else {
            $getSavedSandwiches = array();
        }

        $menu_added_from_array = array_column($getSavedSandwiches, 'menu_added_from');
        $id_array = array_column($getSavedSandwiches, 'id' );
        $is_public_array = array();
         foreach ($getSavedSandwiches as $key => $value) {
         	if($value['menu_added_from'] != 0)
        		$is_public_array[$value['menu_added_from']] = $value['is_public'];
        	else
        		$is_public_array[$value['id']] = $value['is_public'];
        }

        $getSavedSandwiches = array_merge($menu_added_from_array, $id_array);
		$this->smarty->assign('saved_data',$getSavedSandwiches);
	    $this->smarty->assign('is_public_array',$is_public_array);
		$this->smarty->assign('sandwich_count',$totalCount);
		$this->smarty->assign('GALLARY_DATA', $gallery_data);
		$this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
		echo $this->smarty->fetch("sandwich-gallery-ajax-search.tpl");
		exit;
	}
	
	/**
	 *
	 * View sandwich gallery more
	 *
	 */
	function get_more_gallery()
	{
		
		$model           = $this->model;
		$data['sort_id'] = $_POST['sort_id'];
		$count           = $_POST['count'];
		$data['name']  		= $_POST['searchTerm'];
		$limit           = 20;
		$page            = round($count / $limit);
		$data['limit']   = $limit;

		if ($page == 0)
			$data['start'] = $count;
		else {
			$data['start'] = $limit * $page;
		}

		$data = $model->gallery_load_data($data);

		if ( count($data) > 0 ) {
			$gallery_datas = $model->process_user_data($data);
			$gallery_data  = $gallery_datas['data'];
		} else {
			$gallery_data = array();
		}
		$uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
		if ($uid) 
		{ 
            $getSavedSandwiches = $this->model->get_user_sandwich_ids($uid);
        } else {
            $getSavedSandwiches = array();
        }

        $menu_added_from_array = array_column($getSavedSandwiches, 'menu_added_from');
        $id_array = array_column($getSavedSandwiches, 'id' );
        $is_public_array = array();
         foreach ($getSavedSandwiches as $key => $value) {
         	if($value['menu_added_from'] != 0)
        		$is_public_array[$value['menu_added_from']] = $value['is_public'];
        	else
        		$is_public_array[$value['id']] = $value['is_public'];
        }

        $getSavedSandwiches = array_merge($menu_added_from_array, $id_array);
		$this->smarty->assign('saved_data',$getSavedSandwiches);
	    $this->smarty->assign('is_public_array',$is_public_array);

		$this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
		$this->smarty->assign('GALLARY_DATA', $gallery_data);
		$this->smarty->assign('SITE_URL', SITE_URL);
		$this->smarty->assign('ADMIN_URL', ADMIN_URL);
		echo $this->smarty->fetch("sandwich-gallery-more.tpl");
		exit;
	}

	/**
	 *
	 * Add menu item
	 *
	 */
	function add_menu_multi()
	{

		$model = $this->model;
		$data  = array(
				'id' => $_POST['id'],
				'uid' => $_POST['uid'],
				'toast_sandwich' => $_POST['toast_sandwich']
		);
		$model->add_menu_multi($data);
		exit;

	}
	/**
	 *
	 * Remove menu item
	 *
	 */
	function remove_menu()
	{

		$model = $this->model;
		$data  = array(
				'id' => $_POST['id'],
				'uid' => $_POST['id']
		);
		$model->remove_menu($data);
		exit;
	}
	/**
	 *
	 * Add product to cart
	 *
	 */
	function add_to_cart()
	{	
		if (!isset($_SESSION['uid']))
			exit("0");

		$model     = $this->model;
		$item_id   = @$_POST['itemid'];
		$data_type = @$_POST['data_type'];
		$user_id   = @$_POST['user_id'];
		if (!$user_id) {
			$user_id = $_SESSION['uid'];
		}
		$toast = 0;
		if (isset($_POST['toast'])) {
			$toast = $_POST['toast'];
		}

		$qty = @$_POST['qty'];

		$descriptor_id = $extra_id = "";

		
		$spcl_instructions ="";

		if(isset($_POST['spcl_instructions']))
			$spcl_instructions = $_POST['spcl_instructions'];

		if(isset($_POST['clickedItems'])){
			
			$extra_id = $_POST['clickedItems'];
			
		}

		


		$item = $model->create_order($item_id, $user_id, $data_type, $qty, $toast, $spcl_instructions, $extra_id);



		if ($item) {


			$_SESSION['orders']['item_id'][]       = $item_id;
			$_SESSION['orders']['order_item_id'][] = $item->order_id;
			$_SESSION['orders']['item_qty']        = $item->count;

		}

		if (isset($_SESSION['orders']['item_qty'])) {
			echo $_SESSION['orders']['item_qty'];
		} else {
			echo 0;
		}
		exit;
	}
	/**
	 *
	 * Update Toastmenu
	 *
	 */
	function updateToastmenu()
	{
		$model = $this->model;
		$data  = array(
				'item_id' => $_POST['item_id'],
				'toast' => $_POST['toast']
		);
		echo $model->updateToastmenu($data);
		exit;
	}
	/**
	 *
	 * Add like
	 *
	 */
	function addLike()
	{
		$model = $this->model;
		$data  = array(
				'id' => $_POST['id'],
				'uid' => $_POST['uid']
		);
		echo $model->addLike($data);
		exit;

	}
	/**
	 *
	 * Remove cart item
	 *
	 */
	function remove_cart_item()
	{

		$model         = $this->model;
		$order_item_id = @$_POST['order_item_id'];
		$item_id       = @$_POST['itemid'];

		$result = $model->remove_order($order_item_id, $_SESSION['uid']);

		if ($result) {

			if (isset($_SESSION['orders']['item_id'])) {

				if (in_array($item_id, @$_SESSION['orders']['item_id'])) {
					$key = array_search($item_id, $_SESSION['orders']['item_id']);
					unset($_SESSION['orders']['item_id'][$key]);
				}
			} else {

			}


			if (isset($_SESSION['orders']['order_item_id'])) {

				if (!in_array($order_item_id, @$_SESSION['orders']['order_item_id'])) {
					$key = array_search($order_item_id, $_SESSION['orders']['order_item_id']);
					unset($_SESSION['orders']['order_item_id'][$key]);
				}
			}
			

		}



		if (isset($_SESSION['orders']['item_id'])) {
			echo count($_SESSION['orders']['item_id']);
		} else {
			echo 0;
		}
		exit;
	}
	
	function getAllproductsExtras(){
		$model        = $this->model;
		$model->getAllproductsExtras();
		exit;
	}
	
	
	function get_individual_sandwich_data(){
		$id   = $_REQUEST['id'];
		$uid   = $_POST['uid'];
		$data = $this->model->get_individual_sandwich_data($id);
		$data = json_decode($data);
		if(isset($data->Data)){  $data = $data->Data;  } else { $data = array(); }
		echo json_encode($this->model->process_individual_sandwich_data($data));
		exit;
	}


	function featuredSandwiches()
	{
		$this->smarty->assign('LOGGED_IN',true);
	    $this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
	    $this->smarty->assign('SITE_URL',SITE_URL);
		$model        = $this->model;
		$sandwich_data = $model->getSandwichOfTrending();
		$uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
		if ($uid) 
		{
            
            $getSavedSandwiches = $this->model->get_user_sandwich_ids($uid);
        } else {
            $getSavedSandwiches = array();
        }
        

        $menu_added_from_array = array_column($getSavedSandwiches, 'menu_added_from');
        $id_array = array_column($getSavedSandwiches, 'id' );
        
        $is_public_array = array();
         foreach ($getSavedSandwiches as $key => $value) {
         	if($value['menu_added_from'] != 0)
        		$is_public_array[$value['menu_added_from']] = $value['is_public'];
        	else
        		$is_public_array[$value['id']] = $value['is_public'];
        }

        $getSavedSandwiches = array_merge($menu_added_from_array, $id_array);

		if(count($sandwich_data)>0){  
	     $featured_datas = $model->process_user_data($sandwich_data);
	     $featured_data = $featured_datas['data'];
	    } else {
	    	$featured_data = array();
	    }

	    
	    $this->smarty->assign('featured_data',$featured_data);
	    $this->smarty->assign('saved_data',$getSavedSandwiches);
	    $this->smarty->assign('is_public_array',$is_public_array);
	    $this->smarty->assign('heading','FEATURED SANDWICHES');
		
        $this->smarty->assign('content',"featured_sandwiches.tpl");
	}

	function savedSandwiches()
	{
		$uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];

		if ($uid) 
		{
            $menuData = array('uid'=>$uid, 'start' => 0, 'limit' => '' );
            $getSavedSandwiches = $this->model->get_user_sanwich_data($menuData);
        } else {
            $getSavedSandwiches = array();
        }

        if(count($getSavedSandwiches)>0){  
	     $saved_sandwich_datas = $this->model->process_user_data($getSavedSandwiches);
	     $saved_sandwich_data = $saved_sandwich_datas['data'];
	    } else {
	    	$saved_sandwich_data = array();
	    }
        
		$this->smarty->assign('LOGGED_IN',true);
	    $this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
	    $this->smarty->assign('SITE_URL',SITE_URL);
	    $this->smarty->assign('heading','MY SAVED SANDWICHES');
	    $this->smarty->assign('featured_data',$saved_sandwich_data);
        $this->smarty->assign('content',"savedSandwiches.tpl");
	}

	function sandwichMenu()
	{
		$this->title = TITLE;
	    $this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
		$this->smarty->assign('heading','SANDWICHES');

		$galleryCount = $this->model->get_sandwich_shared_creations_count();
		$this->smarty->assign('galleryCount', number_format($galleryCount));

		$menuModel = $this->load_model('menuModel');

		

		$uid = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];

		if ($uid) {
			$paramData = array('uid'=>$uid, 'start' => 0, 'limit' => 5 );

			$getSavedSandwiches= $this->model->get_user_sanwich_data($paramData);
			$getSavedSandwichIds = $this->model->get_user_sandwich_ids($uid);
	    }
        else {
            $getSavedSandwiches = array();
            $getSavedSandwichIds = array();
        }
        $is_public_array = array();
         foreach ($getSavedSandwichIds as $key => $value) {
         	if($value['menu_added_from'] != 0)
        		$is_public_array[$value['menu_added_from']] = $value['is_public'];
        	else
        		$is_public_array[$value['id']] = $value['is_public'];
        }

        $menu_added_from_array = array_column($getSavedSandwichIds, 'menu_added_from');
        $id_array = array_column($getSavedSandwichIds, 'id');

        $getSavedSandwichIds = array_merge($menu_added_from_array, $id_array);

       if(count($getSavedSandwiches)>0){  
	     $saved_sandwich_datas = $this->model->process_user_data($getSavedSandwiches);
	     $saved_sandwich_data = $saved_sandwich_datas['data'];
	    } else {
	    	$saved_sandwich_data = array();
	    }

	    //to get featured sandwiches
	    $trndData = array('limit' => 10 );
	    $sandwich_data = $this->model->getSandwichOfTrending($trndData);
		if(count($sandwich_data)>0){  
	     $featured_datas = $this->model->process_user_data($sandwich_data);     
	     $featured_data = $featured_datas['data'];
	    } else {
	    	$featured_data = array();
	    }

	    
        $fdata = array();
        if(isset($_SESSION['uid']))
        {

        	$fdata = $this->model->getFBfriendsMenu();
	        if (isset($fdata->Data)) 
	        {
	            $fdata = $this->model->_process($fdata->Data);
	        }
	        
	    }
       
        
        $this->smarty->assign('saved_sandwich_data',$saved_sandwich_data);
        $this->smarty->assign('featured_data',$featured_data);
        $this->smarty->assign('saved_sandwich_ids',$getSavedSandwichIds);
        $this->smarty->assign('fbdata', $fdata);
        $this->smarty->assign('is_public_array',$is_public_array);

        $this->smarty->assign('content',"sandwich_menu.tpl");
    }
	
	function make_sandwich_private()
	{
		$model  = $this->model;
		$data   = array('id'=>$_POST['id'],'uid'=>$_POST['uid'],'is_public' => $_POST['is_public'],'sType'=>$_POST['sType']);
		$model->make_sandwich_private($data);
		exit;
	}

	function remove_saved()
	{
		
		$model  = $this->model;
		$data   = array('id'=>$_POST['id'],'uid'=>$_POST['uid'],'sType'=>$_POST['sType']);
		$stype = $model->remove_saved($data);
		echo $stype;
		exit;
	}

	function getCurrentprice(){
		
		$desc = $_POST['pdata']['sandwich_desc'];
		$desc_id = $_POST['pdata']['sandwich_desc_id'];
		$brd = $_POST['pdata']['bread'];
		$pid = $_POST['pdata']['id'];
		$price = $this->model->get_sandwich_current_price($pid, $brd,$desc,$desc_id);

		echo $price;exit;
	}

	function set_flag()
	{
		$model                = $this->model;
        
        $model->send_flag_mail();
		echo $model->toggle_flag();
		exit;
	}
}
