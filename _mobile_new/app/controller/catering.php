<?php

class Catering extends Base
{
	public $model;
	/**
	 *
	 * Load model
	 *
	 */
	public function __construct()
	{
		$this->model = $this->load_model('cateringModel');
	}
	/**
	 *
	 * Load catering page
	 *
	 */
	function index()
	{
		$model       = $this->model;
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL', SITE_URL);
		$data              = array();
		$cateringData      = $model->get_catering_catgory_items(API_URL, $data);
		$cateringDataArray = json_decode($cateringData, true);
		$product = 'product';
        $this->smarty->assign('product', $product);

		//Category Item
		$count            = 0;
		$cateringdataList = Array();

		foreach ($cateringDataArray['Data'] as $catagory) {
			$cateringdataList[$count]['id']       = $catagory['id'];
			$cateringdataList[$count]['name']     = $catagory['category_identifier'];
			$cateringdataList[$count]['cat_name'] = $catagory['standard_cat_name'];
			$cateringdataList[$count]['data']     = $this->standardCatagoryItem($catagory);
			//for adding item
			$count++;
		}

		$this->smarty->assign('cateringdataList', $cateringdataList);
		$this->smarty->assign('uid', @$_SESSION['uid']);
		$nav_ops = array(
				'show' => false
		);
		$this->smarty->assign('salad_image_path',SALAD_IMAGE_PATH);
		$this->smarty->assign('heading','CATERING');
		$this->smarty->assign('SEC_NAV_OPS', $nav_ops);
		$this->smarty->assign('ACTIVE_MAIN_MENU_TEXT', 'catering');
		$this->smarty->assign('content', "catering.tpl");
	}
	/**
	 *
	 * List standard Catagory Item
	 *
	 */
	function standardCatagoryItem($data)
	{
		$model    = $this->model;
		$itemList = $model->get_standard_catgory_items_list(API_URL, $data);
		$itemList = json_decode($itemList, true);
		return $itemList['Data'];
	}

}