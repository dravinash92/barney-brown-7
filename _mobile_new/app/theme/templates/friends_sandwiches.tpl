 {assign var="random_number" value=100000|rand:999990}
 <input type="hidden" name="countMenuItems" value="{$numMenu}"/>
 <div id="frnDpopmenuz" class="sandwich-popup" style="display:none">
   <div class="summary-details-friends-menu">
     <a class="close-button" href="#"></a>
     <h2>Friends' Menus</h2>
     <section class="sandwich-menu" style="position:relative">

       <ul class="frndpopcont">
        <img src="{$SITE_URL}app/images/friends_menu.gif" style="margin: 0px auto; display:block; margin-top: 100px;" /> 
      </ul>
    </section>

  </div>  
</div>

<section class="sandwich new_sandwich_mobile">
  <div class="container">

    <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Friends’ Sandwiches<span> <a href="{$SITE_URL}sandwich/sandwichMenu/?pid={$random_number}" class="back_new"><img src="{$SITE_URL}app/images/back_new.png">
                                <b>Back</b></a></span><a href="{$SITE_URL}sandwich/sandwichMenu/?pid={$random_number}" class="back_new">
                        </a></h3>
                {if $fbCheck == 1}
                  {if !empty($data)}
                    {foreach key=key item=item from=$data}
                    <div class="friends-sandwich--block friends-sandwiches-all">
                        <div class="friends-sandwich ">
                            <div class="friends-dp"><img src="https://graph.facebook.com/{$item->friend_uid}/picture?width=107&height=107"></div>
                            <div class="friend-name friends-all">
                                <h2>{$item->first_name} {$item->last_name}</h2>
                                <p>{$item->sandwich_count} Sandwiches</p>
                            </div>
                            <div class="friends-view-wrap">
                                <div class="friends-view--btn"><a href="{$SITE_URL}menu/friendsMenu/{$item->uid}" class="freinds-view">view</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/foreach}
                  {else}
                    <div class="friends-share">
                      <p>LET YOUR FRIENDS KNOW ABOUT BARNEY BROWN!</p>
                      <a href="javascript:void(0);" id="Share-BarneyBrown" class="viewall-saved-sandwiches"><img src="{$SITE_URL}app/images/view-all--border.png"><span>SHARE</span></a>
                    </div>
                  {/if}
                {else}
                  <div class="friends-share">
                      <p>PLEASE CONNECT TO FACEBOOK TO SEE FRIENDS' SANDWICHES</p>
                      <a target="_blank" href="{php} echo _URL; {/php}" class="viewall-saved-sandwiches"><img src="{$SITE_URL}app/images/view-all--border.png"><span>CONNECT</span></a>
                    </div>
                {/if}
                </div>
</div>

</section>
