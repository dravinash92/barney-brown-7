{assign var="random_number" value=100000|rand:999990}
<div class="container">  
  <div id="content-container">	  	
		
		<div class="order-datails-wrap">
		<div class="confirm-top-section">
		    <img src="{$SITE_URL}app/images/loading_logo_2.png" />
		    <a href="{$SITE_URL}home/index/?pid={$random_number}" class="c-home">HOME</a>		    
		</div>
		<h2>Order Confirmation</h2>
		<p class="good-to-go">You’re good to go!</p>
		<div class="row">
		    <div class="left-section">
				<p class="order-number-label label">Order Number</p>
				<h4>{$orderid}</h4>
            </div>
            {if $check_now_specific eq 0}
            <div class="right-section">
                {if $delivery_type}
					<p class="time-number-label label">Est. Delivery Time</p>
					
					{$messege}
				{else}	
					<p class="time-number-label label">Est. PICKUP Time</p>
					<h4>5 - 10 MINUTES</h4>
				{/if}
            </div>
            {/if}
		</div>
        <div class="row">
			<div class="left-section">
				
				<p class="store-label label"> {if $delivery_type}ADDRESS{else}STORE LOCATION{/if}</p>
				<div class="address-details">
					<p>{if $delivery_type}{$address->name}{else}{$address->store_name}{/if}</p>
					<p> {if $address->address1 ne ''} {$address->address1} {/if} </p>
					<p>{if $address->street ne ''}({$address->street})<br>{/if}</p>
					<p> New York, NY {if $address->zip ne ''} {$address->zip} {/if}</p>
				</div>
			</div>
			<div class="right-section">
				<p class="date-time-label label">DATE/TIME</p>
				<p class="date-time-value">{$date} - {$time}</p>
				
				<p class="billing-info-label label">BILLING INFO</p>
				<p>{if $billing_type ne '' } {$billing_type} <br/> {/if} {if $billing_card_no ne ''} {$billing_card_no} {/if}</p>
					
			</div>
		  </div>	
		</div>
		<table class="table order-c table_checkout_confirm">
              <thead>
                <tr>
                  <th>Items</th>
                  <th class="qty">Qty</th>
                  <th class="text-right price ">Price</th>
                </tr>
              </thead>
              <tbody>
                {section name="orddet" loop=$order_item_detail}
                    <tr>
                        <td>		
                            <span>{$order_item_detail[orddet]->sandwich_name}{$order_item_detail[orddet]->product_name}</span><br>
                             {$order_item_detail[orddet]->product_description}{$order_item_detail[orddet]->sandwich_details|replace:', , , , ,':', '|replace:', , , ,':', '|replace:', , ,':', '|replace:', ,':', '}

                        </td>
                        <td class="padding-left qty"><span>{$order_item_detail[orddet]->qty}</span></td>
                       <!-- <td class="text-right price"><span>${$order_item_detail[orddet]->price|string_format:"%.2f"}</span></td> -->
                       <td class="text-right price"><span>${math equation="x * y" x=$order_item_detail[orddet]->price y=$order_item_detail[orddet]->qty format="%.2f"}</span></td>
                      
                    </tr>	
                {/section}		
                <tr class="subtotal bottom">
                  <td></td>
                  <td class="text-right"><span>Subtotal</span></td>
                  <td class="text-right"><span>${$sub_total}</span></td>
                </tr>
				
                <tr class="discount bottom">
                  <td></td>
                  <td class="text-right"><span>TAX</span></td>
                  <td class="text-right"><span>${$tax}</span></td>
                </tr>
                <tr class="cof_no_border">
                  <td></td>
                  <td class="text-right cof_delivery"><span>DELIVERY</span></td>
                  <td class="text-right cof_delivery"><span>${$delivery_fee}<span></td>
                </tr>
				{if $discount gt 0}
				<tr class="discount bottom">
                  <td></td>
                  <td class="text-right"><span>Discount</span></td>
                  <td class="text-right"><span>{if $discount gt 0} - {/if}${$discount}</span></td>
                </tr>
				{/if}
                <tr class="tip bottom">
                  <td></td>
                  <td class="text-right"><span>TIP</span></td>
                  <td class="text-right"><span>${$tip}</span></td>
                </tr>			
                <tr class="total bottom">
                  <td></td>
                    <td class="total text-right"><span class="total-text">Total</span></td>
                  <td class="text-right"><span>${$total}</span></td>
                </tr>
                </tbody>
            </table>  

  
  </div>
  
  </div>
