 {assign var="random_number" value=100000|rand:999990}
 <input type="hidden" name="countMenuItems" value="{$numMenu}"/>
 <div id="frnDpopmenuz" class="sandwich-popup" style="display:none">
   <div class="summary-details-friends-menu">
     <a class="close-button" href="#"></a>
     <h2>Friends' Menus</h2>
     <section class="sandwich-menu" style="position:relative">

       <ul class="frndpopcont">
        <img src="{$SITE_URL}app/images/friends_menu.gif" style="margin: 0px auto; display:block; margin-top: 100px;" /> 
      </ul>
    </section>

  </div>  
</div>

<section class="sandwich new_sandwich_mobile">
  <div class="container">

    <div class="sandwich-wrapper sandwich-new-wrapp menu-listing">
        <h3>My Saved Sandwiches<span><a href="{$SITE_URL}sandwich/sandwichMenu/?pid={$random_number}" class="back_new"><img src="{$SITE_URL}app/images/back_new.png"><b>Back</b></span></a></h3>
         {if !empty($featured_data)}
            {section name=sandwitch start=0 loop=$featured_data|@count step=1 }

            {assign var = 'bread_name' value=$featured_data[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0] }
            {assign var = 'prot_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
            {assign var = 'cheese_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
            {assign var = 'topping_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
            {assign var = 'cond_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }

            {assign var =  'bread_name' value = $bread_name }

            {php}
            $result = '';
            $d = $_smarty_tpl->get_template_vars('prot_data');
            if($d){
            foreach($d as $d){

            $d = trim($d);
            $d = str_replace(' ','##',$d);
            $result .= ' '.$d;
          }}
          {/php}


          {php}
          $result_1 = '';
          $c = $_smarty_tpl->get_template_vars('cheese_data');
          if($c){
          foreach($c as $c){

          $c = trim($c);
          $c = str_replace(' ','##',$c);

          $result_1 .= ' '.$c;
        }}
        {/php}


        {php}
        $result_2 = '';
        $t = $_smarty_tpl->get_template_vars('topping_data');
        if($t){
        foreach($t as $t){
        $t = trim($t);
        $t = str_replace(' ','##',$t);
        $result_2 .= ' '.$t;
      }}
      {/php}

      {php}
      if($o){       
      $result_3 = '';
      $o = trim($o);
      $o = str_replace(' ','##',$o);
      $o = $_smarty_tpl->get_template_vars('cond_data');
      foreach($o as $o){
      $result_3 .= ' '.$o;
      }}
      {/php}

      {if $order_data.user_sandwich.item_id|@is_array} 
      {assign var = 'items_id' value = $featured_data[$smarty.section.sandwitch.index].id|@in_array:$order_data.user_sandwich.item_id}
      {else}
      {assign var = 'items_id' value = '0' }
      {/if}

      {if $featured_data[$smarty.section.sandwitch.index].by_admin eq 1}
      {assign var="url" value=$ADMIN_URL}
      {else}
      {assign var="url" value=$SITE_URL}
      {/if}

      {assign var="sdesc" value=$featured_data[$smarty.section.sandwitch.index].sandwich_desc}
      {assign var="fname" value=$featured_data[$smarty.section.sandwitch.index].first_name}
      {assign var="user_name" value=$featured_data[$smarty.section.sandwitch.index].user_name}
      {assign var="lname" value=$featured_data[$smarty.section.sandwitch.index].last_name}
      {assign var="sprice" value=$featured_data[$smarty.section.sandwitch.index].current_price}
      {assign var="sname" value=$featured_data[$smarty.section.sandwitch.index].sandwich_name}
      {assign var="tname" value=$featured_data[$smarty.section.sandwitch.index].sandwich_name_trimmed}
    <div class="save-sandwich--wrap" data-id="{$featured_data[$smarty.section.sandwitch.index].id}" data-sandwich_desc="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc}" data-bread="{$bread_name}">
        <input type="hidden" id="featured_sandwich_view_popup" value="1">
        <div class="save-sand--img">
          <span><img class="view_sandwich" title="{$featured_data[$smarty.section.sandwitch.index].sandwich_name}" data-href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png"><img src="{$SITE_URL}app/images/toasted.png" class="toasted" style="{if $featured_data[$smarty.section.sandwitch.index].menu_toast eq 1 } display:block; {/if}"></span>
        </div>

        <div class="save-sand--content save-sand--content-new" id="toastID_{$featured_data[$smarty.section.sandwitch.index].id}" data-sandwich_desc="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc_id}" data-flag="{$featured_data[$smarty.section.sandwitch.index].flag}" data-menuadds="{$featured_data[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$featured_data[$smarty.section.sandwitch.index].uid}" {if $items_id  eq 1} rel="ADD TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-toast="{$featured_data[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$featured_data[$smarty.section.sandwitch.index].formated_date}" data-username="{$featured_data[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$bread_name}"  data-date="{$featured_data[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$featured_data[$smarty.section.sandwitch.index].current_price}" data-id="{$featured_data[$smarty.section.sandwitch.index].id}" data-name="{$featured_data[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$featured_data[$smarty.section.sandwitch.index].like_id}" data-likecount="{$featured_data[$smarty.section.sandwitch.index].like_count}" data-typeSandwich="SS" data-page="SSP">
            <input type="hidden" name="chkmenuactice{$featured_data[$smarty.section.sandwitch.index].id}" value="{$featured_data[$smarty.section.sandwitch.index].menu_is_active}" />
            
            {assign var=jsonData value=$sandwich->sandwich_data|json_decode:1}
            <span class="saved-sand--price">{$sname}</span>
            <p>{$sdesc}</p>
            <input type="hidden" value="{$featured_data[$smarty.section.sandwitch.index].is_public}" id="isPublic{$featured_data[$smarty.section.sandwitch.index].id}">
            <img data-href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}" id="sandwichimg_{$featured_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png" hidden>
            <p>Created by: {$user_name}.</p>
            <div id="featured_new_mobile">
              <h2>${$sprice}</h2>
              <div>
                  <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup leftSpinner"></a>
                    <input id="sandwichQty{$featured_data[$smarty.section.sandwitch.index].id}" name="itemQty" type="text" class="text-box" value="01" readonly="">
                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup rightSpinner"></a>
                  </h3>
                </div>
                <div class="saved-add-cartwrap">
                  <a class="saved-add-cart quickAddToCart" href="#"><img src="{$SITE_URL}app/images/cart.png"><span>Add</span></a>
                </div>
            </div>
            
            <div class="saved-item-btns saved-item-btns-new">
              <a href="#" class="view_sandwich" id="view-sandwich-popup">View</a>
              <a href="javascript:void(0);" class="editSandwich" id="{$featured_data[$smarty.section.sandwitch.index].id}">Edit</a>
              {if $featured_data[$smarty.section.sandwitch.index].menu_is_active eq 1}
              <a href="#" class="saved featured_item_btn saved_btn">SAVED</a>
              {else}
              <a class="featured_item_btn" id="save_toggle{$featured_data[$smarty.section.sandwitch.index].id}" href="#">SAVE</a>
              {/if}
              {if $featured_data[$smarty.section.sandwitch.index].is_public eq 0}
              <span class="locked-item"><img src="{$SITE_URL}app/images/locked-itm.png"></span>
              {/if}
          </div>
        </div>
  <input class="typeSandwich" type="hidden" value="SS">
  <input type="hidden" name="saved_tgl{$featured_data[$smarty.section.sandwitch.index].id}" value="1">

  <input type="hidden" value="product" name="data_type">
 <!--  <div class="add-new no-items">
    
  </div> -->
</div>
{/section}
{else}
<div class="no-saved-sandwiches">
     <h3>
     you currently have no saved sandwiches
     </h3>
     <p>EITHER CREATE YOUR OWN CUSTOM SANDWICH, CHOOSE FROM OUR USER-
     SUBMITTED GALLERY, OR GRAB ONE OF YOUR FRIEND'S SANDWICHES.
     </p>
  </div>
{/if}
</div>

</section>
