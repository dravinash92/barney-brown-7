<section class="sandwich create-sandwhich-menu-wrapper">
<div class="create-sandwhich-menu">
    <div class="finalize-wrapper">
        <div class="counts-sandwich"><p class="sandwich_count" data-count="{$totalCount}">{$totalCount|number_format}</p><span>CREATIONS</span></div>   
      <a href="#" class="filter sandwich_filter_view sandwich_gallery_filter">filters</a>

      <div class="clearfix"></div>
   
    </div>
  </div>
  <div class="container sandwich-all-gallery">
    <div class="sandwich-wrapper">
      <form action="{$SITE_URL}sandwich/gallery" id="sandwich-gallery-search" method="post">
        <input class="sandwiches_search" type="text" name="search"  value="{$serch_term}">
        <button type="submit" class="sandwiches_search_btn">search</button>
      </form>
      <input type="hidden" id="searchTerm" value="{$serch_term}">
  <div class="clearfix"></div>
    <div class="clearfix"></div>
       <ul class="menu-listing">
       {section name=sandwitch start=0 loop=$GALLARY_DATA|@count step=1 }
             
              {assign var = 'bread_name' value=$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0] }
              {assign var = 'prot_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {assign var =  'bread_name' value = $bread_name|replace:' ':'##' }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
              
                   
                   
                   {if $order_data.user_sandwich.item_id|@is_array} 
                   {assign var = 'items_id' value = $GALLARY_DATA[$smarty.section.sandwitch.index].id|@in_array:$order_data.user_sandwich.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
              
              {if $GALLARY_DATA[$smarty.section.sandwitch.index].by_admin eq 1}
              {assign var="url" value=$ADMIN_URL}
              {else}
              {assign var="url" value=$SITE_URL}
              {/if}
              
              {assign var="sname" value=$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}
              <div class="p" class="right-rgt" data-sandwich_desc="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_desc_id}" data-flag="{$GALLARY_DATA[$smarty.section.sandwitch.index].flag}" data-menuadds="{$GALLARY_DATA[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}" {if $items_id  eq 1} rel="ADD TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-toast="{$GALLARY_DATA[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$GALLARY_DATA[$smarty.section.sandwitch.index].formated_date}" data-username="{$GALLARY_DATA[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$bread_name}"  data-date="{$GALLARY_DATA[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_price}" data-id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" data-name="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$GALLARY_DATA[$smarty.section.sandwitch.index].like_id}" data-likecount="{$GALLARY_DATA[$smarty.section.sandwitch.index].like_count}" data-typeSandwich="FS">
             <li>
                           <img data-href="{$SITE_URL}createsandwich/index/{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" data-src="{$image_path}{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}/thumbnails/sandwich_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}_{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}.png" alt="sandwitchimageview" class="sandwich_gallery_view">
<input type="hidden" id="sandwichGalleryImg_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="{$image_path}{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}/sandwich_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}_{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}.png">

                           <input type="hidden" name="chkmenuactice{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="{$GALLARY_DATA[$smarty.section.sandwitch.index].menu_is_active}" />
           
           <h4 class="sandwich_gallery_view">{$sname|upper|truncate:20:"..":true}</h4>
           <a href="#" class="add-cart" id="view-sandwich-popup">VIEW</a>
           {foreach from=$saved_data key=k item=v}
              {if $GALLARY_DATA[$smarty.section.sandwitch.index].id eq $v}
                  {if $is_public_array.$v eq 0}
                  <input type="hidden" value="{$is_public_array.$v}" id="isPublic{$GALLARY_DATA[$smarty.section.sandwitch.index].id}">
                  {/if} 
              {/if}
          {/foreach}
          {if $GALLARY_DATA[$smarty.section.sandwitch.index].id|in_array:$saved_data}
                                <input type="hidden" name="saved_tgl{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="1">
                              {/if}
         </li>
           <input type="hidden" name="hidden_uid" id="hidden_uid" value="{$smarty.session.uid}" ></div>   
          {/section}  
       </ul>
    
    
     <div class="loadMoreGallery" style="text-align:center"><img src="{$SITE_URL}app/images/star.png" alt="Loading" /> </div>
    
    
    
    
    
    </div>
  </div>
</section>
<div class="sandwich-popup" id="sandwich-popup-gallery" style="display:none;">
<div class="summary-details-add-menu">
<div class="banner"> <a href="#" class="close-button">&nbsp;</a> <img src="images/added-menu-sandwiches.png" alt="{$title_text}">
</div>
    <div class="final-list-wrapper">
  <h2 class="sandwich_name">JON'S MOZZARELLA MASTERPIECE</h2>
  <p class="view-popup-scroll">Corned Beef (1.0), Lettuce (N) Red Onion (L), Pickles (L), Tomato (N), Lettuce (L) Red Onion (L), Pickles (L), Lettuce (L) Red Onion (L), Pickles (L)  </p>
  <ul>
  <li>Created by:  <span class="user_name">Jon C</span>.</li>
  <li>Date Created:  <span class="created_date">10/14/14</span></li>

  </ul>
  
  <div class="toastit-wrapper">
      <div class="checkbox-holder-final">
        <input type="checkbox" class="savetousertoast toast" value="check1" name="check" id="check1">
        <label for="check1"> <span>Toast it!</span> <!-- <span>Yum!</span> --></label>
      </div>
    </div>
  
  <div class="add-menu">

   <a class="share share-sandwich" href="#"><img src="{$SITE_URL}app/images/share-img.png" alt=""></img><span class="Share_sandwich">Share</span></a>  
  <a class="add-to-menu save-menu" href="#">Save to my menu</a>
        <input type="hidden" name="hidden_sandwich_data" id="hidden_sandwich_data" value="user_sandwich">
        <input type="hidden" name="hidden_sandwich_id" id="hidden_sandwich_id" value="">
  </div>
   
  <div class="add-cart">
  <h2 class="amount">$10.00</h2>
  <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
      <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
    <a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
           <input id="resetQty" name="itemQuty" type="text" class="text-box"  value="01" readonly>
    <a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
  </h3>
  <a class="add-to-cart" href="#">Add to cart</a>
  </div>
  
    </div>  
  </div>
</div>

<!-- Late loading data -->
<input name="total_item_count" type="hidden" value="{$total_item_count}" />
<input name="sort_type" type="hidden" value="{$sort_id}" />

<div class="sandwich-popup" id="sandwich-popup-filter" style="display:none;">
  <div class="summary-details-login summary-details-discount"> <a href="#" class="close-button-new"></a>
    <div class="saved-creditcards-wrapper">
      <h2 class="filter-option">filter Options <a href="#" class="clear-all-filter">Clear Filters</a></h2>
 
      <ul class="from-holder">
    {foreach from=$categories key=i item=category}

     {foreach from=$category key=k item=data}
       {if $data->category_identifier }

     
        <li> <span class="text-box-holder">
      <div class="mobile-popup-filter">
        <p class="items">{$data->category_name|capitalize}</p>
             <div class="list-options scroll-bar filterToggle" id="fit-{$data->category_name}" style="display:none;"> 
              <ul>
              {if $data->category_name neq "bread" }
                <li>
                  <input id="No {$data->category_name}" type="checkbox" class="{$data->category_name} isFiltered" name="{$data->category_name}" value="No {$data->category_name}">
                  <label for="No {$data->category_name}">No {$data->category_name}</label>
                </li>
            
                {/if}
                {foreach from=$categoryItems key=j item=categoryItem}
                
                  {foreach from=$categoryItem key=j item=dataitems}
                     {if $data->id eq $dataitems->category_id}
            <li>
                  <input id="{$dataitems->item_name}" type="checkbox" class="{$data->category_name} isFiltered" name="{$data->category_name}" value="{$dataitems->item_name}">
                  <label for="{$dataitems->item_name}">{$dataitems->item_name}</label>
                </li>
                
             {/if}
              
                  {/foreach}
                 
            {/foreach} 
       
              </ul>
           </div>
               <span class="filter">
                 <div class="FilterLable" id="{$data->category_name}">Select</div>  </span>
           </div>
              </span> </li>
          
              {/if} 
              
             {/foreach}
                   
            {/foreach}

          <span class="border"></span>
          <a href="#" class="apply-filter">APPLY FILTERS</a>
          <div class="cancel-wrapper"><a href="#" class="cancel-button">CANCEL</a></div>
      </ul>
    </div>
  </div>
</div>
