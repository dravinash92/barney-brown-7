<?php /* Smarty version 2.6.25, created on 2020-10-15 10:42:43
         compiled from myaccount-orders.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'myaccount-orders.tpl', 5, false),array('modifier', 'date_format', 'myaccount-orders.tpl', 18, false),array('modifier', 'upper', 'myaccount-orders.tpl', 18, false),array('modifier', 'replace', 'myaccount-orders.tpl', 24, false),)), $this); ?>

<section class="sandwich create-sandwhich-menu-wrapper">
<div class="container">
  <!-- ORDER 1 -->
  <?php if (count($this->_tpl_vars['orderhistory']) > 0): ?> 
   <?php $_from = $this->_tpl_vars['orderhistory']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['history']):
?>
   
  <div class="order-history">
   
  <div class="container">
  <div class="row">
  <div class="right-float">
    <?php if ($this->_tpl_vars['history']->itemCheck > 0): ?>
  <a href="#" data-order= "<?php echo $this->_tpl_vars['history']->order_id; ?>
" data-from="order_history" class="recorder reorder-button">REORDER</a>
   <?php endif; ?>
  </div>
  <div class="left-float">
  <h2><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['history']->timestampDate)) ? $this->_run_mod_handler('date_format', true, $_tmp, "%a, %b %e, %Y") : smarty_modifier_date_format($_tmp, "%a, %b %e, %Y")))) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</h2>

  <?php $_from = $this->_tpl_vars['history']->items; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['item']):
?>
  		<h3 class="order-product-name">(<?php echo $this->_tpl_vars['item']->qty; ?>
) <?php if ($this->_tpl_vars['item']->type == 'user_sandwich'): ?><span><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->sandwich_name)) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</span><?php endif; ?>
			<?php if ($this->_tpl_vars['item']->type == 'product'): ?><span><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->product_name)) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</span><?php endif; ?>
  		</h3>  		
  		<p><?php echo $this->_tpl_vars['item']->desc; ?>
<!-- <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']->sandwich_details)) ? $this->_run_mod_handler('replace', true, $_tmp, ', , , , ,', ', ') : smarty_modifier_replace($_tmp, ', , , , ,', ', ')))) ? $this->_run_mod_handler('replace', true, $_tmp, ', , , ,', ', ') : smarty_modifier_replace($_tmp, ', , , ,', ', ')))) ? $this->_run_mod_handler('replace', true, $_tmp, ', , ,', ', ') : smarty_modifier_replace($_tmp, ', , ,', ', ')))) ? $this->_run_mod_handler('replace', true, $_tmp, ', ,', ', ') : smarty_modifier_replace($_tmp, ', ,', ', ')); ?>
 --></p> 

      <!-- <?php echo $this->_tpl_vars['item']->desc; ?>
 -->
        <span class="item_details" style="display: none;" data-bread="<?php echo $this->_tpl_vars['item']->bread; ?>
" data-sandwich_desc="<?php echo $this->_tpl_vars['item']->desc; ?>
" data-sandwich_desc_id="<?php echo $this->_tpl_vars['item']->desc_id; ?>
" data-id="<?php echo $this->_tpl_vars['item']->id; ?>
"></span>
  <?php endforeach; endif; unset($_from); ?>
  
  
  <div class="deliver">
  <p><span><?php echo ((is_array($_tmp=$this->_tpl_vars['history']->delivery_type)) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
 : </span>&nbsp;<?php echo $this->_tpl_vars['history']->address->address1; ?>
</p>
  <p><span>PAYMENT : </span>&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['history']->billing_type)) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
 <?php echo $this->_tpl_vars['history']->billing_card_no; ?>
</p>
  </div>
  </div>
  </div>
  </div>
  </div>
 
  <?php endforeach; endif; unset($_from); ?>
  <?php else: ?>
  <span><h3 class="no_order_history">No Previous Orders.</h3></span>
  <?php endif; ?>

</div>
</section>
