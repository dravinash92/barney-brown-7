<?php /* Smarty version 2.6.25, created on 2020-03-18 08:50:39
         compiled from check-out-confirm.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'rand', 'check-out-confirm.tpl', 1, false),array('modifier', 'replace', 'check-out-confirm.tpl', 64, false),array('modifier', 'string_format', 'check-out-confirm.tpl', 68, false),array('function', 'math', 'check-out-confirm.tpl', 69, false),)), $this); ?>
<?php $this->assign('random_number', ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990))); ?>
<div class="container">  
  <div id="content-container">	  	
		
		<div class="order-datails-wrap">
		<div class="confirm-top-section">
		    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/loading_logo_2.png" />
		    <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
home/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="c-home">HOME</a>		    
		</div>
		<h2>Order Confirmation</h2>
		<p class="good-to-go">You’re good to go!</p>
		<div class="row">
		    <div class="left-section">
				<p class="order-number-label label">Order Number</p>
				<h4><?php echo $this->_tpl_vars['orderid']; ?>
</h4>
            </div>
            <?php if ($this->_tpl_vars['check_now_specific'] == 0): ?>
            <div class="right-section">
                <?php if ($this->_tpl_vars['delivery_type']): ?>
					<p class="time-number-label label">Est. Delivery Time</p>
					<!--<h4>30 - 60 MINUTES</h4>-->
					<?php echo $this->_tpl_vars['messege']; ?>

				<?php else: ?>	
					<p class="time-number-label label">Est. PICKUP Time</p>
					<h4>5 - 10 MINUTES</h4>
				<?php endif; ?>
            </div>
            <?php endif; ?>
		</div>
        <div class="row">
			<div class="left-section">
				
				<p class="store-label label"> <?php if ($this->_tpl_vars['delivery_type']): ?>ADDRESS<?php else: ?>STORE LOCATION<?php endif; ?></p>
				<div class="address-details">
					<p><?php if ($this->_tpl_vars['delivery_type']): ?><?php echo $this->_tpl_vars['address']->name; ?>
<?php else: ?><?php echo $this->_tpl_vars['address']->store_name; ?>
<?php endif; ?></p>
					<p> <?php if ($this->_tpl_vars['address']->address1 != ''): ?> <?php echo $this->_tpl_vars['address']->address1; ?>
 <?php endif; ?> </p>
					<p><?php if ($this->_tpl_vars['address']->street != ''): ?>(<?php echo $this->_tpl_vars['address']->street; ?>
)<br><?php endif; ?></p>
					<p> New York, NY <?php if ($this->_tpl_vars['address']->zip != ''): ?> <?php echo $this->_tpl_vars['address']->zip; ?>
 <?php endif; ?></p>
				</div>
			</div>
			<div class="right-section">
				<p class="date-time-label label">DATE/TIME</p>
				<p class="date-time-value"><?php echo $this->_tpl_vars['date']; ?>
 - <?php echo $this->_tpl_vars['time']; ?>
</p>
				
				<p class="billing-info-label label">BILLING INFO</p>
				<p><?php if ($this->_tpl_vars['billing_type'] != ''): ?> <?php echo $this->_tpl_vars['billing_type']; ?>
 <br/> <?php endif; ?> <?php if ($this->_tpl_vars['billing_card_no'] != ''): ?> <?php echo $this->_tpl_vars['billing_card_no']; ?>
 <?php endif; ?></p>
					
			</div>
		  </div>	
		</div>
		<table class="table order-c table_checkout_confirm">
              <thead>
                <tr>
                  <th>Items</th>
                  <th class="qty">Qty</th>
                  <th class="text-right price ">Price</th>
                </tr>
              </thead>
              <tbody>
                <?php unset($this->_sections['orddet']);
$this->_sections['orddet']['name'] = 'orddet';
$this->_sections['orddet']['loop'] = is_array($_loop=$this->_tpl_vars['order_item_detail']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['orddet']['show'] = true;
$this->_sections['orddet']['max'] = $this->_sections['orddet']['loop'];
$this->_sections['orddet']['step'] = 1;
$this->_sections['orddet']['start'] = $this->_sections['orddet']['step'] > 0 ? 0 : $this->_sections['orddet']['loop']-1;
if ($this->_sections['orddet']['show']) {
    $this->_sections['orddet']['total'] = $this->_sections['orddet']['loop'];
    if ($this->_sections['orddet']['total'] == 0)
        $this->_sections['orddet']['show'] = false;
} else
    $this->_sections['orddet']['total'] = 0;
if ($this->_sections['orddet']['show']):

            for ($this->_sections['orddet']['index'] = $this->_sections['orddet']['start'], $this->_sections['orddet']['iteration'] = 1;
                 $this->_sections['orddet']['iteration'] <= $this->_sections['orddet']['total'];
                 $this->_sections['orddet']['index'] += $this->_sections['orddet']['step'], $this->_sections['orddet']['iteration']++):
$this->_sections['orddet']['rownum'] = $this->_sections['orddet']['iteration'];
$this->_sections['orddet']['index_prev'] = $this->_sections['orddet']['index'] - $this->_sections['orddet']['step'];
$this->_sections['orddet']['index_next'] = $this->_sections['orddet']['index'] + $this->_sections['orddet']['step'];
$this->_sections['orddet']['first']      = ($this->_sections['orddet']['iteration'] == 1);
$this->_sections['orddet']['last']       = ($this->_sections['orddet']['iteration'] == $this->_sections['orddet']['total']);
?>
                    <tr>
                        <td>		
                            <span><?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orddet']['index']]->sandwich_name; ?>
<?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orddet']['index']]->product_name; ?>
</span><br>
                             <?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orddet']['index']]->product_description; ?>
<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['order_item_detail'][$this->_sections['orddet']['index']]->sandwich_details)) ? $this->_run_mod_handler('replace', true, $_tmp, ', , , , ,', ', ') : smarty_modifier_replace($_tmp, ', , , , ,', ', ')))) ? $this->_run_mod_handler('replace', true, $_tmp, ', , , ,', ', ') : smarty_modifier_replace($_tmp, ', , , ,', ', ')))) ? $this->_run_mod_handler('replace', true, $_tmp, ', , ,', ', ') : smarty_modifier_replace($_tmp, ', , ,', ', ')))) ? $this->_run_mod_handler('replace', true, $_tmp, ', ,', ', ') : smarty_modifier_replace($_tmp, ', ,', ', ')); ?>


                        </td>
                        <td class="padding-left qty"><span><?php echo $this->_tpl_vars['order_item_detail'][$this->_sections['orddet']['index']]->qty; ?>
</span></td>
                       <!-- <td class="text-right price"><span>$<?php echo ((is_array($_tmp=$this->_tpl_vars['order_item_detail'][$this->_sections['orddet']['index']]->price)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</span></td> -->
                       <td class="text-right price"><span>$<?php echo smarty_function_math(array('equation' => "x * y",'x' => $this->_tpl_vars['order_item_detail'][$this->_sections['orddet']['index']]->price,'y' => $this->_tpl_vars['order_item_detail'][$this->_sections['orddet']['index']]->qty,'format' => "%.2f"), $this);?>
</span></td>
                      
                    </tr>	
                <?php endfor; endif; ?>		
                <tr class="subtotal bottom">
                  <td></td>
                  <td class="text-right"><span>Subtotal</span></td>
                  <td class="text-right"><span>$<?php echo $this->_tpl_vars['sub_total']; ?>
</span></td>
                </tr>
				
                <tr class="discount bottom">
                  <td></td>
                  <td class="text-right"><span>TAX</span></td>
                  <td class="text-right"><span>$<?php echo $this->_tpl_vars['tax']; ?>
</span></td>
                </tr>
                <tr class="cof_no_border">
                  <td></td>
                  <td class="text-right cof_delivery"><span>DELIVERY</span></td>
                  <td class="text-right cof_delivery"><span>$<?php echo $this->_tpl_vars['delivery_fee']; ?>
<span></td>
                </tr>
				<?php if ($this->_tpl_vars['discount'] > 0): ?>
				<tr class="discount bottom">
                  <td></td>
                  <td class="text-right"><span>Discount</span></td>
                  <td class="text-right"><span><?php if ($this->_tpl_vars['discount'] > 0): ?> - <?php endif; ?>$<?php echo $this->_tpl_vars['discount']; ?>
</span></td>
                </tr>
				<?php endif; ?>
                <tr class="tip bottom">
                  <td></td>
                  <td class="text-right"><span>TIP</span></td>
                  <td class="text-right"><span>$<?php echo $this->_tpl_vars['tip']; ?>
</span></td>
                </tr>			
                <tr class="total bottom">
                  <td></td>
                    <td class="total text-right"><span class="total-text">Total</span></td>
                  <td class="text-right"><span>$<?php echo $this->_tpl_vars['total']; ?>
</span></td>
                </tr>
                </tbody>
            </table>  

  
  </div>
  
  </div>