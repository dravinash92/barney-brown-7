<?php /* Smarty version 2.6.25, created on 2020-10-06 03:19:49
         compiled from header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'rand', 'header.tpl', 24, false),)), $this); ?>
<?php  

$FB = new FB_API();
$access_token  = @$_SESSION['access_token'];

if(!isset($_SESSION['FBRLH_state']) || !$_SESSION['FBRLH_state']  ){

$FB->FB_check_login();
$url =  $FB->FB_get_url();
$_SESSION['tmpUrl'] = $url;
$cookietime        = time() + 60 * 60 * 24 * 60;
setcookie("fbloginurl",$url,$cookietime, '/', NULL);
define('_URL',$url);

} else {

define('_URL',$_SESSION['tmpUrl']);

}


  
 ?>
 <?php $this->assign('random_number', ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990))); ?>
<!DOCTYPE HTML>
<html lang="en">
<!-- <html lang="en" manifest="<?php echo $this->_tpl_vars['SITE_URL']; ?>
data.appcache"> -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo $this->_tpl_vars['title_text']; ?>
</title>
<link href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/stylesheets/<?php echo $this->_tpl_vars['deviceSpecificCss']; ?>
" rel="stylesheet">
<link href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/stylesheets/jquery-ui.css" rel="stylesheet">
<link href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/stylesheets/backgrounds.css" rel="stylesheet"> 
<link href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/stylesheets/final.css?v=2.2" rel="stylesheet"> 

<?php echo '
<style type="text/css">
  .slicknav_menu .slicknav_menutxt { display: none;}
</style>
'; ?>

 <script type="text/javascript">
var SITE_URL   = "<?php echo $this->_tpl_vars['SITE_URL']; ?>
";
var siteurl    = "<?php echo $this->_tpl_vars['SITE_URL']; ?>
";
var CMS_URL    = "<?php echo $this->_tpl_vars['CMS_URL']; ?>
";
var API_URL    = "<?php echo $this->_tpl_vars['API_URL']; ?>
";
var ADMIN_URL  = "<?php echo $this->_tpl_vars['ADMIN_URL']; ?>
";
var FB_APP_ID  = "<?php echo $this->_tpl_vars['FB_APP_ID']; ?>
";
var SESSION_ID = "<?php echo $this->_supers['session']['uid']; ?>
";
var $sandwich_img_live_uri = "<?php echo $this->_tpl_vars['sandwich_img_live_uri']; ?>
";
var DATA_ID = "<?php echo $this->_tpl_vars['DATA_ID']; ?>
";
var BASE_FARE = "<?php echo $this->_tpl_vars['BASE_FARE']; ?>
";
 </script>
</head>
<body <?php if ($this->_tpl_vars['cart']): ?><?php echo $this->_tpl_vars['CLASS_NAME']; ?>
<?php endif; ?>> 


<div class="pagelogo">
	<img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/loading_logo.png" width="196" height="88"  /> 
	<div style="text-align: center; display:block;" class="loadMoreGallery spin-star"><img alt="Loading" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/star.png"> </div>
</div>		
<div class="page-contents" style="display:none" > 		
<div id="fb-root"></div> 
<?php echo '
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script src="https://connect.facebook.net/en_US/sdk.js"></script>
<script type="text/javascript">(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0"; fjs.parentNode.insertBefore(js, fjs); }(document, \'script\', \'facebook-jssdk\'));
try{
	FB.init({ 
	    //appId: \'425309574521069\',
	    appId: \'557060384864429\',
	    status: true, 
	    cookie: true, 
	    xfbml: false
	});    
}
catch( e )
{}
</script>
'; ?>

<div class="header_wrapper">
	<header>
	
  		 <nav>
		  
      	<ul id="menu">
      	<!-- 
        	<li>
          	<span class="menu-logo">
            	<a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></a>
            </span>
          </li>  -->
          <?php   if(isset($_SESSION['uid'])) {  ?>
	          <li>
	          	<span class="button-wrapper">
	            	Hi, <?php echo $this->_supers['session']['uname']; ?>

              <a href="<?php echo $this->_tpl_vars['signOutURL']; ?>
" class="signout-account">Sign Out</a> 
			  
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
myaccount/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
"  class="create-account">ORDERS</a>
					<!-- <div class="facebook-likes">
					 <div class="fb-like" data-href="http://cms.ha.allthingsmedia.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					  </div> --> 
	            </span>
	          </li>
	          <?php  } else if($_COOKIE['user_mail'] && $_COOKIE['user_fname'] && $_COOKIE['user_lname'] )  {  ?>
	        <li>
	          	<span class="button-wrapper">
	            	Hi, <?php  echo $_COOKIE['user_fname'];  ?>
              <a href="<?php echo $this->_tpl_vars['signOutURL']; ?>
" class="signout-account">Sign Out</a> 
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
myaccount"  class="create-account">ORDERS</a>
	              <!-- <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/facebook-like.png" alt=""> -->
	            </span>
	          </li>
         <?php  } else {  ?>
	          <li>
	          	<span class="button-wrapper">
	            	<a target="_blank" href="<?php  echo _URL;  ?>" class="login-facebook">LOGIN WITH FACEBOOK</a>
	              <a href="javascript:void(0);" class="login-button" id="logintowebcms">LOGIN</a>
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="javascript:void(0);"  class="create-account connect-register">Create account</a>
					<!-- <div class="facebook-likes">
					  <div class="fb-like" data-href="http://cms.ha.allthingsmedia.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					  </div>-->
	            </span>
	          </li>
         <?php   }  ?>
         
          
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
home/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">Home</a></li>
          <li class="sandwich_sub__menus"><a href="#">SANDWICHES</a>
            <ul>
                <li>
                    <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/sandwichMenu/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">MENU</a>
                    <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">CUSTOMIZE</a>
                    <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/savedSandwiches/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">SAVED</a>
                    <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/featuredSandwiches/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">FEATURED</a>
                    <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/gallery/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">ALL</a>
                    
                    <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/friendSandwiches/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">FRIENDS</a>

                </li>
            </ul>
          </li>
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/salads/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">SALADS</a></li>
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/snacks/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">SNACKS</a></li>
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/drinks/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">DRINKS</a></li>
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
catering/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">CATERING</a></li>
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
site/aboutus/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="nav-hide-img">About us</a></li>
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
site/covid19/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="nav-hide-img">Food Safety</a></li>
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
site/deliverymap/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="nav-hide-img">Delivery Map</a></li>
          <!-- <li><a href="http://mobile.ha.allthingsmedia.com/site/location/index/?pid=863897" class="nav-hide-img">Location</a></li> -->
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
site/covid19/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="nav-hide-img">Food Safety</a></li>
          <li><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
site/contactus/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="nav-hide-img">Contact</a></li>
		 <!--  <li><a href="http://mobile.ha.allthingsmedia.com/site/testmode" class="nav-hide-img">Testmode</a></li> -->
      </ul>
      </nav>
			
      <figure class="logo"><a href="#" class="menu"><?php if ($this->_tpl_vars['heading']): ?> <?php echo $this->_tpl_vars['heading']; ?>
 <?php else: ?><img alt="logo" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/logo.png"><?php endif; ?><!--<?php echo $this->_tpl_vars['heading']; ?>
--></a></figure>
      <div class="cart-top">
		<?php   if(isset($_SESSION['uid'])) {  ?>  
			<a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
checkout/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">
		<?php  } else {  ?>	
			<a class="login-button-cart" href="#">
		<?php  }  ?> 	
        	<img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/shopping-cart.png" alt="shopping-cart">
       	<h1><?php echo $this->_tpl_vars['cart']; ?>
</h1>
        </a>
      </div>
      
		
  </header>
  
  		<div class="dynamic_updates" style="display: none;">
			<div class="left-block"></div>
			<div class="right-block"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
checkout/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="">CHECKOUT</a></div>
		</div>

	<!-- <div class="dynamic_updates_fixed" style="display: none;">
			<div class="left-block"></div>
			<div class="right-block"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
checkout/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="">CHECKOUT</a></div>
		</div> -->
  
</div>
<div class="menu-block-replace"></div>
		