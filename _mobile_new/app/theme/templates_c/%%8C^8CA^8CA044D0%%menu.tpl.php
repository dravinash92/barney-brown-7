<?php /* Smarty version 2.6.25, created on 2018-10-24 04:03:06
         compiled from menu.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'rand', 'menu.tpl', 1, false),array('modifier', 'count', 'menu.tpl', 24, false),array('modifier', 'is_array', 'menu.tpl', 93, false),array('modifier', 'in_array', 'menu.tpl', 94, false),array('modifier', 'date_format', 'menu.tpl', 107, false),array('modifier', 'upper', 'menu.tpl', 116, false),array('modifier', 'truncate', 'menu.tpl', 116, false),)), $this); ?>
 <?php $this->assign('random_number', ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990))); ?>
<input type="hidden" name="countMenuItems" value="<?php echo $this->_tpl_vars['numMenu']; ?>
"/>
<div id="frnDpopmenuz" class="sandwich-popup" style="display:none">
	<div class="summary-details-friends-menu">
  	<a class="close-button" href="#"></a>
  	<h2>Friends' Menus</h2>
	 <section class="sandwich-menu" style="position:relative">
	 
         <ul class="frndpopcont">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/friends_menu.gif" style="margin: 0px auto; display:block; margin-top: 100px;" /> 
	    </ul>
</section>
	
</div>	
</div>


<section class="sandwich">
  <div class="container">
    <div class="sandwich-wrapper"> 
      <h3>Sandwiches</h3>
       <ul class="user-menu-sandwiches">         
          <li class="right-rgt">
           <span class="add-new02-address-main <?php if (count($this->_tpl_vars['usersandwich']) > 0): ?>sandwiches-more<?php endif; ?>">
              <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" >
                  <div class="link-wrap"><span class="btn-01">+</span>CREATE A<br />SANDWICH</div>
              </a>
              <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/gallery/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" >
                  <div class="link-wrap"><span class="btn-01">+</span>SHARED <br />CREATIONS</div>
              </a>
               
              <a id="show_friends_sandwich" data-fbrel="<?php if ($this->_supers['session']['access_token']): ?>true<?php else: ?>false<?php endif; ?>" rel="<?php if ($this->_supers['session']['uid']): ?>true<?php else: ?>false<?php endif; ?>" class="clr-05" href="<?php  echo _URL;  ?>" >
                  <div class="link-wrap"><span class="btn-01">+</span>FRIENDS'<br />SANDWICHES</div>
              </a>
              
           </span>
         </li>
          
         
         
         
        <?php if (count($this->_tpl_vars['usersandwich']) > 0): ?>
        
        
			 <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['usersandwich'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
            
            
             <?php $this->assign('prot_data', $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
              if($c){
              foreach($c as $c){
               $result_1 .= ' '.$c;
              } 
              }
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
            
                  <?php if (is_array($this->_tpl_vars['order_data']['user_sandwich']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id'], $this->_tpl_vars['order_data']['user_sandwich']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
                   
                   
                <?php if ($this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['by_admin'] == 0): ?>
                   <?php $this->assign('img_path', $this->_tpl_vars['CMS_URL']); ?>
                <?php else: ?>
                   <?php $this->assign('img_path', $this->_tpl_vars['ADMIN_URL']); ?>
                <?php endif; ?>   
  
  <?php if ($this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid'] > 0): ?>          
   <li id="item-<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
" class="right-rgt" rel="ADD TO CART" data-sandwich-name="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-sandwich_desc="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
"  data-userid ="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
"  data-toast="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['menu_toast']; ?>
" data-menuadds="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['menu_add_count']; ?>
" data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-formatdate="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]; ?>
"  data-date="<?php echo ((is_array($_tmp=$this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['date_of_creation'])) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp)); ?>
" data-price="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
" data-id="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-likeid="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['like_id']; ?>
" data-likecount="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['like_count']; ?>
" data-flag="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['flag']; ?>
" data-image="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png"> 
		 
				
				        <img width="53" height="24"   class="retina toasted"  style=" <?php if ($this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['menu_toast'] == 1): ?> display:block; <?php else: ?> display:none; <?php endif; ?>" alt="toasted" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/toasted.png">
				        
           <img data-sand-pop="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
" class="sand-pop" title="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png">
       
   
           
           <h4><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_name'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "..", true) : smarty_modifier_truncate($_tmp, 20, "..", true)); ?>
</h4>
           <!--<p><?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['like_count']; ?>
 likes </p> --> <!-- Like btn  -->
          
          
           <a class="common_add_item_cart add-cart" data-uid="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
" data-sandwich_id="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-sandwich="user_sandwich" href="#">Add to Cart</a>
                </li>
                <?php endif; ?>
         <?php endfor; endif; ?>
         
        <?php else: ?>         
        
         <li class="right-rgt">
           <div class="right-a salads-wrapper">
             <h5>YOU CURRENTLY HAVE NO SAVED SANDWICHES</h5>
             <!-- <p>Use one of the options to the left to get started!</p> -->
           </div>
         </li>
         
        <?php endif; ?> 
       </ul>
       
       <div class="loadMoreGallery" style="text-align:center"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/star.png" alt="Loading" /> </div>
       
    </div>
	
	
	<?php $_from = $this->_tpl_vars['stdCategoryItems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['outer'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['outer']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['category']):
        $this->_foreach['outer']['iteration']++;
?>      
		<?php if (count($this->_tpl_vars['category']->categoryProducts) > 0): ?>
			<div class="salads-wrapper">
			  <h3><?php echo $this->_tpl_vars['category']->standard_cat_name; ?>
</h3>
			   <ul>
				<?php $_from = $this->_tpl_vars['category']->categoryProducts; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
          
          
					<?php if (is_array($this->_tpl_vars['order_data']['product']['item_id'])): ?> 
						<?php $this->assign('product_items_id', in_array($this->_tpl_vars['item']->id, $this->_tpl_vars['order_data']['product']['item_id'])); ?>
					<?php else: ?>
						<?php $this->assign('product_items_id', '0'); ?>
					<?php endif; ?>
					<?php $this->assign('prdid', $this->_tpl_vars['item']->id); ?>   
				 <li id="item-<?php echo $this->_tpl_vars['prdid']; ?>
" class="salads-menu-li">
				   <div class="left">
					 <h5><?php echo $this->_tpl_vars['item']->product_name; ?>
</h5>
                     <span class="price">$<?php echo $this->_tpl_vars['item']->product_price; ?>
</span>
					 <p><?php echo $this->_tpl_vars['item']->description; ?>
</p>
				   </div>
				   <div class="right">
					 <input type="hidden" class="amount" name="amount" value="<?php echo $this->_tpl_vars['item']->product_price; ?>
"/>
					 <a id="product-<?php echo $this->_tpl_vars['item']->id; ?>
" data-sandwich="<?php echo $this->_tpl_vars['product']; ?>
" data-sandwich_id="<?php echo $this->_tpl_vars['item']->id; ?>
" data-uid="<?php echo $this->_tpl_vars['uid']; ?>
" class="add-cart common_add_item_cart">Add to cart</a>
				   </div>
				 </li>
				<?php endforeach; endif; unset($_from); ?> 
			   </ul>
			</div>
		<?php endif; ?>	
	<?php endforeach; endif; unset($_from); ?>	
		
    
  </div>
</section>
<!-----------------Menu View Sandwich------------------------------->
<div class="sandwich-popup" id="view-sandwich" style="display:none;">
	<div class="summary-details-add-menu">
  	<a href="#" class="close-button"></a>
  <div class="banner"> <img src="images/added-menu-sandwiches.png" alt="<?php echo $this->_tpl_vars['title_text']; ?>
">
</div>
    <div class="final-list-wrapper">
	<h2 class="name">JON'S MOZZARELLA MASTERPIECE</h2>  
	<p class="desc view-popup-scroll">Corned Beef (1.0), Lettuce (N) Red Onion (L), Pickles (L), Tomato (N), Lettuce (L) Red Onion (L), Pickles (L), Lettuce (L) Red Onion (L), Pickles (L)  </p>
	<ul>
	<li class="user">Created by: <span>Jon C</span>.</li>
	<li class="created">Date Created:  <span>10/14/14</span> </li>
	<!-- <li class="likes">Likes: <span class="likes_cnt">10</span></li>
	<li class="menu-adds">Menu Adds: <span>24</span></li> -->
	</ul>
	
	<div class="toastit-wrapper">
    	<div class="checkbox-holder-final">
      	<input type="checkbox" class="savetousertoast toast" id="check1" name="check" class="toast">
       <label for="check1"> <span>Toast it!</span> <!--<span>Yum!</span>--></label>
      </div>
    </div>
	
	<div class="add-menu" style="padding-bottom:10px;">
	<!-- <a class="like like-sandwich" href="#"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/like-img.png" alt=""></img><span class="Like_sandwich">Like</span></a> -->
	<a class="share share-sandwich" href="#"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/share-img.png" alt=""></img><span class="Share_sandwich">Share</span></a>
	 <a class="add-to-menu remove_menu" href="#">REMOVE FROM MENU</a>
	      <input type="hidden" name="hidden_sandwich_data" id="hidden_sandwich_data" value="user_sandwich">
        <input type="hidden" name="hidden_sandwich_id" id="hidden_sandwich_id" value="">
	</div>
	
	<div class="add-cart">
	<h2 class="amount">$10.00</h2>
    <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
		<a href="javascript:void(0);" class="left_spinner"></a>
           <input id="resetQty" name="itemQuty" type="text" class="text-box"  value="01" readonly>
		<a href="javascript:void(0);" class="right_spinner"></a>
	</h3>	
	<a class="add-to-cart" href="#">Add to cart</a>
	</div>
	
    </div>	
  </div>
</div>

<!-----------------Menu View Sandwich------------------------------->