<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:10:51
  from 'C:\wamp64\www\hashbury\_mobile_new\app\theme\templates\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7dcb1e5283_70585630',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '49ce505280cc207901095f22244710d6adcb36be' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\theme\\templates\\footer.tpl',
      1 => 1616674210,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7dcb1e5283_70585630 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="sandwich-popup" id="login-block" style="display:none">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
  <div class="banner"> <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo1.png"  width="196" height="88" class="retina" alt="logo">
</div>
   <div class="login-wrapper">
   <h2>Login or register to proceed!</h2>

   <a href="<?php  echo _URL; ?>" class="connect-facebook">CONNECT WITH FACEBOOK<br/><span>VIEW FRIENDS' SANDWICHES</span></a>
   <a href="#" class="connect-login">Login</a>
   <a href="#" class="connect-create connect-register">Create account</a>
 
   </div>
  </div>
</div>

<div class="sandwich-popup" id="login-block-mail" style="display:none">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
  <div class="banner"> <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></div>
  
   <div class="login-mail-wrapper">
 
   <h2>Login</h2>
     <span class="error_msg"></span>
   <input class="text-email" id="lemail" name="" type="text" placeholder="Email" autocomplete="off">
    <input id="lpwd" class="text-password" name="" type="password" placeholder="Password" autocomplete="off">
	<a href="javascript:void(0);" id="login_btn" class="connect-create">Log in</a>
	<h6><a href="#" class="bold-text show-forgot-block">Forgot Password</a></h6>
   <!--  <h6>By creating an account you accept our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</h6> -->
   </div>
  </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['user_mail']->value) {?>

<div class="sandwich-popup" id="cookie-login-block" style="display:none">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
  <div class="banner"> <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></div>
  
   <div class="login-mail-wrapper">
   <span class="error_msg"></span>
   <h2>Confirm Password</h2>
       <input class="text-email" value="<?php echo $_smarty_tpl->tpl_vars['user_mail']->value;?>
" id="cemail" name="email" type="hidden" placeholder="Email">
       <input id="cpwd" class="text-password" name="" type="password" placeholder="Password">
	<a href="javascript:void(0);" id="cookie_login_btn" class="connect-create">Log in</a>
	<h6><a href="#" class="bold-text show-forgot-block">Forgot Password</a></h6>

   </div>
  </div>
</div>



<div class="sandwich-popup" id="cookie-fblogin-block" style="display:none">
  <div class="summary-details-login">
    <a href="#" class="close-button"></a>
  <div class="banner"> <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></div>
   <h1>Login with Facebook</h1>
    <p>This action requires you to be logged in with Facebook.</p> 
   <div class="login-mail-wrapper">
  <a target="_blank" href="<?php  echo _URL; ?>" id="fbLoginCookie" class="facebook-button connect-color ">  CONNECT WITH FACEBOOK</a>
   </div>
  </div>
</div>

<?php }?>

<div   id="summary" style="display:none;" class="sandwich-popup">
	<div class="summary-details-login"   >
  	<a class="close-button" href="#"></a>
  
  
    <div class="summary_details" style="min-height:220px;"></div>
  
 
  </div>
</div>


<div class="sandwich-popup" id="forgot-block" style="display:none">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
  <div class="banner"> <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></div>
  
   <div class="login-mail-wrapper">
   <span class="error_msg"></span>
   <h2>Forgot Password</h2>
   <input class="text-email" id="femail" name="" type="text" placeholder="Email">
	<a href="javascript:void(0);" id="forgot_btn" class="connect-create">Submit</a>
	<h6><a href="javascript:void(0)"  class="connect-login bold-text">Login</a></h6>
    <h6>By creating an account you accept our <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
site/termsofuse">Terms of Use</a> and <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
site/privacypolicy">Privacy Policy.</a></h6>
   </div>
  </div>
</div>

<div class="sandwich-popup" id="login-block-fb" style="display:none">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
    <div class="banner"> <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></div>
   <div class="login-facebook-wrapper">
   <h2>Login with Facebook</h2>
   <p>This action requires connecting with Facebook. Don't worry, we won't shadily post anything to your Facebook Wall.</p>
   <a href="#" class="connect-facebook">CONNECT WITH FACEBOOK</a>
   <p class="termsconditions">By creating an account you accept our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
   </div>
  </div>
</div>

<div class="sandwich-popup" id="register-block" style="display:none">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
  <div class="banner"> <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></div>
<div class="register-wrapper">
  
	<h2>Register</h2>
   <div class="form-holder">
    <span class="error_msg"></span>
      <input class="text-firstname" name="" id="rfname" type="text" placeholder="First Name" autocomplete="off">
      <input class="text-lastname" name="" id="rlname" type="text" placeholder="Last Name" autocomplete="off">
    </div>
    <input class="text-email" name="" id="remail" type="text" placeholder="Email" autocomplete="off">
    <input class="text-password" id="rpwd" name="" type="password" placeholder="Password" autocomplete="off">
    <a href="#" id="create_account" class="connect-create">Create account</a>
    <h6>By creating an account you accept our <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
site/termsofuse">Terms of Use</a> and <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
site/privacypolicy">Privacy Policy.</a></h6>
  </div>
</div>
</div>

<div class="sandwich-popup" id="product-descriptor" style="display:none;">
  <!--<div class="add-new-address-inner summary-details-add-menu">--> 
  <div class="summary-details-add-menu"> 
  <a href="#" class="close-button">&nbsp;</a>

	<form method="post" >
	<h2>CHOOSE A SALAD DRESSING</h2>
	<p>All dressings come on the side</p>
    <ul class="main-sandwich-list from-holder">
      
    </ul>
    <div class="separator-line"></div>
    <div class="hidden_fields"></div>
   
    
    <div class="add-cart">
		<h2 class="amount">$10.00</h2>
		<h3 class="cart-items popup_spinner">
			<a class="left_spinner" href="javascript:void(0);"></a>
				<input class="text-box" type="text" readonly="" value="01" name="qty">
			<a class="right_spinner" href="javascript:void(0);"></a>
		</h3>
		  
		  <a class="add_desc" href="javascript:void(0);">Add to cart</a>
	</div>
    
    </form>
   </div>
</div> 

<!-- sandwich edit popup starts -->
   <div class="sandwich-popup edit_sandwich-popup" id="sandwiches_quick_edit" style="display: none;">
            <div class="quick__edit-sandwich">
                <a class="close-button" href="#"></a>
                <h2>Edit Sandwich</h2>
                <form>
                  <div class="bread_images" style="display:none">
            
                    <span class="bread_image">
                    <div class="image-holder"> <img style="display:none" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/create-sandwich-bread.png" alt="Instagram"> </div>
                    </span>
                    
                    <span class="protein_image">
                    </span>
                    
                    <span class="cheese_image">
                    </span>
                    
                    <span class="topping_image">
                    </span>

                    <span class="condiments_image">
                    </span>
                  
                  </div>
                    <div class="edit-wrap-input">
                        <label>Name</label>
                        <input type="text" name="sandwich_name" placeholder="">
                    </div>
                    <div class="edit-wrap-input">
                        <label>Bread</label>
                        <select id="bread_drpdown">
                            <option>Ciabatta</option>
                            <option>Ciabatta</option>
                            <option>Ciabatta</option>
                        </select>
                    </div>
                    <div class="editable-sandwich-container mCustomScrollbar">
                        <div class="spiner-edit--wrapper">
                            <label>Protein</label>
                            <div class="new_spiner-edit">
                                <p>Turkey</p>
                                <div>
                                    <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                                        <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                                        <a href="javascript:void(0);"
                                            class="sandwich_gallery_view_popup left_spinner"></a>
                                        <input id="resetQty" name="itemQuty" type="text" class="text-box"
                                            value="Half Portion" readonly="">
                                        <a href="javascript:void(0);"
                                            class="sandwich_gallery_view_popup right_spinner"></a>
                                    </h3>
                                    <a href="#" class="delete-edit">
                                        <img src="images/delete_spiner.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="add-item--more">
                            <a href="javascript:void(0)" class="open-add-item"><span>+</span> Add new item </a>
                        </div>
                </form>

            </div>
            <div class="add-cart new-popup-bottom-fixed">
                    <h2 class="amount">$117.00</h2>
                    <a class="add-to-cart" id="save-quickedit-sandwich" href="javascript:void(0)">Save</a>
                    <a class="cancel-quick-edit" href="javascript:void(0)">Cancel</a>
                    </div> 
        </div>
<!-- featured sandwich edit popup ends -->

<!-- Add Item popup from Quick edit sandwich popup -->
      <div class="sandwich-popup edit_sandwich-popup" id="sandwiches_quick_edit_add_item" style="display: none;">
            <div class="quick__edit-sandwich">
                <a class="close-button" href="#"></a>
                <h2>Add Item</h2>
                <form class="add-item__single">
                    <div class="edit-wrap-input">
                        <label>Category</label>
                        <select id="add-item-category-select">
                            <option>Protein</option>
                            <option>Ciabatta</option>
                            <option>Ciabatta</option>
                        </select>
                    </div>
                    <div class="edit-wrap-input">
                            <label>item</label>
                            <select id="add-item-select">
                                <option>Turkey</option>
                                <option>Ciabatta</option>
                                <option>Ciabatta</option>
                            </select>
                        </div>
                    

                            <div class="new_spiner-edit add-item--spiner--single">
                                <h3 class="cart-items popup_spinner new_popup-spiner__edit">
                                    <a href="javascript:void(0);"
                                        class="left_spinner"></a>
                                    <input id="resetQty" name="itemQuty" type="text" class="text-box"
                                        value="Normal Portion" readonly="">
                                    <a href="javascript:void(0);"
                                        class="right_spinner"></a>
                                </h3>
                            </div>
                    
                  </form>
                </div>
                <div class="add-cart new-popup-bottom-fixed">
                  <a class="add-to-cart add-item-to-sandwich" href="javascript:void(0)">ADD</a>
                  <a class="add-to-cart cancel-add-item" href="javascript:void(0)">Cancel</a>
                </div>
            </div>
   <!-- End of Add Item popup from Quick edit sandwich popup --> 

<!-- Salad Popup --> 
<div class="sandwich-popup edit_sandwich-popups" id="salad_popup" style="display: none;">
            <div class="quick__edit-sandwich">
                <a class="close-button" href="#"></a>
                <div class="salad_item__popupwrap">
                    <form id="popup-form">
                        <div class="salad_item_image">
                            <img src="">
                        </div>
                        <div class="salad_item__desc">
                            <h4>
                                Asian Crisp Salad
                            </h4>
                            <p>
                                Romaine lettuce, cabbage, mandarin orange, crispy wonton, green onion, shredded carrot,
                                edamame,
                                sesame seeds, and thai peanut dressing
                            </p>
                            <div class="hidden-values">
                    
                            </div>
                            <div class="salad_item__options ">
                              <div class="add_modifier">
                                <a href="#">
                                    <span class="modifier_desc">Add a Protein</span>
                                </a>
                                <div>
                                    <div class="salad-options">
                                        <div class="salad-option-chck">
                                        <div class="salad-item-wrap">
                                            
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <a href="#" class="spcl_instruction">
                                    Special Instructions
                                </a>
                                <div class="special-instructions-text spcl_instruction">
                                    <textarea name="spcl_instructions"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
            <div class="add-cart new-popup-bottom-fixed">
                <h2 class="amount">$117.00</h2>
                <div class="salad-add-item">
                <div class="popup_spinner">
                        <a href="#" class="left_spinner"></a>
                        <input name="itemQuatity" type="text" class="text-box" value="01" readonly="">
                        <a href="#" class="right_spinner"></a>
                    </div>
                <a class="salad-add add-salad-to-cart" href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/cart.png">Add</a>
            </div>
            </div>
        </div>
</div> 
</div> 
<!-- sandwich popup -->
<div class="sandwich-popup" id="mobile-sandwiches-popup" style="display: none;">
    <div class="summary-details-add-menu">
        <div class="banner"> <a href="#" class="close-button">&nbsp;</a> <img  src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
images/salad_popup/salad_item__popupimage.png" alt="BARNEY BROWN">
        </div>
        <div class="final-list-wrapper">
            <h2 class="sandwich_name">VASANTH'S PEPPERONI TREAT</h2>
            <p class="view-popup-scroll">Whole Wheat Wrap, Pepperoni (1.0), Muenster (N), Cucumbers (H), Dijon Mustard (S)</p>
                <ul>
                    <li>Created by:  <span class="user_name username">vasanth kumar</span>.</li>
                    <li>Date Created:  <span class="created_date datefromat">10/15/19</span></li>
                </ul>
                
                <div class="toastit-wrapper">
                    <div class="checkbox-holder-final">
                        <input type="checkbox" class="toast menucheck" value="check18" name="check" id="check18">
                        <label for="check18"> <span>Toast it!</span> </label>
                    </div>
                </div>
                <input type="hidden" name="hidden_sandwich_data" id="hidden_sandwich_data" value="user_sandwich">
                <input type="hidden" name="hidden_sandwich_id" id="hidden_sandwich_id" value="">
                <div class="add-menu toastit-wrapper">

                    <a href="#" class="share-sandwich-popup">Share</a>
                    <a href="#" class="edit-sandwich-popup">Edit</a> 
                    <a href="#" class="save-sandwich-popup saved-sandwich-popup saveMenu">Saved</a>
                    <div class="checkbox-holder-final check-sandwich-private">
                        <input type="checkbox" class="toast make_private_check" value="check-sandwich-private" name="check" id="check-sandwich-private">
                        <label for="check-sandwich-private"> <span>Make Private</span></label>
                        <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/password-gold.png">

                    </div>
                    <img class="gold-flag" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/flag.png">
                </div>
            <div class="add-cart">
                <h2 class="amount">$10.00</h2>
                <a class="addToCart" href="#">Add to cart</a>
                <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                    <a href="javascript:void(0);" class="leftSpinner" ></a>
                    <input id="itemCount" name="itemCount" type="text" class="text-box" value="01" readonly="">
                    <a href="javascript:void(0);" class="rightSpinner"></a>
                </h3>
            </div>

        </div>  
    </div>
</div>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.ui.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/lozad-1.9.0.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.slicknav.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/modernizr.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/checkout.js?v=2.2"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/common.js?v=2.2"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/sandwichcreatormobile.js?v=2.2"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/sandwich_quickedit.js?v=2.2"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.mCustomScrollbar.concat.min.js"><?php echo '</script'; ?>
> 
</body>
</html>
<?php }
}
