<?php /* Smarty version 2.6.25, created on 2018-01-19 01:05:46
         compiled from load_more_user_menu_sandwiches.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'load_more_user_menu_sandwiches.tpl', 2, false),array('modifier', 'is_array', 'load_more_user_menu_sandwiches.tpl', 53, false),array('modifier', 'in_array', 'load_more_user_menu_sandwiches.tpl', 54, false),array('modifier', 'date_format', 'load_more_user_menu_sandwiches.tpl', 67, false),array('modifier', 'upper', 'load_more_user_menu_sandwiches.tpl', 76, false),array('modifier', 'truncate', 'load_more_user_menu_sandwiches.tpl', 76, false),)), $this); ?>

        <?php if (count($this->_tpl_vars['usersandwich']) > 0): ?>
        
        
			 <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['usersandwich'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
            
            
             <?php $this->assign('prot_data', $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
              if($c){
              foreach($c as $c){
               $result_1 .= ' '.$c;
              } 
              }
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
            
                  <?php if (is_array($this->_tpl_vars['order_data']['user_sandwich']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id'], $this->_tpl_vars['order_data']['user_sandwich']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
                   
                   
                <?php if ($this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['by_admin'] == 0): ?>
                   <?php $this->assign('img_path', $this->_tpl_vars['CMS_URL']); ?>
                <?php else: ?>
                   <?php $this->assign('img_path', $this->_tpl_vars['ADMIN_URL']); ?>
                <?php endif; ?>   
  
  <?php if ($this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid'] > 0): ?>          
   <li id="item-<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
" class="right-rgt" rel="ADD TO CART" data-sandwich-name="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-sandwich_desc="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
"  data-userid ="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
"  data-toast="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['menu_toast']; ?>
" data-menuadds="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['menu_add_count']; ?>
" data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-formatdate="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]; ?>
"  data-date="<?php echo ((is_array($_tmp=$this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['date_of_creation'])) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp)); ?>
" data-price="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
" data-id="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-likeid="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['like_id']; ?>
" data-likecount="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['like_count']; ?>
" data-flag="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['flag']; ?>
" data-image="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png"> 
		 
				
				        <img width="53" height="24"   class="retina toasted"  style=" <?php if ($this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['menu_toast'] == 1): ?> display:block; <?php else: ?> display:none; <?php endif; ?>" alt="toasted" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/toasted.png">
				        
           <img class="sand-pop" title="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png">
       
   
           
           <h4><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['sandwich_name'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "..", true) : smarty_modifier_truncate($_tmp, 20, "..", true)); ?>
</h4>
           <!--<p><?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['like_count']; ?>
 likes </p> --> <!-- Like btn  -->
          
          
           <a class="common_add_item_cart add-cart" data-uid="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['uid']; ?>
" data-sandwich_id="<?php echo $this->_tpl_vars['usersandwich'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-sandwich="user_sandwich" href="#">Add to Cart</a>
                </li>
                <?php endif; ?>
         <?php endfor; endif; ?>
         
    <?php else: ?>None<?php endif; ?>     