<?php /* Smarty version 2.6.25, created on 2020-11-12 08:04:10
         compiled from cart-list-items.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'cart-list-items.tpl', 15, false),array('modifier', 'string_format', 'cart-list-items.tpl', 16, false),array('modifier', 'number_format', 'cart-list-items.tpl', 37, false),array('modifier', 'truncate', 'cart-list-items.tpl', 44, false),array('modifier', 'cat', 'cart-list-items.tpl', 105, false),)), $this); ?>

<?php if ($this->_tpl_vars['itemsCount'] > 0): ?>  
          <table class="cart_list_header">
					<thead>
						<tr>
							<th class="item">Items</th>
							<th class="qty">Qty</th>
							<th class="price">price</th>
							<th class="remove_header">&nbsp;</th>
						</tr>
					</thead>
					<tbody></tbody>
			</table>
                        <div class="hundred-min-red-bar first-time"><?php if ($this->_tpl_vars['totalNumber'] < 10 && $this->_tpl_vars['pickupOrDelivery'] != 0): ?> <p>
					<?php echo smarty_function_math(array('assign' => 'reaminingNumber','equation' => "10.00-".($this->_tpl_vars['totalNumber'])), $this);?>

						$10.00 SUBTOTAL MINIMUM. $<?php echo ((is_array($_tmp=$this->_tpl_vars['reaminingNumber'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 TO GO!
                            </p><?php endif; ?></div>
			<div class="cart_item_list_wrapper">
			<div class="cart_item_list_scroller common-scrollbar">	
			<table class="cart_list_items">		
					<tbody>
					<?php $_from = $this->_tpl_vars['sandwiches']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['myId'] => $this->_tpl_vars['sandwich']):
?>
						<tr class="cart_item_list">
							<td data-sandwich-ingredients='<?php echo $this->_tpl_vars['sandwich']['sandwich_data']; ?>
' data-toast = "<?php echo $this->_tpl_vars['sandwich']['toast']; ?>
" class="item cart-item-name"><?php echo $this->_tpl_vars['sandwich']['sandwich_name']; ?>
</td>
							<td class="qty inputbox-disable-overlay-position">
							<div class="inputbox-disable-overlay checkout-input-disable">&nbsp;</div> 
							   <a href="javascript:void(0);" class="left_spinner_checkout"><img class="qty-img-left" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/left-arrow-hover.png"></a>							   
							   
                                                           <input name="itemQuty" type="hidden" class="qtybox"  value="<?php if ($this->_tpl_vars['sandwich']['item_qty'] < 10): ?>0<?php endif; ?><?php echo $this->_tpl_vars['sandwich']['item_qty']; ?>
">
                                                           <span class="qtybox"><?php if ($this->_tpl_vars['sandwich']['item_qty'] < 10): ?>0<?php endif; ?><?php echo $this->_tpl_vars['sandwich']['item_qty']; ?>
</span>
                                                           <a href="javascript:void(0);" class="right_spinner_checkout"><img class="qty-img-right" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/right-arrow-hover.png"></a>
							   <input type="hidden" class="sandwich_id" value="<?php echo $this->_tpl_vars['sandwich']['id']; ?>
" />
							  <input type="hidden" class="order_item_id" value="<?php echo $this->_tpl_vars['sandwich']['order_item_id']; ?>
" />
							  <input type="hidden" class="data_type" name="data_type" value="user_sandwich"/>
							  
							</td>
							<td class="price">$<?php echo ((is_array($_tmp=$this->_tpl_vars['sandwich']['item_total'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2) : number_format($_tmp, 2)); ?>
</td>
							<td class="remove"><a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="<?php echo $this->_tpl_vars['sandwich']['order_item_id']; ?>
" data-item="<?php echo $this->_tpl_vars['sandwich']['item_id']; ?>
">&nbsp;</a> </td>
							  <input type="hidden" class="bread_type" name="bread_type" value="<?php echo $this->_tpl_vars['sandwich']['bread_type']; ?>
"/> 
						</tr>
					<?php endforeach; endif; unset($_from); ?>
					<?php $_from = $this->_tpl_vars['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['myId'] => $this->_tpl_vars['product']):
?>
					   <tr>
							<td class="item"><?php echo ((is_array($_tmp=$this->_tpl_vars['product']->product_name)) ? $this->_run_mod_handler('truncate', true, $_tmp, 19, "..", true) : smarty_modifier_truncate($_tmp, 19, "..", true)); ?>

							<?php if ($this->_tpl_vars['product']->extra_id != ""): ?> <p style="font-size: 10px;">Modifiers : <strong><?php echo $this->_tpl_vars['product']->extra_id; ?>
</strong></p> <?php endif; ?>                 
        					<?php if ($this->_tpl_vars['product']->spcl_instructions != ""): ?> <p style="font-size: 10px;">Special Instructions : <strong><?php echo $this->_tpl_vars['product']->spcl_instructions; ?>
</strong></p> <?php endif; ?></td>
							<td class="qty">
							<div class="inputbox-disable-overlay checkout-input-disable">&nbsp;</div> 
								 <a href="javascript:void(0);" class="left_spinner_checkout"><img class="qty-img-left" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/left-arrow-hover.png"></a>							   
							   <!--<input name="itemQuty" type="text" class="qtybox"  value="<?php if ($this->_tpl_vars['product']->item_qty < 10): ?>0<?php endif; ?><?php echo $this->_tpl_vars['product']->item_qty; ?>
">-->
							   <input name="itemQuty" type="hidden" class="qtybox"  value="<?php if ($this->_tpl_vars['product']->item_qty < 10): ?>0<?php endif; ?><?php echo $this->_tpl_vars['product']->item_qty; ?>
">
                                                           <span class="qtybox"><?php if ($this->_tpl_vars['product']->item_qty < 10): ?>0<?php endif; ?><?php echo $this->_tpl_vars['product']->item_qty; ?>
</span>
                                                           
                                                           <a href="javascript:void(0);" class="right_spinner_checkout"><img class="qty-img-right" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/right-arrow-hover.png"></a>
								<input type="hidden" class="sandwich_id" value="<?php echo $this->_tpl_vars['product']->id; ?>
" />
								<input type="hidden" name="product_parent_type" value="<?php echo $this->_tpl_vars['product']->product_parent_type; ?>
" />
								<input type="hidden" class="order_item_id" value="<?php echo $this->_tpl_vars['product']->order_item_id; ?>
" />
								<input type="hidden" class="data_type" name="data_type" value="product">
							</td>
							<td class="price">$<?php echo ((is_array($_tmp=$this->_tpl_vars['product']->item_total)) ? $this->_run_mod_handler('number_format', true, $_tmp, 2) : number_format($_tmp, 2)); ?>
</td>							
							<td class="remove"><a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="<?php echo $this->_tpl_vars['product']->order_item_id; ?>
" data-item="<?php echo $this->_tpl_vars['product']->item_id; ?>
">&nbsp;</a> </td> 
						</tr>
					<?php endforeach; endif; unset($_from); ?>
					
					
  					</tbody>
			</table>
			</div>
			</div>
             
			
<?php else: ?>
			<ul>
				<li>
				<h3>There are currently no items in your shopping cart.</h3>  
				</li>
			</ul>				
<?php endif; ?>



			<?php if ($this->_tpl_vars['itemsCount'] > 0): ?> 
			 <div class="clearfix sales-info">
			<div class="sales-tax"><p style="visibility:hidden;">Sales tax inclusive </p></div>
			 <div class="check-out">
                <div class="tax-wrapper">
                 <div class="apply-discount-code"><a href="#">Apply discount code</a></div>
				</div>
                <div class="tax-total-wrapper">
					<div class="total"><p>SUBTOTAL</p><p>TAX</p><p>TIP</p><p style="display:none" class="p-discount-title">DISCOUNT</p></div>
					<div class="total"><p class="p-subtotal">$<?php echo ((is_array($_tmp=$this->_tpl_vars['total'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2) : number_format($_tmp, 2)); ?>
</p><p class="p-tax">$<?php echo $this->_tpl_vars['tax']; ?>
</p><p class="p-tips">$<?php echo ((is_array($_tmp=$this->_tpl_vars['tips'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</p><p style="display:none" class="p-discount">$0.00</p></div>
				</div>	
               </div>
                <div class="clearfix"></div><div class="clearfix"></div>
                 <div class="check-out">
                <div class="tax-2"><!-- visibilityHide -->
                    <p class="tip">TIP</p>
                		<div id="dd" class="wrapper-dropdown-tip" tabindex="1">	
                		<span class="tip-wrapper">					
						<select id="select_tip_amount">
							<?php if ($this->_tpl_vars['pickupOrDelivery'] == 0): ?>
								<option value="0.00">$0.00</option>
								<?php unset($this->_sections['foo']);
$this->_sections['foo']['name'] = 'foo';
$this->_sections['foo']['start'] = (int)1;
$this->_sections['foo']['loop'] = is_array($_loop=$this->_tpl_vars['MAX_TIP']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['foo']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['foo']['show'] = true;
$this->_sections['foo']['max'] = $this->_sections['foo']['loop'];
if ($this->_sections['foo']['start'] < 0)
    $this->_sections['foo']['start'] = max($this->_sections['foo']['step'] > 0 ? 0 : -1, $this->_sections['foo']['loop'] + $this->_sections['foo']['start']);
else
    $this->_sections['foo']['start'] = min($this->_sections['foo']['start'], $this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] : $this->_sections['foo']['loop']-1);
if ($this->_sections['foo']['show']) {
    $this->_sections['foo']['total'] = min(ceil(($this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] - $this->_sections['foo']['start'] : $this->_sections['foo']['start']+1)/abs($this->_sections['foo']['step'])), $this->_sections['foo']['max']);
    if ($this->_sections['foo']['total'] == 0)
        $this->_sections['foo']['show'] = false;
} else
    $this->_sections['foo']['total'] = 0;
if ($this->_sections['foo']['show']):

            for ($this->_sections['foo']['index'] = $this->_sections['foo']['start'], $this->_sections['foo']['iteration'] = 1;
                 $this->_sections['foo']['iteration'] <= $this->_sections['foo']['total'];
                 $this->_sections['foo']['index'] += $this->_sections['foo']['step'], $this->_sections['foo']['iteration']++):
$this->_sections['foo']['rownum'] = $this->_sections['foo']['iteration'];
$this->_sections['foo']['index_prev'] = $this->_sections['foo']['index'] - $this->_sections['foo']['step'];
$this->_sections['foo']['index_next'] = $this->_sections['foo']['index'] + $this->_sections['foo']['step'];
$this->_sections['foo']['first']      = ($this->_sections['foo']['iteration'] == 1);
$this->_sections['foo']['last']       = ($this->_sections['foo']['iteration'] == $this->_sections['foo']['total']);
?>
									<option <?php if ($this->_tpl_vars['tips'] == $this->_sections['foo']['index'] || $this->_sections['foo']['index'] < $this->_tpl_vars['tips']): ?> selected <?php endif; ?> value="<?php echo $this->_sections['foo']['index']; ?>
">$<?php echo $this->_sections['foo']['index']; ?>
.00</option>
									<?php $this->assign('tipVal', ((is_array($_tmp=$this->_sections['foo']['index'])) ? $this->_run_mod_handler('cat', true, $_tmp, '.50') : smarty_modifier_cat($_tmp, '.50'))); ?>
									<?php if ($this->_sections['foo']['index'] < 10): ?>  
									<option value="<?php echo $this->_sections['foo']['index']; ?>
.50">$<?php echo $this->_sections['foo']['index']; ?>
.50</option>
									<?php endif; ?>
								<?php endfor; endif; ?> 
								<option <?php if ($this->_tpl_vars['tips'] == $this->_tpl_vars['MAX_TIP']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['MAX_TIP']; ?>
.00">$<?php echo $this->_tpl_vars['MAX_TIP']; ?>
.00</option>
							<?php else: ?>

								<?php unset($this->_sections['foo']);
$this->_sections['foo']['name'] = 'foo';
$this->_sections['foo']['start'] = (int)$this->_tpl_vars['MIN_TIP'];
$this->_sections['foo']['loop'] = is_array($_loop=$this->_tpl_vars['MAX_TIP']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['foo']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['foo']['show'] = true;
$this->_sections['foo']['max'] = $this->_sections['foo']['loop'];
if ($this->_sections['foo']['start'] < 0)
    $this->_sections['foo']['start'] = max($this->_sections['foo']['step'] > 0 ? 0 : -1, $this->_sections['foo']['loop'] + $this->_sections['foo']['start']);
else
    $this->_sections['foo']['start'] = min($this->_sections['foo']['start'], $this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] : $this->_sections['foo']['loop']-1);
if ($this->_sections['foo']['show']) {
    $this->_sections['foo']['total'] = min(ceil(($this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] - $this->_sections['foo']['start'] : $this->_sections['foo']['start']+1)/abs($this->_sections['foo']['step'])), $this->_sections['foo']['max']);
    if ($this->_sections['foo']['total'] == 0)
        $this->_sections['foo']['show'] = false;
} else
    $this->_sections['foo']['total'] = 0;
if ($this->_sections['foo']['show']):

            for ($this->_sections['foo']['index'] = $this->_sections['foo']['start'], $this->_sections['foo']['iteration'] = 1;
                 $this->_sections['foo']['iteration'] <= $this->_sections['foo']['total'];
                 $this->_sections['foo']['index'] += $this->_sections['foo']['step'], $this->_sections['foo']['iteration']++):
$this->_sections['foo']['rownum'] = $this->_sections['foo']['iteration'];
$this->_sections['foo']['index_prev'] = $this->_sections['foo']['index'] - $this->_sections['foo']['step'];
$this->_sections['foo']['index_next'] = $this->_sections['foo']['index'] + $this->_sections['foo']['step'];
$this->_sections['foo']['first']      = ($this->_sections['foo']['iteration'] == 1);
$this->_sections['foo']['last']       = ($this->_sections['foo']['iteration'] == $this->_sections['foo']['total']);
?>
									<option <?php if ($this->_tpl_vars['tips'] == $this->_sections['foo']['index'] || $this->_sections['foo']['index'] < $this->_tpl_vars['tips']): ?> selected <?php endif; ?> value="<?php echo $this->_sections['foo']['index']; ?>
">$<?php echo $this->_sections['foo']['index']; ?>
.00</option>
									<?php $this->assign('tipVal', ((is_array($_tmp=$this->_sections['foo']['index'])) ? $this->_run_mod_handler('cat', true, $_tmp, '.50') : smarty_modifier_cat($_tmp, '.50'))); ?>
									<?php if ($this->_sections['foo']['index'] < 10): ?>  
									<option value="<?php echo $this->_sections['foo']['index']; ?>
.25">$<?php echo $this->_sections['foo']['index']; ?>
.25</option>
									<option value="<?php echo $this->_sections['foo']['index']; ?>
.50">$<?php echo $this->_sections['foo']['index']; ?>
.50</option>
									<option value="<?php echo $this->_sections['foo']['index']; ?>
.75">$<?php echo $this->_sections['foo']['index']; ?>
.75</option>
									<?php endif; ?>
								<?php endfor; endif; ?>
								<option <?php if ($this->_tpl_vars['tips'] == $this->_tpl_vars['MAX_TIP']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['MAX_TIP']; ?>
.00">$<?php echo $this->_tpl_vars['MAX_TIP']; ?>
.00</option>
							<?php endif; ?>	
						</select>
						</span>
						
					</div>
                </div>
                <div class="grand-total"><h3><span>Total</span></h3></div>
                <div class="grand-total"><h2>$<?php echo ((is_array($_tmp=$this->_tpl_vars['grandtotal'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2) : number_format($_tmp, 2)); ?>
</h2></div>
				<?php if ($this->_tpl_vars['total'] >= 0): ?> <p class="est-delivery-time">EST. DELIVERY TIME:
	                <?php if ($this->_tpl_vars['total'] >= 0 && $this->_tpl_vars['total'] < 100): ?>30-60 mins
		                <?php elseif ($this->_tpl_vars['total'] >= 100 && $this->_tpl_vars['total'] < 200): ?>45-75 mins
		                <?php elseif ($this->_tpl_vars['total'] >= 200 && $this->_tpl_vars['total'] < 300): ?>60-90 mins
		                <?php elseif ($this->_tpl_vars['total'] >= 300 && $this->_tpl_vars['total'] < 400): ?>1-2 hrs
		                <?php elseif ($this->_tpl_vars['total'] >= 400 && $this->_tpl_vars['total'] < 500): ?>2-3 hrs
		                <?php elseif ($this->_tpl_vars['total'] >= 500): ?>24 hours                
	                <?php endif; ?></p>
	            <?php endif; ?>
               </div>
               </div>
               <?php endif; ?>


<input type="hidden" value="<?php echo $this->_tpl_vars['order_string']; ?>
" name="odstring" />
<input type="hidden" name="hidden_grand_total" id="hidden_grand_total" value="<?php echo $this->_tpl_vars['grandtotal']; ?>
">
<input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="<?php echo $this->_tpl_vars['total']; ?>
">
<input type="hidden" name="hidden_tip" id="hidden_tip" value="<?php echo $this->_tpl_vars['tips']; ?>
">
<input type="hidden" name="hidden_tax" id="hidden_tax" value="<?php echo $this->_tpl_vars['tax']; ?>
">
<input type="hidden" name="hidden_discount_id" id="hidden_discount_id" value="">
<input type="hidden" name="hidden_discount_amount" id="hidden_discount_amount" value="">