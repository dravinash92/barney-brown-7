<?php /* Smarty version 2.6.25, created on 2020-10-15 10:41:34
         compiled from home.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'home.tpl', 1, false),array('modifier', 'cat', 'home.tpl', 19, false),array('modifier', 'rand', 'home.tpl', 49, false),)), $this); ?>
<?php if (count($this->_tpl_vars['pastOrder']) == 0): ?>
<div class="homepageloader">
<img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/ellipsis.svg" class="homeAnimationLoaderImg" alt="">
</div>
<?php endif; ?>

<div class="banner home-banner-div home-banner-div-new"> 
	<div class="banner-wrapper"> 
	 
	
	<?php if ($this->_tpl_vars['uid'] != '' && count($this->_tpl_vars['pastOrder']) > 0): ?>  
			<?php if ($this->_tpl_vars['pastOrder']): ?>
			<?php if ($this->_tpl_vars['pastOrder']['details']['by_admin'] == 1): ?>
			  <?php $this->assign('path', $this->_tpl_vars['ADMIN_URL']); ?>		
			<?php else: ?>
			  <?php $this->assign('path', $this->_tpl_vars['SITE_URL']); ?>	
			<?php endif; ?>
			
			<?php $this->assign('image_exist', ((is_array($_tmp=$this->_tpl_vars['image_path'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['pastOrder']['details']['image']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['pastOrder']['details']['image']))); ?>	
			<?php 
				$d = $this->get_template_vars('image_exist');			 
				$ch = curl_init($d);
				curl_setopt($ch, CURLOPT_NOBODY, true);
				curl_exec($ch);
				$retCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);		
			 ?>
			
	
			<?php  if($retCode==200){  ?>	
			<span class="new_banner-top">
				<img class="banner-login" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['pastOrder']['details']['image']; ?>
" alt="<?php echo $this->_tpl_vars['title_text']; ?>
">	
				<img class="banner-img-top" src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/banner-top.png" alt="banner-top">		
			</span>
			<?php  } else {  ?>
				<img class="banner-login" src='<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/BB_logo_placeholder.png' >
			<?php  }  ?>
		
		<h2>Your Most Recent Order</h2>
		<h3><?php echo $this->_tpl_vars['pastOrder']['street']; ?>
</h3>
		<p> <?php if ($this->_tpl_vars['pastOrder']['details']['is_delivery'] == 0): ?> pick-up <?php else: ?> Delivery <?php endif; ?> - <?php echo $this->_tpl_vars['pastOrder']['date']; ?>
</p>
		<?php if ($this->_tpl_vars['pastOrder']['details']['bread'] != ""): ?>
            <span class="item_details" data-bread="<?php echo $this->_tpl_vars['pastOrder']['details']['bread']; ?>
" data-sandwich_desc="<?php echo $this->_tpl_vars['pastOrder']['details']['desc']; ?>
" data-sandwich_desc_id="<?php echo $this->_tpl_vars['pastOrder']['details']['desc_id']; ?>
" data-id="<?php echo $this->_tpl_vars['pastOrder']['details']['item_id']; ?>
"></span>
        <?php endif; ?>
		<div class="button-wrapper"> 
			<?php if ($this->_tpl_vars['itemsAvailable'] > 0): ?>
			<a href="#" class="reorder reorder-item" data-order="<?php echo $this->_tpl_vars['order_id']; ?>
" data-from="home_page">reorder</a>
			<?php endif; ?>
			<a class="place-new-order" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/sandwichMenu/?pid=<?php echo ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990)); ?>
">place new order</a>
		</div>
		<?php else: ?>
		
		<?php endif; ?>
	 
		<?php else: ?>
		
		
	   <div class="homebannerauto">
	    
     	  <span class="homepageAutoBanner">
     	  <span class="sandwich-1">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/homepage_animation/sandwich1/1.png" style="display:block" alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich1/2.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich1/3.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich1/4.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich1/5.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich1/6.png" style="display:none"  alt="">
     	  </span>
     	  
     	  <span class="sandwich-2">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich2/7.png"  style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich2/8.png"  style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich2/9.png"  style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich2/10.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich2/11.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich2/12.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich2/13.png" style="display:none"  alt="">
     	  </span>
     	  
     	  <span class="sandwich-3">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich3/14.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich3/15.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich3/16.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich3/17.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich3/18.png" style="display:none"  alt="">
     	    <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/homepage_animation/sandwich3/19.png" style="display:none"  alt="">
     	  </span>
     	  
     	  </span>
      </div> 	  
      
		  <h2>Sandwiches Your way!</h2>
		  <?php if ($this->_tpl_vars['uid'] != ''): ?>
		    <p>Build your own sandwich in 5 easy steps using a <br>curated selection of the finest local ingredients!</p>
		  	<?php else: ?>
		  	<p>Welcome to Barney Brown, a custom-made social-sandwiching experience. Get started by building your own sandwich in just 5 easy steps.</p>
		  	<?php endif; ?>

		  		  <a class="place-order" href="<?php if ($this->_supers['session']['uid'] != ""): ?><?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/<?php else: ?><?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/?pid=<?php echo ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990)); ?>
<?php endif; ?>">get started</a>

		  
 <?php echo '
 <script type="text/javascript">
 window.addEventListener(\'load\',function(){ mobile_ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
 </script>
'; ?>

		
		 <!-- <img src="<?php echo $this->_tpl_vars['banner_image']; ?>
" alt="<?php echo $this->_tpl_vars['title_text']; ?>
">
		  <h2>Sandwiches Your way!</h2>
		  <p>Build your own sandwich in 5 easy steps<br>using a curated selection of the best<br> local breads and other fine ingredients</p>
		  <a class="place-order" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/">place order</a>
		  
		  -->
		
	<?php endif; ?> 
	</div>
</div>
<section class="sandwich-menu">
<div class="sa-delivery" style="background:#695b45;font:bold 11px/16px 'Open Sans';letter-spacing: 0px;text-align:center;padding:5px 0;">
		<p class="timing"><span>Manhattan Delivery, </span>Mon - Fri 10:00AM - 4:00PM</p>
</div>
  <ul class="home_page_menu">
  	<li>
      <div class="left">		
			<h2>Sandwiches</h2>
			<p>A MENU OF ENDLESS POSSIBILITIES</p>        
      </div>
      <div class="right">  <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/sandwichMenu/index/?pid=<?php echo ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990)); ?>
" class="view-button">VIEW</a> </div>
    </li>
    <li>		
		<div class="left">		
			<h2>Salads</h2>
			<p>A SELECTION OF OUR CLASSIC SALADS</p>
		
		</div>		
      <div class="right">  <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/salads/?pid=<?php echo ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990)); ?>
" class="view-button">VIEW</a> </div>
    </li>
    
    <li>
      <div class="left">
         <h2>Snacks</h2>
        <p>A LITTLE SOMETHING ON THE SIDE</p>
      </div>
	   <div class="right"> <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/snacks/?pid=<?php echo ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990)); ?>
" class="view-button">VIEW</a> </div>
	   </li>
	   <li>
	  <div class="left">
        <h2>Drinks</h2>
        <p>SOMETHING TO QUENCH YOUR THIRST</p>
      </div>
      <div class="right"> <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/drinks/?pid=<?php echo ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990)); ?>
" class="view-button">view</a> </div>
    </li>
  </ul>
</section>