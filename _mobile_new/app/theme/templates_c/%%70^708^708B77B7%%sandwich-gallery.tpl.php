<?php /* Smarty version 2.6.25, created on 2020-09-03 00:26:58
         compiled from sandwich-gallery.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', 'sandwich-gallery.tpl', 4, false),array('modifier', 'count', 'sandwich-gallery.tpl', 21, false),array('modifier', 'replace', 'sandwich-gallery.tpl', 29, false),array('modifier', 'is_array', 'sandwich-gallery.tpl', 85, false),array('modifier', 'in_array', 'sandwich-gallery.tpl', 86, false),array('modifier', 'upper', 'sandwich-gallery.tpl', 105, false),array('modifier', 'truncate', 'sandwich-gallery.tpl', 105, false),array('modifier', 'capitalize', 'sandwich-gallery.tpl', 193, false),)), $this); ?>
<section class="sandwich create-sandwhich-menu-wrapper">
<div class="create-sandwhich-menu">
    <div class="finalize-wrapper">
        <div class="counts-sandwich"><p class="sandwich_count" data-count="<?php echo $this->_tpl_vars['totalCount']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['totalCount'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</p><span>CREATIONS</span></div>   
      <a href="#" class="filter sandwich_filter_view sandwich_gallery_filter">filters</a>

      <div class="clearfix"></div>
   
    </div>
  </div>
  <div class="container sandwich-all-gallery">
    <div class="sandwich-wrapper">
      <form action="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/gallery" id="sandwich-gallery-search" method="post">
        <input class="sandwiches_search" type="text" name="search"  value="<?php echo $this->_tpl_vars['serch_term']; ?>
">
        <button type="submit" class="sandwiches_search_btn">search</button>
      </form>
      <input type="hidden" id="searchTerm" value="<?php echo $this->_tpl_vars['serch_term']; ?>
">
  <div class="clearfix"></div>
    <div class="clearfix"></div>
       <ul class="menu-listing">
       <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['GALLARY_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
             
              <?php $this->assign('bread_name', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]); ?>
              <?php $this->assign('prot_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php $this->assign('bread_name', ((is_array($_tmp=$this->_tpl_vars['bread_name'])) ? $this->_run_mod_handler('replace', true, $_tmp, ' ', '##') : smarty_modifier_replace($_tmp, ' ', '##'))); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
      
      
              
                   
                   
                   <?php if (is_array($this->_tpl_vars['order_data']['user_sandwich']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'], $this->_tpl_vars['order_data']['user_sandwich']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
              
              <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['by_admin'] == 1): ?>
              <?php $this->assign('url', $this->_tpl_vars['ADMIN_URL']); ?>
              <?php else: ?>
              <?php $this->assign('url', $this->_tpl_vars['SITE_URL']); ?>
              <?php endif; ?>
              
              <?php $this->assign('sname', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']); ?>
              <div class="p" class="right-rgt" data-sandwich_desc="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
" data-sandwich_desc_id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_desc_id']; ?>
" data-flag="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['flag']; ?>
" data-menuadds="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_add_count']; ?>
" data-userid ="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
" <?php if ($this->_tpl_vars['items_id'] == 1): ?> rel="ADD TO CART" <?php else: ?> rel="ADD TO CART" <?php endif; ?> data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-toast="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_toast']; ?>
" data-formatdate="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['bread_name']; ?>
"  data-date="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-likeid="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['like_id']; ?>
" data-likecount="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['like_count']; ?>
" data-typeSandwich="FS">
             <li>
                           <img data-href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
/thumbnails/sandwich_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png" alt="sandwitchimageview" class="sandwich_gallery_view">
<input type="hidden" id="sandwichGalleryImg_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png">

                           <input type="hidden" name="chkmenuactice<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_is_active']; ?>
" />
           
           <h4 class="sandwich_gallery_view"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['sname'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "..", true) : smarty_modifier_truncate($_tmp, 20, "..", true)); ?>
</h4>
           <a href="#" class="add-cart" id="view-sandwich-popup">VIEW</a>
           <?php $_from = $this->_tpl_vars['saved_data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
              <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'] == $this->_tpl_vars['v']): ?>
                  <?php if ($this->_tpl_vars['is_public_array'][$this->_tpl_vars['v']] == 0): ?>
                  <input type="hidden" value="<?php echo $this->_tpl_vars['is_public_array'][$this->_tpl_vars['v']]; ?>
" id="isPublic<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
">
                  <?php endif; ?> 
              <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
          <?php if (((is_array($_tmp=$this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['saved_data']) : in_array($_tmp, $this->_tpl_vars['saved_data']))): ?>
                                <input type="hidden" name="saved_tgl<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="1">
                              <?php endif; ?>
         </li>
           <input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $this->_supers['session']['uid']; ?>
" ></div>   
          <?php endfor; endif; ?>  
       </ul>
    
    
     <div class="loadMoreGallery" style="text-align:center"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/star.png" alt="Loading" /> </div>
    
    
    
    
    
    </div>
  </div>
</section>
<div class="sandwich-popup" id="sandwich-popup-gallery" style="display:none;">
<div class="summary-details-add-menu">
<div class="banner"> <a href="#" class="close-button">&nbsp;</a> <img src="images/added-menu-sandwiches.png" alt="<?php echo $this->_tpl_vars['title_text']; ?>
">
</div>
    <div class="final-list-wrapper">
  <h2 class="sandwich_name">JON'S MOZZARELLA MASTERPIECE</h2>
  <p class="view-popup-scroll">Corned Beef (1.0), Lettuce (N) Red Onion (L), Pickles (L), Tomato (N), Lettuce (L) Red Onion (L), Pickles (L), Lettuce (L) Red Onion (L), Pickles (L)  </p>
  <ul>
  <li>Created by:  <span class="user_name">Jon C</span>.</li>
  <li>Date Created:  <span class="created_date">10/14/14</span></li>

  </ul>
  
  <div class="toastit-wrapper">
      <div class="checkbox-holder-final">
        <input type="checkbox" class="savetousertoast toast" value="check1" name="check" id="check1">
        <label for="check1"> <span>Toast it!</span> <!-- <span>Yum!</span> --></label>
      </div>
    </div>
  
  <div class="add-menu">

   <a class="share share-sandwich" href="#"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/share-img.png" alt=""></img><span class="Share_sandwich">Share</span></a>  
  <a class="add-to-menu save-menu" href="#">Save to my menu</a>
        <input type="hidden" name="hidden_sandwich_data" id="hidden_sandwich_data" value="user_sandwich">
        <input type="hidden" name="hidden_sandwich_id" id="hidden_sandwich_id" value="">
  </div>
   
  <div class="add-cart">
  <h2 class="amount">$10.00</h2>
  <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
      <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
    <a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
           <input id="resetQty" name="itemQuty" type="text" class="text-box"  value="01" readonly>
    <a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
  </h3>
  <a class="add-to-cart" href="#">Add to cart</a>
  </div>
  
    </div>  
  </div>
</div>

<!-- Late loading data -->
<input name="total_item_count" type="hidden" value="<?php echo $this->_tpl_vars['total_item_count']; ?>
" />
<input name="sort_type" type="hidden" value="<?php echo $this->_tpl_vars['sort_id']; ?>
" />

<div class="sandwich-popup" id="sandwich-popup-filter" style="display:none;">
  <div class="summary-details-login summary-details-discount"> <a href="#" class="close-button-new"></a>
    <div class="saved-creditcards-wrapper">
      <h2 class="filter-option">filter Options <a href="#" class="clear-all-filter">Clear Filters</a></h2>
 
      <ul class="from-holder">
    <?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i'] => $this->_tpl_vars['category']):
?>

     <?php $_from = $this->_tpl_vars['category']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['data']):
?>
       <?php if ($this->_tpl_vars['data']->category_identifier): ?>

     
        <li> <span class="text-box-holder">
      <div class="mobile-popup-filter">
        <p class="items"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']->category_name)) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</p>
             <div class="list-options scroll-bar filterToggle" id="fit-<?php echo $this->_tpl_vars['data']->category_name; ?>
" style="display:none;"> 
              <ul>
              <?php if ($this->_tpl_vars['data']->category_name != 'bread'): ?>
                <li>
                  <input id="No <?php echo $this->_tpl_vars['data']->category_name; ?>
" type="checkbox" class="<?php echo $this->_tpl_vars['data']->category_name; ?>
 isFiltered" name="<?php echo $this->_tpl_vars['data']->category_name; ?>
" value="No <?php echo $this->_tpl_vars['data']->category_name; ?>
">
                  <label for="No <?php echo $this->_tpl_vars['data']->category_name; ?>
">No <?php echo $this->_tpl_vars['data']->category_name; ?>
</label>
                </li>
            
                <?php endif; ?>
                <?php $_from = $this->_tpl_vars['categoryItems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j'] => $this->_tpl_vars['categoryItem']):
?>
                
                  <?php $_from = $this->_tpl_vars['categoryItem']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['j'] => $this->_tpl_vars['dataitems']):
?>
                     <?php if ($this->_tpl_vars['data']->id == $this->_tpl_vars['dataitems']->category_id): ?>
            <li>
                  <input id="<?php echo $this->_tpl_vars['dataitems']->item_name; ?>
" type="checkbox" class="<?php echo $this->_tpl_vars['data']->category_name; ?>
 isFiltered" name="<?php echo $this->_tpl_vars['data']->category_name; ?>
" value="<?php echo $this->_tpl_vars['dataitems']->item_name; ?>
">
                  <label for="<?php echo $this->_tpl_vars['dataitems']->item_name; ?>
"><?php echo $this->_tpl_vars['dataitems']->item_name; ?>
</label>
                </li>
                
             <?php endif; ?>
              
                  <?php endforeach; endif; unset($_from); ?>
                 
            <?php endforeach; endif; unset($_from); ?> 
       
              </ul>
           </div>
               <span class="filter">
                 <div class="FilterLable" id="<?php echo $this->_tpl_vars['data']->category_name; ?>
">Select</div>  </span>
           </div>
              </span> </li>
          
              <?php endif; ?> 
              
             <?php endforeach; endif; unset($_from); ?>
                   
            <?php endforeach; endif; unset($_from); ?>

          <span class="border"></span>
          <a href="#" class="apply-filter">APPLY FILTERS</a>
          <div class="cancel-wrapper"><a href="#" class="cancel-button">CANCEL</a></div>
      </ul>
    </div>
  </div>
</div>