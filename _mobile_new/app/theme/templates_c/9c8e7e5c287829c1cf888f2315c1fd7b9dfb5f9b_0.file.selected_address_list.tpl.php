<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:12:26
  from 'C:\wamp64\www\hashbury\_mobile_new\app\theme\templates\selected_address_list.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7e2ae1f3d0_23305531',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9c8e7e5c287829c1cf888f2315c1fd7b9dfb5f9b' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\theme\\templates\\selected_address_list.tpl',
      1 => 1587960782,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7e2ae1f3d0_23305531 (Smarty_Internal_Template $_smarty_tpl) {
?>	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addresses']->value, 'address', false, 'k');
$_smarty_tpl->tpl_vars['address']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['address']->value) {
$_smarty_tpl->tpl_vars['address']->do_else = false;
?>
	<div class="address">
	<address>
		<h2><?php echo $_smarty_tpl->tpl_vars['address']->value->address1;?>
</h2>
		<input type="hidden" class="zip_hidden" value="<?php echo $_smarty_tpl->tpl_vars['address']->value->zip;?>
"/>
		<div class="left">
		 <p><?php echo $_smarty_tpl->tpl_vars['address']->value->name;?>
<br/><?php echo $_smarty_tpl->tpl_vars['address']->value->company;?>
<br/>
		<?php echo $_smarty_tpl->tpl_vars['address']->value->address1;?>
<br/>		
	
		<?php if ($_smarty_tpl->tpl_vars['address']->value->street != '') {
echo $_smarty_tpl->tpl_vars['address']->value->street;?>
<br/><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['address']->value->cross_streets != '') {?>(<?php echo $_smarty_tpl->tpl_vars['address']->value->cross_streets;?>
) <?php }?> New York, NY <?php echo $_smarty_tpl->tpl_vars['address']->value->zip;?>
 <br/> <?php echo $_smarty_tpl->tpl_vars['address']->value->phone;?>
 <?php if ($_smarty_tpl->tpl_vars['address']->value->extn) {?>EXT: <?php echo $_smarty_tpl->tpl_vars['address']->value->extn;
}?>
		</p>
		</div>
		<div class="right address-right">
		<?php if ($_smarty_tpl->tpl_vars['address']->value->delivery_instructions != '') {?>
		<p>
			DELIVERY INST:<br/> 
			<?php echo $_smarty_tpl->tpl_vars['address']->value->delivery_instructions;?>

		</p>
		<?php }?>
		 <a href="#" data-address-id="<?php echo $_smarty_tpl->tpl_vars['address']->value->address_id;?>
" class="view-button select-address-button address-select">select</a>
		</div>
	</address>
	</div>
	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
