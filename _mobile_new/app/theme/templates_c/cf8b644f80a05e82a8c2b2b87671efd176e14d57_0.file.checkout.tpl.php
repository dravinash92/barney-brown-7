<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:12:25
  from 'C:\wamp64\www\hashbury\_mobile_new\app\theme\templates\checkout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7e295e64e9_94350138',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cf8b644f80a05e82a8c2b2b87671efd176e14d57' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\theme\\templates\\checkout.tpl',
      1 => 1591943649,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7e295e64e9_94350138 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\smarty\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),1=>array('file'=>'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<section class="sandwich create-sandwhich-menu-wrapper">
		<div class="container check-out-head-wrap">
        <div class="check-out-head profile">
		<h2>Almost done!</h2>
        <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/?pid=<?php echo rand(100000,999990);?>
" class="friend-button">MY MENU</a>
        </div>
        </div>
        <div class="clearfix"></div> 
        <div class="check-out-left">  
             <?php echo $_smarty_tpl->tpl_vars['cartItemList']->value;?>

        </div>      
		       
        <div class="selection-wrapper">    
			<div class="amount-done amount-done-checkout">
			
			    <div class="personal-detail">
			    <!-- style="display:none" for launch only -->
               <div style="display:none" class="details-1 first-time" id="select_delivery_or_pickup_div">
               <div class="total"><h3>TYPE</h3></div>
               <div class="tax" ><h2>CHOOSE DELIVERY OR PICK--UP</h2></div>
               <div class="total"><img class="total-img" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/right-arrow-2.png"></div>
               </div>
               <div class="details-1 first-time" id="select_address_div">
               <div class="total"><h3>ADDRESS</h3></div>
               <div class="tax" ><h2>CHOOSE A LOCATION</h2></div>
               <div class="total"><img class="total-img" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/right-arrow-2.png"></div>
               </div>
               <div class="details-1 first-time" id="enter_payment_div">
               <div class="total"><h3>PAYMENT</h3></div>
               <div class="tax" ><h2>CHOOSE PAYMENT METHOD</h2></div>
               <div class="total"><img class="total-img" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/right-arrow-2.png"></div>
               </div>
               <div class="details-1" id="enter_datetime_seelction">
               <div class="total"><h3>WHEN</h3></div>
               <div class="tax" ><h2 id='front_page_delivery_time'>NOW</h2></div>
               <div class="total"><img class="total-img" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/right-arrow-2.png"></div>
               </div>
               </div>
			                     <input type = "hidden" name="_card_number_auth" value="" />
                              <input type = "hidden" name="_expiry_month_auth" value="" />
                              <input type = "hidden" name="_expiry_year_auth" value="" />
			
				<div class="amount-done-wrapper">
					<h2>Total</h2>
					<h3>$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['grandtotal']->value);?>
</h3>
					<a class="done place-order-checkout" href="javascript:void(0)">PLACE ORDER</a>
				</div>
			</div>
		</div>
		<input type="hidden" id="order-type" name="order_type" value="" />
		<input type="hidden" id="address-id" name="address_id" value="" />
		<input type="hidden" id="card-id" name="card_id" value="" />
		
		
</section>

<!--- Select Address --->
<input type="hidden" id="zip_hidden_selected" value=""/>
<div class="sandwich-popup" id="address_list" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
		<div class="add-address-wrapper">
			<h2>Select Address</h2>
		   <a href="javascript:void(0)" id="add-address" class="add-address">Add new address</a>
		   
					  
		   <div class="addresslist">		
			   <div class="address">		
				   <address>
						<h2>205 E 10TH STREET</h2>
						<div class="left">
						<p>Matthew Baer Company<br/> 205 E 10th Street<br/> Apt. 4D <br/> (Cross Streets Here) New York, NY 10003 212-555-5555</p>
						</div>
						<div class="right">
						<p>DELIVERY INST:<br/>Ring buzzer 4308</p>
						 <a href="#" class="view-button select-address-button address-select">select</a>
						</div>
					</address>
				</div>   
			</div>
		</div>  
	</div>
</div>

<!--- Select Address --->
<div class="sandwich-popup" id="warningcheckout" style="display:none;">
  <div class="added-menu-sandwiches-inner">
  <a href="#" class="close-button">Close</a>
  <div class="added-good-news">
  <div class="added-good-news-msg">
    <div class="title-holder">
      <h1>Good News!</h1>
    </div>
    <div class="title2-holder">
    <p>Although Barney Brown does not currently offer regular lunch service in this area, same-day orders of $100.00 or more are accepted to this address!</p>
    </div>
    </div>
    </div>
    <div class="button-holder"> 
      <a href="#" id="warningOk" class="add-to-cart">OK</a>
    </div>
  </div>
</div>  
<!--- Select Delivery or Pickup --->


<div class="sandwich-popup" id="select_delivery_or_pickup" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
	<div class="add-address-wrapper">
	<h2>Select Delivery or Pickup</h2>
	<div class="address">
  <div class=""> 
	<a href="javascript:void(0);" class="view-button order-type" data-order-type="1">Delivery</a>
	<span class="or">or</span>
	<a href="javascript:void(0);" class="view-button order-type" data-order-type="0">Pickup</a>
  </div>
    </div>
   </div>
</div>
</div>

<!--- Add new Address --->

<div class="sandwich-popup" id="add_new_address" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
	<div class="add-new-address">
		<h2>Add New Address</h2>
	<div class="popup-scroll-wrapper">
	<form method="post" class="add_address">
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Recipient Name</p>
        <input name="recipient" type="text" class="text-box-control" autocorrect="off"  spellcheck="false" value="<?php echo $_SESSION['uname'];?>
 <?php echo $_SESSION['lname'];?>
">
        </span> <span class="text-box-holder">
        <p>Company (optional)</p>
        <input name="company" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Street Address</p>
        <input name="address1" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span> <span class="text-box-holder">
      <!--  <p>Street Address 2 (optional)</p>
        <input name="address2" type="text" class="text-box-control" >
        </span> <span class="text-box-holder1">-->
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" autocorrect="off"  spellcheck="false" >
        </span> </li>
		<li><span class="text-box-holder phone-number">
        <p>Phone Number</p>
        <input name="phone1" id="phone1" type="text" class="text-box-phone" autocorrect="off" spellcheck="false" maxlength="3">
        <input name="phone2" id="phone2" type="text" class="text-box-phone" autocorrect="off"  spellcheck="false" maxlength="3">
        <input name="phone3" id="phone3" type="text" class="text-box-phone margin-none" autocorrect="off"  spellcheck="false" maxlength="4" >
        </span> <span class="text-box-holder1 text-box-control-ext" >
        <p>Ext.</p>
        <input name="extn" type="text" id="phoneext" class="text-box-control">
        </span> </li>
      <li> <span class="text-box-holder">
        <h3>New York, NY</h3>
        <span class="text-box-zip-holder1">
        <p>Zip</p>
        <input name="zip" type="text" class="text-box-zip enter_zip" autocorrect="off"  spellcheck="false">
        </span> </span>
      </li>
	  <li> <span class="text-box-holder">
        <p>Delivery Instructions (optional)</p>
        <input name="delivery_instructions" type="text" class="text-box-control enter_delivery" autocorrect="off"  spellcheck="false">
        </span> </li>
		<a href="#" class="add-address save_address">Add address</a>
	
    </ul>
    </form>
    </div>
	</div>
  
</div>
</div>

<!-- Credit card List -->
<div class="sandwich-popup" id="credit_card_list" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
		<div class="add-address-wrapper">
			<h2>Select Payment Method</h2>
		   <a href="javascript:void(0)" id="add-address" class="add-address add-credit-card">Add Credit Card</a>		  
		   <div class="addresslist">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['billingInfo']->value, 'card', false, 'k');
$_smarty_tpl->tpl_vars['card']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['card']->value) {
$_smarty_tpl->tpl_vars['card']->do_else = false;
?>
						<div class="address">	
<?php $_smarty_tpl->_assignInScope('cardnos', str_split($_smarty_tpl->tpl_vars['card']->value->card_number,5));?>							
							<h2><?php if ($_smarty_tpl->tpl_vars['card']->value->card_type == "American Express") {?> AMEX <?php } else { ?> <?php echo mb_strtoupper($_smarty_tpl->tpl_vars['card']->value->card_type, 'UTF-8');?>
 <?php }?> ENDING WITH <?php echo $_smarty_tpl->tpl_vars['cardnos']->value[3];?>
</h2>
							<div class="left">
							<p>	<?php echo $_smarty_tpl->tpl_vars['card']->value->card_number;?>
<br/>		
								<?php if ($_smarty_tpl->tpl_vars['card']->value->card_type != '') {
echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['card']->value->card_type);?>
<br/><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['card']->value->card_zip != '') {
echo $_smarty_tpl->tpl_vars['address']->value->card_zip;?>
<br/><?php }?>							
							</p>
							</div>
							<div class="right">	
							<a rel="<?php echo $_smarty_tpl->tpl_vars['card']->value->id;?>
" class="remove-card">Remove</a>							
							 <a href="#" data-card-id="<?php echo $_smarty_tpl->tpl_vars['card']->value->id;?>
" class="view-button select-card-button">select</a>
							</div>						
						</div>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>				
			</div>
		</div>  
	</div>
</div>	

<!-- Add new credit card -->

<div class="sandwich-popup" id="new_credit_card" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
 <div class="saved-creditcards-wrapper">
 <h2>Add New Credit Card</h2>
             <ul class="from-holder">
      <li> 
	  <span class="text-box-holder">
        <p>Credit Card</p>
        <span class="credit-card">
        <select name="cardType" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>Visa</option>
          <option>MasterCard</option>
          <option>American Express</option>
          <option>Discover</option>          
        </select>
        </span> </span></li>
		<li><span class="text-box-holder">
        <p>Card Number</p>
        <input name="cardNo" type="text" class="text-box-control" placeholder="">
        </span></li> 
		<li><span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="cardCvv" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Expiration Date</p>
        <span class="month">
        <select name="cardMonth" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="03">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <!--<select name="cardYear" id="jumpMenu" >
			<?php
$_smarty_tpl->tpl_vars['__smarty_section_foo'] = new Smarty_Variable(array());
if (true) {
for ($__section_foo_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] = 2015; $__section_foo_2_iteration <= 36; $__section_foo_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']++){
?>
			  <option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
</option>
			<?php
}
}
?>
        </select>-->
	<select name="cardYear"  id="jumpMenu" >
				<?php $_smarty_tpl->_assignInScope('currentyear', smarty_modifier_date_format(time(),"%Y"));?>
				<?php $_smarty_tpl->_assignInScope('numyears', 50);?>
				<?php $_smarty_tpl->_assignInScope('totalyears', $_smarty_tpl->tpl_vars['currentyear']->value+$_smarty_tpl->tpl_vars['numyears']->value);?>
				<?php
$__section_loopyers_3_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['totalyears']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_loopyers_3_start = (int)@$_smarty_tpl->tpl_vars['currentyear']->value < 0 ? max(0, (int)@$_smarty_tpl->tpl_vars['currentyear']->value + $__section_loopyers_3_loop) : min((int)@$_smarty_tpl->tpl_vars['currentyear']->value, $__section_loopyers_3_loop);
$__section_loopyers_3_total = min(($__section_loopyers_3_loop - $__section_loopyers_3_start), $__section_loopyers_3_loop);
$_smarty_tpl->tpl_vars['__smarty_section_loopyers'] = new Smarty_Variable(array());
if ($__section_loopyers_3_total !== 0) {
for ($__section_loopyers_3_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index'] = $__section_loopyers_3_start; $__section_loopyers_3_iteration <= $__section_loopyers_3_total; $__section_loopyers_3_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index']++){
?>
				<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index'] : null);?>
"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index'] : null);?>
</option>
				<?php
}
}
?>
         </select>
        </span></li> <li></span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="cardZip" type="text" class="text-box-billing-zip" >
        </span> </li>
	  <!-- <li> <span class="text-box-holder">
        <p>Street Address</p>
        <input name="address1" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span>  <span class="text-box-holder1">
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span> </li> -->
       <!-- <li> <span class="text-box-holder">
       <p>Street Address 2 (optional)</p>
        <input name="address2" type="text" class="text-box-control" >
        </span><span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" autocorrect="off"  spellcheck="false" >
        </span> </li>-->
      <li> <span class="toastit-wrapper">
        <input id="check2" type="checkbox" checked="checked"  name="save_billing" value="check2">
        <label for="check2">Save Billing Info <!--<span>(Verisign Encryption)</span> --></label>
        </span> </li>
       <a href="#" class="add-address save-credit-card">Add card</a>
    </ul>
 </div>
  
</div>
</div>

<!---Select address --------->


<!---------------Select Date Popup------------------->
<div class="sandwich-popup" id="select_date_time" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
		<div class="add-address-wrapper">
			<h2>Select Date / Time</h2>		   
		   <div class="addresslist">		
				<div class="address">							
					<input type="radio" class="css-checkbox switch_date" id="radio1" <?php if ($_smarty_tpl->tpl_vars['currenthour']->value < 16) {?> checked="checked" <?php }?> name="radiog_lite" value="now">
					<label class="css-label" for="radio1">Now</label>
          
          <p class="or">or</p>
					
					<input type="hidden" name="todays_date" value="<?php  echo date(l); ?>" />
					<input type="hidden" name="todays_time" value="<?php  echo date('H:00'); ?>" />					   
					<input type="hidden" name="delivery_store_id" value="" />
					<input type="radio" class="css-checkbox switch_date" id="radio2" <?php if ($_smarty_tpl->tpl_vars['currenthour']->value > 16) {?>checked="checked" <?php }?> name="radiog_lite" value="specific">
					<label class="css-label" for="radio2">Specific Date/Time</label>
					<div id="cal-holder" class="date_time_wrapper filter date_wrapper inputbox-disable-overlay-position" style="display:none;">
						 <div class="inputbox-disable-overlay calendar-input-disable">&nbsp;</div>
						<input name="" readonly="readonly" type="text" class="datepicker date delivery_date" placeholder="Select Date"/>				
					</div>
					<div class="date_time_wrapper filter" style="display:none; background:none;">
						<div class="timechange">  <?php echo $_smarty_tpl->tpl_vars['times']->value;?>
</div>
					</div>
					
					<input type="hidden" name="hidden_store_id" id="hidden_store_id" value=""/>
                    <input type="hidden" name="selected_day" id="selected_day" value=""/>
                    <input type="hidden" name="selected_date" id="selected_date" value="0"/>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addresses']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
							<?php if ($_smarty_tpl->tpl_vars['k']->value == 0) {?>
							<input value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" type="hidden" name=="hidden_default_store_id" id="hidden_default_store_id" />
							<?php }?>
				        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				        
				       
				</div>   
         <div class="right">						
						<a href="#" class="view-button apply-time-selection done">select</a>
				 </div>
			</div>
		</div>  
	</div>
</div>


<!--Apply discount Popup--->
<div class="sandwich-popup" id="apply_discount_popup" style="display:none;">
	<div class="summary-details-discount">
		<a href="#" class="close-button"></a>
		<div class="apply-discount">
			<div class="discount-wrapper">
				<h2>APPLY DISCOUNT CODE</h2>
				<input class="text-lastname" name="discount_code" id="discount_code" type="text" placeholder="Enter Discount Code">
				<a href="#" class="add-address check-discount-code">apply discount</a>
			</div>
		</div>
	</div>
</div>
<input name="recipientName" type="hidden" value="<?php echo $_SESSION['uname'];?>
 <?php echo $_SESSION['lname'];?>
">

<style type="text/javascript">
.delivery_date{color:#dab37f;}
.date_time_wrapper .ui-datepicker-trigger{visibility:hidden;}
</style>
<?php echo '<script'; ?>
>
var inp1 = document.getElementById('phone1');
var inp2 = document.getElementById('phone2');
var inp3 = document.getElementById('phone3');
var count=0;
inp1.onkeyup = function() {
  
    if(inp1.value.length==3)
    {
        //$('#phone2').focus();
        
    }
   
}
inp2.onkeyup = function() {
  
    if(inp2.value.length==3)
    {
        //$('#phone3').focus();
    }
   
}
inp3.onkeyup = function() {
  
    if(inp3.value.length==4)
    {
        //$('#phoneext').focus();
    }
   
}


<?php echo '</script'; ?>
>

<?php }
}
