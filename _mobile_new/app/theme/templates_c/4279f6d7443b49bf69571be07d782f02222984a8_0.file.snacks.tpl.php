<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:12:14
  from 'C:\wamp64\www\hashbury\_mobile_new\app\theme\templates\snacks.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7e1e377cc5_81590418',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4279f6d7443b49bf69571be07d782f02222984a8' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\theme\\templates\\snacks.tpl',
      1 => 1588054887,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7e1e377cc5_81590418 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="sandwich new_sandwich_mobile">
            <div class="container">

                <div id="mobile_drinks" class="sandwich-wrapper sandwich-new-wrapp">

    

                    <h3>Snacks &amp; Things</h3>

                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['salads']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
                    <div class="save-sandwich--wrap" data-id="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" data-product_name="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['v']->value['description'];?>
" data-product_image="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_image'];?>
" data-product_price="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_price'];?>
" data-standard_category_id="<?php echo $_smarty_tpl->tpl_vars['v']->value['standard_category_id'];?>
" data-image_path="<?php echo $_smarty_tpl->tpl_vars['salad_image_path']->value;?>
" data-uid=<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
 data-product=<?php echo $_smarty_tpl->tpl_vars['product']->value;?>
 data-spcl_instr="<?php echo $_smarty_tpl->tpl_vars['v']->value['allow_spcl_instruction'];?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['v']->value['add_modifier'];?>
" data-modifier_desc="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_desc'];?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['v']->value['add_modifier'];?>
" data-modifier_isoptional="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_isoptional'];?>
" data-modifier_is_single="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_is_single'];?>
" data-modifier_options='<?php echo json_encode($_smarty_tpl->tpl_vars['modifier_options']->value[$_smarty_tpl->tpl_vars['k']->value]);?>
'>
                        <div class="save-sand--img">
                            <span><img title="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
" class="view_sandwich" data-href="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['salad_image_path']->value;
echo $_smarty_tpl->tpl_vars['v']->value['product_image'];?>
"></span>
                        </div>
                        <div class="save-sand--content">
                            <span class="saved-sand--price"><?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
</span>
                             <div class="new-drink_flex">
                            <h2>$<?php echo $_smarty_tpl->tpl_vars['v']->value['product_price'];?>
</h2>
                            <div class="add-new no-items add_btn_item">
                            <div class="add-new-sub">
                                <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                                    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup leftSpinner"></a>
                                    <input id="resetQty" name="itemQuty" type="text" class="text-box" value="01"
                                        readonly="">
                                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup rightSpinner"></a>
                                </h3>
                            </div>
                            <div class="saved-add-cartwrap">
                                <a href="#" class="saved-add-cart <?php if ($_smarty_tpl->tpl_vars['v']->value['allow_spcl_instruction'] == 1 || $_smarty_tpl->tpl_vars['v']->value['add_modifier'] == 1) {?> salad-listing <?php } else { ?> common_add_item_cart <?php }?>" data-sandwich="<?php echo $_smarty_tpl->tpl_vars['product']->value;?>
" data-sandwich_id="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/cart.png"><span>Add</span></a>
                            </div>

                        </div>
                            </div>
                            
                        </div>
                     
                    </div>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>

        </section><?php }
}
