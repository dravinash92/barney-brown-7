<?php /* Smarty version 2.6.25, created on 2020-05-12 03:25:42
         compiled from create-sandwich.tpl */ ?>
<section class="create-sandwhich-menu-wrapper">
	 <div class="create-sandwhich-menu">
	 
	 <span class="create-screen">
  	 <h1 class="steps">Step 1 of 5</h1>
     <ul class="sandwhich-menu">
   		<li> <a class="start-over" >start Over</a> </li>
    	<li class="standard-item-head create-li-heading"><a href="#">Choose Bread</a></li>
        <li class="float-right"> <a class="im-done doneSandwich" >I'm Done</a> </li>
     </ul>
     </span>
     
     <span class="finalizeScreen" >
      <div class="finalize-wrapper">
      <h2 class="finalize">Finalize It!</h2>
      <a class="edit-button" href="choose-bread.html">EDIT</a>
      <a class="summary" href="#">Summary</a>
      </div>
      </span>
</div>
     
     
  
    <div class="sandwhich-banner" style="min-height:175px;">
 
  	
  	
  	        <?php if (! $this->_supers['session']['temp_sandwich_data']): ?> 
            <?php echo $this->_tpl_vars['LANDING_PAGE']; ?>

            <?php endif; ?>
            
            <div class="bread_images">
            
            <span class="bread_image"> 
              <div class="image-holder">
             	<img style="display:none" src="" alt="">
              </div>
            </span>
            
            <span class="protein_image">
            </span>
            
            <span class="cheese_image">
            </span>
            
            <span class="topping_image">
            </span>

            <span class="condiments_image">
            </span>
            
            </div>
  	
    </div>
    
    

    <span class="finalizeScreen">
    
    <div class="sandwhich-banner">
    <div class="name-your-creation-popup">
    <div class="name-your-creation">
    <h2>NAME YOUR CREATION</h2> 
    <input  type="text" id="namecreation" name="namecreation" value="" readonly="readonly" >
    </div>
    </div>
    </div>
    
      <div class="selection-wrapper">
       <div class="toastit-wrapper">
      <div class="checkbox-holder-final">
        <input type="checkbox" value="check1" class="menucheck" name="toastit_" id="toastit_">
        <label for="toastit_"> Toast It! <!-- <span>Yum</span> --></label>
        </div>
         <div class="checkbox-holder-final">
          <input type="checkbox" value="check2" name="check" id="check2">
          <label for="check2" class="saved-item-btns">Make Private<span class="locked-item"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/locked-itm.png"></span></label>
        </div>
        <div class="save-button-share">
  
      <a href="#" class="save-to-my-menu">Save</a>
 
      </div>
    </div>
     
    </div>
 
</span>



    
   
    <div class="selection-wrapper">
    
     <span class="create-screen">
  	<div class="ajaxContent" id="optionList"> 
  	<?php echo $this->_tpl_vars['OPTN_DATA']; ?>

    </div>
    </span>
    

    
    
    
     <div class="amount-done">
       <div class="amount-done-wrapper">
        <h2>Total</h2>
        <h3 class="price">$<?php echo $this->_tpl_vars['BASE_FARE']; ?>
.00</h3>
		<div class="create-navigation">
          <span> <a class="back-button" style="visibility:hidden;" >Back</a> </span> 
          <span> <a class="next-button" >Next</a> </span> 
		</div>	
        <span class="finalizeScreen"> <a href="#" class="addtoCart">ADD TO CART</a> </span>
       </div>
      </div>
     </div>
</section>