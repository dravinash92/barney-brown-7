<?php /* Smarty version 2.6.25, created on 2020-06-04 07:22:12
         compiled from selected_address_list.tpl */ ?>
	<?php $_from = $this->_tpl_vars['addresses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['address']):
?>
	<div class="address">
	<address>
		<h2><?php echo $this->_tpl_vars['address']->address1; ?>
</h2>
		<input type="hidden" class="zip_hidden" value="<?php echo $this->_tpl_vars['address']->zip; ?>
"/>
		<div class="left">
		 <p><?php echo $this->_tpl_vars['address']->name; ?>
<br/><?php echo $this->_tpl_vars['address']->company; ?>
<br/>
		<?php echo $this->_tpl_vars['address']->address1; ?>
<br/>		
	
		<?php if ($this->_tpl_vars['address']->street != ""): ?><?php echo $this->_tpl_vars['address']->street; ?>
<br/><?php endif; ?>
		<?php if ($this->_tpl_vars['address']->cross_streets != ""): ?>(<?php echo $this->_tpl_vars['address']->cross_streets; ?>
) <?php endif; ?> New York, NY <?php echo $this->_tpl_vars['address']->zip; ?>
 <br/> <?php echo $this->_tpl_vars['address']->phone; ?>
 <?php if ($this->_tpl_vars['address']->extn): ?>EXT: <?php echo $this->_tpl_vars['address']->extn; ?>
<?php endif; ?>
		</p>
		</div>
		<div class="right address-right">
		<?php if ($this->_tpl_vars['address']->delivery_instructions != ""): ?>
		<p>
			DELIVERY INST:<br/> 
			<?php echo $this->_tpl_vars['address']->delivery_instructions; ?>

		</p>
		<?php endif; ?>
		 <a href="#" data-address-id="<?php echo $this->_tpl_vars['address']->address_id; ?>
" class="view-button select-address-button address-select">select</a>
		</div>
	</address>
	</div>
	<?php endforeach; endif; unset($_from); ?>