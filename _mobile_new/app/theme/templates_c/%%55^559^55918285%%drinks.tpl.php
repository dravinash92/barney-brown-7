<?php /* Smarty version 2.6.25, created on 2020-04-28 02:25:22
         compiled from drinks.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'json_encode', 'drinks.tpl', 7, false),)), $this); ?>
<section class="sandwich new_sandwich_mobile">
            <div class="container">

                <div id="mobile_drinks" class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Drinks</h3>
                    <?php $_from = $this->_tpl_vars['salads']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
                    <div class="save-sandwich--wrap" data-id="<?php echo $this->_tpl_vars['v']['id']; ?>
" data-product_name="<?php echo $this->_tpl_vars['v']['product_name']; ?>
" data-description="<?php echo $this->_tpl_vars['v']['description']; ?>
" data-product_image="<?php echo $this->_tpl_vars['v']['product_image']; ?>
" data-product_price="<?php echo $this->_tpl_vars['v']['product_price']; ?>
" data-standard_category_id="<?php echo $this->_tpl_vars['v']['standard_category_id']; ?>
" data-image_path="<?php echo $this->_tpl_vars['salad_image_path']; ?>
" data-uid=<?php echo $this->_tpl_vars['uid']; ?>
 data-product=<?php echo $this->_tpl_vars['product']; ?>
 data-spcl_instr="<?php echo $this->_tpl_vars['v']['allow_spcl_instruction']; ?>
" data-add_modifier="<?php echo $this->_tpl_vars['v']['add_modifier']; ?>
" data-modifier_desc="<?php echo $this->_tpl_vars['v']['modifier_desc']; ?>
" data-add_modifier="<?php echo $this->_tpl_vars['v']['add_modifier']; ?>
" data-modifier_isoptional="<?php echo $this->_tpl_vars['v']['modifier_isoptional']; ?>
" data-modifier_is_single="<?php echo $this->_tpl_vars['v']['modifier_is_single']; ?>
" data-modifier_options='<?php echo json_encode($this->_tpl_vars['modifier_options'][$this->_tpl_vars['k']]); ?>
'>
                        <div class="save-sand--img">
                            <span><img title="<?php echo $this->_tpl_vars['v']['product_name']; ?>
" class="view_sandwich" data-href="<?php echo $this->_tpl_vars['v']['id']; ?>
" src="<?php echo $this->_tpl_vars['salad_image_path']; ?>
<?php echo $this->_tpl_vars['v']['product_image']; ?>
"></span>
                        </div>
                        <div class="save-sand--content">
                            <span class="saved-sand--price"><?php echo $this->_tpl_vars['v']['product_name']; ?>
</span>
                            <div class="new-drink_flex">
                                <h2>$<?php echo $this->_tpl_vars['v']['product_price']; ?>
</h2>
                                <div class="add-new no-items add_btn_item">
                            <div class="add-new-sub">
                                <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                                    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup leftSpinner"></a>
                                    <input id="resetQty" name="itemQuty" type="text" class="text-box" value="01"
                                        readonly="">
                                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup rightSpinner"></a>
                                </h3>
                            </div>
                            <div class="saved-add-cartwrap">
                                <a href="#" class="saved-add-cart <?php if ($this->_tpl_vars['v']['allow_spcl_instruction'] == 1 || $this->_tpl_vars['v']['add_modifier'] == 1): ?> salad-listing <?php else: ?> common_add_item_cart <?php endif; ?>" data-sandwich="<?php echo $this->_tpl_vars['product']; ?>
" data-sandwich_id="<?php echo $this->_tpl_vars['v']['id']; ?>
" data-uid="<?php echo $this->_tpl_vars['uid']; ?>
"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/cart.png"><span>Add</span></a>
                            </div>

                        </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <?php endforeach; endif; unset($_from); ?>
                </div>

        </section>