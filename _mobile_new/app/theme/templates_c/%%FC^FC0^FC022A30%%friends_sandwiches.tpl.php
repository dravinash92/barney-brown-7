<?php /* Smarty version 2.6.25, created on 2020-08-07 02:48:03
         compiled from friends_sandwiches.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'rand', 'friends_sandwiches.tpl', 1, false),)), $this); ?>
 <?php $this->assign('random_number', ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990))); ?>
 <input type="hidden" name="countMenuItems" value="<?php echo $this->_tpl_vars['numMenu']; ?>
"/>
 <div id="frnDpopmenuz" class="sandwich-popup" style="display:none">
   <div class="summary-details-friends-menu">
     <a class="close-button" href="#"></a>
     <h2>Friends' Menus</h2>
     <section class="sandwich-menu" style="position:relative">

       <ul class="frndpopcont">
        <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/friends_menu.gif" style="margin: 0px auto; display:block; margin-top: 100px;" /> 
      </ul>
    </section>

  </div>  
</div>

<section class="sandwich new_sandwich_mobile">
  <div class="container">

    <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Friends’ Sandwiches<span> <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/sandwichMenu/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="back_new"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/back_new.png">
                                <b>Back</b></a></span><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/sandwichMenu/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="back_new">
                        </a></h3>
                <?php if ($this->_tpl_vars['fbCheck'] == 1): ?>
                  <?php if (! empty ( $this->_tpl_vars['data'] )): ?>
                    <?php $_from = $this->_tpl_vars['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                    <div class="friends-sandwich--block friends-sandwiches-all">
                        <div class="friends-sandwich ">
                            <div class="friends-dp"><img src="https://graph.facebook.com/<?php echo $this->_tpl_vars['item']->friend_uid; ?>
/picture?width=107&height=107"></div>
                            <div class="friend-name friends-all">
                                <h2><?php echo $this->_tpl_vars['item']->first_name; ?>
 <?php echo $this->_tpl_vars['item']->last_name; ?>
</h2>
                                <p><?php echo $this->_tpl_vars['item']->sandwich_count; ?>
 Sandwiches</p>
                            </div>
                            <div class="friends-view-wrap">
                                <div class="friends-view--btn"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/friendsMenu/<?php echo $this->_tpl_vars['item']->uid; ?>
" class="freinds-view">view</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; endif; unset($_from); ?>
                  <?php else: ?>
                    <div class="friends-share">
                      <p>LET YOUR FRIENDS KNOW ABOUT BARNEY BROWN!</p>
                      <a href="javascript:void(0);" id="Share-BarneyBrown" class="viewall-saved-sandwiches"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/view-all--border.png"><span>SHARE</span></a>
                    </div>
                  <?php endif; ?>
                <?php else: ?>
                  <div class="friends-share">
                      <p>PLEASE CONNECT TO FACEBOOK TO SEE FRIENDS' SANDWICHES</p>
                      <a target="_blank" href="<?php  echo _URL;  ?>" class="viewall-saved-sandwiches"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/view-all--border.png"><span>CONNECT</span></a>
                    </div>
                <?php endif; ?>
                </div>
</div>

</section>