<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:12:25
  from 'C:\wamp64\www\hashbury\_mobile_new\app\theme\templates\cart-list-items.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7e291de9c8_75860578',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6338968c7d176604da29397bdbd40e21f6b2f9c2' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\theme\\templates\\cart-list-items.tpl',
      1 => 1605186247,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7e291de9c8_75860578 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\smarty\\libs\\plugins\\function.math.php','function'=>'smarty_function_math',),1=>array('file'=>'C:\\wamp64\\www\\hashbury\\_mobile_new\\app\\smarty\\libs\\plugins\\modifier.truncate.php','function'=>'smarty_modifier_truncate',),));
?>

<?php if ($_smarty_tpl->tpl_vars['itemsCount']->value > 0) {?>  
          <table class="cart_list_header">
					<thead>
						<tr>
							<th class="item">Items</th>
							<th class="qty">Qty</th>
							<th class="price">price</th>
							<th class="remove_header">&nbsp;</th>
						</tr>
					</thead>
					<tbody></tbody>
			</table>
                        <div class="hundred-min-red-bar first-time"><?php if ($_smarty_tpl->tpl_vars['totalNumber']->value < 10 && $_smarty_tpl->tpl_vars['pickupOrDelivery']->value != 0) {?> <p>
					<?php echo smarty_function_math(array('assign'=>"reaminingNumber",'equation'=>10.00-$_smarty_tpl->tpl_vars['totalNumber']->value),$_smarty_tpl);?>

						$10.00 SUBTOTAL MINIMUM. $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['reaminingNumber']->value);?>
 TO GO!
                            </p><?php }?></div>
			<div class="cart_item_list_wrapper">
			<div class="cart_item_list_scroller common-scrollbar">	
			<table class="cart_list_items">		
					<tbody>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwiches']->value, 'sandwich', false, 'myId');
$_smarty_tpl->tpl_vars['sandwich']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['myId']->value => $_smarty_tpl->tpl_vars['sandwich']->value) {
$_smarty_tpl->tpl_vars['sandwich']->do_else = false;
?>
						<tr class="cart_item_list">
							<td data-sandwich-ingredients='<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['sandwich_data'];?>
' data-toast = "<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['toast'];?>
" class="item cart-item-name"><?php echo $_smarty_tpl->tpl_vars['sandwich']->value['sandwich_name'];?>
</td>
							<td class="qty inputbox-disable-overlay-position">
							<div class="inputbox-disable-overlay checkout-input-disable">&nbsp;</div> 
							   <a href="javascript:void(0);" class="left_spinner_checkout"><img class="qty-img-left" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/left-arrow-hover.png"></a>							   
							   
                                                           <input name="itemQuty" type="hidden" class="qtybox"  value="<?php if ($_smarty_tpl->tpl_vars['sandwich']->value['item_qty'] < 10) {?>0<?php }
echo $_smarty_tpl->tpl_vars['sandwich']->value['item_qty'];?>
">
                                                           <span class="qtybox"><?php if ($_smarty_tpl->tpl_vars['sandwich']->value['item_qty'] < 10) {?>0<?php }
echo $_smarty_tpl->tpl_vars['sandwich']->value['item_qty'];?>
</span>
                                                           <a href="javascript:void(0);" class="right_spinner_checkout"><img class="qty-img-right" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/right-arrow-hover.png"></a>
							   <input type="hidden" class="sandwich_id" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['id'];?>
" />
							  <input type="hidden" class="order_item_id" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['order_item_id'];?>
" />
							  <input type="hidden" class="data_type" name="data_type" value="user_sandwich"/>
							  
							</td>
							<td class="price">$<?php echo number_format($_smarty_tpl->tpl_vars['sandwich']->value['item_total'],2);?>
</td>
							<td class="remove"><a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['order_item_id'];?>
" data-item="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['item_id'];?>
">&nbsp;</a> </td>
							  <input type="hidden" class="bread_type" name="bread_type" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['bread_type'];?>
"/> 
						</tr>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product', false, 'myId');
$_smarty_tpl->tpl_vars['product']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['myId']->value => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->do_else = false;
?>
					   <tr>
							<td class="item"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value->product_name,19,"..",true);?>

							<?php if ($_smarty_tpl->tpl_vars['product']->value->extra_id != '') {?> <p style="font-size: 10px;">Modifiers : <strong><?php echo $_smarty_tpl->tpl_vars['product']->value->extra_id;?>
</strong></p> <?php }?>                 
        					<?php if ($_smarty_tpl->tpl_vars['product']->value->spcl_instructions != '') {?> <p style="font-size: 10px;">Special Instructions : <strong><?php echo $_smarty_tpl->tpl_vars['product']->value->spcl_instructions;?>
</strong></p> <?php }?></td>
							<td class="qty">
							<div class="inputbox-disable-overlay checkout-input-disable">&nbsp;</div> 
								 <a href="javascript:void(0);" class="left_spinner_checkout"><img class="qty-img-left" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/left-arrow-hover.png"></a>							   
							   <!--<input name="itemQuty" type="text" class="qtybox"  value="<?php if ($_smarty_tpl->tpl_vars['product']->value->item_qty < 10) {?>0<?php }
echo $_smarty_tpl->tpl_vars['product']->value->item_qty;?>
">-->
							   <input name="itemQuty" type="hidden" class="qtybox"  value="<?php if ($_smarty_tpl->tpl_vars['product']->value->item_qty < 10) {?>0<?php }
echo $_smarty_tpl->tpl_vars['product']->value->item_qty;?>
">
                                                           <span class="qtybox"><?php if ($_smarty_tpl->tpl_vars['product']->value->item_qty < 10) {?>0<?php }
echo $_smarty_tpl->tpl_vars['product']->value->item_qty;?>
</span>
                                                           
                                                           <a href="javascript:void(0);" class="right_spinner_checkout"><img class="qty-img-right" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/right-arrow-hover.png"></a>
								<input type="hidden" class="sandwich_id" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
" />
								<input type="hidden" name="product_parent_type" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->product_parent_type;?>
" />
								<input type="hidden" class="order_item_id" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->order_item_id;?>
" />
								<input type="hidden" class="data_type" name="data_type" value="product">
							</td>
							<td class="price">$<?php echo number_format($_smarty_tpl->tpl_vars['product']->value->item_total,2);?>
</td>							
							<td class="remove"><a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="<?php echo $_smarty_tpl->tpl_vars['product']->value->order_item_id;?>
" data-item="<?php echo $_smarty_tpl->tpl_vars['product']->value->item_id;?>
">&nbsp;</a> </td> 
						</tr>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					
					
  					</tbody>
			</table>
			</div>
			</div>
             
			
<?php } else { ?>
			<ul>
				<li>
				<h3>There are currently no items in your shopping cart.</h3>  
				</li>
			</ul>				
<?php }?>



			<?php if ($_smarty_tpl->tpl_vars['itemsCount']->value > 0) {?> 
			 <div class="clearfix sales-info">
			<div class="sales-tax"><p style="visibility:hidden;">Sales tax inclusive </p></div>
			 <div class="check-out">
                <div class="tax-wrapper">
                 <div class="apply-discount-code"><a href="#">Apply discount code</a></div>
				</div>
                <div class="tax-total-wrapper">
					<div class="total"><p>SUBTOTAL</p><p>TAX</p><p>TIP</p><p style="display:none" class="p-discount-title">DISCOUNT</p></div>
					<div class="total"><p class="p-subtotal">$<?php echo number_format($_smarty_tpl->tpl_vars['total']->value,2);?>
</p><p class="p-tax">$<?php echo $_smarty_tpl->tpl_vars['tax']->value;?>
</p><p class="p-tips">$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['tips']->value);?>
</p><p style="display:none" class="p-discount">$0.00</p></div>
				</div>	
               </div>
                <div class="clearfix"></div><div class="clearfix"></div>
                 <div class="check-out">
                <div class="tax-2"><!-- visibilityHide -->
                    <p class="tip">TIP</p>
                		<div id="dd" class="wrapper-dropdown-tip" tabindex="1">	
                		<span class="tip-wrapper">					
						<select id="select_tip_amount">
							<?php if ($_smarty_tpl->tpl_vars['pickupOrDelivery']->value == 0) {?>
								<option value="0.00">$0.00</option>
								<?php
$__section_foo_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['MAX_TIP']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_foo_0_start = min(1, $__section_foo_0_loop);
$__section_foo_0_total = min(($__section_foo_0_loop - $__section_foo_0_start), $__section_foo_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_foo'] = new Smarty_Variable(array());
if ($__section_foo_0_total !== 0) {
for ($__section_foo_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] = $__section_foo_0_start; $__section_foo_0_iteration <= $__section_foo_0_total; $__section_foo_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']++){
?>
									<option <?php if ($_smarty_tpl->tpl_vars['tips']->value == (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) || (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) < $_smarty_tpl->tpl_vars['tips']->value) {?> selected <?php }?> value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.00</option>
									<?php $_smarty_tpl->_assignInScope('tipVal', ((isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null)).('.50'));?>
									<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) < 10) {?>  
									<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.50">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.50</option>
									<?php }?>
								<?php
}
}
?> 
								<option <?php if ($_smarty_tpl->tpl_vars['tips']->value == $_smarty_tpl->tpl_vars['MAX_TIP']->value) {?> selected <?php }?> value="<?php echo $_smarty_tpl->tpl_vars['MAX_TIP']->value;?>
.00">$<?php echo $_smarty_tpl->tpl_vars['MAX_TIP']->value;?>
.00</option>
							<?php } else { ?>

								<?php
$__section_foo_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['MAX_TIP']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_foo_1_start = (int)@$_smarty_tpl->tpl_vars['MIN_TIP']->value < 0 ? max(0, (int)@$_smarty_tpl->tpl_vars['MIN_TIP']->value + $__section_foo_1_loop) : min((int)@$_smarty_tpl->tpl_vars['MIN_TIP']->value, $__section_foo_1_loop);
$__section_foo_1_total = min(($__section_foo_1_loop - $__section_foo_1_start), $__section_foo_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_foo'] = new Smarty_Variable(array());
if ($__section_foo_1_total !== 0) {
for ($__section_foo_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] = $__section_foo_1_start; $__section_foo_1_iteration <= $__section_foo_1_total; $__section_foo_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']++){
?>
									<option <?php if ($_smarty_tpl->tpl_vars['tips']->value == (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) || (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) < $_smarty_tpl->tpl_vars['tips']->value) {?> selected <?php }?> value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.00</option>
									<?php $_smarty_tpl->_assignInScope('tipVal', ((isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null)).('.50'));?>
									<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) < 10) {?>  
									<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.25">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.25</option>
									<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.50">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.50</option>
									<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.75">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.75</option>
									<?php }?>
								<?php
}
}
?>
								<option <?php if ($_smarty_tpl->tpl_vars['tips']->value == $_smarty_tpl->tpl_vars['MAX_TIP']->value) {?> selected <?php }?> value="<?php echo $_smarty_tpl->tpl_vars['MAX_TIP']->value;?>
.00">$<?php echo $_smarty_tpl->tpl_vars['MAX_TIP']->value;?>
.00</option>
							<?php }?>	
						</select>
						</span>
						
					</div>
                </div>
                <div class="grand-total"><h3><span>Total</span></h3></div>
                <div class="grand-total"><h2>$<?php echo number_format($_smarty_tpl->tpl_vars['grandtotal']->value,2);?>
</h2></div>
				<?php if ($_smarty_tpl->tpl_vars['total']->value >= 0) {?> <p class="est-delivery-time">EST. DELIVERY TIME:
	                <?php if ($_smarty_tpl->tpl_vars['total']->value >= 0 && $_smarty_tpl->tpl_vars['total']->value < 100) {?>30-60 mins
		                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 100 && $_smarty_tpl->tpl_vars['total']->value < 200) {?>45-75 mins
		                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 200 && $_smarty_tpl->tpl_vars['total']->value < 300) {?>60-90 mins
		                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 300 && $_smarty_tpl->tpl_vars['total']->value < 400) {?>1-2 hrs
		                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 400 && $_smarty_tpl->tpl_vars['total']->value < 500) {?>2-3 hrs
		                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 500) {?>24 hours                
	                <?php }?></p>
	            <?php }?>
               </div>
               </div>
               <?php }?>


<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['order_string']->value;?>
" name="odstring" />
<input type="hidden" name="hidden_grand_total" id="hidden_grand_total" value="<?php echo $_smarty_tpl->tpl_vars['grandtotal']->value;?>
">
<input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="<?php echo $_smarty_tpl->tpl_vars['total']->value;?>
">
<input type="hidden" name="hidden_tip" id="hidden_tip" value="<?php echo $_smarty_tpl->tpl_vars['tips']->value;?>
">
<input type="hidden" name="hidden_tax" id="hidden_tax" value="<?php echo $_smarty_tpl->tpl_vars['tax']->value;?>
">
<input type="hidden" name="hidden_discount_id" id="hidden_discount_id" value="">
<input type="hidden" name="hidden_discount_amount" id="hidden_discount_amount" value="">
<?php }
}
