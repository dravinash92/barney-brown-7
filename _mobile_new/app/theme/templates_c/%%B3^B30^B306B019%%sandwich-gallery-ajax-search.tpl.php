<?php /* Smarty version 2.6.25, created on 2019-12-09 06:14:53
         compiled from sandwich-gallery-ajax-search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'sandwich-gallery-ajax-search.tpl', 2, false),array('modifier', 'replace', 'sandwich-gallery-ajax-search.tpl', 10, false),array('modifier', 'is_array', 'sandwich-gallery-ajax-search.tpl', 66, false),array('modifier', 'in_array', 'sandwich-gallery-ajax-search.tpl', 67, false),array('modifier', 'upper', 'sandwich-gallery-ajax-search.tpl', 85, false),array('modifier', 'truncate', 'sandwich-gallery-ajax-search.tpl', 85, false),)), $this); ?>

       <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['GALLARY_DATA'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
             
              <?php $this->assign('bread_name', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]); ?>
              <?php $this->assign('prot_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php $this->assign('bread_name', ((is_array($_tmp=$this->_tpl_vars['bread_name'])) ? $this->_run_mod_handler('replace', true, $_tmp, ' ', '##') : smarty_modifier_replace($_tmp, ' ', '##'))); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
      
      
              
                   
                   
                   <?php if (is_array($this->_tpl_vars['order_data']['user_sandwich']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'], $this->_tpl_vars['order_data']['user_sandwich']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
              
              <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['by_admin'] == 1): ?>
              <?php $this->assign('url', $this->_tpl_vars['ADMIN_URL']); ?>
              <?php else: ?>
              <?php $this->assign('url', $this->_tpl_vars['SITE_URL']); ?>
              <?php endif; ?>
              
              <?php $this->assign('sname', $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']); ?>
              <div class="p" class="right-rgt" data-sandwich_desc="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
" data-flag="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['flag']; ?>
" data-menuadds="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_add_count']; ?>
" data-userid ="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
" <?php if ($this->_tpl_vars['items_id'] == 1): ?> rel="ADD TO CART" <?php else: ?> rel="ADD TO CART" <?php endif; ?> data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-toast="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_toast']; ?>
" data-formatdate="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['bread_name']; ?>
"  data-date="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_price']; ?>
" data-id="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-likeid="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['like_id']; ?>
" data-likecount="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['like_count']; ?>
" data-typeSandwich="FS">
             <li class="right-rgt">
                           <img class="sandwich_gallery_view" data-href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png" alt="sandwitchimageview">
                           <input type="hidden" id="sandwichGalleryImg_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png">
                           <input type="hidden" name="chkmenuactice<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['menu_is_active']; ?>
" />
           
           <h4 class="sandwich_gallery_view"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['sname'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "..", true) : smarty_modifier_truncate($_tmp, 20, "..", true)); ?>
</h4>
           <a href="#" class="add-cart" id="view-sandwich-popup">VIEW</a>
           <?php $_from = $this->_tpl_vars['saved_data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
              <?php if ($this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'] == $this->_tpl_vars['v']): ?>
                  <?php if ($this->_tpl_vars['is_public_array'][$this->_tpl_vars['v']] == 0): ?>
                  <input type="hidden" value="<?php echo $this->_tpl_vars['is_public_array'][$this->_tpl_vars['v']]; ?>
" id="isPublic<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
">
                  <?php endif; ?> 
              <?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
          <?php if (((is_array($_tmp=$this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['saved_data']) : in_array($_tmp, $this->_tpl_vars['saved_data']))): ?>
                                <input type="hidden" name="saved_tgl<?php echo $this->_tpl_vars['GALLARY_DATA'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="1">
                              <?php endif; ?>
         </li></div>
           <input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $this->_supers['session']['uid']; ?>
" >   
          <?php endfor; endif; ?>
          
        <input type="hidden" name="sandwich_count" id="sandwich_count"  value="<?php echo $this->_tpl_vars['sandwich_count']; ?>
" >
        <input type="hidden" name="search" id="search"  value="true" >
    
    
    