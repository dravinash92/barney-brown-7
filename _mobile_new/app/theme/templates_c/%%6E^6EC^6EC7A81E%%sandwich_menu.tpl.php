<?php /* Smarty version 2.6.25, created on 2020-09-23 03:43:09
         compiled from sandwich_menu.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'rand', 'sandwich_menu.tpl', 1, false),array('modifier', 'count', 'sandwich_menu.tpl', 108, false),array('modifier', 'is_array', 'sandwich_menu.tpl', 172, false),array('modifier', 'in_array', 'sandwich_menu.tpl', 173, false),array('modifier', 'json_decode', 'sandwich_menu.tpl', 200, false),)), $this); ?>
<?php $this->assign('random_number', ((is_array($_tmp=100000)) ? $this->_run_mod_handler('rand', true, $_tmp, 999990) : rand($_tmp, 999990))); ?>
<section class="sandwich new_sandwich_mobile">
            
            <div class="container">
              <div class="new_gallery_count">
              <p><?php echo $this->_tpl_vars['galleryCount']; ?>
</p>
              <span>shared creations</span>
              <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/gallery/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">VIEW ALL</a>
            </div>
                <div id="sandwich_overview_mobile" class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Create a Sandwich</h3>
                    <ul class="user-menu-sandwiches">
                        <li class=" vertical-aligned">
                            <span class="new_create__sandwich">
                                <div class="homebannerauto">
      <div class="homepageloader">
<img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/ellipsis.svg" class="homeAnimationLoaderImg" alt="">
</div>
        <span class="homepageAutoBanner">
        <span class="sandwich-1">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/overviewpage_animation/sandwich1/1.png" style="display:block" alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich1/2.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich1/3.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich1/4.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich1/5.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich1/6.png" style="display:none"  alt="">
        </span>
        
        <span class="sandwich-2">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich2/7.png"  style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich2/8.png"  style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich2/9.png"  style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich2/10.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich2/11.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich2/12.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich2/13.png" style="display:none"  alt="">
        </span>
        
        <span class="sandwich-3">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich3/14.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich3/15.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich3/16.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich3/17.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich3/18.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich3/19.png" style="display:none"  alt="">
          <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/overviewpage_animation/sandwich3/20.png" style="display:none"  alt="">
        </span>
        
        </span>
         <?php echo '
 <script type="text/javascript">
 window.addEventListener(\'load\',function(){ mobile_ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
 </script>
'; ?>

      </div> 
                            </span>
                        </li>
                        <li class="vertical-aligned">
                            <div class="right-a ">
                                <h5 class="create_own__sandwich">sandwich customizer</h5>
                                    <h2>Build Your Own in 5 Easy Steps</h2>
                                <a class="get-started__new" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
">CUSTOMIZE</a>
                                
                            </div>
                        </li>


                    </ul>

                    <div class="loadMoreGallery" style="text-align:center"><img
                            src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
/app/images/star.png" alt="Loading" /> </div>

                </div>
                <!-- <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Sandwich Gallery</h3>
                    <ul class="user-menu-sandwiches">
                        <li class="right-rgt vertical-aligned">
                            <span class="new_create__sandwich">
                                <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/Layer_6_copy.png">
                            </span>
                        </li>

                        <li class="right-rgt vertical-aligned">
                            <div class="right-a ">
                                <h5 class="create_own__sandwich shared-sandwich__head">
                                    <p class="big-count"><?php echo $this->_tpl_vars['galleryCount']; ?>
</p>
                                    Shared Sandwiches
                                </h5>
                                <a class="get-started__new" href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/gallery">VIEW GALLERY</a>
                                 <p>Use one of the options to the left to get started!</p>
                            </div>
                        </li>


                    </ul>

                    <div class="loadMoreGallery" style="text-align:center"><img
                            src="http://mobile.ha.allthingsmedia.com/app/images/star.png" alt="Loading" /> </div>

                </div> -->
                <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>My Saved Sandwiches <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/savedSandwiches/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="view-all-new"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a></h3>
                    <?php if (! empty ( $this->_tpl_vars['saved_sandwich_data'] )): ?>
                              <ul>
                                 
                              <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['saved_sandwich_data'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
             
              <?php $this->assign('bread_name', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]); ?>
              <?php $this->assign('prot_data', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php $this->assign('bread_name', $this->_tpl_vars['bread_name']); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
      
      
              
                   
                   
                   <?php if (is_array($this->_tpl_vars['order_data']['user_sandwich']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id'], $this->_tpl_vars['order_data']['user_sandwich']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
              
              <?php if ($this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['by_admin'] == 1): ?>
              <?php $this->assign('url', $this->_tpl_vars['ADMIN_URL']); ?>
              <?php else: ?>
              <?php $this->assign('url', $this->_tpl_vars['SITE_URL']); ?>
              <?php endif; ?>
              
              <?php $this->assign('sdesc', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_desc']); ?>
              <?php $this->assign('fname', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['first_name']); ?>
              <?php $this->assign('lname', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['last_name']); ?>
              <?php $this->assign('sprice', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['current_price']); ?>
              <?php $this->assign('user_name', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['user_name']); ?>
              <?php $this->assign('sname', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_name']); ?>
               <?php $this->assign('tname', $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_name_trimmed']); ?>
                    <div class="save-sandwich--wrap" data-id="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-sandwich_desc="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
" data-bread="<?php echo $this->_tpl_vars['bread_name']; ?>
">
        <input type="hidden" id="featured_sandwich_view_popup" value="1">
        <div class="save-sand--img">
          <span><img title="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" class="view_sandwich" data-href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/toasted.png" class="toasted" style="<?php if ($this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['menu_toast'] == 1): ?> display:block; <?php endif; ?>"></span>
        </div>

        <div class="save-sand--content save-sand--content-new save-sand_three_sec" id="toastID_<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-sandwich_desc="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
" data-sandwich_desc_id="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_desc_id']; ?>
" data-flag="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['flag']; ?>
" data-menuadds="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['menu_add_count']; ?>
" data-userid ="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
" <?php if ($this->_tpl_vars['items_id'] == 1): ?> rel="ADD TO CART" <?php else: ?> rel="ADD TO CART" <?php endif; ?> data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-toast="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['menu_toast']; ?>
" data-formatdate="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['bread_name']; ?>
"  data-date="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['current_price']; ?>
" data-id="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-likeid="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['like_id']; ?>
" data-likecount="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['like_count']; ?>
" data-typeSandwich="SS" data-page="SOP">
            <input type="hidden" name="chkmenuactice<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['menu_is_active']; ?>
" />
            
            <?php $this->assign('jsonData', ((is_array($_tmp=$this->_tpl_vars['sandwich']->sandwich_data)) ? $this->_run_mod_handler('json_decode', true, $_tmp, 1) : json_decode($_tmp, 1))); ?>
            <span class="saved-sand--price"><?php echo $this->_tpl_vars['sname']; ?>
</span>
            <p><?php echo $this->_tpl_vars['sdesc']; ?>
</p>
            <input type="hidden" value="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['is_public']; ?>
" id="isPublic<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
">
                           
            <img data-href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" id="sandwichimg_<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png" hidden>
            <p>Created by: <?php echo $this->_tpl_vars['user_name']; ?>
.</p>
            <div id="featured_new_mobile">
              <h2>$<?php echo $this->_tpl_vars['sprice']; ?>
</h2>
              <div>
                 <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup leftSpinner"></a>
                       <input id="sandwichQty<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" name="itemQuty" type="text" class="text-box" value="01" readonly="">
                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup rightSpinner"></a>
                </h3>
              </div>
              <div class="saved-add-cartwrap">
                <a class="saved-add-cart quickAddToCart" href="#"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/cart.png"><span>Add</span></a>
              </div>
            </div>
            
            <div class="saved-item-btns menu-saved_list">
              <a href="javascript:void(0);" class="view_sandwich" id="view-sandwich-popup">View</a>
              <!-- <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
">Edit</a> -->
              <a href="javascript:void(0);" class="editSandwich" id="<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
">Edit</a>
              <?php if ($this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['menu_is_active'] == 1): ?>
              
              <a class="saved" href="javascript:void(0);">SAVED</a>
              <input type="hidden" name="saved_tgl<?php echo $this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="1">
              <?php else: ?>
              <a href="javascript:void(0);" class="featured_item_btn">SAVE</a>
              <?php endif; ?>
              <?php if ($this->_tpl_vars['saved_sandwich_data'][$this->_sections['sandwitch']['index']]['is_public'] == 0): ?>
              <span class="locked-item"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/locked-itm.png"></span>
              <?php endif; ?>
          </div>
        </div>
  <input class="typeSandwich" type="hidden" value="SS">
  <input type="hidden" name="saved_tgl<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="1">
 <input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $this->_supers['session']['uid']; ?>
" >
  <input type="hidden" value="product" name="data_type">
 <!--  <div class="add-new no-items">
    

  </div> -->
</div>
                    <?php endfor; endif; ?> 
                </div>
                <?php else: ?>
                  <div class="no-saved-sandwiches">
                     <h3>
                     you currently have no saved sandwiches
                     </h3>
                     <p>EITHER CREATE YOUR OWN CUSTOM SANDWICH, CHOOSE FROM OUR USER-SUBMITTED GALLERY, OR TRY ONE OF YOUR FRIEND'S SANDWICHES.
                     </p>
                  </div>
               <?php endif; ?>
                <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3><span class="span-update">UPDATED MONTHLY!</span><span>Featured Sandwiches </span><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
sandwich/featuredSandwiches/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="view-all-new"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a></h3>
                    <?php unset($this->_sections['sandwitch']);
$this->_sections['sandwitch']['name'] = 'sandwitch';
$this->_sections['sandwitch']['start'] = (int)0;
$this->_sections['sandwitch']['loop'] = is_array($_loop=count($this->_tpl_vars['featured_data'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sandwitch']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['sandwitch']['show'] = true;
$this->_sections['sandwitch']['max'] = $this->_sections['sandwitch']['loop'];
if ($this->_sections['sandwitch']['start'] < 0)
    $this->_sections['sandwitch']['start'] = max($this->_sections['sandwitch']['step'] > 0 ? 0 : -1, $this->_sections['sandwitch']['loop'] + $this->_sections['sandwitch']['start']);
else
    $this->_sections['sandwitch']['start'] = min($this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] : $this->_sections['sandwitch']['loop']-1);
if ($this->_sections['sandwitch']['show']) {
    $this->_sections['sandwitch']['total'] = min(ceil(($this->_sections['sandwitch']['step'] > 0 ? $this->_sections['sandwitch']['loop'] - $this->_sections['sandwitch']['start'] : $this->_sections['sandwitch']['start']+1)/abs($this->_sections['sandwitch']['step'])), $this->_sections['sandwitch']['max']);
    if ($this->_sections['sandwitch']['total'] == 0)
        $this->_sections['sandwitch']['show'] = false;
} else
    $this->_sections['sandwitch']['total'] = 0;
if ($this->_sections['sandwitch']['show']):

            for ($this->_sections['sandwitch']['index'] = $this->_sections['sandwitch']['start'], $this->_sections['sandwitch']['iteration'] = 1;
                 $this->_sections['sandwitch']['iteration'] <= $this->_sections['sandwitch']['total'];
                 $this->_sections['sandwitch']['index'] += $this->_sections['sandwitch']['step'], $this->_sections['sandwitch']['iteration']++):
$this->_sections['sandwitch']['rownum'] = $this->_sections['sandwitch']['iteration'];
$this->_sections['sandwitch']['index_prev'] = $this->_sections['sandwitch']['index'] - $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['index_next'] = $this->_sections['sandwitch']['index'] + $this->_sections['sandwitch']['step'];
$this->_sections['sandwitch']['first']      = ($this->_sections['sandwitch']['iteration'] == 1);
$this->_sections['sandwitch']['last']       = ($this->_sections['sandwitch']['iteration'] == $this->_sections['sandwitch']['total']);
?>
             
              <?php $this->assign('bread_name', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['BREAD']['item_name'][0]); ?>
              <?php $this->assign('prot_data', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['PROTEIN']['item_name']); ?>
              <?php $this->assign('cheese_data', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['CHEESE']['item_name']); ?>
              <?php $this->assign('topping_data', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['TOPPINGS']['item_name']); ?>
              <?php $this->assign('cond_data', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_data']['CONDIMENTS']['item_name']); ?>
              
              <?php $this->assign('bread_name', $this->_tpl_vars['bread_name']); ?>
              
              <?php 
              $result = '';
              $d = $this->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
               ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $this->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
               ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $this->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
               ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $this->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
               ?>
      
      
              
                   
                   
                   <?php if (is_array($this->_tpl_vars['order_data']['user_sandwich']['item_id'])): ?> 
                   <?php $this->assign('items_id', in_array($this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id'], $this->_tpl_vars['order_data']['user_sandwich']['item_id'])); ?>
                   <?php else: ?>
                   <?php $this->assign('items_id', '0'); ?>
                   <?php endif; ?>
              
              <?php if ($this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['by_admin'] == 1): ?>
              <?php $this->assign('url', $this->_tpl_vars['ADMIN_URL']); ?>
              <?php else: ?>
              <?php $this->assign('url', $this->_tpl_vars['SITE_URL']); ?>
              <?php endif; ?>
              
              <?php $this->assign('sdesc', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_desc']); ?>
              <?php $this->assign('fname', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['first_name']); ?>
              <?php $this->assign('lname', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['last_name']); ?>
              <?php $this->assign('sprice', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['current_price']); ?>
              <?php $this->assign('user_name', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['user_name']); ?>
              <?php $this->assign('sname', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_name']); ?>
               <?php $this->assign('tname', $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_name_trimmed']); ?> 
                    <div class="save-sandwich--wrap" data-id="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-sandwich_desc="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
" data-bread="<?php echo $this->_tpl_vars['bread_name']; ?>
">
        <input type="hidden" id="featured_sandwich_view_popup" value="1">
        <div class="save-sand--img">
          <span><img class="view_sandwich" title="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png"></span>
        </div>

        <div class="save-sand--content save-sand--content-new save-sand_three_sec" data-sandwich_desc="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_desc']; ?>
" data-sandwich_desc_id="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_desc_id']; ?>
" data-flag="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['flag']; ?>
" data-menuadds="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['menu_add_count']; ?>
" data-userid ="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
" <?php if ($this->_tpl_vars['items_id'] == 1): ?> rel="ADD TO CART" <?php else: ?> rel="ADD TO CART" <?php endif; ?> data-cheese="<?php  echo $result_1; ?>" data-topping="<?php  echo $result_2; ?>" data-cond="<?php  echo $result_3; ?>" data-toast="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['menu_toast']; ?>
" data-formatdate="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['formated_date']; ?>
" data-username="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['user_name']; ?>
" data-protien="<?php  echo trim($result);  ?>" data-bread="<?php echo $this->_tpl_vars['bread_name']; ?>
"  data-date="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['date_of_creation']; ?>
" data-price="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['current_price']; ?>
" data-id="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" data-name="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['sandwich_name']; ?>
" data-likeid="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['like_id']; ?>
" data-likecount="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['like_count']; ?>
" data-typeSandwich="FS" data-page="SOP">

            <input type="hidden" name="chkmenuactice<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['menu_is_active']; ?>
" />
            
            <?php $this->assign('jsonData', ((is_array($_tmp=$this->_tpl_vars['sandwich']->sandwich_data)) ? $this->_run_mod_handler('json_decode', true, $_tmp, 1) : json_decode($_tmp, 1))); ?>
          
            <span class="saved-sand--price"><?php echo $this->_tpl_vars['sname']; ?>
</span>
            <p><?php echo $this->_tpl_vars['sdesc']; ?>
</p>
            
             <img data-href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" id="sandwichimg_<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" src="<?php echo $this->_tpl_vars['image_path']; ?>
<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
/sandwich_<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
_<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['uid']; ?>
.png" hidden>
            <p>Created by: <?php echo $this->_tpl_vars['user_name']; ?>
.</p>

            <div id="featured_new_mobile">
              
              <h2>$<?php echo $this->_tpl_vars['sprice']; ?>
</h2>
              <div>
                <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                                        <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                                        <a href="javascript:void(0);" class="sandwich_gallery_view_popup leftSpinner"></a>
                                           <input id="sandwichQty<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" name="itemQuty" type="text" class="text-box" value="01" readonly="">
                                        <a href="javascript:void(0);" class="sandwich_gallery_view_popup rightSpinner"></a>
                                    </h3>
              </div>
              <div class="saved-add-cartwrap">
                  <a class="saved-add-cart quickAddToCart" href="#"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/cart.png"><span>Add</span></a>
              </div>

            </div>
              <div class="saved-item-btns saved-item-btns-new menu-saved_list">
                                <a href="javascript:void(0);" class="view_sandwich" id="view-sandwich-popup">View</a>
                                <!-- <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
createsandwich/index/<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
">Edit</a> -->
                                <a href="javascript:void(0);" class="editSandwich" id="<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
">Edit</a>
                                <?php if (((is_array($_tmp=$this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['saved_sandwich_ids']) : in_array($_tmp, $this->_tpl_vars['saved_sandwich_ids']))): ?>
                                
                                <a href="javascript:void(0);" class="saved ">SAVED</a>
                                <input type="hidden" name="saved_tgl<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
" value="1">
                                <?php else: ?>
                                <a class="featured_item_btn" href="javascript:void(0);">SAVE</a>
                                <?php endif; ?>
                                <?php $_from = $this->_tpl_vars['saved_sandwich_ids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
                                  <?php if ($this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id'] == $this->_tpl_vars['v']): ?>
                                      <?php if ($this->_tpl_vars['is_public_array'][$this->_tpl_vars['v']] == 0): ?>
                                      <input type="hidden" value="0" id="isPublicFs<?php echo $this->_tpl_vars['featured_data'][$this->_sections['sandwitch']['index']]['id']; ?>
"> 
                                          <span class="locked-item"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/locked-itm.png"></span>
                                      <?php endif; ?>
                                  <?php endif; ?>

                                     
                                <?php endforeach; endif; unset($_from); ?>
                                    
            </div>
        </div>
  <input class="typeSandwich" type="hidden" value="FS">
<input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $this->_supers['session']['uid']; ?>
" >
  <input type="hidden" value="product" name="data_type">
 <!--  <div class="add-new no-items">
   

  </div> -->
</div>
                    <?php endfor; endif; ?>
                </div>
                <div class="sandwich-wrapper sandwich-new-wrapp bottom-blank">
                    <h3><span class="fb-left-head"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/facebook-round.png"></span>Friends’ Sandwiches<a
                            href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/friendSandwiches/?pid=<?php echo $this->_tpl_vars['random_number']; ?>
" class="view-all-new">
                            <img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a>
                    </h3>
                    <?php if (! empty ( $this->_tpl_vars['fbdata'] )): ?>
                    <div class="friends-sandwich--block">
                        <?php $i = 1; ?>
                            <?php $_from = $this->_tpl_vars['fbdata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                            <?php  if ($i <=5){  ?>
                        <div class="friends-sandwich">
                            <div class="friends-dp"><img src="https://graph.facebook.com/<?php echo $this->_tpl_vars['item']->friend_uid; ?>
/picture?width=107&height=107"></div>
                            <div class="friend-name">
                                <h2><?php echo $this->_tpl_vars['item']->first_name; ?>
 <?php echo $this->_tpl_vars['item']->last_name; ?>
</h2>
                                <p><?php echo $this->_tpl_vars['item']->sandwich_count; ?>
 saved sandwiches</p>
                            </div>
                            <div class="friends-view-wrap">
                                <div class="friends-view--btn"><a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/friendsMenu/<?php echo $this->_tpl_vars['item']->uid; ?>
" class="freinds-view">view</a>
                                </div>
                            </div>
                        </div>
                        <?php $i++; } ?>
                              
                        <?php endforeach; endif; unset($_from); ?>

                        <div>
                            <a href="<?php echo $this->_tpl_vars['SITE_URL']; ?>
menu/friendSandwiches" class="viewall-saved-sandwiches"><img
                                    src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/view-all--border.png"><span>view all
                                    <!--connect--></span></a>
                        </div>
                    </div>
                    <?php else: ?>
                           <div class="friends-items-bottom not-connected-fb">
                              <p>CONNECT TO FACEBOOK TO SEE YOUR FRIENDS’ SANDWICHES!</p>
                              <div class="friends-items-view">
                                 <a href="#" class="viewall-saved-sandwiches"><img src="<?php echo $this->_tpl_vars['SITE_URL']; ?>
app/images/view-all--border.png"><span class="friends-items-view-h2 login-facebook">CONNECT</span></a>
                              </div>
                           </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>