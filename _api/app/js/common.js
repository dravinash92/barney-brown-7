    (function() {
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    Date.prototype.getMonthName = function() {
        return months[ this.getMonth() ];
    };
    Date.prototype.getDayName = function() {
        return days[ this.getDay() ];
    };
})();


// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};



$(document).ready(function () {
    VALID_USER_PICKED_RECENTLY = 0
    LAST_TIME_PICK = '';
    USER_LAST_TIME = '';



    $(".datepicker").val(new Date().toLocaleDateString());
    
    $("select.delevry_time").html(getTimings());


    itemCountArrowEvent();


    if ($(".save-menu").length > 0) {
        $(".save-menu").click(function () {
            $(".popup-wrapper").hide();
            $("#added-to-menu").show();
        });
    }



    if ($(".change-address").length > 0) {
        $(".change-address").click(function () {
            $(".popup-wrapper").hide();
            $("#add-new-address").show();
        });
    }

    if ($(".all-friend").length > 0) {
        $(".all-friend").click(function () {
            $(".popup-wrapper").hide();
            $("#facebook-friends-list").show();
        });
    }


    if ($(".close-button").length > 0) {
        $(".close-button").click(function (e) {
            e.preventDefault();
            var formid = $(this).parent().parent().attr("id");
            $(".rerror_msg").empty();
            $(".error_msg").empty();
            $('#' + formid + ' input').not('input[type="hidden"]').removeClass("register");
            $('#' + formid + ' input').not('input[type="hidden"]').val("");
            $(".popup-wrapper").hide();
             $('body').css('overflow', 'auto');
        });
    }

    $('#warningOk').on('click', function (e) {
        e.preventDefault();


        $('#warningcheckout').hide();

        $('.error_msg').hide();
    });


    $('.scroll-bar').jScrollPane();


    function add() {
        if (jQuery(this).val() === '') {
            jQuery(this).val(jQuery(this).attr('placeholder')).addClass('placeholder');
        }
    }

    function remove() {
        if (jQuery(this).val() === jQuery(this).attr('placeholder')) {
            jQuery(this).val('').removeClass('placeholder');
        }
    }

    // Create a dummy element for feature detection
    if (!('placeholder' in jQuery('<input>')[0])) {
        // Select the elements that have a placeholder attribute
        jQuery('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

        // Remove the placeholder text before the form is submitted
        jQuery('form').submit(function () {
            jQuery(this).find('input[placeholder], textarea[placeholder]').each(remove);
        });
    }

    //Function contains code regarding if user not logged in.
    cardEditRemoveEvents();
    bindMyAccountEvents();
    removeCartItems();
    itemArrowEvent();

    checkDateVariables( );
//    ha.common.look_for_date_change();
    //console.log( " ### END ### " );
});
(function ($) {

    $.session = {
        _id: null,
        _cookieCache: undefined,
        _init: function () {
            if (!window.name) {
                window.name = Math.random();
            }
            this._id = window.name;
            this._initCache();
 
            // See if we've changed protcols

            var matches = (new RegExp(this._generatePrefix() + "=([^;]+);")).exec(document.cookie);
            if (matches && document.location.protocol !== matches[1]) {
                this._clearSession();
                for (var key in this._cookieCache) {
                    try {
                        window.sessionStorage.setItem(key, this._cookieCache[key]);
                    } catch (e) {
                    }
                    ;
                }
            }

            document.cookie = this._generatePrefix() + "=" + document.location.protocol + ';path=/;expires=' + (new Date((new Date).getTime() + 120000)).toUTCString();

        },
        _generatePrefix: function () {
            return '__session:' + this._id + ':';
        },
        _initCache: function () {
            var cookies = document.cookie.split(';');
            this._cookieCache = {};
            for (var i in cookies) {
                var kv = cookies[i].split('=');
                if ((new RegExp(this._generatePrefix() + '.+')).test(kv[0]) && kv[1]) {
                    this._cookieCache[kv[0].split(':', 3)[2]] = kv[1];
                }
            }
        },
        _setFallback: function (key, value, onceOnly) {
            var cookie = this._generatePrefix() + key + "=" + value + "; path=/";
            if (onceOnly) {
                cookie += "; expires=" + (new Date(Date.now() + 120000)).toUTCString();
            }
            document.cookie = cookie;
            this._cookieCache[key] = value;
            return this;
        },
        _getFallback: function (key) {
            if (!this._cookieCache) {
                this._initCache();
            }
            return this._cookieCache[key];
        },
        _clearFallback: function () {
            for (var i in this._cookieCache) {
                document.cookie = this._generatePrefix() + i + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            }
            this._cookieCache = {};
        },
        _deleteFallback: function (key) {
            document.cookie = this._generatePrefix() + key + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            delete this._cookieCache[key];
        },
        get: function (key) {
            return window.sessionStorage.getItem(key) || this._getFallback(key);
        },
        set: function (key, value, onceOnly) {
            try {
                window.sessionStorage.setItem(key, value);
            } catch (e) {
            }
            this._setFallback(key, value, onceOnly || false);
            return this;
        },
        'delete': function (key) {
            return this.remove(key);
        },
        remove: function (key) {
            try {
                window.sessionStorage.removeItem(key);
            } catch (e) {
            }
            ;
            this._deleteFallback(key);
            return this;
        },
        _clearSession: function () {
            try {
                window.sessionStorage.clear();
            } catch (e) {
                for (var i in window.sessionStorage) {
                    window.sessionStorage.removeItem(i);
                }
            }
        },
        clear: function () {
            this._clearSession();
            this._clearFallback();
            return this;
        }

    };

    $.session._init();

})(jQuery);

equalheight = function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}



function getTimings(selectedDate) {
    if (selectedDate)
        var date_selected = new Date(selectedDate);
    else
        var date_selected = new Date();

    var date_today = new Date();

    if (date_today.getDate() == date_selected.getDate()) {
        var cur_hour = new Date().getHours();
        var cur_min = new Date().getMinutes();
        var options = "";

        for (temp_hr = cur_hour; temp_hr <= 21; temp_hr++) {

            var h_in = temp_hr;
            var am_pm = " AM";
            if (h_in > 12) {
                h_in = temp_hr - 12;
                am_pm = " PM";
            }

            if (cur_min + 15 > 59)
                cur_min = (cur_min + 15) - 59;

            if (cur_min > 0 && cur_min <= 15) {
                cur_min = 30;
            } else if (cur_min > 15 && cur_min <= 30) {
                cur_min = 45;
            } else if (cur_min >= 45 && cur_min <= 59)
                cur_min = 0;

            for (temp_min = cur_min; temp_min <= 59; temp_min = temp_min + 15) {
                options = options + "<option value='" + temp_hr + ":" + temp_min + "'>" + h_in + ":" + temp_min + am_pm + "</option>";
            }

        }
    } else if (date_today.getDate() < date_selected.getDate()) {

        var options = "";
        var open_time = 10;
        for (temp_hr = open_time; temp_hr <= 21; temp_hr++) {

            var h_in = temp_hr;
            var am_pm = " AM";
            if (h_in > 12) {
                h_in = temp_hr - 12;
                am_pm = " PM";
            }

            for (temp_min = 15; temp_min <= 59; temp_min = temp_min + 15) {
                options = options + "<option value='" + temp_hr + ":" + temp_min + "'>" + h_in + ":" + temp_min + am_pm + "</option>";
            }
        }
    }

    return options;
}

function itemArrowEvent() {

    $(".right, .right-hover").unbind('click');
    $(document).on("click", ".right, .right-hover", function () {

        if (window.location.href.indexOf("createsandwich") != -1) {
            return;
        }

        var sandwich_id = $(this).parent("div").find(".sandwich_id").val();
        var order_item_id = $(this).parent("div").find(".order_item_id").val();

        var count = $(this).prev(".text-box").val();
        count = +count + +01;
        var counts = count;
        var counts = counts.toString().length;

        if (count > 1) {
            $(this).prev().prev().removeClass("left");
            $(this).prev().prev().addClass("left-hover");
        }
        if (count < 1000) {
            if (counts == 1) {
                count = "0" + count;
            }
            selspin = $(this).parent("div").find('input[name="spinnerinput"]');
            $(selspin).val(count);

            if (count > 998) {
                $(this).removeClass("right");
                $(this).addClass("right-hover");
            }
            if ($(this).hasClass("noaction"))
                return false;

            updateCartContent(this, sandwich_id, count, order_item_id, "right");
            ha.common.checkForAllConditionsPlaceOrderButton();
        }
        checkZipcodeSubtotal();
        ha.common.checkForAllConditionsPlaceOrderButton();
    });

    $(".left, .left-hover").unbind('click');
    $(document).on("click", ".left, .left-hover", function () {

        if (window.location.href.indexOf("createsandwich") != -1) {
            return;
        }

        var sandwich_id = $(this).parent("div").find(".sandwich_id").val();
        var order_item_id = $(this).parent("div").find(".order_item_id").val();

        var count = $(this).next().val();
        count = +count - +01;
        var counts = count;
        var counts = counts.toString().length;
        if (count < 2) {
            $(this).removeClass("left-hover");
            $(this).addClass("left");
        } else if (count == 998) {
            $(this).next().next().removeClass("right-hover");
            $(this).next().next().addClass("right");
        }
        if (count < 1) {
        } else {
            if (counts == 1) {
                count = "0" + count;
            }

            selspin = $(this).parent("div").find('input[name="spinnerinput"]');
            $(selspin).val(count);
            if ($(this).hasClass("noaction"))
                return false;
            updateCartContent(this, sandwich_id, count, order_item_id, "left");
            ha.common.checkForAllConditionsPlaceOrderButton();
        }
        checkZipcodeSubtotal();
        ha.common.checkForAllConditionsPlaceOrderButton();
    });

}
function checkZipcodeSubtotal()
{
    var zip = $('input[name="ziptransfer"]').val();


    $.ajax({
        type: "POST",
        data: {
            "zip": zip
        },
        url: SITE_URL + 'checkout/checkZip',
        success: function (e) {
            //console.log(e);
            if (e == 100) {
                var subtotal = $("input[name=hidden_sub_total]").val();
                if (subtotal < 100)
                {
                    var remain = parseFloat(100.00 - subtotal).toFixed(2);
                    ha.common.setNotEnough( 1 );
                    var msg = '$100.00 SUBTOTAL MINIMUM.' + remain + '  TO GO!';
                    if ($('#messageBar').find('li').length > 0) {
                        $('.less-than-10').html(msg);
                    } else {
                        d = document.createElement('li');
                        $(d).addClass('less-than-10').html(msg).appendTo($("#messageBar"));
                    }
                }
                else
                {
                    ha.common.setNotEnough( 0 );
                }
                //$('#messageBar').addClass('less-than-10').html(msg);

            }
            else
            {
                ha.common.setNotEnough( 0 );
            }
        }
    });
}
function itemCountArrowEvent() {

    $("input.qty").each(function () {
        if ($(this).val() > 1) {
            $(this).prev().removeClass("left");
            $(this).prev().addClass("left-hover");
        }
    });


    $("input.qty").keydown(function (e) {
        e = (e) ? e : window.event;
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            e.preventDefault();
            return false;
        }
    });

    $("input.qty").keyup(function (e) {

        session = ha.common.check_session_state();
        if (session.session_state == false) {
            if (session.facebook_only_user == false) {
                ha.common.login_popup($(this));
            } else {
                ha.common.facebook_login($(this));
            }
            return;
        }

        var count = $(this).val();
        var sandwich_id = $(this).parent("div").find(".sandwich_id").val();
        var order_item_id = $(this).parent("div").find(".order_item_id").val();
        if (e.keyCode == 13) {
            updateCartContent(this, sandwich_id, count, order_item_id);
        }

    });


}

function removeCartItems() {

    $(document).on("click", "a.remove_cart_item", function () {
        var obj = this;
        var order_item_id = $(this).attr("data-target");
        var item_id = $(this).attr("data-item");
        var parent_ul = $(this).parent().parent();
        var qty_input_box = $(this).parent().parent().find(".qty")
        var left_arrow = $(this).parent().parent().find(".left");



        $.ajax({
            type: "POST",
            data: {
                "order_item_id": order_item_id,
                'itemid': item_id
            },
            url: SITE_URL + 'sandwich/remove_cart_item',
            beforeSend: function () {
                //$("#ajax-loader").show();
            },
            complete: function () {
                //$("#ajax-loader").hide();
            },
            success: function (e) {
                //$(parent_ul).css("visibility", "hidden");
                $("a.remove_cart_item").unbind("click");
                $(parent_ul).slideUp("slow").remove();

                if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
                    //resetCartList('deliverychooseaddress');
                    ha.common.updatePickupOrDeliveryInCart(1);
                } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
                    //resetCartList('pickupstorelocation');
                    ha.common.updatePickupOrDeliveryInCart(0);
                } else {
                    //resetCartList('deliverychooseaddress');
                    ha.common.updatePickupOrDeliveryInCart(1);
                }

                updateCartQuantityBox();
                checkZipcodeSubtotal();
            }
        });


        $('.checkout-msg-red').remove();
        check_time_and_change_if_needed(  );
    });

}

function updateTip(obj) {
    var discount = 0;
    var subtotal = $("#hidden_sub_total").val();
    var tax = $("#hidden_tax").val();
    var tip = $(obj).val();

    if ($("#hidden_discount_amount").val()) {
        discount = $("#hidden_discount_amount").val();
    }

    $("#hidden_tip").val(tip);
    $.session.set("hidd_tip", tip);
    var net_tot = parseFloat(tip) + parseFloat(subtotal) + parseFloat(tax);
    $(".grand-total").html(net_tot.toFixed(2));
    $("#hidden_grand_total").html(net_tot.toFixed(2));
    //
    $("#hidden_grand_total").val(net_tot.toFixed(2));
}


function updateCartContent(obj, sandwich_id, count, order_item_id, whichArrow) {
    var qty = count;
    var data_id = sandwich_id;
    var data_type = $(obj).parent().find("input[name=data_type]").val();
    var last_tip = $("#select_tip_amount").val();
    var delvry = -1;

    if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
        delvry = 1;
    } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
        delvry = 0;
    }

    var data = {
        "qty": qty,
        "data_id": data_id,
        "data_type": data_type,
        "order_item_id": order_item_id,
        "deliveryOrPickup": delvry,
        "last_tip": last_tip,
        "whichArrow": whichArrow
    };

    $.ajax({
        type: "POST",
        data: data,
        url: SITE_URL + 'checkout/updateCartItems/',
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        },
        success: function (e) {

            if (!e)
                return;
            //console.log( e );
            var data = JSON.parse(e);

            if (data.status) {
                $(".check-out-left").html(data.message);
                window.ha.common.popup_info_checkout();
                //removeCartItems();

                updateCartQuantityBox();

                itemCountArrowEvent();

                ha.common.checkForAllConditionsPlaceOrderButton();

            } else {

                alert("Session has been expired. login again.");
                window.location = SITE_URL;
            }
        }
    });


}

function updateCartQuantityBox() {

    $.ajax({
        type: "POST",
        data: {},
        url: SITE_URL + 'checkout/getCartItemQtyCount/',
        async: false,
        success: function (e) {
            $("a.shopping-cart span").text(e);
            var subtotal = $("#hidden_sub_total").val();

            //if last tip is greater than current tip then restore
            //commented Sep 22 2016
            //var current_tip = $("#select_tip_amount").val();


            //forcefully false so on increment and decrement tip value will change accordingly of price																						
            /*
             if(false && parseFloat(last_tip) > parseFloat(current_tip)){
             $("#select_tip_amount option[value='"+last_tip+"']").prop('selected', true); 
             
             $("#hidden_tip").val(last_tip);							
             var net_tot = parseInt(current_tip) + parseInt(subtotal);
             $(".grand-total").html(net_tot.toFixed(2));
             }else{
             */

            //commented Sep 22 2016
            /* $("#select_tip_amount option[value='"+ current_tip +"']").attr('selected', 'selected');
             
             $("#hidden_tip").val(current_tip);
             
             var tax = 
             
             var net_tot = parseInt(current_tip) + parseInt(subtotal);
             $(".grand-total").html(net_tot.toFixed(2)); */
            //commented Sep 22 2016	

            //}

        }
    });


    setTimeout(function () {

        if (ha.sandwichcreator.vars.appliedDiscount) {
            //console.log("Here we are")
            $("#apply_discount").val(ha.sandwichcreator.vars.appliedDiscount);
            applyDiscount();
        }
    }, 300);
    checkZipcodeSubtotal();
}

function changeUserpage(pagename) {

    $(".my-account-menu>ul>li>a.active").removeClass("active");
    $("." + pagename).addClass('active');
    
    $.ajax({
        type: "POST",
        url: SITE_URL + "myaccount/" + pagename,
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        }
    })
            .done(function (msg) {
                
                if (pagename == 'profile') {
                    $(".menu-list-wrapper").html(msg);
                    ha.common.add_edit_user_review();
                } else {
                    
                    if(  msg )
                    {
//                        console.dir( $(".accout-info-wrapper") );
                            $(".accout-info-wrapper").html(msg);
                        ha.common.add_edit_user_review();
                    }

                }
                bindMyAccountEvents();
                cardEditRemoveEvents();
                
                if (pagename == 'savedaddress') {
                        equalheight('.saved-address-inner li');
                }
                
            });

    bindMyAccountEvents();
    cardEditRemoveEvents();
}


function bindMyAccountEvents() {
    $(".change-address").on('click', function () {
        $(".popup-wrapper").hide();
        $("#add-new-address").show();
    });
    $(".add-credit-card").on('click', function () {
        $(".popup-wrapper").hide();
        $("#credit-card-details").show();
    });
    $("a.save_address").unbind("click");
    $("a.save_address").click(function () {
        var form = $(this).parent("li").parent("ul").parent("form.add_address");
        var recipient = $(form).find("input[name=recipient]").val();
        var company = $(form).find("input[name=company]").val();
        var address1 = $(form).find("input[name=address1]").val();
        var address2 = $(form).find("input[name=address2]").val();
        var zip = $(form).find("input[name=zip]").val();
        var phone1 = $(form).find("input[name=phone1]").val();
        var phone2 = $(form).find("input[name=phone2]").val();
        var phone3 = $(form).find("input[name=phone3]").val();
        var crossStrt = $(form).find("input[name=cross_streets]").val();




        if (!recipient || recipient.length < 1) {
            alert("Recipient name can't be empty.");
            return false;
        }

        if (!crossStrt || crossStrt.length < 1) {
            alert("Cross Streets can't be empty.");
            return false;
        }


        /*   if (!address1 || address1.length < 1) {
         alert("Street Address can't be empty.");
         return false;
         }*/

        if (!phone1 || phone1.length < 1 || phone1.length > 3 || !phone2 || phone2.length < 1 || phone2.length > 3 || !phone3 || phone3.length < 1 || phone3.length > 4) {
            alert("Phone number should be valid.");
            return false;
        }

        if (!zip) {
            alert("Zip Code required! ");
            return false;
        } else {
            if ($.inArray(zip, ha.common.zipcodes) == -1) {
                //alert("Delivery is not available in this area! ");
                alert("Sorry, delivery is not available in this area! Barney Brown currently delivers in Manhattan from 14th Street to 42nd Street.");
                return false;
            }
        }



        var data = form.serialize();



        $.ajax({
            type: "POST",
            data: data,
            url: SITE_URL + 'myaccount/savingAddress',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {

                $(form).find("input[type=text], textarea").val("");
                $(".popup-wrapper").hide();
                $.ajax({
                    type: "POST",
                    data: {
                        "zip": zip
                    },
                    url: SITE_URL + 'checkout/checkZip',
                    success: function (e) {
                        if (e == 100) {

                            var subtotal = $("input[name=hidden_sub_total]").val();
                             $('#warningcheckout').show();
                            if (subtotal < 100)
                            {
                               
                                var remain = parseFloat(100.00 - subtotal).toFixed(2);

                                var msg = '$100.00 SUBTOTAL MINIMUM.' + remain + '  TO GO!';
                                if ($('#messageBar').find('li').length > 0) {
                                    $('.less-than-10').html(msg);
                                } else {
                                    d = document.createElement('li');
                                    $(d).addClass('less-than-10').html(msg).appendTo($("#messageBar"))
                                }
                            }
                        }
                    }
                });
                //checkZipcodeSubtotal();



                $(form).find("input[name=recipient]").val(recipient);
                changeUserpage('savedaddress');
                ha.common.check_current_condition_for_time();
                check_time_and_change_if_needed(  );
                //$("#radio1").trigger("click");
                ha.common.check_validations_state();
                ha.common.checkForAllConditionsPlaceOrderButton();
            }
        });
    });

    $("a.remove_address").unbind("click");
    $("a.remove_address").click(function () {
        var address_id = $(this).attr("data-target");

        $.ajax({
            type: "POST",
            data: {
                "address_id": address_id
            },
            url: SITE_URL + 'myaccount/removeAddress',
            success: function (e) {
                changeUserpage('savedaddress');
            }
        });

    });

    $("a.edit_address").unbind("click");
    $("a.edit_address").click(function () {
        var address_id = $(this).attr("data-target");

        $.ajax({
            type: "POST",
            data: {
                "address_id": address_id
            },
            url: SITE_URL + 'myaccount/editAddress',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                $("#edit-address div.add-new-address-inner").html(e);
                $("#edit-address").show();
                $("#edit-address div.add-new-address-inner .close-button").click(function () {
                    $("#edit-address").hide();
                });
                bindMyAccountEvents();
            }
        });

    });


    $("a.cancelIt").unbind('click').click(function () {

        $("div.account-edit-inner").slideUp(400, function () {
            $("div.account-show-inner").slideDown(400);
        });
    });
    $("a.edit_mydetails").unbind("click");
    $("a.edit_mydetails").click(function () {
        $("div.account-show-inner").slideUp(400, function () {
            $("div.account-edit-inner").slideDown(400);
        });
    });

    $("a.save_mydetails").unbind("click");
    $("a.save_mydetails").click(function () {
        var form = $("form#myaccount_detail_form");
        var userdata = form.serialize();

        $.ajax({
            type: "POST",
            data: userdata,
            url: SITE_URL + 'myaccount/userNameExist/',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == "true") {
                    //alert(data.msg);
                    return false;
                }

                $.ajax({
                    type: "POST",
                    data: userdata,
                    url: SITE_URL + 'myaccount/savingCustomerInfo/',
                    beforeSend: function () {
                        $("#ajax-loader").show();
                    },
                    complete: function () {
                        $("#ajax-loader").hide();
                    },
                    success: function (data) {
                        window.location.reload();
                    }
                });
            }
        });
    });

    // rebinding reorder.
    ha.common.myaccount_reorder();
}

function resetCartList(checkoutoption) {

    if (checkoutoption == "pickupstorelocation") {
        checkoutoptionOption = 0;
    } else {
        checkoutoptionOption = 1;
    }

    $.ajax({
        type: "POST",
        url: SITE_URL + "checkout/" + checkoutoption,
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        }

    })
            .done(function (msg) {
                $(".checkoutdynamicdiv").html(msg);
                $(".change-address").on('click', function () {
                    $(".popup-wrapper").hide();
                    $("#add-new-address").show();
                });

                bindCheckoutPageEvents();
                ha.common.check_validations_state();
                ha.common.updatePickupOrDeliveryInCart(checkoutoptionOption);
                ha.common.popup_info_checkout();

            });

}

function changeCheckoutoption(checkoutoption) {

    if (checkoutoption == "pickupstorelocation") {
        checkoutoptionOption = 0;
    } else {
        checkoutoptionOption = 1;
    }

    $.ajax({
        type: "POST",
        url: SITE_URL + "checkout/" + checkoutoption,
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        }

    })
            .done(function (msg) {
                $(".checkoutdynamicdiv").html(msg);
                $(".change-address").on('click', function () {
                    $(".popup-wrapper").hide();
                    $("#add-new-address").show();
                });

                bindCheckoutPageEvents();
                ha.common.check_validations_state();
                ha.common.updatePickupOrDeliveryInCart(checkoutoptionOption);
                ha.common.popup_info_checkout();

                if (checkoutoption == "deliverychooseaddress")
                    ha.common.checkForUserAdressesAvailable();
            });

    setTimeout(function () {
        applyDiscount();
    }, 300);


}

function checkDateVariables(  )
{
    ha.common.dumpTimeParams(  ) ;
}

function hideDate( hideValue, source ) 
{
        //console.log( "%c---HideDate Click---", "color:blue" );

    if ( source == null ) {
        source = 'User';
    }
    
   if( source == 'User')
   {
        ha.common.setDateChanged( 0 );
        ha.common.setUserLastPick( 1 );
    //    console.log( "***REAL CLICK*** " + source );
    }
    else
    {
        ha.common.setUserLastPick( 0 );
    //    console.log( "***fake** "+ source );
    }

    off_msg = $('#radio1').data('err-msg' );

    if( off_msg != "" && off_msg != 'undefined' &&  off_msg != undefined )
    {
    	$('.checkout-msg-red').remove();
    	off_msg = "<span class='checkout-msg-red'>" + off_msg + "</span>";
    	$("div.delivery-details-wrapper div.date-time > p").after(off_msg);
    	hideValue = false;
    }
    else
    {
    	$('.checkout-msg-red').remove();                                
    }
            		

    function showNow(  )
    {
       /* if( check_if_closed(  ) == 1 )
        {
        	console.log( "Closed, cancel" );
            showDatePicker( );
            return;
        }*/
        //console.log( "HIDE" );
        $(".date-time-text").hide();
        $("#radio1").prop( "disabled", false );
        $("#radio1").prop( "checked", true );
        $("#radio2").prop( "checked", false );
        $(".date-time-text").hide();
        ha.common.setTimepicked( 0 );

        if( source == 'User'){
//        	console.log( "!!! Clearing User Pick." );
    	 	ha.common.clearUserTimeTemp(  );
    	 	ha.common.clearUserTimeValidated(  );
        }
    } 
    

    function showDatePicker(  )
    {
    	//console.log( "UNHIDE" );
    	if( source == 'User'){
    	 	ha.common.setSpecificDateTimeFromElements(  );
    	 	ha.common.setTimepicked( 1 );
        }
    	 	
        $("#radio1").prop( "checked", false );
        $("#radio2").prop( "checked", true );
        $(".date-time-text").show();
    }
    
//    console.log('check now disabled - ' + ha.common.getNowDisabled(  ) );
    if( ha.common.getNowDisabled(  ) == 1 )
    {
        //console.log( "SHOW PICKER" );
        showDatePicker(  );
        

    }

    if ( hideValue == true ) {
    	//console.log( "SHOW NOW" );
        ha.common.clearUserTimeTemp(  );
        ha.common.clearUserTimeValidated(  );
        showNow(  );

    } else {
    	//console.log( "SHOW PICKER" );
        showDatePicker(  );
       // ha.common.onSpecificDateCallavailTime( hideValue );
        check_time_and_change_if_needed( 1 ); // sned 1 here...
    }
}



function bindCheckoutPageEvents() {
    $("select.select_address").unbind("change");
    $("select.select_address").on("change", function () {


        var add_id = $(this).val();
        if (add_id > 0) {
            $.ajax({
                type: "POST",
                data: {
                    "address_id": add_id
                },
                url: SITE_URL + 'checkout/deliveryto',
                beforeSend: function () {
                    $("#ajax-loader").show();
                },
                complete: function () {
                    $("#ajax-loader").hide();
                },
                success: function (e) {
                    $(".checkoutdynamicdiv").html(e);
                    bindCheckoutPageEvents();

                    zipdata = $(e);
                    zipdata = zipdata[zipdata.length - 1];
                    zip = $(zipdata).val();

                    $.ajax({
                        type: "POST",
                        data: {
                            "zip": zip
                        },
                        url: SITE_URL + 'checkout/checkZip',
                        success: function (e) {
                            //console.log(e);
                            if (e == 100) {
                                var subtotal = $("input[name=hidden_sub_total]").val();
                                if (subtotal < 100)
                                {
                                    $('#warningcheckout').show();

                                    var remain = parseFloat(100.00 - subtotal).toFixed(2);
                                    ha.common.setNotEnough ( 1 );
                                    var msg = '$100.00 SUBTOTAL MINIMUM.' + remain + '  TO GO!';
                                    if ($('#messageBar').find('li').length > 0) {
                                        $('.less-than-10').html(msg);
                                    } else {
                                        d = document.createElement('li');
                                        $(d).addClass('less-than-10').html(msg).appendTo($("#messageBar"))
                                    }
                                    //$('#messageBar').addClass('less-than-10').html(msg);
                                }
                                else
                                    {ha.common.setNotEnough ( 0 );}
                            }
                        }
                    });

                    storeid = ha.common.getStore_id_using_zip(zip)
                    $('input[name="delivery_store_id"]').val(storeid);
                    ha.common.check_validations_state();

                }
            });
        } else if (add_id == "0") {
            $("#add-new-address").show();
            DO_NOT_MESS_WITH_THE_TIME_BUTTON = true;
            var name = $('input[name="recipientName"]').val();
            $('input[name="recipient"]').val(name);
        }

        ha.common.updatePickupOrDeliveryInCart(1);
    });

    $("a.edit_address").unbind("click");
    $("a.edit_address").click(function () {
        var address_id = $(this).attr("data-target");

        $.ajax({
            type: "POST",
            data: {
                "address_id": address_id
            },
            url: SITE_URL + 'myaccount/editAddress',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                $("#edit-address div.add-new-address-inner").html(e);
                $("#edit-address").show();
                $("#edit-address div.add-new-address-inner .close-button").click(function () {
                    $("#edit-address").hide();
                });
                bindCheckoutPageEvents();
            }
        });

    });

    $("a.save_address").unbind("click");
    $("a.save_address").click(function (e) {
        
        DO_NOT_MESS_WITH_THE_TIME_BUTTON = true;
        var form = $(this).parent("li").parent("ul").parent("form.add_address");

        var recipient = $(form).find("input[name=recipient]").val();
        var company = $(form).find("input[name=company]").val();
        var address1 = $(form).find("input[name=address1]").val();
        var address2 = $(form).find("input[name=address2]").val();
        var zip = $(form).find("input[name=zip]").val();
        var phone1 = $(form).find("input[name=phone1]").val();
        var phone2 = $(form).find("input[name=phone2]").val();
        var phone3 = $(form).find("input[name=phone3]").val();
        var crossStrt = $(form).find("input[name=cross_streets]").val();




        if (!recipient || recipient.length < 1) {
            alert("Recipient name can't be empty.");
            return false;
        }

        /*    if (!address1 || address1.length < 1) {
         alert("Street Address can't be empty.");
         return false;
         }*/

        if (!crossStrt || crossStrt.length < 1) {
            alert("Cross Streets can't be empty.");
            return false;
        }

        if (!phone1 || phone1.length < 1 || phone1.length > 3 || !phone2 || phone2.length < 1 || phone2.length > 3 || !phone3 || phone3.length < 1 || phone3.length > 4) {
            alert("Phone number should be valid.");
            return false;
        }

        if (!zip) {
            alert("Zip Code required! ");
            return false;
        } else {
            if ($.inArray(zip, ha.common.zipcodes) == -1) {
                //alert("Delivery is not available in this area! ");
                alert("Sorry, delivery is not available in this area! Barney Brown currently delivers in Manhattan from 14th Street to 42nd Street. ");
                return false;
            }
        }

        var data = form.serialize();

        $.ajax({
            type: "POST",
            data: data,
            url: SITE_URL + 'myaccount/savingAddress',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                $(form).find("input[type=text], textarea").val("");
                $(".popup-wrapper").hide();
                $.ajax({
                    type: "POST",
                    data: {
                        "zip": zip
                    },
                    url: SITE_URL + 'checkout/checkZip',
                    success: function (e) {
                        //console.log(e);
                        if (e == 100) {

                            var subtotal = $("input[name=hidden_sub_total]").val();
                             $('#warningcheckout').show();
                            if (subtotal < 100)
                            {
                            var subtotal = $("input[name=hidden_sub_total]").val();
                            var remain = parseFloat(100.00 - subtotal).toFixed(2);

                            var msg = '$100.00 SUBTOTAL MINIMUM.' + remain + '  TO GO!';
                            if ($('#messageBar').find('li').length > 0) {
                                $('.less-than-10').html(msg);
                            } else {
                                d = document.createElement('li');
                                $(d).addClass('less-than-10').html(msg).appendTo($("#messageBar"))
                            }
                          }
                        }
                    }
                });
                if (e) {

                    e = parseInt(e.trim());
                    $.ajax({
                        type: "POST",
                        data: {
                            "address_id": e
                        },
                        url: SITE_URL + 'checkout/deliveryto',
                        beforeSend: function () {
                            $("#ajax-loader").show();
                        },
                        complete: function () {
                            $("#ajax-loader").hide();
                        },
                        success: function (e) {
                            $(".checkoutdynamicdiv").html(e);
                            bindCheckoutPageEvents();
                            storeid = ha.common.getStore_id_using_zip(zip)
                            $('input[name="delivery_store_id"]').val(storeid);
                            ha.common.check_validations_state();
                        }
                    });
                } else {
                    changeCheckoutoption('deliverychooseaddress');
                }
                 if(ha.common.getCookie("specific_date_exists") == 1){
                   setTimeout(function(){
                        check_time_and_change_if_needed(  );
                      }, 1000);
                }
                else{
                    check_time_and_change_if_needed(  );
                    // DM
                    // if( DO_NOT_MESS_WITH_THE_TIME_BUTTON == false )
                    //     $("#radio1").trigger("click");
                }
                ha.common.check_current_condition_for_time();
                ha.common.check_validations_state();
                ha.common.checkForAllConditionsPlaceOrderButton();
            }
        });
    });

    // calling from common.ha 
    //ha.common.look_for_date_change();
}

function cardEditRemoveEvents() {

    $('a.remove_card').unbind('click');
    $('a.remove_card').click(function () {
        var value = $(this).attr("data-id");
        $.ajax({
            type: "POST",
            data: {
                'id': value
            },
            url: SITE_URL + 'myaccount/cardRemoveEvent/',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                //console.log( "Success delete." );
                changeUserpage('savedbilling');
            }
        });
    });


    $('a.add-credit-card, a.edit_card').unbind('click');
    $('a.add-credit-card, a.edit_card').click(function () {
        $('.error_msg').empty();
        var value = $(this).attr("data-id");

        if (value == 0) {

            $('#credit-card-edit-details h1').text('ADD CREDIT CARD');
            $('#credit-card-edit-details .add-address').text('Add');
            $('#credit-card-edit-details input[name="uid"]').val(SESSION_ID);
            $('#credit-card-edit-details').show();

        } else if (value > 0) {

            $.ajax({
                type: "POST",
                data: {
                    'id': value
                },
                dataType: 'json',
                url: SITE_URL + 'checkout/get_billing/',
                beforeSend: function () {
                    $("#ajax-loader").show();
                },
                complete: function () {
                    $("#ajax-loader").hide();
                },
                success: function (e) {
                    $('#credit-card-edit-details input[name="id"]').val(e.Data[0].id);
                    $('#credit-card-edit-details input[name="uid"]').val(SESSION_ID);
                    $('#credit-card-edit-details input[name="card_number"]').val(e.Data[0].card_number);
                    $('#credit-card-edit-details input[name="card_cvv"]').val('');
                    $('#credit-card-edit-details input[name="card_zip"]').val(e.Data[0].card_zip);
                    $('#credit-card-edit-details select[name="card_type"] option[value="' + e.Data[0].card_type + '"]').prop('selected', true);
                    $('#credit-card-edit-details select[name="expire_month"] option[value="' + e.Data[0].expire_month + '"]').prop('selected', true);
                    $('#credit-card-edit-details select[name="expire_year"] option:contains("' + e.Data[0].expire_year + '")').prop('selected', true);
                    $('#credit-card-edit-details input[name="address1"]').val(e.Data[0].address1);
                    $('#credit-card-edit-details input[name="street"]').val(e.Data[0].street);
                    $('#credit-card-edit-details .add-address').text('Save');

                    $('#credit-card-edit-details').show();

                }
            });

        }
        ha.common.check_current_condition_for_time();
    });

    $("#credit-card-edit-details a.save-card-detail").unbind("click");
    $("#credit-card-edit-details a.save-card-detail").click(function () {
        var form = $("form#edit-billForm");
        arryData = form.serializeArray();
        if ($(this).text() == "Add") {
            arryData.forEach(function (e) {
                if (e.name == "id" && e.value) {
                    form.find("input[name='id']").val('');
                }
            });
        }

        var card_data = form.serialize();
        var card_number = $('#credit-card-edit-details input[name="card_number"]').val();
        var card_cvv = $('#credit-card-edit-details input[name="card_cvv"]').val();
        var card_zip = $('#credit-card-edit-details input[name="card_zip"]').val();
        var card_type = $('#credit-card-edit-details select[name="card_type"]').val();
        var expire_month = $('#credit-card-edit-details select[name="expire_month"]').val();
        var expire_year = $('#credit-card-edit-details select[name="expire_year"]').val();
        var address = $('#credit-card-edit-details input[name="address1"]').val();
        var street = $('#credit-card-edit-details input[name="street"]').val();

 
        //if (isNaN(card_number) == true ||  (card_number.length != 15 && card_number.length != 16 )) {
        
        // DM - strip out all the client side validation.
        /*if (isNaN(card_number) == true || (card_number.length < 13 || card_number.length > 16 || card_number.length == 14)) {
            alert("Invalid Card number!");
            return false;
        } /*else if (isNaN(card_cvv) == true || (card_cvv.length != 3  && card_cvv.length != 4)) {
         alert("Invalid card sec.no");
         return false;
         } */

        jsTime = ha.sandwichcart.getCurrentTimJs();
        currentMonth = parseInt(jsTime.getMonth()) + 1;
        currentYear = parseInt(jsTime.getFullYear());
        selectedMonth = parseInt(expire_month);
        selectedYear = parseInt(expire_year);

        if (selectedYear < currentYear)
        {
            alert("The expiration date is invalid");
            return false;
        } else if (selectedYear == currentYear && selectedMonth <= currentMonth) {

            alert("The expiration date is invalid");
            return false;
        }

        //console.log( "AJAX" );
        $.ajax({
            type: "POST",
            data: card_data,
            dataType: 'json',
            url: SITE_URL + 'myaccount/cardAddEditEvent/',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                //console.log( "cardAddEditEvent success" );
//                console.dir( e );
                if( e.response == undefined || e.response.status_code != '200' )
                {
                         
                    $('.error_msg').text('Please check your credit card details...');
                    $("#ajax-loader").hide();
                    return false;
                }
                
                $('#credit-card-edit-details').hide();
                //console.log( $('#credit-card-edit-details').find('form') );
                $( $('#credit-card-edit-details').find('form') )[0].reset();
                 changeUserpage('savedbilling');
                
            }
        });

    });
}

jQuery(document).ready(function () {

    $("input.reset_password").click(function () {

        var form = $(this).parent("form");
        var password = $(form).find("[name=password]").val();
        var c_password = $(form).find("[name=c_password]").val();
        var data = form.serialize();
        if (!password) {
            alert("Please fill the new password."); 
            return false;
        }

        if (!c_password) {
            alert("Please fill the confirm new password.");
            return false;
        }


        if (password != c_password) {
            alert("Confirm password not matched.");
            return false;
        }


        $.ajax({
            type: "POST",
            data: data,
            url: SITE_URL + 'site/setPassword',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {

                json = JSON.parse(e);
                if (json) {

                    if (json.status == "success") {
                        alert("Password changed successfully!");
                        window.location = SITE_URL;
                    } else {
                        alert(json.message);
                    }

                } else {
                    alert("Changing password failed!");
                }

                //alert("Your Password has been reset.")
                //window.location = SITE_URL;
            }
        });

    });
    $(".sortby").change(function () {
        var sort_id = $(this).val();
        window.location.assign("" + SITE_URL + "sandwich/gallery/?sortid=" + sort_id + "");
    });


//    if( _GLOBAL_CLOSED == 1 ){
  //      tooLateToOrder( );
   // }

});

function tooLateToOrder(  )
{
    //return;
    $('.checkout-msg-red').remove();
    ha.common.setClosed(1);
    var msg = "<span class='checkout-msg-red'>SORRY, WE&apos;RE CURRENTLY CLOSED. <br/> PLEASE SCHEDULE A DELIVERY TIME BELOW.</span>";
    $("div.delivery-details-wrapper div.date-time > p").after(msg);

    
    setTimeout(function(){
        check_time_and_change_if_needed(  );
    // DM - commendted out.
    //                    $("#radio1").attr("checked", false);
    //                    $("#radio2").attr("checked", true);
    //                    $("#radio2").trigger("click");
                    });
}


function on_the_purchase_page() {
    current_time = ha.common.getCurrentTimJs(  );
    jsTime = ha.sandwichcart.getCurrentTimJs();

    currentMonth = parseInt(jsTime.getMonth()) + 1;
    currentYear = parseInt(jsTime.getFullYear());

 



    if( validate_date_against_cc( currentMonth, currentYear ) == false )
    {
        show_card_expired(  );
        return;
    }


    selectedMonth = parseInt(expire_month);
    selectedYear = parseInt(expire_year);

    //    $(".datepicker").val(new Date().toLocaleDateString());    
};

//function check_if_store_is_open( $current_month, $current_year )

function validate_date_against_cc( $current_month, $current_year )
{

    var $expire_month = $('#credit-card-edit-details select[name="expire_month"]').val();
    var $expire_year = $('#credit-card-edit-details select[name="expire_year"]').val();
        
    if( $current_year < $expire_year )
        return true;
    else if( $current_year == $expire_year )
    {
        if( $current_month <= $expire_month )
            return true;
    }
    else
        return false;
}

function check_user_time_info(  )
{
    if( ha.common.getTimepicked(  )==1 )
    {
//        console.log( ha.common.getSpecificDate(  ) );
//        console.log( ha.common.getSpecificTime(  ) );
        
        if ( ha.common.getSpecificDate(  ) == "undefined" || !ha.common.getSpecificTime(  ) == "undefined"  || !ha.common.getSpecificDate(  ) || !ha.common.getSpecificTime(  ))
        {
            ha.common.setTimepicked( 0 );
        }
    }
}

function check_for_big_enough_bill(  )
{
    return;
    if( ha.common.getNotEnough(  ) == 1 ){
        $('.place-order').addClass('disabled');
        $('.place-order').removeClass('link');
    }
    else
    {
            $('.place-order').removeClass('disabled');
            $('.place-order').addClass('link');
    }
}

function set_datepicker_to_now(  )
{
    setManual( new Date(  ), new Date(  ) );
}

function check_if_should_skip(  )
{
	return 0;
	if( ha.common.getTimepicked(  ) == 1 )
		return 0;
    
    //check_user
    if( check_if_closed(  ) != 0 )
    	closed = 0;
    if( ha.common.checkBreadSimple(  ) )

    return 0
    if( mode === last_mode && timeStateArray.user !== 'UserPick' )
    {
 //       console.log( "Cancel 0" );
        return;
    }
}

//// FIGURE OUT THE DATE AND TIME FUNCTIONS
// Entry function...

check_time_and_change_if_needed = debounce ( function ( force_show )
{
  //  console.log( "%cChecking time and changing if needed...", "color:red" );
    var day_time_minumum = 0;

    skip = check_if_should_skip(  );
    // This abortion of a function fills out important stuff. might be able to do without. 
    check_user_time_info(  );   // override when specific pick = 0, sets pick to false.
    check_for_big_enough_bill(  );  // this is for the 100 dollar requirement.  
    ha.common.init_date_picker( );


    var timeStateArray = pick_time_state( );

   //  console.log( "%cTime state: " , "color:red" );
    //console.dir( timeStateArray );

   
    mode = timeStateArray['state'];
//    last_mode = LAST_TIME_PICK;
    
    ha.common.setTimeStateArray( timeStateArray );      // This saves our state array that we built.
     


    checkDateVariables( );                              // This just dumps all the date and time info.


   // console.log( "%cTime state: " , "color:red" );
  //  console.dir( timeStateArray );


    handle_user_time_pick_first_run( timeStateArray  );                // This is first run.  Sets appropriate stuff if it works.
    limit_datepicker_min_date( mode, timeStateArray.timeFloor );                  // Sets datepicker minimum date - either 0 for now or 1 for tomorrow.

    // So now we have the valid user info, we have the floor.  Now what?  
    // Get the actual time to use.

    set_date_time = pick_the_actual_time_to_set_in_datetime_picker( timeStateArray );
  //  console.log( "%cPicked time: " + ha.common.format_date_time( set_date_time ) , "color:red" );
//
  //   console.log( "%cTime state: " , "color:red" );
//    console.dir( timeStateArray );


    if( ha.common.getUserTimeValidated(  ) == 0 )
    {
     //   console.log( "%cUser validation failed.", "color:blue" );
        set_date_time = noon_or_greater( get_min_early_entry(  set_date_time ) );   //this makes sure our pick is not too early. 
    }
    else
    {
       // console.log( "%cUser validation SUCCESS.", "color:blue" );
    }
    // console.log( "%cPicked time try 2: " + ha.common.format_date_time( set_date_time ) , "color:red" );

	timeStateArray['set_date_time'] = set_date_time;
    timeStateArray = figure_out_element_states( timeStateArray );
    timeStateArray = set_open_floor( timeStateArray );

   // console.log( "%cTimestate: ", "color:red"  );
  //  console.dir( timeStateArray );
	//console.log( "%cTime to set: " + ha.common.format_date_time( set_date_time ) , "color:red" );

    if( force_show == 1)
    {
        timeStateArray['nowHidden'] = true;
    }

    set_time_in_html_element( timeStateArray );
    set_date_in_html_element( timeStateArray );


    set_cater_time_estimate( timeStateArray );
    change_html_elements_for_time(  timeStateArray );
    limit_select_times_by_date(  timeStateArray.timeFloor );
    ha.common.check_validations_state(  );
    
   // alert( "Done picking time." );
        
}, 300 );

function limit_datepicker_min_date( time_state_mode, floorDate )
{
    if( time_state_mode =='Cater' || time_state_mode == 'ClosedCater' ||  time_state_mode == 'Closed') 
    {
        $('.delivery_date').datepicker('option', {minDate: new Date(floorDate)});
    }
    else
    {
        $('.delivery_date').datepicker('option', {minDate: 0});   
    }
}


function set_cater_time_estimate(  timeStateArray )
{
    if(  timeStateArray.state == 'Cater' ||  timeStateArray.state == 'ClosedCater' )
        $('.est-delivery-time').text( '24+ hours' );
}

function set_time_in_html_element(  time_state_array )
{
	set_time_html( time_state_array.set_date_time );
}

function set_date_in_html_element( timeStateArray )
{
	set_date_html(   timeStateArray.set_date_time );
}

function get_min_early_entry(  date_time )
{
	var open_time = ha.common.getOpenTime(  );
	var now = new Date(  date_time );

	if( ha.common.is_empty( date_time ) )
		return open_time;
	else
	{
		vs_open = ha.common.compare_times( now, open_time  );
		if( vs_open == "<" )
			return open_time;
		else
			return now;
	}
}

// Figure out the base time. KEY FUNCTION.  This is the state manager function.  
function pick_time_state(  )
{
    var store_closed = check_if_closed(  );
    var store_catered = ha.common.checkBreadSimple(  );

   if( store_closed  == 1 &&   store_catered == 1  )
    {
        timeFloor = get_hours_cater_and_closed(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'ClosedCater';
    }

    else if(  store_closed == 1 &&  store_catered  == 0  )
    {
        timeFloor = get_hours_closed(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'Closed';
    }

    else if( store_closed == 0 &&  store_catered == 1  )
    {
        timeFloor = get_hours_cater(  );
        nowDisabled = true;
        nowHidden = true;
        state = 'Cater';
    }
    else
    {
        state = 'None';
        timeFloor = get_hours_regular(  );
        nowDisabled = false;
        nowHidden = false;
    }
    ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0 };
    return ret;
}

function handle_user_time_pick_first_run( tsa )
{
    // check done
    // validate done
    // store doen
    // delete
    // set params
 //   console.log( "USER PICK HANDLER: " + ha.common.getTimepicked(  ) );
    if( ha.common.getTimepicked(  ) == 0 )
        return;

//    console.log( "Checking validation of user time pick. " );
    valid_time_or_zero = get_valid_user_time_pick( tsa );

 //   console.log( "Checking validation of user time pick. " + valid_time_or_zero );
    validation_to_cookies( valid_time_or_zero );        // clear cookies, etc
    validation_to_global( valid_time_or_zero );

}

function pick_the_actual_time_to_set_in_datetime_picker( time_state_array )
{
    floor_time = time_state_array.timeFloor;
    user_time = ha.common.getUserTimeValidated(  );
    now_time = ha.common.getCurrentDateTime(  );
    
 //   console.log( "Picking..." );
 //   console.log( user_time );

    // FOR DEBUG::
  //  user_time = 0;

    if( user_time != 0 ){
   //     console.log( "User time" );
        return user_time;
    }
    else
    {
        if( floor_time != 0 )
        {
      //      console.log( "FLOOR TIME" );
            return noon_or_greater( floor_time );
        }
        else 
        {
     //          console.log( "NOW TIME" );
            return noon_or_greater( now_time );
        }
            //return 0;    // no user time set. no floor time set.  send zero ( which will mean NOW )
    }
}

function set_open_floor( time_state_array )
{
	if(  ha.common.is_empty( time_state_array.timeFloor ) )
		time_state_array.timeFloor = ha.common.getOpenTime(  );
	return time_state_array;
}

function figure_out_element_states( time_state_array )
{
	if(  time_state_array.state == "None" )
	{
		time_state_array.nowDisabled = false;
		if( ha.common.getUserTimeValidated(  ) != 0 )
		{
			time_state_array.nowHidden = true;
		}
		else
		{
			time_state_array.nowHidden = false;
		}
	}
	else
	{
		time_state_array.nowDisabled = true;
		time_state_array.nowHidden = true;

		if( time_state_array.state == 'Cater' || time_state_array.state == 'ClosedCater'  )
			time_state_array.msg = 'Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you! (3)';

		if( time_state_array.state == 'Closed'  )
		{
			var msg = "SORRY, WE&apos;RE CURRENTLY CLOSED. <br/>HOURS: " + ha.common.format_time( ha.common.getOpenTime(  ) )  + " - " + ha.common.format_time( ha.common.getClosedTime(  ) ) + " <br/>SELECT A FUTURE ORDER TIME BELOW.</span>";
			time_state_array.msg = msg;
		}

	}
	return time_state_array;

}

function change_html_elements_for_time( time_state_array )
{
    //console.log( 'NOW HIDDEN: ' + time_state_array.nowHidden );
    //    console.log( 'NOW DISABLED: ' + time_state_array.nowDisabled );

    if( time_state_array.nowHidden == true )   // HIDE now.
	{
        $("#radio1").prop( "checked", false );      // radio 1 is the now button
        $("#radio2").prop( "checked", true );       // radio 2 is the specific button
        $(".date-time-text").show();    

    }
    else        // SHOW now
    {
    	$("#radio1").prop( "checked", true );
        $("#radio2").prop( "checked", false );
        $(".date-time-text").hide();	
	}

	if( time_state_array.nowDisabled == false )   // is active
	{
        //$("#radio1").prop( "disabled", false );
        $('#radio1').data('err-msg', '' ); //setter
	}
	else
	{
        //$("#radio1").prop( "disabled", true );
        $('#radio1').data('err-msg', time_state_array.msg ); //setter
	}
}


function noon_or_greater( day_time )
{
    user_time = new Date( day_time );
    noon_time = new Date( day_time );
    return_time = new Date( day_time );

    noon_time = ha.common.set_date_time( noon_time, "12:00" );

    user_vs_noon = ha.common.compare_times( user_time, noon_time );
    
    if( user_vs_noon == ">" ){
        return_time = ha.common.set_date_time( day_time, user_time );
    }
    else{
        return_time = ha.common.set_date_time( day_time, "12:00" );
    }

    return return_time;
}


function validation_to_global( valid_time_or_zero )
{
    if(  valid_time_or_zero != 0 )
        VALID_USER_PICKED_RECENTLY = valid_time_or_zero;
    else
        VALID_USER_PICKED_RECENTLY = 0;
}

function get_valid_user_time_pick( timeStateArray )
{
    user_date = ha.common.getSpecificDate(  );
    user_time = ha.common.getSpecificTime(  );

    
    if( ha.common.is_empty( user_date ) == 1 || ha.common.is_empty( user_time ) == 1 )
    {
 //       console.log( "VALIDATION FAILURE" );
        validation = 0;
    }
    else
    {
        validation = validate_user_pick( user_date, user_time, timeStateArray );
  //      console.log( "VALIDATION SUCCESS: " + validation );
    }

    return validation;
}

function validation_to_cookies( valid_time_or_zero )
{
    // If we have a valid date then save it.
    ha.common.setUserTimeValidated( valid_time_or_zero );
    
    // If it's not valid or it's time to clear this information.
    ha.common.clearUserTimeTemp(  );
}



function validate_user_pick( date, time, timeStateArray )
{   
    cookie_date = ha.common.set_date_time( date, time );
//    console.log( "THE USER DATE: " + ha.common.format_date_time(  cookie_date  ) );
    
    date_cookie     = cookie_date;
    date_open       = ha.common.getOpenTime(  );
    date_closed     = ha.common.getClosedTime(  );
    date_current    = ha.common.getCurrentDayHour(  );
    date_floor      = timeStateArray.timeFloor;
    
    if(  ha.common.is_empty( date_floor ) == 1 )
    	date_floor = ha.common.getOpenTime(  );

    vs_date_floor  =  compareDates( date_cookie, date_floor );
    vs_time_floor  =  compareTimes( date_cookie, date_floor );
    
/*
    console.log( "%cDate Cookie:" + date_cookie, "color:blue" );
    console.log( "%cDate Floor:" + date_floor, "color:blue" );
    console.log( "%cDate Current:" + date_current, "color:blue" );

    console.log( "%cTest vs date floor", "color:blue" );
*/
    if( vs_date_floor == "<" )
        return 0;
    if( vs_date_floor == "=" )
    {
        if( vs_time_floor == "<" )
            return 0;
    }

    // first check if this is today or a future day...
    vs_current_date = ha.common.compare_dates( date_cookie, date_current );
    vs_open_time = compareTimes( date_cookie, date_open );
    vs_closed_time = compareTimes( date_cookie, date_closed );

//    console.log( "%cTest current date ", "color:blue" );
    if( vs_current_date == "<" )        // This is before today, so not works. 
        return 0;
    if( vs_open_time == "<" || vs_open_time == "=")
        return 0;
    if( vs_closed_time == ">" || vs_closed_time == "=" )
        return 0;

 //   console.log( "%cTest current times ", "color:blue" );
    var vs_current_time = '';
    if( vs_current_date == "=" )
    {
        vs_current_time = compareTimes( date_cookie, date_current );
        if( vs_current_time == "<" ){   // same day but before current time, so invalid. 
            return 0;}
    }

    if( vs_current_date == "?" || vs_open_time == "?" || vs_closed_time == "?" || vs_current_time == "?")
    {
        console.warn( "%cERROR in date validation", "color:teal; bakcground:black"  );
        return 0;
    }

    // If we get here, we are ok.
//    console.log( "%cWe are validated: " + ha.common.format_date_time( cookie_date ) , "color:teal"  );
    return cookie_date;
}


function account_for_user_time_pick(  )
{

    is_user_pick_valid  = 0;
    if( ha.common.getTimepicked(  ) == 1 ) 
    {
        is_user_pick_valid = validate_user_pick( timeFloor );
        //is_user_pick_valid = 1;
    }
    //console.log( "Testing user time pick: " +is_user_pick_valid );

    adduserpick =0;
    if(  is_user_pick_valid == 1 )
    {
        nowHidden = true;
        adduserpick = 'UserPick';
    }
    
    if( adduserpick == 'UserPick' )
        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":adduserpick, "userTime": set_date_time( ha.common.getSpecificDate(  ), ha.common.getSpecificTime(  ) ) };
    else
        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0 };
}




function setManual( time, floor )
{
     //$(".delivery_date").datepicker("option",{ minDate: new Date(floor)});
    
//    $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', time));
    setDatePicker( new Date( ha.common.getSpecificDate( ) ),  ha.common.getSpecificTime( ) );
}

function setDatepickerAndTimePicker( timeState )
{
    floorDate = timeState['timeFloor'];
    if( timeState == 'Closed' )
        overrideWithNoon = true;
    else
        overrideWithNoon = false;

    $(".delivery_date").datepicker("option",{ minDate: new Date(floorDate)});
    $(".delivery_date").datepicker('option', 'minDate', floorDate );
    $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', floorDate));
    
    if( overrideWithNoon == true )
        $("div.timechange select > option[value='12:00']").attr("selected", "selected");
    else
    {
        min_time = new Date( floorDate );
        hour = pad( min_time.getHours(), 2 );
        minute = pad( min_time.getMinutes(), 2 );
        $("div.timechange select > option[value='"  +hour +":" + minute + "']").attr("selected", "selected");
    }
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function compareTimes( t1, t2 )
{
   if( ha.common.is_empty( t1 ) == 1 )
        return undefined;

    if( ha.common.is_empty( t2 ) == 1 )
        return undefined;

    date1 = new Date( t1 );
    date2 = new Date( t2 );

    date_comp_1 = new Date( date1 );
    date_comp_2 = new Date( date1 );

    date_comp_1 = ha.common.blank_time( date_comp_1 );
    date_comp_2 = ha.common.blank_time( date_comp_2 );

    date_comp_1.setHours( date1.getHours(  ) );
    date_comp_1.setMinutes( date1.getMinutes(  ) );

    date_comp_2.setHours( date2.getHours(  ) );
    date_comp_2.setMinutes( date2.getMinutes(  ) );

    var result = "";
    if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
        result = ">";

    else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
        result = "<";
    
    else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
        result = "=";
    
    else
        result = "?";

//    console.log( "%cComparing TIMES - " + date_comp_1.getTime(  ) +" AKA " + ha.common.format_date_time( date_comp_1 ) + 
//        " vs " +  date_comp_2.getTime(  ) +" AKA " + ha.common.format_date_time( date_comp_2 ) + " Result: " + result, "color:teal" );
    return result;
}


function compareDates( d1, d2 )
{
    if( ha.common.is_empty( d1 ) == 1 )
            return undefined;

        if( ha.common.is_empty( d2 ) == 1 )
            return undefined;

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = ha.common.blank_time( date_comp_1 );
        date_comp_2  = ha.common.blank_time( date_comp_2 );

        date_comp_1.setHours( 0 );
        date_comp_1.setMinutes( 0 );
        date_comp_1.setSeconds( 0 );
        date_comp_1.setMilliseconds( 0 );

        date_comp_2.setHours( 0 );
        date_comp_2.setMinutes( 0 );
        date_comp_2.setSeconds( 0 );
        date_comp_2.setMilliseconds( 0 );


        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";
  		
//  		console.log( "%cComparing DATES - " + date_comp_1.getTime(  ) +" AKA " + ha.common.format_date_time( date_comp_1 ) + 
//        " vs " +  date_comp_2.getTime(  ) +" AKA " + ha.common.format_date_time( date_comp_2 ) + " Result: " + result  , "color:teal" );
        
        return result;
}

// Supplement to the ha_common date picker select event
function dmDaterPickerSelected(  )
{
    timestate = ha.common.getTimeStateArray( );

    currentSelected = new Date( $(".delivery_date").val( ) ); 
    currentSelected.setHours(0,0,0,0);
    
    if( !timestate.timeFloor )
        return;

    floorDate = new Date( timestate.timeFloor );

    floorDate.setHours(0);
    floorDate.setMinutes(0);
    floorDate.setSeconds(0);

    if( currentSelected <= floorDate )
        return -1;

    if( currentSelected == floorDate )
    {
        floorTime = new Date( timestate.floorDate );
        remove_from_time_select_before( floorTime );
    }

 ////   ha.common.setTimepicked( 1 );
  //  ha.common.setSpecificDateTimeFromElements(  );
}

function remove_from_time_select_before( theFloor )
{
    minTime = theFloor.getTime(  );
    
}

// Set the buttons - now and pick date/time
function setup_the_datepicker( timeState  )
{
    if( timeState )
    {

    }
    if( timeState['nowDisabled'] == true )
    {	
        $("#radio1").prop( "disabled", true );
    }
    else
    {
        $("#radio1").prop( "disabled", false );
    }
    if( timeState['nowHidden'] == true )
    {
        $(".date-time-text").show();
        $("#radio1").prop( "checked", false );
        $("#radio2").prop( "checked", true );
    }
    else
    {
        $(".date-time-text").hide();
        $("#radio1").prop( "checked", true );
        $("#radio2").prop( "checked", false );
    }
}

function get_time_from_date( date_time )
{
	if( typeof( date_time ) == "object" )
	{

    //    console.log( ha.common.pad( date_time.getHours(  ), 2 ) );
    //    console.log( ha.common.pad( date_time.getMinutes(  ), 2 ) );

 		result = ( ha.common.pad( date_time.getHours(  ), 2 ) + ":" + ha.common.pad(date_time.getMinutes(  ),2) );
//        console.log( "Result: " + result );
        return result;
	}
	else
	{
        var timeRegex = /([01]\d|2[0-3]):([0-5]\d)/;
        var match = timeRegex.exec( date_time );
        time = match[0];
        return time;
	}
}

function set_time_html( newTime )
{
//    console.log( "Setting new date and time " + ha.common.format_date_time( newTime ) );
    var d = new Date( newTime );
    var new_time = get_time_from_date(d)
    //console.log( "new time : " + new_time );
    
    $('.timedropdown option[value="' + new_time + '"]').prop('selected', true);

    if( $(".timedropdown").val(  )  == null || $(".timedropdown").val(  )  == "null"){
//        console.log( "NULL " );
        //$(".timedropdown option:first").attr('selected','selected');
    }

    //console.log( $(".timedropdown").val(  ) );
}

function set_date_html( newDate )
{
    d = new Date( newDate );
	  $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', d ));
}



function setDatePicker( newDate, newTime )
{   
        $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', newDate ));
        set_time_html( newTime );
}

function checkValidTime(  )
{
    if( ha.common.getTimepicked(  ) == 1 )
    {
        the_picked_date = ha.common.getSpecificDate(  );
        the_picked_time = ha.common.getSpecificTime(  );

        if( ha.common.getToday( ) == 1 && ha.common.get24HoursRequired( ) == 0)
        {pa
            return false;
        }
        return true;
    }
}


function roundMinutes( dateObject )
{
    now = dateObject;
    var mins = now.getMinutes();
    var quarterHours = Math.round(mins/15);
    if (quarterHours == 4)
    {
        now.setHours(now.getHours()+1);
    }   
    var rounded = (quarterHours*15)%60;
    now.setMinutes(rounded);
    return now;
}

function add24Hours( date )
{
    twenty_four_hours_later = new Date( date.getTime() + 60 * 60 * 24 * 1000 );   
    twenty_four_hours_later = roundMinutes( twenty_four_hours_later  );
    return twenty_four_hours_later 
}

function dateAdd( d, daysToAdd )
{
    if( !daysToAdd )
        daysToAdd = 1;
            
    result = new Date( d );
    result.setDate( result.getDate() + daysToAdd );
    return result;
}

function check_if_closed(  )
{
    openTime = ha.common.getOpenTime(  );
    closedTime = ha.common.getClosedTime(  );
    currentTime = ha.common.getCurrentDateTime(  );
    if( compareTimes( currentTime, openTime ) == '<' )
    {
    //    return 1;
    }

    if( compareTimes( currentTime, closedTime ) == '>' || compareTimes( currentTime, closedTime ) == "=" )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


function calcFloor(  )
{
    current = ha.common.getCurrentDayHour(  );
//    console.log( "  ! CURRENT: " +current );
    newDateMin = ha.common.date_plus_1( current );
//    console.log( "  ! PLUS 1: " +newDateMin );
    newDateMin = ha.common.set_date_time( newDateMin, ha.common.getOpenTime( ) );
   
     return newDateMin;
}

function test24HoursTooLate(  )
{
    closedTime = ha.common.getClosedTime(  );
    currentTime = ha.common.getCurrentDateTime(  );

    //console.log(  compareTimes( currentTime, closedTime )  );
    // if it's closed, basically, then it's too late to set it 24 hours, need 2 days.
    if( compareTimes( currentTime, closedTime ) == '>' )
        return 1;
    else
        return 0;
}

function getTimeFloor_closed_and_cater(  )
{
    // ADD SAT SUN CLOSED HERE.
    now = ha.common.getCurrentDayHour(  );
    newDateMin  = now;
    
    if(  check_if_closed(  ) == 1 )
    {
        newDateMin = calcFloor(  );
    }
    
    var check_24_hours_too_late = test24HoursTooLate(  );
    if( check_24_hours_too_late == 1 )
    {
        newDateMin = ha.common.date_plus_1( newDateMin );
    }
    else
    {
        // 24 hours from now is ok. since we are already tomorrow, set the time then for now, which would be 24 hours. 
         newDateMin = ha.common.set_date_time( newDateMin, now );
    }
    
    newDateMin = roundMinutes( newDateMin  );
    return newDateMin;
}


function getTimeFloor_closed_only(  )
{
    // ADD SAT SUN CLOSED HERE.
    newDateMin = ha.common.getCurrentDayHour(  );
    if(  check_if_closed(  ) == 1 )
    {
        newDateMin = calcFloor(  );
    }
       
    newDateMin = roundMinutes( newDateMin  );
    return newDateMin;
}


function getTimeFloor_cater_only(  )
{
    newDateMin = ha.common.getCurrentDayHour(  );
    
    if(  check_if_closed(  ) == 0 )
    {
        newDateMin = add24Hours( newDateMin );
    }
    else
    {
        return getTimeFloor_closed_and_cater(  )
    }
       
    newDateMin = roundMinutes( newDateMin  );
    return newDateMin;
}


function get_hours_cater(  )
{
    floor = getTimeFloor_cater_only(  );
    return floor;
}

function get_hours_closed(  )
{
    floor = getTimeFloor_closed_only(  );
    return floor;
}

function get_hours_cater_and_closed(  )
{
    floor = getTimeFloor_closed_and_cater(  );
    return floor;
}

function get_hours_regular(  )
{
    
    return 0;
}


function set_date_time( date, time  )
{
    day = new Date( date );
    time_split = time.split(/\:|\-/g);
    day.setHours(time_split[0]);
    day.setMinutes(time_split[1]);
    return day;
}


function disableNow( trueToDisable )
{
    ha.common.setNowDisabled( trueToDisable );
}

function dynamic_hide_date( trueToPickNow )
{
//    console.log( "TRUE TO PICK NOW: " +  trueToPickNow );
    function showNow(  )
    {
//    	console.log( "*+ SHOW NOW" );
        if( ha.common.getClosed(  ) == 1 )
        {
            showDatePicker( );
            return;
        }
        $(".date-time-text").hide();
        $("#radio1").prop( "disabled", false );
        $("#radio1").prop( "checked", true );
        $("#radio2").prop( "checked", false );
        $(".date-time-text").hide();
    } 

    function showDatePicker(  )
    {
        $("#radio1").prop( "checked", false );
        $("#radio2").prop( "checked", true );
        $(".date-time-text").show();

        $('.timechange').unbind('change').on('change', function () {
            if (ha.common.checkBreadType() == "error") {
                return false;
            }
            else
            {
                ha.common.setSpecificDateTime( $( '.delivery_date' ).val(  ) , $( '.timedropdown' ).val(  ) );
                
            }
        });
   
        time_state_array = ha.common.getTimeStateArray(  );
//        console.log(  " __TSA__ NOW disabled: " + time_state_array.nowDisabled );
        if(  ha.common.getNowDisabled( ) == 1 )
        {
            showDatePicker(  );
            return;
        }

        if ( trueToPickNow == true ) 
        {
            showNow(  );
        } 
        else
        {
            showDatePicker(  );
            ha.common.onSpecificDateCallavailTime( hideValue );
        }
    }

 



}
   function limit_select_times_by_date( floor_date_time )
    {
        //console.log( "%cENTER THE DATE LIMITER", "color:red" );

        var picked_date = new Date( $( ".delivery_date" ).val(  ) );
        var now_date = ha.common.getCurrentDateTime( );
        var open_time = ha.common.getOpenTime(  );
        var close_time = ha.common.getClosedTime(  );
        
        var floor_date_time = new Date( floor_date_time );

//        console.log( "%cPICKED DATE: " + ha.common.format_date_time( picked_date ), "color:red" );
        if( picked_date.getHours(  ) == 0 )
        {
            picked_date = ha.common.set_date_time( picked_date, ha.common.format_time( floor_date_time )  );
        }
        /*console.log( "%cNOW   DATE: " + ha.common.format_date_time( now_date ), "color:red" );
        console.log( "%cFLOOR : " + ha.common.format_date_time( floor_date_time ), "color:red" );

        console.log( "%cOPEN TIME: " + open_time, "color:red" );
        console.log( "%cCLOSE TIME: " + close_time, "color:red" );
    */

        if(  ha.common.is_empty(floor_date_time) == 1 )
        {
            //console.log( "%cNo floor, return", "color:red" );
            return;
        }
        
        if( compareDates( picked_date , floor_date_time ) == "<" )
        {
           // console.log( "Date picked is less than the floor." );
            ha.common.set_html_date( floor_date_time );
            limit_select_times_by_date( floor_date_time  );
            picked_date = $( ".delivery_date" ).val(  );
        }
        else if( compareDates( picked_date , floor_date_time ) == "=" )
        {
            //console.log( "%cDay match, loop through time and compare to open time and floor time.", "color:red" );
            

            $(".timedropdown option").each(function()
            {
                var index_time = $(this).val();
                var index_date_time = ha.common.set_date_time( picked_date, index_time );
                var vs_index_time_and_floor_time = compareTimes( index_date_time, floor_date_time );

//                console.log( "%cTime in question: " + index_time +" Compared with floor: " + ha.common.format_time( floor_date_time ) + " Result: " + vs_index_time_and_floor_time, "color:red" );
                if( vs_index_time_and_floor_time == "<" )
                {
                    //console.log( "Remove A - " + index_time + " is less than " + ha.common.format_time( floor_date_time ));
                    $( this ).remove(  );
                }

                var vs_index_time_and_closed_time = compareTimes( index_date_time, close_time );
                /*console.log( "%cTime in question: " + index_time +" Compared with close_time: " + ha.common.format_time( close_time ) +
                 " Result: " + vs_index_time_and_closed_time, "color:red" );*/

                if( vs_index_time_and_closed_time == ">" )
                {
                    //console.log( "Remove B - " + index_time + " is less than " + ha.common.format_time( close_time ));
                    $( this ).remove(  );
                }
            });
        }

        else if( compareDates( picked_date , floor_date_time ) == ">" )
        {
            $(".timedropdown option").each(function()
            {
                var index_time = $(this).val();
                var index_date_time = ha.common.set_date_time( picked_date, index_time );
               
                vs_index_time_and_open_time = compareTimes( index_date_time, open_time );
                if( vs_index_time_and_open_time == "<" )
                    {
                    //console.log( "Remove C - " + index_time + " is < than " + ha.common.format_time( open_time ));
                    $( this ).remove(  );
                }

                vs_index_time_and_closed_time = compareTimes( index_date_time, close_time );
                if( vs_index_time_and_closed_time == ">" )
                    {
                    //console.log( "Remove D - " + index_time + " is GREATER than " + ha.common.format_time( close_time ));
                    $( this ).remove(  );
                }
            });
            
        }

    }