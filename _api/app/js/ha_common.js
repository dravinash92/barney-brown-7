

ha.common = window.ha.common || {
    
    errChkVars: {
        choose_shop_type: false,
        choose_address: false,
        choose_date: false,
        choose_billing: false,
    },
    allow_date:true,
    zipcodes: [],
    product_extras: null,
    tempSlector: null,
    specific_exec : false,
    init: function() {
        var self = this;
        
        self.login();
        self.create_account();
        self.forgot_password();
        self.load_slides();
        self.fb_popup();
        self.cookie_login();
        self.common_button_click_events();
        self.add_standard_menu_item_cart();
        self.add_edit_user_review();
       
        if(window.location.href.indexOf('createsandwich') == -1){ 
        self.get_all_zip_codes();
        self.get_all_product_extra();
        }
        
        
       if (window.location.href.indexOf("checkout") !== -1 )
       {
            self.setCheckoutFirstRun(  );
       }
        
        
        if(window.location.href.indexOf('sandwich') != -1 && window.location.href.indexOf('gallery') != -1 ){ 
        	$('.sandwich-gallery-left input[type="checkbox"]').prop('checked', false);
        	//alert("test");
        }
        
        cookieAddCartData = ha.common.getCookie('addCartData');
        isFbOnlyUser      = ha.common.getCookie('facebookOnlyUser');
       // alert(cookieAddCartData);
        if(isFbOnlyUser == "1"){ 
        if(cookieAddCartData != "" && cookieAddCartData != "false"){
        	
            session = ha.common.check_session_state();
            if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                   ha.common.login_popup($(this));
                 } else {
                   ha.common.facebook_login($(this));
                 }
                return;
            }
        	outJSon = JSON.parse(cookieAddCartData);
        	if(outJSon.type == 'reorder'){
        		
        		$(window).load(function(){ 
        			$("a[data-order='"+outJSon.sandwichId+"']").trigger("click");
        		});
        		 
        		
        	} else {
        		$(window).load(function(){ 
        			if(outJSon.type == 'product') ftype = 'product'; else ftype = 'user_sandwich';
        			if(!outJSon.uid) uid = SESSION_ID; else uid = outJSon.uid;							
					ha.sandwichcreator.add_sandwich_to_cart(outJSon.sandwichId,uid,ftype, outJSon.qty); 
        		});
        	} 
        	ha.common.setCookie("addCartData",'false',1);
        } }
        
        
        self.myaccount_reorder();
        self.order_view_details();
        $("#loginForm_cookie").submit(function(e) {
            e.preventDefault();
        });
        $("#loginForm").submit(function(e) {
            e.preventDefault();
        });
        
        $init_prior = false;
        if(window.location.href.indexOf('createsandwich') == -1){ 
        	$init_prior = self.check_out_autoSelect();
        }

         
        if( $init_prior == false )   
            self.common_init(  );
               
        self.lazy_load_sandwiches();
        
        
       

        if (self.getCookie("sandwich_end_screen") == "true") {
         
            session = ha.common.check_session_state();
            if (session.session_state == false) {
            }
       
        }
        self.enable_friend_sandwiches();
        self.show_friends_popup_onload();
        self.show_sandwich_detail_onload();
        self.popup_info_checkout();
        self.lazyLoadMenuSandwiches();
        self.checkout_time_drop_down();


        self.look_for_date_change(  );
        self.checkTimingsRealtedConditions(  );

        self.getTimeAvilableSlots(  );
        check_time_and_change_if_needed(  ); // init..
        self.check_validations_state();
    },
  
    checkout_time_drop_down:function(){
        $(document).on('change', ".timedropdown", function() {
           
        });
        
    },
     popup_info_checkout : function(){
		
    	var self = this;
    	setTimeout(function(){			
			$('.tip-image img').hover(function(){							
				pos = $(this).offset();
				//$('.tip-image-info').css('left':pos,'top':pos.top);
				$('.tip-image-info').offset({ top: pos.top , left: pos.left-220});
				$('.tip-image-info').show();
				$('.tip-image-info .close-button').unbind('click').on('click',function(){
					$('.tip-image-info').hide();
				});
			},function(){   });
    	
		}, 3000);
    	setTimeout(function(){			
			$('.tip-image img').hover(function(){							
				pos = $(this).offset();
				//$('.tip-image-info').css('left':pos,'top':pos.top);
				$('.tip-image-info').offset({ top: pos.top , left: pos.left-220});
				$('.tip-image-info').show();
				$('.tip-image-info .close-button').unbind('click').on('click',function(){
					$('.tip-image-info').hide();
				});
			},function(){   });
    	
		}, 200);
		
		$('.tip-image img').hover(function(){			
				
			pos = $(this).offset();
			//$('.tip-image-info').css('left':pos,'top':pos.top);
			$('.tip-image-info').offset({ top: pos.top , left: pos.left-220});
			$('.tip-image-info').show();
			$('.tip-image-info .close-button').unbind('click').on('click',function(){
				$('.tip-image-info').hide();
			});
		},function(){   });
    },

    show_sandwich_detail_onload: function() {
        $(window).ready(function() {
            sandwichId = ha.sandwichcreator.get_url_params("galleryItem");
            if (window.location.href.indexOf('gallery') != -1 && sandwichId) {

                sandwichId = parseInt(sandwichId);
                $.ajax({
                    type: "POST",
                    data: {
                        'id': sandwichId
                    },
                    url: SITE_URL + 'sandwich/get_individual_sandwich_data/',
                    async: false,
                    success: function(data) {
                        json = JSON.parse(data);
                       
                        if (json.state == false) {
                            alert("Sorry sandwich you are looking for has been removed!")
                        } else {
                            $("#sandwich-view").show();
                            $("#sandwich-view .title-holder h1").text(json.sandwich_name);
                            $("#sandwich-view .title-holder h2").text(json.sandwich_price);
                            $("#sandwich-view p:first").text(json.sandwich_desc);
                            $("#sandwich-view img").attr('src', json.imagepath);
                            $("#sandwich-view img").attr('width', 350);
                            $("#sandwich-view img").attr('height', 194);
                            $("#sandwich-view .title2-holder .username").text(json.user_name);
                            $("#sandwich-view .title2-holder .datefromat").text(json.date_of_creation);
                            $("#sandwich-view .title2-holder .likescount").text(json.like_count);
                            $("#sandwich-view .title2-holder .menuadds").text(json.menu_add_count);
                            $("#sandwich-view .save-menu").attr('rel', json.id);
                        }

                    }
                });
            }
        });

    },


    //friend sandwiches 
    enable_friend_sandwiches: function() {
        var self = this;
        
        $('.connect-color').unbind('click').on('click', function(e) {
           e.preventDefault();
           text = $(this).text();
           
           
         
           if(text.indexOf("FRIENDS' SANDWICHES") != -1 && $(this).attr('id') != 'fbLogin_alt' ) { 
        	  
            session = ha.common.check_session_state();
            if (session.session_state == false) {
                ha.common.setCookie("reopen_fb_pop", true, 1);
                ha.common.facebook_login();
                return;
            }
			else if(session.facebook_only_user == false)
                {
                	self.show_friends_popup();
                	self.set_friends_popup_data();
                } 
			else {
                valid_toc = self.check_valid_User_AccessToken(true);
                if (valid_toc.state == true) {
                    self.show_friends_popup();
                    self.set_friends_popup_data();
                }
				else {
                     ha.common.setCookie("reopen_fb_pop", true, 1);
                    self.facebook_login();
                }
            }
           }

        });
    },

   show_friends_popup: function() {
        $("#facebook-friends-list").show();
    },


    set_friends_popup_data: function() {
		
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'menu/getFBfriendsMenu',
            success: function(data) {
                $('body').css('overflow', 'hidden');
                $("#facebook-friends-list ul").html(data);
                ha.sandwichcreator.enable_scrolbar();
                //on clicking back to menu button on friends menu pop-up
                $('.back-to-menu').on('click',function(e){
                	e.preventDefault();
                	$('.close-button').trigger('click');
                });
            }
        });

    },

    show_friends_popup_onload: function() {
		
        var self = this;
        if (window.location.href.indexOf("menu") != -1) {
            $(document).ready(function() {
                
                if (self.getCookie("reopen_fb_pop") == "true") {

                    valid_toc = self.check_valid_User_AccessToken(true);
                    if (valid_toc.state == true) {
                        self.show_friends_popup();
                        self.set_friends_popup_data();
                    }
                    self.setCookie("reopen_fb_pop", false, 1);
                }
            });
        }  else {
        	self.setCookie("reopen_fb_pop", false, 1);
        }
    },
	
	//Lazy loading for User Menu Sandwiches
	lazyLoadMenuSandwiches : function(){
 
		var self = this; var set = 0;
		$(window).load(function(){
			if(window.location.href.indexOf("menu") != -1 && window.location.href.indexOf("friends_menu") == -1){   	
				$(window).scroll(self.menu_bind_scrolls);
			}
		});
    },
    
    Menu_More_Item : true,    
	
	menu_bind_scrolls         : function(){    		
		var self = this;
		//filter Scroll Prevention
		if(ha.common.Menu_More_Item == true && ha.common.Ajax_busy == false){
			
			ha.common.load_more_menu_sandwiches();	
		} 	
   	 
    },
    
    load_more_menu_sandwiches : function(){
    	
		var self = this;
		self.Ajax_busy = true;
		var noScroll = false;
		
		totalMenuitems = $('input[name="countMenuItems"]').val();

		 
		$('.loadMoreGallery').show();
		$current_count = $(".menu-listing li[data-id]").length;	
		 
		
		if(totalMenuitems == $current_count) return false;
			 
		if( parseInt($current_count)){ 
			$.ajax({
					type: "POST",
					data:{ "count" : $current_count-1 },
					url: SITE_URL+'menu/lazyLoadMoreUserSandwich',
					success: function(e){
						if(e.trim() != "None"){
							$('.menu-listing li:last').after(e);						
							self.Ajax_busy = false;
							$('.loadMoreGallery').hide();
							ha.sandwichcreator.listing_click();
							self.add_standard_menu_item_cart();
						}else{
							ha.common.Menu_More_Item	= false;
							$('.loadMoreGallery').hide();
						}	
					}
			}); 
			
		} else {  $('.loadMoreGallery').hide(); } 
   		 
    },
	

    check_valid_User_AccessToken: function() {
        var self = this;

        var out;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'myaccount/getUserAccessToken',
            async: false,
            dataType: 'json',
            success: function(data) {
                out = data;
            }
        });
        return out;
    },

    //check_session_state

    check_validations_state: function() {
        var self = this;
        if (window.location.href.indexOf("checkout") == -1) return;
        var i = 0;

        var payment_Form = $('#_payment_form').serializeArray();
/*
        console.log( "-----------------------" );
       console.log( 'address selected val: ' + $('input[name="address_selected"]').val() ) ;
       console.log( 'billing selected val: ' + $("#changeBilling").val()  ) ;
       console.log( 'payment_Form: ' );
       console.dir( payment_Form );
*/

       var temporary_card_entered = false;
       var address_selected = false;
       var billing_selected = false;

       if( payment_Form[1].value )  
       {
            temporary_card_entered = true;
       }
       else{
            temporary_card_entered = false;
       }

       if ( $('input[name="address_selected"]').val( ) == undefined )       {
            address_selected = false;
       }
       else{
            address_selected = true;
       }

        var change_billing_val = $("#changeBilling").val();
  
        if ( change_billing_val == undefined || change_billing_val == -1 || change_billing_val == 0 ) {
            billing_selected = false;
        }
        else {
            billing_selected = true;
        }

        if( ( billing_selected == false && temporary_card_entered == false ) || address_selected == false )
            enable_purchase_button = false;
        else
            enable_purchase_button = true;

        if ( enable_purchase_button == true ) {
          $('.place-order').removeClass('disabled');
            $('.place-order').addClass('link');
        } else {
              $('.place-order').addClass('disabled');
            $('.place-order').removeClass('link');

            
        }
		
		applyDiscount();
    },

    get_all_product_extra: function() {
        
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'sandwich/getAllproductsExtras/',
            async: false,
            dataType: 'json',
            success: function(data) {
                self.product_extras = data;
            }
        });

    },

    lazy_load_sandwiches: function() {

        var self = this;
        var set = 0;
        $(window).load(function() {
            if (window.location.href.indexOf("gallery") != -1) {
                $(window).scroll(self.bind_scrolls);
            }
        });
    },

    Ajax_busy: false,

    bind_scrolls: function() {

        var $current_count = parseInt($('.menu-listing li').length);
        var $total_items = parseInt($('input[name="total_item_count"]').val());

        if ($('.menu-listing li').length < 20) {
            if ($(window).scrollTop() > 500) {
                if (ha.common.Ajax_busy == false && $('input[name="search"]').length == 0) ha.common.load_more_sandwiches();
            }
        } else {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                setTimeout(function() {
                    if (ha.common.Ajax_busy == false && ($current_count < $total_items) && $('input[name="search"]').length == 0) {
                        ha.common.load_more_sandwiches();
                    }
                }, 300);
            }
        }
    },

   load_more_sandwiches: function() {

        var self = this;
        var Array = {};

        Array['BREAD'] = [];
        Array['PROTEIN'] = [];
        Array['CHEESE'] = [];
        Array['TOPPINGS'] = [];
        Array['CONDIMENTS'] = [];
        $txt = $(this).next().text();
        $txt = $txt.trim();
       //$txt = $txt.replace(/ /g, "##"); commenting for AJAX search.
        parent = $(this).parent().parent();
        $key = $(parent).prev('h3').text()
        $key = $key.trim();
        $key = $key.toUpperCase();



        switch ($key) {

            case 'BREAD':

                if ($(this).prop('checked') == true) {
                    if ($.inArray($txt, Array['BREAD']) == -1) {
                        Array['BREAD'].push($txt);
                    }
                } else {
                    if ($.inArray($txt, Array['BREAD']) != -1) {
                        Array['BREAD'].splice($.inArray($txt, Array['BREAD']), 1);
                    }
                }
                break;

            case 'PROTEIN':
                if ($(this).prop('checked') == true) {
                    if ($.inArray($txt, Array['PROTEIN']) == -1) {
                        Array['PROTEIN'].push($txt);
                    }
                } else {
                    if ($.inArray($txt, Array['PROTEIN']) != -1) {
                        Array['PROTEIN'].splice($.inArray($txt, Array['PROTEIN']), 1);
                    }
                }
                break;

            case 'CHEESE':
                if ($(this).prop('checked') == true) {
                    if ($.inArray($txt, Array['CHEESE']) == -1) {
                        Array['CHEESE'].push($txt);
                    }
                } else {
                    if ($.inArray($txt, Array['CHEESE']) != -1) {
                        Array['CHEESE'].splice($.inArray($txt, Array['CHEESE']), 1);
                    }
                }
                break;

            case 'TOPPINGS':
                if ($(this).prop('checked') == true) {
                    if ($.inArray($txt, Array['TOPPINGS']) == -1) {
                        Array['TOPPINGS'].push($txt);
                    }
                } else {
                    if ($.inArray($txt, Array['TOPPINGS']) != -1) {
                        Array['TOPPINGS'].splice($.inArray($txt, Array['TOPPINGS']), 1);
                    }
                }
                break;

            case 'CONDIMENTS':
                if ($(this).prop('checked') == true) {

                    if ($.inArray($txt, Array['CONDIMENTS']) == -1) {
                        Array['CONDIMENTS'].push($txt);
                    }
                } else {
                    if ($.inArray($txt, Array['CONDIMENTS']) != -1) {
                        Array['CONDIMENTS'].splice($.inArray($txt, Array['CONDIMENTS']), 1);
                    }
                }
                break;

        }
        
       
        var sortBy = $('.sortby').val();
        var $string = JSON.stringify(Array);
        
        
        self.Ajax_busy = true;
        $('.loadMoreGallery').show();
        $current_count = $('.menu-listing li').length;
        $total_items = $('input[name="total_item_count"]').val();
        sort_id = $('input[name="sort_type"]').val();

        if (parseInt($current_count) < parseInt($total_items)) {
            $.ajax({
                type: "POST",
                data: {
                    "count": $current_count,
                    'filter' : $string ,
                    'sortBy' : sortBy,
                    "sort_id": sort_id
                },
                url: SITE_URL + 'sandwich/get_more_gallery',
                success: function(e) {
                    $('.menu-listing li:last').after(e);
                    ha.sandwichcreator.listing_click();
                    self.Ajax_busy = false;
                    $('.sandwitchitemCount').text($('.menu-listing li').length + " Creations");
                    $('.loadMoreGallery').hide();
                }
            });
        } else {
            $('.loadMoreGallery').hide();
        }

    },

    init_date_picker: function(  )
    {
        //console.log( "init date picker, OVERRIDES?" );
        $(".delivery_date").datepicker({
            showOn: "button",
            buttonImage: siteurl + "/app/images/date-picker.png",
            buttonImageOnly: true,
            buttonText: "Select date",
            defaultDate: new Date(),
            minDate: 0,
            dateFormat: 'D, M d, yy'
        });
    },


    // look for date change in checkout page
    look_for_date_change: function() {

        var self = this;
		
		$("select.select_pickup").unbind("click");

        $(document).on('change', "select.select_pickup", function() {
            var add_id = $(this).val();
            var isCurrentDay = $("#selected_date").val();
            var selectedDay = $("#selected_day").val();
            var currentTime
            
            /*
            if ($('input[name="radiog_lite"]:checked').val() == "now") {
                currentTime = $("input[name='todays_time']").val();
            } else {
                currentTime = $('.timechange select').val();
            }
            */
           
            if (selectedDay == null || selectedDay == '') selectedDay = $('input[name="todays_date"]').val();

            $("#hidden_store_id").val(add_id);
            if (add_id > 0) {
                self.getAddressInfo(add_id);
               
            }
            
            self.updatePickupOrDeliveryInCart(0);
            self.check_current_condition_for_time();
            self.popup_info_checkout();            
            
           
            // DM 
            // if( DO_NOT_MESS_WITH_THE_TIME_BUTTON !== true )
            //    $("#radio1").trigger("click");
        });
        

        $(".select_address").unbind("change");
        $(document).on('change', "select.select_address", function() {	
            self.check_current_condition_for_time();
            self.popup_info_checkout();
             check_time_and_change_if_needed(  );     
        });


        //console.log("coming here", self.getCookie("specific_date_sel"));
        var default_load_date = new Date();
        
        if(self.getCookie("specific_date_exists") == 1){
           setTimeout(function(){
                // DM 
                // $("#radio1").attr("checked", false);
                // $("#radio2").attr("checked", true);
                // $("#radio2").trigger("click");
                //check_time_and_change_if_needed(  )
                var default_load_date = self.getCookie("specific_date_sel");
                 /*$( ".delivery_date" ).datepicker( "setDate", new Date(default_load_date.substring(4)));
                 $(".timedropdown").val(self.getCookie("specific_date_time_sel"));
                 $(".checkout-msg-red").hide();*/
              }, 6000);
            
        }

        //console.log( "address change -  date picker, OVERRIDES?" );
        $(".delivery_date").datepicker({
            showOn: "button",
            buttonImage: siteurl + "/app/images/date-picker.png",
            buttonImageOnly: true,
            buttonText: "Select date",
            defaultDate: new Date(),
            minDate: 0,
            dateFormat: 'D, M d, yy',
            

        });
		
		$( ".delivery_date" ).datepicker( "setDate", new Date());
              
        $( '.date-time-text' ).on( 'change' , function( e )
        { 

            //console.log( "CHANGE" );
            if( e.which )
            {
                //console.log( "ACTUAL CLICK " );
            }
            else
            {
                //console.log( "FAKE CLIKC " );
            }
            ha.common.setTimepicked( 1 );
            ha.common.setSpecificDateTimeFromElements(  );
            self.check_current_condition_for_time();
        });
        
        $('.delivery_date').datepicker('option', 'onSelect', function( selectedDate )  
        { 
				
                        ha.common.setTimepicked( 1 );
                 ha.common.setSpecificDateTimeFromElements(  );

				if(self.checkBreadType() == 'error'  ){
            		return false;
            	}
					
				currentdate = new Date().setHours( 0, 0, 0, 0 );
                var selected_date = new Date(selectedDate);
                
                var isCurrentDay = $("#selected_date").val();
                var dayNames = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                var selectedDay = dayNames[selected_date.getDay()];
                var options = getTimings(selectedDate);
                $("select.delevry_time").html(options);
                dateSel = selectedDate;
                $("#selected_day").val(selectedDay);
                var add_id = $("#hidden_store_id").val();
                if (add_id == "") {
                    var add_id = $("#hidden_default_store_id").val();
                }
                var is_delivery = -1;
                if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
                   // return true;
                } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
                    is_delivery = 0;
                }


                var isCurrentDay = $("#selected_date").val();
                var selectedDay = $("#selected_day").val();
                var currentTime;


                if( isCurrentDay === 1  )
                {

                    self.setToday( 1 );
                    if ($('input[name="radiog_lite"]:checked').val() == "now") {
                        currentTime = $("input[name='todays_time']").val();
                    } else {
                        currentTime = $('.timechange select').val();
                    }
                }
                else
                {
                    self.setToday( 0 );
                    currentTime = '09';
                }
                
                
			  ctimeDt  = self.getCurrentTimJs();
	 

			if(  (selected_date.getDate() == ctimeDt.getDate()) &&  (selected_date.getMonth() == ctimeDt.getMonth()) && (selected_date.getFullYear() == ctimeDt.getFullYear()) ) {
				 currentTime = ctimeDt.getHours() + ":" + ctimeDt.getMinutes(); 
			} else {
				currentTime =  $('.timechange select').val();
			}
			$.ajax({
	            type: "POST",
	            dataType: 'json',
	            data: {
	                "current_day": dateSel,
	                "current_time": currentTime,
	                "store_id": add_id,
	                "selectedDay": selectedDay,
	                "isCurrentDay": isCurrentDay
	            },
	            url: SITE_URL + 'checkout/changeTimeOnStoreSelection',
	            success: function(e) {
	               
	            	var message  = e.message;
	            	if(message !=''){ //console.log("message:"+data.message);
	            		 $('.checkout-msg-red').remove();
	                     var msg = "<span class='checkout-msg-red'>"+e.message+"</span>";

                        //console.log( "Disabling the place order button because message exists "  );

	                     $("div.delivery-details-wrapper div.date-time > p").after(msg);
                         $(".place-order").addClass("disabled");
	            	}
	            	else{ 
	                if (e == "Closed") {

	                    e = "<div class='shopclosemsg' style='padding: 5px 0px 5px 10px'>Close</div>";
	                }
	                $(".timechange").html("");
	                $(".timechange").html(e.html);
	                
	                date_string = new Date($('input[name="updatedDate"]').val());
	                $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', date_string));
	                if(ha.common.getCookie("specific_date_exists") == 1)
                    {
                        self.setCookie("specific_date_sel",ha.common.getCookie("specific_date_sel"), 1);
                    }else{
                        self.setCookie("specific_date_sel",$(".delivery_date").val(), 1);
                    }
	            	}setTimeout(function () {
                		$(".place-order").removeClass("disabled");
                    }, 500); 
	            }
                

	        });
			self.checkTimingsRealtedConditions(selectedDate);
			self.onSpecificDateCallavailTime(  );
            
            self.check_current_condition_for_time();
            check_time_and_change_if_needed( );
        
		}
        );	
        
        dmDaterPickerSelected(  );
         
        $('.delivery_date').click(function(){
        $('.delivery_date').datepicker('show');
        });

    },

    set_html_date( date )
    {
        d = new Date(  date  );
        $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', d));
    },
    
	getCurrentTimJs : function(){
		var self = this;
		servtime  = self.getServerTime();
                servtime  = servtime.split(",");
		servtime[1] = parseInt(servtime[1]-1);
        return new Date(servtime[0],servtime[1],servtime[2],servtime[3],servtime[4],servtime[5] );
	},
	
	getAlphaDay : function(day){
		var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        return days[day];
	},
	
	getAlphaMonth : function(month){
		var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		return monthNames[month];
	}, 
	
   onSpecificDateCallavailTime : function( target_date ){
		// this is called after click on one or the other date/time radio buttons.
      //  return;
	    var self = this;
        
        return;

        if( !target_date )
		  target_date  = self.getCurrentTimJs();
		
        // THIS is today
		datepickerDate = new Date( $( '.delivery_date' ).datepicker( "getDate" ) );
   

        // THIS IS WHERE IT GETS THE Thu, June 29, 2017
		var current_day = self.getAlphaDay( datepickerDate.getDay() )+", "+self.getAlphaMonth(datepickerDate.getMonth())+" "+datepickerDate.getDate()+", "+datepickerDate.getFullYear();

        

		var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		selectedDay = $("#selected_day").val();
		$("#selected_day").val(selectedDay);
		var add_id = $("#hidden_store_id").val();
		if(add_id=="")
		{
			add_id=$("#hidden_default_store_id").val();
		}
		
		var is_delivery = -1;
		var order_type = $("#order-type").val();
		if(order_type){
			//return true;
			is_delivery  = 1;
		} else {
			is_delivery  = 0;
		}
		
	 
		 var isCurrentDay = 1;
		 var selectedDay  = $("#selected_day").val();
		 var currentTime;
		 currentTime = currentDate.getHours() + ":" + currentDate.getMinutes();
		
		if(currentDate.withoutTime() < datepickerDate.withoutTime()){
			
			dateSel          = days[datepickerDate.getDay()];
			
			$obj = { "isDelivery": is_delivery , "current_time" : currentTime , "current_day" : current_day , "store_id" : add_id , "selectedDay" : dateSel , "isCurrentDay" : 0  };
		}else{
			
			dateSel          = days[currentDate.getDay()];
			$obj = { "isDelivery": is_delivery , "current_time" : currentTime , "current_day" : current_day , "store_id" : add_id , "selectedDay" : dateSel , "isCurrentDay" : isCurrentDay  };	
		}
        self.setCurrentDate( current_day );
		
	},

    getAddressInfo: function(add_id) {
        $.ajax({
            type: "POST",
            data: {
                "store_id": add_id
            },
            url: SITE_URL + 'checkout/pickupAddressInfo',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) {
                $(".checkoutdynamicdiv").html(e);
                bindCheckoutPageEvents();
                ha.common.check_validations_state();
            }
        });
    },

    setDateChanged: function( dateChanged )
    {
        ha.common.setCookie( "date_changed", dateChanged );
    },

    getDateChanged: function(  )
    {
          if( ha.common.getCookie("date_changed") )
            return ha.common.getCookie("date_changed");
        else
            return 0;

    },


    setToday: function( today )
    {
        ha.common.setCookie( "today",today );
    },

    getToday: function(  )
    {
        if( ha.common.getCookie("today") )
            return ha.common.getCookie("today");
        else
            return 0;
    },

    setDelivery: function( trueForDelivery )
    {
        ha.common.setCookie( "is_delivery",trueForDelivery );
    },

    getDelivery: function(  )
    {
          if( ha.common.getCookie("is_delivery") )
            return ha.common.getCookie("is_delivery");
        else
            return 0;

    },

    clearUserTimeTemp : function(  )
    {
//        console.log( "_____ CLEAR TIME TEMP" );
        this.setTimepicked( 0 );
        this.setSpecificDateTime( 0 );
    },

    clearUserTimeValidated : function(  )
    {
//        console.log( "_____ CLEAR TIME VALIDATED" );
        this.setUserTimeValidated( 0 );
    },

    setUserTimeValidated: function( user_date )
    {
        //console.log( "_____ SET USER VALIDATED TIME: " );
        //console.log(  user_date );
        if( user_date != 0  )
            date_pack = JSON.stringify(  user_date );
        else
            date_pack = 0;
        this.setCookie( "user_date_validated", date_pack , 1);
    },

    getUserTimeValidated: function(  )
    {
        if( ha.common.getCookie("user_date_validated") )
            return JSON.parse( ha.common.getCookie("user_date_validated") );
        else
            return 0;
    },



    setSpecificDateTimeFromElements: function(  )
    {
        
        this.setTimepicked( 1 );
        ha.common.setCookie( "newDate", $( ".delivery_date" ).val(  ) );
        ha.common.setCookie( "newTime", $('.timedropdown').val(  ) );

    },

    setSpecificDateTime: function( newDate, newTime )
    {
        if( newDate == 0 ){
            ha.common.setCookie( "newDate", 0,1 );
            ha.common.setCookie( "newTime", 0, 1);
        }
        else
           ha.common.setSpecificDateTimeFromElements(  );
       
    },

    getSpecificDate: function(  )
    {
        if( ha.common.getCookie("newDate") )
            return ha.common.getCookie("newDate");
        else
            return 0;
    },

    getSpecificTime: function(  )
    {
        if( ha.common.getCookie("newTime") )
            return ha.common.getCookie("newTime");
        else
            return 0;
    },

    setOpenTime: function ( openTime )
    {
        //console.log( "_____ SET OPENED TIME: " + openTime );
        ha.common.setCookie( "open_time", openTime );
    },

    setClosedTime: function ( closedTime )
    {
        //console.log( "_____ SET CLOSED TIME: " + closedTime );
        ha.common.setCookie( "closed_time", closedTime );
    },

    setCurrentTime: function ( time_set )
    {
//        console.log( "*------------ SET CURRENT TIME - " + this.formatDateBB( time_set ) +" " +this.formatTimeBB( time_set ) );

         ha.common.setCookie( "current_time", JSON.stringify( time_set ) );
    },

    getCurrentDateFromServer: function(  )
    {
        $.ajax({
                    type: "POST",
                    url: SITE_URL + 'checkout/getCurrentHour/',
                    async: false,                   
                    success: function(data) {
                        currentDayHour = JSON.parse(data);
                    }
                });
                

                
                var currentHour = parseInt(currentDayHour.hour);
                var currentDay = parseInt(currentDayHour.day);
                var currentDate = currentDayHour.date_time_js;

                currentDate = currentDate.split(",");
                var current_date = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);
              
                return current_date;
    },
    getCurrentTime: function (  )
    {
        if( ha.common.getCookie("current_time") ){
            return JSON.parse(  ha.common.getCookie("current_time") );
        }
        else
        {
            return this.getServerTime(  );
        }
        
    },

    getCurrentDayHour: function(  )
    {
        date = this.getCurrentDateFromServer(  );

        date = new Date( date );
        

//        console.log( "IMPORTANT!!! OVERRIDE HERE TO TEST STORE CLOSING!!" );
//        date.setHours( 22 );

        return date;
    },

    getCurrentDateTime: function(  )
    {
        date = this.getCurrentDayHour(  );
        return date;
    },



    format_date: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var date_string = self.getAlphaDay(date.getDay())+", "+self.getAlphaMonth(date.getMonth())+" "+date.getDate()+", "+date.getFullYear();
        return date_string;
    },

    format_time: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var time_string = this.pad( date.getHours(), 2) + ":" + this.pad( date.getMinutes(), 2);
        return time_string;
    },

    format_date_time: function( d )
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date_string = this.format_date( d );
        time_string = this.format_time( d );

        return date_string + " " + time_string;
    },

    compare_times: function( d1, d2 )
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date1 = new Date( d1 );
        date2 = new Date( d2 );

        date_comp_1 = new Date( date1 );
        date_comp_2 = new Date( date1 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2 = this.blank_time( date_comp_2 );

        date_comp_1.setHours( date1.getHours(  ) );
        date_comp_1.setMinutes( date1.getMinutes(  ) );
        date_comp_1.setSeconds( date1.getMinutes(  ) );
        date_comp_1.setMilliseconds( date1.getMinutes(  ) );

        date_comp_2.setHours( date2.getHours(  ) );
        date_comp_2.setMinutes( date2.getMinutes(  ) );
        date_comp_2.setSeconds( date2.getMinutes(  ) );
        date_comp_2.setMilliseconds( date2.getMinutes(  ) );

        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) == date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

//        console.log( "%cComparing TIMES - " + this.format_date_time( date_comp_1 ) + " vs " + this.format_date_time( date_comp_2 ) + " Result: " + result, "color:teal" );
        return result;
    },

    get_date_difference( start_date, end_date )
    {
        var date1 = new Date( start_date );
        var date2 = new Date( end_date ); //less than 1
        var start = Math.floor(date1.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var end = Math.floor(date2.getTime() / (3600 * 24 * 1000)); //days as integer from..
        var daysDiff = end - start; // exact dates
        return daysDiff;
    },


    blank_time: function( d )
    {
        d = new Date(  d );
        d.setHours( 0 );
        d.setMinutes( 0 );
        d.setSeconds( 0 );
        d.setMilliseconds( 0 );
        return d;
    },

    compare_dates: function( d1, d2 )
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2  = this.blank_time( date_comp_2 );


        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";


        return result;
    },

    set_date_time : function( date, time  )
    {
        time_split = time.split(/\:|\-/g);
        day = new Date( date );
        day.setSeconds( 0 );
        day.setMilliseconds( 0 );

        day.setHours( time_split[0] );
        day.setMinutes( time_split[1] );
        
        return day;
    },


    pad : function (num, size) 
    {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    },


    is_empty: function( thing )
    {
        if( thing == 'undefined'    ||
            thing == undefined      ||
            thing == null           ||
            thing == ''             ||
            thing === false )
            return 1;
        else
            return 0;
    },
    date_plus_1 : function( d )
    {
        var date = new Date( d );
        date.setDate( date.getDate() + 1 );
        return date;
    },
    

    getClosedTime: function (  )
    {
         if( ha.common.getCookie("closed_time") )
            return ha.common.getCookie("closed_time");
        else
        {
            this.checkTimingsRealtedConditions(  );
            return this.getCookie("closed_time");
        }
        
    },

    getOpenTime: function( ) 
    {
         
         if( ha.common.getCookie("open_time") )
            return ha.common.getCookie("open_time");
        else
        {
            this.checkTimingsRealtedConditions(  );
            return this.getCookie("open_time");
        }
    },

    setNowDisabled : function( trueToDisableNow )
    {

        ha.common.setCookie( "trueToDisableNow", trueToDisableNow );
    },


    getNowDisabled : function(   )
    {

         if( ha.common.getCookie("trueToDisableNow") == true )
            return 1;
        else
            return 0;
    },


    setCurrentDate : function( currentDate )
    {

        ha.common.setCookie( "current_date",currentDate );
    },

    getCurrentDate : function(  )
    {
        if( ha.common.getCookie("current_date") ){
              current_date = ha.common.getCookie("current_date");
              var day = new Date(current_date);
            var current_day = this.getAlphaDay(day.getDay())+", "+this.getAlphaMonth(day.getMonth())+" "+ ( day.getDate() ) +", "+day.getFullYear();
            return current_day;
        }
        else
        {
            var day = new Date();
            var current_day = this.getAlphaDay(day.getDay())+", "+this.getAlphaMonth(day.getMonth())+" "+ ( day.getDate() ) +", "+day.getFullYear();
            return current_day;
        }
    },

    getCurrentDatePlus1 : function(  )
    {
            var result = new Date( );
            result.setDate( result.getDate() + 1 );
         
            var current_day = this.getAlphaDay(result.getDay(  ) )+", "+this.getAlphaMonth(result.getMonth())+" "+ ( result.getDate() ) +", "+result.getFullYear();
            return current_day;
    },


    pad : function (num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    },

    formatTimeBB: function( d )
    {
        if( !d ) 
            return;

        day = new Date( d );
        hour = this.pad( day.getHours(), 2 );
        minute = this.pad( day.getMinutes(), 2 );

        return hour +":"+minute;
    }, 

    formatDateTimeBB: function( datetime )
    {
        return(  this.formatDateBB( datetime ) + " " + this.formatTimeBB( datetime ) );
    },

    formatDateBB: function( d )
    {
        self = this;
        day = new Date( d );
        if( !day ) 
            return;

        return(  self.getAlphaDay(day.getDay())+", "+self.getAlphaMonth(day.getMonth())+" "+day.getDate() +", "+day.getFullYear() );
    },
    getNoon: function( )
    {

    },


    setCheckoutFirstRun : function(  )
    {
        ha.common.setCookie( "checkout_first_run", 1 );
    },

    getCheckoutFirstRun : function(  )
    {
        if( ha.common.getCookie("checkout_first_run") )
            return ha.common.getCookie("checkout_first_run");
        else
            return 0;
    },

    clearCheckoutFirstRun : function(  )
    {
         ha.common.setCookie( "checkout_first_run", 0 );
    },


    setUserLastPick : function(  wasUser )
    {
        ha.common.setCookie( "last_pick", wasUser );
    },

    getUserLastPick : function(   )
    {
         if( ha.common.getCookie("last_pick") )
            return ha.common.getCookie("last_pick");
        else
            return 0;
    },

    setTimeStateArray: function ( timestate )
    {
//        console.log( "*------------ TIMESTATE ARRAY -  state: " + timestate['state'] +" Disabled/Hidden: " + timestate['nowDisabled'] +"/"+timestate['nowHidden'] +
//            " Floor date: " + this.formatDateBB( timestate.timeFloor ) +" " +this.formatTimeBB( timestate.timeFloor ) );
        ha.common.setCookie( "time_state_array", timestate );
    },

    getTimeStateArray: function (  )
    {
         if( ha.common.getCookie("time_state_array") )
            {
                tsa = ha.common.getCookie("time_state_array");
               return tsa;
            }   
        else{
            default_ts_array = {'state':'None', 'timeFloor':new Date(  ), 'nowDisabled':false, 'nowHidden':false};
            return default_ts_array;
        }
    },

    setNotEnough: function ( notEnough )
    {

        ha.common.setCookie( "not_enough",notEnough );
    },

    getNotEnough: function ( notEnough )
    {
        if( ha.common.getCookie("not_enough") )
            return ha.common.getCookie("not_enough");
        else
            return 0;
    },

    setTimepicked: function ( timePicked )
    {

        ha.common.setCookie( "time_picked",timePicked );
    },

    getTimepicked: function ( )
    {
        if( ha.common.getCookie("time_picked") )
            return ha.common.getCookie("time_picked");
        else
            return 0;
    },

    set24HoursRequired: function ( is24Hours )
    {
         ha.common.setCookie( "requires_24_hours",is24Hours);
    },

    get24HoursRequired: function ( )    
    {
        if( ha.common.getCookie("requires_24_hours") )
            return ha.common.getCookie("requires_24_hours");
        else
            return 0;
    },

    setOpenHour: function( hour )
    {

        ha.common.setCookie( "open_hour", hour);
    },

    setCloseHour: function( hour )
    {

        ha.common.setCookie( "close_hour", hour);
    },

     getCloseHour: function ( )    
    {
        if( ha.common.getCookie("close_hour") )
            return ha.common.getCookie("close_hour");
        else
            return 0;
    },

    getOpenHour: function ( )    
    {
        if( ha.common.getCookie("open_hour") )
            return ha.common.getCookie("open_hour");
        else
            return 0;
    },


    setClosed: function( isClosed )
    {
        ha.common.setCookie( "store_is_closed", isClosed);
    },

    getClosed: function( )
    {
        if( ha.common.getCookie("store_is_closed") )
            return ha.common.getCookie("store_is_closed");
        else
            return 0;
    },

    //~getting server time using async Ajax call.
    getServerTime : function(){
                
        var dateTime;
        var self = this;
        var currentDayHour = 0;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentHour/',
            async: false,
            success: function(data) {
                 $json =  JSON.parse(data);
                
                if($json.date_time_js){
                    dateTime = $json.date_time_js;
                }
                
            }
        });
        return dateTime;
    },

    dumpTimeParams: function(  )
    {
        return;
        
    },

    getTimeAvilableSlots: function(oBj) {
        time_list = this.get_all_possible_times(  );
         $(".timechange").html("");
         current_value = $(".timechange").val();
        $(".timechange").html(
            '<select class="timedropdown">' +
            time_list +
            "</select>"
            );
        set_time_html( current_value );

        return;
            
        
        if(!ha.common.specific_exec)
        {
    		var self = this;
    		if(!oBj.current_day){
    			var currentDate = new Date();
    			var current_day = self.getAlphaDay(currentDate.getDay())+", "+self.getAlphaMonth(currentDate.getMonth())+" "+currentDate.getDate()+", "+currentDate.getFullYear();
    			oBj.current_day = current_day;
    		}else {
			
    			var dateChoosen = new Date(oBj.current_day);			
    			if(self.getCookie("specific_date_sel")=='')
                {
                var currentDate = new Date(); 
                }else{
                     var currentDate = new Date(self.getSpecificDate(  ));   
                }
			
			if(dateChoosen.getFullYear() < currentDate.getFullYear()){	
				
				var current_day = self.getAlphaDay(currentDate.getDay())+", "+self.getAlphaMonth(currentDate.getMonth())+" "+currentDate.getDate()+", "+currentDate.getFullYear();
				oBj.current_day = current_day;
			}
		}
		
        $.ajax({
            type: "POST",
            dataType: 'json',
            data: {
                "current_day": oBj.current_day,
                "current_time": oBj.current_time,
                "store_id": oBj.store_id,
                "selectedDay": oBj.selectedDay,
                "isCurrentDay": oBj.isCurrentDay
            },
            url: SITE_URL + 'checkout/changeTimeOnStoreSelection',
            success: function(e) {
                
                var message  = e.message;
            	if(message !='')
                { 
                    console.warn("message:"+data.message);
            		

                     $('.checkout-msg-red').remove();
                     var msg = "<span class='checkout-msg-red'>"+e.message+"</span>";
                     $(".place-order").addClass("disabled");
                     $("div.delivery-details-wrapper div.date-time > p").after(msg);
            		
            	}

            	else{ 
                if (e == "Closed") {
                    self.setClosed( 1 );
                    e = "<div class='shopclosemsg' style='padding: 5px 0px 5px 10px'>Close</div>";
                }
                $(".timechange").html("");
                $(".timechange").html(e.html);
            
                if(ha.common.getCookie("specific_date_exists") == 1)
                    {
                        self.setCookie("specific_date_sel",ha.common.getCookie("specific_date_sel"), 1);
                    }else
                       self.setCookie("specific_date_sel",$(".delivery_date").val(), 1);
            	}
            	setTimeout(function () {
            		//$(".place-order").removeClass("disabled");
                }, 500); 
                ha.common.specific_exec = true;
            }
        });
    }
    },

     tConvert: function (time)
      {
     
       // Check correct time format and split into components
       time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) 
        { // If time format correct
            
          time = time.slice (1);  // Remove full string match value
          time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
          time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        //return time; // return adjusted time or original string
          return time[0] + ":" + time[2] + time[5];
      },

    get_all_possible_times(  )
    {
        buffer = '';
       for( hour = 8; hour < 24; hour ++ )
       {
            for( minute = 0; minute <60; minute = minute + 15  )
            {
                time_text = this.pad( hour, 2 ) + ":" + this.pad( minute, 2);
                time_new = this.tConvert( time_text );
                buffer = buffer + '<option value="' + time_text  + '">' +  time_new +  '</option>';
            }
       }
       return buffer;
     },

    check_out_autoSelect: function() {
        var self = this;
        var dataObj = null;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getLastOrder/',
            async: false,
            dataType: 'json',
            success: function(data) {
                dataObj = data;
            }
        });

        if (dataObj) $dat = dataObj.Data;
        else $dat = null;

        var $init_prior = false;

        if ($dat)
        {
            //console.log( "PRIOR" );
            $init_prior = true;
            if ($dat.delivery == 1)
            {
                setTimeout(function() {
                    $('a:contains(DELIVERY)').trigger('click');
                    setTimeout(function() {
                        $('.select_address').val($dat.address_id).trigger('change');

                        if( $('#changeBilling').length>0 )
                        {
                          $('#changeBilling > option:eq(2)').prop( 'selected',true );
                        }



                        if($('#changeBilling option[value='+$dat.billinginfo_id+']').length>0)
                        {
                            $('#changeBilling').val($dat.billinginfo_id).prop( 'selected', true );
                            $('#changeBilling').val($dat.billinginfo_id).trigger('change');
                        }
                        else
                        {
                            $('#changeBilling').val(-1).trigger('change');    
                        }
                        ha.common.check_validations_state();
                        self.check_current_condition_for_time();
                        $('input[name="delivery_store_id"]').val($dat.store_id);
                        $("#hidden_store_id").val($dat.store_id);

                    }, 1000);
                }, 1000);
            }
            else
            {
                //console.log( "4" );
                setTimeout(function() 
                {
                  //  console.log( "5" );
                    $('a:contains(PICK-UP)').trigger('click');
                    setTimeout(function()
                    {
                    //    console.log( "6" );
                        $('.select_pickup').val($dat.address_id).trigger('change');
                        self.getAddressInfo($dat.address_id);
                        if($('#changeBilling option[value='+$dat.billinginfo_id+']').length>0){
                        $('#changeBilling').val($dat.billinginfo_id).trigger('change');
                        }
                        else{
                        $('#changeBilling').val(-1).trigger('change');    
                        }
                        ha.common.check_validations_state();
                        self.check_current_condition_for_time();
                        $("#hidden_store_id").val($dat.store_id);
                        self.updatePickupOrDeliveryInCart(0);
						self.check_current_condition_for_time();
						
//                        check_time_and_change_if_needed(  );
                    }, 1000);
                }, 1000);
            }
            
            if( self.getUserTimeValidated(  ) == 0  && self.getTimepicked(  ) == 0)
                $($("input[name='radiog_lite'] ")[0]).trigger("click");
        return true;
        }
       
        return false;
    },
    
     common_init: function(  )
    {
        var self = this;
        //console.log( "COMMON INIT" );

        if( $('#changeBilling').length > 0 )
        {
            $('#changeBilling > option:eq(1)').prop( 'selected',true );
        }
        else
        {
            $('#changeBilling').val(-1).trigger('change');    
        }
        
        ha.common.check_validations_state();
        self.check_current_condition_for_time(  );
        
//        check_time_and_change_if_needed(  );
        if( self.getUserTimeValidated(  ) == 0  && self.getTimepicked(  ) == 0)
            $( $("input[name='radiog_lite'] ")[0] ).trigger("click");

    },

    getTimeFromTimeChangeElement: function(  )
    {
        var time_val = $('div.timechange select').val();
        if( time_val == undefined || time_val == 'undefined' ){
            time_val = "12:00PM";
        }
        var time_only = time_val.replace(" PM", "");
        time_only = time_val.replace(" AM", "");
        time_only = time_only.split(":");
    },


    get_store_timeslot_ajax: function( dayOfWeekName, address_id, delivery_pickup )
    {
        

       return  $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentStoreTimeSlot/',
            async: false,
            data: {
                deliveryOrPickup: delivery_pickup,
                id: address_id,
                day: dayOfWeekName
            },
            success: function( ajax_response ) {
                if(ajax_response == "false")
                {
                    alert("Delivery not available in this area");
                    return false;

                }
                else
                {
                    store_timing = JSON.parse(ajax_response);
                    self.timeslot_information = store_timing ;
                    return   store_timing;
                }
                
            }
        });
        
    },

    checkTimingsRealtedConditions:  function( selectedDate = undefined )
    {
        var self = this;
//        console.dir( $("input.address_selected") );
        if( selectedDate == undefined )
        {
            selectedDate = $( ".delivery_date" ).val(  );
        }

        var today_Date = new Date ( self.getCurrentDateFromServer(  ) );
        var today_DayOfWeek = today_Date.getDayName(  );
      
        var is_delivery = -1;

        if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
            is_delivery = 1;
            self.setDelivery( 1 );
        } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
            is_delivery = 0;
            self.setDelivery( 0 );
        }   
        
        var is_delivery = 1;    // DEFAULT ONLY DELIVERY IN THIS VERSION
        
      
            if ( is_delivery == 0 )
            {
                 deliveryOrPickup = "pickup";
                var pickupStoreId = $("input.pickup_address_id").val();
                address_id = pickupStoreId;
            }
            else
            {
                deliveryOrPickup = "delivery";
                var deliveryAddressId = $("input.address_selected").val();
                address_id = deliveryAddressId;
            }
            
            
            var store_timeslot_response = self.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
            
            store_timeslot_response_text = JSON.parse( store_timeslot_response.responseText  );
        
            
            this.setOpenTime( this.set_date_time(  today_Date,  store_timeslot_response_text['open'] ) );
            this.setClosedTime( this.set_date_time(  today_Date,  store_timeslot_response_text['close'] ) );
    
            this.setOpenHour( store_timeslot_response_text['open'] );
            this.setCloseHour( store_timeslot_response_text['close'] );
            
            if( store_timeslot_response_text == false || store_timeslot_response_text == null ){
                return false;
            };

            $("#hidden_store_id").val( store_timeslot_response.store_id ); 
               
	}, 

    check_current_condition_for_time : function(  )
    {

    },

	checkNowStoreIsOpened: function()
    {
	},
	
	checkForAllConditionsPlaceOrderButton: function(){
		setTimeout(function(){
			var i = 0;
			
			if(ha.sandwichcreator.vars.appliedDiscount){
				$("#apply_discount").val(ha.sandwichcreator.vars.appliedDiscount);
				applyDiscount();
			}
			
			if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
                
				if($(".check-out-left ul .less-than-10").length > 0){				
				}else{					
					i++;
				}	
				
            } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
                //$(".place-order").removeClass("disabled");
				i++;
            }
			
			if($("#changeBilling").val() == -1 || $("#changeBilling").val() == 0 || $("#changeBilling").val() == null){
				i = 0;
			}

//            console.log( "Disabling the place order button: - " + i );
			if(i > 0){
				$(".place-order").removeClass("disabled");
				$(".place-order").addClass("link");
			}
			
		},600)
		
	} ,

    get_all_zip_codes: function() {
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'myaccount/getAllzipcodes/',
            async: false,
            dataType: 'json',
            success: function(data) {
                self.zipcodes = [];
                Object.keys(data).forEach(function(e) {
                    self.zipcodes.push(data[e]);
                });
            }
        });
    },


    getStore_id_using_zip: function(zip) {
        var self = this;
        var store_id;
        $.ajax({
            type: "POST",
            data: {
                'zip': zip
            },
            url: SITE_URL + 'myaccount/getStoreFomZip/',
            async: false,
            dataType: 'json',
            success: function(data) {
                if (data[0]) {
                    store_id = data[0].store_id;
                } else {
                    store_id = 0;
                }
            }
        });

        return store_id;
    },

    
    common_button_click_events: function() {
        var self = this;
        //facebook login poup
        $(".login-facebook").unbind('click').on('click', function(e) {
            e.preventDefault();
            self.facebook_login();
        });
        //login popup
        $(".login").unbind('click').on('click', function(e) {
        	$(".error_msg").empty();
            e.preventDefault();
            self.login_popup();
        });
        // user login popup
        $(".user-login").unbind('click').on('click', function(e) {
        	$(".error_msg").empty();
            e.preventDefault();
            self.user_login_popup();
        });
        //forgot password
        $(".forgot-password").unbind('click').on('click', function(e) {
        	$(".error_msg").empty();
            e.preventDefault();
            self.forgot_password_popup();
        });
        //create account
        $(".create-account,.create-account-btn").unbind('click').on('click', function(e) {
        	$(".error_msg").empty();
            e.preventDefault();
            self.register_popup();
        });

        $('.account-profile,.shopping-cart, .deliver-button').unbind('click').on('click', function(e) {
        	$(".error_msg").empty();
            e.preventDefault();
            session = ha.common.check_session_state();
            if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                   ha.common.login_popup($(this));
                 } else {
                   ha.common.facebook_login($(this));
                 }
                return;
            } else {
                window.location.href = $(this).attr('href');
            }
        });

    },

    //clicking on account review submit button 
    add_edit_user_review: function() {
		
        var self = this;

        $("a#submit_review").unbind("click").on("click", function() {

            var form = $("form#user_review");
            var how_know = $("form#user_review [name=how_know]").val();
            var rating_order = $("form#user_review [name=rating_order]").val();
            var rating_customer_service = $("form#user_review [name=rating_customer_service]").val();
            var rating_food = $("form#user_review [name=rating_food]").val();
            var message = $("form#user_review [name=message]").val();

            var userdata = form.serialize();
            var userdataArray = form.serializeArray();

            userdataArray.forEach(function(a) {

                if (a.value > 0) {
                    parent = $("select[name='" + a.name + "']").parent();
                    ptag = $(parent).prev();
                    $(parent).hide(0);
                    $(ptag).hide(0);
                }
            });


			if(message){
				$.ajax({
					type: "POST",
					data: userdata,
					url: SITE_URL + 'myaccount/addUserReview/',
					beforeSend: function() {
						$("#ajax-loader").show();
					},
					complete: function() {
						$("#ajax-loader").hide();
					},
					success: function(data) {

						if (data) {
							//alert("Your review saved. Thanks.");
							$( ".review-submit" ).remove();
							$( "<p class='review-submit'>Your review saved. Thanks.</p>" ).insertBefore( "#submit_review" );                                           	
							document.getElementById("user_review").reset();

						}
					}
				});
			}else{
				
				$( ".review-submit" ).remove();
                $( "<p class='review-submit'>Please Write Review before submiting.</p>" ).insertBefore( "#submit_review" );	
			}

        });

    },

    //add item to cart 
    add_standard_menu_item_cart: function() {
		
        var self = this;
        $(".common_add_item_cart").unbind('click').on('click', function(e) {
						
            e.preventDefault();
            $data = $(this).data();
            ob = $(this);
			
			liData = $(this).parent().parent().data();
					
            prev = $(this).parent().prev();
            input = $(prev).find('input[name="spinnerinput"]');
            qtyInp = $(input).val();
            if (qtyInp) qty = qtyInp;
            else qty = 1;
			

			if($data.sandwich == 'product'){
				
				 ha.sandwichcreator.add_sandwich_to_cart($data.sandwich_id, $data.uid, $data.sandwich, qty, $(this));
			} else if (liData.toast == 0) {
				
                ha.sandwichcreator.add_sandwich_to_cart($data.sandwich_id, $data.uid, $data.sandwich, qty, $(this));
            } else {
				
                ha.sandwichcreator.add_toast_sandwich_to_cart($data.sandwich_id, liData.toast, $data.uid, $data.sandwich, qty, $(this));
            }
        });

    },

    login_popup: function(clkitem) {
        self = this;
        self.tempSlector = clkitem;

        if ($('#login-form-cookie').length > 0) $('#login-form-cookie').show();
        else { $("#login").show();  
        
        
        if(clkitem){
        	
        	if(clkitem.hasClass('save-menu')) return;
			isProd = clkitem.hasClass('common_add_item_cart');
			
			data   = clkitem.data();
			//console.log(data);
			
			var qty = 1;
			
			if(data.sandwich == "product") prdChk = true;  else prdChk = false;
			if(isProd && prdChk){
				type       = 'product'; 
				sandwichId = data.sandwich_id;
													
				prev = $(clkitem).parent().prev();
				input = $(prev).find('input[name="spinnerinput"]');
				qtyInp = $(input).val();
				if (qtyInp) qty = qtyInp;
				else qty = 1;
				
			} else if(clkitem.hasClass('reorder-button')){
				
				type       = 'reorder'; 
				sandwichId = data.order;
				
			} else {
				
				type       = 'sandwich';
				if(data.sandwich_id){
					sandwichId = data.sandwich_id
				} else {
				    
				   sandwichId = $(clkitem.parent()).find('input[name="hidden_sandwich_id"]').val();
				}
			}
            
			uid  = ha.common.getCookie('user_id');
			
			if( type && sandwichId){ 
			finalDobject = { 'uid' : '' , 'type' : type , 'sandwichId' : sandwichId, "qty" : qty   }
			
			ha.common.setCookie("addCartData",JSON.stringify(finalDobject),1);
			 }
        }
         
       }
    },

    facebook_login: function(clkitem) {
        self = this;
		$("#facebook-login").show();
		
        if(clkitem){
        	
        	if(clkitem.hasClass('save-menu')) return;
			isProd = clkitem.hasClass('common_add_item_cart');
			
			data   = clkitem.data();			
			var qty = 1;
			
			if(data.sandwich == "product") prdChk = true;  else prdChk = false;
			if(isProd && prdChk){
				type       = 'product'; 
				sandwichId = data.sandwich_id;
													
				prev = $(clkitem).parent().prev();
				input = $(prev).find('input[name="spinnerinput"]');
				qtyInp = $(input).val();
				if (qtyInp) qty = qtyInp;
				else qty = 1;
				
			} else if(clkitem.hasClass('reorder-button')){
				
				type       = 'reorder'; 
				sandwichId = data.order;
				
			} else {
				
				type       = 'sandwich';
				if(data.sandwich_id){
					sandwichId = data.sandwich_id
				} else {
				    
				   sandwichId = $(clkitem.parent()).find('input[name="hidden_sandwich_id"]').val();
				}
				 
			}
		
			uid  = ha.common.getCookie('user_id');
			
			
			if(uid && type && sandwichId){ 
			finalDobject = { 'uid' : uid , 'type' : type , 'sandwichId' : sandwichId, "qty" : qty   }
			
			ha.common.setCookie("addCartData",JSON.stringify(finalDobject),1);
			 }
        }
        
    },

    user_login_popup: function() {
        self = this;
        $("#login").hide();
        $("#forgot-form").hide();
        if ($('#login-form-cookie').length > 0) $('#login-form-cookie').show();
        else $("#login-form").show();
    },

    forgot_password_popup: function() {
        self = this;
        $("#login").hide();
        $("#forgot-form").show();
    },

    register_popup: function() {
        self = this;
        $("#login").hide();
        $("#regiter-form").show();
    },

    blank_time: function( d )
    {
        d = new Date(  d );
        d.setHours( 0 );
        d.setMinutes( 0 );
        d.setSeconds( 0 );
        d.setMilliseconds( 0 );
        return d;
    },

    date_plus_1 : function( d )
    {
        var date = new Date( d );
        date.setDate( date.getDate() + 1 );
        return date;
    },



    set_date_time : function( date_passed, time_passed  )
    {
        if( this.is_empty(time_passed ) || time_passed == null )
        {
            return date_passed;
        }

        if( typeof(  time_passed  ) == "object" ) 
        {
            time_string_composer = this.pad( time_passed.getHours(  ) , 2 ) + ":" + this.pad( time_passed.getMinutes(  ), 2 );
            time_passed = time_string_composer;
        }
        else
        {
            if(  time_passed == undefined  ){
                time_passed = '12:00';
            }
            if(  !time_passed  ){
                time_passed = '12:00';
            }
            if(  time_passed == undefined  ){
                time_passed = '12:00';
            }
            if(  time_passed == 'null'  ){
                time_passed = '12:00';
            }

            var timeRegex = /([01]\d|2[0-3]):([0-5]\d)/;
            var match = timeRegex.exec( time_passed );
            time_passed = match[0];
        }
        time_split = time_passed.split(/\:|\-/g);
        
        new_day = new Date( date_passed );
        new_day.setSeconds( 0 );
        new_day.setMilliseconds( 0 );

        new_day.setHours( time_split[0] );
        new_day.setMinutes( time_split[1] );

        return new_day;
    },


    login: function() {

        $("#login_btn").on('click', function(e) {
            e.preventDefault();
            
            if($('#login-form').css('display') == 'none')
			{
				return;
			}
            
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            $(".register").removeClass('register');
            $(".error_msg").html('');
            var lemail = $("#lemail").val();
            var lpwd = $("#lpwd").val();

            if (lemail == "") {

                $("#lemail").addClass('register').focus();
                $(".error_msg").html("Please enter a valid email address");
            } else if (!filter.test(lemail)) {
                $("#lemail").addClass('register').focus();
                $(".error_msg").html("Please enter a valid email address");


            } else if (lpwd == "") {

                $("#lpwd").addClass('register').focus();
                $(".error_msg").html("Please enter a password");
            } else if (lpwd.length < 6) {
                $("#lpwd").addClass('register').focus();
                $(".error_msg").html("Your password must be at least 6 characters long");
            } else {
                $.post(SITE_URL + "myaccount/loginCheck", $("#loginForm").serialize(),
                    function(data, status) {
                        data = JSON.parse(data);
				
                        if (data.state == "true") {
							var tempClick = 0;
                            var accounturl;
                            loc = window.location.href;
                            arr = loc.split(SITE_URL);
                            accounturl = SITE_URL + arr[1].replace('#', '');
                            
                           if (self.tempSlector != null) {
                            	
                            	className  = $(self.tempSlector).attr('class');
                            	IdName     = $(self.tempSlector).attr('id');
                            	text       = $(self.tempSlector).text();
                            	if(className) className  = className.toLowerCase(); else className = '';
                            	if(IdName) IdName     = IdName.toLowerCase(); else IdName = '';
                            	if(text) text       = text.toLowerCase(); else text = '';
                            	
                            	$(self.tempSlector).trigger('click');
		                    	self.tempSlector = null;
                            	
                             if(className.indexOf('cart') != -1 || IdName.indexOf('cart') != -1 || text.indexOf('cart') != -1)
                              {
                            	    var intV = setInterval(function(){ 
   			                    	 
   			                    	if( ha.sandwichcreator.tmpTransAnim == 1 ){ 
   			                    	  window.location = accounturl;
   			                    	  clearInterval(intV);
   			                    	}else {
   			                    		window.location = accounturl; 
     			                    	clearInterval(intV); 
   			                    	}
   			                    },10);
                            	 
                              }	 else {
                            	  $(".popup-wrapper").hide()
                            	  window.location = accounturl;
                              }
		                         
                                   
                            } else {
                            	$(".popup-wrapper").hide()
		                		 window.location = accounturl;
		                	}
									
                        } else {
                           $(".error_msg").html(data.msg);
                        }
                    }
                );
            }
        });
    },

    cookie_login: function() {
		
        var self = this;
        $("#login_btn_cookie").on('click', function(e) {
            e.preventDefault();
            
            if($('#login-form-cookie').css('display') == 'none')
			{
				return;
			}

            var lemail = $("#lemail_cookie").val();
            var lpwd = $("#lpwd_cookie").val();


            if (lpwd == "") {
                $("#lpwd_cookie").addClass('register').focus();
                $(".error_msg").html("Please enter a password");
            } else if (lpwd.length < 6) {
                $("#lpwd_cookie").addClass('register').focus();
                $(".error_msg").html("Your password must be at least 6 characters long");
            } else {

                $.post(SITE_URL + "myaccount/loginCheck", $("#loginForm_cookie").serialize(),
                    function(data, status) {
                        data = JSON.parse(data);
                        locationSplit = window.location.href.split("/")
						if(locationSplit.indexOf("choosedeliveryOrPickup")!=-1){
							window.location.reload();
						}

                        if (data.state == "true") {
                            if ($(".create-sandwich-wrapper").length == 2) {
                                $('.create-sandwich-menu').hide();
                                $('.create-sandwich-right-wrapper').hide();
                                $("#login-form-cookie").hide(0, function() {
                                    var accounturl;
                                    loc = window.location.href;
                                    arr = loc.split(SITE_URL);
                                    accounturl = SITE_URL + arr[1].replace('#', '');
                                    window.location = accounturl;
                                });
                            } else {
								
								 if (self.tempSlector != null) {
                            	
                            	className  = $(self.tempSlector).attr('class');
                            	IdName     = $(self.tempSlector).attr('id');
                            	text       = $(self.tempSlector).text();
                            	if(className) className  = className.toLowerCase(); else className = '';
                            	if(IdName) IdName     = IdName.toLowerCase(); else IdName = '';
                            	if(text) text       = text.toLowerCase(); else text = '';
                            	
                            	$(self.tempSlector).trigger('click');
		                    	self.tempSlector = null;
                            	
                             if(className.indexOf('cart') != -1 || IdName.indexOf('cart') != -1 || text.indexOf('cart') != -1)
                              {
                            	    var intV = setInterval(function(){ 
   			                    	 
   			                    	 if( ha.sandwichcreator.tmpTransAnim == 1 ){ 
   			                    	  window.location = accounturl;
   			                    	  clearInterval(intV);
   			                    	 }
   			                    },10);
                            	 
                              }	 else {
                            	  $(".popup-wrapper").hide()
                            	  window.location = accounturl;
                              }
		                         
                                   
                            } else {
                            	$(".popup-wrapper").hide()
		                		 window.location = accounturl;
		                	}

                            }
                        } else {
                            $(".error_msg").html(data.msg);
                        }
                    }
                );

            }
        });

    },

    setCookie: function(cname, cvalue, exdays, path) {
		
        var d = new Date();
	    if(!path) path = "/";
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    var path    =  "path="+path; 
	    document.cookie = cname + "=" + cvalue + "; " + expires+";  "+path+";"; 
	    
    },

    getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0){ 
				var string = c.substring(name.length, c.length);
				string = decodeURI(string);
				string = string.replace("+", " ");
				return string;
			}
        }
        return "";
    },

    check_session_state: function() {
    	var self = this;
        var result;
        $.ajax({
            async: false,
            type: "POST",
            data: {"user_id": SESSION_ID},
            url: SITE_URL + "myaccount/is_session",
            dataType: 'json',
            success: function(e) {
                result = e;
               
            },
            fail: function( e )
            {
                console.warn( e );
            }
        });
        
        if(result.userAvail == 0){
        	alert("User has been removed!");
        	if($('.login-name-holder a').attr('href')){ 
        	window.location.href = $('.login-name-holder a').attr('href');
        	}
        	return
        }
        
        return result;
    },

    forgot_password: function() {

        $("#forgot_btn").unbind('click').unbind('keyup').on('click keyup', function(e) {
        	e.preventDefault();
            $(".register").removeClass('register');
            $(".error_msg").html('');
            var femail = $("#femail").val();
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (femail == "") {

                $("#femail").addClass('register').focus();
                $(".error_msg").html("Please enter a valid email address");

            } else if (!filter.test(femail)) {
                $("#femail").addClass('register').focus();
                $(".error_msg").html("Please enter a valid email address");
            } else {
                $.ajax({
                        type: "POST",
                        url: SITE_URL + "myaccount/forgotPassword",
                        data: $("#forgotForm").serialize()
                    })
                    .done(function(data) {
                        data = JSON.parse(data);
                         
                        if (data.state == false) {
                            $(".error_msg").html(data);
                            $( ".f-link-email-send" ).remove();
                            $( "<p class='f-link-email-send'>"+data.msg+"</p>" ).insertAfter( "#femail" );
                        }
                        
                        if (data.state == true) {
                        	$( ".f-link-email-send" ).remove();
                        	$( "<p class='f-link-email-send'> A link has been sent to your email id to reset the password.</p>" ).insertAfter( "#femail" );
                        }

                    });
            }
        });
        $("#femail").on('keypress', function(e) {
        	if(e.keyCode==13)
        		{
        	            e.preventDefault();
        	            $(".register").removeClass('register');
        	            $(".error_msg").html('');
        	            var femail = $("#femail").val();
        	            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        	            if (femail == "") {

        	                $("#femail").addClass('register').focus();
        	                $(".error_msg").html("Please enter a valid email address");

        	            } else if (!filter.test(femail)) {
        	                $("#femail").addClass('register').focus();
        	                $(".error_msg").html("Please enter a valid email address");
        	            } else {
        	                $.ajax({
        	                        type: "POST",
        	                        url: SITE_URL + "myaccount/forgotPassword",
        	                        data: $("#forgotForm").serialize()
        	                    })
        	                    .done(function(data) {
        	                        data = JSON.parse(data);
        	                         
        	                        if (data.state == false) {
        	                            $(".error_msg").html(data);
        	                            $( ".f-link-email-send" ).remove();
        	                            $( "<p class='f-link-email-send'>"+data.msg+"</p>" ).insertAfter( "#femail" );
        	                        }
        	                        
        	                        if (data.state == true) {
        	                        	$( ".f-link-email-send" ).remove();
        	                        	$( "<p class='f-link-email-send'> A link has been sent to your email id to reset the password.</p>" ).insertAfter( "#femail" );
        	                        }

        	                    });
        	            }
        		}
        	});

    },

    create_account: function() {

        $("#create_account").on('click', function(e) {
            e.preventDefault();

            $(".register").removeClass('register');
            $(".rerror_msg").html('');
            var rfname = $("#rfname").val();
            var rlname = $("#rlname").val();
            var remail = $("#remail").val();
            var rpwd = $("#rpwd").val();
            var rphone = $("#rphone").val();
            var rcompany = $("#rcompany").val();
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
            if (rfname == "") {

                $("#rfname").addClass('register').focus();
            } else if (rlname == "") {

                $("#rlname").addClass('register').focus();
            } else if (remail == "") {

                $("#remail").addClass('register').focus();


            } else if (!filter.test(remail)) {
                $("#remail").addClass('register').focus();
                $(".rerror_msg").html("Please enter a valid email address");
            } else if (rpwd == "") {

                $("#rpwd").addClass('register').focus();
            } else if (rpwd.length < 6) {
                $("#rpwd").addClass('register').focus();
                $(".rerror_msg").html("Your password must be at least 6 characters long");
            } else {
                $.ajax({
                    type: "POST",
                    url: SITE_URL + "myaccount/createAccount",
                    data: $("#signupForm").serialize(),
                    success: function(data) {
                        data = JSON.parse(data);
                        if (data.state == false) {
                            $("#regiter-form div.login-inner span.rerror_msg").html(data.msg);
                           
                        } else {
                            if (window.location.href.indexOf("createsandwich") != -1) {
                                var accounturl = SITE_URL + "createsandwich";
                            } else {
                                var accounturl = SITE_URL;
                            }


                            if (self.tempSlector != null) {
                            	
                            	className  = $(self.tempSlector).attr('class');
                            	IdName     = $(self.tempSlector).attr('id');
                            	text       = $(self.tempSlector).text();
                            	if(className) className  = className.toLowerCase(); else className = '';
                            	if(IdName) IdName     = IdName.toLowerCase(); else IdName = '';
                            	if(text) text       = text.toLowerCase(); else text = '';
                            	
                            	$(self.tempSlector).trigger('click');
		                    	self.tempSlector = null;
                            	
								if(className.indexOf('cart') != -1 || IdName.indexOf('cart') != -1 || text.indexOf('cart') != -1)
								{
									var intV = setInterval(function(){ 
									
										if( ha.sandwichcreator.tmpTransAnim == 1 ){ 
										window.location = accounturl;
										clearInterval(intV);
										}
									},10);
								 
								} else {
									$(".popup-wrapper").hide()
									window.location = accounturl;
								}
		                         
                                   
                            } else {
                            	$(".popup-wrapper").hide()
		                		 window.location = accounturl;
		                	}

                        }
                    }
                });

            }
        });
        
        $("#rpwd").on('keypress', function(e) {

        	if(e.keyCode==13)
        	{
				$('#create_account').click();

			}
		});

    },

    load_slides: function() {

        var length_slides = $('#slides div.slides_container div.slide').length;
        if (length_slides > 1) {
            $('#slides').slides({
                preload: false,

                play: 5000,
                pause: 2500,

                hoverPause: true,
                animationStart: function(current) {
                    $('.caption').animate({
                        bottom: -35
                    }, 100);
                    
                },
                animationComplete: function(current) {
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                    
                },
                slidesLoaded: function() {
                    $("div.slides_control div.slide:first").css("left", "960px");
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                }
            });
        }
    },

    fb_popup: function() {
		
		if(document.getElementById("fbLogin_alt").addEventListener){
			document.getElementById("fbLogin_alt").addEventListener("click", function(e) {
				e.preventDefault();
				window.open(this.getAttribute('href'), '_blank', 'width=1065,height=637');
			});

			document.getElementById("fbLogin").addEventListener("click", function(e) {
				e.preventDefault();
				window.open(this.getAttribute('href'), '_blank', 'width=1065,height=637');
			});
		}else{
			
			document.getElementById("fbLogin_alt").attachEvent("click", function(e) {
				e.preventDefault();
				window.open(this.getAttribute('href'), '_blank', 'width=1065,height=637');
			});

			document.getElementById("fbLogin").attachEvent("click", function(e) {
				e.preventDefault();
				window.open(this.getAttribute('href'), '_blank', 'width=1065,height=637');
			});
			
		}	
    },

    myaccount_reorder: function() {
        var self = this;
        $("a.reorder-button").click(function() {

            session = ha.common.check_session_state();
            if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                   ha.common.login_popup($(this));
                 } else {
                   ha.common.facebook_login($(this));
                 }
                return;
            } else {

                var order_id = $(this).data("order");
                $.ajax({
                    type: "POST",
                    data: {
                        'order_id': order_id
                    },
                    dataType: 'json',
                    url: SITE_URL + 'myaccount/myOrderReorder/',
                    beforeSend: function() {
                        $("#ajax-loader").show();
                    },
                    complete: function() {
                        $("#ajax-loader").hide();
                    },
                    success: function(e) {
                        //console.log(e);
                        //return false;
                        window.location = SITE_URL + "checkout/choosedeliveryOrPickup";
                    }
                });

            }
        });
    },


    order_view_details: function() {

        $(document).on("click", "div.accout-info-wrapper a.view-details", function() {

             
            session = ha.common.check_session_state();
            if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                   ha.common.login_popup($(this));
                 } else {
                   ha.common.facebook_login($(this));
                 }
                return;
            } else {

                var orderId = $(this).data("order");
                var order = $(this).data("order-id");

                $.ajax({
                    type: "POST",
                    data: {
                        'order_id': orderId
                    },
                    url: SITE_URL + 'myaccount/myOrderDetails/',
                    beforeSend: function() {
                        //$("#ajax-loader").show();
                        $("#order-detail-pop").removeAttr("style");
                    },
                    complete: function() {

                    },
                    success: function(data) {
                        $("#order-detail-pop").find("div.content").html(data);
                         
                        $("#order-detail-pop").show();
                        $("#order-detail-pop h1 .reorder-button").attr("data-order", order);

                        if ($("#order-detail-pop div.content").length > 0) {
                          
                        	$('#order-detail-pop div.content').jScrollPane();
                        }
                    }
                });

            }

        });

    },
    
    updatePickupOrDeliveryInCart : function(deliveryOrPickup){
		
    	//console.log(deliveryOrPickup);return;
		var data = {'deliveryOrPickup': deliveryOrPickup};
		
		var last_tip = $("#select_tip_amount").val();		
		 var grandTotal= $("#hidden_grand_total").val();
		$.ajax({
            type: "POST",
            data:data,
            url: SITE_URL+'checkout/updatePickupOrDelivery',
            beforeSend: function() {
				
			},
			complete:function() {
				
			},
            success: function(data){				
				data = JSON.parse(data);
				if(data.status == true){					
					$(".check-out-left").html(data.message);
					
					$.ajax({
						type: "POST",
						data:data,
						url: SITE_URL+'checkout/getCartItemQtyCount/',
						success: function(e){
							$("div.cart-top h1").text(e);
							var subtotal = $("#hidden_sub_total").val();
							var tip = $("#select_tip_amount").val();								
						}
					});
					
					//cart item list total value to Bottom Total 
					$("div.amount-done-wrapper h3").text($("div.grand-total h2").text());		

				}
			}
		});		 
		
	},
	
   
    //~checking if any 3-foot of 6-foot is existing in order. if there is then you get a TRUE else a FALSE.
    checkBreadSimple : function(){
        
        var self = this;
        var cateredBread = false

        $('.bread_type').each(function(){ 
        
            if( $(this).val() > 0) {  
                cateredBread = 1;
            } 
        });
        if( cateredBread == 1 )
            return 1;
        else
            return 0;
    },


	//~checking if any 3-foot of 6-foot is existing in order if any order will be available after 24 hr only. if everything fine this function will return true
	checkBreadType : function(){
		
		var self = this;
		var bradTypeAvilable = false
		
		$('.bread_type').each(function(){  
			
			if($(this).val() > 0) {  
				
				bradTypeAvilable = true
			} 
		});
		
 
		
	   if(bradTypeAvilable == true){
            self.set24HoursRequired( 1 ); 
	       if( $('#radio1:checked').length > 0 ){
				this.set24HoursRequired( 1 );
				alert("Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you! (1)");	
				return 'error';
				
			} else { 
			
			currentTime     = self.getServerTime();
			//~calculate hover diffence here.
			currentTime     = currentTime.split(',');
                        
			$date           = $('.delivery_date').val();
			$time           = $('.timechange select').val();
            
			
			$date = new Date($date);
			$date = $date.toLocaleDateString();
			
			$dateSplits     = $date.split('/');
			
			$timeSplits     = $time.split(':');
		$month          = parseInt($dateSplits[0]) - 1;
			$dateSelectd    = new Date(parseInt($dateSplits[2]),$month,parseInt($dateSplits[1]),parseInt($timeSplits[0]),parseInt($timeSplits[1]));
			ccdmonth  = parseInt(currentTime[1])-1
			$currentTime    = new Date(parseInt(currentTime[0]), ccdmonth , parseInt(currentTime[2]) , parseInt(currentTime[3]) , parseInt(currentTime[4]) , 0 );
			
			
			
			diffMs          =  Math.abs($dateSelectd - $currentTime);
			hours           = Math.floor(diffMs/3600000);
		
			 
			if(hours<24){
			
			}
            else
            {
            
                
            }
			
		  }
			
		}  
        else
        {
            this.set24HoursRequired( 0 );
        }
	   return true;		
	},
	
	// ~getting server time using async Ajax call.
	getServerTime : function(){
        var dateTime;
		var self = this;
        var currentDayHour = 0;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentHour/',
            async: false,
            success: function(data) {
            	 $json =  JSON.parse(data);
        		
            	if($json.date_time_js){
            		dateTime = $json.date_time_js;
            	}
            	
            }
        });
        return dateTime;
	},
	
	//Check if User have Address or not if not then popup Add New Adress
	checkForUserAdressesAvailable: function(){
		
		if($(".select_address option").length <= 2){
			//Choose option and Add new option = 2
			$('.select_address').val(0).trigger('change');				
		}
	
	}
	
}

	Date.prototype.withoutTime = function () {
		var d = new Date(this);
		d.setHours(0, 0, 0, 0, 0);
		return d
	}


$(document).ready(function() {
    ha.common.init();

})

