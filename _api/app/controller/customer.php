<?php

class Customer extends Controller
{
    /**
     *
     * Loading  constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * Insert customer information
     *
     */
    public function savingCustomerInfo()
    {
        $model = new customerModel();
        $data  = $model->savingCustomerInfo($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch customer Notes
     *
     */    
    public function getCustomerNotes()
    {
        $model = new customerModel();
        $data  = $model->getCustomerNotes($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert customer Notes
     *
     */  
    public function addCustomerNote()
    {
        $model = new customerModel();
        $data  = $model->addCustomerNote($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Remove customer Notes
     *
     */    
    public function removeCustomerNote()
    {
        $model = new customerModel();
        $data  = $model->removeCustomerNote($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Check user name exist
     *
     */    
    public function userNameExist()
    {
        $model = new customerModel();
        $data  = $model->userNameExist($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch customer details
     *
     */    
    public function get_customers_details()
    {
        
        $model = new customerModel();
        $data  = $model->get_customers_details($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch user review
     *
     */    
    public function get_user_reviews()
    {
        
        $model = new customerModel();
        $data  = $model->get_user_reviews($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch all review contents
     *
     */    
    public function get_all_review_contents()
    {
        
        $model = new customerModel();
        $data  = $model->get_all_review_contents();
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update customer details
     *
     */    
    public function update_customer()
    {
        
        $model = new customerModel();
        $data  = $model->update_customer($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch customer details
     *
     */    
    public function getCustomerData()
    {
        
        $model = new customerModel();
        $data  = $model->getCustomerData($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Delete all customer orders
     *
     */    
    public function delete_all_orders()
    {
        
        $model = new customerModel();
        $data  = $model->delete_all_orders($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    
    public function removeCard(){
    	
    	$model = new customerModel();
    	$data  = $model->removeCard($_POST);
    	
    	if ($data == false) {
    		$status_code = "204";
    		$message     = "Invalid";
    	} else {
    		$status_code = "200";
    		$message     = "Ok";
    	}
    	
    	$this->json_success($status_code, $message, $data);
    }
}