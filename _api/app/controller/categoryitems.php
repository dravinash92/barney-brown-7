<?php

class categoryitems extends Controller
{
    /**
     *
     * Loading  constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * Insert category items
     *
     */
    public function add_catgory_items()
    {
        $model       = new categoryitemsModel();
        $data        = $model->add_catgory_items($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
    }
    /**
     *
     * fetch custom category Items
     *
     */
    public function get_custom_catgory_items()
    {
        
        $model = new categoryitemsModel();
        $data  = $model->get_custom_catgory_items();
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * List custom category Items
     *
     */
    public function get_custom_catgory_items_list()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_custom_catagory_items_list($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch sandwich category items
     *
     */
    public function get_sandwich_category_items()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_sandwich_category_items_list($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Update sandwich category items
     *
     */
    public function update_sandwich_category_items()
    {
        $model       = new categoryitemsModel();
        $data        = $model->update_catgory_items($_POST);
        $status_code = "201";
        $message     = "Updated";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Delete sandwich category items
     *
     */
    public function delete_sandwich_category_items()
    {
        $model       = new categoryitemsModel();
        $data        = $model->delete_catgory_items($_POST);
        $status_code = "201";
        $message     = "Deleted";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch bread information
     *
     */
    public function get_bread_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_bread_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch customer information
     *
     */
    public function get_customer_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_customer_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }

/**
     *
     * Fetch customer information
     *
     */
    public function get_customer_data_count()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_customer_data_count($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }    /**
     *
     * Fetch all sandwich data
     *
     */
    public function get_all_sandwich_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_all_sandwich_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch all sandwich data
     *
     */
    public function get_standard_catgory_items()
    {
        
        $model = new categoryitemsModel();
        $data  = $model->get_standard_catgory_items();
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch catering category items
     *
     */
    public function get_catering_catgory_items()
    {
        
        $model = new categoryitemsModel();
        $data  = $model->get_catering_catgory_items();
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch standard category item list
     *
     */
    
    public function get_standard_catgory_items_list_front()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_standard_catgory_items_list_front($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch standard category item list
     *
     */
    
    public function get_standard_catgory_items_list()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_standard_catgory_items_list($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Insert standard category item
     *
     */
    
    public function add_standard_catgory_items()
    {
        $model       = new categoryitemsModel();
        $data        = $model->add_standard_catgory_items($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Insert standard category product
     *
     */
    
    public function add_standard_catgory_products()
    {
        $model       = new categoryitemsModel();
        $data        = $model->add_standard_catgory_products($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch standard product
     *
     */
    
    public function get_standard_product_items()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_standard_product_items($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Update standard category product
     *
     */
    
    public function update_standard_category_products()
    {
        $model       = new categoryitemsModel();
        $data        = $model->update_standard_category_products($_POST);
        $status_code = "201";
        $message     = "Updated";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Delete standard category product
     *
     */
    
    public function delete_standard_category_products()
    {
        $model       = new categoryitemsModel();
        $data        = $model->delete_standard_category_products($_POST);
        $status_code = "201";
        $message     = "Deleted";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Update standard category product
     *
     */
    public function update_standard_category_priority()
    {
        $model       = new categoryitemsModel();
        $data        = $model->update_standard_category_priority($_POST);
        $status_code = "201";
        $message     = "Updated";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch all pickup items informations
     *
     */
    public function get_all_pickups_items()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_all_pickups_items($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch all order status
     *
     */
    public function get_all_order_status()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_all_order_status($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Update order status
     *
     */
    public function update_order_status()
    {
        $model       = new categoryitemsModel();
        $data        = $model->update_order_status($_POST);
        $status_code = "201";
        $message     = "Updated";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch all delivery items informations
     *
     */
    public function get_all_delivery_items()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_all_delivery_items($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch all active delivery items informations of an user
     *
     */
    public function get_user_active_delivery_items()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_user_active_delivery_items($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch search delivery items informations by store
     *
     */
    public function get_search_delivery_items_by_store()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_search_delivery_items_by_store($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch search delivery items informations
     *
     */
    public function get_search_delivery_items()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_search_delivery_items($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch admin user information
     *
     */
    public function get_admin_user_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_admin_user_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    
    /**
     *
     * Fetch admin user information
     *
     */
    public function get_delivery_user_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_delivery_user_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch admin user category information
     *
     */
    public function get_admin_user_category()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_admin_user_category($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Insert new admin user
     *
     */
    public function add_new_admin_user()
    {
        $model       = new categoryitemsModel();
        $data        = $model->add_new_admin_user($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * List all admin user
     *
     */
    public function get_admin_user()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_admin_user($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }

    /**
     *
     * List all admin user
     *
     */
    public function get_delivery_user()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_delivery_user($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Update  admin user
     *
     */
    public function update_admin_user()
    {
        $model       = new categoryitemsModel();
        $data        = $model->update_admin_user($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Delete  admin user
     *
     */
    public function delete_admin_user()
    {
        $model       = new categoryitemsModel();
        $data        = $model->delete_admin_user($_POST);
        $status_code = "201";
        $message     = "Deleted";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Delete  Delivery user
     *
     */
    public function delete_delivery_user()
    {
        $model       = new categoryitemsModel();
        $data        = $model->delete_delivery_user($_POST);
        $status_code = "201";
        $message     = "Deleted";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch Store accounts data
     *
     */
    public function get_store_accounts_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_store_accounts_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert new Store account
     *
     */
    public function add_new_store_account()
    {
        $model       = new categoryitemsModel();
        $data        = $model->add_new_store_account($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch Store accounts
     *
     */
    public function get_store_account()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_store_account($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Update Store accounts
     *
     */
    public function update_store_account()
    {
        
        $model       = new categoryitemsModel();
        $data        = $model->update_store_account($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Delete Store accounts
     *
     */
    public function delete_store_account()
    {
        $model       = new categoryitemsModel();
        $data        = $model->delete_store_account($_POST);
        $status_code = "201";
        $message     = "Deleted";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch standard category
     *
     */
    public function getStandardCategories()
    {
        $model = new categoryitemsModel();
        $data  = $model->getStandardCategories();
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch standard category product
     *
     */
    public function getStandardCategoryProducts()
    {
        $model = new categoryitemsModel();
        $data  = $model->getStandardCategoryProducts($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch sponser meal category product
     *
     */
    public function getSponserMealCategoryProducts()
    {
        $model = new categoryitemsModel();
        $data  = $model->getSponserMealCategoryProducts($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert web page banner data 
     *
     */
    public function add_webpage_banner_data()
    {
        $model       = new categoryitemsModel();
        $data        = $model->add_webpage_banner_data($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch web Home page data
     *
     */
    public function get_web_homepage_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_web_homepage_data();
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update web Home page priority
     *
     */
    public function update_web_home_page_priority()
    {
        $model       = new categoryitemsModel();
        $data        = $model->update_web_home_page_priority($_POST);
        $status_code = "201";
        $message     = "Updated";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch web Home page banner
     *
     */
    public function get_web_homepage_banner()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_web_homepage_banner($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update web Home page banner
     *
     */
    public function update_webpage_banner_data()
    {
        $model       = new categoryitemsModel();
        $data        = $model->update_webpage_banner_data($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Update web page data
     *
     */
    public function update_webpage_data()
    {
        $model       = new categoryitemsModel();
        $data        = $model->update_webpage_data($_POST);
        $status_code = "201";
        $message     = "Created";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch web page data
     *
     */
    public function get_webpages_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_webpages_data();
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch web page data
     *
     */
    public function get_webpagedata_data()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_webpagedata_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Delete Home page banner
     *
     */
    public function delete_home_page_banner()
    {
        $model       = new categoryitemsModel();
        $data        = $model->delete_home_page_banner($_POST);
        $status_code = "201";
        $message     = "Deleted";
        $this->json_success($status_code, $message, '');
        
    }
    /**
     *
     * Fetch sandwich custom category item
     *
     */
    public function get_sandwich_custom_category_items()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_sandwich_custom_category_items($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch individual item count
     *
     */
    public function get_individual_item_count()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_individual_item_count($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update sandwich category item
     *
     */
    public function updateSandwichCategoryItems()
    {
        $model = new categoryitemsModel();
        $data  = $model->updateSandwichCategoryItems($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Delete Standard category
     *
     */
    public function deleteStandardCategory()
    {
        $model = new categoryitemsModel();
        $data  = $model->deleteStandardCategory($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch standard product extra data
     *
     */
    public function get_standard_product_extraData()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_standard_product_extraData($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch all products extra information
     *
     */
    public function getAllproductsExtras()
    {
        
        $model = new categoryitemsModel();
        $data  = $model->getAllproductsExtras($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
     /**
     *
     * Fetch all delivery account users
     *
     */
    public function get_del_users()
    {
        $model = new categoryitemsModel();
        $data  = $model->get_del_users($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
     /**
     *
     * Assign delivery user id to order
     *
     */
     public function assign_delivery_user()
     {
        $model = new categoryitemsModel();
        $data  = $model->assign_delivery_user($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
     }
      /**
     *
     * Remove delivery user id to order
     *
     */
     public function remove_delivery_user()
     {
        $model = new categoryitemsModel();
        $data  = $model->remove_delivery_user($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
     }
      /**
     *
     * Fetch all assigned orders for delivery account user
     *
     */
    public function getAssignedOrders()
    {
        $model = new categoryitemsModel();
        $data  = $model->getAssignedOrders($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }

    /**
     *
     * Fetch assigned order details
     *
     */
    public function getOrderDetails()
    {
        
        $model = new categoryitemsModel();
        $data  = $model->getOrderDetails($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
}
