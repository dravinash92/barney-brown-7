<?php

class Menu extends Controller
{
    /**
     *
     * Loading constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * Fetch sandwich data
     *
     */
    public function get_sandwich_data()
    {
        $model = new menuModel();
        $data  = $model->get_sandwich_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update Menu option
     *
     */
    public function edit_options()
    {
        $model = new menuModel();
        $data  = $model->edit_options($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert Menu option
     *
     */
    public function addoptions()
    {
        $model = new menuModel();
        $data  = $model->addoptions($_POST);
        
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Delete Menu option
     *
     */
    public function deleteOption()
    {
        $model = new menuModel();
        $data  = $model->deleteOption($_POST);
        
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update Menu option status
     *
     */
    public function updateoptionstatus()
    {
        $model = new menuModel();
        $data  = $model->updateoptionstatus($_POST);
        
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch facebook friends menu
     *
     */
    public function getFBfriendsMenu()
    {
        $model = new menuModel();
        $data  = $model->getFBfriendsMenu($_POST);
        
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
     /**
     *
     * check facebook login
     *
     */
    public function checkFBLogin()
    {
        $model = new menuModel();
        $data  = $model->checkFBLogin($_POST);
        
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch friend sandwich
     *
     */
    public function getfriendSandwichCount()
    {
        
        $model = new menuModel();
        $data  = $model->getfriendSandwichCount($_POST);
        
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    
    public function fill_unique_slugs(){
    	
    	$model = new menuModel();
    	$data  = $model->fill_unique_slugs($_POST);
    	
    	if ($data) {
    		$status_code = "200";
    		$message     = "OK";
    	} else {
    		$status_code = "204";
    		$message     = "No Content";
    	}
    	$this->json_success($status_code, $message, $data);
    	
    }
    
}