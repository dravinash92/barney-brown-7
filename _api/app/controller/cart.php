<?php
include_once(DIR_MODEL . 'authorizeApiModel' . '.php');
// api cart controller
class Cart extends Controller
{
	/**
	 *
	 * Loading  constructor
	 *
	 */	
    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * List Pickup Address
     *
     */    
    public function listPickupAddress()
    {
        $model = new cartmodel();
        $data  = $model->pickupAddresses($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * List Pickup Address
     *
     */
    public function pickupAddress()
    {
        $model = new cartmodel();
        $data  = $model->pickupAddress($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Updating cart Items
     *
     */ 
    public function updateCartItems()
    {
        $model = new cartmodel();
        $data  = $model->updateCartItems($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Saving billing information
     *
     */    
    public function savebillinginfo()
    {
        $model = new cartmodel();
        $authorize_model = new authorizeApiModel();
        $data  = $authorize_model->savebillinginfo($_POST);
        // Model echos the JSON for us, no need to get involved.  
        exit(  );
        
    }
    /**
     *
     * Fetch Billing Information
     *
     */    
    public function getBillinginfo()
    {
        $model = new cartmodel();
        $data  = $model->getBillinginfo($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }    
    /**
     *
     * Fetch user all order information
     *
     */    
    public function getUserOrdersInfo()
    {
        $model = new cartmodel();
        $data  = $model->getUserOrdersInfo($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch all cart items
     *
     */    
    public function cartItems()
    {
        $model = new cartmodel();
        $data  = $model->cartItems($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert items to cart
     *
     */
    public function addItemToCart()
    {
        $model = new cartmodel();
        
        $data  = $model->addItemToCart($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update items to cart
     *
     */   
    public function updateItemToCart()
    {
        $model = new cartmodel();
        $data  = $model->updateItemToCart($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Remove items from cart
     *
     */    
    public function removeCartItem()
    {
        $model = new cartmodel();
        $data  = $model->removeCartItem($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }    
    /**
     *
     * Fetch discount details information 
     *
     */    
    public function geDiscountDetails()
    {
        $model = new cartmodel();
        $data  = $model->geDiscountDetails($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch all order list
     *
     */    
    public function getOrdersList()
    {
        $model = new cartmodel();
        $data  = $model->getOrdersList($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch item on store selection
     *
     */    
    public function changeTimeOnStoreSelection()
    {
        $model = new cartmodel();
        $data  = $model->getTimeOnStoreSelection($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
        
    }
    /**
     *
     * Fetch cart item quantity
     *
     */    
    public function getCartItemQtyCount()
    {
        $model = new cartmodel();
        $data  = $model->getCartItemQtyCount($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
        
    }
	/**
     *
     * make sure the user's items haven't been purged from the cart due to inactivity
     *
     */
	public function checkIfEmpty() {
		$model = new cartmodel();
        $data  = $model->checkIfEmpty($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
	}
    /**
     *
     * Check Cvv
     *
     */   
    public function check_cvv()
    {
        $model = new cartmodel();
        $data  = $model->check_cvv($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch current store time slot 
     *
     */    
    public function getCurrentStoreTimeSlot()
    {
        $model = new cartmodel();
        $data  = $model->getCurrentStoreTimeSlot( $_POST );

        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }

        $this->json_success( $status_code, $message, $data );
    }
    
    public function getAllCloseTimes()
    {
    	$model = new cartmodel();
    	$data  = $model->getAllCloseTimes($_POST);
    	if ($data == false) {
    		$status_code = "204";
    		$message     = "Invalid";
    	} else {
    		$status_code = "200";
    		$message     = "Ok";
    	}
    	$this->json_success($status_code, $message, $data);
    }
    
   
    public function getspecificday(){
    	$model = new cartmodel();
    		
    	$data  = $model->getspecificday($_POST);
    	if ($data == false) {
    		$status_code = "204";
    		$message     = "Invalid";
    	} else {
    		$status_code = "200";
    		$message     = "Ok";
    	}
    	$this->json_success($status_code, $message, $data);
    }

    public function getAllZips(  )
    {
        $model = new cartmodel();
         
        $data  = $model->getAllZips( );

        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success( $status_code, $message, json_encode($data) );
    }
 
    public function checkZip(){
    	$model = new cartmodel();
    	 
    	$data  = $model->checkZip($_POST);
    	if ($data == false) {
    		$status_code = "204";
    		$message     = "Invalid";
    	} else {
    		$status_code = "200";
    		$message     = "Ok";
    	}
    	$this->json_success($status_code, $message, $data);
    }

    public function get_storeClosed_days(  )
    {
        $model = new cartmodel();
         
        $data  = $model->get_storeClosed_days( $_POST );

        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success( $status_code, $message, json_encode($data) );
    }
    /**
     *
     * Get card zip
     *
     */   
    public function getCardZIp()
    {
        $model = new cartmodel();
        $data  = $model->getCardZIp($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
}