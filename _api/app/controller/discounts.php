<?php


class discounts extends Controller
{
	/**
	 *
	 * Loading  constructor
	 *
	 */	
    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * Listing all discounts
     *
     */    
    public function get_discount_list()
    {
        $model = new discountsModel();
        $data  = $model->get_discount_list($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert discounts
     *
     */    
    public function add_discount()
    {
        $model = new discountsModel();
        $data  = $model->add_discount($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch a discount coupon
     *
     */    
    public function get_discount_row()
    {
        $model = new discountsModel();
        $data  = $model->get_discount_row($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update discount coupon
     *
     */   
    public function update_discount()
    {
        $model = new discountsModel();
        $data  = $model->update_discount($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Delete discount coupon
     *
     */   
    public function delete_items()
    {
        $model = new discountsModel();
        $data  = $model->delete_items($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update live discount coupon 
     *
     */    
    public function update_is_live()
    {
        
        $model       = new discountsModel();
        $data        = $model->update_is_live($_POST);
        $status_code = "201";
        $message     = "Updated";
        $this->json_success($status_code, $message, '');
        
    }
}