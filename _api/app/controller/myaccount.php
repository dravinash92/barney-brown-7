<?php
// api controller DEV
class MyAccount extends Controller
{
    /**
     *
     * Loading constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * List saved address of customer
     *
     */
    public function listSavedAddress()
    {
        $model = new myaccountmodel();
        $data  = $model->listAddress($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert customer address
     *
     */
    public function saveUserAddress()
    {
        
        $model = new myaccountmodel();
        $data  = $model->saveAddress($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "201";
            $message     = "Created";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Delete customer address
     *
     */
    public function removeAddress()
    {
        
        $model = new myaccountmodel();
        $data  = $model->removeAddress($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update customer address
     *
     */
    public function editAddress()
    {
        
        $model = new myaccountmodel();
        $data  = $model->editAddress($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch web page data
     *
     */
    public function get_webpages_data()
    {
        $model = new myaccountmodel();
        $data  = $model->get_webpages_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch web pages data
     *
     */
    public function get_webpagesdata_data()
    {
        $model = new myaccountmodel();
        $data  = $model->get_webpagesdata_data($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Check Store user name exist
     *
     */
    public function storeUsernameExists()
    {
        $model = new myaccountmodel();
        $data  = $model->storeUsernameExists($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Check CMS user name exist
     *
     */
    public function cmsUsernameExists()
    {
        $model = new myaccountmodel();
        $data  = $model->cmsUsernameExists($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Check login
     *
     */
    public function loginCheck()
    {
        $model = new myaccountmodel();
        $data  = $model->loginCheck($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Create / Insert user account
     *
     */
    public function createAccount()
    {
        $model = new myaccountmodel();
        $data  = $model->createAccount($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch order history
     *
     */
    public function orderhistory()
    {
        $model = new myaccountmodel();
        $data  = $model->orderhistory($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch last 10 order items
     *
     */
    public function orderItemHistory()
    {
        $model = new myaccountmodel();
        $data  = $model->orderItemHistory($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert user review
     *
     */
    public function addUserReview()
    {
        $model = new myaccountmodel();
        $data  = $model->addUserReview($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert blling address
     *
     */
    public function savedBilling()
    {
        $model = new myaccountmodel();
        $data  = $model->savedBilling($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert / update card information - api
     *
     */
    public function cardAddEditEvent()
    {
        $model_authorize_api = new authorizeApiModel();
        $data = $model_authorize_api->cardAddEditEvent($_POST);
        // Model echos the JSON for us, no need to get involved.  
        exit(  );
    }
    /**
     *
     * Card remove evnt
     *
     */
    public function cardRemoveEvent()
    {
        $model_authorize_api = new authorizeApiModel();
        $data = $model_authorize_api->cardRemoveEvent($_POST);
        // Model echos the JSON for us, no need to get involved.  
    }
    /**
     *
     * Fetch saved meals
     *
     */
    public function getSavedMeals()
    {
        $model = new myaccountmodel();
        $data  = $model->getSavedMeals($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch password for forgot user
     *
     */
    public function forgotPassword()
    {
        $model = new myaccountmodel();
        $data  = $model->forgotPassword($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert social links information
     *
     */
    public function saveSocialLinks()
    {
        $model = new myaccountmodel();
        $data  = $model->saveSocialLinks($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch social links information
     *
     */
    public function getSocialLinks()
    {
        $model = new myaccountmodel();
        $data  = $model->getSocialLinks($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert / update password for user
     *
     */
    public function setPassword()
    {
        $model = new myaccountmodel();
        $data  = $model->setPassword($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * update store order
     *
     */
    public function updateStoreOrder()
    {
        $model = new myaccountmodel();
        $data  = $model->updateStoreOrder($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update user lives
     *
     */
    public function updateUsersLive()
    {
        $model = new myaccountmodel();
        $data  = $model->updateUsersLive($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Update user lives
     *
     */
    public function updateDeliveryUsersLive()
    {
        $model = new myaccountmodel();
        
        $data  = $model->updateDeliveryUsersLive($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch saved sandwiches
     *
     */
    public function get_saved_sandwiches()
    {
        $model = new myaccountmodel();
        $data  = $model->get_saved_sandwiches($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch past order history
     *
     */
    public function get_past_order_history()
    {
        $model = new myaccountmodel();
        $data  = $model->get_past_order_history($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch user review messages
     *
     */
    public function get_user_review_messages()
    {
        $model = new myaccountmodel();
        $data  = $model->get_user_review_messages($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch all zipcode for selected address
     *
     */
    public function getAllzipcodes()
    {
        $model = new myaccountmodel();
        $data  = $model->getAllzipcodes($_POST); 
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Insert reorder information
     *
     */
    public function myOrderReorder()
    {
        $model = new myaccountmodel();
        $data  = $model->myOrderReorder($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Delete zipcode of stores
     *
     */
    public function deleteZipcode()
    {
        $model = new myaccountmodel();
        $data  = $model->deleteZipcode($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch store informatiom from zip
     *
     */
    public function getStoreFomZip()
    {
        $model = new myaccountmodel();
        $data  = $model->getStoreFomZip($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch useraccount access token
     *
     */
    public function getUserAccessToken()
    {
        $model = new myaccountmodel();
        $data  = $model->getUserAccessToken($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
    }
    
     public function checkUserAvailability(){
     	
     	$model = new myaccountmodel();
     	$data  = $model->checkUserAvailability($_POST);  
     	if ($data) {
     		$status_code = "200";
     		$message     = "OK";
     	} else {
     		$status_code = "204";
     		$message     = "No Content";
     	}
     	$this->json_success($status_code, $message, $data);
     	
     }
    
    /**
     *
     * Check if useraccount token is valid to reset password
     *
     */
     public function isTokenValid()
     {
         $model = new myaccountmodel();
        $data  = $model->isTokenValid($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "204";
            $message     = "No Content";
        }
        $this->json_success($status_code, $message, $data);
     }

     /**
     *
     * Delivery User - Check login
     *
     */
    public function delLoginCheck()
    {
        $model = new myaccountmodel();
        $data  = $model->delLoginCheck($_POST);
        if ($data) {
            $status_code = "200";
            $message     = "OK";
        } else {
            $status_code = "401";
            $message     = "Invalid credentials";
        }
        $this->json_success($status_code, $message, $data);
    }
}