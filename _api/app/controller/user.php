<?php


class user extends Controller
{
	/**
	 *
	 * Loading constructor
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}
	/**
	 *
	 * Insert user
	 *
	 */
	public function add()
	{

		$usermodel=new userModel();
		$usermodel->add($_POST);
		$status_code="201";
		$message="Created";
		$this->json_success($status_code,$message,'');
	}
	/**
	 *
	 * Update user information
	 *
	 */
	public function update()
	{

		$usermodel=new userModel();
		$user=$usermodel->update($_POST);
		if($user)
		{
			$status_code="200";
			$message="OK";
		}
		else{
			$status_code="204";
			$message="No Content";
		}
		$this->json_success($status_code,$message,$user);
	}
	/**
	 *
	 * List all user Information
	 *
	 */
	public function get()
	{
		$usermodel=new userModel();
		if(isset($_POST['user_id']))
		{
			$user_id=$_POST['user_id'];
		}
		else{
			$user_id=0;
		}

		$user=$usermodel->get($user_id);
		if($user){
			$status_code="200";
			$message="OK";
		}
		else{
			$status_code="204";
			$message="No Content";
		}
		$this->json_success($status_code,$message,$user);

	}
}