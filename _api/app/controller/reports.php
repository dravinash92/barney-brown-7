<?php
class Reports extends Controller
{
	/**
	 *
	 * Loading constructor
	 *
	 */	
    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * Fetch / List orders
     *
     */    
    public function getOrders()
    {
        $model = new reportsmodel();
        $data  = $model->getOrders($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch / List orders
     *
     */    
    public function getDiscountsList()
    {
        $model = new reportsmodel();
        $data  = $model->getDiscountsList($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch / List system users
     *
     */    
    public function getSystemUsers()
    {
        $model = new reportsmodel();
        $data  = $model->getSystemUsers($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch / List  total sales report
     *
     */    
    public function doTotalSalesFilterReports()
    {

        $model = new reportsmodel();
        
        $data  = $model->doTotalSalesFilterReports($_POST);

        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch / List Item Sales Reports
     *
     */   
    public function getItemSalesReports()
    {
        $model = new reportsmodel();
        $data  = $model->getItemSalesReports();
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch / List sandwich Item
     *
     */    
    public function sandwichitems()
    {
        $model = new reportsmodel();
        $data  = $model->getSandwichItems();
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch / List standared items
     *
     */   
    public function standareditems()
    {
        $model = new reportsmodel();
        $data  = $model->getStandaredItems();
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch / List Standard Category
     *
     */    
    public function getStandardCategory()
    {
        $model = new reportsmodel();
        $data  = $model->getStandardCategory();
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    
    
    public function getCateringCategory()
    {
    	$model = new reportsmodel();
    	$data  = $model->getCateringCategory();
    	if ($data == false) {
    		$status_code = "204";
    		$message     = "Invalid";
    	} else {
    		$status_code = "200";
    		$message     = "Ok";
    	}
    	$this->json_success($status_code, $message, $data);
    }
    
    /**
     *
     * Fetch / List Standard item quantity
     *
     */    
    public function standareditemsqty()
    {
        $model = new reportsmodel();
        $data  = $model->getStandaredItemsQty($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch / List custom items
     *
     */    
    public function customitems()
    {
        $model = new reportsmodel();
        $data  = $model->getCustomItems($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Report total item filter
     *
     */    
    public function doTotalItemsFilterReports()
    {
        $model = new reportsmodel();
        $data  = $model->doTotalItemsFilterReports($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Report Total Custom Items Filter
     *
     */    
    public function doTotalCustomItemsFilterReports()
    {
        $model = new reportsmodel();
        $data  = $model->doTotalCustomItemsFilterReports($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     * Fetch order items
     *
     */    
    public function getOrderedItems()
    {
        $model = new reportsmodel();
        $data  = $model->getOrderedItems($_POST);
        
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     *  Fetch Hourly Order Report
     *
     */    
    public function getHourlyOrderReport()
    {
        
        $model = new reportsmodel();
        $data  = $model->getHourlyOrderReport($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     *  Fetch order details
     *
     */    
    public function getOrderDetails()
    {
        
        $model = new reportsmodel();
        $data  = $model->getOrderDetails($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     *  Check report pin
     *
     */   
    public function checkReportPin()
    {
        
        $model = new reportsmodel();
        $data  = $model->checkReportPin($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
    /**
     *
     *  Fetch total sales count
     *
     */    
    public function getTotalSaleCount()
    {
        $model = new reportsmodel();
        $data  = $model->getTotalSaleCount($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }

    /**
     *
     *  Fetch transaction log
     *
     */    
    public function getTransactionLog()
    {
        $model = new reportsmodel();
        $data  = $model->getTransactionLog($_POST);
        if ($data == false) {
            $status_code = "204";
            $message     = "Invalid";
        } else {
            $status_code = "200";
            $message     = "Ok";
        }
        $this->json_success($status_code, $message, $data);
    }
}