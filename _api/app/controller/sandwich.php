<?php
// THE API CONTROLLER
class sandwich extends Controller
{

	use CensorWords;
	/**
	 *
	 * Loading constructor
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}
	/**
	 *
	 * Insert category information
	 *
	 */
	public function category_add()
	{
		$model     = new sanwichModel();
		$state     = $model->cat_add($_POST);

		if($state == false){
			$status_code="204";
			$message="Invalid Data";
		}
		else {
			$status_code="201";
			$message="Created";
		}

		$this->json_success($status_code,$message,'');
	}
	/**
	 *
	 * Fetch getting category data
	 *
	 */
	public function category_get()
	{
		$model     = new sanwichModel();
		$data      = $model->get_cat($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Insert category item
	 *
	 */
	public function category_item_add(){
		$model       = new sanwichModel();
		$state       = $model->category_item_add($_POST);
		$status_code = "201";
		$message     = "Created";
		$this->json_success($status_code,$message,'');
	}
	/**
	 *
	 * List item data
	 *
	 */
	public function get_item(){
		$model       = new sanwichModel();
		$data        = $model->get_item($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Insert item options
	 *
	 */
	public function add_item_options(){
		$model       = new sanwichModel();
		$data        = $model->add_item_options($_POST);
		$status_code = "201";
		$message     = "Created";
		$this->json_success($status_code,$message,'');
	}
	/**
	 *
	 * List item options
	 *
	 */
	public function get_item_options(){

		$model       = new sanwichModel();
		$data        = $model->get_item_options($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch product data
	 *
	 */
	public function get_product_data(){

		$model       = new sanwichModel();
		$data        = $model->get_product_data($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Insert sandwich information
	 *
	 */
	public function add_sandwich_data(){
		$model       = new sanwichModel();
		$data        = $model->add_sandwich_data($_POST);
		if($data == false){
			$status_code = "204";
			$message     = "Invalid";
		}else {
			$status_code = "201";
			$message     = "Created";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Update sandwich information
	 *
	 */	
	public function update_sandwich_data(){

		$model       = new sanwichModel();
		$data        = $model->update_sandwich_data($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "Failed";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List sandwich information
	 *
	 */	
	public function get_sandwich_data(){

		$model       = new sanwichModel();
		$data        = $model->get_sandwich_data($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch gallery data count 
	 *
	 */	
	public function get_gallery_data_count(){
		$model       = new sanwichModel();
		$data        = $model->get_gallery_data_count($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * fetch gallery information
	 *
	 */	
	public function get_gallery_data(){

		$model       = new sanwichModel();
		$data        = $model->get_gallery_data($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	
	public function filter_seacrh_ajax_more(){
		
		$model       = new sanwichModel();
		$data        = $model->filter_seacrh_ajax_more($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);
	}
	public function filter_seacrh_ajax_more_admin(){
		$model       = new sanwichModel();
		$data        = $model->filter_seacrh_ajax_more_admin($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Fetch gallery data in admin
	 *
	 */	
	public function get_gallery_data_admin(){

		$model       = new sanwichModel();
		$data        = $model->get_gallery_data_admin($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch menu data 
	 *
	 */
	public function get_menu_data(){

		$model       = new sanwichModel();
		$data        = $model->get_menu_data($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch user sandwich ids 
	 *
	 */
	public function get_user_sanwich_ids(){

		$model       = new sanwichModel();
		print_r($_POST);exit;
		$data        = $model->get_user_sanwich_ids($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List all checkout data
	 *
	 */
	public function get_checkout_data(){
		$model       = new sanwichModel();
		$data        = $model->get_checkout_data($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Insert order items   API CONTROLLER
	 *
	 */
	public function add_order_items(){


		$model       = new sanwichModel();
		$data        = $model->add_order_items($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Insert checkout placeorder data - api controller
	 *
	 */	
	public function insert_checkout_placeorder_data(){
		$model       = new sanwichModel();
		$authorize_model = new authorizeApiModel(  );

		$checkout_result        = $authorize_model->insert_checkout_placeorder_data($_POST);
		
		// If we have gotten here then things are all good, else we short-circuited with an error echo and exit back in the auth model.
		try{
				$order_details = $model->get_checkout_order_success_data( array( 'order_id'=>  $checkout_result ) );
				//$model->sendOrderConfirmationMail($order_details); 
		}
		catch ( Exception $e )
		{
			// If error usually sendmail is broke.  How to handle this?
			// Let's just use the log function in authAPI to note this and, since the result has already been printed, quit in all cases.
			$authorize_model->log_error( $e, 'sendmail' );
		}
		exit(  );
			
	}
	
	public function insert_Admin_checkout_placeorder()
	{
		$model       = new sanwichModel();
		$authorize_model = new authorizeApiModel(  );
		//print_r(  $_POST );
		//exit(  );
		$checkout_result        = $authorize_model->insert_admin_checkout_placeorder( $_POST );
				// If we have gotten here then things are all good, else we short-circuited with an error echo and exit back in the auth model.
		try{
			// SOMEHOW WE GET GETTING BASCK THE WRONG INFO HERE -- 
				$order_details = $model->get_checkout_order_success_data( array( 'order_id'=>  $checkout_result ) );

				$model->sendOrderConfirmationMail( $order_details ); 
		}
		catch ( Exception $e )
		{
			// If error usually sendmail is broke.  How to handle this?
			// Let's just use the log function in authAPI to note this and, since the result has already been printed, quit in all cases.
			$authorize_model->log_error( $e, 'sendmail_admin' );
		}
		exit(  );

		
	
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success( $status_code, $message, $data );
	}


	/**
	 *
	 * Insert address information
	 *
	 */	
	public function insert_address_data(){
		$model       = new sanwichModel();
		$data        = $model->insert_address_data($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Delete Placed order information
	 *
	 */
	public function remove_placed_orders(){
		$model       = new sanwichModel();
		$data        = $model->remove_placed_orders($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * remove cart order
	 *
	 */	
	public function remove_cart_order(){
		$model       = new sanwichModel();
		$data        = $model->remove_cart_item($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * List admin gallery data
	 *
	 */	
	public function get_admin_gallery_data(){



		$model       = new sanwichModel();
		$data        = $model->get_admin_gallery_data($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}



		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Update gallery public state
	 *
	 */	
	public function update_gallery_public_state(){
		$model       = new sanwichModel();
		$data        = $model->update_gallery_public_state($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Delete gallery items
	 *
	 */	
	public function delete_gallery_item(){
		$model       = new sanwichModel();
		$data        = $model->delete_gallery_item($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Delete menu items
	 *
	 */
	public function  remove_menu_item(){

		$model       = new sanwichModel();
		$data        = $model->remove_menu_item($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Insert menu items
	 *
	 */
	public function add_menu_multi(){


		$model       = new sanwichModel();
		$data        = $model->add_menu_multi($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Update flag
	 *
	 */
	public function toggle_flag(){


		$model       = new sanwichModel();
		$data        = $model->toggle_flag($_POST);
		exit;
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List sandwich category items
	 *
	 */	
	public function getSandwichCategoryItems(){

		$model       = new sanwichModel();
		$data        = $model->getSandwichCategoryItems();
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List sandwich category
	 *
	 */	
	public function getSandwichCategories(){

		$model       = new sanwichModel();
		$data        = $model->getSandwichCategories();
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Insert product likes
	 *
	 */	
	public function addLike(){

		$model       = new sanwichModel();
		$data        = $model->addLike($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}



		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List sandwich likes
	 *
	 */
	public function  get_sandwich_like() {

		$model       = new sanwichModel();
		$data        = $model->get_sandwich_like($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List sandwich likes count
	 *
	 */	
	public function get_sandwich_like_count(){

		$model       = new sanwichModel();
		$data        = $model->get_sandwich_like_count($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Update Toast menu
	 *
	 */	
	public function updateToastmenu(){

		$model       = new sanwichModel();
		$data        = $model->updateToastmenu($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List all images
	 *
	 */
	
	public function get_all_images(){

		$model       = new sanwichModel();
		$data        = $model->get_all_images($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List category item data
	 *
	 */	
	public function get_category_items_data(){

		$model       = new sanwichModel();
		$data        = $model->get_category_items_data($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List purchased ite
	 *
	 */	
	public function get_purchase(){

		$model       = new sanwichModel();
		$data        = $model->get_purchase($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List random names
	 *
	 */	
	public function get_random_name()
	{
		$model       = new sanwichModel();
		$data        = $model->get_random_name();

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * List sanwich names
	 *
	 */
	public function get_sandwich_name()
	{
		$model       = new sanwichModel();
		$data        = $model->get_sandwich_name($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Send flag mail
	 *
	 */
	public function sendFlagMail(){

		$mailObject = new MailSender();
		$data  = $this->process_input($_POST);
		$usrname       = $data['user'];
		$sandwichname  = $data['sandwich'];
		$mailObject->setSubject($sandwichname.' has been flagged by '.$usrname);
		$mailObject->setBody($sandwichname.' has been flagged by '.$usrname);
		$mailObject->setMailTo(ADMIN_EMAIL);
		$response = $mailObject->sendMail();

		if($response){
			$status_code = "200";
			$message     = "Mail Send";
		}
		else {
			$status_code = "204";
			$message     = "Sending Failed";
		}
		return $this->json_success($status_code,$message,$response);
	}
	/**
	 *
	 * Fetch last order
	 *
	 */
	public function getLastOrder(){

		$model       = new sanwichModel();
		$data        = $model->getLastOrder($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch order sandwich banner image
	 *
	 */	
	public function OrderSandwichBannerImage(){

		$model       = new sanwichModel();
		$data        = $model->OrderSandwichBannerImage($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch address street
	 *
	 */
	public function get_address_street(){

		$model       = new sanwichModel();
		$data        = $model->get_address_street($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch banner order details
	 *
	 */
	public function banner_order_details(){

		$model       = new sanwichModel();
		$data        = $model->banner_order_details($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch checkout order success data
	 *
	 */	
	public function get_checkout_order_success_data(){


		$model       = new sanwichModel();
		$data        = $model->get_checkout_order_success_data($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Fetch order popup data
	 *
	 */	
	public function get_order_popup_data(){
		$model       = new sanwichModel();
		$data        = $model->get_order_popup_data($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Process inputs
	 *
	 */
	private function process_input($data){
		array_walk($data,function(&$val,$key){
			$val = urldecode($val);

		});
		return $data;
	}
	/**
	 *
	 * Insert admin like
	 *
	 */	
	public function addAdminLike(){

		$model       = new sanwichModel();
		$data        = $model->addAdminLike($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}



		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Insert purchase
	 *
	 */	
	public function addPuchase(){

		$model       = new sanwichModel();
		$data        = $model->addPuchase($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}



		$this->json_success($status_code,$message,$data);

	}
	/**
	 *
	 * Insert sandwich to sotd
	 *
	 */	
	public function addSandwichToSOTD(){

		$model       = new sanwichModel();
		$data        = $model->addSandwichToSOTD($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Fetch sandwich of the day
	 *
	 */	
	public function getSandwichOfTheDay(){

		$model       = new sanwichModel();
		$data        = $model->getSandwichOfTheDay($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Remove sandwich to sotd
	 *
	 */	
	public function removeSandwichToSOTD(){

		$model       = new sanwichModel();
		$data        = $model->removeSandwichToSOTD($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Insert sandwich to TRND
	 *
	 */	
	public function addSandwichToTRND(){

		$model       = new sanwichModel();
		$data        = $model->addSandwichToTRND($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * List  sandwich of Trending
	 *
	 */	
	public function getSandwichOfTrending(){

		$model       = new sanwichModel();
		$data        = $model->getSandwichOfTrending($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Delete  sandwich of Trending
	 *
	 */	
	public function removeSandwichToTRND(){

		$model       = new sanwichModel();
		$data        = $model->removeSandwichToTRND($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Set TrndLive
	 *
	 */	
	public function setTrndLive(){

		$model       = new sanwichModel();
		$data        = $model->setTrndLive($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * Fetch all data
	 *
	 */	
	public function filterWord(){
		 
		$this->replacer = '*';
		$this->setDictionary('en-us');
		$out = $this->censorString(urldecode($_POST['word']));
		if(count($out['matched']) > 0){
			$status_code = "500";
			$message     = "Abusive Words";
		}
		else {
			$status_code = "200";
			$message     = "Content clean";
		}

		$this->json_success($status_code,$message,$out);

	}
	/**
	 *
	 * List order count
	 *
	 */	
	public function get_order_count(){

		$model       = new sanwichModel();
		$data        = $model->get_order_count($_POST);

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}

		$this->json_success($status_code,$message,$data);
	}
	/**
	 *
	 * List individual Sandwich data
	 *
	 */	
	public function get_individual_sandwich_data(){
		 
		$model       = new sanwichModel();
		$data        = $model->get_individual_sandwich_data($_POST);
		 
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		 
		$this->json_success($status_code,$message,$data);
		 
	}
	
	
	
	public function getAllsandwichDetailsSearch(){
		
		$model       = new sanwichModel();

		$data        = $model->getAllsandwichDetailsSearch($_POST);
			file_put_contents('app/log/search_data.log', print_r($data,true),  FILE_APPEND );
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
		
		/*$success_array=array();
		$success_array['Response']['status-code']=$status_code;
		$success_array['Response']['message']=$message;
		$success_array['Data']=$data;
		file_put_contents('app/log/search_data.log', json_encode($success_array),  FILE_APPEND );*/
		$this->json_success($status_code,$message,$data);
			
	}

	
	// the api entr point.
	public function  filter_seacrh_ajax(){
		
		$model       = new sanwichModel();
		//dgb( array('TEST'='test' ));

		$data        = $model->filter_seacrh_ajax($_POST);
		
	//	var_dump( $data );exit(  );

		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
			
		$this->json_success($status_code,$message,$data);
			
	}
	public function filter_seacrh_ajax_admin(){
		$model       = new sanwichModel();
		$data        = $model->filter_seacrh_ajax_admin($_POST);
			
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
			
		$this->json_success($status_code,$message,$data);
	}
	public function retriveMostSoldSandwich( ){
		
		$model       = new sanwichModel();
		$data        = $model->retriveMostSoldSandwich($_POST);
			
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
			
		$this->json_success($status_code,$message,$data);
		
	}
	
	public function getTotalSanwichMenuCount(){
		
		$model       = new sanwichModel();
		$data        = $model->getTotalSanwichMenuCount($_POST);
			
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
			
		$this->json_success($status_code,$message,$data);
	}
	
	
	public function isFbOnlyUser(){
	
		$model       = new sanwichModel();
		$data        = $model->isFbOnlyUser($_POST);
			
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
			
		$this->json_success($status_code,$message,$data);
	}
	
	public function deliveryTime(){
		$model       = new sanwichModel();
		
		$data        = $model->deliveryTime($_POST);
					
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
			
		$this->json_success($status_code,$message,$data);
	}

	public function get_sandwich_current_price(){
		$model       = new sanwichModel();
		
		$data        = $model->Getcurrentprice($_POST);
					
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
			
		$this->json_success($status_code,$message,$data);
	}
	public function get_sandwich_current_price_menu(){
		$model       = new sanwichModel();
		
		$data        = $model->get_sandwich_current_price_menu($_POST);
					
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "No Content";
		}
			
		$this->json_success($status_code,$message,$data);
	}
	public function get_searchItem_count(){

		$model       = new sanwichModel();
		$data        = $model->get_searchItem_count($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "Failed";
		}
		$this->json_success($status_code,$message,$data);

	}
	public function sandwich_filter()
	{
		$model       = new sanwichModel();
		$data        = $model->sandwich_filter($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "Failed";
		}
		$this->json_success($status_code,$message,$data);

	}
	public function sandwich_filter_count()
	{
		$model       = new sanwichModel();
		$data        = $model->sandwich_filter_count($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "Failed";
		}
		$this->json_success($status_code,$message,$data);

	}

	public function more_sandwich_filter()
	{
		$model       = new sanwichModel();
		$data        = $model->more_sandwich_filter($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "Failed";
		}
		$this->json_success($status_code,$message,$data);

	}
	public function gallery_load_data()
	{
		$model       = new sanwichModel();
		$data        = $model->gallery_load_data($_POST);
		if($data){
			$status_code = "200";
			$message     = "OK";
		}
		else {
			$status_code = "204";
			$message     = "Failed";
		}
		$this->json_success($status_code,$message,$data);

	}
}