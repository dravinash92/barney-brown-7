<?php
// API MODEL
// REFACTORED VERSION....!!!!

define('ORDER_ID_BASE_NUMBER', 11100000);




class sanwichModel extends Model
{
	public $db; //database connection object
	/**
	* invoke database connection object
	*/
	public $orders;
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * Add sandwich category
	 */
	public function cat_add($post)
	{
		$catName = $this->clean_for_sql($post['cat_name']);
		$catIDn  = $this->clean_for_sql($post['cat_ident']);
		if ($catName) {
			$query = "INSERT INTO `sandwich_categories` (category_name, category_identifier ) values ('" . $catName . "','" . $catIDn . "') ";
			return $this->db->Query($query);
		} else {
			return false;
		}
	}
	/**
	 * List sandwich category
	 */
	public function get_cat($post)
	{
		$id    = @$this->clean_for_sql($post['cat_id']);
		$idn   = @$this->clean_for_sql($post['cat_ident']);
		$query = "SELECT * FROM `sandwich_categories` ";
		if ($idn) {
			$query .= " WHERE category_identifier = '" . $idn . "' ";
		} else if ($id) {
			$query .= " WHERE id = '" . $id . "' ";
		}
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * for adding sandwich category items
	 */
	public function category_item_add($post)
	{
		$post     = $this->process_input($post);
		$catName  = @$post['name'];
		$price    = @$post['price'];
		$image    = @$post['image'];
		$cat_id   = @$post['category_id'];
		$hav_optn = @$post['options_id'];
		$query    = "INSERT INTO `sandwich_category_items` (item_name,item_price,item_image,category_id,options_id) VALUES ('" . $catName . "','" . $price . "','" . $image . "','" . $cat_id . "','" . $hav_optn . "')";
		return $this->db->Query($query);
	}
	/**
	 * List category items
	 */
	public function get_item($post)
	{
		$post   = $this->process_input($post);
		$id     = $post['item_id'];
		$query  = "SELECT sct.*,sc.category_name FROM sandwich_category_items as sct JOIN sandwich_categories as sc ON sct.category_id = sc.id WHERE sct.category_id = '" . $id . "' AND sct.live=1 ORDER BY sct.priority ASC ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Add sandwich item options
	 */
	public function add_item_options($post)
	{
		$post       = $this->process_input($post);
		$name       = $post['name'];
		$unit       = $post['unit'];
		$multiplier = $post['multiplier'];
		$query      = "INSERT INTO `sandwich_item_options` (option_name,option_unit,price_mult) values ('" . $name . "','" . $unit . "','" . $multiplier . "') ";
		return $this->db->Query($query);
	}
	/**
	 * List sandwich item options
	 */
	public function get_item_options($post)
	{
		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `sandwich_item_options` WHERE id = '" . $id . "'   ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Add user sandwich data
	 */
	public function add_sandwich_data($post)
	{
		$post          = $this->process_input($post);
		$id            = $post['uid'];
		$data          = $post['data'];
		$price         = $post['price'];
		$date          = date('Y-m-d H:i:s');
		$pub           = $post['is_pub'];
		$name          = htmlspecialchars_decode($post['name']);
		$sandwich_items = $post['sandwich_items'];
		$menu_isactive = $post['menu_active'];
		$toast         = $post['toast'];
		$menu_add_count = 0;
		$menu_added_from = ($post['id'] != '' && isset($post['id'])) ? $post['id'] : 0;
		
		if (strpos($name, "’") !== false) 
		{
		    $name = str_replace("’","\'",$name);
		}
		
		//$name = htmlspecialchars($name);

		if ($menu_isactive == 1)
			$menu_add_count = 1;
		if (isset($post['by_admin']))
			$createdbyadmin = $post['by_admin'];
		else
			$createdbyadmin = 0;
		if (!$id) {
			return false;
		} else {
			$query = "INSERT INTO `user_sandwich_data` (uid,sandwich_name,sandwich_data,sandwich_price,sandwich_items,date_of_creation,is_public,menu_is_active,menu_add_count,menu_added_from,menu_toast,description,by_admin) VALUES ('" . $id . "','" . $name . "','" . $data . "','" . $price . "','"  . $sandwich_items . "','" . $date . "','" . $pub . "','" . $menu_isactive . "','" . $menu_add_count . "','" . $menu_added_from . "','" . $toast . "',' ',$createdbyadmin) ";
			
			$this->db->Query($query);
			return $this->db->insertId();
		}
	}
	/**
	 * List user sandwich data
	 */
	public function get_sandwich_data($post)
	{
		$query ="";
		$post = $this->process_input($post);
		if ($post['id'] == '*') {
			$query = "SELECT * FROM `user_sandwich_data` WHERE uid = '" . $post['uid'] . "' ";
		} else if($post['id']){
			 $query = "SELECT * FROM `user_sandwich_data` WHERE id = '" . $post['id'] . "' ";
		}
		 
		
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List standard category products
	 */
	public function get_product_data($post)
	{
		$post   = $this->process_input($post);
		$query  = "SELECT * FROM `standard_category_products` WHERE id = '" . $post['id'] . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * List gallery data
	 */
	public function get_gallery_data_count($post)
	{

		$result = $this->db->Query("SELECT id FROM `user_sandwich_data` WHERE  is_public = '1'  GROUP BY sandwich_name,sandwich_data");

		$result = $this->db->FetchAllArray($result);

		return count($result);

	}
	/**
	 * List user gallery data
	 */
	public function get_gallery_data($post)
	{

		$start = $limit = 0;
		if (isset($post['start']) && ($post['start'] >= 0)) {
			$start = $post['start'];
		}

		if (isset($post['limit']) && ($post['limit'] >= 0)) {
			$limit = $post['limit'];
		}

		if (isset($post['sort_id']) && ($post['sort_id'] == 1)) {
			
			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1' GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
		} else if (isset($post['sort_id']) && ($post['sort_id'] == 2)) {
			$query = "SELECT user.*,slike.id as likeid,user.like_count as count FROM `user_sandwich_data` as user LEFT JOIN sandwich_like as slike  on (user.id=slike.sandwich_id) WHERE  user.is_public = '1'   GROUP BY user.sandwich_name,user.sandwich_data ORDER BY count DESC   ";
		} else if (isset($post['sort_id']) && ($post['sort_id'] == 3)) {
			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1'  GROUP BY sandwich_name,sandwich_data ORDER BY menu_add_count DESC   ";
		} else {
			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1' GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
		}

		$query .= " LIMIT $start, $limit";
		$result = $this->db->Query($query);

		return $this->db->FetchAllArray($result);
	}
	
	// And the guts in dev...
	public function filter_seacrh_ajax($post){
		
		
		$post = $this->process_input($post);
		
		
		$start = $limit = 0;
		if (isset($post['limit']) && ($post['limit'] >= 0)) {
			$limit = $post['limit'];
		}
		if (isset($post['sortBy']) && ($post['sortBy'] == 1)) {
			$orderQuery = "ORDER BY date_of_creation DESC";
		} else if (isset($post['sortBy']) && ($post['sortBy'] == 2)) {
			$orderQuery = "ORDER BY count DESC";
		} else if (isset($post['sortBy']) && ($post['sortBy'] == 3)) {
			$orderQuery = "ORDER BY menu_add_count DESC";
		} else {
			$orderQuery = "ORDER BY date_of_creation DESC";
		}

		
		 $query = "SELECT user.*,slike.id as likeid,user.like_count as count FROM `user_sandwich_data` as user LEFT JOIN sandwich_like as slike  on (user.id=slike.sandwich_id) WHERE  user.is_public = '1' AND user.id IN (".$post['id'].")  GROUP BY user.id ".$orderQuery;
		 
		 
		 $result = $this->db->Query($query);
		 return $this->db->FetchAllArray($result);
	}
	
	public function filter_seacrh_ajax_more($post){
		$post = $this->process_input($post);
		
		if (isset($post['limit']) && ($post['limit'] >= 0)) {
			$limit = $post['limit'];
		}
		if (isset($post['start']) && ($post['start'] >= 0)) {
			$start = $post['start'];
		}
		if (isset($post['sortBy']) && ($post['sortBy'] == 1)) {
			$orderQuery = "ORDER BY count DESC";
		} else if (isset($post['sortBy']) && ($post['sortBy'] == 2)) {
			$orderQuery = "ORDER BY date_of_creation DESC";
		} else if (isset($post['sortBy']) && ($post['sortBy'] == 3)) {
			$orderQuery = "ORDER BY menu_add_count DESC";
		} else {
			$orderQuery = "ORDER BY count DESC";
		}
		 
		if (isset($post['sort_id']) && ($post['sort_id'] == 1)) {

			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1' GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
		} else if (isset($post['sort_id']) && ($post['sort_id'] == 2)) {
			$query = "SELECT user.*,slike.id as likeid,user.like_count as count FROM `user_sandwich_data` as user LEFT JOIN sandwich_like as slike  on (user.id=slike.sandwich_id) WHERE  user.is_public = '1'   GROUP BY user.sandwich_name,user.sandwich_data ORDER BY count DESC   ";
		} else if (isset($post['sort_id']) && ($post['sort_id'] == 3)) {
			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1'  GROUP BY sandwich_name,sandwich_data ORDER BY menu_add_count DESC   ";
		} else {
			$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1' GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
		} 
		$query .= " LIMIT $start, $limit";
		

		return $query;

		
		 $result = $this->db->Query($query);
		 return $this->db->FetchAllArray($result);
		
	}
	
	public function filter_seacrh_ajax_more_admin($post){
		$post = $this->process_input($post);

		if (isset($post['limit']) && ($post['limit'] >= 0)) {
			$limit = $post['limit'];
		}
		if (isset($post['start']) && ($post['start'] >= 0)) {
			$start = $post['start'];
		}
		$query = "SELECT * FROM `user_sandwich_data` WHERE  is_public = '1' GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
		 $query .= " LIMIT $start, $limit";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	
	public function filter_seacrh_ajax_admin($post){
		$post = $this->process_input($post);
		$start = $limit = 0;
		if (isset($post['limit']) && ($post['limit'] >= 0)) {
			$limit = $post['limit'];
		}
		if (isset($post['sortBy']) && ($post['sortBy'] == 1)) {
			$orderQuery = "ORDER BY date_of_creation DESC";
		} else if (isset($post['sortBy']) && ($post['sortBy'] == 2)) {
			$orderQuery = "ORDER BY count DESC";
		} else if (isset($post['sortBy']) && ($post['sortBy'] == 3)) {
			$orderQuery = "ORDER BY menu_add_count DESC";
		} else {
			$orderQuery = "ORDER BY date_of_creation DESC";
		}

		
		if (isset($post['start']) && ($post['start'] >= 0)) {
			$start = $post['start'];
		}

		$query = "SELECT user.*,slike.id as likeid,user.like_count as count FROM `user_sandwich_data` as user LEFT JOIN sandwich_like as slike  on (user.id=slike.sandwich_id) WHERE  user.is_public = '1' AND user.id IN (".$post['id'].")  GROUP BY user.id ";

		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	public  function getAllsandwichDetailsSearch()
	{

		if( isset($_POST['sort_id']) )
			$sort_id = $_POST['sort_id'];

		if ( $sort_id == 1 ) {
			$orderQuery = "ORDER BY date_of_creation DESC";
		} else if ( $sort_id == 2 ) {
			$orderQuery = "ORDER BY like_count DESC";
		} else if ( $sort_id == 3 ) {
			$orderQuery = "ORDER BY menu_add_count DESC";
		} else {
			$orderQuery = " ORDER BY sandwich_name ASC ";
		}

		$query = "SELECT id,sandwich_data FROM `user_sandwich_data` WHERE is_public = '1' ";
		$noDuplicates = " GROUP BY sandwich_name  $orderQuery LIMIT 2500"; //for avoiding duplicate sandwiches 
		$query .= $noDuplicates;

		//return $query;
		// TODO - Why not distinct?
		$this->db->Query($query);
		$result = $this->db->FetchAllArray();
		
		return $result;	
	}
	
	/**
	 * List user gallery data
	 */
	public function get_gallery_data_admin($post)
	{
            $user_id = $post['uid'];
            $where = '';
            if($user_id != ''){
                $where = "WHERE user_sandwich_data.uid =".$user_id;
            }
		$query  = "SELECT * FROM `user_sandwich_data` ".$where." ORDER BY menu_add_count DESC";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Update user gallery data
	 */
	public function update_sandwich_data($post)
	{
		$post = $this->process_input($post);

		$chk    = "SELECT * FROM user_sandwich_data  WHERE id = '" . @$post['id'] . "' ";
		$result = $this->db->Query($chk);
		$data   = $this->db->FetchAllArray($result);
		if ($post['menu_active'] == 1) {
			$menu_add_count = $data[0]['menu_add_count'] + 1;
		} else {
			$menu_add_count = $data[0]['menu_add_count'];
		}
		$post['description'] = isset($post['description']) ? $post['description'] : '';
		$date   = date('Y-m-d H:i:s');

		$name = htmlspecialchars_decode($post['name']);
		if (strpos($name, "’") !== false) 
		{
		    $name = str_replace("’","\'",$name);
		}

			$query  = "UPDATE `user_sandwich_data` SET menu_toast = '" . $post['toast'] . "', is_public = '" . $post['is_pub'] . "' , sandwich_name = '" . $name ."' , sandwich_items = '" . $post['sandwich_items'] . "', description = '" . $post['description'] . "' , sandwich_data = '" . $post['data'] . "' , date_of_creation  = '" . $date . "' , sandwich_price  = '" . $post['price'] . "', menu_is_active = '" . $post['menu_active'] . "', menu_add_count = '" . $menu_add_count . "'  WHERE id = '" . $post['id'] . "' AND uid = '" . $post['uid'] . "' ";

			$result = $this->db->Query($query);
			return $post['id'];
		
	}
	/**
	 * Process input
	 */
	private function process_input($data)
	{

		foreach($data as $key => $value){
	        //If $value is an array.
	        if(is_array($value)){
	            //We need to loop through it.
	            $data[$key] = $this->process_input($value);
	        } else{
	            $data[$key] = urldecode($value);
	            $data[$key] = $this->clean_for_sql($data[$key]);
	        }
	    }
	  	return $data;
	}
	/**
	 * Trim post values
	 */
	private function realEscape($data)
	{

		array_walk($data, function(&$val, $key)
		{
			array_walk($val, function(&$vals, $keys)
			{
				$vals = stripslashes($vals);
				$vals = $this->clean_for_sql($vals);
			});
		});
		return $data;
	}
	/**
	 * List user sandwich data
	 */
	public function get_menu_data($post)
	{
		$post   = $this->process_input($post);
		$uid    = $post['uid'];
		
		$limit = "";
		if(isset($post['limit']) && $post['limit']){
			
			$start = 0;
			if(isset($post['start']) && $post['start']){
				$start = $post['start'];	
			}
			
			$limit = " LIMIT $start, {$post['limit']}";
		}
		
		$query  = "SELECT a.*,b.first_name,b.last_name FROM user_sandwich_data a JOIN users b on a.uid = b.uid WHERE  a.uid  = '" . $uid . "' AND menu_is_active = '1' ORDER BY date_of_creation DESC $limit";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List user sandwich ids
	 */
	public function get_user_sanwich_ids($post)
	{
		$post   = $this->process_input($post);
		$uid    = $post['uid'];
		
		$query  = "SELECT id FROM user_sandwich_data WHERE  uid  = '" . $uid . "' AND menu_is_active = '1'";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List orders items
	 */
	public function get_checkout_data($post)
	{
		$post      = $this->process_input($post);
		$date_time = date("Y-m-d h:i:s");
		
		$query     = "DELETE FROM orders_items WHERE ordered=0 AND TIMESTAMPDIFF(HOUR, date_time, '$date_time') > " . DELETE_ORDER_HOUR . " AND user_id = {$post['uid']}";
		$this->db->Query($query);

		$query  = "SELECT * FROM `orders_items`  WHERE user_id = '" . $post['uid'] . "' AND ordered=0";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Add Order items
	 */
	public function add_order_items($post)
	{
		$uid       = $post['uid'];
		$id        = $post['itemid'];
		$data_type = $post['data_type'];
		$qty       = $post['qty'];
		$price     = $post['price'];
		$toast     = $post['toast'];

		$total_price = $price;

		$descriptor_ids = $extra_ids = 0;
		$extra_ids_cost = 0;


		if(isset($post['spcl_instructions']))
		{
			$spcl_instructions = preg_replace('/[^A-Za-z0-9\-]/', ' ', $post['spcl_instructions']);
		}

		if (isset($post['extra_id']))
		{
			$option_price = $post['extra_id'];
		}

		$opt=explode('%2C', $option_price); 

		$option=array();
		$price=array();
		foreach($opt as $key=>$val)
		{
			if($key % 2 == 0)
			{
				$option[]=$val;
			}
			else
			{
				$price[]=$val;
			}
		}

		$opt_fin=implode(', ', preg_replace('/[^A-Za-z0-9\-]/', ' ', $option));
		$prc_fin=array_sum($price);

		if ($extra_ids) 
		{
			$query  = "select SUM(cost) as cost FROM product_extras WHERE id IN ($extra_ids)";
			$result = $this->db->Query($query);
			$result = $this->db->FetchObjectRow($result);
			$extra_ids_cost = $result->cost;
		}

		$select_query = "SELECT * from orders_items WHERE user_id='" . $uid . "' AND item_id='" . $id . "' AND item_type = '" . $data_type . "' AND ordered = '0'  ";

		$result_query = $this->db->Query($select_query);
		$vals         = $this->db->FetchAllArray($result_query);

		if ($vals) 
		{
			$checkupdate = 0;
			foreach ($vals as $val) 
			{
				if (isset($val['item_id'])) 
				{
					$item_id = $val['item_id'];
				}

				$type = $val['item_type'];
				
				if(($type == "product") && ($id == $item_id) && ($opt_fin == $val['extra_id']) && ($toast == $val['toast']) )
                {
                    if ( $spcl_instructions != $val['spcl_instructions'] || $opt_fin != $val['extra_id']) 
                    {
                        $date = date("Y-m-d h:i:s");
                        $query = "INSERT INTO orders_items (`item_id`,`item_type`,`user_id` ,`item_qty` ,`item_price` ,`item_tax` , `toast`, descriptor_id, extra_id, extra_cost,spcl_instructions, date_time ) VALUES ('" . $id . "','" . $data_type . "','" . $uid . "','" . $qty . "','" . $total_price . "','0','" . $toast . "', '$descriptor_ids', '".$opt_fin."', '".$prc_fin."','".$spcl_instructions."', '$date') ";
                        
                        $result = $this->db->Query($query);
                        $order_id = $this->db->insertId();
                        $checkupdate = 1;
                    }
                    else
                    {
                        $query = "UPDATE orders_items SET item_qty = item_qty + $qty WHERE user_id='" . $uid . "' AND item_id='" . $id . "' AND item_type = '" . $data_type . "' AND extra_id='$opt_fin' AND ordered = '0'";
                        
                        $this->db->Query($query);
                        $order_id    = $val['order_item_id'];
                        $checkupdate = 1;
                    }
                }
                else
                {
                    if ($id == $item_id && $opt_fin == $val['extra_id'] && $toast == $val['toast']) 
                    {
                       $query = "UPDATE orders_items SET item_qty = item_qty + $qty WHERE user_id='" . $uid . "' AND item_id='" . $id . "' AND item_type = '" . $data_type . "' AND extra_id='$opt_fin' AND ordered = '0'";

                        $this->db->Query($query);
                        $order_id    = $val['order_item_id'];
                        $checkupdate = 1;
                    }
                    
                }
			}

			if ($checkupdate == 0) 
			{
				$date_time = date("Y-m-d h:i:s");
				$query     = "INSERT INTO orders_items (`item_id`,`item_type`,`user_id` ,`item_qty` ,`item_price` ,`item_tax` , `toast`, descriptor_id, extra_id, extra_cost,spcl_instructions,date_time ) VALUES ('" . $id . "','" . $data_type . "','" . $uid . "','" . $qty . "','" . $total_price . "','0','" . $toast . "', '$descriptor_ids', '".$opt_fin."', '".$prc_fin."' ,'".$spcl_instructions."', '$date_time') ";
				$result    = $this->db->Query($query);
				$order_id  = $this->db->insertId();
			}
		} 
		else 
		{
			$date_time = date("Y-m-d h:i:s");
			$query     = "INSERT INTO orders_items (`item_id`,`item_type`,`user_id` ,`item_qty` ,`item_price` ,`item_tax` , `toast`, descriptor_id, extra_id, extra_cost,spcl_instructions,date_time) VALUES ('" . $id . "','" . $data_type . "','" . $uid . "','" . $qty . "','" . $total_price . "','0','" . $toast . "', '$descriptor_ids', '".$opt_fin."', '".$prc_fin."','".$spcl_instructions."', '$date_time') ";

			$result   = $this->db->Query($query);
			$order_id = $this->db->insertId();
		}

		$query = "SELECT SUM(item_qty) as count FROM `orders_items`  WHERE user_id = '" . $uid . "' AND ordered=0";

		$result = $this->db->Query($query);
		$result = $this->db->FetchObjectRow($result);
		return array(
				"count" => $result['count'],
				"order_id" => $order_id
		);
	}
	
	

	

	
	/**
	 * Add order address
	 */
	public function insert_address_data($post)
	{
		$post   = $this->process_input($post);
		$query  = "INSERT INTO `order_address` (uid,name,company,address1,address2,street,cross_streets,phone,extn,zip,delivery_instructions) VALUES('" . $post['user_id'] . "', '" . $post['name'] . "', '" . $post['company'] . "', '" . $post['address1'] . "', '" . $post['address2'] . "', '" . $post['street'] . "', '" . $post['cross_streets'] . "','" . $post['phone'] . "','" . $post['extn'] . "','" . $post['zip'] . "','" . $post['delivery_instructions'] . "')";
		$result = $this->db->Query($query);
		return $result;
	}
	/**
	 * TRUNCATE order items
	 */
	public function remove_placed_orders($post)
	{
		$post   = $this->process_input($post);
		$query  = "TRUNCATE TABLE `orders_items`";
		$result = $this->db->Query($query);
		return $result;
	}
	/**
	 * Remove cart items
	 */
	public function remove_cart_item($post)
	{
		$post          = $this->process_input($post);
		$order_item_id = $post['order_item_id'];
		$uid           = $post['uid'];
		$query         = "DELETE FROM orders_items WHERE order_item_id = $order_item_id AND user_id = $uid";
		$result        = $this->db->Query($query);
		return $result;
	}
	/**
	 * List admin gallery
	 */
	public function get_admin_gallery_data($post)
	{
		$post  = $this->process_input($post);

		$start = $limit = 0;
		if (isset($post['start']) && ($post['start'] >= 0)) {
			$start = $post['start'];
		}
		
		if (isset($post['limit']) && ($post['limit'] >= 0)) {
			$limit = $post['limit'];
		}

		$query = "SELECT * FROM `user_sandwich_data` WHERE is_public = '1' ";
		
		if (isset($post['sort_id']) && ($post['sort_id'] == 0)) 
		{
			if (isset($post['name']) && ($post['name'] != '')) 
			{
				$query .= "AND sandwich_name  LIKE '%".$post['name']."%' GROUP BY sandwich_name,sandwich_data ORDER BY date_of_creation DESC"; 
			}
			else
			{
				$query .= " GROUP BY sandwich_name,sandwich_data ORDER BY flag DESC, date_of_creation DESC";
			}
			
		}
		
		else if (isset($post['sort_id']) && ($post['sort_id'] == 1)) 
		{
			$query .= "GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
		} 
		else if (isset($post['sort_id']) && ($post['sort_id'] == 2)) 
		{
			$query = "SELECT user.*,slike.id as likeid,user.like_count as count FROM `user_sandwich_data` as user LEFT JOIN sandwich_like as slike  on (user.id=slike.sandwich_id) WHERE  user.is_public = '1'   GROUP BY user.sandwich_name,user.sandwich_data ORDER BY count DESC   ";
		} 
		else if (isset($post['sort_id']) && ($post['sort_id'] == 3)) 
		{
			$query .= "GROUP BY sandwich_name,sandwich_data ORDER BY menu_add_count DESC   ";
		} 
		else 
		{
			$query .= "GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
		}
		$query .= " LIMIT $start, $limit";
		$result = $this->db->Query($query);
		$sandwiches =  $this->db->FetchAllArray($result);
		foreach ($sandwiches as $key => $sandwich) {
			$query             = "SELECT COUNT(id) as count FROM sandwich_like WHERE sandwich_id = {$sandwich['id']}";
			$result            = $this->db->Query($query);
			$result            = $this->db->FetchObjectRow($result);
			$sandwich['likes'] = $result->count;
			$sandwiches[$key]  = $sandwich;
		}
		return $sandwiches;
		
	}
	/**
	 * Update admin gallery state
	 */
	public function update_gallery_public_state($post)
	{
		$query = "UPDATE user_sandwich_data SET is_public = '" . @$post['public'] . "' WHERE id = '" . @$post['id'] . "' ";
		return $this->db->Query($query);
	}
	/**
	 * Delete gallery item
	 */
	public function delete_gallery_item($post)
	{
		$query = "DELETE FROM user_sandwich_data WHERE id = '" . @$post['id'] . "' ";
		return $this->db->Query($query);
	}
	/**
	 * Delete menu item
	 */
	public function remove_menu_item($post)
	{
		
		if(isset($post['sType']) && @$post['sType'] == "FS")
		{
			$query = "UPDATE user_sandwich_data SET menu_is_active = '0' WHERE menu_added_from = '" . @$post['id'] . "' AND uid = '" . @$post['uid'] . "' ";
		}
		else
		{
			$query = "UPDATE user_sandwich_data SET menu_is_active = '0' WHERE id = '" . @$post['id'] . "' AND uid = '" . @$post['uid'] . "' ";
		}

		
		return $this->db->Query($query);
	}
	/**
	 * Add to menu
	 */
	public function add_menu_multi($post)
	{

		$chk    = "SELECT * FROM user_sandwich_data  WHERE id = '" . @$post['id'] . "' ";
		$result = $this->db->Query($chk);
		$data   = $this->db->FetchAllArray($result);
		$date   = date('Y-m-d H:i:s');

		$menu_add_count = $data[0]['menu_add_count'] + 1;
		$query          = "UPDATE user_sandwich_data SET menu_add_count =" . $menu_add_count . " WHERE id = '" . @$post['id'] . "' ";
		$this->db->Query($query);

		$data = $this->realEscape($data);

        if ($data[0]['uid'] == $post['uid']) 
        {
            $query = "UPDATE user_sandwich_data SET menu_toast ='" . @$post['toast_sandwich'] . "' , menu_is_active = '1' WHERE id = '" . @$post['id'] . "' ";
        } 
        else 
        {
            $query = "INSERT INTO user_sandwich_data (menu_toast,uid,sandwich_name,sandwich_data,sandwich_price,date_of_creation,is_public,menu_is_active,description,menu_added_from) VALUES ('" . @$post['toast_sandwich'] . "','" . $post['uid'] . "','" . $data[0]['sandwich_name'] . "','" . $data[0]['sandwich_data'] . "','" . $data[0]['sandwich_price'] . "','" . $date . "','" . $data[0]['is_public'] . "','1','" . $data[0]['description'] . "','" . @$post['id'] . "')";
        }

        file_put_contents("query.txt", print_r($query, true));
		$this->db->Query($query);
		$insid = $this->db->insertId();
		if (!$insid)
			$insid = 0;

		$arr = array(
				'id' => $insid,
				'image' => 'sandwich_' . $data[0]['id'] . '_' . $data[0]['uid'] . '.png',
				'uid' => $data[0]['uid']
		);
		return $arr;

	}
	/**
	 * Update sandwich flag
	 */
	public function toggle_flag($post)
	{
		$query = "UPDATE  user_sandwich_data SET flag = '" . $post['state'] . "' WHERE id = '" . $post['id'] . "'  ";
		return $this->db->Query($query);
	}
	/**
	 * List Sandwich Category Items
	 */
	public function getSandwichCategoryItems()
	{

		$query  = "SELECT * FROM `sandwich_category_items` WHERE live=1 ORDER BY priority ASC";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List Sandwich Category
	 */
	public function getSandwichCategories()
	{

		$query = "SELECT * FROM `sandwich_categories`";
		
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Add sandwich likes
	 */
	public function addLike($post)
	{

		$query = "SELECT id FROM `sandwich_like` WHERE sandwich_id=" . $post['id'] . " AND u_id=" . $post['uid'] . "";

		$result    = $this->db->Query($query);
		$resultnew = $this->db->FetchArray($result);

		if ($resultnew) {
			return false;
		} else {
			$query  = "UPDATE `user_sandwich_data` SET like_count=like_count+1 WHERE id=" . $post['id'] . "";
			$result = $this->db->Query($query);

			$query = "INSERT INTO sandwich_like (sandwich_id,u_id) values (" . $post['id'] . "," . $post['uid'] . ")";
			$this->db->Query($query);
			return $this->db->insertId();
		}


	}
	/**
	 * List sandwich likes
	 */
	public function get_sandwich_like($post)
	{
		if (isset($post['id']) && isset($post['uid'])) {
			$query  = "SELECT id FROM `sandwich_like` WHERE sandwich_id=" . $post['id'] . " AND u_id=" . $post['uid'] . "";
			$result = $this->db->Query($query);
			return $this->db->FetchAllArray($result);
		}
	}
	/**
	 * Count sandwich likes
	 */
	public function get_sandwich_like_count($post)
	{
		$query  = "SELECT id,sandwich_id,COUNT(*) AS count FROM `sandwich_like` WHERE sandwich_id=" . $post['id'] . "  GROUP BY `sandwich_id` ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Update Toast menu
	 */
	public function updateToastmenu($post)
	{
		$query = "UPDATE user_sandwich_data SET menu_toast = '" . @$post['toast'] . "' WHERE id = '" . @$post['item_id'] . "' AND menu_is_active = 1 ";
		$this->db->Query($query);
		return $post['toast'];
	}
	/**
	 * List all images
	 */
	public function get_all_images($post)
	{
		$query  = "SELECT item_image,image_round,image_long,image_trapezoid,item_image_sliced FROM sandwich_category_items GROUP BY item_image";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List category items
	 */
	public function get_category_items_data($post)
	{
		$post      = $this->process_input($post);
		$id_string = $post['ids'];
		$query     = "SELECT `id`,`category_id`,`options_id`,`option_images`,`item_price`,`image_round`,`image_trapezoid`,`image_long`,`item_name`  FROM `sandwich_category_items` WHERE `id` IN ($id_string)";

		$result    = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List purchase count
	 */
	public function get_purchase($post)
	{
		$post   = $this->process_input($post);
		$query  = "SELECT COUNT( * ) AS count FROM `orders_items` WHERE `item_id` = '" . $post['id'] . "' AND `item_type` = 'user_sandwich' AND `orderid` != '0'";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List random name
	 */
	public function get_random_name()
	{
		$query  = "SELECT name FROM `sandwich_fun_words` ORDER BY rand() limit 1";
		$result = $this->db->Query($query);
		return $this->db->FetchArray($result);
	}
	/**
	 * List sandwich name
	 */
	public function get_sandwich_name($post)
	{
		$post   = $this->process_input($post);
		$query  = "SELECT sandwich_name FROM `user_sandwich_data` WHERE id = '" . $post['id'] . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchArray($result);
	}
	/**
	 * List last order
	 */
	// public function getLastOrder($post)
	// {
	// 	$post   = $this->process_input($post);
	// 	$query  = "SELECT *  FROM `orders` WHERE user_id = '" . $post['uid'] . "' ORDER BY `order_id` DESC LIMIT 0 , 1 ";
	// 	$result = $this->db->Query($query);
	// 	return $this->db->FetchArray($result);
	// }

	/**
	 * List last order
	 */
	public function getLastOrder($post)
	{
		$post   = $this->process_input($post);
		//$query  = "SELECT *  FROM `orders` WHERE user_id = '" . $post['uid'] . "' ORDER BY `order_id` DESC LIMIT 0 , 1 ";
		$query  = "SELECT * FROM `orders` WHERE `user_id`='" . $post['uid'] . "' AND `date` NOT BETWEEN '2020-09-04 00:00:01' AND '2020-10-06 23:59:59' ORDER BY `order_id` DESC LIMIT 0 , 1 ";
		$result = $this->db->Query($query);
		$order = $this->db->FetchArray($result);
		$order['itemCheck'] = $this->checkOrderItems($order['order_item_id']);
		
		//return $this->db->FetchArray($result);
		return $order;
	}

	public function checkOrderItems($order_item_id)
	{
		$data  = explode(":", $order_item_id);
		$itmid = implode(',', $data);
		$this->db->Query("SELECT count(order_item_id) as items FROM `orders_items` WHERE `order_item_id` IN (" . $itmid . ") ");
		$count = $this->db->FetchArray();
		return $count;
	}
	/**
	 * List Order Sandwich Banner Image
	 */
	public function OrderSandwichBannerImage($post)
	{
		$post   = $this->process_input($post);
		$query  = "SELECT `B`.`item_id` FROM `orders` AS `A` JOIN `orders_items` AS `B` ON `A`.`order_item_id` = `B`.`order_item_id` WHERE `A`.`user_id` = '" . $post['uid'] . "' AND `B`.`item_type` = 'user_sandwich' ORDER BY `A`.`date` DESC LIMIT 0 , 1";
		$result = $this->db->Query($query);
		$data   = $this->db->FetchArray($result);
		if ($data['item_id']) {

			return 'upload/sandwicth/' . $post['uid'] . '/' . 'sandwicth_' . $data['item_id'] . '_' . $post['uid'] . '.png';
		} else {
			return false;
		}
	}
	/**
	 * List address street
	 */
	public function get_address_street($post)
	{
		$state  = 0; 
		$post   = $this->process_input($post);
		if(isset($post['is_delivery'])) $state = $post['is_delivery']; else $state = 0;
		if($state == 1){ 
		$query  = "SELECT `address1` 	FROM `order_address` WHERE `address_id` = '" . $post['id'] . "' ";
		} else {
		$query  = "SELECT *  FROM `pickup_stores` WHERE `id` = '" . $post['id'] . "' ";
		}
		$result = $this->db->Query($query);
		return $this->db->FetchArray($result);
	}
	/**
	 * List banner order details
	 */
	public function banner_order_details($post)
	{

		$post  = $this->process_input($post);
		
		$data  = explode(":", $post['id_string']);
		$itmid = implode(',', $data);


		$this->db->Query("SELECT * FROM `orders_items` WHERE `order_item_id` IN (" . $itmid . ") ");
		$ordreDat = $this->db->FetchAllArray();
		$oredrID  = $ordreDat[0]['orderid'];

		$this->db->Query("SELECT `delivery` FROM `orders` WHERE `order_id`  = '$oredrID' ");
		$delivery = $this->db->FetchArray();

		$orderDatas = array();
		$itms       = array();
		$img        = null;
		$by_admin = null;
		$sandwich_desc = array();
		$sandwich_desc_id = array();
		foreach ($ordreDat as $order) {
			$itmid = $order['item_id'];
			if ($order['item_type'] == 'user_sandwich') {
				$sanwh = "SELECT sandwich_name,uid,by_admin FROM `user_sandwich_data` WHERE `id` = '$itmid'";
				$this->db->Query($sanwh);
				$san      = $this->db->FetchArray();
				$itms[]   = '(' . $order['item_qty'] . ') ' . $san['sandwich_name'];
				$by_admin = $san['by_admin'];
				if ($san['uid'] && $itmid)
					$img = $san['uid'] . '/' . 'sandwich_' . $itmid . '_' . $san['uid'] . '.png';
				else
					$img = '';
			} else if ($order['item_type'] == 'product') {
				$product = "SELECT `product_name` FROM `standard_category_products` WHERE `id` = '$itmid'";
				$this->db->Query($product);
				$prod   = $this->db->FetchArray();
				$itms[] = '(' . $order['item_qty'] . ') ' . $prod['product_name'];
			}
			$sandwich_bread = '';
			if ($order['item_type'] == "user_sandwich") {
					
				$desc_query    = "SELECT * FROM user_sandwich_data WHERE id = '$itmid'";
				$desc_result    = $this->db->query($desc_query);
				$item_desc = $this->db->FetchAllArray($desc_result);
				if(!empty($item_desc[0]['sandwich_items'])){
					$expbrd= explode(',', $item_desc[0]['sandwich_items']);
					$sandwich_bread = $expbrd[0];
				}
				if(count($item_desc)>0){  
				     $sandwich_descs = $this->process_user_data($item_desc);
				     
				     //$sandwich_desc = $sandwich_descs['data'];
				     $get_current_price_array = array("id"=>$itmid,"bread"=>$sandwich_bread,"sandwich_desc"=>$sandwich_descs['data'][0]['sandwich_desc'],"sandwich_desc_id"=>$sandwich_descs['data'][0]['sandwich_desc_id']);
				     $this->Getcurrentprice($get_current_price_array);
				     array_push($sandwich_desc, $sandwich_descs['data'][0]['sandwich_desc']);
				     array_push($sandwich_desc_id, $sandwich_descs['data'][0]['sandwich_desc_id']);
			    } else {
			    	$sandwich_desc = $sandwich_desc;
			    }
			}else{
				$sandwich_desc = $sandwich_desc;
			}

		}
		
		if ($order['item_type'] == "user_sandwich") {
			$orderDatas['desc']    	   = $sandwich_desc;
			//$orderDatas['desc']    	   = $sandwich_desc[0]['sandwich_desc'];
			$orderDatas['desc_id']    	   = $sandwich_desc_id;
			//$orderDatas['desc_id']    	   = $sandwich_desc[0]['sandwich_desc_id'];
			$orderDatas['bread']       = $sandwich_bread;
			$orderDatas['item_id']       = $item_desc[0]['id'];
		}

		$orderDatas['items']       = $itms;
		$orderDatas['image']       = $img;
		$orderDatas['by_admin']    = $by_admin;
		$orderDatas['is_delivery'] = $delivery['delivery'];


		return $orderDatas;
	}

	public function process_user_data($data)
	{
		
		$self  = $this;
		array_walk($data,function(&$value,$key) use($self){
			$value = get_object_vars((object)$value);
			$array = get_object_vars(json_decode($value['sandwich_data']));
			array_walk($array,function(&$val,$key){
				$val = get_object_vars($val);
			});
			$bread     = $array['BREAD']['item_name'][0];
			$protein   = $self->getCombinedString($array['PROTEIN']['item_qty']); 
			$cheese    = $self->getCombinedString($array['CHEESE']['item_qty']);  
			$topping   = $self->getCombinedString($array['TOPPINGS']['item_qty']); 
			$condiment = $self->getCombinedString($array['CONDIMENTS']['item_qty']);  

			// item ids 
			$bread_id     = $array['BREAD']['item_id'][0];
			$protein_id   = implode(', ',$array['PROTEIN']['item_id']); 
			$cheese_id    = implode(', ',$array['CHEESE']['item_id']);  
			$topping_id   = implode(', ',$array['TOPPINGS']['item_id']); 
			$condiment_id = implode(', ',$array['CONDIMENTS']['item_id']);
			    
		    $desc      = $bread.", ".$protein.", ".$cheese.", ".$topping.", ".$condiment; 
			$descData  = explode(', ',$desc);
			$descData  = array_filter($descData);
		    $desc      = implode(', ',$descData);

		    $desc_id = $bread_id.", ".$protein_id.", ".$cheese_id.", ".$topping_id.", ".$condiment_id;
		    $descDataId  = explode(', ',$desc_id);
			$descDataId  = array_filter($descDataId);
		    $desc_id      = implode(', ',$descDataId);

			$value['sandwich_desc'] = $desc;
			$value['sandwich_desc_id'] = $desc_id;
		});

		return array('data'=>$data);
	}
	public function getCombinedString($data)
	{
		$arr = array();
		
		if(is_object($data)){
			$data = get_object_vars($data);
		} else $data = array();
		
		foreach ($data as $key => $data) {
			$arr[] = $key . " (" . $data[1] . ")";
		}
		return implode(', ', $arr);
	}
	/**
	 * List checkout order success data
	 */
	public function get_checkout_order_success_data($post)
	{
		ini_set("display_errors", 1);
		$post      = $this->process_input($post);

	
		$order_det = "SELECT user_id, order_number, `delivery`,`address_id`,`billinginfo_id`,`order_item_id`,`time`,`date`,`total`,`tip`,`delivery_fee`,`off_amount`,`manual_discount`,`sub_total`, tax FROM `orders` WHERE `order_number` = '" . $post['order_id'] . "' ";
		$this->db->Query($order_det);
		$order_det = $this->db->FetchArray();
		
		$query = "SELECT * FROM users WHERE uid = {$order_det['user_id']} ";
		$this->db->Query($query);
		$userData = $this->db->FetchArray();
		
		$delivery_type = $order_det['delivery'];
		$address_id    = $order_det['address_id'];
		$billing_id    = $order_det['billinginfo_id'];
		$order_itm_id  = $order_det['order_item_id'];
		$order_number  = $order_det['order_number'];
		$check_now_specific = $order_det['time'];
		$date_day      = date('D', strtotime($order_det['date']));
		$date_month    = date('M', strtotime($order_det['date']));
		$date_dayNum   = date('d', strtotime($order_det['date']));
		$date_suffix   = $this->ordinalSuffix($date_dayNum);
		$date_year     = date('Y', strtotime($order_det['date']));
		$date_dayz     = $date_dayNum . $date_suffix;

		$total     = $order_det['total'];
		$tip       = $order_det['tip'];
		$delivery_fee = $order_det['delivery_fee'];
		$tax       = $order_det['tax'];
		$discount  = $order_det['off_amount'] + $order_det['manual_discount'];
		$sub_total = $order_det['sub_total'];

		$Ordtime    = date('h:iA', strtotime($order_det['date']));
		$final_date = $date_day . ", " . $date_month . " " . $date_dayz . ", " . $date_year;
		if ($delivery_type)
			$address_data = "SELECT * FROM `order_address` WHERE `address_id` = '" . $address_id . "' ";
		else
			$address_data = "SELECT * FROM `pickup_stores` WHERE `id` = '" . $address_id . "' ";
		$this->db->Query($address_data);
		$address_datas = $this->db->FetchArray();

		$addressData           = $address_datas;
		
		$delivery_instructions = "";
		if (isset($address_datas['delivery_instructions']))
			$delivery_instructions = $address_datas['delivery_instructions'];


		$billing_data = "SELECT * FROM `order_billinginfo` WHERE `id` = '" . $billing_id . "' ";
		$this->db->Query($billing_data);
		$billing_datas = $this->db->FetchArray();

		$billing_type    = $billing_datas['card_type'];
		$billing_card_no = 'XXXX-XXXX-XXXX-' . substr($billing_datas['card_number'], -4);


		$orderIds = explode(':', $order_itm_id);

		$order_details = array();
		$tmp_order_det = array();

		foreach ($orderIds as $ordId) {
			$ordItmDat = "SELECT * FROM `orders_items` WHERE `order_item_id` = '" . $ordId . "' ";
			$this->db->Query($ordItmDat);
			$ordItmData             = $this->db->FetchArray();
			$type                   = $ordItmData['item_type'];
			$itmId                  = $ordItmData['item_id'];
			//$tmp_order_det['price'] = $ordItmData['item_price'];
			$tmp_order_det['price'] = $ordItmData['item_price']+$ordItmData['extra_cost'];
			$tmp_order_det['qty']   = $ordItmData['item_qty'];
			$tmp_order_det['toast']   = $ordItmData['toast'];
			$tmp_order_det['extra_id']   = !empty($ordItmData['extra_id']) ? $ordItmData['extra_id'] : '';
			$tmp_order_det['spcl_instructions']   = !empty($ordItmData['spcl_instructions']) ? $ordItmData['spcl_instructions'] : '';
			$tmp_order_det['type']  = $type;
			if ($type == 'user_sandwich') {
				$itmDeatilquery = "SELECT * FROM `user_sandwich_data` WHERE `id` = '" . $itmId . "' ";
				$this->db->Query($itmDeatilquery);
				$itm_details                    = $this->db->FetchArray();
				$tmp_order_det['sandwich_name'] = $itm_details['sandwich_name'];

				$san_data = json_decode($itm_details['sandwich_data']);

				$bread = $san_data->BREAD->item_name[0];

				$prot = $san_data->PROTEIN->item_name;
				array_walk($prot, function(&$val, $key) use ($san_data)
				{
					if ($san_data->PROTEIN->item_qty->{$val})
						$val = $val . " (" . $san_data->PROTEIN->item_qty->{$val}[1] . ")";
				});

				$prot = implode(', ', $prot);

				$cheese = $san_data->CHEESE->item_name;
				array_walk($cheese, function(&$val, $key) use ($san_data)
				{
					if ($san_data->CHEESE->item_qty->{$val})
						$val = $val . " (" . $san_data->CHEESE->item_qty->{$val}[1] . ")";
				});

				$cheese = implode(', ', $cheese);

				$topping = $san_data->TOPPINGS->item_name;
				array_walk($topping, function(&$val, $key) use ($san_data)
				{
					if ($san_data->TOPPINGS->item_qty->{$val})
						$val = $val . " (" . $san_data->TOPPINGS->item_qty->{$val}[1] . ")";
				});

				$topping = implode(', ', $topping);

				$cond = $san_data->CONDIMENTS->item_name;
				array_walk($cond, function(&$val, $key) use ($san_data)
				{
					if ($san_data->CONDIMENTS->item_qty->{$val})
						$val = $val . " (" . $san_data->CONDIMENTS->item_qty->{$val}[1] . ")";
				});

				$cond = implode(', ', $cond);

				if ($cond)
					$topping = $topping . ", ";
				if ($topping || $cond)
					$cheese = $cheese . ", ";
				if ($cheese || $topping || $cond)
					$prot = $prot . ", ";

				$ingrs = $prot . $cheese . $topping . $cond;

				if ($ingrs)
					$bread = $bread . ", ";

				$ingrs                             = $bread . $ingrs;
				$tmp_order_det['sandwich_details'] = $ingrs;
				unset($tmp_order_det['product_name']);
			} else {
				$itmDeatilquery = "SELECT * FROM `standard_category_products` WHERE `id` = '" . $itmId . "' ";
				$this->db->Query($itmDeatilquery);
				$itm_details                   = $this->db->FetchArray();
				$tmp_order_det['product_name'] = $itm_details['product_name'];
				$tmp_order_det['product_description'] = $itm_details['description'];
				unset($tmp_order_det['sandwich_details']);
				unset($tmp_order_det['sandwich_name']);

			}

			$order_details['order_item_detail'][] = $tmp_order_det;

		}

		$order_details['date']                  = $final_date;
		$order_details['time']                  = $Ordtime;
		$order_details['billing_type']          = $billing_type;
		$order_details['billing_card_no']       = $billing_card_no;
		$order_details['address']               = $addressData;
		$order_details['delivery_instructions'] = $delivery_instructions;
		$order_details['total']                 = $total;
		$order_details['sub_total']             = $sub_total;
		$order_details['tip']                   = $tip;
		$order_details['delivery_fee']         = $delivery_fee;
		$order_details['tax']                   = $tax;
		$order_details['discount']              = $discount;
		$order_details['delivery_type']         = $delivery_type;
		$order_details['check_now_specific']	= $check_now_specific;
		$order_details['userData']				= $userData;
		$order_details['order_number']			= $order_number;
		if($delivery_type){
			$order_details['name']			= $addressData['name'];			
			$order_details['address_title'] = "Delivery Address";
		}else{
			$order_details['name']			= $addressData['store_name'];	
			$order_details['address_title'] = "Store Address";
		}	
				
		
		
		return $order_details;

	}

	/**
	 * Send Order Confirmation Mail
	 */
	public function sendOrderConfirmationMail( $order_details )
	{

		$old_err_level  = error_reporting( );  // suppress NOTICEs
		error_reporting(E_ERROR);
		
		$subject = "Barney Brown Order Confirmation";

		$message = $this->getOrderConfirmationBody($order_details);	
		
			$mailObject = new MailSender();
			$mailObject->setSubject($subject);
			$mailObject->setBody($message);
			$mailObject->setMailFrom(ORDER_MAIL);
			$mailObject->setMailTo($order_details['userData']['username']);
			$response = $mailObject->sendMail();

			if( !$response ){
				$error_message = "sendMail - Order #" . $order_details['order_number']. " - To: " .  $order_details['userData']['username'] . " ID#".$order_details['userData']['uid'];
			//	throw new CustomExceptionApi( ORDER_ERROR_MAIL_ERROR, $error_message );
			}

		error_reporting($old_err_level);  // restore old error levels
		/*
		$file = '/home/barneybrown/www/api/app/model/log/log.txt';
		$myfile = fopen($file, "a") or die("Unable to open file!");
		$txt = $order_details['order_number']." - - - ".date("Y-m-d H:i:s");
		fwrite($myfile, "\n". $txt);
		fclose($myfile);*/
	}
	
	public function getOrderConfirmationBody($order_details){
		
		
			$body = '<html>			
			<head>			
			<title>HA | Order Conformation</title>
			<style type="text/css"> 
			<!--
			.ReadMsgBody { width: 100%;}
			.ExternalClass {width: 100%;}
			.ExternalClass * {line-height: 125%}
			-->
			</style>

			<style type="text/css">
			div, p, a, li, td { -webkit-text-size-adjust:none; }
			</style> 
			</head>
			<body bgcolor="#ffffff">

			<table width="100%" bgcolor="#ffffff">
				<tr>
					<td>
						<table align="center" border="0" width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" style="font-size: 11px; color: #262626; font-family: helvetica, arial, sans-serif;">
							<tr>
								<td>
									<table align="center" border="0" width="600"  cellpadding="0" cellspacing="0" >
										<tr>
										<td align="left" valign="top" style="text-align: center;"><a href="#"><img src="'.LOGO_URL.'app/e-images/logo.png" alt="logo"/></a></td>
										</tr>
									</table>
									
									<table align="center" border="0" width="600"  cellpadding="0" cellspacing="0" style="font-size: 12px; color: #000000; font-family: helvetica, arial, sans-serif; padding-left: 15px; padding-top: 40px;">
										<tr><td>
										Hi '.$order_details['userData']['first_name'].',<br/><br/>
										Thank you for placing your order at barneybrown.com!  If you have any questions or concerns regarding your order, you can reach us at 212-634-9114 or <a style="text-decoration: none; color: #000;" href="mailto:orders@barneybrown.com">orders@barneybrown.com.</a><br/><br/>
										With extra mayo,<br/>The Barney Brown Team</td></tr>
									</table>
									<br/>
									<br/>
									
									<table align="center" border="0" width="600"  cellpadding="0" cellspacing="0" style="font-size: 12px; color: #000000; font-family: helvetica, arial, sans-serif;">
										<tr><th style="font-size: 10px;font-weight: bold; padding: 10px 15px;text-align: left;color: #fff;background-color: #000;">ORDER DETAILS</th></tr>
										<tr><td style="padding-left: 15px; padding-top:20px;"><strong>Order Number:</strong>  '.$order_details['order_number'].' <br/>
										<strong>Date/Time:</strong>  '.$order_details['date'].' at '.$order_details['time'].'<br/>
										<strong>Payment:</strong><span style="text-transform:capitalize;">'.$order_details['billing_type'].'</span> ending in '.substr($order_details['billing_card_no'], -4).'<br/></td></tr>
										<tr><td style="padding-left: 15px; padding-top: 15px;"><strong>'.$order_details['address_title'].':</strong><br/>'.$order_details['name'].'<br/>';
			
			                            if($order_details['address']['company']){
			                        	    $body .= $order_details['address']['company'].'<br/>';
			                            }
			
			                            if( $order_details['address']['address1'] ){ 
			                                $body .= $order_details['address']['address1'].',<br/>';
			                            }
			                           
										if($order_details['address']['street']){
											$body .= $order_details['address']['street'].'<br/>';
										}	
										
										
										if($order_details['address']['cross_street']){
											$body .= $order_details['address']['cross_street'].'<br/>';
										}	
										
										$body .= 'New York, NY '.$order_details['address']['zip'].'<br/>'.$order_details['address']['phone'].'</td></tr>
									</table>						
									<br/>
									<br/>						
									<table align="center" border="0" width="600"  cellpadding="0" cellspacing="0" style="font-size: 12px; color: #000000; font-family: helvetica, arial, sans-serif;font-weight: bold;">
										
										<tr style="font-size: 10px;font-weight: bold; color: #fff;background-color: #000;">						
										<th style="width: 420px; padding:10px 0 10px 15px; text-align: left;">ITEMS</th>
										<th style="width: 52px; text-align: center; padding:10px 0;">QTY</th>
										<th style=" text-align: right;width: 100px; padding:10px 20px 10px 0;">PRICE</th>
										</tr>
									';
									
									foreach($order_details['order_item_detail'] as $order_item){
										
										$toasted = "";$extra_id="";$spcl_instructions="";
										if(isset($order_item['toast']) && $order_item['toast'])
											$toasted = "<br/> <strong><i>Toasted</i></strong>";
										if(isset($order_item['extra_id']) && $order_item['extra_id'])
											$extra_id = '<br/> <strong>Modifiers: '.$order_item['extra_id'].'</strong>';
										if(isset($order_item['spcl_instructions']) && $order_item['spcl_instructions'])
											$spcl_instructions = '<br/><strong>Special Instructions: '.$order_item['spcl_instructions'].'</strong>';
										
										$body .= '
																		<tr>
										<td style="padding-left: 15px; padding-top:20px; font-weight: normal;">
										<strong>'.stripslashes($order_item['sandwich_name']).stripslashes($order_item['product_name']).'</strong> <br/>
											'.$order_item['sandwich_details'].$toasted.$extra_id.$spcl_instructions.'																
										</td>
										<td style="padding-top:20px;vertical-align: top;text-align: center;">'.$order_item['qty'].'</td>
										<td style="padding-top:20px;vertical-align: top;text-align: right;padding-right: 20px;">$'. number_format( ($order_item['price'] * $order_item['qty']), 2, '.', '').'</td>
										
										</tr>
										';
									}
										
							$body .=	'<tr>
										<td>&nbsp;</td>
										<td style="text-align: right;">Subtotal</td>
										<td style="text-align: right;padding-right: 20px;">$'.number_format($order_details['sub_total'], 2, '.', '').'</td>
										</tr>
                                                                          <tr>
										<td>&nbsp;</td>
										<td style="text-align: right;">Tax</td>
										<td style="text-align: right;padding-right: 20px;">$'.number_format($order_details['tax'], 2, '.', '').'</td>
										</tr>      
                                                                            ';
							if($order_details['discount']>0) {
							$body.=		'<tr>
									<td>&nbsp;</td>
									<td style="text-align: right;">Discount</td>
									<td style="text-align: right;padding-right: 20px;">- $'.number_format($order_details['discount'], 2, '.', '').'</td>
									</tr>';
							}
							$body.=		'<tr>
										<td>&nbsp;</td>
										<td style="text-align: right;">Delivery</td>
										<td style="text-align: right;padding-right: 20px;">$'.number_format($order_details['delivery_fee'], 2, '.', '').'</td>
										</tr>
										<tr>
										<td>&nbsp;</td>
										<td style="text-align: right;">Tip</td>
										<td style="text-align: right;padding-right: 20px;">$'.number_format($order_details['tip'], 2, '.', '').'</td>
										</tr>
										
										<tr>
										<td style="padding-top: 15px;">&nbsp;</td>
										<td style="padding-top: 15px;text-align: right;">Total</td>
										<td style="padding-top: 15px; text-align: right;padding-right: 20px;">$'.number_format($order_details['total'], 2, '.', '').'</td>
										</tr>
										
									</table>
									
									<table width="280" border="0" align="center" cellpadding="0" cellspacing="0"  style="padding-top: 70px;">
										<tr>
											<td><a href="https://itunes.apple.com/us/app/barney-brown/id1097260883?mt=8" target="_blank"><img src="'.LOGO_URL.'app/e-images/app-store.png" alt=""/></a></td>
											<td><a href="https://play.google.com/store/apps/details?id=com.barneybrown" target="_blank"><img src="'.LOGO_URL.'app/e-images/google-play.png" alt=""/></a></td>
										</tr>
									</table>
									
									<table width="150" border="0" align="center" cellpadding="0" cellspacing="0"  style="padding-top: 15px;">
										<tr>
											
											<td><a href="https://www.facebook.com/barneybrownusa/"><img src="'.LOGO_URL.'app/e-images/fb.png" alt=""/></a></td>
											<td><a href="https://www.instagram.com/barneybrownusa/"><img src="'.LOGO_URL.'app/e-images/instagram.png" alt=""/></a></td>
											<td><a href="https://www.twitter.com/barneybrownusa/"><img src="'.LOGO_URL.'app/e-images/twitter.png" alt=""/></a></td>
											
										</tr>
									</table>
									
									<table align="center" border="0" width="600"  cellpadding="0" cellspacing="0" style="font-size: 12px; color: #000000; font-family: helvetica, arial, sans-serif;padding-top: 10px; font-weight: bold;">
										<tr>
											<td style="text-align: center;"><a style="color: #000; text-decoration: none;" href="#">www.barneybrown.com</a></td>
										</tr>
									</table>
									
									<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" >
										<tr>
											<td>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			</body>
			</html>	
					';	
					
			return $body;		
			
	}

	/**
	 * List order popup data
	 */
	
	public function get_order_popup_data($post)
	{
		$post      = $this->process_input($post);
		//$order_det = "SELECT `time`,`address_id`,`store_id`,`authorizenet_transaction_id`,`billinginfo_id`,`order_item_id`,`date`, `order_date`, `total`,`tip`,`off_amount`,`sub_total`,`delivery` FROM `orders` WHERE `order_number` = '" . $post['order_id'] . "' ";
		$order_det = "SELECT o.time, o.address_id,o.store_id,o.authorizenet_transaction_id,o.billinginfo_id,o.order_item_id,o.date, o.order_date, o.total,o.tip,o.off_amount,o.sub_total,o.delivery, u.first_name, u.last_name FROM orders o JOIN users u ON o.user_id = u.uid WHERE o.order_number = '" . $post['order_id'] . "' ";
		$this->db->Query($order_det);
		$order_det = $this->db->FetchArray();


		$authorizenet_transaction_id = $order_det['authorizenet_transaction_id'];
		$address_id                  = $order_det['address_id'];
		$billing_id                  = $order_det['billinginfo_id'];
		$order_itm_id                = $order_det['order_item_id'];
		$date_day                    = date('D', strtotime($order_det['date']));
		$date_month                  = date('M', strtotime($order_det['date']));
		$date_dayNum                 = date('d', strtotime($order_det['date']));
		$date_suffix                 = $this->ordinalSuffix($date_dayNum);
		$date_year                   = date('Y', strtotime($order_det['date']));
		$date_dayz                   = $date_dayNum . $date_suffix;

		$total        = $order_det['total'];
		$tip          = $order_det['tip'];
		$discount     = $order_det['off_amount'];
		$sub_total    = $order_det['sub_total'];
		$specificTime = $order_det['time'];

		$storeId     = $order_det['store_id'];
		$is_delivery = $order_det['delivery'];

		$Ordtime    = date('H:iA', strtotime($order_det['date']));
		$final_date = $date_day . ", " . $date_month . " " . $date_dayz . ", " . $date_year;

		$addressData           = array();
		$delivery_instructions = null;

		if ($is_delivery) {
			$address_data = "SELECT * FROM `order_address` WHERE `address_id` = '" . $address_id . "' ";
			$this->db->Query($address_data);
			$address_datas                = $this->db->FetchArray();
			$addressData["name"]          = $address_datas['name'];
			$addressData["company"]       = $address_datas['company'];
			$addressData["addline1"]      = $address_datas['address1'];
			$addressData["addline2"]      = $address_datas['address2'];
			$addressData["street"]        = $address_datas['street'];
			$addressData["zip"]           = $address_datas['zip'];
			$addressData['cross_streets'] = $address_datas['cross_streets'];
			$addressData["phone"]         = $address_datas['phone'];
			$addressData["extn"]         = $address_datas['extn'];
			$delivery_instructions        = $address_datas['delivery_instructions'];
		} else {

			$address_data = " SELECT * FROM `pickup_stores` WHERE id = '$storeId' ";
			$this->db->Query($address_data);
			$address_datas           = $this->db->FetchArray();
			$addressData["name"]     = $address_datas['store_name'];
			$addressData["addline1"] = $address_datas['address1'];
			$addressData["addline2"] = $address_datas['address2'];
			$addressData["zip"]      = $address_datas['zip'];
			$addressData["phone"]    = $address_datas['phone'];
			$addressData["extn"]     = '';


		}
		$getZipAbbr = "SELECT abbreviation FROM `store_zipcodes` WHERE `zipcode` = '" . $addressData["zip"] . "' ";
		$this->db->Query($getZipAbbr);
		$getZipAbbrs = $this->db->FetchArray();
		$addressData["zipAbbr"]=$getZipAbbrs['abbreviation'];
		$billing_data = "SELECT * FROM `order_billinginfo` WHERE `id` = '" . $billing_id . "' ";
		$this->db->Query($billing_data);
		$billing_datas = $this->db->FetchArray();

		$billing_type    = $billing_datas['card_type'];
		$billing_card_no = 'XXXX-XXXX-XXXX-' . substr($billing_datas['card_number'], -4);


		$orderIds = explode(':', $order_itm_id);

		$order_details = array();
		$tmp_order_det = array();

		$toralQty = 0;

		foreach ($orderIds as $ordId) 
		{
			$ordItmDat = "SELECT * FROM `orders_items` WHERE `order_item_id` = '" . $ordId . "' ";
			$this->db->Query($ordItmDat);
			$ordItmData             = $this->db->FetchArray();
			$type                   = $ordItmData['item_type'];
			$itmId                  = $ordItmData['item_id'];
			$tmp_order_det['price'] = $ordItmData['item_price'];
			$tmp_order_det['extra_id'] = !empty($ordItmData['extra_id']) ? $ordItmData['extra_id'] : '';
			$tmp_order_det['spcl_instructions'] = !empty($ordItmData['spcl_instructions']) ? $ordItmData['spcl_instructions'] : '';
			$tmp_order_det['qty']   = $ordItmData['item_qty'];
			$toralQty               = $toralQty + $ordItmData['item_qty'];
			$tmp_order_det['type']  = $type;
			if ($type == 'user_sandwich') 
			{
				$itmDeatilquery = "SELECT * FROM `user_sandwich_data` WHERE `id` = '" . $itmId . "' ";
				$this->db->Query($itmDeatilquery);
				$itm_details                    = $this->db->FetchArray();
				$tmp_order_det['sandwich_name'] = $itm_details['sandwich_name'];
				$tmp_order_det['toasted']       = $itm_details['menu_toast'];

				$san_data = json_decode($itm_details['sandwich_data']);
				$bread                           = $san_data->BREAD->item_name[0];
				$tmp_order_det['sandwich_bread'] = $bread;

				$cat_identifier = array_keys(json_decode(json_encode($san_data), true));

				/*--------- Protein ---------*/
				$prot 		= $san_data->PROTEIN->item_name;
				$prot_cat 	= $this->get_cat_by_identifiers("PROTEIN");

				$final_prot = array();
				foreach ($prot_cat as $okey => $ovalue) 
				{
					foreach ($prot as $pkey => $pvalue) 
					{
						if($ovalue == $pvalue) $final_prot[] = $pvalue;
					}
					
				}
				$prot = $final_prot;
				array_walk($prot, function(&$val, $key) use ($san_data)
				{
					if ($san_data->PROTEIN->item_qty->{$val})
						$val = $val . "(" . $san_data->PROTEIN->item_qty->{$val}[1] . ")";
				});

				$prot                              = implode(', ', $prot);
				$tmp_order_det['sandwich_protein'] = $prot;

				/*--------- Cheese ---------*/
				$cheese = $san_data->CHEESE->item_name;
				$cheese_cat = $this->get_cat_by_identifiers("CHEESE");
				
				$final_cheese = array();
				foreach ($cheese_cat as $okey => $ovalue) 
				{
					foreach ($cheese as $pkey => $pvalue) 
					{
						if($ovalue == $pvalue) $final_cheese[] = $pvalue;
					}
					
				}
				$cheese = $final_cheese;
				array_walk($cheese, function(&$val, $key) use ($san_data)
				{
					if ($san_data->CHEESE->item_qty->{$val})
						$val = $val . "(" . $san_data->CHEESE->item_qty->{$val}[1] . ")";
				});

				$cheese                           = implode(', ', $cheese);
				$tmp_order_det['sandwich_cheese'] = $cheese;

				/*--------- Toppings ---------*/
				$topping_cat = $this->get_cat_by_identifiers("TOPPINGS");
				$topping = $san_data->TOPPINGS->item_name;
				$final_topp = array();
				foreach ($topping_cat as $okey => $ovalue) 
				{
					foreach ($topping as $tkey => $tvalue) 
					{
						if($ovalue == $tvalue) $final_topp[] = $tvalue;
					}
					
				}
				$topping = $final_topp;

				array_walk($topping, function(&$val, $key) use ($san_data)
				{
					if ($san_data->TOPPINGS->item_qty->{$val})
						$val = $val . "(" . $san_data->TOPPINGS->item_qty->{$val}[1] . ")";
				});

				$topping                           = implode(', ', $topping);
				$tmp_order_det['sandwich_topping'] = $topping;

				/*--------- Condiments ---------*/

				$cond = $san_data->CONDIMENTS->item_name;
				$cond_cat = $this->get_cat_by_identifiers("CONDIMENTS");
				$final_cond = array();
				
				foreach ($cond_cat as $okey => $ovalue) 
				{
					foreach ($cond as $pkey => $pvalue) 
					{
						if($ovalue == $pvalue) $final_cond[] = $pvalue;
					}
					
				}
				$cond = $final_cond;
				array_walk($cond, function(&$val, $key) use ($san_data)
				{
					if ($san_data->CONDIMENTS->item_qty->{$val})
						$val = $val . "(" . $san_data->CONDIMENTS->item_qty->{$val}[1] . ")";
				});

				$cond                                 = implode(', ', $cond);
				$tmp_order_det['sandwich_condiments'] = $cond;

				/*-------------------------------------*/

				if ($cond)
					$topping = $topping . ", ";
				if ($topping || $cond)
					$cheese = $cheese . ", ";
				if ($cheese || $topping || $cond)
					$prot = $prot . ", ";

				$ingrs = $prot . $cheese . $topping . $cond;

				if ($ingrs)
					$bread = $bread . ", ";

				$ingrs                             = $bread . $ingrs;
				$tmp_order_det['sandwich_details'] = $ingrs;
				unset($tmp_order_det['product_name']);
			} 
			else 
			{
				$itmDeatilquery = "SELECT * FROM `standard_category_products` WHERE `id` = '" . $itmId . "' ";
				$this->db->Query($itmDeatilquery);
				$itm_details                   = $this->db->FetchArray();
				$tmp_order_det['product_name'] = $itm_details['product_name'];
				unset($tmp_order_det['sandwich_details']);
				unset($tmp_order_det['sandwich_name']);
				unset($tmp_order_det['sandwich_bread']);
				unset($tmp_order_det['sandwich_protein']);
				unset($tmp_order_det['sandwich_cheese']);
				unset($tmp_order_det['sandwich_topping']);
				unset($tmp_order_det['sandwich_condiments']);
				unset($tmp_order_det['toasted']);

			}

			$order_details['order_item_detail'][] = $tmp_order_det;

		}
		
		
		$order_details['order_date']                  = $order_det['order_date'];
		$order_details['specified_date']              = $order_det['date'];
		$order_details['date']                        = $final_date;
		$order_details['time']                        = $Ordtime;
		$order_details['billing_type']                = $billing_type;
		$order_details['billing_card_no']             = $billing_card_no;
		$order_details['address']                     = $addressData;
		$order_details['delivery_instructions']       = $delivery_instructions;
		$order_details['total']                       = $total;
		$order_details['sub_total']                   = $sub_total;
		$order_details['tip']                         = $tip;
		$order_details['discount']                    = $discount;
		$order_details['orderQty']                    = $toralQty;
		
		$order_details['specific_time']               = $specificTime;
		$order_details['authorizenet_transaction_id'] = $authorizenet_transaction_id;
		$order_details['orderCount']                  = count($order_details['order_item_detail']);
		$order_details['is_delivery']                 = $is_delivery;
		$order_details['first_name']                  = $order_det['first_name'];
		$order_details['last_name']                   = $order_det['last_name'];
		return $order_details;

	}
	/**
	 * ordinal Suffix
	 */
	public function ordinalSuffix($num)
	{
		$suffixes  = array(
				"st",
				"nd",
				"rd"
		);
		$lastDigit = $num % 10;

		if (($num < 20 && $num > 9) || $lastDigit == 0 || $lastDigit > 3)
			return "th";
		return $suffixes[$lastDigit - 1];
	}
	/**
	 * Add Admin Like
	 */
	public function addAdminLike($post)
	{
		$query = "UPDATE `user_sandwich_data` SET like_count=" . $post['count'] . " WHERE id=" . $post['id'] . "";

		$result = $this->db->Query($query);
		if ($result) {
			return true;
		}

	}
	/**
	 * Add Puchase
	 */
	public function addPuchase($post)
	{
		$query = "UPDATE `user_sandwich_data` SET purchase_count=" . $post['count'] . " WHERE id=" . $post['id'] . "";

		$result = $this->db->Query($query);
		if ($result) {
			return true;
		}
	}
	/**
	 * Add Sandwich To SOTD
	 */
	public function addSandwichToSOTD($post)
	{

		$query  = "UPDATE `user_sandwich_data` SET sotd = 1, sotd_date='" . $post['sotd_date'] . "' WHERE id=" . $post['id'] . "";
		$result = $this->db->Query($query);
		if ($result) {
			return TRUE;
		} else {

			return FALSE;
		}

	}
	/**
	 * Remove Sandwich ToSOTD
	 */
	public function removeSandwichToSOTD($post)
	{

		$query  = "UPDATE `user_sandwich_data` SET sotd = 0, sotd_date='' WHERE id=" . $post['id'] . "";
		$result = $this->db->Query($query);
		if ($result) {
			return TRUE;
		} else {

			return FALSE;
		}

	}
	/**
	 * List Sandwich Of TheDay
	 */
	public function getSandwichOfTheDay($post)
	{

		$query  = "SELECT s.*, u.first_name, u.last_name FROM user_sandwich_data as s JOIN users as u ON s.uid = u.uid WHERE sotd = 1 ORDER BY s.sotd_date";
		$result = $this->db->Query($query);
		if ($result) {
			return $this->db->FetchAllArray($result);
		} else {

			return FALSE;
		}

	}
	/**
	 * Add Sandwich To TRND
	 */
	public function addSandwichToTRND($post)
	{

		$query  = "UPDATE `user_sandwich_data` SET trnd = 1 WHERE id=" . $post['id'] . "";
		$result = $this->db->Query($query);
		if ($result) {
			return TRUE;
		} else {

			return FALSE;
		}

	}
	/**
	 * Remove Sandwich To TRND
	 */
	public function removeSandwichToTRND($post)
	{

		$query  = "UPDATE `user_sandwich_data` SET trnd = 0 WHERE id=" . $post['id'] . "";
		$result = $this->db->Query($query);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}

	}
	/**
	 * List Sandwich To TRND
	 */
	public function getSandwichOfTrending($post)
	{
		
		$query  = "SELECT s.*, u.first_name, u.last_name FROM user_sandwich_data as s JOIN users as u ON s.uid = u.uid WHERE trnd = 1 ORDER BY s.sandwich_name";

		if(isset($post['limit'])){
			$limit = $post['limit'];
			$query .= " LIMIT 0, $limit";
		}
		$result = $this->db->Query($query);
		if ($result) {
			return $this->db->FetchAllArray($result);
		} else {
			return FALSE;
		}
	}
	/**
	 * Update Sandwich To TRND
	 */
	public function setTrndLive($post)
	{

		$query = "UPDATE `user_sandwich_data` SET trnd_live = {$post['state']} WHERE id=" . $post['id'] . "";

		$result = $this->db->Query($query);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	/**
	 * List order count
	 */
	public function get_order_count($post)
	{

		$query  = "SELECT count(order_id) as order_count FROM orders WHERE user_id=" . $post['user_id'] . "";
		$result = $this->db->Query($query);
		if ($result) {
			return $this->db->FetchArrayAssoc($result);
		} else {
			return FALSE;
		}
	}
	/**
	 * List individual sandwich data
	 */
	public function get_individual_sandwich_data($post)
	{
		$id     = $post['id'];
		$uid     = $post['uid'];

		$query  = "";
		if($uid == 0)
		{
			$query  = "SELECT * FROM  `user_sandwich_data`  WHERE  `id` = '$id' AND is_public = 1 ";
			$result = $this->db->Query($query);
			$res = $this->db->FetchArrayAssoc($result);
			$res['saved'] = 0;
			return $res;
		}
		else
		{

			$query  = "SELECT * FROM  `user_sandwich_data`  WHERE  `id` = '$id' AND is_public = 1 ";
			$result = $this->db->Query($query);
			$res = $this->db->FetchArrayAssoc($result);

			if($res['uid'] == $uid)
			{
				$res['saved'] = 1;
			}
			else
			{
				$data =  $this->db->Query("SELECT id FROM  `user_sandwich_data`  WHERE  menu_added_from = ".$id." AND uid = ".$uid);
				$data = $this->db->FetchArrayAssoc($data);
				$res['saved'] = (!empty($data))?1:0;

			}
			return $res;
		}
		
	}
	
	public function retriveMostSoldSandwich(){
	
		$query  = "SELECT * FROM  `user_sandwich_data`  WHERE  `purchase_count` = (SELECT MAX(`purchase_count`) FROM `user_sandwich_data`)";
		$result = $this->db->Query($query);
		return $this->db->FetchArrayAssoc($result);
	
	}
	
	public function getTotalSanwichMenuCount($post){
	$uid = 	$post['uid'];
	$result = $this->db->Query("SELECT COUNT(*) AS `count` FROM `user_sandwich_data` WHERE `uid` = '$uid' AND `menu_is_active` = 1 ");
	return $this->db->FetchArrayAssoc($result);
	}
	
	public function isFbOnlyUser($post){
		$uid    = $post['uid'];
		$query  = " SELECT COUNT(*) AS count  FROM `users` WHERE `fb_id` != '' AND `uid` = '$uid' ";
		$result = $this->db->Query($query);
		return $this->db->FetchArrayAssoc($result);
	}
	
	public function deliveryTime($sub_total)
	{
		$subTotal = $sub_total['subTotal'];
		if($subTotal<500){
			$query  = " SELECT * FROM `delivery_time` WHERE   (min <= $subTotal && max >= $subTotal)";
		}
		else{
			$query  = " SELECT * FROM `delivery_time` WHERE   min = 500 ";
		}
		$result = $this->db->Query($query);
		return $this->db->FetchArrayAssoc($result);
	}


		/**
	 * Add checkout placeorder Only Admin
	 */
public function insert_Admin_checkout_placeorder($post)
	{
	
		$post = $this->process_input($post);
		$time = 0;

		if ($post['now_or_specific'] == "now") {
			$date = date("Y-m-d H:i:s");
			$date = "'$date'";
			$time = 0;
		} else {
			$date = date("Y-m-d H:i:s", strtotime($post['date'] . " " . $post['time'] . ":00"));
			$date = "'$date'";
			$time = 1;
		}


		if (isset($post['coupon_code'])) {

			$query = "UPDATE  discounts SET uses_amount = uses_amount - 1 WHERE id = '" . $post['coupon_code'] . "' AND uses_type = 0 ";

			$this->db->Query($query);

		}

		$is_admin = "";
		$by_admin = "";

		if (!isset($post['by_admin']) && !isset($post['coupon_code'])) {
			$post['coupon_code'] = "";
		}

		if (isset($post['by_admin'])) {
			$is_admin = ", 1";
			$by_admin = ", by_admin";
		}
		$off_col   = "";
		$off_value = "";
		if (isset($post['off_type']) && isset($post['off_amount'])) {
			$off_col   = ", off_type, off_amount";
			$off_value = ", '{$post['off_type']}', {$post['off_amount']}";
		} else if (isset($post['off_amount']) && $post['off_amount']!="") {  
			$off_col   = ",  off_amount";
			$off_value = ", {$post['off_amount']}";
		}

		if (isset($post['manual_discount']))
			$manual_discount = $post['manual_discount'];
		else
			$manual_discount = 0;

		$post['sub_total'] = str_replace(",", "", $post['sub_total']);
		$post['sub_total'] = floatval($post['sub_total']);

		
		if(isset($post['credit_card_id']) && $post['credit_card_id'] != "no" ){
		
			//starting of authorize


			$authObject = new authorizenetCIM();
			$query      = "SELECT first_name,last_name,username,authorizenet_profile_id FROM `users` WHERE uid = '" . $post['user_id'] . "' ";
			$result     = $this->db->Query($query);
			$result     = $this->db->FetchArray($result);
			$email      = $result['username'];
			$first_name      = $result['first_name'];
			$last_name      = $result['last_name'];
			$profileID  = $result['authorizenet_profile_id'];

			if ($profileID == 0) {
				$profileID = $authObject->createProfile($email, $post['user_id']);
					$query     = "UPDATE users SET authorizenet_profile_id = '" . $profileID . "'  WHERE uid = '" . $post['user_id'] . "' ";
					$this->db->Query($query);
			}   //profile ID



			$address_insid = "";
			if ($post['delivery']) {
				$query             = "SELECT * FROM `order_address` WHERE address_id = '" . $post['address_id'] . "' ";
				$result            = $this->db->Query($query);
				$shippingresult    = $this->db->FetchArray($result);
				$shippingProfileId = $shippingresult['authorizenet_shipping_profile_id'];
			} else {
				/*$query_store          = "SELECT * FROM `pickup_stores` WHERE id = '" . $post['address_id'] . "' ";
				$result_store         = $this->db->Query($query_store);
				$shippingresult_store = $this->db->FetchArray($result_store);
				$query                = "SELECT * FROM `order_address` WHERE name = '" . $shippingresult_store['store_name'] . "' AND address1 = '" . $shippingresult_store['address1'] . "' AND address2 = '" . $shippingresult_store['address2'] . "' AND zip = '" . $shippingresult_store['zip'] . "' AND uid='" . $post['user_id'] . "' ";
				$result               = $this->db->Query($query);
				$shippingresult       = $this->db->FetchArray($result);*/
				$query_store          = "SELECT * FROM `order_billinginfo` WHERE id = '" . $post['credit_card_id'] . "' ";
				$result_store         = $this->db->Query($query_store);
				$shippingresult_store = $this->db->FetchArray($result_store);
				$query                = "SELECT * FROM `order_address` WHERE name = '" . $first_name . "' AND address1 = '" . $shippingresult_store['address1'] . "' AND street = '" . $shippingresult_store['street'] . "' AND zip = '" . $shippingresult_store['zip'] . "' AND uid='" . $post['user_id'] . "' ";
				$result               = $this->db->Query($query);
				$shippingresult       = $this->db->FetchArray($result);
		
				if ($shippingresult) {
					$shippingProfileId = $shippingresult['authorizenet_shipping_profile_id'];
				} else {
					//$query             = "INSERT INTO order_address (name,company,phone,address1,address2,zip,authorizenet_shipping_profile_id,street,cross_streets,extn,delivery_instructions,uid) VALUES('" . $shippingresult_store['store_name'] . "','" . $shippingresult_store['store_name'] . "','" . $shippingresult_store['phone'] . "','" . $shippingresult_store['address1'] . "','" . $shippingresult_store['address2'] . "','" . $shippingresult_store['zip'] . "',0,'','','','','" . $post['user_id'] . "')";
					
					$query = "SELECT * FROM pickup_stores WHERE id = {$post['store_id']}";
					$result               = $this->db->Query($query);
					$shippingresult_store       = $this->db->FetchArray($result);
					
					$query                = "SELECT * FROM `order_address` WHERE name = '" . $first_name . "' AND company = '".$shippingresult_store['store_name']."' AND address1 = '" . $shippingresult_store['address1'] . "' AND street = '" . $shippingresult_store['street'] . "' AND zip = '" . $shippingresult_store['zip'] . "' AND uid='" . $post['user_id'] . "' ";
					$result               = $this->db->Query($query);
					$shippingresult       = $this->db->FetchArray($result);
					
					if(is_array($shippingresult)){
						
						$shippingProfileId = $shippingresult['authorizenet_shipping_profile_id'];
						
					}else{
					
						$query             = "INSERT INTO order_address (name,company,phone,address1,address2,zip,authorizenet_shipping_profile_id,street,cross_streets,extn,delivery_instructions,uid) VALUES('" . $first_name . "','" . $shippingresult_store['store_name'] . "','','" . $shippingresult_store['address1'] . "','','" . $shippingresult_store['zip'] . "',0,'" . $shippingresult_store['street'] . "','" . $shippingresult_store['cross_streets'] . "','','','" . $post['user_id'] . "')";

						$shippingProfileId = 0;

						$result         = $this->db->Query($query);
						$address_insid  = $this->db->insertId();
						$query          = "SELECT * FROM `order_address` WHERE address_id = '" . $address_insid . "' ";
						$result         = $this->db->Query($query);
						$shippingresult = $this->db->FetchArray($result);
					}	

				}
				
			}  //pickup 

					
			$shippingresult["address2"]=$shippingresult['street'];
			if ($shippingProfileId == 0) 
			{
				$shippingProfileId = $authObject->createShippingProfile($profileID, $shippingresult);
						if ($post['delivery'] || $address_insid == "") {
							$query = "UPDATE order_address SET authorizenet_shipping_profile_id = '" . $shippingProfileId . "' WHERE address_id = '" . $post['address_id'] . "' ";
						} else {
							$query = "UPDATE order_address SET authorizenet_shipping_profile_id = '" . $shippingProfileId . "' WHERE address_id = '" . $address_insid . "' ";
						}
						$this->db->Query($query);
			} //shipping profile id 141
			
			//else {		
			
			if($shippingProfileId){
			
				$shippingProfile = $authObject->updateShippingProfile($shippingProfileId,$profileID, $shippingresult);
					
			}
			//$query            = "SELECT authorizenet_payment_profile_id,expire_year,expire_month,card_number,card_zip FROM `order_billinginfo` WHERE id = '" . $post['credit_card_id'] . "' ";
			$query            = "SELECT authorizenet_payment_profile_id,expire_year,expire_month,card_number,card_zip,address1,street,cross_streets FROM `order_billinginfo` WHERE id = '" . $post['credit_card_id'] . "' ";

			$result           = $this->db->Query($query);
			$result           = $this->db->FetchArray($result);
			$paymentProfileId = $result['authorizenet_payment_profile_id'];
			$billing_address=$shippingresult;
			$billing_address["zip"]=$result['card_zip'];
			$billing_address["address1"]=$result['address1'];
			$billing_address["street"]=$result['street'];
			$billing_address["address2"]=$result['street'];
			//$billing_address["cross_streets"]=$result['cross_streets'];
			if ($post['delivery']){
			$billing_address["last_name"]="";
			}
			else
			{
			$billing_address["name"]=$first_name;
			$billing_address["last_name"]=$last_name;
			}



			if ($paymentProfileId == 0) {
				$paymentProfileId = $authObject->createPaymentProfile($profileID, $result['card_number'], $result['expire_year'], $result['expire_month'], $billing_address);
				
					if( $paymentProfileId == 0 ){
						 throw new customErrorApi( ORDER_ERROR_CREATING_PAYMENT_PROFILE, "Something went wrong creating payment profile Id", array( $profileID, $result['card_number'], $result['expire_year'], $result['expire_month'], $billing_address ) );
						}
					else{
						$query            = "UPDATE order_billinginfo SET authorizenet_payment_profile_id = '" . $paymentProfileId . "' WHERE id = '" . $post['credit_card_id'] . "' ";
						$this->db->Query($query);
					}
			}
			
			//else{
			if($paymentProfileId){
				$paymentProfile = $authObject->updatePaymentProfile($paymentProfileId, $profileID, $result['card_number'], $result['expire_year'], $result['expire_month'], $billing_address);
					
			}
			$post['customerProfileId']        = $profileID;
			$post['customerPaymentProfileId'] = $paymentProfileId;

			$post['customerShippingAddressId'] = $shippingProfileId;
			
			
		}///Ending credit card No filter.
		
		$order_items                       = explode(":", $post['order_item_id']);

		$orderItems = array();
		$i          = 0;
		
		//looping user sandwich
		foreach ($order_items as $order_item_id) {
			$query  = "SELECT o.item_id,o.item_qty,o.item_price,SUBSTRING(t.sandwich_name, 1, 30) as sandwich_name,SUBSTRING(t.description, 1, 30) as description  FROM `orders_items` as o LEFT JOIN `user_sandwich_data` as t on o.item_id = t.id WHERE o.item_type='user_sandwich' AND order_item_id = '" . $order_item_id . "' ";
			$result = $this->db->Query($query);
			$result = $this->db->FetchArrayAssoc($result);

			if (!empty($result)) {
				$orderItems[$i] = $result;

				$i++;
			}
		}


		//looping product
		foreach ($order_items as $order_item_id) {
			$query  = "SELECT o.item_id,o.item_qty,o.item_price,SUBSTRING(t.product_name, 1, 30) as product_name,SUBSTRING(t.description, 1, 30) as description FROM `orders_items` as o LEFT JOIN `standard_category_products` as t on o.item_id = t.id WHERE o.item_type='product' AND order_item_id = '" . $order_item_id . "' ";
			$result = $this->db->Query($query);
			$result = $this->db->FetchArrayAssoc($result);
			if (!empty($result)) {
				$orderItems[$i] = $result;

				$i++;
			}
		}

		//if No credit card
		if(isset($post['credit_card_id']) && $post['credit_card_id'] != "no" ){
			$tarnsaction = $authObject->createTransaction($post, $orderItems);

			
			if ((isset($tarnsaction['error']) && $tarnsaction['error'] == 1) && $tarnsaction['data'] != "The payment gateway account is in Test Mode. The request cannot be processed.") {

				return $tarnsaction;
			} else {



				if ($tarnsaction == "The payment gateway account is in Test Mode. The request cannot be processed.") {
					$tarnsaction_id = 0;
				} else
					$tarnsaction_id = $tarnsaction;

				$date_time = date("Y-m-d H:i:s");
				//ending of of authorize

				$query = "INSERT INTO orders (user_id,store_id,address_id,billinginfo_id,order_item_id,sub_total,tip,coupon_code,total,tax,delivery,date, manual_discount,time, authorizenet_transaction_id, order_date $by_admin $off_col) VALUES('" . $post['user_id'] . "', '" . $post['store_id'] . "', '" . $post['address_id'] . "' , '" . $post['credit_card_id'] . "' , '" . $post['order_item_id'] . "' , '" . $post['sub_total'] . "' , '" . $post['tip'] . "' , '" . $post['coupon_code'] . "', '" . $post['total'] . "', '" . $post['tax'] . "', '" . $post['delivery'] . "', $date , $manual_discount,$time , $tarnsaction_id , '$date_time' $is_admin $off_value)";


				$result = $this->db->Query($query);
				$insid  = $this->db->insertId();

				$order_number = 11100000 + $insid;

				$update = "UPDATE orders SET order_number = '$order_number' WHERE order_id = $insid";
				$result = $this->db->Query($update);




				if ($insid AND isset($post['mealname']) AND strlen($post['mealname']) > 0) {
					$save_meal = "INSERT INTO saved_meals (order_id,meal_name) VALUES ('" . $insid . "','" . $post['mealname'] . "') ";
					$this->db->Query($save_meal);
				}

				$order_items = explode(":", $post['order_item_id']);

				$query  = "UPDATE orders_items SET ordered = 1, orderid=LAST_INSERT_ID() WHERE order_item_id IN (" . implode(",", $order_items) . ")";
				$result = $this->db->Query($query);

				$query  = "UPDATE `user_sandwich_data` SET purchase_count=purchase_count+1 WHERE id IN (SELECT item_id FROM orders_items WHERE item_type='user_sandwich' AND order_item_id IN (" . implode(",", $order_items) . "))";
				$result = $this->db->Query($query);

				$tarnsaction  = $order_number;
				
				$order_details = $this->get_checkout_order_success_data(array('order_id'=> $order_number));
				
				$this->sendOrderConfirmationMail($order_details); 

				return $tarnsaction;

			}
		} else {
			
			//Ending No 
			
			$date_time = date("Y-m-d H:i:s");
			$tarnsaction_id = 0;
				//ending of of authorize

				$query = "INSERT INTO orders (user_id,store_id,address_id,billinginfo_id,order_item_id,sub_total,tip,coupon_code,total,tax,delivery,date, manual_discount,time, authorizenet_transaction_id, order_date $by_admin $off_col) VALUES('" . $post['user_id'] . "', '" . $post['store_id'] . "', '" . $post['address_id'] . "' , '" . $post['credit_card_id'] . "' , '" . $post['order_item_id'] . "' , '" . $post['sub_total'] . "' , '" . $post['tip'] . "' , '" . $post['coupon_code'] . "', '" . $post['total'] . "', '" . $post['tax'] . "', '" . $post['delivery'] . "', $date , $manual_discount,$time , $tarnsaction_id , '$date_time' $is_admin $off_value)";


				$result = $this->db->Query($query);
				$insid  = $this->db->insertId();

				$order_number = 11100000 + $insid;

				$update = "UPDATE orders SET order_number = '$order_number' WHERE order_id = $insid";
				$result = $this->db->Query($update);




				if ($insid AND isset($post['mealname']) AND strlen($post['mealname']) > 0) {
					$save_meal = "INSERT INTO saved_meals (order_id,meal_name) VALUES ('" . $insid . "','" . $post['mealname'] . "') ";
					$this->db->Query($save_meal);
				}

				$order_items = explode(":", $post['order_item_id']);

				$query  = "UPDATE orders_items SET ordered = 1, orderid=LAST_INSERT_ID() WHERE order_item_id IN (" . implode(",", $order_items) . ")";
				$result = $this->db->Query($query);

				$query  = "UPDATE `user_sandwich_data` SET purchase_count=purchase_count+1 WHERE id IN (SELECT item_id FROM orders_items WHERE item_type='user_sandwich' AND order_item_id IN (" . implode(",", $order_items) . "))";
				$result = $this->db->Query($query);

				$tarnsaction['error'] = 2;
				$tarnsaction['data']  = $order_number;
				
				$order_details = $this->get_checkout_order_success_data(array('order_id'=> $order_number));
				
				$this->sendOrderConfirmationMail($order_details); 

				return $tarnsaction;

			
		}

	}
	/**
	 * List sandwich category by identifies
	 */
	public function get_cat_by_identifiers($cat_idnt)
	{
		$query = "SELECT s.item_name FROM sandwich_category_items s JOIN sandwich_categories c ON s.category_id = c.id WHERE c.category_identifier = '".$cat_idnt."' ORDER BY s.sheet_priority ASC";
		$qry = $this->db->Query($query);
		$res = $this->db->FetchAllArray($qry);
	  	return array_column($res, 'item_name'); 
	}

	public function Getcurrentprice($post)
	{
		$post = $this->process_input($post);
		$itms = explode(', ', $post['desc']);
		$itms_id = explode(', ', $post['desc_id']);

		$extra =array();
		foreach($itms as $k=>$val){
			if($k != 0){
				$extra[]= $val;
			}
		}

		$extra_id =array();
		foreach($itms_id as $k=>$val){
			if($k != 0){
				$extra_id[]= $val;
			}
		}

		$extra = array_combine($extra_id, $extra);

		$bread = $post['bread'];
		$total =0;
		$sandwich_query = "SELECT sandwich_data FROM user_sandwich_data WHERE id = ".$post['id'];
			$sandwich_runquery = $this->db->Query($sandwich_query);
			$sandwich_result = $this->db->FetchArray($sandwich_runquery);

			$sandwich_itemdata = json_decode($sandwich_result['sandwich_data'], true);
			
			
		foreach($extra as $k=>$val)
		{
			preg_match('#\((.*?)\)#', $val,$match);

			$item_qty = $match[1];
			
			$unitquery  = "SELECT price_mult FROM sandwich_item_options WHERE option_unit = '$item_qty'";
			$unitresult = $this->db->Query($unitquery);
			$unit = $this->db->FetchArray($unitresult);
						
			$item_name = trim(preg_replace('/\s*\([^)]*\)/', '', $val));
			
			if($item_name){
				$query = "SELECT ci.item_price, c.category_identifier FROM sandwich_category_items as ci JOIN sandwich_categories as c ON c.id = ci.category_id WHERE ci.id = '".$k."'";
				$result = $this->db->Query($query);
				$final = $this->db->FetchArray($result);
			}
			
			$item_total_price = sprintf("%.2f",$unit['price_mult'] * $final['item_price']);
			$total += ($unit['price_mult'] * $final['item_price']);

			foreach ($sandwich_itemdata[$final['category_identifier']]['item_name'] as $key => $value) {
				if($value == $item_name){
					$sandwich_itemdata[$final['category_identifier']]['item_price'][$key] = $item_total_price;
					$sandwich_itemdata[$final['category_identifier']]['actual_price'][$value] = $final['item_price'];
				}
			}
		}
		$brdval = 0;
		if(!empty($bread)){
			$brdqry = "SELECT item_price FROM sandwich_category_items WHERE item_name like '$bread' ";
			$brdresult = $this->db->Query($brdqry);
			$brea = $this->db->FetchArray($brdresult);
			if(!empty($brea['item_price'])){
				$brdval = $brea['item_price'];
			}
			foreach ($sandwich_itemdata['BREAD']['item_name'] as $key => $value) {
				$sandwich_itemdata['BREAD']['item_price'][$key] = $brdval;				
			}
			
		}
				
		$sandwichtot = sprintf("%.2f",$total + $brdval + 6);
		if(!empty($sandwich_itemdata)){
			$sandwich_data = json_encode($sandwich_itemdata,true);
			$query  = "UPDATE `user_sandwich_data` SET sandwich_data = '$sandwich_data',sandwich_price ='$sandwichtot' WHERE id = '".$post['id']."' ";
				$result = $this->db->Query($query);
		}
		return $sandwichtot;
	}
	public function get_sandwich_current_price_menu($post)
	{
		$post = $this->process_input($post);
		$itms = explode(', ', $post['desc']);
		$itms_id = explode(', ', $post['desc_id']);
		
		$extra =array();
		foreach($itms as $k=>$val){
			if($k != 0){
				$extra[]= $val;
			}
		}

		$extra_id =array();
		foreach($itms_id as $k=>$val){
			if($k != 0){
				$extra_id[]= $val;
			}
		}
		
		$bread = $post['bread'];
		$total =0;
		
		$extra = array_combine($extra_id, $extra);
	
		foreach($extra as $k=>$val)
		{
			preg_match('#\((.*?)\)#', $val,$match);
			$item_qty = $match[1];
			
			$unitquery  = "SELECT price_mult FROM sandwich_item_options WHERE option_unit = '$item_qty'";
			$unitresult = $this->db->Query($unitquery);
			$unit = $this->db->FetchArray($unitresult);
				
			$item_name = trim(preg_replace('/\s*\([^)]*\)/', '', $val));
			
			if($item_name){
				
				$query = "SELECT ci.item_price, c.category_identifier FROM sandwich_category_items as ci JOIN sandwich_categories as c ON c.id = ci.category_id WHERE ci.id = '".$k."'";
				$result = $this->db->Query($query);
				$final = $this->db->FetchArray($result);
			}
			
			$total += ($unit['price_mult'] * $final['item_price']);
		}
		$brdval = 0;
		if(!empty($bread)){
			$brdqry = "SELECT item_price FROM sandwich_category_items WHERE item_name like '$bread' ";
			$brdresult = $this->db->Query($brdqry);
			$brea = $this->db->FetchArray($brdresult);
			if(!empty($brea['item_price'])){
				$brdval = $brea['item_price'];
			}
		}
		$sandwichtot = $total + $brdval + 6;
	
		return sprintf("%.2f", $sandwichtot);
	}


	public function get_searchItem_count($post)
	{
		$post = $this->process_input($post);
		//$query = 'SELECT id FROM user_sandwich_data WHERE  is_public = 1  AND sandwich_name LIKE "%'.$post["name"].'%" GROUP BY sandwich_name,sandwich_data ORDER BY date_of_creation DESC';
		$query = 'SELECT id FROM user_sandwich_data WHERE  is_public = 1  AND sandwich_name LIKE "%'.$post["name"].'%" GROUP BY sandwich_name,sandwich_data';
        $countRes = $this->db->Query($query);
        $res = $this->db->Rows($countRes);
        return $res;
	}
	public function sandwich_filter($data)
    {
    	
        $data = $this->process_input($data);
        $orderQuery ='';
        if ( $data['sortBy'] == 1 ) {
            $orderQuery = "ORDER BY date_of_creation DESC";
        } else if ( $data['sortBy'] == 2 ) {
            $orderQuery = "ORDER BY like_count DESC";
        } else if ( $data['sortBy'] == 3 ) {
            $orderQuery = "ORDER BY menu_add_count DESC";
        } else {
            $orderQuery = " ORDER BY sandwich_name ASC ";
        }
        $where = "";
        $i = 1;
        foreach ($data['s_items'] as $key) 
        {
            $where .= "sandwich_items LIKE '%".$key."%' ";
            if($i < count($data['s_items']))
            {
                $where .= " AND ";
            }
            $i++;
        }
        
        if(!empty($data['name']))
        {
            $qry = "SELECT * FROM user_sandwich_data WHERE sandwich_name  LIKE '%".$data['name']."%' AND ".$where." AND is_public = '1'";
        }
        else
        {
            $qry = "SELECT * FROM user_sandwich_data WHERE ".$where." AND is_public = '1'";
        }
        
        $noDuplicates = " GROUP BY sandwich_name  $orderQuery LIMIT 100"; //for avoiding duplicate sandwiches 
        $qry .= $noDuplicates;
        file_put_contents("query.txt", print_r($qry, true), FILE_APPEND);
        $res = $this->db->Query($qry);
        $result = $this->db->FetchAllArray($res);
        return $result;
    }

    public function sandwich_filter_count($data)
    {
        $data = $this->process_input($data);
        $orderQuery ='';
        if ( $data['sortBy'] == 1 ) {
            $orderQuery = "ORDER BY date_of_creation DESC";
        } else if ( $data['sortBy'] == 2 ) {
            $orderQuery = "ORDER BY like_count DESC";
        } else if ( $data['sortBy'] == 3 ) {
            $orderQuery = "ORDER BY menu_add_count DESC";
        } else {
            $orderQuery = " ORDER BY sandwich_name ASC ";
        }
        $where = "";
        $i = 1;
        foreach ($data['s_items'] as $key) 
        {
            $where .= "sandwich_items LIKE '%".$key."%' ";
            if($i < count($data['s_items']))
            {
                $where .= " AND ";
            }
            $i++;
        }
        
        if(!empty($data['name']))
        {
            $countQry = "SELECT count(*) FROM user_sandwich_data WHERE sandwich_name  LIKE '%".$data['name']."%' AND ".$where." AND is_public = '1'";
        }
        else
        {
            $countQry = "SELECT count(*) FROM user_sandwich_data WHERE ".$where." AND is_public = '1'";
        }
        
        $countQry .= " GROUP BY sandwich_name  $orderQuery";

        $countRes = $this->db->Query($countQry);
		$countResult =  $this->db->Rows($countRes);
        return $countResult;
    }
    public function more_sandwich_filter($data)
    {
    	$data = $this->process_input($data);
        $orderQuery ='';
        if ( $data['sort_id'] == 1 ) {
            $orderQuery = "ORDER BY date_of_creation DESC";
        } else if ( $data['sort_id'] == 2 ) {
            $orderQuery = "ORDER BY like_count DESC";
        } else if ( $data['sort_id'] == 3 ) {
            $orderQuery = "ORDER BY menu_add_count DESC";
        } else {
            $orderQuery = " ORDER BY sandwich_name ASC ";
        }
        $where = "";
        $start = $data['start'];
        $limit = $data['limit'];
        $i = 1;
        foreach ($data['s_items'] as $key) 
        {
            $where .= "sandwich_items LIKE '%".$key."%' ";
            if($i < count($data['s_items']))
            {
                $where .= " AND ";
            }
            $i++;
        }
        if(!empty($data['name']))
        {
            $qry = "SELECT * FROM user_sandwich_data WHERE sandwich_name  LIKE '%".$data['name']."%' AND  ".$where." AND is_public = '1'";
        }
        else
        {
            $qry = "SELECT * FROM user_sandwich_data WHERE ".$where." AND is_public = '1'";
        }
        
        $noDuplicates = " GROUP BY sandwich_name  $orderQuery LIMIT ".$start.",".$limit; //for avoiding duplicate sandwiches 
        $qry .= $noDuplicates;
        $res = $this->db->Query($qry);
        return $this->db->FetchAllArray($res);
    }
     public function gallery_load_data($post)
    {
        //echo "<pre>";print_r($post);exit;
        $post = $this->process_input($post);
        $start = $limit = 0;
        if (isset($post['start']) && ($post['start'] >= 0)) {
            $start = $post['start'];
        }

        if (isset($post['limit']) && ($post['limit'] >= 0)) {
            $limit = $post['limit'];
        }

        if (isset($post['name']) && ($post['name'] != '')) 
        {
            $name = $this->clean_for_sql($post['name']);
        }
        $query = "SELECT * FROM `user_sandwich_data` WHERE is_public = '1' ";
        
        if (isset($post['sort_id']) && ($post['sort_id'] == 0)) 
        {
            if (isset($name) && ($name != '')) 
            {
                $query .= "AND sandwich_name  LIKE '%".$name."%' GROUP BY sandwich_name,sandwich_data ORDER BY date_of_creation DESC"; 
            }
            else
            {
                $query .= " GROUP BY sandwich_name,sandwich_data ORDER BY flag DESC, date_of_creation DESC";
            }
        }
        
        else if (isset($post['sort_id']) && ($post['sort_id'] == 1)) 
        {
            if (isset($name) && ($name != '')) 
            {
                $query .= "AND sandwich_name  LIKE '%".$name."%' GROUP BY sandwich_name,sandwich_data ORDER BY date_of_creation DESC"; 
            }
            else
            {
                $query .= "GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
            }
        } 
        else if (isset($post['sort_id']) && ($post['sort_id'] == 2)) 
        {
            if (isset($name) && ($name != '')) 
            {
                $query .= "AND sandwich_name  LIKE '%".$name."%' GROUP BY sandwich_name,sandwich_data ORDER BY like_count DESC"; 
            }
            else
            {
                $query = "SELECT user.*,slike.id as likeid,user.like_count as count FROM `user_sandwich_data` as user LEFT JOIN sandwich_like as slike  on (user.id=slike.sandwich_id) WHERE  user.is_public = '1'   GROUP BY user.sandwich_name,user.sandwich_data ORDER BY count DESC   ";
            }
        } 
        else if (isset($post['sort_id']) && ($post['sort_id'] == 3)) 
        {
            if (isset($name) && ($name != '')) 
            {
                $query .= "AND sandwich_name  LIKE '%".$name."%' GROUP BY sandwich_name,sandwich_data ORDER BY menu_add_count DESC"; 
            }
            else
            {
                $query .= "GROUP BY sandwich_name,sandwich_data ORDER BY menu_add_count DESC   ";
            }
        } 
        else 
        {
            $query .= "GROUP BY sandwich_name,sandwich_data  ORDER BY date_of_creation DESC     ";
        }
        $query .= " LIMIT $start, $limit";
       
        $result = $this->db->Query($query);

        return $this->db->FetchAllArray($result);
    }
}
