<?php
class discountsModel extends Model
{

	public $db; //database connection object
	/**
	* invoke database connection object
	*/
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * List customer data
	 */
	public function get_discount_list()
	{

		$query  = "SELECT d.*,(SELECT COUNT(O.coupon_code) FROM orders O WHERE O.coupon_code = d.id) as count FROM discounts d";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * Insert discount
	 */
	public function add_discount($data)
	{
		$data               = $this->process_input($data);
		$data['expiration'] = date('Y-m-d H:i:s', strtotime($data['expiration']));

		$query = "INSERT INTO `discounts` (name, code, type, discount_amount, applicable_to, uses_type, uses_amount, minimum_order, minimum_order_amount, maximum_order, maximum_order_amount, expiration, one_per_user) values
				('" . $data['name'] . "','" . $data['code'] . "','" . $data['type'] . "','" . $data['discount_amount'] . "','" . $data['applicable_to'] . "','" . $data['uses_type'] . "','" . $data['uses_amount'] . "','" . $data['minimum_order'] . "','" . $data['minimum_order_amount'] . "', '" . $data['maximum_order'] . "','" . $data['maximum_order_amount'] . "', '" . $data['expiration'] . "', '" . $data['one_per_user'] . "' ) ";

		return $this->db->Query($query);
	}
	/**
	 * Get discount row
	 */
	public function get_discount_row($post)
	{

		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `discounts` WHERE id = '" . $id . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Update discount
	 */
	public function update_discount($data)
	{
		$data               = $this->process_input($data);
		$data['expiration'] = date('Y-m-d H:i:s', strtotime($data['expiration']));

		$query = "UPDATE  `discounts` SET " . " name = '" . $data['name'] . "', " . "code = '" . $data['code'] . "', " . "type= '" . $data['type'] . "', " . "discount_amount= '" . $data['discount_amount'] . "', " . "applicable_to= '" . $data['applicable_to'] . "', " . "uses_type= '" . $data['uses_type'] . "', " . "uses_amount= '" . $data['uses_amount'] . "', " . "minimum_order= '" . $data['minimum_order'] . "', " . " minimum_order_amount= '" . $data['minimum_order_amount'] . "', " . "maximum_order= '" . $data['maximum_order'] . "', " . " maximum_order_amount= '" . $data['maximum_order_amount'] . "', " . " expiration =  '" . $data['expiration'] . "' , " . " one_per_user =  '" . $data['one_per_user'] . "' ";
		$query = $query . " WHERE `id` = '" . $data['id'] . "';";
		return $this->db->Query($query);
	}
	/**
	 * Delete Items
	 */
	public function delete_items($data)
	{

		$query = "DELETE FROM `discounts` WHERE `id` = '" . $data['id'] . "';";

		return $this->db->Query($query);
	}
	/**
	 * Update is live
	 */
	public function update_is_live($data)
	{
		$id    = explode("---", $data['id']);
		$query = '';
		foreach ($id as $key => $value) {
			$query = "UPDATE discounts SET is_live = '1' WHERE id = '" . $value . "' ;";
			$this->db->Query($query);
		}
		$query = "UPDATE discounts SET is_live = '0' WHERE id NOT IN ('" . join("','", $id) . "')";
		$this->db->Query($query);
		return;
	}
	/**
	 * Process input
	 */
	private function process_input($data)
	{
		array_walk($data, function(&$val, $key)
		{
			$val = urldecode($val);
			$val = $this->clean_for_sql($val);
		});
		return $data;
	}
}