<?php
class userModel extends Model
{

	public $db; //database connection object
	/**
	 * invoke database connection object
	 */
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * Add user
	 */
	public function add($post)
	{

		$fname         = $this->clean_for_sql($post['first_name']);
		$lname         = $this->clean_for_sql($post['last_name']);
		$username      = $this->clean_for_sql($post['username']);
		$fb_id         = $this->clean_for_sql($post['fb_id']);
		$fb_auth_token = $this->clean_for_sql($post['fb_auth_token']);
		$password      = $this->clean_for_sql($post['password']);
		$query         = "INSERT INTO `users` (username, fb_id, password, fb_auth_token, first_name, last_name) values ('" . $username . "','" . $fb_id . "','" . $password . "','" . $fb_auth_token . "','" . $fname . "','" . $lname . "') ";
		$this->db->Query($query);
	}
	/**
	 * Update user
	 */
	public function update($post)
	{
		$user_id       = $this->clean_for_sql($post['user_id']);
		$fname         = $this->clean_for_sql($post['first_name']);
		$lname         = $this->clean_for_sql($post['last_name']);
		$username      = $this->clean_for_sql($post['username']);
		$fb_id         = $this->clean_for_sql($post['fb_id']);
		$fb_auth_token = $this->clean_for_sql($post['fb_auth_token']);
		$password      = $this->clean_for_sql($post['password']);
		$query         = "UPDATE `users` SET username='" . $username . "', fb_id='" . $fb_id . "', fb_auth_token='" . $fb_auth_token . "', password='" . $password . "', first_name='" . $fname . "', last_name='" . $lname . "' WHERE uid=$user_id";
		$result        = $this->db->Query($query);
		$query         = "SELECT * FROM `users` WHERE uid=$user_id";
		$result        = $this->db->Query($query);
		$result        = $this->db->FetchArray($result);
		return $result;

	}
	/**
	 * List user
	 */
	public function get($user_id)
	{

		if ($user_id) {

			$query = "SELECT * FROM `users` WHERE uid = '$user_id' ";
		} else {

			$query = "SELECT * FROM `users`";
		}
		file_put_contents("query.txt", print_r($query, true));
		$result = $this->db->Query($query);
		$result = $this->db->FetchAllArray($result);
		return $result;
	}
}