<?php
class menuModel extends Model
{

	public $db; //database connection object
	/**
	* invoke database connection object
	*/
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * List Sandwich data
	 */
	public function get_sandwich_data($post)
	{
		$post   = $this->process_input($post);
		$query  = "SELECT * FROM `user_sandwich_data` WHERE uid = '" . $post['uid'] . "' and menu_isactive = 1";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Update option
	 */
	public function edit_options($post)
	{
		$post = $this->process_input($post);
		if (isset($post['id']) && $post['id']) {
			$query = "SELECT * FROM `sandwich_item_options` WHERE id = '" . $post['id'] . "' ORDER BY priority";
		} else {
			$query = "SELECT * FROM `sandwich_item_options`  ORDER BY priority ";
		}
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Insert Option
	 */
	public function addoptions($post)
	{
		$post        = $this->process_input($post);
		$id          = $post['hid'];
		$priority    = $this->db->Query("SELECT MAX(priority) AS PRTY FROM `sandwich_item_options`");
		$priority    = $this->db->FetchAllArray($priority);
		$PTY         = $priority[0]['PRTY'];
		$PTY         = $PTY + 1;
		$displayname = $post['displayname'];
		$optionname  = $post['optionname'];
		$pricemult   = $post['pricemult'];
		$unit        = $post['unit'];

		if ($id) {
			$query = "UPDATE `sandwich_item_options` SET option_name = '$optionname' , option_unit = '$unit' , price_mult = '$pricemult' , option_display_name = '$displayname' WHERE id =  '$id' ";
		} else {
			$query = "INSERT INTO `sandwich_item_options` (option_name,option_unit,price_mult,option_display_name,priority,active) VALUES ('$optionname','$unit','$pricemult','$displayname','$PTY',1) ";
		}

		$this->db->Query($query);

	}
	/**
	 * Delete Option
	 */
	public function deleteOption($post)
	{
		$post = $this->process_input($post);
		$id   = $post['id'];
		$this->db->Query("DELETE FROM `sandwich_item_options` WHERE id = '$id' ");
	}
	/**
	 * Update option
	 */
	public function updateoptionstatus($post)
	{

		$ln   = count($post['hid']);
		$id   = $post['hid'];
		$prty = $post['priority'];
		$live = $post['live'];

		for ($i = 0; $i < $ln; $i++) {

			if (isset($prty[$i]))
				$pty = $prty[$i];
			else
				$pty = 0;
			if (isset($live[$i]))
				$lived = $live[$i];
			else
				$lived = 0;
			if (isset($id[$i]))
				$pid = $id[$i];
			else
				$pid = 0;


			if ($id) {
				$q = "UPDATE sandwich_item_options SET priority =  '$pty' , active = '$lived'  WHERE id = '$pid'  ";
				$this->db->Query($q);
			}
		}

	}
	/**
	 * List facebook friend menu
	 */
	function getFBfriendsMenu($post)
	{
		$id     = $post['id'];
		$sql    = "SELECT `uid`,`friend_uid`,`parent_uid`,`uid`,`first_name`,`last_name`,`username` FROM `friend_data` AS `FRIEND` JOIN `users` AS `USER` ON `FRIEND`.`friend_uid` =  `USER`.`fb_id` WHERE `FRIEND`.`parent_uid` = '$id' ";
		$result = $this->db->Query($sql);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * check fb login
	 */
	function checkFBLogin($post)
	{
		$id     = $post['id'];
		$sql    = "SELECT `fb_id` FROM `users`  WHERE `uid` = '$id' ";
		$result = $this->db->Query($sql);
		return $res = $this->db->FetchArrayRow($result);
	}
	/**
	 * List friend Sandwich Count
	 */
	
	
	function fill_unique_slugs(){
		
		
	}
	
	function getfriendSandwichCount($post)
	{
		
		$id     = $post['uid'];
		$sql    = "SELECT COUNT( * ) AS  `COUNT` FROM  `user_sandwich_data` WHERE  `uid` = '$id' AND `menu_is_active` = 1 ";
		$result = $this->db->Query($sql);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Process input
	 */
	private function process_input($data)
	{
		array_walk($data, function(&$val, $key)
		{
			$val = urldecode($val);
			$val = $this->clean_for_sql($val);
		});
		return $data;
	}
}