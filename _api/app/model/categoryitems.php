<?php
date_default_timezone_set('America/New_York');

class categoryitemsModel extends Model
{

	public $db; //database connection object
	/**
	* invoke database connection object
	*/
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * List pickup address
	 */
	public function add_catgory_items($post)
	{

		$data = $this->process_input($post);

		$item_image        = urldecode($data['item_image']);
		$item_image_sliced = urldecode($data['item_image_sliced']);
		$image_long        = urldecode($data['image_long']);
		$image_round       = urldecode($data['image_round']);
		$image_trapezoid   = urldecode($data['image_trapezoid']);

		if (isset($post['taxable']) AND $post['taxable'])
			$taxable = 1;
		else
			$taxable = 0;
		
		if (isset($post['premium']) AND $post['premium'])
			$premium = 1;
		else
			$premium = 0;
		

		$query       = "SELECT MAX( priority )+1 from `sandwich_category_items` WHERE category_id=" . $data['category_id'] . " ";
		$result      = $this->db->Query($query);
		$prioritymax = $this->db->FetchArray($result);
		$priority    = $prioritymax[0];

		$img_query       = "SELECT MAX( image_priority )+1 from `sandwich_category_items` WHERE category_id=" . $data['category_id'] . " ";
		$img_result      = $this->db->Query($img_query);
		$img_prioritymax = $this->db->FetchArray($img_result);
		$img_priority    = $img_prioritymax[0];

		$sheet_query       = "SELECT MAX( sheet_priority )+1 from `sandwich_category_items` WHERE category_id=" . $data['category_id'] . " ";
		$sheet_result      = $this->db->Query($sheet_query);
		$sheet_prioritymax = $this->db->FetchArray($sheet_result);
		$sheet_priority    = $sheet_prioritymax[0];

		$query = "INSERT INTO `sandwich_category_items` (item_name, item_price, category_id, item_image,item_image_sliced,image_long,image_round,image_trapezoid,bread_type,bread_shape,options_id, taxable, premium , priority, image_priority, sheet_priority, option_images) values ('" . $data['item_name'] . "','" . $data['item_price'] . "','" . $data['category_id'] . "','" . $item_image . "','" . $item_image_sliced . "','" . $image_long . "','" . $image_round . "','" . $image_trapezoid . "','" . $data['bread_type'] . "','" . $data['bread_shape'] . "','" . $data['select_sandwich_option'] . "', '" . $taxable . "', '".$premium."' , '" . $priority . "','" . $img_priority . "','" . $sheet_priority . "','" . $data['option_images']. "') ";
		return $this->db->Query($query);
	}
	/**
	 * List custom category items
	 */
	public function get_custom_catgory_items()
	{

		$query  = "SELECT * FROM `sandwich_categories` ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * List custom category items list
	 */
	public function get_custom_catagory_items_list($post)
	{

		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `sandwich_category_items` WHERE category_id = '" . $id . "' ORDER BY priority ASC ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List sandwich category item list
	 */
	public function get_sandwich_category_items_list($post)
	{

		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `sandwich_category_items` WHERE id = '" . $id . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Process input
	 */
	private function process_input($data)
	{
		array_walk($data, function(&$val, $key)
		{
			$val = $this->clean_for_sql($val);
			$val = urldecode($val);
		});
		return $data;
	}
	/**
	 * Update category item
	 */
	public function update_catgory_items($post)
	{

		$data              = $this->process_input($post);
		$item_image        = urldecode($data['item_image']);
		$item_image_sliced = urldecode($data['item_image_sliced']);
		$image_long        = urldecode($data['image_long']);
		$image_round       = urldecode($data['image_round']);
		$image_trapezoid   = urldecode($data['image_trapezoid']);
         
		if (isset($post['premium']) AND $post['premium'])
			$premium = 1;
		else
			$premium = 0;

		if (isset($post['taxable']) AND $post['taxable'])
			$taxable = 1;
		else
			$taxable = 0;

		$query = "UPDATE `sandwich_category_items` SET `item_name` = '" . $data['item_name'] . "',`bread_type` = '" . $data['bread_type'] . "',`bread_shape` = '" . $data['bread_shape'] . "',`options_id` = '" . $data['select_sandwich_option'] . "', `item_price` = '" . $data['item_price'] . "', `item_image` = '" . $item_image . "',`item_image_sliced` = '" . $item_image_sliced . "', `image_long` = '" . $data['image_long'] . "', `image_round` = '" . $image_round . "',`image_trapezoid` = '" . $image_trapezoid . "', taxable = '".$taxable."' , premium = '".$premium."', option_images = '".$data['option_images']."' ";
		
		$query = $query . " WHERE `sandwich_category_items`.`id` = '" . $data['id'] . "' ";
		if ($data['item_price'] != $data['old_price']) {
			//$this->updateAllSandwichPrice($data);
		}

		return $this->db->Query($query);
	}
	/**
	 * update all sandwich price
	 */
	public function updateAllSandwichPrice($data)
	{

		$query      = "SELECT id, category_identifier FROM sandwich_categories WHERE id='" . $data['category_id'] . "'";
		$result     = $this->db->Query($query);
		$category   = $this->db->FetchObjectRow($result);
		$identifier = $category->category_identifier;

		$query      = "SELECT id, sandwich_data FROM user_sandwich_data ";
		$result     = $this->db->Query($query);
		$sandwiches = $this->db->FetchAllArray($result);

		if ($this->db->Rows()) {

			foreach ($sandwiches as $sandwich) {

				$sandwich_data = $sandwich['sandwich_data'];
				$sandwich_data = json_decode($sandwich_data);

				$item_id    = $sandwich_data->$identifier->item_id;
				$item_price = $sandwich_data->$identifier->item_price;
				$key        = array_search($data['id'], $item_id);

				if (strlen($key)) {

					$item_price[$key]                       = $data['item_price'];
					$sandwich_data->$identifier->item_price = $item_price;

				}
				$bread_item_name=$sandwich_data->BREAD->item_name[0];
                                $sandwich_data = json_encode($sandwich_data);
                                if($bread_item_name=="3-Foot Hero" || $bread_item_name=="6-Foot Hero" || $bread_item_name=="3-Foot Hero (Catering)" || $bread_item_name=="6-Foot Hero (Catering)")
                                {
                                    $price = $this->price_updated($sandwich_data);
                                }else{
                                    $price = $this->price_updated($sandwich_data)+$data['base_fare'];
                                }

				$query  = "UPDATE user_sandwich_data SET sandwich_data = '$sandwich_data' , sandwich_price =  '$price' WHERE id = '" . $sandwich['id'] . "'";
				$result = $this->db->Query($query);

			}
		}


	}
	/**
	 * Update price
	 */
	public function price_updated($json)
	{
		$price = 0;
		$data  = json_decode($json);

		foreach ($data as $keys => $values) {

			$price += array_sum($values->item_price);

			if ($keys == 'BREAD' && isset($values->type) && $values->type > 0) {
				$price = $values->item_price[0];
				break;
			}

		}

		return $price;

	}
	/**
	 * Delete category item
	 */
	public function delete_catgory_items($data)
	{
		$item_image = urldecode($data['item_image']);
		
		$query      = "DELETE FROM `sandwich_category_items` WHERE `id` = '" . $data['id'] . "';";

		return $this->db->Query($query);
	}
	/**
	 * List sandwich category item list
	 */
	public function get_bread_data()
	{
		$id     = 1;
		$query  = "SELECT * FROM `sandwich_category_items` WHERE category_id = '" . $id . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * List customer data
	 */
	public function get_customer_data()
	{
		$start = $limit = 0;
		if (isset($_POST['start']) && ($_POST['start'] >= 0)) {
			$start = $_POST['start'];
		}

		if (isset($_POST['limit']) && ($_POST['limit'] >= 0)) {
			$limit = $_POST['limit'];
		}

		$stype = '';
		$sval  = '';
		if (isset($_POST['search']))
			$stype = $_POST['search'];
		if (isset($_POST['searchval']))
			$sval = $_POST['searchval'];

		$cond_query = "";
		if (!empty($stype)) {

			switch ($stype) {

				case 'name':
					$cond_query .= " where u.first_name like '%" . $sval . "%'";
					break;
				case 'lastname':
					$cond_query .= " where u.last_name like '%" . $sval . "%'";
					break;
				case 'phone':
					$cond_query .= " where u.phone like '%" . $sval . "%'";
					break;

				case 'email':
					$sval=str_replace('%40','@',$sval);
					$cond_query .= " where u.username like '%" . $sval . "%'";
					break;

				case 'company':
					$cond_query .= " where u.company like '%" . $sval . "%'";
					break;

			}

		}

		$query = "SELECT * FROM users as u $cond_query ORDER BY u.uid DESC";
		if (empty($stype)) {
		$query .= " LIMIT $start, $limit";
		}
		// $query;exit;
		$result = $this->db->Query($query);
		$users  = $this->db->FetchAllArray($result);


		foreach ($users as $key => $user) {

			//$query = "SELECT count(order_item_id) as sandwich_count, SUM(item_price) as total_price FROM orders_items WHERE user_id = {$user['uid']}  and ordered = 1 GROUP BY user_id";

			$query = "SELECT COUNT(`orders_items`.`order_item_id`) as sandwich_count, SUM(`orders`.`total`) as total_price  FROM `orders_items` LEFT JOIN `orders` ON `orders_items`.`order_item_id` = `orders`.`order_item_id` WHERE `orders_items`.`user_id` = {$user['uid']} AND  `orders_items`.`ordered` = 1 GROUP BY `orders_items`.`user_id`";

			$result = $this->db->Query($query);
			$result = $this->db->FetchAllArray($result);

			if (isset($result[0])) {
				$result                 = $result[0];
				$user['sandwich_count'] = $result['sandwich_count'];
				$user['total_price']    = $result['total_price'];
			} else {

				$user['sandwich_count'] = 0;
				$user['total_price']    = 0;

			}

			$users[$key] = $user;
		}

		return $users;

	}
	/**
	 * List customer data
	 */
	public function get_customer_data_count()
	{
		$query = "SELECT count(*) as count FROM users";
		$result = $this->db->Query($query);
		$users_count  = $this->db->FetchAllArray($result);
		return $users_count[0];
	}

	/**
	 * List all sandwich information
	 */
	public function get_all_sandwich_data()
	{
		$id     = 1;
		$query  = "SELECT * FROM `user_sandwich_data`";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * List standard category item list
	 */
	public function get_standard_catgory_items()
	{

		$query  = "SELECT * FROM `standard_category` WHERE type='standard'";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * List catering category item list
	 */
	public function get_catering_catgory_items()
	{

		$query  = "SELECT * FROM `standard_category` WHERE `type`='catering' AND `sort` != 0 ORDER BY sort ASC";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * List standard catgory items list
	 */
	public function get_standard_catgory_items_list($post)
	{

		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `standard_category_products` WHERE standard_category_id = '" . $id . "' ORDER BY priority ASC ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List standard catgory items list front
	 */
	public function get_standard_catgory_items_list_front($post)
	{

		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `standard_category_products` WHERE standard_category_id = '" . $id . "' AND live = '1'  ORDER BY priority ASC ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Insert standard catgory items 
	 */
	public function add_standard_catgory_items($data)
	{

		$data = $this->process_input($data);

		$query = "INSERT INTO `standard_category` (standard_cat_name,category_identifier, type) values ('" . $data['standard_cat_name'] . "','" . strtoupper($data['standard_cat_name']) . "', '" . $data["type"] . "') ";
		return $this->db->Query($query);
	}
	/**
	 * Insert standard catgory product
	 */
	public function add_standard_catgory_products($post)
	{
		$data = $this->process_input($post);
		if (isset($post['taxable']) && $post['taxable'])
			$taxable = 1;
		else
			$taxable = 0;

		if (isset($post['allow_spcl_instruction']) && $post['allow_spcl_instruction'])
			$allow_spcl_instruction = 1;
		else
			$allow_spcl_instruction = 0;

		if (isset($post['add_modifier']) && $post['add_modifier'])
			$add_modifier = 1;
		else
			$add_modifier = 0;
		
		$query       = "SELECT MAX(`priority`)+1 from `standard_category_products` WHERE standard_category_id = '" . $data['standard_category_id'] . "' ";
		$result      = $this->db->Query($query);
		$prioritymax = $this->db->FetchArray($result);
		$priority    = $prioritymax[0];
		$query       = "INSERT INTO `standard_category_products` (product_name,description,product_image,product_price,standard_category_id, taxable, priority,active_extras,live,allow_spcl_instruction,add_modifier,modifier_options,modifier_isoptional,modifier_is_single,modifier_desc) values ('" . $data['product_name'] . "','" . $data['description'] . "','" . $data['product_image'] ."','" . $data['product_price'] . "','" . $data['standard_category_id'] . "','$taxable', '$priority','" . $data['is_extra'] . "',1,'" . $data['allow_spcl_instruction'] . "','" . $data['add_modifier'] . "','" . $data['modifier_options'] . "','" . $data['modifier_isoptional'] . "','" . $data['modifier_is_single'] . "','" . $data['modifier_desc'] . "') ";
		$this->db->Query($query);
		$ins_id = $this->db->insertId();

		$desc       = explode(',', $data['descriptor']);
		$desc_ids   = explode(',', $data['descriptor_ids']);
		$extra_name = explode(',', $data['extra_name']);
		$extra_id   = explode(',', $data['extra_id']);
		$extra_cost = explode(',', $data['extra_cost']);

		foreach ($desc as $keys => $values) {

			if ($desc) {
				$ds_q = "INSERT INTO `product_descriptor` (`pid`,`desc`) VALUES ('$ins_id','$values') ";
				$this->db->Query($ds_q);
				$insid = $this->db->insertId();

				foreach ($extra_name as $key => $extra) {
					if ($extra) {

						$cost          = $extra_cost[$key];
						$extra_default = $data['extra_default'];
						if ($extra == $extra_default)
							$default = 1;
						else
							$default = 0;
						$exquery = "INSERT INTO `product_extras` (pid,name,cost,is_default) VALUES ('$insid','$extra','$cost','$default')";
						$this->db->Query($exquery);
					}
				}

			}

		}

		foreach ($extra_name as $key => $extra) {
			if ($extra) {



				$id            = $extra_id[$key];
				$cost          = $extra_cost[$key];
				$extra_default = $data['extra_default'];
				if ($extra == $extra_default)
					$default = 1;
				else
					$default = 0;


				$this->db->Query("INSERT INTO `product_extras` (pid,name,cost,is_default) VALUES ('$ins_id','$extra','$cost','$default')");
			}
		}
		/* Create Record for Tab users, Start*/
			$exquery1 = "update `updation_handeler` set isNewUpdation='0' order by `id` desc limit 1";
			$this->db->Query($exquery1);
			$exquery2 = "INSERT INTO `updation_handeler` (isNewUpdation) VALUES ('1')";
			$this->db->Query($exquery2); 
		/* Create Record, End */
		return $ins_id;
	}
	/**
	 * List standard product items
	 */
	public function get_standard_product_items($post)
	{

		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `standard_category_products` WHERE id = '" . $id . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * update standard catgory product
	 */
	public function update_standard_category_products($post)
	{
		
		if (isset($post['taxable']) && $post['taxable'])
			$taxable = 1;
		else
			$taxable = 0;

		if (isset($post['allow_spcl_instruction']) && $post['allow_spcl_instruction'])
			$allow_spcl_instruction = 1;
		else
			$allow_spcl_instruction = 0;

		if (isset($post['add_modifier']) && $post['add_modifier'])
			$add_modifier = 1;
		else
			$add_modifier = 0;

		$data  = $this->process_input($post);

		$query = "UPDATE `standard_category_products` SET `product_name` = '" . $data['product_name'] . "',`description`='" . $data['description'] . "',`product_image` = '" . $data['product_image'] ."', `product_price` = '" . $data['product_price'] . "'
		, `taxable` = $taxable , `active_extras` = '" . $data['is_extra'] . "',`allow_spcl_instruction` = $allow_spcl_instruction,`add_modifier` = $add_modifier,`modifier_options` = '" . $data['modifier_options'] . "',`modifier_isoptional` = '" . $data['modifier_isoptional'] . "',`modifier_is_single` = '" . $data['modifier_is_single'] . "',`modifier_desc` = '" . $data['modifier_desc'] . "'  WHERE `id` = '" . $data['id'] . "' ";

		$desc       = explode(',', $data['descriptor']);
		$desc_ids   = explode(',', $data['descriptor_ids']);
		$extra_name = explode(',', $data['extra_name']);
		$extra_id   = explode(',', $data['extra_id']);
		$extra_cost = explode(',', $data['extra_cost']);


		foreach ($desc as $keys => $values) {

			if ($desc) {

				$ds_id = $desc_ids[$keys];

				if (!$ds_id) {
					$ds_q = "INSERT INTO `product_descriptor` (`pid`,`desc`) VALUES ('" . $data['id'] . "','$values') ";
				} else {
					$ds_q = "UPDATE `product_descriptor` SET  `desc` = '$values' WHERE id = '$ds_id' ";
				}



				$this->db->Query($ds_q);
				$insid = $this->db->insertId();
				if (!$insid)
					$insid = $ds_id;


				foreach ($extra_name as $key => $extra) {
					if ($extra) {
						$id            = $extra_id[$key];
						$cost          = $extra_cost[$key];
						$extra_default = $data['extra_default'];
						if ($extra == $extra_default)
							$default = 1;
						else
							$default = 0;

						if ($id) {
							$exquery = "UPDATE `product_extras` SET pid = '$insid' , name = '$extra' , cost = '$cost' , is_default = '$default' WHERE id = '$id' ";
						} else {
							$exquery = "INSERT INTO `product_extras` (pid,name,cost,is_default) VALUES ('$insid','$extra','$cost','$default')";
						}
						$this->db->Query($exquery);
					}
				}

			}

		}


		return $this->db->Query($query);
	}
	/**
	 * List standard product extra data
	 */
	public function get_standard_product_extraData($post)
	{
		$post   = $this->process_input($post);
		$query  = " SELECT *  FROM product_descriptor WHERE pid = '" . $post['pid'] . "' ";
		$result = $this->db->Query($query);
		$data   = $this->db->FetchAllArray($result);

		$in = array();
		$ch = array();
		foreach ($data as $data) {
			$xt          = "SELECT * FROM product_extras WHERE pid = '" . $data['id'] . "' ";
			$xout        = $this->db->Query($xt);
			$ch['id']    = $data['id'];
			$ch['pid']   = $data['pid'];
			$ch['desc']  = $data['desc'];
			$ch['extra'] = $this->db->FetchAllArray($xout);
			$in[]        = $ch;
		}
		return $in;
	}
	/**
	 * Delete standard category product
	 */
	public function delete_standard_category_products($data)
	{
		$query = "DELETE FROM `standard_category_products` WHERE `id` = '" . $data['id'] . "';";
		//delete extras if any

		$extras_query = " DELETE FROM `product_extras` WHERE pid = '" . $data['id'] . "' ";
		$this->db->Query($extras_query);
		return $this->db->Query($query);
	}
	/**
	 * update standard category priority
	 */
	public function update_standard_category_priority($data)
	{
		$post = $this->process_input($data);

		$ids          = explode(',', $post['id']);
		$priority     = explode(',', $post['priority']);
		$livecheckbox = explode(',', $post['livecheckbox']);


		foreach ($ids as $key => $val) {

			if (isset($livecheckbox[$key]) AND $livecheckbox[$key])
				$livecheckboxvalue = $livecheckbox[$key];
			else
				$livecheckboxvalue = 0;

			$pri   = $priority[$key];
			$query = "UPDATE `standard_category_products` SET `priority` = '" . $pri . "', `live` = " . $livecheckboxvalue . "  WHERE `standard_category_products`.`id` = '" . $val . "' ";

			$a = $this->db->Query($query);

		}
	}
	/**
	 * List all pickup items
	 */
	public function get_all_pickups_items($post)
	{

		$is_store = "";
		if (isset($post['store_id']) && $post['store_id']) {
			$is_store = " AND store_id = {$post['store_id']}";
		}

		if (isset($post['search_date']))
			$str_to_time = date('Y-m-d', strtotime($post['search_date']));
		else
			$str_to_time = date('Y-m-d');

		$result = array();
		$select = "SELECT *,  DATE_FORMAT(date,'%a, %d %b %Y') as date, DAYNAME(date) as dayname, DATE_FORMAT(date,'%h:%i %p ') as timeformat, TIMESTAMPDIFF(MINUTE, NOW(), orders.date) as timediff FROM orders where delivery=0 AND DATE(date)= '$str_to_time' $is_store ORDER BY orders.order_date DESC";

		$select_result = $this->db->Query($select);
		$orders        = $this->db->FetchAllArray($select_result);

		if ($orders) {
			foreach ($orders as $key => $order) {
				/*Getting User Details which are coming through tab, Start*/
				 	//if($order->by_tab==1){
						//$order['tab'] =  [$order['firstName'],$order['lastName'],"<br />".$order['email']]; 					 
				 	//}
				/*Getting User Details which are coming through tab, End*/
				if ($order['delivery']) {
					$query                  = "SELECT * FROM order_address WHERE address_id = {$order['address_id']}";
					$order['delivery_type'] = "Delivery";
				} else {
					$query                  = "SELECT * FROM pickup_stores WHERE id = {$order['store_id']}";
					$order['delivery_type'] = "Pickup";
				}
 
				$result           = $this->db->query($query);
				$order['address'] = "";
				if ($this->db->Rows($result)) {
					$address = $this->db->FetchAllArray($result);

					$order['address'] = $address[0];
				}

				$ordersItems = str_replace(":", ",", $order['order_item_id']);
				$query       = "SELECT * FROM orders_items WHERE order_item_id IN ($ordersItems)"; //29309, 29284 // 29313, 17415
				$result      = $this->db->query($query);
				$ordersItems = $this->db->FetchAllArray($result);

				$items = array();

				foreach ($ordersItems as $item) {

					if ($item['item_type'] == "user_sandwich") {
						$query    = "SELECT * FROM user_sandwich_data WHERE id = {$item['item_id']}"; //11761  11743
						$name_col = "sandwich_name";
					} else if ($item['item_type'] == "product") {
						$query    = "SELECT * FROM standard_category_products WHERE id = {$item['item_id']}";
						$name_col = "product_name";
					}

					$result            = $this->db->query($query);
					$item_temp         = $this->db->FetchAllArray($result);
					$item_temp         = $item_temp[0];
					$item_temp['qty']  = $item['item_qty'];
					$item_temp['name'] = $item_temp[$name_col];
					$items[]           = $item_temp;
				}

				$order['items'] = $items;

				$orders[$key] = $order;
			} 
			return $orders;
		} else {
			$orders = array();
		}
		
		return $orders;

	}
	/**
	 * List all order status
	 */
	public function get_all_order_status($post)
	{
		$status = $post['status'];

		$query  = "SELECT * FROM `order_status` where status=$status";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * update order status
	 */
	public function update_order_status($data)
	{

		$update = "";

		$current_time = date('Y-m-d H:i:s');

		if ($data['status_code'] == 7) { //if delivered
			$update = ", order_delivered = '".$current_time."'";

		}
		if ($data['status_code'] == 6) 
		{
			$qry = "UPDATE `orders` SET  `order_delivered` = '0000-00-00 00:00:00' WHERE `order_id` = '". $data['order_id']."'";
        	$this->db->Query($qry);
		}
		if ($data['status_code'] == 5) 
		{
			$qry = "UPDATE `orders` SET `delivery_assigned_to` = 0, `delivery_assigned_time` = '0000-00-00 00:00:00' WHERE `order_id` = '". $data['order_id']."'";
        	$this->db->Query($qry);
		}

		$query = "UPDATE `orders` SET `order_status` = '" . $data['status_code'] . "' $update  where order_id='" . $data['order_id'] . "' ";

		$a = $this->db->Query($query);
	}
	/**
	 * List all delivery items
	 */
	public function get_all_delivery_items($post)
	{

		$dt = new DateTime('now', new DateTimezone('America/New_York'));
		$is_store = "";
		if (isset($post['store_id']) && $post['store_id']) {
			$is_store = " AND o.store_id = {$post['store_id']}";
		}
		if (isset($post['search_date']))
			$str_to_time = date('Y-m-d', strtotime(urldecode($post['search_date'])));
		else
			$str_to_time = date('Y-m-d');

		$result = array();
		// neede to make the date >= not just > because was not returning naything.  Is this case in live?  Prolly not an issue there...wont be here either when we finally unfk everything
		$select = "SELECT o.*,  DATE_FORMAT(date,'%a, %d %b %Y') as date, DAYNAME(date) as dayname, DATE_FORMAT(date,'%h:%i %p ') as timeformat, TIMESTAMPDIFF(MINUTE, '".$dt->format('Y-m-d H:i:s')."', o.date) as timediff, du.uid as delivery_uid, du.name as delivery_uname FROM orders as o LEFT JOIN delivery_user as du ON o.delivery_assigned_to = du.uid where o.delivery=1 AND DATE(o.date)>= '$str_to_time' $is_store ORDER BY o.order_date ASC";

		file_put_contents("query.txt", print_r($select, true));

		$select_result = $this->db->Query($select);
		$orders        = $this->db->FetchAllArray($select_result);

		if ($orders) {
			foreach ($orders as $key => $order) {

				if ($order['delivery']) {
					$query                  = "SELECT * FROM order_address WHERE address_id = {$order['address_id']}";
					$order['delivery_type'] = "Delivery";
				} else {
					$query                  = "SELECT * FROM pickup_stores WHERE id = {$order['address_id']}";
					$order['delivery_type'] = "Pickup";
				}



				$result           = $this->db->query($query);
				$order['address'] = "";
				if ($this->db->Rows($result)) {
					$address = $this->db->FetchAllArray($result);

					$order['address'] = $address[0];
				}

				$ordersItems = str_replace(":", ",", $order['order_item_id']);
				$query       = "SELECT * FROM orders_items WHERE order_item_id IN ($ordersItems)";
				$result      = $this->db->query($query);
				$ordersItems = $this->db->FetchAllArray($result);

				$items = array();

				foreach ($ordersItems as $item) {

					if ($item['item_type'] == "user_sandwich") {
						$query    = "SELECT * FROM user_sandwich_data WHERE id = {$item['item_id']}";
						$name_col = "sandwich_name";
					} else if ($item['item_type'] == "product") {
						$query    = "SELECT * FROM standard_category_products WHERE id = {$item['item_id']}";
						$name_col = "product_name";
					}

					$result    = $this->db->query($query);
					$item_temp = $this->db->FetchAllArray($result);
					if (isset($item_temp[0]))
						$item_temp = $item_temp[0];
					$item_temp['qty'] = $item['item_qty'];
					if ($item['item_type'] == "product"){
						$item_temp['modifiers'] = $item['extra_id'];
						$item_temp['spl_instructions'] = $item['spcl_instructions'];
					}
					if (isset($item_temp[$name_col]))
						$item_temp['name'] = $item_temp[$name_col];
					else
						$item_temp['name'] = '';
					$items[] = $item_temp;
				}

				$order['items'] = $items;

				$orders[$key] = $order;
			}

			return $orders;
		} else {
			$orders = array();
		}
		
		return $orders;

	}

	/**
	 * List all active delivery items of an user
	 */
	public function get_user_active_delivery_items($post)
	{
		foreach ($post as $key => $value) {
			$$key = $this->clean_for_sql(urldecode($value));
		}
		
		$dt = new DateTime('now', new DateTimezone('America/New_York'));
		
		$str_to_time = date('Y-m-d');

		$result = array();
		// neede to make the date >= not just > because was not returning naything.  Is this case in live?  Prolly not an issue there...wont be here either when we finally unfk everything
		$select = "SELECT o.*,  DATE_FORMAT(date,'%a, %d %b %Y') as date, DAYNAME(date) as dayname, DATE_FORMAT(date,'%h:%i %p ') as timeformat, TIMESTAMPDIFF(MINUTE, '".$dt->format('Y-m-d H:i:s')."', o.date) as timediff, du.uid as delivery_uid, du.name as delivery_uname FROM orders as o LEFT JOIN delivery_user as du ON o.delivery_assigned_to = du.uid where o.order_status != 8 AND o.user_id = '$user_id' AND o.delivery=1 AND DATE(o.date)>= '$str_to_time' ORDER BY o.order_date ASC";

		//file_put_contents("query.txt", print_r($select, true));exit;

		$select_result = $this->db->Query($select);
		$orders        = $this->db->FetchAllArray($select_result);

		if ($orders) {
			foreach ($orders as $key => $order) {

				if ($order['delivery']) {
					$query                  = "SELECT * FROM order_address WHERE address_id = {$order['address_id']}";
					$order['delivery_type'] = "Delivery";
				} else {
					$query                  = "SELECT * FROM pickup_stores WHERE id = {$order['address_id']}";
					$order['delivery_type'] = "Pickup";
				}



				$result           = $this->db->query($query);
				$order['address'] = "";
				if ($this->db->Rows($result)) {
					$address = $this->db->FetchAllArray($result);

					$order['address'] = $address[0];
				}

				$ordersItems = str_replace(":", ",", $order['order_item_id']);
				$query       = "SELECT * FROM orders_items WHERE order_item_id IN ($ordersItems)";
				$result      = $this->db->query($query);
				$ordersItems = $this->db->FetchAllArray($result);

				$items = array();

				foreach ($ordersItems as $item) {

					if ($item['item_type'] == "user_sandwich") {
						$query    = "SELECT * FROM user_sandwich_data WHERE id = {$item['item_id']}";
						$name_col = "sandwich_name";
					} else if ($item['item_type'] == "product") {
						$query    = "SELECT * FROM standard_category_products WHERE id = {$item['item_id']}";
						$name_col = "product_name";
					}

					$result    = $this->db->query($query);
					$item_temp = $this->db->FetchAllArray($result);
					if (isset($item_temp[0]))
						$item_temp = $item_temp[0];
					$item_temp['qty'] = $item['item_qty'];
					if ($item['item_type'] == "product"){
						$item_temp['modifiers'] = $item['extra_id'];
						$item_temp['spl_instructions'] = $item['spcl_instructions'];
					}
					if (isset($item_temp[$name_col]))
						$item_temp['name'] = $item_temp[$name_col];
					else
						$item_temp['name'] = '';
					$items[] = $item_temp;
				}

				$order['items'] = $items;

				$orders[$key] = $order;
			}

			return $orders;
		} else {
			$orders = array();
		}
		
		return $orders;

	}
	/**
	 * List all search delivery items
	 */
	public function get_search_delivery_items($post)
	{
		$dt = new DateTime('now', new DateTimezone('America/New_York'));
		foreach ($post as $key => $val)
			$post[$key] = urldecode($val);


		$status_flag = $post['status_flag'];

		$cond = '';
		if (isset($post['order_status']) != '' && strlen($post['order_status'])) {
			$cond .= " AND o.order_status='" . $post['order_status'] . "' ";
		}

		if (isset($post['store_id']) && $post['store_id']) {
			$cond .= " AND o.store_id='" . $post['store_id'] . "' ";
		}


		if (isset($post['date']) && $post['date'] != '1970-01-01') {

			if ($post['date']) {
				$cond .= " AND DATE(date) ='" . $post['date'] . "' ";
                $str_to_time = date('Y-m-d', strtotime($post['date']));
			} else {
				$cond .= " AND DATE(date) = '" . date("Y-m-d") . "' ";
                $str_to_time = date('Y-m-d');
            }
		}
		/*if (isset($post['search_date']))
			$str_to_time = date('Y-m-d', strtotime(urldecode($post['search_date'])));
		else
			$str_to_time = date('Y-m-d');
        */

		if (!isset($post['delivery'])) {
			$delivery = 0;
		} else
			$delivery = $post['delivery'];

		$result = array();

		// $select = "SELECT *,  DATE_FORMAT(date,'%a, %d %b %Y') as date, DAYNAME(date) as dayname, DATE_FORMAT(date,'%h:%i %p ')as timeformat, TIMESTAMPDIFF(MINUTE, NOW(), orders.date) as timediff FROM orders where delivery=$delivery $cond ORDER BY orders.order_date ASC";
		$select = "SELECT o.*,  DATE_FORMAT(date,'%a, %d %b %Y') as date, DAYNAME(date) as dayname, DATE_FORMAT(date,'%h:%i %p ') as timeformat, TIMESTAMPDIFF(MINUTE, '".$dt->format('Y-m-d H:i:s')."', o.date) as timediff, du.uid as delivery_uid, du.name as delivery_uname FROM orders as o LEFT JOIN delivery_user as du ON o.delivery_assigned_to = du.uid where o.delivery=1 AND DATE(o.date)>= '$str_to_time' ".$cond." ORDER BY o.order_date ASC";
        file_put_contents("test.txt",print_r($select,true));
		$select_result = $this->db->Query($select);
		$orders        = $this->db->FetchAllArray($select_result);

		if ($orders) {
			foreach ($orders as $key => $order) {

				if ($order['delivery']) {
					$query                  = "SELECT * FROM order_address WHERE address_id = {$order['address_id']}";
					$order['delivery_type'] = "Delivery";
				} else {
					$query                  = "SELECT * FROM pickup_stores WHERE id = {$order['address_id']}";
					$order['delivery_type'] = "Pickup";
				}



				$result           = $this->db->query($query);
				$order['address'] = "";
				if ($this->db->Rows($result)) {
					$address = $this->db->FetchAllArray($result);

					$order['address'] = $address[0];
				}

				$ordersItems = str_replace(":", ",", $order['order_item_id']);
				$query       = "SELECT * FROM orders_items WHERE order_item_id IN ($ordersItems)";
				$result      = $this->db->query($query);
				$ordersItems = $this->db->FetchAllArray($result);

				$items = array();

				foreach ($ordersItems as $item) {

				/*Getting User Details which are coming through tab, Start*/
				 	//if($order->by_tab==1){
						//$order['tab'] =  [$order['firstName'],$order['lastName'],"<br />".$order['email']]; 					 
				 	//}
				/*Getting User Details which are coming through tab, End*/
				
					if ($item['item_type'] == "user_sandwich") {
						$query    = "SELECT * FROM user_sandwich_data WHERE id = {$item['item_id']}";
						$name_col = "sandwich_name";
					} else if ($item['item_type'] == "product") {
						$query    = "SELECT * FROM standard_category_products WHERE id = {$item['item_id']}";
						$name_col = "product_name";
					}

					$result            = $this->db->query($query);
					$item_temp         = $this->db->FetchAllArray($result);
					$item_temp         = $item_temp[0];
					if ($item['item_type'] == "product"){
						$item_temp['modifiers'] = $item['extra_id'];
						$item_temp['spl_instructions'] = $item['spcl_instructions'];
					}
					$item_temp['qty']  = $item['item_qty'];
					$item_temp['name'] = $item_temp[$name_col];
					$items[]           = $item_temp;
				}

				$order['items'] = $items;

				$orders[$key] = $order;
			}

			return $orders;
		} else {
			$orders = array();
		}

		return $orders;

	}
	/**
	 * List all search delivery items
	 */
	function get_admin_user_data($post)
	{

		$query  = "select * from admin_user ad left join admin_user_category auc on auc.id=ad.admin_cat_id";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * List all search delivery users
	 */
	function get_delivery_user_data($post)
	{
		$query  = "SELECT *, (SELECT p.store_name FROM pickup_stores p WHERE p.id = d.store_id) as storeName FROM delivery_user d";
        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
	}
	/**
	 * List admin user category
	 */
	function get_admin_user_category($post)
	{

		$query  = "select * from  admin_user_category ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * Insert new admin user
	 */
	public function add_new_admin_user($data)
	{
		$data  = $this->process_input($data);
		$query = $query = "INSERT INTO `admin_user` (admin_cat_id,first_name, user_name, password) values ('" . $data['admin_cat_id'] . "','" . $data['name'] . "','" . $data['username'] . "','" . md5($data['password']) . "') ";
		return $this->db->Query($query);
	}
	/**
	 * List admin users
	 */
	public function get_admin_user($post)
	{
		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `admin_user` WHERE uid = '" . $id . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List delivery users
	 */
	public function get_delivery_user($post)
	{
		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `delivery_user` WHERE uid = '" . $id . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Update admin user 
	 */
	public function update_admin_user($data)
	{
		$data  = $this->process_input($data);
		$query = "UPDATE `admin_user` SET `first_name` = '" . $data['name'] . "',`admin_cat_id`='" . $data['admin_cat_id'] . "', `password` = '".md5($data['password'])."' where uid='" . $data['uid'] . "' ";

		$a = $this->db->Query($query);
	}
	/**
	 * Delete admin user
	 */
	public function delete_admin_user($data)
	{

		$query = "DELETE FROM `admin_user` WHERE `uid` = '" . $data['id'] . "';";
		return $this->db->Query($query);
	}
	/**
	 * Delete Delivery user
	 */
	public function delete_delivery_user($data)
	{

		$query = "DELETE FROM `delivery_user` WHERE `uid` = '" . $data['id'] . "';";
		return $this->db->Query($query);
	}
	/**
	 * List  store accounts data
	 */
	public function get_store_accounts_data($post)
	{
		$query  = "SELECT * FROM `pickup_stores` ORDER BY `ordered`";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Add new store account
	 */
	public function add_new_store_account($data)
	{

		$post      = $this->process_input($data);
		$query     = "INSERT INTO `admin_user` (first_name, user_name, password) values ('" . $post['name11'] . "','" . $post['username'] . "','" . md5($post['password']) . "') ";
		$aa        = $this->db->Query($query);
		$insert_id = mysql_insert_id();

		$update_query = "UPDATE `pickup_stores` SET `uid` = '" . $insert_id . "',`report_pin`='" . $post['report_pin'] . "' where id='" . $post['store_id'] . "' ";
		$this->db->Query($update_query);


		for ($j = 1; $j < 10; $j++) {
			if ($post["zipcode$j"] != '') {
				$query_store_zipcode = "INSERT INTO `store_zipcodes`(zipcode,abbreviation,store_id)values('" . @$post["zipcode$j"] . "','" . @$post["abbreviation$j"] . "','" . @$post["store_id"] . "') ";
				$this->db->Query($query_store_zipcode);
			}
		}

		for ($i = 0; $i < 7; $i++) {
			$query_store_timings = "INSERT INTO `store_timings`(day,open,close,store_id)values('" . @$post["day$i"] . "','" . @$post["day_open$i"] . "','" . @$post["day_close$i"] . "','" . @$post["store_id"] . "')";
			$aa                  = $this->db->Query($query_store_timings);
		}

	}
	/**
	 * List  store accounts data
	 */
	public function get_store_account($post)
	{

		$post = $this->process_input($post);
		$id   = $post['id'];

		$selectedResult                  = array();
		$query                           = "SELECT * FROM `pickup_stores` WHERE id = '" . $id . "' ";
		$res                             = $this->db->Query($query);
		$res                             = $this->db->FetchAllArray($res);
		$selectedResult['pickup_stores'] = $res[0];


		$store_timings_select            = "SELECT * FROM `store_timings` WHERE store_id='" . $id . "' ORDER BY count_order";
		$store_timings_result            = $this->db->Query($store_timings_select);
		$selectedResult['store_timings'] = $this->db->FetchAllArray($store_timings_result);

		$store_zipcodes_select            = "SELECT * FROM `store_zipcodes` WHERE store_id='" . $id . "' ORDER BY count_order";
		$store_zipcodes_result            = $this->db->Query($store_zipcodes_select);
		$selectedResult['store_zipcodes'] = $this->db->FetchAllArray($store_zipcodes_result);

		return $selectedResult;

	}
	/**
	 * Update store accpunt
	 */
public function update_store_account($data)
	{

		$post = $this->process_input($data);
		//echo '<pre>';print_r($post);exit;
		if ($post['id']) {

			$zipcount = $post['zipcount'];

			$append = "";

			if ($post['password'] != "")
				$append .= ",`password`='" . md5($post['password']) . "'";

			if (!isset($post['phone']))
				$post['phone'] = "";
				$specificday_active    = 0;
			if (isset($post['specificday_active']))
				$specificday_active    = 1;
                        if(isset($post['report_pin'])){
                            $pin_update = '`report_pin`="' . $post['report_pin'] . '",';
                        }else{
                            $pin_update = '';
                        }
			$update_query = "UPDATE `pickup_stores` SET ".$pin_update."`store_name`='" . $post['store_name'] . "', `address1`='" . $post['address1'] . "', `address2`='" . $post['address2'] . "',`zip`='" . $post['zip'] . "', `phone`='" . $post['phone'] . "', `specificday`='" . $post['specificday'] . "', `specificday_message`='" . $this->clean_for_sql($post['specificday_message']) . "', `specificday_active`='" . $specificday_active . "', `restrict_ip`='" . $post['restrict_ip'] . "' $append where id='" . $post['id'] . "' ";
			$this->db->Query($update_query);

			$file = 'active_day.log';
			file_put_contents($file,$update_query,FILE_APPEND);

			for ($i = 0; $i < 7; $i++) {
				$store_active    = 0;
				$pickup_active   = 0;
				$delivery_active = 0;

				if (isset($post["day$i"]))
					$store_active = 1;
				if (isset($post['checkbox-' . $i . '-pickup']))
					$pickup_active = 1;
				if (isset($post['checkbox-' . $i . '-delivery']))
					$delivery_active = 1;
				
				$update_store_timings = "UPDATE `store_timings` SET `store_active` = '" . $store_active . "', `delivery_active` = '" . $delivery_active . "', `pickup_active` = '" . $pickup_active . "',`open`='" . @$post["day_open$i"] . "',`close`='" . @$post["day_close$i"] . "',`store_id`='" . @$post["id"] . "' where count_order='$i' AND store_id='" . $post["id"] . "'  ";
				$this->db->Query($update_store_timings);
			}

			if (isset($post['zipcount'])){
			$query  = "DELETE FROM store_zipcodes WHERE store_id = '" . $post["id"] . "'";
			$result = $this->db->Query($query);
			}

			for ($j = 1; $j <= $zipcount; $j++) {

				if (!isset($post["abbreviation$j"]))
					$post["abbreviation$j"] = " ";


				if (!isset($post["minAmount$j"]))
						$post["minAmount$j"] = " ";
					
				if (strlen($post["zipcode$j"])) {
					$query_store_zipcode = "INSERT INTO `store_zipcodes`(zipcode,abbreviation,count_order,store_id,min_order,delivery_fee)values('" . $post["zipcode$j"] . "','" . $post["abbreviation$j"] . "',$j,'" . $post["id"] . "','".$post["minAmount$j"]."','".$post["deliveryfee$j"]."') ";

					$this->db->Query($query_store_zipcode);
				}
			}
			
		} else {

			$zipcount = $post['zipcount'];

			if (!isset($post['phone']))
				$post['phone'] = "";
				$specificday_active    = 0;
			if (isset($post['specificday_active']))
				$specificday_active    = 1;
			$update_query = "INSERT INTO `pickup_stores` (store_name, address1, address2, zip, phone, report_pin, restrict_ip, username, password, specificday, specificday_message,specificday_active) VALUES ('" . $post['store_name'] . "', '" . $post['address1'] . "', '" . $post['address2'] . "', '" . $post['zip'] . "', '" . $post['phone'] . "', '" . $post['report_pin'] . "', '" . $post['restrict_ip'] . "', '" . $post['username'] . "', '" . md5($post['password']) . "', '" . $post['specificday'] . "', '" . $post['specificday_message'] . "', '".$specificday_active."')";
			$this->db->Query($update_query);
			$insert_id = $this->db->insertId();

			for ($j = 1; $j <= $zipcount; $j++) {

				if (!isset($post["abbreviation$j"]))
					$post["abbreviation$j"] = " ";

				/* if (!isset($post["zipcode$j"]))
					$post["zipcode$j"] = " "; */
					if (!isset($post["minAmount$j"]))
						$post["minAmount$j"] = " ";
				if (strlen($post["zipcode$j"])) {
					$query_store_zipcode = "INSERT INTO `store_zipcodes`(zipcode,abbreviation,count_order,store_id,min_order,delivery_fee)values('" . $post["zipcode$j"] . "','" . $post["abbreviation$j"] . "',$j,'$insert_id','".$post["minAmount$j"]."','".$post["deliveryfee$j)"]."'";

					$this->db->Query($query_store_zipcode);
				}
			}
			for ($i = 0; $i < 7; $i++) {
				$store_active    = 0;
				$pickup_active   = 0;
				$delivery_active = 0;


				if (isset($post["day$i"]))
					$store_active = 1;
				if (isset($post['checkbox-' . $i . '-pickup']))
					$delivery_active = 1;
				if (isset($post['checkbox-' . $i . '-delivery']))
					$pickup_active = 1;

				if (!isset($post["day_open$i"]))
					$post["day_open$i"] = " ";

				if (!isset($post["day_close$i"]))
					$post["day_close$i"] = " ";

				if (!isset($post["day$i"]))
					$post["day$i"] = " ";

				$query_store_timings = "INSERT INTO `store_timings`(day,open,close,count_order,store_id,store_active,delivery_active,pickup_active)values('" . $post["day$i"] . "','" . $post["day_open$i"] . "','" . $post["day_close$i"] . "',$i,'$insert_id','$store_active','$delivery_active','$pickup_active')";

				$aa = $this->db->Query($query_store_timings);
			}

		}

	}
	/**
	 * Delete store account
	 */
	public function delete_store_account($data)
	{

		$delete_admin_query = "DELETE FROM `admin_user` WHERE `uid` = '" . $data['uid'] . "'";
		$this->db->Query($delete_admin_query);


		$delete_pickup_stores = "DELETE FROM `pickup_stores` WHERE `id` = '" . $data['id'] . "'";
		$this->db->Query($delete_pickup_stores);

		$delete_store_timings = "DELETE FROM `store_timings` WHERE `store_id` = '" . $data['id'] . "'";
		$this->db->Query($delete_store_timings);

		$delete_store_zipcodes = "DELETE FROM `store_zipcodes` WHERE `store_id` = '" . $data['id'] . "'";
		$this->db->Query($delete_store_zipcodes);
		return;
	}
	/**
	 * List standard category
	 */
	public function getStandardCategories()
	{
		$query = "SELECT * FROM  standard_category WHERE Type='standard'";
		$query = $this->db->Query($query);
		return $this->db->FetchAllArray($query);
	}
	/**
	 * List standard category products
	 */
	public function getStandardCategoryProducts($post)
	{
		foreach ($post as $key => $val)
			$$key = urldecode($val);

		$query = "SELECT * FROM  standard_category_products WHERE standard_category_id = '$standard_category_id' AND live = '1'  ORDER BY priority ";
		$query = $this->db->Query($query);
		return $this->db->FetchAllArray($query);
	}

	/**
	 * List sponser meal category products
	 */
	public function getSponserMealCategoryProducts($post)
	{
		foreach ($post as $key => $val)
			$$key = urldecode($val);
		$qry = "SELECT id FROM  standard_category WHERE category_identifier ='$category_identifier'";
		$qry = $this->db->Query($qry);
		$qry =  $this->db->FetchArrayAssoc($qry);
		$query = "SELECT * FROM  standard_category_products WHERE standard_category_id = '".$qry['id']."' AND live = '1'  ORDER BY priority ";

		$query = $this->db->Query($query);
		$res =  $this->db->FetchAllArray($query);
		return $res;
	}
	/**
	 * Insert web page banner data 
	 */
	public function add_webpage_banner_data($data)
	{
		$post  = $this->process_input($data);
		$query = "INSERT INTO `webpages_homepage_data`(banner_name,banner_type, image,text,banner_status,button_text,redirection_status, link) values ('" . $post['banner_name'] . "','" . $post['banner_type'] . "','" . $post['image'] . "','" . $post['text'] . "','" . $post['banner_status'] . "','" . $post['button_text'] . "','" . $post['redirection_status'] . "', '" . $post['link'] . "') ";
		return $this->db->Query($query);
	}
	/**
	 * List webpages homepage data 
	 */
	public function get_web_homepage_data()
	{
		$query = "SELECT * FROM  webpages_homepage_data ORDER BY priority";
		$query = $this->db->Query($query);
		return $this->db->FetchAllArray($query);
	}
	/**
	 * Update web home page priority
	 */
	public function update_web_home_page_priority($post)
	{

		unset($post['api_username']);
		unset($post['api_password']);

		foreach ($post as $key => $val)
			$$key = $val;

		$ids = explode(',', $id);

		foreach ($priority as $id => $val) {

			$query = "UPDATE `webpages_homepage_data` SET `priority` = '$val' WHERE id = $id ";

			$a = $this->db->Query($query);

		}

		print_r($live);

		$banner_live = array();
		if (isset($live) AND $live) {
			foreach ($live as $banner => $val) {
				$banner_live[] = $banner;
			}

			//activate which are available
			$query = "UPDATE webpages_homepage_data SET live=1 WHERE id IN (" . implode(",", $banner_live) . ")";

			$this->db->query($query);

			//deactivate which are not avail
			$query = "UPDATE webpages_homepage_data SET live=0 WHERE id NOT IN (" . implode(",", $banner_live) . ")";

			$this->db->query($query);
		} else {
			//deactivate which are not avail
			$query = "UPDATE webpages_homepage_data SET live=0 ";

			$this->db->query($query);
		}
	}
	/**
	 * List web home page banner
	 */
	public function get_web_homepage_banner($post)
	{
		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `webpages_homepage_data` WHERE id = '" . $id . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Update webpage banner data
	 */
	public function update_webpage_banner_data($data)
	{
		$post = $this->process_input($data);

		$query = "UPDATE `webpages_homepage_data` SET `banner_name` = '" . @$post['banner_name'] . "',`banner_type`='" . @$post['banner_type'] . "',`image`='" . @$post['image'] . "',
				`text`='" . @$post['text'] . "',`banner_status`='" . $post['banner_status'] . "',`button_text`='" . $post['button_text'] . "',`redirection_status`='" . $post['redirection_status'] . "', `link`='" . $post['link'] . "' where id='" . $post['id'] . "' ";

		return $this->db->Query($query);
	}
	/**
	 * Update webpage  data
	 */
	public function update_webpage_data($data)
	{
		$post = $this->process_input($data);

		foreach ($post as $key => $value) {
			$$key = $this->clean_for_sql($value);
		}
		if (!isset($status))
			$status = 0;

		$query = "UPDATE `webpages_data` SET `webpage_name` = '" . $webpage_name . "',`text`='" . $text . "',`status`='" . $status . "', orders = $orders, slug='$slug' where id='" . $id . "' ";

		return $this->db->Query($query);
	}
	/**
	 * List webpage  data
	 */
	public function get_webpages_data()
	{
		$query = "SELECT * FROM  webpages_data ORDER by orders";
		$query = $this->db->Query($query);
		return $this->db->FetchAllArray($query);
	}
	/**
	 * List webpage  data
	 */
	public function get_webpagedata_data($data)
	{
		$post  = $this->process_input($data);
		$query = "SELECT * FROM  webpages_data where id='" . $post['id'] . "' ";
		$query = $this->db->Query($query);
		return $this->db->FetchAllArray($query);
	}
	/**
	 * Delete home page banner
	 */
	public function delete_home_page_banner($data)
	{
		$query = "DELETE FROM `webpages_homepage_data` WHERE `id` = '" . $data['id'] . "';";
		return $this->db->Query($query);
	}
	/**
	 * List sandwich custom category items
	 */
	public function get_sandwich_custom_category_items()
	{
		$query = "SELECT * FROM  sandwich_item_options ORDER BY priority ASC";
		$query = $this->db->Query($query);
		return $this->db->FetchAllArray($query);
	}
	/**
	 * List individual item count
	 */
	public function get_individual_item_count()
	{
		$query = "SELECT * FROM `sandwich_categories` ";
		$query = $this->db->query($query);
		$loop  = $this->db->FetchAllArray($query);
		$data  = array();
		foreach ($loop as $loop) {
			$countq                             = "SELECT COUNT(*) AS count FROM `sandwich_category_items` WHERE category_id = '" . $loop['id'] . "'";
			$result                             = $this->db->query($countq);
			$result                             = $this->db->FetchArray($result);
			$data[$loop['category_identifier']] = $result['count'];
		}
		return $data;

	}
	/**
	 * Update Sandwich Category Items
	 */
	public function updateSandwichCategoryItems($post)
	{
		$post         = $this->process_input($post);
		$ids          = explode(',', $post['id']);
		$priority     = explode(',', $post['priority']);
		$impriority   = explode(',', $post['image_priority']);
		
		if(isset($post['sheet_priority']) && !empty($post['sheet_priority']))
		{
			$sheetpriority  = explode(',', $post['sheet_priority']);
		}
		else
		{
			$sheetpriority  = 0;
		}
		$livecheckbox = explode(',', $post['livecheckbox']);

		foreach ($ids as $key => $val) {
			$livecheckboxvalue = $livecheckbox[$key];
			if ($livecheckboxvalue == "")
				$livecheckboxvalue = 0;
			$pri   = $priority[$key];
			$imri  = $impriority[$key];
			$shpri  = $sheetpriority[$key];
			$query = "UPDATE `sandwich_category_items` SET `image_priority` = '" . $imri . "', `sheet_priority` = '" . $shpri . "', `priority` = '" . $pri . "', `live` = " . $livecheckboxvalue . "  WHERE id = '" . $val . "' ";
			$a     = $this->db->Query($query);

		}

	}
	/**
	 * Delete Sandwich Category Items
	 */
	public function deleteStandardCategory($data)
	{

		$query = "DELETE FROM `standard_category_products` WHERE `standard_category_id` = '" . $data['categoryId'] . "';";
		$this->db->Query($query);
		$query = "DELETE FROM `standard_category` WHERE `id` = '" . $data['categoryId'] . "';";
		$this->db->Query($query);
	}
	/**
	 * List all product extra
	 */
	public function getAllproductsExtras($post)
	{

		$products = $this->db->Query("SELECT `id` FROM `standard_category_products` WHERE `live` = '1' AND `active_extras` = '1' ");
		$data     = $this->db->FetchAllArray($products);

		$dt = array();

		foreach ($data as $data) {

			$id      = $data['id'];
			$data    = $this->get_standard_product_extraData(array(
					'pid' => $id
			));
			$dt[$id] = $data;

		}

		return $dt;

	}
	/**
	 * List search delivery items by store
	 */
	public function get_search_delivery_items_by_store($post)
	{

		$dt = new DateTime('now', new DateTimezone('America/New_York'));
		foreach ($post as $key => $val)
			$post[$key] = urldecode($val);


		$status_flag = $post['status_flag'];

		$cond = '';
		if (isset($post['order_status']) != '' && strlen($post['order_status'])) {
			$cond .= " AND o.order_status='" . $post['order_status'] . "' ";
		}

		if (isset($post['store_id']) && $post['store_id']) {
			$cond .= " AND o.store_id='" . $post['store_id'] . "' ";
		}


		if (isset($post['date']) && $post['date'] != '1970-01-01') {

			if ($post['date']) {
				$cond .= " AND DATE(date) ='" . $post['date'] . "' ";
                $str_to_time = date('Y-m-d', strtotime($post['date']));
			} else {
				$cond .= " AND DATE(date) = '" . date("Y-m-d") . "' ";
                $str_to_time = date('Y-m-d');
            }	
		}


		if (!isset($post['delivery'])) {
			$delivery = 0;
		} else
			$delivery = $post['delivery'];

		$result = array();

		// $select = "SELECT *,  DATE_FORMAT(date,'%a, %d %b %Y') as date, DAYNAME(date) as dayname, DATE_FORMAT(date,'%h:%i %p ') as timeformat FROM orders where delivery=$delivery $cond ORDER BY orders.order_date DESC";

		$select = "SELECT o.*,  DATE_FORMAT(date,'%a, %d %b %Y') as date, DAYNAME(date) as dayname, DATE_FORMAT(date,'%h:%i %p ') as timeformat, TIMESTAMPDIFF(MINUTE, '".$dt->format('Y-m-d H:i:s')."', o.date) as timediff, du.uid as delivery_uid, du.name as delivery_uname FROM orders as o LEFT JOIN delivery_user as du ON o.delivery_assigned_to = du.uid where o.delivery=1 AND DATE(o.date)>= '$str_to_time' ".$cond." ORDER BY o.order_date ASC";
		
		$select_result = $this->db->Query($select);
		$orders        = $this->db->FetchAllArray($select_result);

		if ($orders) {
			foreach ($orders as $key => $order) {

				if ($order['delivery']) {
					$query                  = "SELECT * FROM order_address WHERE address_id = {$order['address_id']}";
					$order['delivery_type'] = "Delivery";
				} else {
					$query                  = "SELECT * FROM pickup_stores WHERE id = {$order['address_id']}";
					$order['delivery_type'] = "Pickup";
				}



				$result           = $this->db->query($query);
				$order['address'] = "";
				if ($this->db->Rows($result)) {
					$address = $this->db->FetchAllArray($result);

					$order['address'] = $address[0];
				}

				$ordersItems = str_replace(":", ",", $order['order_item_id']);
				$query       = "SELECT * FROM orders_items WHERE order_item_id IN ($ordersItems)";
				$result      = $this->db->query($query);
				$ordersItems = $this->db->FetchAllArray($result);

				$items = array();

				foreach ($ordersItems as $item) {

				/*Getting User Details which are coming through tab, Start*/
				 	//if($order->by_tab==1){
						//$order['tab'] =  [$order['firstName'],$order['lastName'],"<br />".$order['email']]; 					 
				 	//}
				/*Getting User Details which are coming through tab, End*/
				
					if ($item['item_type'] == "user_sandwich") {
						$query    = "SELECT * FROM user_sandwich_data WHERE id = {$item['item_id']}";
						$name_col = "sandwich_name";
					} else if ($item['item_type'] == "product") {
						$query    = "SELECT * FROM standard_category_products WHERE id = {$item['item_id']}";
						$name_col = "product_name";
					}

					$result            = $this->db->query($query);
					$item_temp         = $this->db->FetchAllArray($result);
					$item_temp         = $item_temp[0];
					if ($item['item_type'] == "product")
					{
						$item_temp['modifiers'] = $item['extra_id'];
						$item_temp['spl_instructions'] = $item['spcl_instructions'];
					}
					$item_temp['qty']  = $item['item_qty'];
					$item_temp['name'] = $item_temp[$name_col];
					$items[]           = $item_temp;
				}

				$order['items'] = $items;

				$orders[$key] = $order;
			}

			return $orders;
		}  else {
			$orders = array();
		}


	}

	public function get_del_users($post)
	{
		$qry = "SELECT * FROM `delivery_user` WHERE `store_id` LIKE '%".$post['store_id']."%'";
        $run_query = $this->db->Query($qry);
        $result = $this->db->FetchAllArray($run_query);
        return $result;  
	}

	public function assign_delivery_user($post)
	{
		$current_time = date('Y-m-d H:i:s');
		$qry = "UPDATE `orders` SET `delivery_assigned_to` = '" . $post['uid'] . "',`delivery_assigned_time`= '".$current_time."' WHERE `order_id` = '". $post['order_id']."'";
        return $this->db->Query($qry);
	}

	public function remove_delivery_user($post)
	{
		$qry = "UPDATE `orders` SET `delivery_assigned_to` = 0 WHERE `order_id` = '". $post['order_id']."'";
        return $this->db->Query($qry);
	}

	public function get_this_del_users($post)
	{
		$qry = "SELECT * FROM `delivery_user` WHERE `uid` = '".$post['uid']."'";
        $run_query = $this->db->Query($qry);
        $result = $this->db->FetchAllArray($run_query);
        return $result;  
	}
	public function getAssignedOrders($post)
	{
		$qry = "SELECT *,  DATE_FORMAT(order_date,'%c/%d/%Y %h:%i %p') as order_date, DATE_FORMAT(date,'%h:%i %p ')as timeformat FROM `orders` WHERE `delivery_assigned_to` = '".$post['delivery_userID']."' AND `date` BETWEEN '".$post['search_date']." 00:00:00' AND '".$post['search_date']." 23:59:59'";
        $run_query = $this->db->Query($qry);
        $orders = $this->db->FetchAllArray($run_query);
        $orderList = array();
        if ($orders) 
        {
        	foreach ($orders as $key => $order) 
        	{

				if ($order['delivery']) 
				{
					$query= "SELECT * FROM order_address WHERE address_id = {$order['address_id']}";
					$order['delivery_type'] = "Delivery";
				} 

				$result           = $this->db->query($query);
				if ($this->db->Rows($result)) 
				{
					$address = $this->db->FetchAllArray($result);
					$orderList['name'] = $address[0]['name'];
					$orderList['company'] = $address[0]['company'];
					// $orderList['address'] = $address[0]['address1'].' '.$address[0]['address2'].' '.$address[0]['street'].' '.$address[0]['cross_streets'].' '.$address[0]['zip'];
					$orderList['address_1'] = $address[0]['address1'];
					//$orderList['address_2'] = $address[0]['address2'];
					$orderList['street'] = $address[0]['street'];
					$orderList['cross_streets'] = $address[0]['cross_streets'];
					$orderList['city'] = 'New York, NY';
					$orderList['zip'] = $address[0]['zip'];
					$orderList['phone'] = $address[0]['phone'];
					$orderList['extn'] = $address[0]['extn'];
					$orderList['delivery_instructions'] = $address[0]['delivery_instructions'];
				}

				$storeQry = "SELECT `store_name` FROM `pickup_stores` WHERE `id` = {$order['store_id']}";
				$storeName           = $this->db->query($storeQry);
				if ($this->db->Rows($storeName)) 
				{
					$address = $this->db->FetchAllArray($storeName);
					$orderList['store_name'] = $address[0]['store_name'];
				}
				$orderList['due'] = $order['timeformat'];
				$orderList['placed'] = $order['order_date'];
				$orderList['order_id'] = $order['order_id'];
				$orderList['order_status'] = $order['order_status'];
				$orderList['specific_time'] = $order['time'];
				$orderList['order_delivered'] = $order['order_delivered'];
				// if($order['order_status'] == 7)
				// {
				// 	$orderList['order_delivered'] = $order['order_delivered'];
				// }
				
				

				$orders[$key] = $orderList;
			}
        }
        return $orders;
         
	}
	public function getOrderDetails($post)
	{
		
        $post      = $this->process_input($post);

		$order_det = "SELECT user_id, order_number, `delivery`,`address_id`,`billinginfo_id`,`order_item_id`,`time`,`date`,`total`,`tip`,`delivery_fee`,`off_amount`,`manual_discount`,`sub_total`, tax FROM `orders` WHERE `order_id` = '" . $post['order_id'] . "' ";
		$this->db->Query($order_det);
		$order_det = $this->db->FetchArray();
		
		
		$address_id    = $order_det['address_id'];
		$billing_id    = $order_det['billinginfo_id'];
		$order_itm_id  = $order_det['order_item_id'];
		$order_number  = $order_det['order_number'];
		


		$query = "SELECT `first_name`,`last_name` FROM users WHERE uid = {$order_det['user_id']} ";
		$this->db->Query($query);
		$userData = $this->db->FetchArray();

		$total     = $order_det['total'];
		$tip       = $order_det['tip'];
		$delivery_fee = $order_det['delivery_fee'];
		$tax       = $order_det['tax'];
		$discount  = $order_det['off_amount'] + $order_det['manual_discount'];
		$sub_total = $order_det['sub_total'];

		
		$orderIds = explode(':', $order_itm_id);

		$order_details = array();
		$tmp_order_det = array();

		foreach ($orderIds as $ordId) {
			$ordItmDat = "SELECT * FROM `orders_items` WHERE `order_item_id` = '" . $ordId . "' ";
			$this->db->Query($ordItmDat);
			$ordItmData             = $this->db->FetchArray();
			$type                   = $ordItmData['item_type'];
			$itmId                  = $ordItmData['item_id'];
			$tmp_order_det['price'] = $ordItmData['item_price']+$ordItmData['extra_cost'];
			$tmp_order_det['qty']   = $ordItmData['item_qty'];
			$tmp_order_det['toast']   = $ordItmData['toast'];
			$tmp_order_det['extra_id']   = !empty($ordItmData['extra_id']) ? $ordItmData['extra_id'] : '';
			$tmp_order_det['spcl_instructions']   = !empty($ordItmData['spcl_instructions']) ? $ordItmData['spcl_instructions'] : '';
			$tmp_order_det['type']  = $type;
			if ($type == 'user_sandwich') {
				$itmDeatilquery = "SELECT * FROM `user_sandwich_data` WHERE `id` = '" . $itmId . "' ";
				$this->db->Query($itmDeatilquery);
				$itm_details                    = $this->db->FetchArray();
				$tmp_order_det['sandwich_name'] = $itm_details['sandwich_name'];

				$san_data = json_decode($itm_details['sandwich_data']);

				$bread = $san_data->BREAD->item_name[0];

				$prot = $san_data->PROTEIN->item_name;
				array_walk($prot, function(&$val, $key) use ($san_data)
				{
					if ($san_data->PROTEIN->item_qty->{$val})
						$val = $val . " (" . $san_data->PROTEIN->item_qty->{$val}[1] . ")";
				});

				$prot = implode(', ', $prot);

				$cheese = $san_data->CHEESE->item_name;
				array_walk($cheese, function(&$val, $key) use ($san_data)
				{
					if ($san_data->CHEESE->item_qty->{$val})
						$val = $val . " (" . $san_data->CHEESE->item_qty->{$val}[1] . ")";
				});

				$cheese = implode(', ', $cheese);

				$topping = $san_data->TOPPINGS->item_name;
				array_walk($topping, function(&$val, $key) use ($san_data)
				{
					if ($san_data->TOPPINGS->item_qty->{$val})
						$val = $val . " (" . $san_data->TOPPINGS->item_qty->{$val}[1] . ")";
				});

				$topping = implode(', ', $topping);

				$cond = $san_data->CONDIMENTS->item_name;
				array_walk($cond, function(&$val, $key) use ($san_data)
				{
					if ($san_data->CONDIMENTS->item_qty->{$val})
						$val = $val . " (" . $san_data->CONDIMENTS->item_qty->{$val}[1] . ")";
				});

				$cond = implode(', ', $cond);

				if ($cond)
					$topping = $topping . ", ";
				if ($topping || $cond)
					$cheese = $cheese . ", ";
				if ($cheese || $topping || $cond)
					$prot = $prot . ", ";

				$ingrs = $prot . $cheese . $topping . $cond;

				if ($ingrs)
					$bread = $bread . ", ";

				$ingrs                             = $bread . $ingrs;
				$tmp_order_det['sandwich_details'] = $ingrs;
				unset($tmp_order_det['product_name']);
			} else {
				$itmDeatilquery = "SELECT * FROM `standard_category_products` WHERE `id` = '" . $itmId . "' ";
				$this->db->Query($itmDeatilquery);
				$itm_details                   = $this->db->FetchArray();
				$tmp_order_det['product_name'] = $itm_details['product_name'];
				$tmp_order_det['product_description'] = $itm_details['description'];
				unset($tmp_order_det['sandwich_details']);
				unset($tmp_order_det['sandwich_name']);

			}

			$order_details['order_item_detail'][] = $tmp_order_det;

		}

		$order_details['total']                 = $total;
		$order_details['sub_total']             = $sub_total;
		$order_details['tip']                   = $tip;
		$order_details['delivery_fee']         = $delivery_fee;
		$order_details['tax']                   = $tax;
		$order_details['discount']              = $discount;
		$order_details['customer_name']			= $userData['first_name'].' '.$userData['last_name'];
		$order_details['order_number']			= $order_number;
				
		return $order_details;
	}
}
