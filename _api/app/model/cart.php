<?php
// api cart model  -- REFACTORED!!!!
class cartModel extends Model {

    public $db; //database connection object

    /**
     * invoke database connection object
     */

    public function __construct() {
        $this->db = parent::__construct();
    }

    /**
     * List pickup address
     */
    public function pickupAddresses($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql($value);
        }

        $query = "SELECT * FROM pickup_stores ";

        if (isset($enabled) && $enabled == 1)
            $query .= " WHERE live = 1";

        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
    }

    /**
     * Get pickup address
     */
    public function pickupAddress($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql($value);
        }

        $query = "SELECT * FROM pickup_stores WHERE id = $store_id";
        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
    }

    /**
     * Update cart items
     */
    public function updateCartItems($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql($value);
        }
        $count = intval($count);
        $query = "SELECT sandwich_price FROM user_sandwich_data WHERE id=$item_id";

        $result = $this->db->Query($query);
        $result = $this->db->FetchAllArray($result);
        $item_price = $result[0];
        foreach ($item_price as $key => $val)
            $$key = $val;
        $query = "UPDATE orders_items SET item_qty = $count , item_price =$sandwich_price WHERE item_id = $item_id AND  user_id = $uid AND item_type = '$data_type' AND ordered=0";
        
        $result = $this->db->Query($query);
        $query = "SELECT * FROM `orders_items` AS a LEFT JOIN `user_sandwich_data` AS b ON a.item_id=b.id  WHERE a.user_id = '$uid' AND a.ordered=0";
        $result = $this->db->Query($query);
    }

    /**
     * Insert Billing address
     */
   

    /**
     * Check cvv
     */
    public function check_cvv($post) {
        $post = $this->process_input($post);
        $cvv = $post['card_cvv'];
        $id = $post['card_id'];
        $query = "SELECT COUNT(*) AS `count` FROM order_billinginfo WHERE id = '$id' AND card_cvv = '$cvv' ";
        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
    }

    /**
     * List billing information
     */
    public function getBillinginfo($post) {
        $query = "SELECT * FROM order_billinginfo WHERE uid = '" . $post['uid'] . "' ";
        if (isset($post['id'])) {
            $query .= " AND id = '" . $post['id'] . "' ";
        } else {
            $query .= "AND display_dropdown=1 ";
        }
        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
    }

    /**
     * Insert process input
     */
    private function process_input($data) {
        array_walk($data, function(&$val, $key) {
            $val = urldecode($val);
            $val = $this->clean_for_sql($val);
        });
        return $data;
    }

    /**
     * List user order information
     */
    public function getUserOrdersInfo($post) {
        // $query = "SELECT count(order_item_id) as orders, SUM(item_price) as sales_sum FROM orders_items WHERE user_id = '" . $post['user_id'] . "'  and ordered = 1 GROUP BY user_id";

        $query = "SELECT COUNT(`orders_items`.`order_item_id`) as orders, SUM(`orders`.`total`) as sales_sum  FROM `orders_items` LEFT JOIN `orders` ON `orders_items`.`order_item_id` = `orders`.`order_item_id` WHERE `orders_items`.`user_id` =  '" . $post['user_id'] . "' AND  `orders_items`.`ordered` = 1 GROUP BY `orders_items`.`user_id`";

        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
    }

    /**
     * List cart items
     */
    public function cartItems($post) {

        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql(urldecode($value));
        }

        $date_time = date("Y-m-d h:i:s");
        //Delete Items more than life span of 3 Hours
        $query = "DELETE FROM orders_items WHERE ordered=0 AND TIMESTAMPDIFF(HOUR, date_time, '$date_time') > " . DELETE_ORDER_HOUR . " AND user_id = $user_id";
        $this->db->Query($query);

        $response = new stdClass();
        $response->sandwiches = $this->getUserCartSandwiches($user_id);

        //for products product type also needs to be added..
        $prodLists = $this->getUserCartProducts($user_id);


        foreach ($prodLists as $key => $prodList) {

            if (isset($prodList['standard_category_id']) && $prodList['standard_category_id']) {
                $prodLists[$key]['product_parent_type'] = $this->getProdutItemParentType($prodList['standard_category_id']);
            }
        }

        $response->products = $prodLists;

        return $response;
    }

    /*
     * return product parent item type (catering or standard ) 
     *   */

    public function getProdutItemParentType($id) {

        $result = $this->db->Query("SELECT  `type`  FROM  `standard_category` WHERE id = '$id' ");
        $out = $this->db->fetchArray($result);
        return $out['type'];
    }

    /**
     * Insert cart items
     */
    public function addItemToCart($post) 
    {

        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql(urldecode($value));
        }


        $total_price = $price = $this->getItemPrice($data_type, $data_id);
        //file_put_contents("query.txt", print_r($price,true));exit;

        //$price *= $qty;


        $descriptor_ids = $extra_ids = 0;
        $extra_ids_cost = 0;

        if (isset($post['descriptor_id'])) {

            $descriptor_ids = $post['descriptor_id'];
        }

        if (isset($post['extra_id'])) {
            $extra_ids = $post['extra_id'];
        }

        $spcl_instructions = '';
        if(isset($post['spcl_instructions'])){
            $spcl_instructions = preg_replace('/[^A-Za-z0-9\-]/', ' ', $post['spcl_instructions']);
        }
        $option_price = '';
        if(isset($_POST['clickedItems']))
        {            
            $option_price = $_POST['clickedItems'];
        }

        $opt=explode('%2C', $option_price); 

        $option=array();
        $price=array();
        foreach($opt as $key=>$val){
            if($key % 2 == 0){
                $option[]=$val;
            }else{
                $price[]=$val;
            }
        }
        
        $opt_fin=implode(', ', preg_replace('/[^A-Za-z0-9\-]/', ' ', $option));
        // file_put_contents("query.txt", print_r($opt_fin,true));exit;
        $prc_fin=array_sum($price);
        
        if ($extra_ids) {
            $query = "select SUM(cost) as cost FROM product_extras WHERE id IN ($extra_ids)";
            $result = $this->db->Query($query);
            $result = $this->db->FetchObjectRow($result);
            $extra_ids_cost = $result->cost;
        }

        $select_query = "SELECT * from orders_items WHERE user_id='" . $user_id . "' AND item_id='" . $data_id . "' AND item_type = '" . $data_type . "' AND ordered = '0'  ";
        $result_query = $this->db->Query($select_query); 
        $vals = $this->db->FetchAllArray($result_query);
        
        //file_put_contents("query.txt", print_r($vals,true), FILE_APPEND);exit;
        if ($vals) 
        {
            $checkupdate = 0;
            foreach ($vals as $val) 
            {
                if (isset($val['item_id'])) 
                {
                    $item_id = $val['item_id'];
                }

                $type = $val['item_type'];
                
                if(($type == "product") && ($data_id == $item_id) && ($opt_fin == $val['extra_id']) && ($toast == $val['toast']) )
                {
                    if ( $spcl_instructions != $val['spcl_instructions'] || $opt_fin != $val['extra_id']) 
                    {
                        $date = date("Y-m-d h:i:s");
                        $query = "INSERT INTO orders_items (`item_id`,`item_type`,`user_id` ,`item_qty` ,`item_price` ,`item_tax` , `toast`, descriptor_id, extra_id, extra_cost,spcl_instructions, date_time ) VALUES ('" . $data_id . "','" . $data_type . "','" . $user_id . "','" . $qty . "','" . $total_price . "','0','" . $toast . "', '$descriptor_ids', '".$opt_fin."', '".$prc_fin."','".$spcl_instructions."', '$date') ";
                        $result = $this->db->Query($query);
                        $order_id = $this->db->insertId();
                        $checkupdate = 1;
                    }
                    else
                    {
                        $query = "UPDATE orders_items SET item_qty = item_qty + $qty WHERE user_id='" . $user_id . "' AND item_id='" . $data_id . "' AND item_type = '" . $data_type . "' AND extra_id='$opt_fin' AND ordered = '0'";
                        //file_put_contents("query.txt", print_r($query,true));exit;
                        $this->db->Query($query);
                        $order_id    = $val['order_item_id'];
                        $checkupdate = 1;
                    }
                }
                else
                {
                    if ($data_id == $item_id && $opt_fin == $val['extra_id'] && $toast == $val['toast']) 
                    {
                        $query = "UPDATE orders_items SET item_qty = item_qty + $qty WHERE user_id='" . $user_id . "' AND item_id='" . $data_id . "' AND item_type = '" . $data_type . "' AND extra_id='$opt_fin' AND ordered = '0'";
                        $this->db->Query($query);
                        $order_id    = $val['order_item_id'];
                        $checkupdate = 1;
                    }
                }

            }
            if ($checkupdate == 0) 
            {
                $date = date("Y-m-d h:i:s");
                $query = "INSERT INTO orders_items (`item_id`,`item_type`,`user_id` ,`item_qty` ,`item_price` ,`item_tax` , `toast`, descriptor_id, extra_id, extra_cost,spcl_instructions, date_time ) VALUES ('" . $data_id . "','" . $data_type . "','" . $user_id . "','" . $qty . "','" . $total_price . "','0','" . $toast . "', '$descriptor_ids', '".$opt_fin."', '".$prc_fin."','".$spcl_instructions."', '$date') ";
                $result = $this->db->Query($query);
                $order_id = $this->db->insertId();
            }
        } 
        else
        {
            $date = date("Y-m-d h:i:s");
            $query = "INSERT INTO orders_items (`item_id`,`item_type`,`user_id` ,`item_qty` ,`item_price` ,`item_tax` , `toast`, descriptor_id, extra_id, extra_cost,spcl_instructions, date_time ) VALUES ('" . $data_id . "','" . $data_type . "','" . $user_id . "','" . $qty . "','" . $total_price . "','0','" . $toast . "', '$descriptor_ids', '".$opt_fin."', '".$prc_fin."','".$spcl_instructions."', '$date') ";

            $result = $this->db->Query($query);
            $order_id = $this->db->insertId();
        }

        $query = "SELECT SUM(item_qty) as count FROM `orders_items`  WHERE user_id = '" . $user_id . "' AND ordered=0";

        $result = $this->db->Query($query);

        $response = $this->cartItems($post);

        return $response;
    }

    /**
     * Update cart items
     */
    public function updateItemToCart($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql(urldecode($value));
        }

        $price = $this->getItemPrice($data_type, $data_id);


        $query = "UPDATE orders_items SET item_qty = $qty , item_price =$price WHERE item_id = $data_id AND  user_id = $user_id AND item_type = '$data_type' AND order_item_id = $order_item_id AND ordered=0";
        $result = $this->db->Query($query);

        $response = $this->cartItems($post);

        return $response;
    }

    /**
     * Remove cart items
     */
    public function removeCartItem($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql(urldecode($value));
        }

        $query = "DELETE FROM orders_items WHERE order_item_id = $order_item_id";
        $result = $this->db->Query($query);

        $response = $this->cartItems($post);

        return $response;
    }

    /**
     * List item price
     */
    public function getItemPrice($data_type, $item_id) {

        if ($data_type == "product") {
            $table = "standard_category_products";
        } else {
            $table = "user_sandwich_data";
        }

        $query = "SELECT * FROM $table WHERE id=$item_id";

        $result = $this->db->Query($query);
        $result = $this->db->FetchAllArray($result);
        $item_price = $result[0];
        foreach ($item_price as $key => $val)
            $$key = $val;

        if ($data_type == "product") {
            return $product_price;
        } else {
            return $sandwich_price;
        }
    }

    /**
     * List cart sandwiches
     */
    public function getUserCartSandwiches($uid) {

        $query = "SELECT * FROM `orders_items` AS a LEFT JOIN `user_sandwich_data` AS b ON a.item_id=b.id  WHERE a.user_id = '$uid' AND a.ordered=0 AND a.item_type = 'user_sandwich'";

        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
    }

    /**
     * List user cart product
     */
    public function getUserCartProducts($uid) {
        $query = "SELECT o.*, p.*, d.desc as desc_name, e.name as extra_name FROM orders_items as o LEFT JOIN standard_category_products as p ON o.item_id =  p.id LEFT JOIN product_descriptor as d ON o.descriptor_id =  d.id LEFT JOIN product_extras as e ON o.extra_id =  e.id  WHERE o.item_type = 'product' AND o.ordered=0 AND o.user_id = $uid";

        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
    }

    /**
     * List discount details
     */
    public function geDiscountDetails($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql(urldecode($value));
        }

        $query = "SELECT * FROM discounts WHERE code='$code' AND is_live = 1 AND expiration > NOW()";
        $result = $this->db->Query($query);
        $result = $this->db->FetchArrayAssoc($result);

        //If not alive
        if ($result['is_live'] == 0) {
            return array("status" => "error", "message" => "not alive");
        }

        if ($user_id && $result['one_per_user']) {

            $query = "SELECT order_id FROM orders WHERE coupon_code = '{$result['id']}' AND user_id = $user_id";
            $row = $this->db->Query($query);
            $row = $this->db->FetchAllArray($row);
            if (count($row)) {

                return array("status" => "error", "message" => "single use per user");
            }
        }

        //If uses is limited and usage count is 0 return false
        if ($result['uses_type'] == 0 && $result['uses_amount'] == 0) {
            return array();
        }

        return $result;
    }

    /**
     * List order list
     */
    public function getOrdersList($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql(urldecode($value));
        }

        $append = "";
        if (isset($search))
            $search = trim($search);
        //$append = " WHERE u.first_name LIKE '%$search%' OR u.last_name LIKE '%$search%' OR u.phone LIKE '%$search%' OR o.order_id = '%$search%'  ";
        $append = " WHERE u.first_name LIKE '%$search%' OR u.last_name LIKE '%$search%' OR  o.order_number LIKE '%$search%'  ";
        $query = "SELECT o.*, u.* FROM orders as o LEFT JOIN users as u ON o.user_id = u.uid $append";

        $result = $this->db->Query($query);
        return $this->db->FetchAllArray($result);
    }

    /**
     * Fetch time on store selection
     */
    public function getAllCloseTimes($post) {

        $storeId = $post['storeID'];
        $results = $this->db->Query("SELECT `close`, `day` FROM  `store_timings` WHERE  `store_id` = '$storeId' ");
        $datas = $this->db->FetchAllArray($results);
        $out = array();
        foreach ($datas as $data) {
            $out[$data['day']] = $data['close'];
        }

        return $out;
    }

    public function getTimeOnStoreSelection($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql($value);
        }

        if ($isCurrentDay) {

            $query = "SELECT open,close FROM store_timings WHERE (('$currentTime' >= HOUR(`open`) OR  '$currentTime' <= HOUR(`open`)) AND '$currentTime' <= HOUR(`close`) ) AND store_id= '" . $post['storeID'] . "' AND day = '" . $post['selectedDay'] . "' AND store_active = '1' ";
            
        } else
            $query = "SELECT open,close FROM store_timings WHERE store_id= '" . $post['storeID'] . "' AND day = '" . $post['selectedDay'] . "' AND store_active = '1' ";


        if ($post['isDelivery'] == 1) {
            $query .= " AND delivery_active != '1' ";
        } 


        $result = $this->db->Query($query);
        return $this->db->FetchArray($result);
    }

    /**
     * List cart item quantity count
     */
    public function getCartItemQtyCount($post) {
        foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql($value);
        }

        $query = "SELECT SUM(item_qty) as count FROM `orders_items`  WHERE user_id = '" . $user_id . "' AND ordered=0";
        $result = $this->db->Query($query);
        return $this->db->FetchObjectRow($result);
    }
	
	/**
     * make sure the user's items haven't been purged from the cart due to inactivity
     */
	public function checkIfEmpty($post) {
		foreach ($post as $key => $value) {
            $$key = $this->clean_for_sql($value);
        }
		
		$query = "SELECT count(*) as count FROM `orders_items`  WHERE user_id = '" . $user_id . "' AND ordered=0";
		$result = $this->db->Query($query);
		return $this->db->FetchObjectRow($result);
	}

    /**
     * List current store time slot
     * $post - Includes 
     *     deliveryOrPickup
     *     address_id as id
     *     day
     *     
     */
    public function getCurrentStoreTimeSlot($post) {
        if( isset( $post['id'] ) )
            $id = $this->clean_for_sql( $post['id'] );
        else
            $id = null;
      
        $day = '';
        if( !isset($post['day'] ) || !$post['day'] )
        {
            
            $dt = new DateTime('now', new DateTimezone('America/New_York'));
            $today_is         = $dt->format("l");
            $day = $today_is;
        }
        else
        {
             $day = $this->clean_for_sql( $post['day'] ); 
        }

        if( isset( $post['deliveryOrPickup'] ) )
            $deliveryOrPickup = $this->clean_for_sql( $post['deliveryOrPickup'] );
        else
             $deliveryOrPickup = 'delivery';        // default to deliv.
        


        $order_address = array(  );
        
        if ($deliveryOrPickup == "delivery") 
        {
            
            if(  !$id )     // no address, i guess use all available ranges..
            {
                $order_address['zip'] = '10001';
            }
            else
            {
                $query = "SELECT * FROM order_address WHERE address_id = $id";
                $result = $this->db->Query($query);
                $order_address = $this->db->FetchArray($result);
            }

             if( !isset( $order_address['zip'] ) )
                $order_address['zip']  = '10001';   // default to midtown.  

            $query = "SELECT * FROM pickup_stores as p JOIN store_zipcodes as z ON z.store_id = p.id WHERE z.zipcode = {$order_address['zip']}";
            $result = $this->db->Query($query);
            $pickup_store = $this->db->FetchArray($result);
        }
        else {
            $pickup_store['store_id'] = $id;
        }

        $query = "SELECT * FROM store_timings as s JOIN pickup_stores as p on s.store_id = p.id WHERE s.store_id = '" . $pickup_store['store_id'] . "' AND s.day = '" . $day . "'";

        $result = $this->db->Query($query);
        $store_timing = $this->db->FetchObjectRow($result);
        return $store_timing;
    }

    public function getspecificday($post) {
        $store_id = $post['storeId'];
        $query = "SELECT * FROM pickup_stores WHERE id = $store_id";
        $result = $this->db->Query($query);
        return $this->db->FetchArray($result);
    }

    public function checkZip($post) {
        $zip = $post['zip'];
        $query = "SELECT * FROM store_zipcodes WHERE zipcode = $zip";
        $result = $this->db->Query($query);
        $results = $this->db->FetchArray($result);
        //return $results['min_order'];
        return $results;
    }

    public function getAllZips(  )
    {
       $query =  "SELECT zipcode, store_id, min_order FROM store_zipcodes";
        $result = $this->db->Query($query);
        $results = $this->db->FetchAllArray($result);
        if( !$results )
            return false;
        return $results;
    }

    public function get_storeClosed_days( $post )
    {
       $query = "SELECT count_order FROM store_timings WHERE `store_id` = '".$post['store_id']."' AND `store_active` = '0'";
       
        $result = $this->db->Query($query);
        $results = $this->db->FetchAllArray($result);
        if( !$results )
        {
            return false;
        }
        return array_column($results, 'count_order');
    }

    public function getCardZIp($post) {
        $cardId = $post['cardId'];
        $query = "SELECT `card_zip`,`card_number` FROM `order_billinginfo` WHERE `id` = $cardId";
        $result = $this->db->Query($query);
        $results = $this->db->FetchArray($result);
        //return $results['min_order'];
        return $results;
    }

}
