<?php
class customerModel extends Model
{

	public $db; //database connection object
	/**
	 * invoke database connection object
	 */
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * List customer data
	 */
	public function get_customer_data()
	{

		$query  = "SELECT u.*, count(sw.id) as count, SUM(sw.sandwich_price) as total FROM users as u LEFT JOIN user_sandwich_data as sw ON u.uid = sw.uid GROUP BY sw.uid";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);

	}
	/**
	 * List customer details
	 */
	public function get_customers_details($post)
	{

		$post   = $this->process_input($post);
		$id     = $post['id'];
		$query  = "SELECT * FROM `users` WHERE uid = '" . $id . "' ";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * List user filter
	 */
	public function users_filter()
	{
		$query  = "SELECT u.*, count(sw.id) as count, SUM(sw.sandwich_price) as total FROM users as u LEFT JOIN user_sandwich_data as sw ON u.uid = sw.uid GROUP BY sw.uid";
		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Insert customer information
	 */
	public function savingCustomerInfo($post)
	{

		unset($post['api_username']);
		unset($post['api_password']);

		$update_items = array();

		foreach ($post as $key => $val) {
			$$key = $this->clean_for_sql(urldecode($val));

			if ($key == "password" && $password) {
				if (strlen($password >= 6)) {
					$password       = md5(urldecode($val));
					$update_items[] = " $key = '" . $password . "'";
				}
			} else if ($key == "username" && $username) {
				$update_items[] = " $key = '" . $this->clean_for_sql(urldecode($val)) . "'";
			} else {
				if ($key && $val)
					$update_items[] = " $key = '" . $this->clean_for_sql(urldecode($val)) . "'";
			}
		}


		$query = "UPDATE users SET " . implode(",", $update_items) . " WHERE uid = $uid";

		return $this->db->Query($query);
	}
	/**
	 * List customer information
	 */
	public function getCustomerNotes($post)
	{

		unset($post['api_username']);
		unset($post['api_password']);

		foreach ($post as $key => $val) {
			$$key = urldecode($val);
		}
		$query = "SELECT *, DATE_FORMAT(date,'%m/%d/%y') as date FROM customer_notes WHERE user_id = $user_id ORDER BY date DESC";

		$result = $this->db->Query($query);
		return $this->db->FetchAllArray($result);
	}
	/**
	 * Add customer informations
	 */
	public function addCustomerNote($post)
	{

		unset($post['api_username']);
		unset($post['api_password']);

		foreach ($post as $key => $val) {
			$$key = $this->clean_for_sql(urldecode($val));
		}
		$query = "INSERT INTO customer_notes (user_id, date, notes) VALUES ($user_id, NOW(), '$notes') ";

		return $this->db->Query($query);

	}
	/**
	 * Remove customer notes
	 */
	public function removeCustomerNote($post)
	{

		unset($post['api_username']);
		unset($post['api_password']);

		foreach ($post as $key => $val) {
			$$key = urldecode($val);
		}
		$query = "DELETE FROM customer_notes WHERE id = $id ";

		return $this->db->Query($query);

	}
	/**
	 * Check user name exist
	 */
	public function userNameExist($post)
	{
		foreach ($post as $key => $val) {
			$$key = urldecode($val);
		}

		$query    = "SELECT uid FROM users WHERE username = '$username' and uid != $uid";
		$result   = $this->db->Query($query);
		$response = new stdClass;
		if ($this->db->Rows($result)) {
			$response->status = "true";
			$response->msg    = "Username/Email already exist";
		} else {
			$response->status = "false";
		}

		return $response;
	}
	/**
	 * List user reviews
	 */
	public function get_user_reviews($post)
	{
		$post     = $this->process_input($post);
		$query    = "SELECT * FROM user_reviews WHERE uid = '" . $post['uid'] . "' ";
		$result   = $this->db->Query($query);
		$response = $this->db->FetchAllArray($result);
		return $response;
	}
	/**
	 * List all review
	 */
	public function get_all_review_contents()
	{
		$query    = "SELECT * FROM user_review_content ";
		$result   = $this->db->Query($query);
		$response = $this->db->FetchAllArray($result);
		return $response;
	}
	/**
	 * Update customer information
	 */
	public function update_customer($post)
	{
		$post = $this->process_input($post);

		switch ($post['type']) {

			case 'name':
				$query = "UPDATE users SET first_name = '" . $post['fname'] . "' , last_name = '" . $post['lname'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;

			case 'email':
				$query = "UPDATE users SET username = '" . $post['email'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;

			case 'password':
				if ($post['password'])
					$query = "UPDATE users SET password = '" . md5($post['password']) . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;

			case 'phone':
				$query = "UPDATE users SET phone = '" . $post['phone'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;

			case 'company':
				$query = "UPDATE users SET company = '" . $post['company'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;

			case 'hear_about':
				$query = "UPDATE user_reviews SET how_know = '" . $post['nameinp'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;


			case 'ordering_exprience':
				$query = "UPDATE user_reviews SET rating_order = '" . $post['nameinp'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;


			case 'customer_service':
				$query = "UPDATE user_reviews SET rating_customer_service = '" . $post['nameinp'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;


			case 'quality_food':
				$query = "UPDATE user_reviews SET rating_food = '" . $post['nameinp'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;

			case 'message':
				$query = "UPDATE user_reviews SET message = '" . $post['nameinp'] . "'  WHERE uid = '" . $post['uid'] . "'  ";
				break;

		}

		if ($query)
			return $result = $this->db->Query($query);


	}
	/**
	 * Process input
	 */
	private function process_input($data)
	{
		array_walk($data, function(&$val, $key)
		{
			$val = urldecode($val);
			$val = $this->clean_for_sql($val);
		});
		return $data;
	}
	/**
	 * List customer data
	 */
	public function getCustomerData($post)
	{
		foreach ($post as $key => $val) {
			$$key = urldecode($val);
		}
		$stype = '';
		$sval  = '';
		if (isset($search))
			$stype = $search;
		if (isset($searchval))
			$sval = $searchval;

		
		$query = "SELECT * FROM users as u ";

		$append = "WHERE";
		if (isset($name) AND $name != "") {
			$append .= " u.first_name LIKE '%$name%'";
		}
		if (isset($lastname) AND $lastname != "") {
			if ($append == "WHERE") {
				$append .= " u.last_name LIKE '%$lastname%' ";
			} else {
				$append .= " AND u.last_name LIKE '%$lastname%' ";
			}
		}

		if (isset($email) AND $email != "") {

			if ($append == "WHERE") {
				$append .= " u.username LIKE '%$email%' ";
			} else {
				$append .= " AND u.username LIKE '%$email%' ";
			}
		}

		$query .= $append . "ORDER BY u.uid DESC";



		$result = $this->db->Query($query);
		$users  = $this->db->FetchAllArray($result);


		foreach ($users as $key => $user) {
			
			$query = "SELECT count(order_item_id) as sandwich_count, SUM(item_price) as total_price FROM orders_items WHERE user_id = {$user['uid']}  and ordered = 1 GROUP BY user_id";

			$result = $this->db->Query($query);
			$result = $this->db->FetchAllArray($result);

			if (isset($result[0])) {
				$result                 = $result[0];
				$user['sandwich_count'] = $result['sandwich_count'];
				$user['total_price']    = $result['total_price'];
			} else {

				$user['sandwich_count'] = 0;
				$user['total_price']    = 0;
			}

			$users[$key] = $user;
		}

		return $users;

	}
	/**
	 * Delete all orders
	 */
	public function delete_all_orders($post)
	{
		if (isset($post['user_id'])){
			$where = " AND user_id = {$post['user_id']}";
		
			$query = "DELETE FROM orders_items WHERE ordered = '0' $where";
			return $this->db->Query($query);
		}
	}
	
	public function removeCard($post){
		$id = $post['card_id'];
		
		if(isset($id)){
			$res = $this->db->Query("SELECT COUNT(*) AS count FROM `orders` WHERE `billinginfo_id` = '$id'");
			$out = $this->db->FetchArray($res);
			if($out['count'] > 1){ return 'used_in_oredr';   } 
			$query   = "DELETE FROM `order_billinginfo` WHERE `id` = '$id' ";
			return $this->db->Query($query);
		}
		
	}
}