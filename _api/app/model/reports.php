<?php
class ReportsModel extends Model
{

	public $db; //database connection object
	/**
	* invoke database connection object
	*/
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * List order
	 */
	public function getOrders($post)
	{

		foreach ($post as $key => $value) {
			if (is_array($value)) {
				$$key = $value;
			} else {
				$$key = $this->clean_for_sql(urldecode($value));
			}
		}

		$where = "";
		if (isset($pickup_store)) {
			$where = " WHERE o.store_id = $pickup_store ";
		} else {
			$where = " WHERE o.store_id > 0 ";
		}

		if (isset($order_type)) {
			if ($where) {
				$where .= " AND o.delivery IN (" . implode(",", $order_type) . ") ";
			} else {
				$where = " WHERE o.delivery IN (" . implode(",", $order_type) . ") ";
			}
		}

		if (isset($order_status)) {
			if ($where) {
				$where .= " AND o.order_status IN (" . implode(",", $order_status) . ") ";
			} else {
				$where = " WHERE o.order_status IN (" . implode(",", $order_status) . ") ";
			}
		}

		if (isset($type_purchase)) {
			if ($where) {
				$where .= " AND o.by_admin IN (" . implode(",", $type_purchase) . ") ";
			} else {
				$where = " WHERE o.by_admin IN (" . implode(",", $type_purchase) . ") ";
			}
		}

		if (isset($discount)) {
			if ($where)
				$where .= " AND o.coupon_code = $discount ";
			else
				$where = " WHERE o.coupon_code = $discount ";
		}

		if (isset($order_date_filter)) {

			$date_col = $order_date_filter;

		} else {
			$date_col = "order_date";
		}

		if( !$from_date  )
				$from_date  = date("Y-m-d", strtotime($from_date));


		if (isset($from_date)) {
			//print $from_date;
			//exit(  );
			$from_date = date("Y-m-d", strtotime($from_date));
			
			if (isset($from_time))
				$from_date .= " " . $from_time . ":00";
			else
				$from_date .= " 00:00:00";

			if ($where)
				$where .= " AND o.$date_col >= '$from_date' ";
			else
				$where = " WHERE o.$date_col >= '$from_date' ";
		}


		if (isset($to_date)) {

			$to_date = date("Y-m-d", strtotime($to_date));
			if (isset($to_time))
				$to_date .= " " . $to_time . ":00";
			else
				$to_date .= " 00:00:00";

			if ($where)
				$where .= " AND o.$date_col <= '$to_date' ";
			else
				$where = " WHERE o.$date_col <= '$to_date' ";
		}


		if (!isset($from_date) AND !isset($to_date)) {

			$from_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-1 month"));

			$from_date .= " 00:00:00";

			$where .= " AND $date_col >= '$from_date' ";

			$to_date = date("Y-m-d");
			$to_date .= " 23:59:00";

			$where .= " AND $date_col < '$to_date' ";
		}


		if (isset($search_text)) {
			if ($where)
				$where .= " AND u.first_name LIKE '%$search_text%' OR u.last_name LIKE '%$search_text%' ";
			else
				$where = " WHERE u.first_name LIKE '%$search_text%' OR u.last_name LIKE '%$search_text%' ";
		}

		$query = "
		SELECT o.*, DATE_FORMAT(o.date,'%b %d %Y %h:%i %p') as f_date,DATE_FORMAT(o.order_date,'%b %d %Y %h:%i %p') as o_date,
		u.first_name, u.last_name, (o.sub_total + o.tip) as calc_total
		FROM orders as o
		JOIN users as u
		ON o.user_id = u.uid
		$where
		ORDER BY o.order_date DESC
		";

		$result = $this->db->Query($query);
		$orders = $this->db->FetchAllArray($result);
		if ($orders) {
			foreach ($orders as $key => $order) {
				$order             = (Object) $order;
				$order->item_total = $this->getOrderitemsPrice($order->order_item_id);
				//$order->total_tax  = $this->getTaxAmount($order->order_item_id);
				/* $order->total_tax  = $order->sub_total * TAX; */
				$order->total_tax  = $order->sub_total*0.08875;
				$order->amount_wt  = $order->item_total - $order->total_tax;
				
				//$order->total_tax  = $order->tax;
				//$order->amount_wt  = $order->item_total - $order->total_tax;
				$orders[$key]      = $order;
			}
			return $orders;
		} else
			return array();

	}
	/**
	 * List order item price
	 */
	public function getOrderitemsPrice($items)
	{
		$items  = str_replace(":", ",", $items);
		$query  = "SELECT SUM(`item_price`*`item_qty`) as item_total FROM `orders_items` WHERE `order_item_id` IN ($items)";
		$result = $this->db->Query($query);
		$rows   = $this->db->FetchAllArray($result);
		return $row = $rows[0]['item_total'];

	}
	/**
	 * List tax amount
	 */
	public function getTaxAmount($items)
	{
		$items = str_replace(":", ",", $items);

		//For Standard products
		$query    = "SELECT * FROM `standard_category_products` WHERE taxable  = 1 AND id IN (SELECT item_id FROM `orders_items` WHERE `order_item_id` IN ($items) AND item_type= 'product')";
		$result   = $this->db->Query($query);
		$products = $this->db->FetchAllArray($result);

		$total_tax = 0.00;

		foreach ($products as $product) {
			if ($product['taxable']) {
				$tax = $product['product_price'] * 0.08875; 
				$total_tax += $tax;
			}
		}

		//get sandwich categories
		$query      = "SELECT * FROM `sandwich_categories`";
		$result     = $this->db->Query($query);
		$categories = $this->db->FetchAllArray($result);
		
		

		//For Sandwiches
		$query      = "SELECT * FROM `user_sandwich_data` WHERE id IN (SELECT item_id FROM `orders_items` WHERE `order_item_id` IN ($items) AND item_type= 'user_sandwich')";
		$result     = $this->db->Query($query);
		$sandwiches = $this->db->FetchAllArray($result);

		//$total_tax = 0.00;

		foreach ($sandwiches as $sandwich) {
			$tax = $this->getSandwichTaxAmount($sandwich);
			$total_tax += $tax;
		}

		return $total_tax;

	}
	/**
	 * List Sandwich TaxAmount
	 */
	public function getSandwichTaxAmount($sandwich)
	{

		$total_tax = 0;

		$sandwich_data = json_decode($sandwich['sandwich_data']);

		foreach ($sandwich_data as $key => $data) {

			foreach ($data->item_id as $kkey => $ddata) {
				//get sandwich categories items
				$query  = "SELECT * FROM `sandwich_category_items` WHERE id = $ddata";
				$result = $this->db->Query($query);
				$item   = $this->db->FetchArray($result);

				if ($item['taxable']) {

					$tax = $item['item_price'] * 0.08875; 
					$total_tax += $tax;

				}
			}
		}

		return $total_tax;

	}
	/**
	 * List Discounts
	 */
	public function getDiscountsList($post)
	{
		$query  = "SELECT * FROM discounts";
		$result = $this->db->Query($query);
		$rows   = $this->db->FetchAllArray($result);
		if (count($rows))
			return $rows;
		else
			return array();
	}
	/**
	 * List System Users
	 */
	public function getSystemUsers($post)
	{
		$query  = "SELECT * FROM admin_user";
		$result = $this->db->Query($query);
		return $rows = $this->db->FetchAllArray($result);
	}
	/**
	 * List / report total sales
	 */
	public function doTotalSalesFilterReports($post)
	{
		foreach ($post as $key => $value) {
			if (is_array($value)) {
				$$key = $value;
			} else {
				$$key = $this->clean_for_sql(urldecode($value));
			}
		}

		$where = "";
		if ($pickup_store) {
			$where = " WHERE o.store_id = $pickup_store ";
		} else {
			$where = " WHERE o.store_id > 0 ";
		}

		if (isset($order_type)) {
			if ($where) {
				$where .= " AND o.delivery IN (" . implode(",", $order_type) . ") ";
			} else {
				$where = " WHERE o.delivery IN (" . implode(",", $order_type) . ") ";
			}
		}
		
		$status_value = "";
		if (isset($order_status)) {
			
			if (isset($order_type)) {
				
				if(count($order_type) == 1){
					$status = $order_status;
										
					if($order_type[0] == "true"){//if True means delivery
						foreach($status as $key => $s){
							$s = urldecode($s);
						//	print_r($s);
							$s = explode(",", $s);
							$status_value .= $s[0];							
							if($key < count($status)-1 && count($status) > 1){
								$status_value .= ",";	
							}	
						}
					}else{
						foreach($status as $key => $s){
							$s = urldecode($s);														
							print_r($s);
							$s = explode(",", $s);
							$status_value .= $s[1];							
							if($key < count($status)-1 && count($status) > 1){
								$status_value .= ",";	
							}	
						}
					}
				}	
			}else{
				$status_value = urldecode(implode(",", $order_status));
			}
						
			if ($where) {
				$where .= " AND o.order_status IN (" . $status_value . ") ";
			} else {
				$where = " WHERE o.order_status IN (" . $status_value . ") ";
			}
		}

		if (isset($type_purchase)) {
			if ($where) {
				$where .= " AND o.by_admin IN (" . implode(",", $type_purchase) . ") ";
			} else {
				$where = " WHERE o.by_admin IN (" . implode(",", $type_purchase) . ") ";
			}
		}

		if ($discount) {
			if ($where)
				$where .= " AND o.coupon_code = $discount ";
			else
				$where = " WHERE o.coupon_code = $discount ";
		}

		if (isset($order_date_filter)) {

			$date_col = $order_date_filter;

		} else {
			$date_col = "order_date";
		}

		if( !$from_date  )
				$from_date  = date("Y-m-d", strtotime($from_date));

		
		if ($from_date) {
			$from_date = date("Y-m-d", strtotime($from_date));
			if ($from_time)
				$from_date .= " " . $from_time . ":00";
			else
				$from_date .= " 00:00:00";
				
			if ($where)
				$where .= " AND o.$date_col >= '$from_date' ";
			else
				$where = " WHERE o.$date_col >= '$from_date' ";
		}


		if ($to_date) {

			$to_date = date("Y-m-d", strtotime($to_date));
			if ($to_time)
				$to_date .= " " . $to_time . ":00";
			else
				$to_date .= " 23:59:00";	

			if ($where)
				$where .= " AND o.$date_col <= '$to_date' ";
			else
				$where = " WHERE o.$date_col <= '$to_date' ";
		}


		if (!isset($from_date) AND !isset($to_date)) {

			$from_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-1 month"));

			$from_date .= " 00:00:00";

			$where .= " AND $date_col >= '$from_date' ";

			$to_date = date("Y-m-d");
			$to_date .= " 23:59:00";

			$where .= " AND $date_col < '$to_date' ";
		}

		if ($system_user) {
			if ($where)
				$where .= " AND o.user_id = $system_user AND by_admin = 1 ";
			else
				$where = " WHERE o.user_id = $system_user AND by_admin = 1 ";
		}
 
		$search_text = trim($search_text);

		if ($search_text) {
			if ($where)
				$where .= " AND u.first_name LIKE '%$search_text%' OR u.last_name LIKE '%$search_text%' OR o.order_number LIKE '%$search_text%' ";
			else
				$where = " WHERE u.first_name LIKE '%$search_text%' OR u.last_name LIKE '%$search_text%' OR o.order_number LIKE '%$search_text%' ";
		}

		$query = "
		SELECT o.*, DATE_FORMAT(o.date,'%b %d %Y %h:%i %p') as f_date,DATE_FORMAT(o.order_date,'%b %d %Y %h:%i %p') as o_date,
		u.first_name, u.last_name, (o.sub_total + o.tip) as calc_total
		FROM orders as o
		JOIN users as u
		ON o.user_id = u.uid
		$where
		ORDER BY o.order_date DESC
		";
		
		$result = $this->db->Query($query);
		$orders = $this->db->FetchAllArray($result);
		if ($orders) {
			foreach ($orders as $key => $order) {
				$order             = (Object) $order;
				$order->item_total = $this->getOrderitemsPrice($order->order_item_id);
				//$order->total_tax  = $this->getTaxAmount($order->order_item_id);
				$order->total_tax  = $order->sub_total*0.08875;
				$order->amount_wt  = $order->item_total - $order->total_tax;
				$orders[$key]      = $order;
			}
			return $orders;
		} else
			return array();
	}
	/**
	 * List / report item sales
	 */
	public function getItemSalesReports()
	{

		$itemSalesReport                      = new stdClass();
		$itemSalesReport->sandwich_categories = $this->getSandwichCategoriesReport();
		$itemSalesReport->standard_items      = array();
		
	}
	/**
	 * List / report Sandwich Categories
	 */
	public function getSandwichCategoriesReport()
	{

		$query         = "SELECT * FROM `sandwich_categories`";
		$result        = $this->db->Query($query);
		$sandwich_cats = $this->db->FetchAllArray($result);

		foreach ($sandwich_cats as $key => $cat) {
			$query          = "SELECT * FROM sandwich_category_items WHERE category_id = '{$cat['id']}' ";
			$result         = $this->db->Query($query);
			$sandwich_items = $this->db->FetchAllArray($result);
			foreach ($sandwich_items as $key => $item) {

			}
		}

		return $sandwich_cats;
	}
	/**
	 * List  Sandwich items
	 */
	public function getSandwichItems()
	{

		$query          = "SELECT * FROM `sandwich_category_items`";
		$result         = $this->db->Query($query);
		$sandwich_items = $this->db->FetchAllArray($result);
		return $sandwich_items;
	}
	/**
	 * List Standared Items
	 */
	public function getStandaredItems()
	{

		$query           = "SELECT * FROM `standard_category_products`";
		$result          = $this->db->Query($query);
		$standared_items = $this->db->FetchAllArray($result);
		return $standared_items;
	}
	/**
	 * List Standard Category
	 */
	public function getStandardCategory()
	{

		$query           = "SELECT * FROM `standard_category` WHERE type='standard'";
		$result          = $this->db->Query($query);
		$standared_items = $this->db->FetchAllArray($result);
		return $standared_items;
	}
	
	/**
	 * List catering categories.
	 */
	public function getCateringCategory()
	{
	
		$query           = "SELECT * FROM `standard_category` WHERE type='catering'";
		$result          = $this->db->Query($query);
		$standared_items = $this->db->FetchAllArray($result);
		return $standared_items;
	}
	
	/**
	 * List Standared Items Qty
	 */
	public function getStandaredItemsQty($post)
	{
		foreach ($post as $key => $value) {
			if (is_array($value)) {
				$$key = $value;
			} else {
				$$key = $this->clean_for_sql(urldecode($value));
			}
		}

		$where = "";

		$date_col = "";

		if (isset($order_date_filter)) {

			$date_col = $order_date_filter;

		} else {
			$date_col = "order_date";
		}


		if (!isset($from_date) AND !isset($to_date)) {

			$from_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-1 month"));

			$from_date .= " 00:00:00";

			$where .= " AND $date_col >= '$from_date' ";

			$to_date = date("Y-m-d");
			$to_date .= " 23:59:00";

			$where .= " AND $date_col < '$to_date' ";
		} else {


			$from_date = date("Y-m-d", strtotime($from_date));

			$where .= " AND $date_col >= '$from_date' ";

			$to_date = date("Y-m-d", strtotime($to_date));


			$where .= " AND $date_col < '$to_date' ";


		}


		if (isset($pickup_store) && $pickup_store) {
			$query  = " SELECT REPLACE(GROUP_CONCAT(order_item_id SEPARATOR ','), ':', ',') as order_item_id FROM orders WHERE store_id=$pickup_store $where";
			$result = $this->db->Query($query);
			$row    = $this->db->FetchObjectRow($result);

			$query = "SELECT item_id ,SUM(item_qty) as qty FROM `orders_items` WHERE ordered=1 AND item_type='product' AND orders_items.order_item_id IN ($row->order_item_id) GROUP BY item_id";

			
		} else {
			
			$query = " SELECT REPLACE(GROUP_CONCAT(order_item_id SEPARATOR ','), ':', ',') as order_item_id FROM orders WHERE store_id > 0 $where";

			$result = $this->db->Query($query);
			$row    = $this->db->FetchObjectRow($result);

			$query = "SELECT item_id ,SUM(item_qty) as qty FROM `orders_items` WHERE ordered=1 AND item_type='product' AND orders_items.order_item_id IN ($row->order_item_id) GROUP BY item_id";


		}



		$result              = $this->db->Query($query);
		$standared_items_qty = $this->db->FetchAllArray($result);
		return $standared_items_qty;
	}
	/**
	 * ListCustom Items
	 */
	public function getCustomItems($post)
	{

		foreach ($post as $key => $value) {
			if (is_array($value)) {
				$$key = $value;
			} else {
				$$key = $this->clean_for_sql(urldecode($value));
			}
		}

		$where = "";

		$date_col = "";

		if (isset($order_date_filter)) {

			$date_col = $order_date_filter;

		} else {
			$date_col = "order_date";
		}

		if (!isset($from_date) AND !isset($to_date)) {

			$from_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-1 month"));

			$from_date .= " 00:00:00";

			$where .= " AND $date_col >= '$from_date' ";

			$to_date = date("Y-m-d");
			$to_date .= " 23:59:00";

			$where .= " AND $date_col < '$to_date' ";
		} else {


			$from_date = date("Y-m-d", strtotime($from_date));
			$from_date .= " 00:00:00";

			$where .= " AND $date_col >= '$from_date' ";

			$to_date = date("Y-m-d", strtotime($to_date));
			$to_date .= " 23:59:00";


			$where .= " AND $date_col < '$to_date' ";


		}

		if (isset($pickup_store) && $pickup_store) {
			
			if ($pickup_store) {
				$store = " store_id = $pickup_store ";
			} else
				$store = " store_id > 0";

			
			$query  = "SELECT order_item_id FROM orders as b WHERE $store $where";
			$result = $this->db->Query($query);
			$row    = $this->db->FetchAllArray($result);
			
			$out    = array();
			foreach($row as $rowData){
					
				$splitdata  = explode(":",$rowData['order_item_id']);
				foreach($splitdata as $split){
					$out[] =  $split;
				}
					
			}
			$orderIds = implode(",",$out);
			if(empty($orderIds)) $orderIds = "''";
			$query = "SELECT * FROM `orders_items` LEFT JOIN `user_sandwich_data` ON orders_items.item_id = user_sandwich_data.id WHERE orders_items.ordered=1 AND orders_items.item_type='user_sandwich' AND orders_items.order_item_id IN ($orderIds)";


		} else {
			
			
			$query  = "SELECT  order_item_id FROM orders WHERE store_id > 0 $where";
			$result = $this->db->Query($query);
			$row    = $this->db->FetchAllArray($result);
			
			$out    = array();
			foreach($row as $rowData){
					
				$splitdata  = explode(":",$rowData['order_item_id']);
				foreach($splitdata as $split){
					$out[] =  $split;
				}
					
			}
			$orderIds = implode(",",$out);
            if(empty($orderIds)) $orderIds = "''";
		    $query = "SELECT * FROM `orders_items` LEFT JOIN `user_sandwich_data` ON orders_items.item_id = user_sandwich_data.id WHERE orders_items.ordered=1 AND orders_items.item_type='user_sandwich' AND orders_items.order_item_id IN ($orderIds)";

		}


		$result      = $this->db->Query($query);
		$customitems = $this->db->FetchAllArray($result);
		return $customitems;
	}
	/**
	 * report TotalItemsFilter
	 */
	public function doTotalItemsFilterReports($post)
	{


		foreach ($post as $key => $value) {
			if (is_array($value)) {
				$$key = $value;
			} else {
				$$key = $this->clean_for_sql(urldecode($value));
			}
		}
		$where = " ";
		
		if (isset($order_type)) {
			$where .= " AND delivery IN (" . implode(",", $order_type) . ") ";

		}
		if (isset($order_status)) {

			$where .= " AND order_status IN (" . implode(",", $order_status) . ") ";

		}
		if (isset($type_purchase)) {

			$where .= " AND by_admin IN (" . implode(",", $type_purchase) . ") ";

		}

		$date_col = "";

		if (isset($order_date_filter)) {

			$date_col = $order_date_filter;

		} else {
			$date_col = "order_date";
		}

		if ($from_date) {

			$from_date = date("Y-m-d", strtotime($from_date));
			if ($from_time)
				$from_date .= " " . $from_time . ":00";
			else
				$from_date .= " 00:00:00";	

			$where .= " AND $date_col >= '$from_date' ";
		}
		if ($to_date) {

			$to_date = date("Y-m-d", strtotime($to_date));
			if ($to_time)
				$to_date .= " " . $to_time . ":00";
			else
				$to_date .= " 23:59:00";	

			$where .= " AND $date_col < '$to_date' ";
		}



		if ($system_user) {

			$where .= " AND user_id = $system_user AND by_admin = 1";
		}

		if (isset($pickup_store) && $pickup_store) {

			if ($pickup_store) {
				$store = " store_id = $pickup_store ";
			} else
				$store = " store_id > 0";

			$query  = " SELECT REPLACE(GROUP_CONCAT(order_item_id SEPARATOR ','), ':', ',') as order_item_id FROM orders as b WHERE $store $where";
			$result = $this->db->Query($query);
			$row    = $this->db->FetchObjectRow($result);

			$query = "SELECT item_id ,SUM(item_qty) as qty FROM `orders_items` as a WHERE a.ordered=1 AND a.item_type='product' AND a.order_item_id IN ($row->order_item_id) GROUP BY item_id";

			
		} else {
			
			$query  = " SELECT REPLACE(GROUP_CONCAT(order_item_id SEPARATOR ','), ':', ',') as order_item_id FROM orders WHERE store_id > 0 $where";
			$result = $this->db->Query($query);
			$row    = $this->db->FetchObjectRow($result);

			$query = "SELECT item_id ,SUM(item_qty) as qty FROM `orders_items` WHERE ordered=1 AND item_type='product' AND orders_items.order_item_id IN ($row->order_item_id) GROUP BY item_id";

		}

		$result = $this->db->Query($query);
		$orders = $this->db->FetchAllArray($result);
		return $orders;

	}
	/**
	 * Report TotalCustomItemsFilter
	 */
	public function doTotalCustomItemsFilterReports($post)
	{
		foreach ($post as $key => $value) {
			if (is_array($value)) {
				$$key = $value;
			} else {
				$$key = $this->clean_for_sql(urldecode($value));
			}
		}
		$where = " ";
		;
		
		if (isset($order_type)) {

			$where .= " AND delivery IN (" . implode(",", $order_type) . ") ";

		}
		if (isset($order_status)) {

			$where .= " AND order_status IN (" . implode(",", $order_status) . ") ";

		}
		if (isset($type_purchase)) {

			$where .= " AND by_admin IN (" . implode(",", $type_purchase) . ") ";

		}

		$date_col = "";

		if (isset($order_date_filter)) {

			$date_col = $order_date_filter;

		} else {
			$date_col = "order_date";
		}


		if ($from_date) {
			$from_date = date("Y-m-d", strtotime($from_date));
			if ($from_time)
				$from_date .= " " . $from_time . ":00";
			else
				$from_date .= " 00:00:00";		

			$where .= " AND $date_col >= '$from_date' ";

		}
		if ($to_date) {
			$to_date = date("Y-m-d", strtotime($to_date));
			if ($to_time)
				$to_date .= " " . $to_time . ":00";
			else
				$to_date .= " 23:59:00";		

			$where .= " AND $date_col < '$to_date' ";
		}
		if ($system_user) {

			$where .= " AND user_id = $system_user AND by_admin = 1";
		}

		if (isset($pickup_store) && $pickup_store) {

			if ($pickup_store) {
				$store = " store_id = $pickup_store ";
			} else
				$store = " store_id > 0";

			//$query  = "SELECT REPLACE(GROUP_CONCAT(order_item_id SEPARATOR ','), ':', ',') as order_item_id FROM orders as b WHERE $store $where";
			$query  = "SELECT order_item_id FROM orders as b WHERE $store $where";
			$result = $this->db->Query($query);
			$row    = $this->db->FetchAllArray($result);
			
			$out    = array();
			foreach($row as $rowData){
			
				$splitdata  = explode(":",$rowData['order_item_id']);
				foreach($splitdata as $split){
					$out[] =  $split;
				}
			
			}
			$orderIds = implode(",",$out);
			if(empty($orderIds)) $orderIds = "''";
			$query = "SELECT * FROM `orders_items` LEFT JOIN `user_sandwich_data` ON orders_items.item_id = user_sandwich_data.id WHERE orders_items.ordered=1 AND orders_items.item_type='user_sandwich' AND orders_items.order_item_id IN ($orderIds)";

			
		} else {
			
			$query  = "SELECT  order_item_id FROM orders WHERE store_id > 0 $where";
			$result = $this->db->Query($query);
			$row    = $this->db->FetchAllArray($result);
			
			$out    = array();
			foreach($row as $rowData){
				
				 $splitdata  = explode(":",$rowData['order_item_id']);
				 foreach($splitdata as $split){
				 	$out[] =  $split;
				 }
				
			}
			
			$orderIds = implode(",",$out);
			if(empty($orderIds)) $orderIds = "''";
			$query = "SELECT * FROM `orders_items` LEFT JOIN `user_sandwich_data` ON orders_items.item_id = user_sandwich_data.id WHERE orders_items.ordered=1 AND orders_items.item_type='user_sandwich' AND orders_items.order_item_id IN ($orderIds)";
		}
		
		//echo $query;
		//exit;

		$result = $this->db->Query($query);
		$orders = $this->db->FetchAllArray($result);
		return $orders;

	}
	/**
	 * List OrderedItems
	 */
	public function getOrderedItems($post)
	{

		foreach ($post as $key => $value) {
			if (is_array($value)) {
				$$key = $value;
			} else {
				$$key = $this->clean_for_sql(urldecode($value));
			}
		}

		$store_where = " ";
		if (isset($store_id)) {
			$store_where = " AND store_id = $store_id";
		}

		$time_slots = array(
				"0" => "12:00 AM - 12:59 AM",
				"1" => "1:00AM - 1:59AM",
				"2" => "2:00AM - 2:59AM",
				"3" => "3:00AM - 3:59AM",
				"4" => "4:00AM - 4:59AM",
				"5" => "5:00AM - 5:59AM",
				"6" => "6:00AM - 6:59AM",
				"7" => "7:00AM - 7:59AM",
				"8" => "8:00AM - 8:59AM",
				"9" => "9:00AM - 9:59AM",
				"10" => "10:00AM - 10:59AM",
				"11" => "11:00AM - 11:59AM",
				"12" => "12:00PM - 12:59PM",
				"13" => "1:00PM - 1:59PM",
				"14" => "2:00PM - 2:59PM",
				"15" => "3:00PM - 3:59PM",
				"16" => "4:00PM - 4:59PM",
				"17" => "5:00PM - 5:59PM",
				"18" => "6:00PM - 6:59PM",
				"19" => "7:00PM - 7:59PM",
				"20" => "8:00PM - 8:59PM",
				"21" => "9:00PM - 9:59PM",
				"22" => "10:00PM - 10:59PM",
				"23" => "11:00PM - 11:59PM"
		);

		$date    = date("Y-m-d");
		$results = array();
		foreach ($time_slots as $key => $slot) {

			$time = $slot;

			$query  = "SELECT count(order_id) as total_order, SUM(total) as total_sales FROM orders WHERE HOUR(order_date)= '$key' AND DATE(order_date) = '$date' $store_where ";
			$result = $this->db->Query($query);
			$total  = $this->db->FetchObjectRow($result);

			$query    = "SELECT count(order_id) as count FROM orders WHERE HOUR(order_date)= '$key' AND delivery = 1 AND DATE(order_date) = '$date' $store_where";
			$result   = $this->db->Query($query);
			$delivery = $this->db->FetchObjectRow($result);


			$pickups = $total->total_order - $delivery->count;
			if ($total->total_sales == NULL)
				$total->total_sales = 0;

			if ($total->total_order > 0)
				$sales = $total->total_sales / $total->total_order;
			else
				$sales = 0;

			$results[$key] = (Object) array(
					"total" => $total,
					"delivery" => $delivery->count,
					"pickups" => $pickups,
					"sales" => $sales,
					"time" => $time
			);

		}


		return $results;

	}
	/**
	 * List HourlyOrderReport
	 */
	public function getHourlyOrderReport($post)
	{

		foreach ($post as $key => $value) {
			if (is_array($value)) {
				$$key = $value;
			} else {
				$$key = $this->clean_for_sql(urldecode($value));
			}
		}

		$store_where = " ";
		if (isset($store_id) && $store_id) {
			$store_where = " AND store_id = $store_id";
		}

		$from = $post["from"];
		$to   = $post["to"];
		$date = $post['datehourreport'];

		$time_slots = array(
				"0" => "12:00 AM - 12:59 AM",
				"1" => "1:00AM - 1:59AM",
				"2" => "2:00AM - 2:59AM",
				"3" => "3:00AM - 3:59AM",
				"4" => "4:00AM - 4:59AM",
				"5" => "5:00AM - 5:59AM",
				"6" => "6:00AM - 6:59AM",
				"7" => "7:00AM - 7:59AM",
				"8" => "8:00AM - 8:59AM",
				"9" => "9:00AM - 9:59AM",
				"10" => "10:00AM - 10:59AM",
				"11" => "11:00AM - 11:59AM",
				"12" => "12:00PM - 12:59PM",
				"13" => "1:00PM - 1:59PM",
				"14" => "2:00PM - 2:59PM",
				"15" => "3:00PM - 3:59PM",
				"16" => "4:00PM - 4:59PM",
				"17" => "5:00PM - 5:59PM",
				"18" => "6:00PM - 6:59PM",
				"19" => "7:00PM - 7:59PM",
				"20" => "8:00PM - 8:59PM",
				"21" => "9:00PM - 9:59PM",
				"22" => "10:00PM - 10:59PM",
				"23" => "11:00PM - 11:59PM"
		);

	


		$results = array();
		foreach ($time_slots as $key => $slot) {

			if ($key >= $from AND $key <= $to) {

				$time = $slot;

				$query  = "SELECT count(order_id) as total_order, SUM(total) as total_sales FROM orders WHERE HOUR(order_date)= '$key' AND DATE(order_date) = '$date' $store_where ";
				$result = $this->db->Query($query);
				$total  = $this->db->FetchObjectRow($result);

				$query    = "SELECT count(order_id) as count FROM orders WHERE HOUR(order_date)= '$key' AND delivery = 1 AND DATE(order_date) = '$date' $store_where ";
				$result   = $this->db->Query($query);
				$delivery = $this->db->FetchObjectRow($result);


				$pickups = $total->total_order - $delivery->count;
				if ($total->total_sales == NULL)
					$total->total_sales = 0;

				if ($total->total_order > 0)
					$sales = $total->total_sales / $total->total_order;
				else
					$sales = 0;

				$results[$key] = (Object) array(
						"total" => $total,
						"delivery" => $delivery->count,
						"pickups" => $pickups,
						"sales" => $sales,
						"time" => $time
				);

			}

		}


		return $results;


	}
	/**
	 * List OrderDetails
	 */
	public function getOrderDetails($post)
	{
		foreach ($post as $key => $value) {
			$$key = $this->clean_for_sql(urldecode($value));
		}

		$query  = "SELECT *, DATE_FORMAT(date,'%m-%d-%Y') as date, u.first_name, u.last_name FROM orders as o JOIN users as u ON o.user_id = u.uid WHERE order_id = $order_id ";
		$result = $this->db->query($query);
		if ($this->db->Rows($result) < 1)
			return array();

		$orders = $this->db->FetchAllArray($result);

		foreach ($orders as $key => $order) {

			if ($order['delivery']) {
				$query                  = "SELECT * FROM order_address WHERE address_id = {$order['address_id']}";
				$order['delivery_type'] = "Delivery";
			} else {
				$query                  = "SELECT * FROM pickup_stores WHERE id = {$order['address_id']}";
				$order['delivery_type'] = "Pickup";
			}



			$result           = $this->db->query($query);
			$order['address'] = "";
			if ($this->db->Rows($result)) {
				$address = $this->db->FetchAllArray($result);

				$order['address'] = $address[0];
			}

			$ordersItems = str_replace(":", ",", $order['order_item_id']);
			$query       = "SELECT * FROM orders_items WHERE order_item_id IN ($ordersItems)";
			$result      = $this->db->query($query);
			$ordersItems = $this->db->FetchAllArray($result);

			$items = array();

			foreach ($ordersItems as $item) {

				if ($item['item_type'] == "user_sandwich") {
					$query    = "SELECT * FROM user_sandwich_data WHERE id = {$item['item_id']}";
					$name_col = "sandwich_name";
				} else if ($item['item_type'] == "product") {
					$query    = "SELECT * FROM standard_category_products WHERE id = {$item['item_id']}";
					$name_col = "product_name";
				}

				$result            = $this->db->query($query);
				$item_temp         = $this->db->FetchAllArray($result);
				$item_temp         = $item_temp[0];
				$item_temp['qty']  = $item['item_qty'];
				$item_temp['name'] = $item_temp[$name_col];
				$items[]           = $item_temp;
			}

			$order['items'] = $items;

			$orders[$key] = $order;
		}

		return $orders;

	}
	/**
	 * List ReportPin
	 */
	public function checkReportPin($post)
	{
		foreach ($post as $key => $value) {
			$$key = $this->clean_for_sql(urldecode($value));
		}

		$query    = "SELECT * FROM pickup_stores WHERE id = $id AND report_pin = '$report_pin'";
		$result   = $this->db->Query($query);
		$response = $this->db->FetchObjectRow($result);
		if ($this->db->Rows($result)) {
			if ($response->id == $id) {
				return TRUE;
			} else {
				return False;
			}
		} else {
			return FALSE;
		}
	}
	/**
	 * List TotalSaleCount
	 */
	public function getTotalSaleCount($post)
	{
		$post     = $this->process_input($post);
		$id       = $post['ids'];
		$sql      = "SELECT COUNT(*) AS `count` FROM `orders_items` WHERE item_id IN ($id) AND ordered = 1 ";
		$result   = $this->db->Query($sql);
		$response = $this->db->FetchObjectRow($result);
		return $response->count;
	}
	/**
	 * List process input
	 */
	private function process_input($data)
	{
		array_walk($data, function(&$val, $key)
		{
			$val = urldecode($val);
			$val = $this->clean_for_sql($val);
		});
		return $data;
	}

	/**
	 * List Transaction Log
	 */
	public function getTransactionLog($post)
	{
		$post     = $this->process_input($post);
		$sql      = "SELECT `log` FROM `log_transaction` ORDER BY id DESC";
		$result   = $this->db->Query($sql);
		$response = $this->db->FetchAllArray($result);
		return $response;
	}
}
