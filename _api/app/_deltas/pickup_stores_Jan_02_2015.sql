-- phpMyAdmin SQL Dump
-- version 4.0.10.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2015 at 06:52 PM
-- Server version: 5.5.40-0ubuntu0.12.04.1
-- PHP Version: 5.4.34-1+deb.sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `pickup_stores`
--

CREATE TABLE IF NOT EXISTS `pickup_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(200) NOT NULL,
  `address1` varchar(200) NOT NULL,
  `adress2` varchar(200) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `report_pin` varchar(100) NOT NULL,
  `restrict_ip` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pickup_stores`
--

INSERT INTO `pickup_stores` (`id`, `store_name`, `address1`, `adress2`, `zip`, `phone`, `uid`, `report_pin`, `restrict_ip`) VALUES
(1, 'Haight-Ashbury Store #1 ', '123 E', '12th Street, New York', 'NY 10003 ', '212-123-4567', 10, '', ''),
(2, 'Haight-Ashbury Store #2', '123 E F', '12th Street, New York', 'NY 10003 ', '212-123-4567', 10, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
