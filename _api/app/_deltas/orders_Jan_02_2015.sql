-- phpMyAdmin SQL Dump
-- version 4.0.10.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2015 at 06:51 PM
-- Server version: 5.5.40-0ubuntu0.12.04.1
-- PHP Version: 5.4.34-1+deb.sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `billinginfo_id` int(12) NOT NULL,
  `order_item_id` varchar(200) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `delivery` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `store_id`, `user_id`, `address_id`, `billinginfo_id`, `order_item_id`, `total`, `tax`, `date`, `time`, `delivery`, `order_status`) VALUES
(12, 1, 4, 1, 0, '2:3', 166.74, 0.00, '2014-12-27', '1:00 PM', 1, 7),
(13, 1, 4, 1, 0, '2:3', 170.00, 0.00, '2014-12-27', '1:00 PM', 1, 3),
(14, 1, 4, 1, 0, '2:3', 170.00, 0.00, '2014-12-27', '1:00 PM', 1, 3),
(15, 1, 4, 2, 0, '2:3', 150.00, 0.00, '2014-12-30', '12:05PM', 0, 3),
(16, 1, 4, 2, 0, '2:3', 150.00, 0.00, '2014-12-30', '12:05PM', 0, 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
