-- phpMyAdmin SQL Dump
-- version 4.0.10.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 12, 2015 at 07:03 PM
-- Server version: 5.5.40-0ubuntu0.12.04.1
-- PHP Version: 5.3.10-1ubuntu3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `admin_cat_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`uid`, `admin_cat_id`, `first_name`, `last_name`, `user_name`, `password`) VALUES
(1, 1, 'Vasanth1', 'Kumar', 'vasanth@office-partners360.com', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 2, 'ranganath', '', 'ranganath_admin', 'qwerty'),
(3, 1, 'test2', '', 'test', '098f6bcd4621d373cade4e832627b4f6'),
(5, 0, 'Ranganath BR', '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(6, 0, 'Ranganath BR', '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(7, 0, 'test ranganth', '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(8, 0, 'Ranganath BR', '', 'what else', 'd41d8cd98f00b204e9800998ecf8427e'),
(9, 0, 'Ranganath BR', '', 'what else', 'd41d8cd98f00b204e9800998ecf8427e'),
(10, 0, 'Ranganath BRr', '', 'what else', 'd41d8cd98f00b204e9800998ecf8427e');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_category`
--

CREATE TABLE IF NOT EXISTS `admin_user_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `cat_privilege` int(11) NOT NULL DEFAULT '0',
  `cat_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin_user_category`
--

INSERT INTO `admin_user_category` (`id`, `cat_name`, `cat_privilege`, `cat_status`) VALUES
(1, 'ADMIN', 0, 0),
(2, 'CUSTOMER SERVICE', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_notes`
--

CREATE TABLE IF NOT EXISTS `customer_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `customer_notes`
--

INSERT INTO `customer_notes` (`id`, `user_id`, `date`, `notes`) VALUES
(1, 4, '2015-01-12', 'This is my first comment.');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `is_live` tinyint(1) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `discount_amount` int(11) NOT NULL,
  `applicable_to` tinyint(1) NOT NULL,
  `uses_type` tinyint(1) NOT NULL,
  `uses_amount` int(11) NOT NULL,
  `minimum_order` tinyint(1) NOT NULL,
  `minimum_order_amount` int(11) NOT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `is_live`, `name`, `code`, `type`, `discount_amount`, `applicable_to`, `uses_type`, `uses_amount`, `minimum_order`, `minimum_order_amount`, `expiration`) VALUES
(1, 0, 'test', '12345', 1, 20, 0, 0, 1999, 0, 0, '2015-01-28 18:30:00'),
(2, 0, 'sankranti', 'qwerty', 0, 30, 1, 0, 20, 0, 999, '2015-01-30 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `friend_data`
--

CREATE TABLE IF NOT EXISTS `friend_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friend_uid` int(200) NOT NULL,
  `friend_name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `billinginfo_id` int(12) NOT NULL,
  `order_item_id` varchar(200) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `tip` decimal(10,2) NOT NULL,
  `coupon_code` varchar(250) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `delivery` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `store_id`, `user_id`, `address_id`, `billinginfo_id`, `order_item_id`, `sub_total`, `tip`, `coupon_code`, `total`, `tax`, `date`, `time`, `delivery`, `order_status`) VALUES
(12, 1, 4, 1, 0, '2:3', 0.00, 0.00, '', 166.74, 0.00, '2014-12-27', '1:00 PM', 1, 7),
(13, 1, 4, 1, 0, '2:3', 0.00, 0.00, '', 170.00, 0.00, '2014-12-27', '1:00 PM', 1, 3),
(14, 1, 4, 1, 0, '2:3', 0.00, 0.00, '', 170.00, 0.00, '2014-12-27', '1:00 PM', 1, 3),
(15, 1, 4, 2, 0, '2:3', 0.00, 0.00, '', 150.00, 0.00, '2014-12-30', '12:05PM', 0, 3),
(16, 1, 4, 2, 0, '2:3', 0.00, 0.00, '', 150.00, 0.00, '2014-12-30', '12:05PM', 0, 3),
(17, 1, 4, 1, 1, '54:57:66:81', 242.66, 4.00, '1', 226.66, 0.00, '2015-01-12', '', 0, 0),
(18, 1, 4, 3, 3, '54:57:66:81', 242.66, 4.00, '1', 226.66, 0.00, '2015-01-12', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders_items`
--

CREATE TABLE IF NOT EXISTS `orders_items` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_qty` int(11) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_tax` decimal(10,2) NOT NULL,
  `ordered` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `orders_items`
--

INSERT INTO `orders_items` (`order_item_id`, `item_id`, `item_type`, `user_id`, `item_qty`, `item_price`, `item_tax`, `ordered`) VALUES
(2, 1, 'user_sandwich', 4, 1, 49.52, 0.00, 1),
(3, 2, 'user_sandwich', 4, 1, 113.22, 0.00, 1),
(5, 2, 'user_sandwich', 4, 5, 566.10, 0.00, 1),
(6, 1, 'user_sandwich', 4, 6, 297.12, 0.00, 1),
(8, 1, 'user_sandwich', 4, 8, 396.16, 0.00, 1),
(9, 1, 'user_sandwich', 4, 2, 99.04, 0.00, 1),
(11, 3, 'user_sandwich', 4, 8, 472.16, 0.00, 1),
(12, 2, 'user_sandwich', 4, 4, 452.88, 0.00, 1),
(13, 3, 'user_sandwich', 4, 1, 59.02, 0.00, 1),
(14, 2, 'user_sandwich', 4, 1, 113.22, 0.00, 1),
(15, 3, 'user_sandwich', 4, 1, 59.02, 0.00, 1),
(16, 2, 'user_sandwich', 4, 2, 226.44, 0.00, 1),
(17, 2, 'user_sandwich', 4, 1, 113.22, 0.00, 1),
(18, 2, 'user_sandwich', 4, 1, 113.22, 0.00, 1),
(30, 2, '', 0, 1, 0.00, 0.00, 0),
(45, 3, '', 0, 1, 0.00, 0.00, 0),
(54, 3, 'user_sandwich', 4, 1, 59.02, 0.00, 1),
(56, 5, 'user_sandwich', 0, 1, 0.00, 0.00, 0),
(57, 5, 'user_sandwich', 4, 1, 58.42, 0.00, 1),
(66, 2, 'user_sandwich', 4, 1, 113.22, 0.00, 1),
(80, 6, 'user_sandwich', 0, 1, 0.00, 0.00, 0),
(81, 1, 'product', 4, 1, 49.52, 0.00, 1),
(90, 2, 'user_sandwich', 4, 1, 113.22, 0.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_address`
--

CREATE TABLE IF NOT EXISTS `order_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `company` varchar(250) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `street` varchar(250) NOT NULL,
  `cross_streets` varchar(250) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `extn` varchar(200) NOT NULL,
  `zip` varchar(200) NOT NULL,
  `delivery_instructions` text NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `order_address`
--

INSERT INTO `order_address` (`address_id`, `name`, `company`, `address1`, `address2`, `street`, `cross_streets`, `phone`, `extn`, `zip`, `delivery_instructions`, `uid`) VALUES
(1, 'Ranganath BR', 'test', 'test', '', '', '', '918722397961--918722397961', '', '577527', '', 4);

-- --------------------------------------------------------

--
-- Table structure for table `order_billinginfo`
--

CREATE TABLE IF NOT EXISTS `order_billinginfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_name` text NOT NULL,
  `card_number` varchar(100) NOT NULL,
  `card_type` varchar(200) NOT NULL,
  `card_zip` varchar(150) NOT NULL,
  `expire_month` varchar(100) NOT NULL,
  `expire_year` varchar(100) NOT NULL,
  `uid` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `order_billinginfo`
--

INSERT INTO `order_billinginfo` (`id`, `card_name`, `card_number`, `card_type`, `card_zip`, `expire_month`, `expire_year`, `uid`) VALUES
(1, '', '1234567812345678', 'visa', 'plpl', '03', '2021', 4),
(2, '', '1234567812345678', 'visa', '', '01', '2015', 4),
(3, '', '1234567812345678', 'visa', '', '01', '2015', 4),
(4, '', '1234567812345678', 'visa', '', '01', '2015', 4),
(5, '', '1234567812345678', 'visa', 'qqq', '01', '2015', 4),
(6, '', '1234567812345678', 'visa', '', '01', '2015', 4),
(7, '', '1234567812345678', 'visa', 'jiui', '03', '2017', 4);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  `code` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `label`, `code`, `status`) VALUES
(1, 'PROCESSED', 'pickup_prc', 0),
(2, 'BAGGED/READY', 'pickup_bag', 0),
(3, 'PICKED UP', 'pickup_out', 0),
(4, 'CANCEL', 'pickup_cancel', 0),
(5, 'PROCESSED', 'Delivery_Pro', 1),
(6, 'OUT FOR DELIVERY', 'Delivery_out', 1),
(7, 'DELIVERED', 'Delivery_del', 1),
(8, 'CANCEL', 'Delivery_Cancel', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pickup_stores`
--

CREATE TABLE IF NOT EXISTS `pickup_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(200) NOT NULL,
  `address1` varchar(200) NOT NULL,
  `adress2` varchar(200) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `report_pin` varchar(100) NOT NULL,
  `restrict_ip` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pickup_stores`
--

INSERT INTO `pickup_stores` (`id`, `store_name`, `address1`, `adress2`, `zip`, `phone`, `uid`, `report_pin`, `restrict_ip`) VALUES
(2, 'Haight-Ashbury Store #2', '123 E F', '12th Street, New York', 'NY 10003 ', '212-123-4567', 10, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `post_data`
--

CREATE TABLE IF NOT EXISTS `post_data` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `fb_post_id` int(200) NOT NULL,
  `fb_post_msg` text NOT NULL,
  `fb_post_link` text NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_categories`
--

CREATE TABLE IF NOT EXISTS `sandwich_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` text NOT NULL,
  `category_identifier` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sandwich_categories`
--

INSERT INTO `sandwich_categories` (`id`, `category_name`, `category_identifier`) VALUES
(1, 'bread', 'BREAD'),
(2, 'protein', 'PROTEIN'),
(3, 'cheese', 'CHEESE'),
(4, 'toppings', 'TOPPINGS'),
(5, 'condiments', 'CONDIMENTS'),
(6, 'condiments', 'CONDIMENTS');

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_category_items`
--

CREATE TABLE IF NOT EXISTS `sandwich_category_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` text NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_image` varchar(250) NOT NULL,
  `category_id` int(10) NOT NULL,
  `options_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `sandwich_category_items`
--

INSERT INTO `sandwich_category_items` (`id`, `item_name`, `item_price`, `item_image`, `category_id`, `options_id`) VALUES
(1, 'ITALIAN+HERO', 14.50, 'create-sandwich-bread.png', 1, '0'),
(2, 'SESAME HERO', 18.50, 'create-sandwich-bread.png', 1, '0'),
(3, 'CIABATTA', 22.00, 'create-sandwich-bread.png', 1, '0'),
(4, 'CROISSANT', 8.20, 'create-sandwich-bread.png', 1, '0'),
(5, 'RYE', 16.10, 'create-sandwich-bread.png', 1, '0'),
(6, 'WHITE', 10.50, 'create-sandwich-bread.png', 1, '0'),
(7, '9-GRAIN WHOLE WHEAT', 11.50, 'create-sandwich-bread.png', 1, '0'),
(8, 'GLUTEN-FREE', 5.50, 'create-sandwich-bread.png', 1, '0'),
(9, 'TURKEY', 15.00, 'create_protein.png', 2, '1:2'),
(10, 'HAM', 10.22, 'create_protein.png', 2, '0'),
(11, 'SALAMI', 18.22, 'create_protein.png', 2, '0'),
(12, 'ROAST BEEF', 6.22, 'create_protein.png', 2, '0'),
(13, 'PROSCIUTTO', 2.80, 'create_protein.png', 2, '0'),
(14, 'CORNED BEEF', 9.78, 'create_protein.png', 2, '1:2'),
(15, 'PASTRAMI', 7.66, 'create_protein.png', 2, '0'),
(16, 'CHICKEN SALAD', 12.00, 'create_protein.png', 2, '0'),
(17, 'CHEDDAR', 8.00, 'create_cheese.png', 3, '1:2'),
(18, 'SWISS', 10.00, 'create_cheese.png', 3, '0'),
(19, 'AMERICAN', 20.57, 'create_cheese.png', 3, '0'),
(20, 'PROVOLONE', 15.22, 'create_cheese.png', 3, '0'),
(21, 'MUENSTER', 18.78, 'create_cheese.png', 3, '0'),
(22, 'MOZZARELLA', 23.22, 'create_cheese.png', 3, '1:2'),
(23, 'BRIE', 15.00, 'create_cheese.png', 3, '0'),
(24, 'FETA', 6.22, 'create_cheese.png', 3, '0'),
(25, 'LETTUCE', 9.00, 'create_toppings.png', 4, '3:4'),
(26, 'TOMATO', 8.00, 'create_toppings.png', 4, '0'),
(27, 'RED ONION', 7.72, 'create_toppings.png', 4, '0'),
(28, 'CUCUMBER', 6.22, 'create_toppings.png', 4, '0'),
(29, 'PICKLES', 9.22, 'create_toppings.png', 4, '0'),
(30, 'JALAPENOS', 15.22, 'create_toppings.png', 4, '3:4'),
(31, 'AVOCADO', 17.00, 'create_toppings.png', 4, '0'),
(32, 'SAURKRAUT', 6.58, 'create_toppings.png', 4, '0'),
(33, 'MAYONNAISE', 8.00, 'create_condiments.png', 5, '3:4'),
(34, 'SPICY MAYONNAISE', 7.00, 'create_condiments.png', 5, '0'),
(35, 'SPICY BROWN MUSTARD', 12.00, 'create_condiments.png', 5, '0'),
(36, 'DIJON MUSTARD', 16.12, 'create_condiments.png', 5, '0'),
(37, 'HONEY CUP MUSTARD', 20.00, 'create_condiments.png', 5, '0'),
(38, 'RUSSIAN DRESSING', 17.58, 'create_condiments.png', 5, '3:4'),
(39, 'PESTO', 3.70, 'create_condiments.png', 5, '0'),
(40, 'THAI PEANUT', 5.54, 'create_condiments.png', 5, '0');

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_item_options`
--

CREATE TABLE IF NOT EXISTS `sandwich_item_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_name` text NOT NULL,
  `option_unit` varchar(200) NOT NULL,
  `price_mult` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sandwich_item_options`
--

INSERT INTO `sandwich_item_options` (`id`, `option_name`, `option_unit`, `price_mult`) VALUES
(1, 'NORMAL (1.0)', '1.0', '1'),
(2, 'DOUBLE (2.0)', '2.0', '2'),
(3, 'NORMAL (N)', 'N', '1'),
(4, 'HEAVY (H)', 'H', '2');

-- --------------------------------------------------------

--
-- Table structure for table `standard_category`
--

CREATE TABLE IF NOT EXISTS `standard_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `standard_cat_name` text NOT NULL,
  `category_identifier` text NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `standard_category`
--

INSERT INTO `standard_category` (`id`, `standard_cat_name`, `category_identifier`, `priority`) VALUES
(1, 'salads', 'SALADS', 1),
(2, 'SIDES', 'SIDES', 2),
(3, 'test', 'TEST', 0),
(4, 'test+test', 'TEST1', 0),
(5, 'testing', 'Testing', 0),
(6, '', '', 0),
(7, '', '', 0),
(8, 'Test', 'Test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `standard_category_products`
--

CREATE TABLE IF NOT EXISTS `standard_category_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` text NOT NULL,
  `description` text NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `standard_category_id` int(11) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `standard_category_products`
--

INSERT INTO `standard_category_products` (`id`, `product_name`, `description`, `product_price`, `standard_category_id`, `priority`) VALUES
(1, 'Cobb+Salad', '', 12.00, 1, 1),
(2, 'Chef Salad', '', 32.00, 1, 2),
(3, 'Garlic Dill Pickle', '', 21.00, 2, 1),
(4, 'Honeycrisp Apple', '', 123.00, 2, 2),
(7, 'what+is', '', 87.00, 1, 3),
(8, 'nice+one', '', 123.00, 3, 1),
(9, 'good+q', '', 12.00, 4, 1),
(10, 'nothing+ya', '', 321.00, 5, 2),
(11, 'why', '', 123.00, 6, 5),
(13, 'test', '', 56.00, 8, 2),
(14, 'Cobb+Salad', '', 78678.00, 1, 4),
(15, 'test', '', 111.00, 8, 0),
(16, 'what%2Bhappen1', '', 11.00, 7, 0),
(17, 'fruit salad', 'Ciabatta1, Turkey (1), Ham (0.5), Pepperoni (0.5), Cheddar (1), Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado, Mayonnaise (N), Blueberry Jam (S)', 111.00, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `store_timings`
--

CREATE TABLE IF NOT EXISTS `store_timings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(50) NOT NULL,
  `open` varchar(50) NOT NULL,
  `close` varchar(50) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `store_timings`
--

INSERT INTO `store_timings` (`id`, `day`, `open`, `close`, `store_id`) VALUES
(1, '', '12.00AM', '11.59PM', 0),
(2, '', '12.00AM', '11.59PM', 0),
(3, '', '', '', 0),
(4, '', '12.00AM', '11.59PM', 0),
(5, '', '12.00AM', '11.59PM', 0),
(6, '', '12.00AM', '11.59PM', 0),
(7, '', '12.00AM', '11.59PM', 0),
(8, 'Sunday', '12.00AM', '11.59PM', 0),
(9, 'Monday', '12.00AM', '11.59PM', 0),
(10, '', '', '', 0),
(11, '', '12.00AM', '11.59PM', 0),
(12, '', '12.00AM', '11.59PM', 0),
(13, '', '12.00AM', '11.59PM', 0),
(14, '', '12.00AM', '11.59PM', 0),
(15, '', '12.00AM', '11.59PM', 0),
(16, 'Monday', '12.00AM', '11.59PM', 0),
(17, '', '', '', 0),
(18, '', '12.00AM', '11.59PM', 0),
(19, '', '12.00AM', '11.59PM', 0),
(20, '', '12.00AM', '11.59PM', 0),
(21, '', '11.59PM', '11.59PM', 0),
(22, '', '12.00AM', '11.59PM', 0),
(23, '', '12.00AM', '11.59PM', 0),
(24, '', '', '', 0),
(25, '', '12.00AM', '11.59PM', 0),
(26, '', '12.00AM', '11.59PM', 0),
(27, '', '12.00AM', '11.59PM', 0),
(28, '', '11.59PM', '11.59PM', 0),
(29, '', '12.00AM', '11.59PM', 0),
(30, 'Monday', '12.00AM', '11.59PM', 0),
(31, '', '', '', 0),
(32, 'Tuesday', '12.00AM', '11.59PM', 0),
(33, '', '12.00AM', '11.59PM', 0),
(34, '', '12.00AM', '11.59PM', 0),
(35, '', '11.59PM', '11.59PM', 0),
(36, 'Sunday', '12.00AM', '11.59PM', 0),
(37, 'Monday', '12.00AM', '11.59PM', 0),
(38, '', '', '', 0),
(39, '', '12.00AM', '11.59PM', 0),
(40, '', '12.00AM', '11.59PM', 0),
(41, '', '12.00AM', '11.59PM', 0),
(42, '', '12.00AM', '11.59PM', 0),
(43, 'Sunday', '12.00AM', '11.59PM', 0),
(44, 'Monday', '12.00AM', '11.59PM', 0),
(45, '', '', '', 0),
(46, 'Tuesday', '12.00AM', '11.59PM', 0),
(47, 'Wednesday', '12.00AM', '11.59PM', 0),
(48, '', '12.00AM', '11.59PM', 0),
(49, '', '11.59PM', '11.59PM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `store_zipcodes`
--

CREATE TABLE IF NOT EXISTS `store_zipcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zipcode` int(11) NOT NULL,
  `abbreviation` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `store_zipcodes`
--

INSERT INTO `store_zipcodes` (`id`, `zipcode`, `abbreviation`, `store_id`, `status`) VALUES
(1, 577527, 'qwqw', 2, 0),
(2, 1231241, 'dfs', 2, 0),
(3, 1234123, 'erwr', 2, 0),
(4, 577527, 'hjkhjk', 2, 0),
(5, 54545, 'hjkhjk', 2, 0),
(6, 577527, 'hjkhjk', 2, 0),
(7, 54545, 'hjkhjk', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fb_id` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(250) CHARACTER SET utf8 NOT NULL,
  `fb_auth_token` varchar(250) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `fb_id`, `password`, `fb_auth_token`, `first_name`, `last_name`) VALUES
(4, 'vasanth@office-partners360.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 'Vasanth', 'Kumar');

-- --------------------------------------------------------

--
-- Table structure for table `user_sandwich_data`
--

CREATE TABLE IF NOT EXISTS `user_sandwich_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(20) NOT NULL,
  `sandwich_name` text NOT NULL,
  `sandwich_data` longtext NOT NULL,
  `sandwich_price` decimal(10,2) NOT NULL,
  `date_of_creation` datetime NOT NULL,
  `is_public` int(20) DEFAULT NULL,
  `menu_is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user_sandwich_data`
--

INSERT INTO `user_sandwich_data` (`id`, `uid`, `sandwich_name`, `sandwich_data`, `sandwich_price`, `date_of_creation`, `is_public`, `menu_is_active`) VALUES
(1, 4, 'custom sandwich 1', '{"BREAD":{"item_name":["SESAME HERO"],"item_price":["18.50"],"item_qty":{},"item_id":[2],"item_image":["create-sandwich-bread.png"]},"PROTEIN":{"item_name":["PROSCIUTTO","SALAMI"],"item_price":["2.80",18.22],"item_qty":{},"item_id":[13,11],"item_image":["create_protein.png","create_protein.png"]},"CHEESE":{"item_name":["SWISS"],"item_price":["10.00"],"item_qty":{},"item_id":[18],"item_image":["create_cheese.png"]},"TOPPINGS":{"item_name":[],"item_price":[],"item_qty":{},"item_id":[],"item_image":[]},"CONDIMENTS":{"item_name":[],"item_price":[],"item_qty":{},"item_id":[],"item_image":[]}}', 49.52, '2014-12-22 14:53:17', 1, 0),
(2, 4, 'custom sandwich 2', '{"BREAD":{"item_name":["CIABATTA"],"item_price":["22.00"],"item_qty":{},"item_id":[3],"item_image":["create-sandwich-bread.png"]},"PROTEIN":{"item_name":["CORNED BEEF","ROAST BEEF","SALAMI","HAM"],"item_price":[9.78,6.22,18.22,10.22],"item_qty":{"CORNED BEEF":["NORMAL (1.0)","1.0",1,""]},"item_id":[14,12,11,10],"item_image":["create_protein.png","create_protein.png","create_protein.png","create_protein.png"]},"CHEESE":{"item_name":["PROVOLONE"],"item_price":[15.22],"item_qty":{},"item_id":[20],"item_image":["create_cheese.png"]},"TOPPINGS":{"item_name":["PICKLES","CUCUMBER"],"item_price":[9.22,6.22],"item_qty":{},"item_id":[29,28],"item_image":["create_toppings.png","create_toppings.png"]},"CONDIMENTS":{"item_name":["DIJON MUSTARD"],"item_price":[16.12],"item_qty":{},"item_id":[36],"item_image":["create_condiments.png"]}}', 113.22, '2014-12-19 17:15:15', 1, 1),
(3, 4, 'custom sandwich 3', '{"BREAD":{"item_name":["CIABATTA"],"item_price":["22.00"],"item_qty":{},"item_id":[3],"item_image":["create-sandwich-bread.png"]},"PROTEIN":{"item_name":["PROSCIUTTO","ROAST BEEF"],"item_price":["2.80",6.22],"item_qty":{},"item_id":[13,12],"item_image":["create_protein.png","create_protein.png"]},"CHEESE":{"item_name":["MUENSTER"],"item_price":[18.78],"item_qty":{},"item_id":[21],"item_image":["create_cheese.png"]},"TOPPINGS":{"item_name":["PICKLES"],"item_price":[9.22],"item_qty":{},"item_id":[29],"item_image":["create_toppings.png"]},"CONDIMENTS":{"item_name":[],"item_price":[],"item_qty":{},"item_id":[],"item_image":[]}}', 59.02, '2014-12-19 17:15:51', 1, 1),
(4, 4, 'custom sandwich 4', '{"BREAD":{"item_name":["9-GRAIN WHOLE WHEAT"],"item_price":["11.50"],"item_qty":{},"item_id":[7],"item_image":["create-sandwich-bread.png"]},"PROTEIN":{"item_name":["CORNED BEEF","ROAST BEEF","SALAMI","HAM"],"item_price":[9.78,6.22,18.22,10.22],"item_qty":{"CORNED BEEF":["NORMAL (1.0)","1.0",1,""]},"item_id":[14,12,11,10],"item_image":["create_protein.png","create_protein.png","create_protein.png","create_protein.png"]},"CHEESE":{"item_name":["PROVOLONE","FETA"],"item_price":[15.22,6.22],"item_qty":{},"item_id":[20,24],"item_image":["create_cheese.png","create_cheese.png"]},"TOPPINGS":{"item_name":["PICKLES","CUCUMBER"],"item_price":[9.22,6.22],"item_qty":{},"item_id":[29,28],"item_image":["create_toppings.png","create_toppings.png"]},"CONDIMENTS":{"item_name":["DIJON MUSTARD"],"item_price":[16.12],"item_qty":{},"item_id":[36],"item_image":["create_condiments.png"]}}', 108.94, '2015-01-08 12:53:10', 1, 0),
(5, 4, 'test', '{"BREAD":{"item_name":["ITALIAN+HERO"],"item_price":["14.50"],"item_qty":{},"item_id":[1],"item_image":["create-sandwich-bread.png"]},"PROTEIN":{"item_name":["ROAST BEEF"],"item_price":[6.22],"item_qty":{},"item_id":[12],"item_image":["create_protein.png"]},"CHEESE":{"item_name":["MUENSTER"],"item_price":[18.78],"item_qty":{},"item_id":[21],"item_image":["create_cheese.png"]},"TOPPINGS":{"item_name":["JALAPENOS"],"item_price":[15.22],"item_qty":{"JALAPENOS":["NORMAL (N)","N",1,""]},"item_id":[30],"item_image":["create_toppings.png"]},"CONDIMENTS":{"item_name":["PESTO"],"item_price":["3.70"],"item_qty":{},"item_id":[39],"item_image":["create_condiments.png"]}}', 58.42, '2015-01-08 15:21:50', 1, 1),
(6, 4, 'ttttt', '{"BREAD":{"item_name":["RYE"],"item_price":["16.10"],"item_qty":{},"item_id":[5],"item_image":["create-sandwich-bread.png"]},"PROTEIN":{"item_name":["ROAST BEEF"],"item_price":[6.22],"item_qty":{},"item_id":[12],"item_image":["create_protein.png"]},"CHEESE":{"item_name":["PROVOLONE"],"item_price":[15.22],"item_qty":{},"item_id":[20],"item_image":["create_cheese.png"]},"TOPPINGS":{"item_name":["AVOCADO"],"item_price":["17.00"],"item_qty":{},"item_id":[31],"item_image":["create_toppings.png"]},"CONDIMENTS":{"item_name":["PESTO"],"item_price":["3.70"],"item_qty":{},"item_id":[39],"item_image":["create_condiments.png"]}}', 58.24, '2015-01-08 15:30:13', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `webpages_data`
--

CREATE TABLE IF NOT EXISTS `webpages_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webpage_name` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `webpages_data`
--

INSERT INTO `webpages_data` (`id`, `webpage_name`, `text`, `status`) VALUES
(1, 'Locations', '<p>test</p>\r\n', 0),
(2, 'ABOUT US', '<p><em><strong><img alt="" src="http://admin.ha/upload/20150106_111836.jpg" style="height:177px; width:284px" />About Us About UsAbout UsAbout UsAbout UsAbout UsAbout UsAbout Us</strong></em></p>\r\n', 0),
(3, 'CATERING', '<p>Catering&nbsp;Catering</p>\r\n\r\n<p>Catering</p>\r\n\r\n<p>CateringCatering</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>CateringCateringCatering</p>\r\n', 0),
(4, 'JOBS', '<p>Job</p>\r\n', 0),
(7, 'CONTACT', '<p>Contact</p>\r\n', 0),
(8, 'FEEDBACK', 'FEEDBACK', 0);

-- --------------------------------------------------------

--
-- Table structure for table `webpages_homepage_data`
--

CREATE TABLE IF NOT EXISTS `webpages_homepage_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(250) NOT NULL,
  `banner_type` int(10) NOT NULL,
  `image` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `priority` int(10) NOT NULL,
  `banner_status` int(11) NOT NULL,
  `redirection_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `webpages_homepage_data`
--

INSERT INTO `webpages_homepage_data` (`id`, `banner_name`, `banner_type`, `image`, `text`, `priority`, `banner_status`, `redirection_status`) VALUES
(1, 'fruits test', 1, 'http://admin.ha/upload/20150106_111758.jpeg', 'u r doining', 1, 1, 1),
(2, 'fruitss', 2, 'http://admin.ha/upload/20150107_035049.png', 'testing', 2, 1, 0),
(3, 'cake', 3, 'http://admin.ha/upload/20150106_114442.jpg', 'cake is awesome', 0, 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
