-- phpMyAdmin SQL Dump
-- version 4.0.10.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2015 at 06:51 PM
-- Server version: 5.5.40-0ubuntu0.12.04.1
-- PHP Version: 5.4.34-1+deb.sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`uid`, `first_name`, `last_name`, `user_name`, `password`) VALUES
(1, 'Vasanth', 'Kumar', 'vasanth@office-partners360.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `is_live` tinyint(1) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `discount_amount` int(11) NOT NULL,
  `applicable_to` tinyint(1) NOT NULL,
  `uses_type` tinyint(1) NOT NULL,
  `uses_amount` int(11) NOT NULL,
  `minimum_order` tinyint(1) NOT NULL,
  `minimum_order_amount` int(11) NOT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friend_data`
--

CREATE TABLE IF NOT EXISTS `friend_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friend_uid` int(200) NOT NULL,
  `friend_name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `billinginfo_id` int(12) NOT NULL,
  `order_item_id` varchar(200) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `delivery` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `store_id`, `user_id`, `address_id`, `billinginfo_id`, `order_item_id`, `total`, `tax`, `date`, `time`, `delivery`, `order_status`) VALUES
(12, 1, 4, 1, 0, '2:3', 166.74, 0.00, '2014-12-27', '1:00 PM', 1, 7),
(13, 1, 4, 1, 0, '2:3', 170.00, 0.00, '2014-12-27', '1:00 PM', 1, 3),
(14, 1, 4, 1, 0, '2:3', 170.00, 0.00, '2014-12-27', '1:00 PM', 1, 3),
(15, 1, 4, 2, 0, '2:3', 150.00, 0.00, '2014-12-30', '12:05PM', 0, 3),
(16, 1, 4, 2, 0, '2:3', 150.00, 0.00, '2014-12-30', '12:05PM', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders_items`
--

CREATE TABLE IF NOT EXISTS `orders_items` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_qty` int(11) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_tax` decimal(10,2) NOT NULL,
  `ordered` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `orders_items`
--

INSERT INTO `orders_items` (`order_item_id`, `item_id`, `user_id`, `item_qty`, `item_price`, `item_tax`, `ordered`) VALUES
(2, 1, 4, 1, 49.52, 0.00, 1),
(3, 2, 4, 1, 113.22, 0.00, 1),
(5, 2, 4, 5, 566.10, 0.00, 1),
(6, 1, 4, 6, 297.12, 0.00, 1),
(7, 2, 0, 1, 0.00, 0.00, 0),
(8, 1, 4, 8, 396.16, 0.00, 1),
(9, 1, 4, 2, 99.04, 0.00, 1),
(11, 3, 4, 8, 472.16, 0.00, 1),
(12, 2, 4, 4, 452.88, 0.00, 1),
(13, 3, 4, 1, 59.02, 0.00, 1),
(14, 2, 4, 1, 113.22, 0.00, 1),
(15, 3, 4, 1, 59.02, 0.00, 1),
(16, 2, 4, 2, 226.44, 0.00, 1),
(17, 2, 4, 1, 113.22, 0.00, 1),
(18, 2, 4, 1, 113.22, 0.00, 1),
(19, 2, 4, 1, 113.22, 0.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_address`
--

CREATE TABLE IF NOT EXISTS `order_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `company` varchar(250) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `street` varchar(250) NOT NULL,
  `cross_streets` varchar(250) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `extn` varchar(200) NOT NULL,
  `zip` varchar(200) NOT NULL,
  `delivery_instructions` text NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_billinginfo`
--

CREATE TABLE IF NOT EXISTS `order_billinginfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_name` text NOT NULL,
  `card_number` varchar(100) NOT NULL,
  `card_type` varchar(200) NOT NULL,
  `card_zip` varchar(150) NOT NULL,
  `expire_month` varchar(100) NOT NULL,
  `expire_year` varchar(100) NOT NULL,
  `uid` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `pickup_stores`
--

CREATE TABLE IF NOT EXISTS `pickup_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(200) NOT NULL,
  `address1` varchar(200) NOT NULL,
  `adress2` varchar(200) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `report_pin` varchar(100) NOT NULL,
  `restrict_ip` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pickup_stores`
--

INSERT INTO `pickup_stores` (`id`, `store_name`, `address1`, `adress2`, `zip`, `phone`, `uid`, `report_pin`, `restrict_ip`) VALUES
(1, 'Haight-Ashbury Store #1 ', '123 E', '12th Street, New York', 'NY 10003 ', '212-123-4567', 10, '', ''),
(2, 'Haight-Ashbury Store #2', '123 E F', '12th Street, New York', 'NY 10003 ', '212-123-4567', 10, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `post_data`
--

CREATE TABLE IF NOT EXISTS `post_data` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `fb_post_id` int(200) NOT NULL,
  `fb_post_msg` text NOT NULL,
  `fb_post_link` text NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_categories`
--

CREATE TABLE IF NOT EXISTS `sandwich_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` text NOT NULL,
  `category_identifier` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sandwich_categories`
--

INSERT INTO `sandwich_categories` (`id`, `category_name`, `category_identifier`) VALUES
(1, 'bread', 'BREAD'),
(2, 'protein', 'PROTEIN'),
(3, 'cheese', 'CHEESE'),
(4, 'toppings', 'TOPPINGS'),
(5, 'condiments', 'CONDIMENTS'),
(6, 'condiments', 'CONDIMENTS');

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_category_items`
--

CREATE TABLE IF NOT EXISTS `sandwich_category_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` text NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_image` varchar(250) NOT NULL,
  `category_id` int(10) NOT NULL,
  `options_id` varchar(100) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `sandwich_category_items`
--

INSERT INTO `sandwich_category_items` (`id`, `item_name`, `item_price`, `item_image`, `category_id`, `options_id`, `priority`) VALUES
(1, 'ITALIAN+HERO', 14.50, 'create-sandwich-bread.png', 1, '0', 12),
(2, 'SESAME HERO', 18.50, 'create-sandwich-bread.png', 1, '0', 2),
(3, 'CIABATTA', 22.00, 'create-sandwich-bread.png', 1, '0', 2),
(4, 'CROISSANT', 8.20, 'create-sandwich-bread.png', 1, '0', 12),
(5, 'RYE', 16.10, 'create-sandwich-bread.png', 1, '0', 12),
(6, 'WHITE', 10.50, 'create-sandwich-bread.png', 1, '0', 1),
(7, '9-GRAIN WHOLE WHEAT', 11.50, 'create-sandwich-bread.png', 1, '0', 1),
(8, 'GLUTEN-FREE', 5.50, 'create-sandwich-bread.png', 1, '0', 1),
(9, 'TURKEY', 15.00, 'create_protein.png', 2, '1:2', 0),
(10, 'HAM', 10.22, 'create_protein.png', 2, '0', 0),
(11, 'SALAMI', 18.22, 'create_protein.png', 2, '0', 0),
(12, 'ROAST BEEF', 6.22, 'create_protein.png', 2, '0', 0),
(13, 'PROSCIUTTO', 2.80, 'create_protein.png', 2, '0', 0),
(14, 'CORNED BEEF', 9.78, 'create_protein.png', 2, '1:2', 0),
(15, 'PASTRAMI', 7.66, 'create_protein.png', 2, '0', 0),
(16, 'CHICKEN SALAD', 12.00, 'create_protein.png', 2, '0', 0),
(17, 'CHEDDAR', 8.00, 'create_cheese.png', 3, '1:2', 0),
(18, 'SWISS', 10.00, 'create_cheese.png', 3, '0', 0),
(19, 'AMERICAN', 20.57, 'create_cheese.png', 3, '0', 0),
(20, 'PROVOLONE', 15.22, 'create_cheese.png', 3, '0', 0),
(21, 'MUENSTER', 18.78, 'create_cheese.png', 3, '0', 0),
(22, 'MOZZARELLA', 23.22, 'create_cheese.png', 3, '1:2', 0),
(23, 'BRIE', 15.00, 'create_cheese.png', 3, '0', 0),
(24, 'FETA', 6.22, 'create_cheese.png', 3, '0', 0),
(25, 'LETTUCE', 9.00, 'create_toppings.png', 4, '3:4', 0),
(26, 'TOMATO', 8.00, 'create_toppings.png', 4, '0', 0),
(27, 'RED ONION', 7.72, 'create_toppings.png', 4, '0', 0),
(28, 'CUCUMBER', 6.22, 'create_toppings.png', 4, '0', 0),
(29, 'PICKLES', 9.22, 'create_toppings.png', 4, '0', 0),
(30, 'JALAPENOS', 15.22, 'create_toppings.png', 4, '3:4', 0),
(31, 'AVOCADO', 17.00, 'create_toppings.png', 4, '0', 0),
(32, 'SAURKRAUT', 6.58, 'create_toppings.png', 4, '0', 0),
(33, 'MAYONNAISE', 8.00, 'create_condiments.png', 5, '3:4', 0),
(34, 'SPICY MAYONNAISE', 7.00, 'create_condiments.png', 5, '0', 0),
(35, 'SPICY BROWN MUSTARD', 12.00, 'create_condiments.png', 5, '0', 0),
(36, 'DIJON MUSTARD', 16.12, 'create_condiments.png', 5, '0', 0),
(37, 'HONEY CUP MUSTARD', 20.00, 'create_condiments.png', 5, '0', 0),
(38, 'RUSSIAN DRESSING', 17.58, 'create_condiments.png', 5, '3:4', 0),
(39, 'PESTO', 3.70, 'create_condiments.png', 5, '0', 0),
(40, 'THAI PEANUT', 5.54, 'create_condiments.png', 5, '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_item_options`
--

CREATE TABLE IF NOT EXISTS `sandwich_item_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_name` text NOT NULL,
  `option_unit` varchar(200) NOT NULL,
  `price_mult` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sandwich_item_options`
--

INSERT INTO `sandwich_item_options` (`id`, `option_name`, `option_unit`, `price_mult`) VALUES
(1, 'NORMAL (1.0)', '1.0', '1'),
(2, 'DOUBLE (2.0)', '2.0', '2'),
(3, 'NORMAL (N)', 'N', '1'),
(4, 'HEAVY (H)', 'H', '2');

-- --------------------------------------------------------

--
-- Table structure for table `standard_category`
--

CREATE TABLE IF NOT EXISTS `standard_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `standard_cat_name` text NOT NULL,
  `category_identifier` text NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `standard_category_products`
--

CREATE TABLE IF NOT EXISTS `standard_category_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` text NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `standard_category_id` int(11) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `standard_category_products`
--

INSERT INTO `standard_category_products` (`id`, `product_name`, `product_price`, `standard_category_id`, `priority`) VALUES
(1, 'Cobb+Salad', 12.00, 1, 5),
(2, 'Chef Salad', 32.00, 1, 8),
(3, 'Garlic Dill Pickle', 21.00, 2, 10),
(4, 'Honeycrisp Apple', 123.00, 2, 20),
(7, 'what+is', 87.00, 1, 13),
(8, 'nice+one', 123.00, 3, 2),
(9, 'good+q', 12.00, 4, 0),
(10, 'nothing+ya', 321.00, 5, 0),
(11, 'why', 123.00, 6, 5),
(12, 'some+ting', 21.00, 7, 0),
(13, 'test', 56.00, 8, 0),
(14, 'wefrdsfds', 5.20, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `store_timings`
--

CREATE TABLE IF NOT EXISTS `store_timings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(50) NOT NULL,
  `open` varchar(50) NOT NULL,
  `close` varchar(50) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `store_timings`
--

INSERT INTO `store_timings` (`id`, `day`, `open`, `close`, `store_id`) VALUES
(1, '', '12.00AM', '11.59PM', 0),
(2, '', '12.00AM', '11.59PM', 0),
(3, '', '', '', 0),
(4, '', '12.00AM', '11.59PM', 0),
(5, '', '12.00AM', '11.59PM', 0),
(6, '', '12.00AM', '11.59PM', 0),
(7, '', '12.00AM', '11.59PM', 0),
(8, 'Sunday', '12.00AM', '11.59PM', 0),
(9, 'Monday', '12.00AM', '11.59PM', 0),
(10, '', '', '', 0),
(11, '', '12.00AM', '11.59PM', 0),
(12, '', '12.00AM', '11.59PM', 0),
(13, '', '12.00AM', '11.59PM', 0),
(14, '', '12.00AM', '11.59PM', 0),
(15, '', '12.00AM', '11.59PM', 0),
(16, 'Monday', '12.00AM', '11.59PM', 0),
(17, '', '', '', 0),
(18, '', '12.00AM', '11.59PM', 0),
(19, '', '12.00AM', '11.59PM', 0),
(20, '', '12.00AM', '11.59PM', 0),
(21, '', '11.59PM', '11.59PM', 0),
(22, '', '12.00AM', '11.59PM', 0),
(23, '', '12.00AM', '11.59PM', 0),
(24, '', '', '', 0),
(25, '', '12.00AM', '11.59PM', 0),
(26, '', '12.00AM', '11.59PM', 0),
(27, '', '12.00AM', '11.59PM', 0),
(28, '', '11.59PM', '11.59PM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `store_zipcodes`
--

CREATE TABLE IF NOT EXISTS `store_zipcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zipcode` int(11) NOT NULL,
  `abbreviation` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fb_id` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(250) CHARACTER SET utf8 NOT NULL,
  `fb_auth_token` varchar(250) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `fb_id`, `password`, `fb_auth_token`, `first_name`, `last_name`) VALUES
(4, 'vasanth@office-partners360.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 'Vasanth', 'Kumar');

-- --------------------------------------------------------

--
-- Table structure for table `user_sandwich_data`
--

CREATE TABLE IF NOT EXISTS `user_sandwich_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(20) NOT NULL,
  `sandwich_name` text NOT NULL,
  `sandwich_data` longtext NOT NULL,
  `sandwich_price` decimal(10,2) NOT NULL,
  `date_of_creation` datetime NOT NULL,
  `is_public` int(20) DEFAULT NULL,
  `menu_is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_sandwich_data`
--

INSERT INTO `user_sandwich_data` (`id`, `uid`, `sandwich_name`, `sandwich_data`, `sandwich_price`, `date_of_creation`, `is_public`, `menu_is_active`) VALUES
(2, 4, 'custom sandwich 2', '{"BREAD":{"item_name":["CIABATTA"],"item_price":["22.00"],"item_qty":{},"item_id":[3],"item_image":["create-sandwich-bread.png"]},"PROTEIN":{"item_name":["CORNED BEEF","ROAST BEEF","SALAMI","HAM"],"item_price":[9.78,6.22,18.22,10.22],"item_qty":{"CORNED BEEF":["NORMAL (1.0)","1.0",1,""]},"item_id":[14,12,11,10],"item_image":["create_protein.png","create_protein.png","create_protein.png","create_protein.png"]},"CHEESE":{"item_name":["PROVOLONE"],"item_price":[15.22],"item_qty":{},"item_id":[20],"item_image":["create_cheese.png"]},"TOPPINGS":{"item_name":["PICKLES","CUCUMBER"],"item_price":[9.22,6.22],"item_qty":{},"item_id":[29,28],"item_image":["create_toppings.png","create_toppings.png"]},"CONDIMENTS":{"item_name":["DIJON MUSTARD"],"item_price":[16.12],"item_qty":{},"item_id":[36],"item_image":["create_condiments.png"]}}', 113.22, '2014-12-19 17:15:15', 1, 1),
(3, 4, 'custom sandwich 3', '{"BREAD":{"item_name":["CIABATTA"],"item_price":["22.00"],"item_qty":{},"item_id":[3],"item_image":["create-sandwich-bread.png"]},"PROTEIN":{"item_name":["PROSCIUTTO","ROAST BEEF"],"item_price":["2.80",6.22],"item_qty":{},"item_id":[13,12],"item_image":["create_protein.png","create_protein.png"]},"CHEESE":{"item_name":["MUENSTER"],"item_price":[18.78],"item_qty":{},"item_id":[21],"item_image":["create_cheese.png"]},"TOPPINGS":{"item_name":["PICKLES"],"item_price":[9.22],"item_qty":{},"item_id":[29],"item_image":["create_toppings.png"]},"CONDIMENTS":{"item_name":[],"item_price":[],"item_qty":{},"item_id":[],"item_image":[]}}', 59.02, '2014-12-19 17:15:51', 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
