-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 26, 2014 at 02:44 PM
-- Server version: 5.5.40
-- PHP Version: 5.4.34-1+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `standard_category_products`
--

CREATE TABLE IF NOT EXISTS `standard_category_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` text NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `standard_category_id` int(11) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `standard_category_products`
--

INSERT INTO `standard_category_products` (`id`, `product_name`, `product_price`, `standard_category_id`, `priority`) VALUES
(1, 'Cobb+Salad', 12.00, 1, 5),
(2, 'Chef Salad', 32.00, 1, 8),
(3, 'Garlic Dill Pickle', 21.00, 2, 10),
(4, 'Honeycrisp Apple', 123.00, 2, 20),
(7, 'what+is', 87.00, 1, 13),
(8, 'nice+one', 123.00, 3, 2),
(9, 'good+q', 12.00, 4, 0),
(10, 'nothing+ya', 321.00, 5, 0),
(11, 'why', 123.00, 6, 5),
(12, 'some+ting', 21.00, 7, 0),
(13, 'test', 56.00, 8, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
