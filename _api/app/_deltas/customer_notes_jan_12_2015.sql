DROP TABLE IF EXISTS `customer_notes`;
CREATE TABLE `customer_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `customer_notes` (`id`, `user_id`, `date`, `notes`) VALUES
(1,	4,	'2015-01-12',	'This is my first comment.');
