-- phpMyAdmin SQL Dump
-- version 4.0.10.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 07, 2015 at 02:38 PM
-- Server version: 5.5.40-0ubuntu0.12.04.1
-- PHP Version: 5.3.10-1ubuntu3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `webpages_data`
--

CREATE TABLE IF NOT EXISTS `webpages_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webpage_name` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `webpages_data`
--

INSERT INTO `webpages_data` (`id`, `webpage_name`, `text`, `status`) VALUES
(1, 'Locations', '<p>test</p>\r\n', 0),
(2, 'ABOUT US', '<p><em><strong><img alt="" src="http://admin.ha/upload/20150106_111836.jpg" style="height:177px; width:284px" />About Us About UsAbout UsAbout UsAbout UsAbout UsAbout UsAbout Us</strong></em></p>\r\n', 0),
(3, 'CATERING', '<p>Catering&nbsp;Catering</p>\r\n\r\n<p>Catering</p>\r\n\r\n<p>CateringCatering</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>CateringCateringCatering</p>\r\n', 0),
(4, 'JOBS', '<p>Job</p>\r\n', 0),
(7, 'CONTACT', '<p>Contact</p>\r\n', 0),
(8, 'FEEDBACK', 'FEEDBACK', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
