DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `billinginfo_id` int(12) NOT NULL,
  `order_item_id` varchar(200) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `tip` decimal(10,2) NOT NULL,
  `coupon_code` varchar(250) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `delivery` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
