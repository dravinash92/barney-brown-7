-- phpMyAdmin SQL Dump
-- version 4.0.10.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 13, 2015 at 06:34 PM
-- Server version: 5.5.40-0ubuntu0.12.04.1
-- PHP Version: 5.4.34-1+deb.sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `admin_cat_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`uid`, `admin_cat_id`, `first_name`, `last_name`, `user_name`, `password`) VALUES
(1, 1, 'Vasanth1', 'Kumar', 'vasanth@office-partners360.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_category`
--

CREATE TABLE IF NOT EXISTS `admin_user_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `cat_privilege` int(11) NOT NULL DEFAULT '0',
  `cat_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin_user_category`
--

INSERT INTO `admin_user_category` (`id`, `cat_name`, `cat_privilege`, `cat_status`) VALUES
(1, 'ADMIN', 0, 0),
(2, 'CUSTOMER SERVICE', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_notes`
--

CREATE TABLE IF NOT EXISTS `customer_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `is_live` tinyint(1) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `discount_amount` int(11) NOT NULL,
  `applicable_to` tinyint(1) NOT NULL,
  `uses_type` tinyint(1) NOT NULL,
  `uses_amount` int(11) NOT NULL,
  `minimum_order` tinyint(1) NOT NULL,
  `minimum_order_amount` int(11) NOT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friend_data`
--

CREATE TABLE IF NOT EXISTS `friend_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friend_uid` int(200) NOT NULL,
  `friend_name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `billinginfo_id` int(12) NOT NULL,
  `order_item_id` varchar(200) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `tip` decimal(10,2) NOT NULL,
  `coupon_code` varchar(250) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `delivery` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders_items`
--

CREATE TABLE IF NOT EXISTS `orders_items` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_type` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_qty` int(11) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_tax` decimal(10,2) NOT NULL,
  `ordered` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_address`
--

CREATE TABLE IF NOT EXISTS `order_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `company` varchar(250) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `street` varchar(250) NOT NULL,
  `cross_streets` varchar(250) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `extn` varchar(200) NOT NULL,
  `zip` varchar(200) NOT NULL,
  `delivery_instructions` text NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_billinginfo`
--

CREATE TABLE IF NOT EXISTS `order_billinginfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_name` text NOT NULL,
  `card_number` varchar(100) NOT NULL,
  `card_type` varchar(200) NOT NULL,
  `card_zip` varchar(150) NOT NULL,
  `expire_month` varchar(100) NOT NULL,
  `expire_year` varchar(100) NOT NULL,
  `uid` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  `code` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `label`, `code`, `status`) VALUES
(1, 'PROCESSED', 'pickup_prc', 0),
(2, 'BAGGED/READY', 'pickup_bag', 0),
(3, 'PICKED UP', 'pickup_out', 0),
(4, 'CANCEL', 'pickup_cancel', 0),
(5, 'PROCESSED', 'Delivery_Pro', 1),
(6, 'OUT FOR DELIVERY', 'Delivery_out', 1),
(7, 'DELIVERED', 'Delivery_del', 1),
(8, 'CANCEL', 'Delivery_Cancel', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pickup_stores`
--

CREATE TABLE IF NOT EXISTS `pickup_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(200) NOT NULL,
  `address1` varchar(200) NOT NULL,
  `adress2` varchar(200) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `report_pin` varchar(100) NOT NULL,
  `restrict_ip` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pickup_stores`
--

INSERT INTO `pickup_stores` (`id`, `store_name`, `address1`, `adress2`, `zip`, `phone`, `uid`, `report_pin`, `restrict_ip`) VALUES
(2, 'Haight-Ashbury Store #2', '123 E F', '12th Street, New York', 'NY 10003 ', '212-123-4567', 10, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `post_data`
--

CREATE TABLE IF NOT EXISTS `post_data` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `fb_post_id` int(200) NOT NULL,
  `fb_post_msg` text NOT NULL,
  `fb_post_link` text NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_categories`
--

CREATE TABLE IF NOT EXISTS `sandwich_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` text NOT NULL,
  `category_identifier` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sandwich_categories`
--

INSERT INTO `sandwich_categories` (`id`, `category_name`, `category_identifier`) VALUES
(1, 'bread', 'BREAD'),
(2, 'protein', 'PROTEIN'),
(3, 'cheese', 'CHEESE'),
(4, 'toppings', 'TOPPINGS'),
(5, 'condiments', 'CONDIMENTS'),
(6, 'condiments', 'CONDIMENTS');

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_category_items`
--

CREATE TABLE IF NOT EXISTS `sandwich_category_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` text NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_image` varchar(250) NOT NULL,
  `category_id` int(10) NOT NULL,
  `options_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `sandwich_category_items`
--

INSERT INTO `sandwich_category_items` (`id`, `item_name`, `item_price`, `item_image`, `category_id`, `options_id`) VALUES
(1, 'ITALIAN HERO', 14.50, 'create-sandwich-bread.png', 1, ''),
(2, 'SESAME HERO', 18.50, 'create-sandwich-bread.png', 1, '0'),
(3, 'CIABATTA', 22.00, 'create-sandwich-bread.png', 1, '0'),
(4, 'CROISSANT', 8.20, 'create-sandwich-bread.png', 1, '0'),
(5, 'RYE', 16.10, 'create-sandwich-bread.png', 1, '0'),
(6, 'WHITE', 10.50, 'create-sandwich-bread.png', 1, '0'),
(7, '9-GRAIN WHOLE WHEAT', 11.50, 'create-sandwich-bread.png', 1, '0'),
(8, 'GLUTEN-FREE', 5.50, 'create-sandwich-bread.png', 1, '0'),
(9, 'TURKEY', 15.00, 'create_protein.png', 2, '1:2'),
(10, 'HAM', 10.22, 'create_protein.png', 2, '0'),
(11, 'SALAMI', 18.22, 'create_protein.png', 2, '0'),
(12, 'ROAST BEEF', 6.22, 'create_protein.png', 2, '0'),
(13, 'PROSCIUTTO', 2.80, 'create_protein.png', 2, '0'),
(14, 'CORNED BEEF', 9.78, 'create_protein.png', 2, '1:2'),
(15, 'PASTRAMI', 7.66, 'create_protein.png', 2, '0'),
(16, 'CHICKEN SALAD', 12.00, 'create_protein.png', 2, '0'),
(17, 'CHEDDAR', 8.00, 'create_cheese.png', 3, '1:2'),
(18, 'SWISS', 10.00, 'create_cheese.png', 3, '0'),
(19, 'AMERICAN', 20.57, 'create_cheese.png', 3, '0'),
(20, 'PROVOLONE', 15.22, 'create_cheese.png', 3, '0'),
(21, 'MUENSTER', 18.78, 'create_cheese.png', 3, '0'),
(22, 'MOZZARELLA', 23.22, 'create_cheese.png', 3, '1:2'),
(23, 'BRIE', 15.00, 'create_cheese.png', 3, '0'),
(24, 'FETA', 6.22, 'create_cheese.png', 3, '0'),
(25, 'LETTUCE', 9.00, 'create_toppings.png', 4, '3:4'),
(26, 'TOMATO', 8.00, 'create_toppings.png', 4, '0'),
(27, 'RED ONION', 7.72, 'create_toppings.png', 4, '0'),
(28, 'CUCUMBER', 6.22, 'create_toppings.png', 4, '0'),
(29, 'PICKLES', 9.22, 'create_toppings.png', 4, '0'),
(30, 'JALAPENOS', 15.22, 'create_toppings.png', 4, '3:4'),
(31, 'AVOCADO', 17.00, 'create_toppings.png', 4, '0'),
(32, 'SAURKRAUT', 6.58, 'create_toppings.png', 4, '0'),
(33, 'MAYONNAISE', 8.00, 'create_condiments.png', 5, '3:4'),
(34, 'SPICY MAYONNAISE', 7.00, 'create_condiments.png', 5, '0'),
(35, 'SPICY BROWN MUSTARD', 12.00, 'create_condiments.png', 5, '0'),
(36, 'DIJON MUSTARD', 16.12, 'create_condiments.png', 5, '0'),
(37, 'HONEY CUP MUSTARD', 20.00, 'create_condiments.png', 5, '0'),
(38, 'RUSSIAN DRESSING', 17.58, 'create_condiments.png', 5, '3:4'),
(39, 'PESTO', 3.70, 'create_condiments.png', 5, '0'),
(40, 'THAI PEANUT', 5.54, 'create_condiments.png', 5, '0'),
(41, '', 0.00, 'http://admin.ha/upload/20150113_062548.', 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_item_options`
--

CREATE TABLE IF NOT EXISTS `sandwich_item_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_name` text NOT NULL,
  `option_unit` varchar(200) NOT NULL,
  `price_mult` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sandwich_item_options`
--

INSERT INTO `sandwich_item_options` (`id`, `option_name`, `option_unit`, `price_mult`) VALUES
(1, 'NORMAL (1.0)', '1.0', '1'),
(2, 'DOUBLE (2.0)', '2.0', '2'),
(3, 'NORMAL (N)', 'N', '1'),
(4, 'HEAVY (H)', 'H', '2');

-- --------------------------------------------------------

--
-- Table structure for table `standard_category`
--

CREATE TABLE IF NOT EXISTS `standard_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `standard_cat_name` text NOT NULL,
  `category_identifier` text NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `standard_category`
--

INSERT INTO `standard_category` (`id`, `standard_cat_name`, `category_identifier`, `priority`) VALUES
(1, 'salads', 'SALADS', 1),
(2, 'SIDES', 'SIDES', 2),
(3, 'test', 'TEST', 0);

-- --------------------------------------------------------

--
-- Table structure for table `standard_category_products`
--

CREATE TABLE IF NOT EXISTS `standard_category_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` text NOT NULL,
  `description` text NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `standard_category_id` int(11) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `standard_category_products`
--

INSERT INTO `standard_category_products` (`id`, `product_name`, `description`, `product_price`, `standard_category_id`, `priority`) VALUES
(2, 'Chef Salad', '', 32.00, 1, 2),
(3, 'Garlic Dill Pickle', '', 21.00, 2, 1),
(4, 'Honeycrisp Apple', '', 123.00, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `store_timings`
--

CREATE TABLE IF NOT EXISTS `store_timings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(50) NOT NULL,
  `open` varchar(50) NOT NULL,
  `close` varchar(50) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `store_timings`
--

INSERT INTO `store_timings` (`id`, `day`, `open`, `close`, `store_id`) VALUES
(1, '', '12.00AM', '11.59PM', 0),
(2, '', '12.00AM', '11.59PM', 0),
(3, '', '', '', 0),
(4, '', '12.00AM', '11.59PM', 0),
(5, '', '12.00AM', '11.59PM', 0),
(6, '', '12.00AM', '11.59PM', 0),
(7, '', '12.00AM', '11.59PM', 0),
(8, 'Sunday', '12.00AM', '11.59PM', 0),
(9, 'Monday', '12.00AM', '11.59PM', 0),
(10, '', '', '', 0),
(11, '', '12.00AM', '11.59PM', 0),
(12, '', '12.00AM', '11.59PM', 0),
(13, '', '12.00AM', '11.59PM', 0),
(14, '', '12.00AM', '11.59PM', 0),
(15, '', '12.00AM', '11.59PM', 0),
(16, 'Monday', '12.00AM', '11.59PM', 0),
(17, '', '', '', 0),
(18, '', '12.00AM', '11.59PM', 0),
(19, '', '12.00AM', '11.59PM', 0),
(20, '', '12.00AM', '11.59PM', 0),
(21, '', '11.59PM', '11.59PM', 0),
(22, '', '12.00AM', '11.59PM', 0),
(23, '', '12.00AM', '11.59PM', 0),
(24, '', '', '', 0),
(25, '', '12.00AM', '11.59PM', 0),
(26, '', '12.00AM', '11.59PM', 0),
(27, '', '12.00AM', '11.59PM', 0),
(28, '', '11.59PM', '11.59PM', 0),
(29, '', '12.00AM', '11.59PM', 0),
(30, 'Monday', '12.00AM', '11.59PM', 0),
(31, '', '', '', 0),
(32, 'Tuesday', '12.00AM', '11.59PM', 0),
(33, '', '12.00AM', '11.59PM', 0),
(34, '', '12.00AM', '11.59PM', 0),
(35, '', '11.59PM', '11.59PM', 0),
(36, 'Sunday', '12.00AM', '11.59PM', 0),
(37, 'Monday', '12.00AM', '11.59PM', 0),
(38, '', '', '', 0),
(39, '', '12.00AM', '11.59PM', 0),
(40, '', '12.00AM', '11.59PM', 0),
(41, '', '12.00AM', '11.59PM', 0),
(42, '', '12.00AM', '11.59PM', 0),
(43, 'Sunday', '12.00AM', '11.59PM', 0),
(44, 'Monday', '12.00AM', '11.59PM', 0),
(45, '', '', '', 0),
(46, 'Tuesday', '12.00AM', '11.59PM', 0),
(47, 'Wednesday', '12.00AM', '11.59PM', 0),
(48, '', '12.00AM', '11.59PM', 0),
(49, '', '11.59PM', '11.59PM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `store_zipcodes`
--

CREATE TABLE IF NOT EXISTS `store_zipcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zipcode` int(11) NOT NULL,
  `abbreviation` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fb_id` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(250) CHARACTER SET utf8 NOT NULL,
  `fb_auth_token` varchar(250) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `fb_id`, `password`, `fb_auth_token`, `first_name`, `last_name`) VALUES
(4, 'vasanth@office-partners360.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 'Vasanth', 'Kumar');

-- --------------------------------------------------------

--
-- Table structure for table `user_sandwich_data`
--

CREATE TABLE IF NOT EXISTS `user_sandwich_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(20) NOT NULL,
  `sandwich_name` text NOT NULL,
  `description` text NOT NULL,
  `sandwich_data` longtext NOT NULL,
  `sandwich_price` decimal(10,2) NOT NULL,
  `date_of_creation` datetime NOT NULL,
  `is_public` int(20) DEFAULT NULL,
  `menu_is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `webpages_data`
--

CREATE TABLE IF NOT EXISTS `webpages_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webpage_name` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `webpages_data`
--

INSERT INTO `webpages_data` (`id`, `webpage_name`, `text`, `status`) VALUES
(1, 'Locations', '<p>test</p>\r\n', 0),
(2, 'ABOUT US', '<p><em><strong><img alt="" src="http://admin.ha/upload/20150106_111836.jpg" style="height:177px; width:284px" />About Us About UsAbout UsAbout UsAbout UsAbout UsAbout UsAbout Us</strong></em></p>\r\n', 0),
(3, 'CATERING', '<p>Catering&nbsp;Catering</p>\r\n\r\n<p>Catering</p>\r\n\r\n<p>CateringCatering</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>CateringCateringCatering</p>\r\n', 0),
(4, 'JOBS', '<p>Job</p>\r\n', 0),
(7, 'CONTACT', '<p>Contact</p>\r\n', 0),
(8, 'FEEDBACK', 'FEEDBACK', 0);

-- --------------------------------------------------------

--
-- Table structure for table `webpages_homepage_data`
--

CREATE TABLE IF NOT EXISTS `webpages_homepage_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(250) NOT NULL,
  `banner_type` int(10) NOT NULL,
  `image` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `priority` int(10) NOT NULL,
  `banner_status` int(11) NOT NULL,
  `redirection_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
