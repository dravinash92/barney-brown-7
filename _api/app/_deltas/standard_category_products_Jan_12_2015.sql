DROP TABLE IF EXISTS `standard_category_products`;
CREATE TABLE `standard_category_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` text NOT NULL,
  `description` text NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `standard_category_id` int(11) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
