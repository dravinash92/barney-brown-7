-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 26, 2014 at 04:29 PM
-- Server version: 5.5.38-0ubuntu0.12.04.1
-- PHP Version: 5.3.10-1ubuntu3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `sandwich_category_items`
--

CREATE TABLE IF NOT EXISTS `sandwich_category_items` (
`id` int(11) NOT NULL,
  `item_name` text NOT NULL,
  `priority` int(11) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_image` varchar(250) NOT NULL,
  `category_id` int(10) NOT NULL,
  `options_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sandwich_category_items`
--

INSERT INTO `sandwich_category_items` (`id`, `item_name`, `priority`, `item_price`, `item_image`, `category_id`, `options_id`) VALUES
(1, 'ITALIANB%2BHERO', 1, 14.55, 'create-sandwich-bread.png', 1, '0'),
(2, 'SESAME+HERO', 3, 18.50, 'create-sandwich-bread.png', 1, '0'),
(3, 'CIABATTA', 3, 22.00, 'create-sandwich-bread.png', 1, '0'),
(4, 'CROISSANT', 4, 8.20, 'create-sandwich-bread.png', 1, '0'),
(5, 'RYE', 5, 16.10, 'create-sandwich-bread.png', 1, '0'),
(6, 'WHITE', 6, 10.50, 'create-sandwich-bread.png', 1, '0'),
(7, '9-GRAIN WHOLE WHEAT', 7, 11.50, 'create-sandwich-bread.png', 1, '0'),
(8, 'GLUTEN-FREE', 8, 5.50, 'create-sandwich-bread.png', 1, '0'),
(9, 'TURKEY', 1, 15.00, 'create_protein.png', 2, '1:2'),
(10, 'HAM', 0, 10.22, 'create_protein.png', 2, '0'),
(11, 'SALAMI', 0, 18.22, 'create_protein.png', 2, '0'),
(12, 'ROAST BEEF', 0, 6.22, 'create_protein.png', 2, '0'),
(13, 'PROSCIUTTO', 0, 2.80, 'create_protein.png', 2, '0'),
(14, 'CORNED BEEF', 2, 9.78, 'create_protein.png', 2, '1:2'),
(15, 'PASTRAMI', 0, 7.66, 'create_protein.png', 2, '0'),
(16, 'CHICKEN SALAD', 0, 12.00, 'create_protein.png', 2, '0'),
(17, 'CHEDDAR', 0, 8.00, 'create_cheese.png', 3, '1:2'),
(18, 'SWISS', 0, 10.00, 'create_cheese.png', 3, '0'),
(19, 'AMERICAN', 0, 20.57, 'create_cheese.png', 3, '0'),
(20, 'PROVOLONE', 0, 15.22, 'create_cheese.png', 3, '0'),
(21, 'MUENSTER', 0, 18.78, 'create_cheese.png', 3, '0'),
(22, 'MOZZARELLA', 2, 23.22, 'create_cheese.png', 3, '1:2'),
(23, 'BRIE', 0, 15.00, 'create_cheese.png', 3, '0'),
(24, 'FETA', 0, 6.22, 'create_cheese.png', 3, '0'),
(25, 'LETTUCE', 0, 9.00, 'create_toppings.png', 4, '3:4'),
(26, 'TOMATO', 0, 8.00, 'create_toppings.png', 4, '0'),
(27, 'RED ONION', 0, 7.72, 'create_toppings.png', 4, '0'),
(28, 'CUCUMBER', 1, 6.22, 'create_toppings.png', 4, '0'),
(29, 'PICKLES', 0, 9.22, 'create_toppings.png', 4, '0'),
(30, 'JALAPENOS', 0, 15.22, 'create_toppings.png', 4, '3:4'),
(31, 'AVOCADO', 0, 17.00, 'create_toppings.png', 4, '0'),
(32, 'SAURKRAUT', 0, 6.58, 'create_toppings.png', 4, '0'),
(33, 'MAYONNAISE', 0, 8.00, 'create_condiments.png', 5, '3:4'),
(34, 'SPICY MAYONNAISE', 0, 7.00, 'create_condiments.png', 5, '0'),
(35, 'SPICY BROWN MUSTARD', 0, 12.00, 'create_condiments.png', 5, '0'),
(36, 'DIJON MUSTARD', 0, 16.12, 'create_condiments.png', 5, '0'),
(37, 'HONEY CUP MUSTARD', 0, 20.00, 'create_condiments.png', 5, '0'),
(38, 'RUSSIAN DRESSING', 0, 17.58, 'create_condiments.png', 5, '3:4'),
(39, 'PESTO', 0, 3.70, 'create_condiments.png', 5, '0'),
(40, 'THAI PEANUT', 0, 5.54, 'create_condiments.png', 5, '0'),
(66, 'testing+cheese', 0, 123.00, 'http://192.168.1.81/hashbury_admin/upload/20141222_063006.png', 3, NULL),
(67, 'testing1111', 0, 123.00, 'http://192.168.1.81/hashbury_admin/upload/20141222_063125.png', 2, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sandwich_category_items`
--
ALTER TABLE `sandwich_category_items`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sandwich_category_items`
--
ALTER TABLE `sandwich_category_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
