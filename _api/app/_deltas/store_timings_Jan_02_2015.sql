-- phpMyAdmin SQL Dump
-- version 4.0.10.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2015 at 06:52 PM
-- Server version: 5.5.40-0ubuntu0.12.04.1
-- PHP Version: 5.4.34-1+deb.sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `store_timings`
--

CREATE TABLE IF NOT EXISTS `store_timings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(50) NOT NULL,
  `open` varchar(50) NOT NULL,
  `close` varchar(50) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `store_timings`
--

INSERT INTO `store_timings` (`id`, `day`, `open`, `close`, `store_id`) VALUES
(1, '', '12.00AM', '11.59PM', 0),
(2, '', '12.00AM', '11.59PM', 0),
(3, '', '', '', 0),
(4, '', '12.00AM', '11.59PM', 0),
(5, '', '12.00AM', '11.59PM', 0),
(6, '', '12.00AM', '11.59PM', 0),
(7, '', '12.00AM', '11.59PM', 0),
(8, 'Sunday', '12.00AM', '11.59PM', 0),
(9, 'Monday', '12.00AM', '11.59PM', 0),
(10, '', '', '', 0),
(11, '', '12.00AM', '11.59PM', 0),
(12, '', '12.00AM', '11.59PM', 0),
(13, '', '12.00AM', '11.59PM', 0),
(14, '', '12.00AM', '11.59PM', 0),
(15, '', '12.00AM', '11.59PM', 0),
(16, 'Monday', '12.00AM', '11.59PM', 0),
(17, '', '', '', 0),
(18, '', '12.00AM', '11.59PM', 0),
(19, '', '12.00AM', '11.59PM', 0),
(20, '', '12.00AM', '11.59PM', 0),
(21, '', '11.59PM', '11.59PM', 0),
(22, '', '12.00AM', '11.59PM', 0),
(23, '', '12.00AM', '11.59PM', 0),
(24, '', '', '', 0),
(25, '', '12.00AM', '11.59PM', 0),
(26, '', '12.00AM', '11.59PM', 0),
(27, '', '12.00AM', '11.59PM', 0),
(28, '', '11.59PM', '11.59PM', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
