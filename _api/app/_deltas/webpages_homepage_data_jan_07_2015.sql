-- phpMyAdmin SQL Dump
-- version 4.0.10.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 07, 2015 at 02:39 PM
-- Server version: 5.5.40-0ubuntu0.12.04.1
-- PHP Version: 5.3.10-1ubuntu3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `webpages_homepage_data`
--

CREATE TABLE IF NOT EXISTS `webpages_homepage_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(250) NOT NULL,
  `banner_type` int(10) NOT NULL,
  `image` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `priority` int(10) NOT NULL,
  `banner_status` int(11) NOT NULL,
  `redirection_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `webpages_homepage_data`
--

INSERT INTO `webpages_homepage_data` (`id`, `banner_name`, `banner_type`, `image`, `text`, `priority`, `banner_status`, `redirection_status`) VALUES
(1, 'fruits test', 1, 'http://admin.ha/upload/20150106_111758.jpeg', 'u r doining', 1, 1, 1),
(2, 'fruits', 2, 'http://admin.ha/upload/20150106_111836.jpg', 'testing', 2, 1, 0),
(3, 'cake', 3, 'http://admin.ha/upload/20150106_114442.jpg', 'cake is awesome', 0, 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
