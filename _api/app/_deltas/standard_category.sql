-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 26, 2014 at 02:43 PM
-- Server version: 5.5.40
-- PHP Version: 5.4.34-1+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hashbury`
--

-- --------------------------------------------------------

--
-- Table structure for table `standard_category`
--

CREATE TABLE IF NOT EXISTS `standard_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `standard_cat_name` text NOT NULL,
  `category_identifier` text NOT NULL,
  `priority` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `standard_category`
--

INSERT INTO `standard_category` (`id`, `standard_cat_name`, `category_identifier`, `priority`) VALUES
(1, 'salads', 'SALADS', 1),
(2, 'SIDES', 'SIDES', 2),
(3, 'test', 'TEST', 0),
(4, 'test+test', 'TEST1', 0),
(5, 'testing', 'Testing', 0),
(6, '', '', 0),
(7, '', '', 0),
(8, 'Test', 'Test', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
