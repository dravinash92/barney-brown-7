<?php
class customException extends Exception
{
	private $error_code;
	private $error_text;
	private $extra;
	
	public function __construct($error_code, $error_text = null, $extra = null )
	{
		// First take our starting vars and save them
		$this->error_code = $error_code;
		$this->error_text = $error_text;
		$this->extra = $extra;

		$this->parseConstructor(  );	// takes this->error_code and error_text, and sort of makes them overridable.  May swap them around...
		$this->parseExtra( );			// takes extra and parses it out, for arrray, object string and empty

		// this takes our internal defines and writes the default class stuff...
		$this->code = $this->error_code;
		$this->message = $this->error_text;

		$this->translation(  );
	}

	protected function translation(  )
	{
		// Override this if you want to mess with the constructor but not rewrite the whole damn thing.  It will fire last thing, after the error_code and error_text are assigned to the defaults.  So remember, if you so desire to set $this->code and $this->message if you mess with error_code and error_message.
	}

	// >> OVERRIDE and PRIMARY GETTERS
	// Use these in your application.  They are for printing and retrieving the needed information either in JSON, array, or as a string.  
	// they are setup to work out of the box but feel free to override them...
	public function printError(  )
	{
		echo $this->errorMessage(  );
		exit(  );
	}

	public function printErrorJson(  )
	{
		echo $this->getErrorJson( true );	// true to turn on pretty print
		exit(  );
	}

	public function getErrorJson(  )
	{
		return $this->buildErrorJson( true );
	}

	public function getError(  )
	{
		return $this->errorMessage( );
	}


	public function printErrorArray(  )
	{
		print_r( $this->buildErrorArray(  ) );
		die;
	}

	public function getErrorArray(  )
	{
		return $this->buildErrorArray(  );
	}


	// This is mostly routine stuff
	protected function parseExtra( )
	{
		$extra =  $this->extra;
				// saving extra which can be anything from the post array to just a string saying hello world
		if( $extra === null ) $this->extra = '';
		else
		{
			if( is_array( $extra ) )
				$this->extra = var_export( $extra, true );
			else if( is_object( $extra ) )
				$this->extra = var_export( get_object_vars( $extra, true ) , true );
			else
				$this->extra = $extra;
		}
	}

	protected function parseConstructor(  )
	{
		$error_code = $this->error_code;
		$error_text = $this->error_text;

		if( is_numeric( $error_code ) && !is_numeric( $error_text ))	// error( 12345, "this is an error" );
		{
			$this->error_code 	= $error_code;
			$this->error_text 	= $error_text;
		}
		else if( !is_numeric( $error_code ) && is_numeric( $error_text ) )  // error( "this is an error", 12345 );
		{
			$this->error_code 	= $error_code;
			$this->error_text 	= $error_text;
		}
		else if(  !is_numeric( $error_code ) && empty( $error_text ) )   // error( "this is an error" );
		{
			$this->error_text 	= $error_code;
			$this->error_code  = 99999;			// unknown
		}
		else if( is_numeric( $error_code )  && !empty( $error_text )  )		// error( 12345 );
		{
			$this->error_code  = $error_code;
			$this->error_text 	="GENERIC ERROR";
		}
		else
		{
			if( empty( $error_code ) && empty( $error_text ) )			// error(  )
			{
				$this->error_code  = 99999;	
				$this->error_text 	="GENERIC ERROR";
			}
			else // who knows
			{
				$this->error_code  = $error_code;	
				$this->error_text  = $error_text ;
			}
		}
	}

	public function getExtra( )
	{
		return $this->extra;
	}
	
	public function buildErrorArray(  )
	{
		 return array( 
		 	"error_code"=>$this->getCode(),  
		 	"message"=>$this->getMessage(), 
		 	"extra"=>$this->getExtra( ), 
		 	"caller"=>$this->get_caller(  ), 
		 	"location"=>$this->getFile(  ) .":".$this->getLine(  ),
		 	"trace"=>$this->getTraceAsString()  );
	}

	private function get_caller(  )
	{
		$backtrace = $this->getTrace(  );
		return $backtrace[0]['function'] ;
	}

	public function errorString(  )
	{
		return $this->error_text;
	}

	public function errorCode(  )
	{
		return $this->error_code;
	}

	public function setErrorCode( $newCode )	// this will be seperate from the actual error code, this is like for status code, like 500 = always an error.
	{
		$this->error_code = $newCode;
	}

	public function errorJson( $prettyPrint = false, $statusCodeOverride = null  )
	{	
		echo $this->buildErrorJson( $prettyPrint, $statusCodeOverride );
		exit(  );
	}

	public function buildErrorJson( $prettyPrint = false, $statusCodeOverride = null )
	{
		if( $statusCodeOverride ) $this->setErrorCode( $statusCodeOverride );
		if( $prettyPrint == true )
			$encoded = json_encode( array( "status_code"=>$this->error_code, "data"=>$this->buildErrorArray(  ) ), JSON_PRETTY_PRINT );
		else
			$encoded = json_encode( array( "status_code"=>$this->error_code, "data"=>$this->buildErrorArray(  ) ) );

		return $encoded;
	}

	public function errorTrace() {
		return $this->getTraceAsString();
	}

	public function errorMessage() {
		$extra = $this->getExtra();
		if( $extra ) {
			$extra = " - $extra \n";
		}
		else {
			$extra = "\n";
		}
		$code = $this->getCode(  );
		$msg =  $this->getMessage( );
        $trace = $this->getTraceAsString();

		$r =  "$code: $msg $extra$trace";
		return  $r;
	}
	
	public function dumpError( $exitAfter = false )
	{
		print "<pre>\n";
		print_r( $this->getErrorArray() );
		print "</pre>";
		if( $exitAfter == true )die;
	}

	protected  function get_calling_function( $n ) {
		// a funciton x has called a function y which called this
		// see stackoverflow.com/questions/190421
		$caller = debug_backtrace();
		$caller = $caller[$n];
		$r = $caller['function'] . ':' . $caller['line'];
		
		if ( isset($caller['class'] ) ) {
		  $r .= ' in ' . $caller['class'];
		}
		
		if ( isset( $caller['object'] ) ) {
		  $r .= ' (' . get_class($caller['object']) . ')';
		}
		return $r;
	}

}
