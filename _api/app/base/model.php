<?php 

class Model extends INSPIRE {

    public $db;
  public function __construct(){
  //setting up db object
  return $this->create_db();
  }

 //create db connection
public function create_db() {
    require(DIR_CONF . 'conf.my_db.php');
    $db = new BaseClass($b_type, $Cfg_host, $Cfg_user, $Cfg_password, $Cfg_db);
$this->db = $db;
    
    return $db;
}

    public function clean_for_sql_html( $var )
    {
        return $this->clean_for_sql( $this->clean_for_html( $var ) );
    }
    
    public function clean_for_sql( $var )
    {
        return mysqli_real_escape_string($this->db->connection, $var );
    }
    
    public function clean_for_json( $var )
    {
        return  $var ;
    }
    
    
    public function clean_for_html( $var )
    {
        return htmlspecialchars( $var );
    }
    
    public function clean_for_int( $var )
    {
        return filter_var( $var, FILTER_SANITIZE_NUMBER_INT );
    }
    
    public function clean_for_1_letter( $var )
    {
        return filter_var( $var, FILTER_SANITIZE_STRING );
    }
    public function clean_for_email( $var )
    {
        return filter_var( $var, FILTER_SANITIZE_EMAIL );
    }
    
    public function clean_array_for_sql( $arr )
    {
        $ret = array();
        if( is_array( $arr ) )
        {
            foreach ( $arr as $key => $val )
            {
                $ret[$key] = $this->clean_for_sql( $val );
            }
        }
        else
            return $this->clean_for_sql( $arr );
            return $ret;
    }
    
    function clean_array_for_html( $arr, $arr_keys = array('') )
    {
        $ret = array();
        if( is_array( $arr ) )
        {
            foreach ( $arr as $key => $val )
            {
                //if keylist not empty, not default, tgt array not assoc,  not in array
                if( ($arr_keys) && $arr_keys[0] !== '' &&  !isset($arr[0]) && !in_array( $key, $arr_keys) )
                {
                    $ret[$key] = $val;
                    continue;
                }
                $ret[$key] = $this->clean_for_html( $val );
            }
        }
        else
            return $this->clean_for_html( $arr );
            return $ret;
    }
    
    public function deep_clean_array_for_sql( $arr )
    {
        $ret = array();
        if( is_array( $arr ) )
        {
            foreach ( $arr as $key => $val )
            {
                if( is_array( $val ) )
                   $ret[$key] = $this->deep_clean_array_for_sql( $val );
                else
                    $ret[$key] = $this->clean_for_sql( $val );
            }
        }
        else
            return $this->clean_for_sql( $arr );
        return $ret;
    }
    public function clean_array_for_int( $arr )
    {
        $ret = array();
        if( is_array( $arr ) )
        {
            foreach ( $arr as $key => $val )
            {
                $ret[$key] = $this->clean_for_int( $val );
            }
        }
        else
            return $this->clean_for_int( $arr );
            return $ret;
    }

 
    public function clean_array_for_sql_html( $arr )
    {
        foreach( $arr as $k=>$v )
        {
            $v = $this->clean_for_html(  $v );
            $arr[$k] = $this->clean_for_sql( $v );
        }
        return $arr;

    }

    function dump( $arr, $quit=0 )
    {
        echo "<pre>";
        print_r( $arr );
        echo "</pre>";
        if( $quit !== 0  ) die;
    }


    function trace(  $quit=0  )
    {
        print "<pre>";
        debug_print_backtrace(  );
        print "</pre>";
        if( $quit !== 0  ) die;
    }


}
