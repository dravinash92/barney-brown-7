<?php



trait CensorWords
{
	
	public function __construct() {	
		$this->replacer = '*';
		$this->setDictionary('en-us');

	}
	
	
	/**
	 *  Sets the dictionar(y|ies) to use
	 *  This can accept a string to a language file path, 
	 *  or an array of strings to multiple paths
	 * 
	 *  @param		string/array
	 *  string
	 */

        public $badwords;
	public function setDictionary($dictionary) {
		
		$badwords = array();
		$this->dictionary = $dictionary;
		
		if (is_array($this->dictionary)) {
			for ($x=0; $x < count($this->dictionary); $x++) {
				if (file_exists(__DIR__ . DIRECTORY_SEPARATOR .'dict/'.$this->dictionary[$x].'.php')) {
					include(__DIR__ . DIRECTORY_SEPARATOR .'dict/'.$this->dictionary[$x].'.php');	
				} else {
					// if the file isn't in the dict directory, 
					// it's probably a custom user library
					include($this->dictionary[$x]);						
				}
				
			} 
		
		// just a single string, not an array	
		} elseif (is_string($this->dictionary)) {
			if (file_exists(__DIR__ . DIRECTORY_SEPARATOR .'dict/'.$this->dictionary.'.php')) {
				include(__DIR__ . DIRECTORY_SEPARATOR .'dict/'.$this->dictionary.'.php');
			} else {
				include($this->dictionary);
			}
		}	
		$this->badwords = $badwords;
			 
	}
	
	
	/**
	 *  Sets the replacement character to use
	 * 
	 *  @param		string			$replacer        Character to use.
	 *  string
	 */
	public function setReplaceChar($replacer) {
		$this->replacer = $replacer;			 
	}


    /**
	 *  Generates a random string.
	 *  @param        string          $chars        Chars that can be used.
	 *  @param        int             $len          Length of the output string.
	 *  string
	 */
	public function randCensor($chars, $len) {

		return str_shuffle(str_repeat($chars, intval($len/strlen($chars))).
			substr($chars, 0, ($len%strlen($chars))));

	}


	/**
	 *  Apply censorship to $string, replacing $badwords with $censorChar.
	 *  @param        string          $string        String to be censored.
	 *  string[string]
	 */
	public function censorString($string) {
			$badwords = $this->badwords;
			$anThis = &$this;
			
			$leet_replace = array();
			$leet_replace['a']= '(a|a\.|a\-|4|@|Ã�|Ã¡|Ã€|Ã‚|Ã |Ã‚|Ã¢|Ã„|Ã¤|Ãƒ|Ã£|Ã…|Ã¥|Î±|Î”|Î›|Î»)';
			$leet_replace['b']= '(b|b\.|b\-|8|\|3|ÃŸ|Î’|Î²)';
			$leet_replace['c']= '(c|c\.|c\-|Ã‡|Ã§|Â¢|â‚¬|<|\(|{|Â©)';
			$leet_replace['d']= '(d|d\.|d\-|&part;|\|\)|Ãž|Ã¾|Ã�|Ã°)';
			$leet_replace['e']= '(e|e\.|e\-|3|â‚¬|Ãˆ|Ã¨|Ã‰|Ã©|ÃŠ|Ãª|âˆ‘)';
			$leet_replace['f']= '(f|f\.|f\-|Æ’)';
			$leet_replace['g']= '(g|g\.|g\-|6|9)';
			$leet_replace['h']= '(h|h\.|h\-|Î—)';
			$leet_replace['i']= '(i|i\.|i\-|!|\||\]\[|]|1|âˆ«|ÃŒ|Ã�|ÃŽ|Ã�|Ã¬|Ã­|Ã®|Ã¯)';
			$leet_replace['j']= '(j|j\.|j\-)';
			$leet_replace['k']= '(k|k\.|k\-|Îš|Îº)';
			$leet_replace['l']= '(l|1\.|l\-|!|\||\]\[|]|Â£|âˆ«|ÃŒ|Ã�|ÃŽ|Ã�)';
			$leet_replace['m']= '(m|m\.|m\-)';
			$leet_replace['n']= '(n|n\.|n\-|Î·|Î�|Î )';
			$leet_replace['o']= '(o|o\.|o\-|0|ÎŸ|Î¿|Î¦|Â¤|Â°|Ã¸)';
			$leet_replace['p']= '(p|p\.|p\-|Ï�|Î¡|Â¶|Ã¾)';
			$leet_replace['q']= '(q|q\.|q\-)';
			$leet_replace['r']= '(r|r\.|r\-|Â®)';
			$leet_replace['s']= '(s|s\.|s\-|5|\$|Â§)';
			$leet_replace['t']= '(t|t\.|t\-|Î¤|Ï„)';
			$leet_replace['u']= '(u|u\.|u\-|Ï…|Âµ)';
			$leet_replace['v']= '(v|v\.|v\-|Ï…|Î½)';
			$leet_replace['w']= '(w|w\.|w\-|Ï‰|Ïˆ|Î¨)';
			$leet_replace['x']= '(x|x\.|x\-|Î§|Ï‡)';
			$leet_replace['y']= '(y|y\.|y\-|Â¥|Î³|Ã¿|Ã½|Å¸|Ã�)';
			$leet_replace['z']= '(z|z\.|z\-|Î–)';

			$words = explode(" ", $string);

			for ($x=0; $x<count($badwords); $x++) {
				$badwords[$x] =  '/'.str_ireplace(array_keys($leet_replace),array_values($leet_replace), $badwords[$x]).'/i';
			}

			$counter=0;
			$match = array();
			$newstring = array();
			$fliter = array();
			$newstring['orig'] = html_entity_decode($string);
			// $anThis for <= PHP5.3
			$newstring['clean'] =  preg_replace_callback($badwords, function($matches) use (&$anThis,&$counter,&$match,&$words,&$fliter) {
				$match[$counter++] = $matches[0];
				
				
				// to avoid strict filtering.
				if(count($match) > 0){
					foreach($match as $mh){ 
					   foreach($words as $word){
					   	
					   	  $strps = strpos($word,$mh);  
					   	  $wdlen = strlen($word); 
					   	  $mhlen = strlen($mh); 
					   	  $sum   = $strps + $mhlen; 
					   	  if($wdlen == $sum && $strps == 0) {  $fliter[] = $mh;  }
					   }
					}
					
				}
				
				

				// is $anThis->replacer a single char?
				return (strlen($anThis->replacer) === 1)
					? str_repeat($anThis->replacer, strlen($matches[0]))
					: $anThis->randCensor($anThis->replacer, strlen($matches[0]));
			}, $newstring['orig']);
			 
			$newstring['matched'] = !count($fliter)?[]:$match;
             
			return $newstring;

	}
}