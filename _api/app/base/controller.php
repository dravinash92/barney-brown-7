<?php

class Controller extends INSPIRE
{
	public function __construct()
	{
		$this->validate();
	
	}
public function dbg_api( $msg )
{
		$dbg['Response']['status-code']='999';
		$dbg['Response']['message']='API DEBUG';
		$dbg['Data']=$msg;
		echo json_encode($dbg); exit;
}

	public function json_success($status_code,$message,$data)
	{

		$success_array=array();
		$success_array['Response']['status-code']=$status_code;
		$success_array['Response']['message']=$message;
		$success_array['Data']=$data;
		$success = json_encode($success_array);
		echo json_encode($success_array); exit;
	}
	public function json_error($status_code,$message,$data)
	{
		$error_array=array();
		$error_array['Response']['status-code']=$status_code;
		$error_array['Response']['message']=$message;
		$error_array['Data']=$data;
		echo json_encode($error_array); exit;
	}
	public function validate()
	{
		global $ENVIRONMENT;
		$set_flag=0;

		if($_POST)
		{

			$username=$_POST['api_username'];
			$password=$_POST['api_password'];

			if(isset($_POST['ENVIRONMENT'])){
				$ENVIRONMENT = $_POST['ENVIRONMENT'];				
				unset($_POST['ENVIRONMENT']);
			}	
			if(USERNAME==$username && PASSWORD==$password)
			{

				$set_flag=1;
			}
			if(API_USERNAME==$username && API_PASSWORD==$password)
			{
				$set_flag=1;
			}
		}

		
		if(!$set_flag)
		{
			$status_code="203";
			$message="Non-Authoritative Information";
			$data="";
			$this->json_error($status_code,$message,$data);
		}
	}
	

}
?>
