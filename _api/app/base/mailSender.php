<?php

require_once 'aws/aws-autoloader.php';
use Aws\Ses\SesClient;

Class MailSender{
	
	private $headers;
	private $footer;
	private $body;
	private $subject;
	private $mailTo;
	// private $mailFrom = "noreply@barneybrown.com";
	private $mailFrom = "orders@barneybrown.com";
	private $mailFromName = TITLE;
	private $logoPath =  "https://mobile.barneybrown.com/app/e-images/logo.png";
	
	public function __construct(){
			
		$this->setHeaders();	
	
	}
	
	public function setMailFrom($mailFrom = "orders@barneybrown.com"){
		
		$this->mailFrom = $mailFrom;
	
	}
		
	public function setHeaders(){
		
		$this->headers = "MIME-Version: 1.0" . "\r\n";
		$this->headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$this->headers .= "From: ". $this->mailFromName ." <". $this->mailFrom . ">\r\n" .		
		"X-Mailer: PHP/" . phpversion();	
	}
	
	public function setSubject($subject = "Subject not set yet."){
		
		$this->subject = $subject;	
			
	}	
	
	public function setBody($body="Body not set yet."){
		
		/*$this->body = " <html style='height: 100%;'>
	                           <body style='height: 100%;'>	                           
								$body
				   </body>
				</html>	
			";*/
		$this->body = $body;
	}
	
	public function setMailTo($mailTo = ""){
		
		$this->mailTo = $mailTo;	
	
	}
	
	/*public function sendMail(){
		
		return mail($this->mailTo,$this->subject,$this->body,$this->headers);
	
	}*/


	public function sendMail(){
		
		$client =  SesClient::factory(array(
					'region' => AMAZON_SES_REGION,
					'version' => 'latest',
					'credentials' => array(
							'key'    => AMAZON_SES_KEY,
							'secret' => AMAZON_SES_SECRET,
				)
		
		));

		

		$msg = array();
		$msg['Source'] = $this->mailFrom;
		//ToAddresses must be an array
		$msg['Destination']['ToAddresses'][] = $this->mailTo;
		$msg['Destination']['BccAddresses'][] ='michaelk@allthingsmedia.com';

		$msg['Message']['Subject']['Data'] = $this->subject;
		$msg['Message']['Subject']['Charset'] = "UTF-8";

		$msg['Message']['Body']['Text']['Data'] = $this->body;
		$msg['Message']['Body']['Text']['Charset'] = "UTF-8";
		$msg['Message']['Body']['Html']['Data'] = $this->body;
		$msg['Message']['Body']['Html']['Charset'] = "UTF-8";

		try{
		     $result = $client->sendEmail($msg);
		     $msg_id = $result->get('MessageId');

		     if($msg_id){
		     	return true;
		     }
		     else{
		     	return false;
		     }
		} catch (Exception $e) {

		     echo($e->getMessage());
		}
	
	}
	
}
