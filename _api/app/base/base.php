<?php
include_once( DIR_BASE. 'customExceptionApi.php' );
class Base extends INSPIRE
{
    
    var $controller;
    var $action;
    var $postdata;
    var $title;
    var $navigation;
    var $db;
    var $languages;
    var $vehicle_class;
    var $body_types;
    var $devices;
    var $apps;
    
    function init()
    {
        
        $this->SessionStart();
        $this->ProcessUrl();
        
        $this->db = $this->db();
        
        if ($this->UrlArray[0] != '') {
            $this->controller = $this->UrlArray[0];
        } else {
            $this->controller = 'user';
        }
        if ($this->UrlArray[1] != '') {
            $this->action = $this->UrlArray[1];
        } else {
            $this->action = 'index';
        }
        $params = $this->process();
        
        
    }
    
    function process()
    {
    	if(!class_exists($this->controller) ){
    		header('Location: '.SITE_URL.'error/_404/');
    		exit();
    	} 
    	
    	$obj = new $this->controller;
        if(file_exists(DIR_MODEL . $this->controller . '.php')) include_once(DIR_MODEL . $this->controller . '.php');
        $obj->postdata = $this->postdata;
        $obj->UrlArray = $this->UrlArray;
        $obj->{$this->action}();
        return $obj;
        
        if(!method_exists($obj,$this->action)){
        	header('Location: '.SITE_URL.'error/_404/');
        	exit();
        }
    }
    
    
    function br2nl($string)
    {
        return str_replace(array(
            "\r\n",
            "\r",
            "\n"
        ), "\\n", $string);
    }
    
}
