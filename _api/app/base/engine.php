<?php
class INSPIRE
{
    
    var $DirTree = 0; //url offset
    var $DirLimit = 10; //url variable limit 
    var $UpDirCounter;
    var $UrlArray;
    var $smarty; 
    var $ShortCuts = array(); 
    var $db;
    
    function SessionStart() {
        session_start();
    }
    
	
	function db(){
		require(DIR_DB.'database.svc.php');
		return $db;
	}

    function ProcessUrl() {
    	$url = str_replace(PROJECT_BASE,'',$_SERVER['REQUEST_URI']);
      
    	$this->UrlArray = explode("/",$url);
        array_shift ($this->UrlArray);
        for($i=0; $i < $this->DirTree; $i++)
        {
            array_shift ($this->UrlArray);
        }

        $Counter=count($this->UrlArray);
        for($i=0; $i<$Counter; $i++) {  $this->UrlArray[$i]=str_replace('.html', '', $this->UrlArray[$i]); }
        for($i=1; $i<$Counter; $i++) {  $this->UpDirCounter.='../'; }

        for($i=0; $i<$this->DirLimit; $i++) {  if(!isset($this->UrlArray[$i])) $this->UrlArray[$i]=''; $this->UrlArray[$i]=trim($this->UrlArray[$i]);}

    
    }
    
    function PrepareLink($title, $ext='.html', $step=';') {
        $prepared_title=trim($title);
        $prepared_title = str_replace(" ", $step, $prepared_title);
        $prepared_title = ereg_replace("[^0-9a-zA-Z_,-]","", $prepared_title);
        $prepared_title.=$ext;
        return $prepared_title;
    }
   
    
   
}
?>