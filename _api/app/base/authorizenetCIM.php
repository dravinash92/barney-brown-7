<?php

define( 'DUMP_ON_ERROR',1 );
Class authorizenetCIM  extends Model
{
	
	public $db; //database connection object

	private $last_response;
	private $address;
	private $parsed_response;
	private $last_request;

	public function __construct()
	{		
		$this->g_loginname = AUTHORIZENET_LOGIN;
		$this->g_transactionkey  = AUTHORIZENET_TRANSACTION_KEY;
		$this->g_apihost = AUTHORIZENET_API_HOST;
		$this->g_apipath  = AUTHORIZENET_API_PATH;
		$this->g_transactiontype= AUTHORIZENET_TRANSACTION_TYPE;
		$this->g_validationmode= AUTHORIZENET_VALIDATION_MODE;

		$this->init_validate(  );
		$this->db = parent::__construct();
	}

	private $testResponseOverride = null;
	
	public function setTestResponseOverride( $xml )
	{
		$this->testResponseOverride = $xml;
	}
	public function clearTestResponseOverride(  )
	{
		$this->testResponseOverride = null;
	}


	public function getParsedResponse(  )
	{
		return $this->parsed_response;
	}

	public function getRequest(  )
	{
		return $this->last_request;
	}


	public function get_parsed_response(  )
	{
		return $this->parsed_response;
	}

	public function get_last_result(  )
	{
		return $this->last_response;
	}

	public function send_xml_request($content)
	{
		return $this->send_request_via_fsockopen($this->g_apihost,$this->g_apipath,$this->content);
	}
	public function get_address(  )
	{
		return $this->address;
	}


	public function dump_everything( $code )
	{
		if( DUMP_ON_ERROR == 1 )
		{
			@$email = $this->email;
		 	@$uid = $this->uid;
			$str = "TRANSACTION - $code User: $email ($uid) - ". date( "c" )."\n";
			$str .= ">>>>>>>>>>>>>>\n";
			@$str .=  $this->content;
			$str .=  "\n\n";
			$str .= "<<<<<<<<<<<<<<\n";
			@$str .= var_export( $this->response , true );
			$str .= "\n##############\n\n";
			
			@file_put_contents( 'app/log/dump_auth.txt', $str , FILE_APPEND );
		}
	}

	//function to send xml request via fsockopen
	//It is a good idea to check the http status code.
	public function send_request_via_fsockopen($host,$path,$content)
	{
		$this->posturl = "ssl://" . $host;
		$this->header = "Host: $host\r\n";
		$this->header .= "User-Agent: PHP Script\r\n";
		$this->header .= "Content-Type: text/xml\r\n";
		$this->header .= "Content-Length: ".strlen($content)."\r\n";
		$this->header .= "Connection: close\r\n\r\n";
		$this->fp = fsockopen($this->posturl, 443, $this->errno, $this->errstr, 30);
		if (!$this->fp)
		{
			$body = false;
		}
		else
		{
			error_reporting(E_ERROR);
			fputs($this->fp, "POST $path  HTTP/1.1\r\n");
			fputs($this->fp, $this->header.$content);
			fwrite($this->fp, $this->out);
			$this->response = "";
			while (!feof($this->fp))
			{
				$this->response = $this->response . fgets($this->fp, 128);
			}
			fclose($this->fp);
			error_reporting(E_ALL ^ E_NOTICE);
	
			$this->len = strlen($this->response);
			$this->bodypos = strpos($this->response, "\r\n\r\n");
			if ($this->bodypos <= 0)
			{
				$this->bodypos = strpos($this->response, "\n\n");
			}
			while ($this->bodypos < $this->len && $this->response[$this->bodypos] != '<')
			{
				$this->bodypos++;
			}
			$this->body = substr($this->response, $this->bodypos);
		}
		return $this->body;
	}
	
	//function to send xml request via curl
	public function send_request_via_curl($host,$path,$content)
	{
		$this->last_request = $content;
		$posturl = "https://" . $host . $path;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $posturl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		return $response;
	}

	private function init_validate(  )
	{
		if( isset( $this->validate["name"] ) ){
			return;
		}
		
		// NOTE Important -- giving a -1 to the length parameter means skip the length check.
		$this->validate["name"]['length'] 	= 50;
		$this->validate["name"]['whitelist'] = "AN";
		
		$this->validate["last_name"]['length'] 	= 50;
		$this->validate["last_name"]['whitelist'] = "AN" ;
		
		$this->validate["company"]['length'] 	= 50;
		$this->validate["company"]['whitelist'] = "AN";
		
		$this->validate["address1"]['length'] 	= 60;
		$this->validate["address1"]['whitelist'] = "AN";
		
		$this->validate["company"]['length'] 	= 50;
		$this->validate["company"]['whitelist'] = "AN";
		
		$this->validate["address"]['length'] 	= 60;
		$this->validate["address"]['whitelist'] = "AN";
		
		$this->validate["zip"]['length'] 	= 20;
		$this->validate["zip"]['whitelist'] = "Z";
		
		$this->validate["phone"]['length'] 	= 25;
		$this->validate["phone"]['whitelist'] = "P";
		
		$this->validate["description"]['length'] 	= 32;
		$this->validate["description"]['whitelist'] = "ALL";

		$this->validate["description"]['length'] 	= 32;
		$this->validate["description"]['whitelist'] = "ALL";

		$this->validate["card_number"]['length'] 	= 32;
		$this->validate["card_number"]['whitelist'] = "CC";
		// Validation.  All of these go in [ ] brackets, so ^ = NOT whatever. 
		
		$this->validate_regex['ALL'] = '^.';					// All characters OK.  ( Note gt, lt done via strip_html call )
		$this->validate_regex['AN'] = '^A-Za-z0-9\-';			// AlphaNumeric
		$this->validate_regex['P'] = '^0-9\s\(\)\-';			// Phone -- 0-9 spaces, (  ) and -
		$this->validate_regex['NUM'] = '^0-9\.\-';				// Number - 0-9 and .
		$this->validate_regex['CC'] = '^0-9X';					// Credit Card - 0-9 and X
		$this->validate_regex['Z'] = '^0-9\-';					// Zip Code - 0-9 and -
	}

	private function validate_array_for_XML( $array )
	{
		foreach( $array as $key=>$val )
		{
			$array[$key] = $this->validate_xml_value( $key, $val );
			$array[$key] = htmlspecialchars( $array[$key] );
		}
		return $array;
	}

	private function validate_xml_value( $key_name, $value )
	{
		if( isset( $this->validate[$key_name]['whitelist'] ) )		
		{
			$regex_code = $this->validate[$key_name]['whitelist'];
			$regex_string = $this->validate_regex[$regex_code];
			$regex_actual = "/[$regex_string]/";
			$value_cleaned =   preg_replace( $regex_actual, '',$value );

			$max_length  = $this->validate[$key_name]['length'];
			if ( $max_length > 0 )
				$value_cleaned = substr( $value_cleaned, 0 , $max_length );
			return $value_cleaned;
		}
		else {
			return $value;
		}
	}



	private function isEmpty( $test )
    {
        if ( !isset($test) )
            return true;

        else if( isset($test) && $test == "0" )
        	return true;

        else if( isset($test) && $test == "" )
        	return true;

        else if( isset($test) && $test == 0 )
        	return true;

        else if( isset($test) && empty($test) )
        	return true;

        else
            return null;
    }



	private function init_address(  $address )
	{
		//$this->customerProfileId =  "35191835";
		if( $this->isEmpty( $address["phone"] ) )
			$address["phone"]="0000000000";
		if( isset($address["name"]) && $address["name"]=="")
			$address["name"]="name";
		if( isset($address["last_name"]) && $address["last_name"]=="")
			$address["last_name"]="";
		if( !(isset($address["last_name"] ) ) )
			$address["last_name"] = '';
		if( isset($address["company"]) && $address["company"]=="" && 1 == 1 ) 
			$address["company"]="";
		if( isset($address["zip"]) && $address["zip"]=="")
			$address["zip"]="00000";
		if( isset($address["address1"]) && $address["address1"]=="")
			$address["address1"]="address1";
		if( isset($address["address2"]) && $address["address2"]=="")
			$address["address2"]="address2";

		$this->address = $address;
		return $address;
	}

	//function to parse the api response
	//The code uses SimpleXML. http://us.php.net/manual/en/book.simplexml.php
	//There are also other ways to parse xml in PHP depending on the version and what is installed.
	public function parse_api_response($content)
	{
		$parsedresponse = simplexml_load_string($content, "SimpleXMLElement", LIBXML_NOWARNING);
		@$resultCode = $parsedresponse->messages->resultCode;
		if ("Ok" != $resultCode ) {
			@$this->dump_everything( $resultCode );
		} 

		$this->parsed_response = $parsedresponse;

		/*if ("Ok" != $parsedresponse->messages->resultCode) {
			echo "The operation failed with the following errors:<br>";
			foreach ($parsedresponse->messages->message as $msg) {
				echo "[" . htmlspecialchars($msg->code) . "] " . htmlspecialchars($msg->text) . "<br>";
			}
			echo "<br>";
		} */
		return $parsedresponse;
	}
	
	public function MerchantAuthenticationBlock() {
		return
		"<merchantAuthentication>".
		"<name>" . $this->g_loginname . "</name>".
		"<transactionKey>" . $this->g_transactionkey . "</transactionKey>".
		"</merchantAuthentication>";
	}

	public function extract_xml_tag( $tag_name, $text )
	{
		preg_match( "/<$tag_name>(.*?)<\/$tag_name>/" , $text, $match);
		return $match[1];
	}

	public function check_xml_response_for_error( $response )
	{
		$parsed_response = $response ;

		preg_match('/<code>(.*?)<\/code>/', $response, $match_code);
		$code = $match_code[1];
		
		preg_match('/<text>(.*?)<\/text>/', $response, $match_text);
		$text = $match_text[1];

		return  array( 'code'=>$code, 'text'=>$text );
	}

	private function handle_response( $response )
	{
		return;
		//handle_response
	}

	
	private function filter_error_except_on_passed_codes( $error_response, $whitelist_codes = array( ) )
	{
		if( in_array( $error_response['code'], $whitelist_codes )){
			return $error_response['code'];
		}
		else
		{
			$error_string = $error_response['code'] . " - ". $error_response['text'];
			throw new CustomExceptionApi( ORDER_ERROR_AUTHORIZE_NET_ERROR, "AuthorizeNet Error: $error_string", $error_response['code'] );
		}
	}

	/**
	 * createProfile - Creates a new authorizenet profile
	 * @param  string $email 
	 * @param  string $uid   
	 * @return string 			Retuns with the 
	 */
	public function createProfile( $email, $uid )
	{
		$this->email = $email;
		$this->uid = $uid;

		$this->createProfile_createXML(  );
		$this->response = $this->send_xml_request($this->content);
		$error_response = $this->check_xml_response_for_error( $this->response );
		
//		debug_x( 'createProfile' );
		// This error - e00039 - means this user is already in the system.  So grab the id and return it.
		if( $error_response['code'] =='E00039' )
		{
			// Grab the user id.
            $profile_id = preg_match( '/ID (\d{10})/', $error_response['text'], $match_id );
            $return_value = $match_id[1] ;
			$this->last_response = $return_value;
			return $return_value;
		}


		$parsedresponse = $this->parse_api_response( $this->response );

		if ("Ok" == $parsedresponse->messages->resultCode) {
			$return_value = htmlspecialchars( $parsedresponse->customerProfileId );
		}
		else{	
			$this->filter_error_except_on_passed_codes( $error_response );
		}
		
		$this->last_response = $return_value;
		return $return_value;	
	}

	private function createProfile_createXML(  )
	{
		//build xml to post
		$this->content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<createCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<profile>".
		"<merchantCustomerId>".$this->uid."</merchantCustomerId>". // Your own identifier for the customer.
		"<description></description>".
		"<email>" . $this->email . "</email>".
		"</profile>".
		"</createCustomerProfileRequest>";
	}


	/**
	 * createPaymentProfile - Send credit card info with customerProfileId and register in db
	 * @param  int 		$customerProfileId 
	 * @param  string 	$card_num          
	 * @param  string 	$exp_yr            
	 * @param  string 	$exp_mth           
	 * @param  array 	$address           	Billing address
	 * @return string 	PaymentProfileId 	Responds with PaymentProfileId                    
	 */
	public function createPaymentProfile( $customerProfileId, $card_num, $exp_yr, $exp_mth, $address )
	{
		//print_r($result);exit;
		$this->customerProfileId = $customerProfileId;
		$this->card_num = $this->validate_xml_value( 'card_number', $card_num );
		$this->exp_yr = $exp_yr;
		$this->exp_mth = $exp_mth;
	
		$address = $this->init_address( $address );
		
		$this->createPaymentProfile_createXML( $address );	
		$this->response = $this->send_xml_request( $this->content );
		$parsedresponse = $this->parse_api_response( $this->response );
		$error_response = $this->check_xml_response_for_error( $this->response );

		/*
		print "\ncreatePaymentProfile RESPONSE\n";
		//$this->debug_x(  'createCustomerPaymentProfileRequest' );
		print "\n\n";
		*/

		// This error - e00039 - means this user is already in the system.  So grab the id and return it.
		if( $error_response['code'] =='E00039' )
		{
			$return_value = htmlspecialchars( $parsedresponse->customerPaymentProfileId );
            $this->last_response = $return_value;
			return $return_value;	
		}
		
		if ("Ok" == $parsedresponse->messages->resultCode) {
			$return_value = htmlspecialchars( $parsedresponse->customerPaymentProfileId );
		}
		else if ( "Error" == $parsedresponse->messages->resultCode ) {
			$this->insertResponse( $parsedresponse,$customerProfileId,false );
			$this->filter_error_except_on_passed_codes( $error_response, array( 'E00039' ) );
		}
		
		$this->last_response = $return_value;
		return $return_value;		
	}

	private function createPaymentProfile_createXML( $address )
	{
		$address = $this->validate_array_for_XML( $address );

		//build xml to post
		$this->content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<createCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>" . $this->customerProfileId . "</customerProfileId>".
		"<paymentProfile>".
		"<billTo>".
				"<firstName>".$address["name"]."</firstName>".
                "<lastName>".  $address["last_name"] ."</lastName>".
                "<company>".  $address["company"] ."</company>".
                "<address>".  $address["address1"] ."</address>".
                "<zip>".  	  $address["zip"] ."</zip>".
                "<phoneNumber>".$address["phone"]."</phoneNumber>".
		"</billTo>".
		"<payment>".
		"<creditCard>".
		"<cardNumber>".$this->validate_xml_value( 'card_number', $this->card_num )."</cardNumber>".
		"<expirationDate>".$this->exp_yr."-".$this->exp_mth."</expirationDate>". // required format for API is YYYY-MM
		"</creditCard>".
		"</payment>".
		"</paymentProfile>".
		"<validationMode>".$this->g_validationmode."</validationMode>". // or testMode
		"</createCustomerPaymentProfileRequest>";

	}


	/**
	 * createShippingProfile
	 * Create the shipping profile in authorizenet, given a customerID and address.
	 * @param  int $customerProfileId [description]
	 * @param  array $address           [description]
	 * @return customerShippingID 		The ID for shipping location, actually in authorizenet it's called the customerAddressId
	 */
	public function createShippingProfile( $customerProfileId, $address )
	{
		$address = $this->init_address( $address );
		$address["address"]=$address["address1"]." ".$address["address2"];		
		
		//$this->customerProfileId =  "35191835";

		$this->customerProfileId = $customerProfileId;
				
		$this->createShippingProfile_createXML( $address );
		$this->response = $this->send_xml_request( $this->content );
		$parsedresponse = $this->parse_api_response($this->response);
		$error_response = $this->check_xml_response_for_error( $this->response );

		//$this->debug_x( 'createshippingprofile' );
				
		/*print "\ncreateShippingProfile RESPONSE\n";
		print_r( $this->response );
		print "\n\n";*/

		
		// This error - e00039 - means this user is already in the system.  So grab the id and return it.
		if( $error_response['code'] =='E00039' )
		{
			$return_value = $this->extract_xml_tag( 'customerAddressId', $this->response );
			$this->last_response = $return_value;
			return $return_value;            
		}


		if ("Ok" == $parsedresponse->messages->resultCode) {
			$return_value = htmlspecialchars( $parsedresponse->customerAddressId );
		}
		else if ( "Error" == $parsedresponse->messages->resultCode ) {
			$this->insertResponse( $parsedresponse, $customerProfileId, false );
			$this->filter_error_except_on_passed_codes( $error_response, array( 'E00039' ) );
		}

		$this->last_response = $return_value;
		return $return_value;	
	}

	private function createShippingProfile_createXML( $address )
	{
		$address = $this->validate_array_for_XML( $address );
		
		$this->content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<createCustomerShippingAddressRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>" . $this->customerProfileId . "</customerProfileId>".
		"<address>".
				"<firstName>".  $address["name"]."</firstName>".
                "<lastName></lastName>".
                "<company>".   $address["company"]."</company>".
                "<address>".   $address["address"]."</address>".
                "<zip>".    $address["zip"].   "</zip>".
                "<phoneNumber>". $address["phone"].  "</phoneNumber>".
		"</address>".
		"</createCustomerShippingAddressRequest>";
	}

	/**
	 * createTransaction 
	 * createTransaction - Send a transaction to AuthorizeNet, using payment, shipping and profile ID's.
	 * @param  array $post       	All parameters passed to the transaction here
	 * @param  array $orderItems 	Deprecated, this is not used in this version; kept in for backwards compatibility. 
	 * @return Transaction Number 
	 */
	public function createTransaction( $post, $orderItems )
	{

		$this->createTransaction_createXML( $post );
		//echo "Raw request: " . htmlspecialchars($content) . "<br><br>";
		$this->response = $this->send_xml_request($this->content);
		$parsedresponse = $this->parse_api_response($this->response);
		$error_response = $this->check_xml_response_for_error( $this->response );

		//$this->debug_x( 'createTransactionRequest' );

		$this->parsed_response = $parsedresponse;

		$directResponseFields = explode(",", $parsedresponse->directResponse);
		$responseCode = intval( $directResponseFields[0] ); // 1 = Approved 2 = Declined 3 = Error
		$responseReasonCode = $directResponseFields[2]; // See http://www.authorize.net/support/AIM_guide.pdf
		$responseReasonText = $directResponseFields[3];
		$approvalCode = $directResponseFields[4]; // Authorization code
		$transId = $directResponseFields[6];


		if ("Ok" == $parsedresponse->messages->resultCode) 
		{
			if (isset($parsedresponse->directResponse))
			{
				/*echo "direct response: <br>"
				 . htmlspecialchars($this->parsedresponse->directResponse)
				. "<br><br>"; */
			
		

				if( $responseCode != 1 ){
					throw new CustomExceptionTransaction( $parsedresponse->messages->message->code, $parsedresponse->messages->message->text );
				}

				//if ("1" == $responseCode) return htmlspecialchars($transId);//echo "The transaction was successful.<br>";
				/*else if ("2" == $responseCode) echo "The transaction was declined.<br>";
				else echo "The transaction resulted in an error.<br>";
			
				echo "responseReasonCode = " . htmlspecialchars($responseReasonCode) . "<br>";
				echo "responseReasonText = " . htmlspecialchars($responseReasonText) . "<br>";
				echo "approvalCode = " . htmlspecialchars($approvalCode) . "<br>";
				echo "transId = " . htmlspecialchars($transId) . "<br>";*/
				
			}
			$return_value = htmlspecialchars($transId);
		}
		else if( "Error" == $parsedresponse->messages->resultCode )
		{
			$this->insertResponse($parsedresponse,$post["customerProfileId"],false);
			file_put_contents("avi.txt", print_r($parsedresponse, true));
			if( $responseCode != 1 )
			{

					throw new CustomExceptionTransaction( $parsedresponse->messages->message->code, $parsedresponse->messages->message->text );
			}
		}
		$this->last_response = $return_value;
		return $return_value;	
	}
		
	// I don't order information this is used in this transaction version, instead the shipping, customer, payment id's are used.
	private function createTransaction_createOrderItemXML( $orderItems )
	{
		$this->line_items="";
		foreach($orderItems as $orderItem)
		{
			if($orderItem["sandwich_name"]=="")
				$orderItem["sandwich_name"]="name";
			if($orderItem["description"]=="")
				$orderItem["description"]="description";

			$this->line_items.="<lineItems>".
				"<itemId>".$orderItem["item_id"]."</itemId>".
				"<name>".htmlspecialchars($orderItem["sandwich_name"])."</name>".
				"<description>".substr($orderItem["description"], 0, 32)."</description>".
				"<quantity>".$orderItem["item_qty"]."</quantity>".
				"<unitPrice>".$orderItem["item_price"]."</unitPrice>".
				"<taxable>true</taxable>".
			"</lineItems>";
		}
	}

	private function createTransaction_createXML( $post )
	{
		$this->content =
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
			"<createCustomerProfileTransactionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
			$this->MerchantAuthenticationBlock().
			"<transaction>".
			"<".$this->g_transactiontype.">".			// This is defined in the constructor and = the constant value for AUTHORIZENET_TRANSACTION_TYPE
			"<amount>" . $post["total"] . "</amount>". // should include tax, shipping, and everything.
			"<shipping>".
			"<amount>0.00</amount>".
			"<name>Free Shipping</name>".
			"<description>Free Shipping.</description>".
			"</shipping>".
			/*"<lineItems>".
			"<itemId>".$post["order_item_id"]."</itemId>".
			"<name>name of item sold</name>".
			"<description>Description of item sold</description>".
			"<quantity>1</quantity>".
			"<unitPrice>" . ($post["sub_total"]) . "</unitPrice>".
			"<taxable>false</taxable>".
			"</lineItems>".
			"<lineItems>".
			"<itemId>456789</itemId>".
			"<name>name of item sold</name>".
			"<description>Description of item sold</description>".
			"<quantity>1</quantity>".
			"<unitPrice>1.00</unitPrice>".
			"<taxable>false</taxable>".
			"</lineItems>".*/
			//$this->line_items.
			"<customerProfileId>" . $post["customerProfileId"] . "</customerProfileId>".
			"<customerPaymentProfileId>" . $post["customerPaymentProfileId"] . "</customerPaymentProfileId>".
			"<customerShippingAddressId>" . $post["customerShippingAddressId"] . "</customerShippingAddressId>".
			"<order>".
			"<invoiceNumber></invoiceNumber>".
			"</order>".
			"</".$this->g_transactiontype.">".
			"</transaction>".
			"<extraOptions>
			<![CDATA[x_duplicate_window=0]]>
			</extraOptions>".
			"</createCustomerProfileTransactionRequest>";

	}



	/**
	 * createTransactionRequest description
	 * createTransactionRequest - Create one-time use transaction with Authorizenet; it doesn't save the cc number or address information
	 * @param  array $post             	Post has all values used in transaction.
	 * @param  array $orderItems       	List of items to be ordered, formatted.
	 * @param  array $billing_address  
	 * @param  array $shipping_address 
	 * @return transactionID                   
	 */
	public function createTransactionRequest( $post, $orderItems, $billing_address, $shipping_address )
    {
		//echo '<pre>';print_r($address);print_r($shippingaddress);exit;
		$billing_address_clean 	= $this->init_address( $billing_address );
		$shipping_address_clean = $this->init_address( $shipping_address );
	
		$this->createTransactionRequest_createOrderXML( $orderItems );
		$this->createTransactionRequest_createXML( $post, $billing_address_clean, $shipping_address_clean );
	
		//echo "Raw request: " . htmlspecialchars($this->content) . "<br><br>";
		$this->response = $this->send_xml_request($this->content);
		$parsedresponse = $this->parse_api_response($this->response);
		$error_response = $this->check_xml_response_for_error( $this->response );
		$this->parsed_response = $parsedresponse;

		//$this->debug_x( 'createTransactionRequest' );
		//exit(  );

		if ( isset($parsedresponse->transactionResponse) )
		{
			$directResponseFields = $parsedresponse->transactionResponse;
			$responseCode = $parsedresponse->transactionResponse->responseCode; // 1 = Approved 2 = Declined 3 = Error
			$transId = $parsedresponse->transactionResponse->transId;
		}

		if ( "Ok" == $parsedresponse->messages->resultCode ) 
		{	
			if( $responseCode != 1 ){
					throw new CustomExceptionTransaction( $parsedresponse->messages->message->code, $directResponseFields );
			}

			$return_value = htmlspecialchars($transId);
		}
		else if ("Error" == $parsedresponse->messages->resultCode )
		{
			$this->insertResponse($parsedresponse, $post["customerProfileId"], false );
			if( $responseCode != 1 ){
					throw new CustomExceptionTransaction( $parsedresponse->messages->message->code, $directResponseFields );
			}
		}
		$this->last_response = $return_value;
		return $return_value;
	}


	private function createTransactionRequest_createOrderXML( $orderItems )
	{
		$this->line_items = "";

		foreach($orderItems as $orderItem)
		{
			if( !isset( $orderItem["sandwich_name"] ) || $orderItem["sandwich_name"]=="" ){
				$orderItem["sandwich_name"]="name";
			}
			
			if( !isset($orderItem["description"]) ||  $orderItem["description"]=="" ){
				$orderItem["description"]="description";
			}
				
			$this->line_items .=
				"<lineItem>".
					"<itemId>".$orderItem["item_id"]."</itemId>".
					"<name>".htmlspecialchars($orderItem["sandwich_name"])."</name>".
					"<description>".substr($orderItem["description"], 0, 32)."</description>".
					"<quantity>".$orderItem["item_qty"]."</quantity>".
					"<unitPrice>".$orderItem["item_price"]."</unitPrice>".
				"</lineItem>";
		}
	}

	private function createTransactionRequest_createXML( $post, $billing_address, $shipping_address )
	{
		$billing_address 	= $this->validate_array_for_XML( $billing_address );
		$shipping_address 	= $this->validate_array_for_XML( $shipping_address );

		$refId = 'ref' . time();
		
		$this->content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<createTransactionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<refId>".$refId."</refId>".
		"<transactionRequest><transactionType>authCaptureTransaction</transactionType>".
		"<amount>" . $post["total"] . "</amount>". // should include tax, shipping, and everything.
		"<payment>".
      		"<creditCard>".
        		"<cardNumber>".$this->validate_xml_value( 'card_number', $post["credit_card"] )."</cardNumber>".
        		"<expirationDate>".$post["exp_yr"]."-".$post["exp_mth"]."</expirationDate>". 
        		"<cardCode>".$post['cvv']."</cardCode>".
      		"</creditCard>".
    	"</payment>".
		"<order>
     		<invoiceNumber></invoiceNumber>
     		<description>abc</description>
    	</order>".
    	"<lineItems>".
		$this->line_items.
		"</lineItems>".
		"<tax>".
      		"<amount>0</amount>".
      		"<name>level2 tax name</name>".
      		"<description>level2 tax</description>".
    	"</tax>".
    	"<duty><amount>0</amount><name>duty name</name><description>duty description</description></duty><shipping><amount>0</amount><name>level2 tax name</name><description>level2 tax</description></shipping>".
		"<poNumber>".$post["credit_card_id"]."</poNumber><customer><id>".$post["credit_card_id"]."</id></customer>".
		"<billTo>".
		"<firstName>". $billing_address["name"] ."</firstName>".
                "<lastName>". $billing_address["last_name"] ."</lastName>".
                "<company>". $billing_address["company"] ."</company>".
                "<address>". $billing_address["address1"] ."</address>".
                "<zip>". $billing_address["zip"] ."</zip>".
                "<phoneNumber>". $billing_address["phone"] ."</phoneNumber>".
		"</billTo>".
		"<shipTo>".
        		"<firstName>".$shipping_address["name"]  ."</firstName>".
         		"<lastName>". $shipping_address["last_name"]  ."</lastName>".
                "<company>". $shipping_address["company"]  ."</company>".
                "<address>". $shipping_address["address1"]  ."</address>".
                "<zip>".  $shipping_address["zip"]  ."</zip>".
                "<country>USA</country>".
	    "</shipTo>".
	    "<customerIP></customerIP>".
	    "<transactionSettings><setting><settingName>testRequest</settingName><settingValue>false</settingValue></setting></transactionSettings>".
		"<userFields><userField><name>MerchantDefinedFieldName1</name><value>MerchantDefinedFieldValue1</value></userField><userField><name>favorite_color</name><value>blue</value></userField></userFields>".
		"</transactionRequest>".
		"</createTransactionRequest>";
	}

	/**
	 * deleteProfile
	 * @return String 		Returns HTML String with a continue GET link using customerProfileId
	 */
    public function deleteProfile( )
	{
		$response = send_xml_request($this->content);
		
		$parsedresponse = parse_api_response($response);
		$error_response = $this->check_xml_response_for_error( $this->response );

		if ("Ok" == $parsedresponse->messages->resultCode) {
			echo "customerProfileId <b>"
					. htmlspecialchars($_POST["customerProfileId"])
					. "</b> was successfully deleted.<br><br>";
		}
		else
		{
			$this->filter_error_except_on_passed_codes( $error_response );
		}
		
		echo "<br><a href=index.php?customerProfileId="
				. urlencode($_POST["customerProfileId"])
				. ">Continue</a><br>";
	}

	private function deleteProfile_createXML(  )
	{
		//build xml to post
		$this->content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<deleteCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		MerchantAuthenticationBlock().
		"<customerProfileId>" . htmlspecialchars( $_POST["customerProfileId"] ) . "</customerProfileId>".
		"</deleteCustomerProfileRequest>";
	}

	/**
	 * deletePaymentProfile
	 * @param  array $post 
	 * @return true 		True on delete OK
	 */
	public function deletePaymentProfile( $post )
	{

		$this->content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<deleteCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>" . $post["customerProfileId"] . "</customerProfileId>".
		"<customerPaymentProfileId>" . $post["customerPaymentProfileId"] . "</customerPaymentProfileId>".
		"</deleteCustomerPaymentProfileRequest>";

		$this->response = $this->send_xml_request($this->content);
		$parsedresponse = $this->parse_api_response($this->response);
		$error_response = $this->check_xml_response_for_error( $this->response );
		
		if ("Ok" == $parsedresponse->messages->resultCode) {
			return true;
		}
		else{
			$this->filter_error_except_on_passed_codes( $error_response );
		}
		return true;
		//return $parsedresponse;
	}

	/**
	 * deleteShippingProfile
	 * @param  array $post 	
	 * @return true       	True on delete OK
	 */
	public function deleteShippingProfile($post)
	{
		$this->content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<deleteCustomerShippingAddressRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>" . $post["customerProfileId"] . "</customerProfileId>".
		"<customerAddressId>" . $post["customerAddressId"] . "</customerAddressId>".
		"</deleteCustomerShippingAddressRequest>";
		$this->response = $this->send_xml_request($this->content);
		$parsedresponse = $this->parse_api_response($this->response);
		$error_response = $this->check_xml_response_for_error( $this->response );

		if ("Ok" == $parsedresponse->messages->resultCode) {
			return true;
		}
		else{
			$this->filter_error_except_on_passed_codes( $error_response );
		}		
	}

	public function updatePaymentProfile( $paymentProfileId, $customerProfileId, $card_num, $exp_yr, $exp_mth, $address )
	{
		//print_r($result);exit;
		$this->paymentProfileId =  $paymentProfileId;
		$this->customerProfileId = $customerProfileId;

		$this->card_num = $this->validate_xml_value( 'card_number', $card_num );
		$this->exp_yr = $exp_yr;
		$this->exp_mth = $exp_mth;

		$address = $this->init_address( $address );
		$this->updatePaymentProfile_createXML( $address );		//updateCustomerPaymentProfileRequest

		$this->response = $this->send_xml_request($this->content);
		$parsedresponse = $this->parse_api_response($this->response);
		$error_response = $this->check_xml_response_for_error( $this->response );
		
		//$this->debug_x( 'updateCustomerPaymentProfileRequest' );

		if ("Ok" == $parsedresponse->messages->resultCode) {
			$return_value = htmlspecialchars($parsedresponse->customerPaymentProfileId);
			return true;
		}
		else if ("Error" == $parsedresponse->messages->resultCode) {
			$this->filter_error_except_on_passed_codes( $error_response );
		}
		$this->last_response = $return_value;
		return $return_value;		
	}


	public function updatePaymentProfile_createXML( $address )
	{
		$address = $this->validate_array_for_XML( $address );
		
		//build xml to post
		$this->content =
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
			"<updateCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
			$this->MerchantAuthenticationBlock().
			"<customerProfileId>" . $this->customerProfileId . "</customerProfileId>".
			"<paymentProfile>";
			
		
		if( $this->array_keys_exist( $address, array( 'firstName', 'last_name', 'company', 'address1', 'zip', 'phone' ) ) >0 )
		{
			$this->content .= "<billTo>";

			if( isset( $address["firstName"] ) && $address["firstName"] )
				$this->content .="<firstName>".  $address["firstName"]  ."</firstName>";

			if( isset( $address["last_name"] ) && $address["last_name"] )
	            $this->content .= "<lastName>".  $address["last_name"]  ."</lastName>";

	       if( isset( $address["company"] ) && $address["company"] )
	            $this->content .= "<company>".  $address["company"]  ."</company>";

	        if( isset( $address["address1"] ) && $address["address1"] )
	            $this->content .= "<address>".  $address["address1"]  ."</address>";
	            
	        if( isset( $address["zip"] ) && $address["zip"] )
	            $this->content .= "<zip>".  $address["zip"]		  ."</zip>";

	       	if( isset( $address["phone"] ) && $address["phone"] )
	            $this->content .= "<phoneNumber>". $address["phone"]."</phoneNumber>";
			
			$this->content .="</billTo>";
		}
			
		if( ( $this->exp_yr && $this->exp_mth )  ||  $this->card_num )
		{
			$this->content .="<payment>";
			$this->content .=	"<creditCard>";
				
			if( $this->card_num  )
			{
				$this->content .= "<cardNumber>". $this->validate_xml_value( 'card_number', $this->card_num ) ."</cardNumber>";
			}

			if( $this->exp_yr && $this->exp_mth )
				$this->content .= "<expirationDate>".$this->exp_yr."-".$this->exp_mth."</expirationDate>"; // required format for API is YYYY-MM
				
			$this->content .=	"</creditCard>";
			$this->content .= "</payment>";
		}		

		$this->content .=
			"<customerPaymentProfileId>" . $this->paymentProfileId . "</customerPaymentProfileId>".
			"</paymentProfile>".
			"<validationMode>".$this->g_validationmode."</validationMode>". // or testMode
			"</updateCustomerPaymentProfileRequest>";
	}


	/**
	 * updateShippingProfile 
	 * @param  int 		$shippingProfileId 
	 * @param  int 		$customerProfileId 
	 * @param  array 	$address           
	 * @return int 		Responds with the net customerAddressId which is AKA the shippingID
	 */
	public function updateShippingProfile( $shippingProfileId, $customerProfileId, $address )
	{
		$this->customerProfileId = $customerProfileId;
		$this->shippingProfileId = $shippingProfileId;

		//$this->customerProfileId =  "35191835";
		$address["address"] = $address["address1"]." ".$address["address2"];
		$address = $this->init_address( $address );

		$this->updateShippingProfile_createXML( $address );
		$this->response = $this->send_xml_request($this->content);

		$parsedresponse = $this->parse_api_response($this->response);
		$error_response = $this->check_xml_response_for_error( $this->response );

		// This error - e00039 - means this user is already in the system.  So grab the id and return it.
		if( $error_response['code'] =='E00039' )
		{
			$existing_address_id = $this->extract_xml_tag( 'customerAddressId', $this->response );
            $return_value =  $parsedresponse->customerAddressId ;
		}
		else if ( $error_response['code'] =='E00040'  ) // Customer profile does not exist.
		{

		//	return 'E00040';
			//$this->createProfile(  )
		}

		if ("Ok" == $parsedresponse->messages->resultCode) {
			$return_value = htmlspecialchars($parsedresponse->customerAddressId);
		}
		else if ("Error" == $parsedresponse->messages->resultCode) 
		{
			$return_value=htmlspecialchars($parsedresponse->messages->message->text);
			$this->filter_error_except_on_passed_codes( $error_response, array( 'E00039' ) );
		}

		$this->last_response = $return_value;
		return $return_value;
	}

	public function updateShippingProfile_createXML( $address )
	{
		$address = $this->validate_array_for_XML( $address );

		//build xml to post
		$this->content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<updateCustomerShippingAddressRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>" . $this->customerProfileId . "</customerProfileId>".
		"<address>".
		"<firstName>". $address["name"] ."</firstName>".
                "<lastName></lastName>".
                "<company>". $address["company"]."</company>".
                "<address>". $address["address"]."</address>".
                "<zip>".  $address["zip"] .  "</zip>".
                "<phoneNumber>".   $address["phone"] ."</phoneNumber>".
				"<customerAddressId>" . $this->shippingProfileId . "</customerAddressId>".
		"</address>".
		"</updateCustomerShippingAddressRequest>";
	}


	/**
	 * insertResponse
	 * insertResponse - Logs the error problem in SQL database.
	 * @param  array 	$parsedresponse 	The response from the XML POST to authorizenet, parsed via simplexml_load_string
	 * @param  int 		$user_id        
	 * @param  bool 	$flag           If true, grabs uid from session
	 * @return none 
	 */
	public function insertResponse( $parsedresponse, $user_id, $flag )
	{
	/*	if(!$flag)
		{
			$result = $this->db->Query( "SELECT  `uid`  FROM  `users` WHERE authorizenet_profile_id = '$user_id' " );
			$out = $this->db->fetchArray($result);
			$user_id=$out['uid'];
		}
		$parsedresponse=get_object_vars($parsedresponse);
		$parsedresponse=json_encode($parsedresponse);
		$query = "INSERT INTO `transaction_log` (user_id, datetime, raw_response ) values ('" . $user_id . "',NOW(),'" . addslashes($parsedresponse) . "') ";
		$this->db->Query($query);*/
	}



	public function getCustomerProfileRequest($customerProfileid)
	{
		$this->customerProfileId = $customerProfileid;
		
		$this->content ="<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<getCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		  $this->MerchantAuthenticationBlock().
		"<customerProfileId>" . $this->customerProfileId . "</customerProfileId>".
		  		"</getCustomerProfileRequest>";

		
		$this->response = $this->send_xml_request($this->content);
		$parsedresponse = $this->parse_api_response($this->response);

		return $parsedresponse;

		if( isset( $parsedresponse->profile ) ){
			foreach ($parsedresponse->profile->paymentProfiles as $listing)
			{
				echo $listing->title;
			}
			echo '<pre/>';print_r($parsedresponse);exit;
		}
	}


	private function debug_x( $transaction_name )
	{
		print "\n$transaction_name REQUEST\n";
		print ( $this->content );
		print "\n\n $transaction_name RESPONSE\n";
		print_r( $this->response );
		print "\nError response:\n";
		print_r( $this->error_response );
		print "\n\n";
	}

	/**
	 * Checks if multiple keys exist in an array
	 *
	 * @param array $array
	 * @param array|string $keys
	 *
	 * @return bool
	 */
	function array_keys_exist( array $array, $keys ) {
    $count = 0;
    if ( ! is_array( $keys ) ) {
        $keys = func_get_args();
        array_shift( $keys );
    }
    foreach ( $keys as $key ) {
        if ( array_key_exists( $key, $array ) ) {
            $count ++;
        }
    }
 
    return $count;
}
}
