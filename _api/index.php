<?php
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors',0);
date_default_timezone_set('America/New_York');
//date_default_timezone_set('Asia/Kolkata');
//date_default_timezone_set('Etc/GMT');

  //Load Directory Conf
  include_once('app/conf/conf.cms.php');   
      //error_reporting(E_ALL);
//ini_set('display_errors', 1);
  //Load DB
 // include_once(DIR_CONF.'conf.my_db.php'); 
  //include_once(DIR_DB.'object.my_db.php');

  //Require Template Engine
  
  //Include Engine
  include_once(DIR_BASE.'engine.php');
  
  include_once(DIR_BASE.'customException.php');

  include_once(DIR_BASE.'base.php');
  include_once(DIR_BASE.'controller.php');
  include_once(DIR_BASE.'model.php');
  include_once(DIR_BASE.'mailSender.php'); 
  include_once(DIR_BASE.'authorizenetCIM.php');
  include_once(DIR_BASE.'derogatory_filter/CensorWords.php');	
  include_once(DIR_BASE.'MCAPI.php');

  foreach (glob("app/controller/*.php") as $filename)
  {
    include_once $filename;
  }
  
  $inspire = new Base;
  $inspire->postdata = $_POST;
  $inspire->init();
  
   
