<?php


class Base extends INSPIRE
{
    
    var $controller;
    var $action;
    var $postdata;
    var $title;
    var $navigation;
    var $db;
    var $languages;
    var $vehicle_class;
    var $body_types;
    var $devices;
    var $apps;
    var $meta_tag;
    var $headerTag;
    var $keywords;
    var $description;
    
    
    function init()
    {
        
        $this->SessionStart();
        $this->ProcessUrl();
		
		if( isset($_GET['noApp']) and $_GET['noApp'] == 'true'){
			$_SESSION['noAppLanding'] = true;
		}
        
        $this->db = $this->db();
        $this->create_template_dir();
        
        foreach ($this->meta_tag as $key => $val){
        	//echo '<br><pre>'.$key;print_r($val);
        	if ($this->UrlArray[0] != '') {
        		if(($key == $this->UrlArray[0])&&($key!='site')){
        			
        			$this->headerTag = $val['HeaderTag'];
        			$this->description = $val['Description'];
        			$this->keywords = $val['Keywords'];
        		}
        		else if($key=='site'){
        			foreach ($val as $keys => $value){
        				if ($this->UrlArray[1] == $keys) {
        					$this->headerTag = $value['HeaderTag'];
        					$this->description = $value['Description'];
        					$this->keywords = $value['Keywords'];
        				}
        			}
        		}
        	}
        	else {
        		$this->headerTag = $this->meta_tag['home']['HeaderTag'];
        		$this->description = $this->meta_tag['home']['Description'];
        		$this->keywords = $this->meta_tag['home']['Keywords'];
        	}
        }
         
        if ($this->UrlArray[0] != '') {
            $this->controller = $this->UrlArray[0];
        } else {
            $this->controller = 'home';
        }
        
        if ($this->UrlArray[1] != '') {
            $this->action = $this->UrlArray[1];
        } else {
            $this->action = 'index';
        }
        
     
        
        $query  = "SELECT * FROM site_social_links WHERE id = 1";
        $result = $this->db->Query($query);
        $social = $this->db->FetchAllArray($result);
        if (isset($social[0])) {
            $social = $social[0];
        } else {
            $social = "";
        }
        
        $social = json_decode(json_encode($social));
        $query      = "SELECT * FROM webpages_data WHERE status=1 ORDER BY orders";
        $result     = $this->db->Query($query);
        $footerMenu = $this->db->FetchAllArray($result);
        $footerMenu = json_decode(json_encode($footerMenu));
        
        $params = $this->process();
        $this->resume_session();
        
        $this->smarty->assign('HeaderTag', $this->headerTag);
        $this->smarty->assign('KeyWords', $this->keywords);
        $this->smarty->assign('Description', $this->description);
        
        $this->smarty->assign('Title', $params->title);
        $this->smarty->assign('title_text', TITLE);
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('API_URL', API_URL);
        $this->smarty->assign('ADMIN_URL', ADMIN_URL);
        $this->smarty->assign('footerMenu', $footerMenu);
        $co = 0;
        
        $checkoutObj = new Checkout();
        $co          = $checkoutObj->getCartCount();
        
        if (isset($_SESSION['fbnoaccess']) && !$_SESSION['fbnoaccess']) {
            unset($_SESSION['fbnoaccess']);
            unset($_SESSION['uid']);
            unset($_SESSION['uname']);
            unset($_SESSION['orders']);
            $cookietime = time() + 60 * 60 * 24 * 60;
            setcookie("reopen_fb_pop", false, $cookietime, '/', NULL);
            setcookie("user_mail", null, -1, '/', NULL);
            setcookie("user_fname", null, -1, '/', NULL);
            setcookie("user_lname", null, -1, '/', NULL);
            setcookie("user_id", null, -1, '/', NULL);
            
            echo '<script type="text/javascript">';
            echo 'alert("Facebook account already using by another user!");';
            echo 'window.location.reload();';
            echo '</script>';
        }
        
        $this->smarty->assign('signOutURL',$this->get_logout_URL());
        $this->smarty->assign('cart', $co);
        die;
         if (Model::isMobile () == true && (!isset($_SESSION['noAppLanding']) || !$_SESSION['noAppLanding']) ) {
        	if(!in_array('app', $this->UrlArray) ) {
        		if(!in_array('gallery', $this->UrlArray) && !$_REQUEST['galleryItem']){ 
        		header ( 'Location: ' . SITE_URL . 'home/app/' );
        		exit; }
        	}
        }
        
        
        $this->smarty->assign('BASE_FARE',BASE_FARE);
        $this->smarty->assign('social', $social);
        $this->smarty->display('index.tpl');
        
    }
    
    
    function create_template_dir()
    {
        if (!is_dir('app/theme/templates_c'))
         mkdir('app/theme/templates_c', 0777);
    }
    
    function resume_session()
    {
        
        
        $model = $this->load_model('sandwichModel');
        $data  = $model->resume_order();
        if (is_array($data) && isset($_SESSION['uid'])) {
            $_SESSION['orders'] = $data;
            
        }
    }
    
    
    function process()
    {
		if (strpos($this->controller, '?') !== false) {
			header ( 'Location: ' . SITE_URL . '' );
			exit ();
		}

		if (! class_exists ( $this->controller )) {
			//echo $this->controller;
			header ( 'Location: ' . SITE_URL . 'error/_404/' );
			exit ();
		}  else {
			$this->LoadSite ();
		}
		
		$obj = new $this->controller ();
		$obj->postdata = $this->postdata;
		$obj->smarty = $this->smarty;
		$obj->db = $this->db;
		$obj->UrlArray = $this->UrlArray;
		
		if (! method_exists ( $obj, $this->action )) {
			header ( 'Location: ' . SITE_URL . 'error/_404/' );
			exit ();
		}
        
        $obj->{$this->action}();
        

        if(isset($_SESSION['temp_sandwich_data'])  && ( !in_array('createsandwich', $this->UrlArray) && !in_array('sandwich', $this->UrlArray) && !in_array('myaccount', $this->UrlArray) && !in_array('error', $this->UrlArray) )) {
        	unset($_SESSION['temp_sandwich_data']);
        	 
        } else if( in_array('sandwich', $this->UrlArray) && in_array('gallery', $this->UrlArray) ){
        	unset($_SESSION['temp_sandwich_data']);
        }
        
        if (( !in_array('createsandwich', $this->UrlArray) && !in_array('sandwich', $this->UrlArray) && !in_array('myaccount', $this->UrlArray) && !in_array('error', $this->UrlArray) )) {
        	if( isset($_COOKIE["sandwich_end_screen"]) && $_COOKIE["sandwich_end_screen"]  ){
        		setcookie("sandwich_end_screen", 'false', time() + 60 * 60 * 24 * 60, "/");
        	}
        }  else if( in_array('sandwich', $this->UrlArray) && in_array('gallery', $this->UrlArray) ){
           if( isset($_COOKIE["sandwich_end_screen"]) && $_COOKIE["sandwich_end_screen"]  ){
        		setcookie("sandwich_end_screen", 'false', time() + 60 * 60 * 24 * 60, "/");
        	}
        }
        

        
        return $obj;
        
    }
    
    
    function br2nl($string)
    {
        return str_replace(array(
            "\r\n",
            "\r",
            "\n"
        ), "\\n", $string);
    }
    
    public function load_model($modelname)
    {
        spl_autoload_register(function($class)
        {
            require_once DIR_MODEL . $class . '.php';
        });
        return new $modelname;
    }
    
    
}
 
