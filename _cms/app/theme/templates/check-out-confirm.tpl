<div class="container">
  
  <div id="content-container">
  <h1>Order Confirmation</h1>
<h2 class="your-good">You're good to go!</h2>
	



  
  <table>
    
      {if $check_now_specific eq 0}

  		<p class="delivery-time" >Estimated   Delivery  time</p>
  		<p class='checkout-order-number'>{if $delivery_type} {$messege} {else} 5 - 10 Minutes {/if}</p>
  		
    {/if}
    
        <p class="delivery-time" >ORDER NUMBER</p>
        <p class='checkout-order-number'>{$order_number} </p>

    

  <table class="table">
      <thead>
        <tr>
          <th>{if $delivery_type}Delivery{else}Delivery Address{/if}</th>
          <th>Billing</th>
          <th>Date/Time</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{if $delivery_type}{$address->name}{else}{$address->store_name}{/if}<br> 
          {if $address->address1 ne ''} {$address->address1}{/if}{if $address->address2 ne ''}, {$address->address2} {/if}<br> 
          {if $address->street ne ''}({$address->street})<br>{/if}
          New York, NY {if $address->zip ne ''} {$address->zip} {/if}<br>
          {if $address->phone ne ''} {$address->phone} {/if}<br>
          {if $address->delivery_instructions ne ''}
          <span>delivery instructions:</span><br> {$address->delivery_instructions}
          {/if}
          </td>
          <td> <span>{if $billing_type ne '' } {$billing_type}</span> <br/> {/if} {if $billing_card_no ne ''} {$billing_card_no} {/if} </td>
          <td>{$date}<br> {$time}</td>
        </tr>
      </tbody>
    </table>
    <div class="br"></div>
      <table class="table discounted-price">
      <thead>
        <tr>
          <th>Items</th>
          <th>Qty</th>
          <th class="text-right">Price</th>
        </tr>
      </thead>
      <tbody>
        {section name="orddet" loop=$order_item_detail}
			<tr>
				<td>		
					<span>{$order_item_detail[orddet]->sandwich_name}{$order_item_detail[orddet]->product_name}</span><br>
					
					
					
					 {if $order_item_detail[orddet]->type == 'user_sandwich'}
					 {assign var = "sandwichDetails"  value=","|explode:$order_item_detail[orddet]->sandwich_details}
           {else}
           {assign var = "sandwichDetails"  value=","|explode:$order_item_detail[orddet]->product_description}
           {/if}
					 
					 {assign var='sandwichDet' value=''}
					 {foreach name="det" from=$sandwichDetails item=foo}
					 
					  {if $smarty.foreach.det.index eq 0}    
						  {assign var='sep' value=""}  
						   {else }
						   {assign var='sep' value=", "}  
						{/if}  
					 
					  {if $foo neq ' '}
						{assign var='sandwichDet' value= $sandwichDet|cat:$sep|cat:$foo  }  
					  {/if}
					 {/foreach}
					 
					 <!-- Sandwich Details with all unwanted commas removed -->
					 {$sandwichDet}
					 
					{if $order_item_detail[orddet]->toast}<br/><span style='font: 18px "lobster_1.4regular";
    color: #342920;'>Toasted</span>{/if}
				</td>
				<td class="padding-left">{$order_item_detail[orddet]->qty}</td>
				<!--<td class="text-right">${$order_item_detail[orddet]->price|string_format:"%.2f"}</td>-->
				<td class="text-right">${math equation="x * y" x=$order_item_detail[orddet]->price y=$order_item_detail[orddet]->qty format="%.2f"}</td>
			</tr>	
		{/section}		
		<tr>
          <td></td>
          <td class="text-right"><span>Subtotal</span></td>
          <td class="text-right">${$sub_total}</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Tax</span></td>
          <td class="text-right">${$tax}</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Delivery</span></td>
          <td class="text-right">${$delivery_fee}</td>
        </tr>
		    <tr>
          <td></td>
          <td class="text-right"><span>Tip</span></td>
          <td class="text-right">${$tip}</td>
        </tr>

        {if $discount gt 0}
        <tr>
          <td></td>
          <td class="text-right"><span>Discount</span></td>
           <td class="text-right">{if $discount gt 0} - {/if}${$discount}</td>
        </tr>
        {/if}
        <tr>
          <td></td>
          <td class="total text-right">Total</td>
          <td class="text-right">${$total}</td>
        </tr>
        </tbody>
    </table>

  
  </div>
  <div id="sidebar">
  <h2 class="your-good">If you have a minute...</h2>
  <div class="accout-info-wrapper">
  <div class="accout-info-form width100">
	   <form id="user_review">
	   

            <p>What can we do to be better?</p>
            <textarea name="message" cols="" rows="" class="text-area-accout-info width100"></textarea>
            <input type="hidden" name="uid" value="{$user->uid}">
            <input type="hidden" name="id" value="{$user_review->id}">
            <a href="javascript:void(0);" class="submit link" id="submit_review">Submit</a> </div>
			</div>
		</form>	
            <div class="clearfix"></div><div class="clearfix"></div>
 			<h2 class="play">Play Some More...</h2>
				<a href="{$SITE_URL}createsandwich/" class="ord-btn">Create a Sandwich</a>
                <a href="{$SITE_URL}sandwich/gallery/" class="ord-btn">Sandwich Gallery</a>
                <a href="{$SITE_URL}sandwich/sandwichMenu/" class="ord-btn">View My Menu</a>
</div>
  </div>
