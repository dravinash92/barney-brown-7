<div class="featured_sandwitch-wrapper menu-list-wrapper">
                    <div class="friend_profile">
                      <div class="friend_profile_img">
                        <img src="https://graph.facebook.com/{$fb_id}/picture?width=107&height=107">
                      </div>
                      <div class="friend_profile_detail">
                        <h4>{$fname}</h4>
                        <p>{$sandwichCount} SANDWICHES</p>
                      </div>
                    </div>
                    <input type="hidden" name="countMenuItems" value="0">
                    <h2><span class="mr_8"></span>
                      Sandwiches
                      <span class="featured-back-arrow">
                       <a href="{$SITE_URL}menu/friendSandwiches"><img src="{$SITE_URL}app/images/back-arrow.png">
                       <h3 class="">BACK</h3></a>
                      </span>
                    </h2>
                     <div class="featured_items menu-listing">
                      <ul>
                        {section name=sandwitch start=0 loop=$featured_data|@count step=1 }
             
              {assign var = 'bread_name' value=$featured_data[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0] }
              {assign var = 'prot_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {assign var =  'bread_name' value = $bread_name|replace:' ':'##' }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
              
                   
                   
                   {if $order_data.user_sandwich.item_id|@is_array} 
                   {assign var = 'items_id' value = $featured_data[$smarty.section.sandwitch.index].id|in_array:$order_data.user_sandwich.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
              
              {if $featured_data[$smarty.section.sandwitch.index].by_admin eq 1}
              {assign var="url" value=$ADMIN_URL}
              {else}
              {assign var="url" value=$SITE_URL}
              {/if}
              
              {assign var="sdesc" value=$featured_data[$smarty.section.sandwitch.index].sandwich_desc}
              {assign var="fname" value=$featured_data[$smarty.section.sandwitch.index].first_name}
              {assign var="lname" value=$featured_data[$smarty.section.sandwitch.index].last_name}
              {assign var="user_name" value=$featured_data[$smarty.section.sandwitch.index].user_name}
              {assign var="sprice" value=$featured_data[$smarty.section.sandwitch.index].current_price}
              {assign var="sname" value=$featured_data[$smarty.section.sandwitch.index].sandwich_name}
               {assign var="tname" value=$featured_data[$smarty.section.sandwitch.index].sandwich_name_trimmed}
                        <li class="featured_item">
                          <input type="hidden" id="featured_sandwich_view_popup" value="1">
                           
                         <div class="featured_img">
                           <img title="{$featured_data[$smarty.section.sandwitch.index].sandwich_name}" class="view_sandwich" data-href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png">
                         </div>
                         <div class="featured_item_detail salads-wrapper">
                           <ul data-sandwich_desc="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc_id}" data-flag="{$featured_data[$smarty.section.sandwitch.index].flag}" data-menuadds="{$featured_data[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$featured_data[$smarty.section.sandwitch.index].uid}" {if $items_id  eq 1} rel="ADD TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-toast="{$featured_data[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$featured_data[$smarty.section.sandwitch.index].formated_date}" data-username="{$featured_data[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$bread_name}"  data-date="{$featured_data[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$featured_data[$smarty.section.sandwitch.index].current_price}" data-id="{$featured_data[$smarty.section.sandwitch.index].id}" data-name="{$featured_data[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$featured_data[$smarty.section.sandwitch.index].like_id}" data-likecount="{$featured_data[$smarty.section.sandwitch.index].like_count}" data-ispublic="{$saved_data[2]}">
                          
                          
                          <input type="hidden" name="chkmenuactice{$featured_data[$smarty.section.sandwitch.index].id}" value="{$featured_data[$smarty.section.sandwitch.index].menu_is_active}" />

                            <img data-href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png" hidden>
                            <input type="hidden" name="image" id="sandImg_{$featured_data[$smarty.section.sandwitch.index].id}" value="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png">
                             <li class="list-one">
                             
                               <h3>{$sname}</h3>
                               <p>
                               {$sdesc}

                             </p>
                               <p class="created_by">Created by: <span style="margin-left: 3px;">{$user_name}.</span></p>
                               <a class="featured_item_btn" href="javascript:void(0);">VIEW</a>
                               <a class="featured_item_btn" href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}">EDIT</a>
                               {if $featured_data[$smarty.section.sandwitch.index].id|in_array:$saved_data}
                               
                               <a class="featured_item_btn saved_btn" href="javascript:void(0);">SAVED</a>
                               <input type="hidden" name="saved_tgl{$featured_data[$smarty.section.sandwitch.index].id}" value="1">
                              {else}
                               <a class="featured_item_btn" id="save_toggle{$featured_data[$smarty.section.sandwitch.index].id}" href="javascript:void(0);">SAVE</a>
                              {/if}

                              {foreach from=$saved_data key=k item=v}
                                  {if $featured_data[$smarty.section.sandwitch.index].id eq $v}
                                    <input type="hidden" value="{$is_public_array.$v}" id="isPublic{$featured_data[$smarty.section.sandwitch.index].id}"> 
                                      {if $is_public_array.$v eq 0}
                                          <span style="vertical-align: sub;"><img title="private" src="{$SITE_URL}images/sandwich_menu/lock.png"></span>
                                      {/if}
                                  {/if}
                                   
                              {/foreach}
                             
                             </li>
                             <input type="hidden" name="hidden_uid" id="hidden_uid" value="{$smarty.session.uid}" >
                             <li class="list-two">
                               <h4>${$sprice}</h4>
                             </li>
                             <li class="list-three">
                               <div class="quantity-control"> 
              
                                  <a href="javascript:void(0);" class="left noaction sandwichQtyLeft"></a>
                                        <input name="sandwichQty{$featured_data[$smarty.section.sandwitch.index].id}" type="text" class="text-box qty" value="01" readonly="">
                                 
                                  <a href="javascript:void(0);" class="right noaction sandwichQtyRight"></a> 
                                    
                                  <input class="typeSandwich" type="hidden" value="FS">
                                  <input type="hidden" value="product" name="data_type">
                
                              </div>
                             </li>
                             <li class="list-four">
                               <!-- <a href="javascript:void(0);">ADD</a> -->
                               <a href="javascript:void(0);" class="quickAddToCart"><img src="{$SITE_URL}app/images/cart.png"><span>ADD</span></a>
                             </li>
                           </ul>
                         </div>
                        </li>
                        {/section}
                       
                      </ul>
                     </div>
                  </div>
