{php} 

$FB = new FB_API();
$access_token  = @$_SESSION['access_token'];

if(!isset($_SESSION['FBRLH_state']) || !$_SESSION['FBRLH_state']  ){

$FB->FB_check_login();
$url =  $FB->FB_get_url();
$_SESSION['tmpUrl'] = $url;
define('_URL',$url);

} else {
define('_URL',$_SESSION['tmpUrl']);

}
 

{/php}


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="{$Description}">
<meta name="keywords" content="{$KeyWords}">
<meta name="author" content="">
<title>{$HeaderTag}</title>

    <meta property="og:url"           content="{$SITE_URL}" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Barney Brown, Inc." />
  <meta property="og:description"   content="Finally, a New York City sandwich service that does it just the way you want it! Check it out at barneybrown.com!" />
  <meta property="og:image"         content="{$SITE_URL}app/images/bb_logo_wood.jpg" />

<link href="{$SITE_URL}app/stylesheets/screen.css?v=2.2" rel="stylesheet">
<link rel="stylesheet" href="{$SITE_URL}app/stylesheets/jquery-ui.css">
<link rel="stylesheet" href="{$SITE_URL}app/stylesheets/jquery.jscrollpane.css"> 
<script>
  global_LAST_CLICKED = false;
global_DATE_OVERRIDE = 0;
global_FORCE_24 = 2;
global_CLOSED = 1;
DO_NOT_MESS_WITH_THE_TIME_BUTTON = false;
</script>


<!--<link rel="stylesheet" href="{$SITE_URL}app/stylesheets/slider.css">-->
<!--[if IE 8]>
 <link rel="stylesheet" href="{$SITE_URL}app/stylesheets/ie8.css">
<![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

 <script type="text/javascript">
var SITE_URL   = "{$SITE_URL}";
var siteurl    = "{$SITE_URL}";
var API_URL    = "{$API_URL}";
var ADMIN_URL  = "{$ADMIN_URL}";
var FB_APP_ID = "{$FB_APP_ID}";
var SESSION_ID = "{if $smarty.session.uid ne ""}{$smarty.session.uid}{else}{php}echo $_COOKIE['user_id'];{/php}{/if}";
var $sandwich_img_live_uri = "{$sandwich_img_live_uri}";
var DATA_ID = "{$DATA_ID}";
var BASE_FARE = "{$BASE_FARE}";
 </script>
 
 
<script src="{$SITE_URL}app/js/jquery.min.js"></script>
<script src="{$SITE_URL}app/js/jquery.ui.min.js"></script>
<script src="{$SITE_URL}app/js/lozad-1.9.0.min.js"></script>
<script src="{$SITE_URL}app/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!--<script src="{$SITE_URL}app/js/jquery.placeholder.js"></script>-->
<script src="{$SITE_URL}app/js/jquery.jscrollpane.min.js"></script>
<script src="{$SITE_URL}app/js/jquery.mousewheel.min.js"></script>
<script src="{$SITE_URL}app/js/sandwichcreator.js"></script>
<script src="{$SITE_URL}app/js/sandwich_quickedit.js?v=2.2"></script>
<script src="{$SITE_URL}app/js/sandwichcart.js?v=2.2"></script>
<script src="{$SITE_URL}app/js/checkout.js?v=2.2"></script>
<script src="{$SITE_URL}app/js/ha_common.js?v=2.2"></script>
<script src="{$SITE_URL}app/js/slides.min.jquery.js"></script>
</head>
<body>
    <div class="dynamic_updates_fixed">
      <div class="left-block"><p> 1 item(s) added to cart.</p></div>
      <div class="right-block"><a href="{$SITE_URL}checkout/choosedeliveryorpickup">CHECKOUT</a></div>
    </div>  
<div id="fb-root"></div> 
{literal}
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
FB.init({
    appId: '557060384864429',
    status: true, 
    cookie: true, 
    xfbml: true
});    
</script>
{/literal}

<div id="outer-wrapper">
 <div id="inner-wrapper">
  <div class="top-control"> 
  
  <a class="deliver-button" href="{$SITE_URL}checkout/choosedeliveryorpickup">Manhattan Delivery</a>
  <p class="timing"> Mon - Fri, 10:00AM - 4:00PM</p>
 <!--  <a href="{$SITE_URL}checkout/choosedeliveryorpickup" class="deliver-button link">delivery <img src="{$SITE_URL}app/images/or.png" width="11" height="22"> Pick-up</a> -->
    <div class="container">
      <!--Header Starting-->
      <div class="header">
        <div class="logo"> <a href="{$SITE_URL}"><img src="{$SITE_URL}app/images/logo.png" alt="{$title_text}"></a> </div>
        <div class="navigation">
          {assign var="currentURL" value=$smarty.server.HTTP_HOST|cat:$smarty.server.REQUEST_URI} 
          <ul>
              <li class="sandwich-menu"><a class="menu-head" href="{$SITE_URL}sandwich/sandwichMenu" >SANDWICHES</a>
                            <div class="rollover">
                              <a href="{$SITE_URL}sandwich/sandwichMenu">MENU</a>
                              <a href="{$SITE_URL}createsandwich">CUSTOMIZE</a>
                              <a href="{$SITE_URL}sandwich/savedSandwiches">SAVED</a>
                              <a href="{$SITE_URL}sandwich/featuredSandwiches">FEATURED</a>
                              <a href="{$SITE_URL}sandwich/gallery">ALL</a>
                              
                              <!-- <a href="{$SITE_URL}sandwich/friendsSandwiches">FRIENDS' SANDWICHES</a> -->
                              <!--<a   href="{php} echo _URL; {/php}" id="showFriendsSandwich"   data-fbrel="true" rel="{if $smarty.session.uid}true{else}false{/if}" class="buttons facebook-login"><span class="links">FRIENDS</span> </a>-->
                              {php}  if(isset($_SESSION['fbnoaccess'])) { {/php}
                                <a href="{$SITE_URL}menu/friendSandwiches" class="buttons"><span class="links">FRIENDS</span> </a>
                              {php} } else { {/php}
                                <a href="javascript:void(0);" data-fbrel="true" rel="{if $smarty.session.uid}true{else}false{/if}" class="buttons login-facebook facebook-login"><span class="links">FRIENDS</span> </a>
                              {php} } {/php}
                            </div>
                           </li>
            </li>

            <li><a href="{$SITE_URL}menu/salads"  {if $currentURL|strstr:"salads"} class="active" {/if}>SALADS</a></li>
            <li><a href="{$SITE_URL}menu/snacks"  {if $currentURL|strstr:"snacks"} class="active" {/if}>SNACKS</a></li>
            <li><a href="{$SITE_URL}menu/drinks"  {if $currentURL|strstr:"drinks"} class="active" {/if}>DRINKS</a></li>
            <li><a href="{$SITE_URL}catering"  {if $currentURL|strstr:"catering"} class="active" {/if}>CATERING</a></li>
          </ul>


        </div>
       <div class="facebook-container"><div class="facebook-container-right">
        {php} /* if( $validtoken === true ) { */  {/php}      
        
<!--             <div class="login-name-holder">
              <h1>Hi, test <span>{$smarty.session.uname }</span></h1>
              <a href="{$signOutURL}">Sign Out</a> 
            </div>
        <a href="{$SITE_URL}myaccount" class="account-profile">MY ACCOUNT</a>
        </div>
          
          <div class="facebook-container-left"> <a href="{$SITE_URL}checkout/choosedeliveryorpickup" class="shopping-cart"><img src="{$SITE_URL}app/images/shopping-cart.png" alt="cart"> 
      <span>{$cart}</span>
          </a> 
      </div>
    <div class="dynamic_updates">
      <div class="left-block"></div>
      <div class="right-block"><a href="{$SITE_URL}checkout/choosedeliveryorpickup" class="  ">CHECKOUT</a></div>
    </div> -->
      
       {php} /* } else */ if(isset($_SESSION['uid'])) { {/php}
       
       
       <div class="login-name-holder">
            <h1>Hi, <span>{$smarty.session.uname }</span></h1>
            <a href="{$signOutURL}">Sign Out</a> 
          </div>
           {php}  if(isset($_SESSION['fbnoaccess'])) { {/php}
          <div class="main-account-profile">
           <img src="https://graph.facebook.com/{php} echo $_SESSION['fb_id']; {/php}/picture?width=57&height=57">
           <a href="{$SITE_URL}myaccount" class="account-profile">MY ACCOUNT</a>
           </div>

            {php}  } else{ {/php}

           <a href="{$SITE_URL}myaccount" class="account-profile" style="width: 100%">MY ACCOUNT</a>
            {php}  }  {/php}
           
          </div>
          
          <div class="facebook-container-left"> <a href="{$SITE_URL}checkout/choosedeliveryorpickup" class="shopping-cart"><img src="{$SITE_URL}app/images/shopping-cart.png" alt="cart">  <span>{$cart}</span></a> 
      </div>
    <div class="dynamic_updates">
      <div class="left-block"></div>
      <div class="right-block"><a href="{$SITE_URL}checkout/choosedeliveryorpickup" class="">CHECKOUT</a></div>
    </div>
       
       {php} } else if($_COOKIE['user_mail'] && $_COOKIE['user_fname'] && $_COOKIE['user_lname'] )  { {/php}
      
       <div class="login-name-holder">
            <h1>Hi, <span>{php} echo $_COOKIE['user_fname']; {/php}</span></h1>
            <a href="{$signOutURL}">Sign Out</a> 
          </div> 
          <div class="main-account-profile">
           <!-- <img src="{$SITE_URL}app/images/user-img.png"> -->
            <a href="{$SITE_URL}myaccount" style="width: 100%" class="account-profile">MY ACCOUNT</a>
           </div>
         
         
          </div>
          <div class="facebook-container-left"> <a href="{$SITE_URL}checkout/choosedeliveryorpickup" class="shopping-cart"><img src="{$SITE_URL}app/images/shopping-cart.png" alt="cart">  <span>{$cart}</span></a> 
       
      </div>
    <div class="dynamic_updates">
      <div class="left-block"></div>
      <div class="right-block"><a href="{$SITE_URL}checkout/choosedeliveryorpickup" class="">CHECKOUT</a></div>
    </div>
       {php} } else  { {/php}
      <a href="javascript:void(0);" class="login-facebook facebook-login">login with facebook</a> <a href="javascript:void(0);" class="login link">login</a>  <a href="javascript:void(0);" class="create-account">Create Account</a>
           
          </div>
          <div class="facebook-container-left"> <a href="javascript:void(0);" class="shopping-cart login link"><img src="{$SITE_URL}app/images/shopping-cart.png" alt="cart">  <span>{$cart}</span></a>
      </div>
      
      <div class="dynamic_updates" style="display: none;">
      <div class="left-block"></div>
      <div class="right-block"><a href="{$SITE_URL}checkout/choosedeliveryorpickup" class="  ">CHECKOUT</a></div>
    </div>
        {php} } {/php}
        
      </div>
      
      
      <!-- <div class="facebook-likes">
      <div class="fb-like" data-href="http://www.barneybrown.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
      </div> -->
      
      
      </div>
      
