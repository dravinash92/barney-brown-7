      <div class="menu-list-wrapper">
	  <a href="{$SITE_URL}sandwich/sandwichMenu" class="back-to-my-menu-button"> <img src="{$SITE_URL}app/images/keep-shopping.png"  alt="Back to my menu button" /> </a>
    
        <h1>CHECKOUT</h1>
        <div class="checkout-wrapper">
          <div class="check-out-left">
			
             {$cartItemList}
			</div>
           
          
          {if $cart ne ""}
          <div class="check-out-right">
            <div class="delivery-details-wrapper">
            <div class="checkoutdynamicdiv">
              {if $order_count gt 0}
               <div class="top-buttons-wrapper">
                 <ul>
				 <!--  Needs to remove width:100% after uncommenting pick-up -->
                   <li style="width:100%;display:none"><a onclick=changeCheckoutoption('deliverychooseaddress') href="javascript:void(0)">DELIVERY</a></li>
				    <!-- REMOVED FOR LAUNCH -->
                     <!--  <li><a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)">PICK-UP</a></li> -->
					<!-- END --> 
                  </ul>
                </div>
              
                <div class="choose-address">
                <p>Choose a</p>
                <a onclick=changeCheckoutoption('deliverychooseaddress') href="javascript:void(0)" class="delivery-address link">DELIVERY ADDRESS</a>
                <!-- REMOVED FOR LAUNCH -->
                  <!--
                <p>Or Pick-Up From</p>
                <a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)" class="delivery-address link">STORE LOCATION</a> --> 
				<!-- END --> 
				</div>
                </div>
              {else}
                <div class="choose-address first_order" style="padding-top: 5px;">
                <p style="margin-bottom: 35px;">Delivery Address</p>
                <a onclick=changeCheckoutoption('deliverychooseaddress') href="javascript:void(0)" class="delivery-address link">ADD DELIVERY ADDRESS <!-- DELIVERY ORDER --></a>
                <p class="first_order_text">{$DELIVERY_FIRST_ORDER}</p>
				 <!-- REMOVED FOR LAUNCH -->
               <!-- <p style="text-align:center">or</p>
                <a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)" class="delivery-address link">PICK-UP ORDER</a> 
                 <p class="first_order_text">{$PICKUP_FIRST_ORDER}</p>
                 -->
				 <!-- END --> 
                </div>
                </div>
              {/if}
              <div class="date-time">
                <p>Date / Time</p> 

                <input type="radio" class="css-checkbox" id="radio1"  onclick="hideDate(true)" name="radiog_lite" value="now">
                <label class="css-label" for="radio1">Now</label>
                
                <input type="hidden" name="todays_date" value="{php} echo date(l); {/php}" />
                   <input type="hidden" name="todays_time" value="{php} echo date('H:00'); {/php}" />
                   
                   <input type="hidden" name="delivery_store_id" value="" />
                
                <input type="radio" class="css-checkbox" id="radio2" onclick="hideDate(false)" {if $currenthour gt 16}checked="checked" {/if} name="radiog_lite" value="specific">
                <label class="css-label" for="radio2">Specific Date/Time</label>
                {if $currenthour lt 16 and $currenthour gte 10}  {else} <script>console.log( "TOO LATE MANUAL FILE" ); _GLOBAL_CLOSED = 1; </script>{/if}

                <div class="date-time-text" style="display: none;">
                     <div class="date-picker">
                    <input name="" type="text" class="datepicker date delivery_date" placeholder="" readonly />
                    <input type="hidden" name="hidden_store_id" id="hidden_store_id" value=""/>
                    <input type="hidden" name="selected_day" id="selected_day" value=""/>
                    <input type="hidden" name="selected_date" id="selected_date" value="0"/>
                       {foreach from=$addresses key=k item=v}
            {if $k==0}
            <input value="{$v->id}" type="hidden" name=="hidden_default_store_id" id="hidden_default_store_id" />
            {/if}
                {/foreach}
                    </div>
                  
                   
                  <div class="time"> <div class="timechange">  {$times}</div>
                   

                  </div>
                </div>
              </div>
              <p>Billing
              <a class="change change-billing" style="display:none;">CHANGE</a>

              </p>
              
              
            
              
              <div class="select_address_wrap" {if $billingInfo|@count eq 0} style="display:none;"{/if}>
			  <select class="billinglist" name="changeBilling" id="changeBilling" >
				<option value="-1">Choose a Credit card</option>
				{section name=billings  start=0 loop=$billingInfo|@count step=1}
				<option value="{$billingInfo[$smarty.section.billings.index].id}">
				{assign var="cardname" value=$billingInfo[$smarty.section.billings.index].card_type|capitalize:true}
				{if $cardname eq "American Express"}  
				{assign var="cardname" value="AMEX" } 
				{else}
				{/if}
				{assign var="cardnos" value=$billingInfo[$smarty.section.billings.index].card_number}
				{$cardname} {$cardnos}
				</option>
				{/section}
				<option value="0">Add New</option>
			</select>
			 </div>
              
              {if $billingInfo|@count eq 0}
              <a id="addcreidt_card" href="javascript:void(0)" class="delivery-address link add-credit-card-link"> {if $order_count gt 0}ADD CREDIT CARD {else} ADD BILLING INFO {/if}</a> 
              {/if}
              

              
              <h6 class="billingDetails" rel="" style="display:none">
             

               <span class="card_typ" style="text-transform: uppercase;">Visa</span> 
               <br>
               Ending in <span class="card_no"></span> <br>

               
               <input type="hidden" name="card_id" value="" />
              </h6>
                                <input type = "hidden" name="_card_number_auth" value="" />
							  <input type = "hidden" name="_expiry_month_auth" value="" />
							  <input type = "hidden" name="_expiry_year_auth" value="" />
							              
              </div>
              
            
             <input type="hidden" value="{$order_string}" name="odstring" />
             <a href="javascript:void(0);" class="place-order link">PLACE ORDER</a></div>
        
        {/if}<!-- if no cart value there -->     
        </div>
      </div>
      <!--Create Sandwich Ending-->
    </div>
  </div>
<!--Outer wrapper Ending-->
<!--Popup Start-->
<div class="popup-wrapper" id="credit-card-details">
  <div class="add-new-address-inner"> <a href="javascript:void(0)" class="close-button">Close</a>
    <div class="title-holder">
      <h1>ADD NEW CREDIT CARD</h1>
    </div>
    <span class="error_msg"></span>
     <form id="billForm">
    <ul class="from-holder">
   
      <li><span class="text-box-holder">
        <p>Card Number</p>
        <input name="cardNo" type="text" class="text-box-control" placeholder="">
        </span> <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="cardCvv" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
      <p>Credit Card</p>
        <span class="credit-card">
        <select name="cardType"  >
               <option value="visa">Visa</option>
          <option value="MasterCard">MasterCard</option>
          <option value="Discover">Discover</option>
          <option value="American Express">American Express</option>
        </select>
        </span> <br><br><br>
        <p>Expiration Date</p>
        <span class="month">
        <select name="cardMonth"  >
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="04">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <select name="cardYear">
         
          
          
{assign var="currentyear" value=$smarty.now|date_format:"%Y" }
{assign var="numyears" value=50}
{assign var="totalyears" value=$currentyear+$numyears}
{section name=loopyers  start=$currentyear loop=$totalyears step=1}
<option>{$smarty.section.loopyers.index}</option>
{/section}
         
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="cardZip" type="text" class="text-box-billing-zip" >
        </span> </li>

      <li> <span class="checkbox-save-bill">
        <input  type="checkbox" id="save_billing"  name="save_billing" value="save_billing" checked="checked"/>
        <label for="save_billing">Save Billing Info <!--<span>(Verisign Encryption)</span>--></label>
        </span> </li>
      <li> <span class="card-holder-margin">
        <h3>Barney Brown meets all requirements for PCI Compliance, customer information is stored using Authorize.NET's secure servers. Customer information is encrypted using a 256-bit SSL.</h3>
        <a href="javascript:void(0)" class="add-address">ADD</a> </span> </li>
    </ul>
    </form>
  </div>
  
  <form id="_payment_form" action="paymentpage/" method="post" style="display:none">
  <input type = "hidden" name="_card_type" value="" />
  <input type = "hidden" name="_card_number" value="" />
  <input type = "hidden" name="_card_cvv" value="" />
  <input type = "hidden" name="_card_zip" value="" />
  <input type = "hidden" name="_card_name" value="" />
  <input type = "hidden" name="_expiry_month" value="" />
  <input type = "hidden" name="_expiry_year" value="" />
  </form>
  
</div>


<!--Popup Ending-->


