
  <h2> Choose Your Cheese </h2>
  <h3><!--  INCLUDES ONE PORTION OF CHEESE. <br> $1.00 PER EXTRA CHEESE. --></h3>
  <div class="protein-select-wrapper scroll-bar " style="position: relative; overflow: visible;">
  
  <ul>
                

                {section name=options start=0 loop=$CHEESE_DATA|@count step=1 }
                
                
            
                
                  <li>
                    <input data-priority = "{$CHEESE_DATA[$smarty.section.options.index].image_priority}" data-image_trapezoid="{$CHEESE_DATA[$smarty.section.options.index].image_trapezoid}" data-image_round="{$CHEESE_DATA[$smarty.section.options.index].image_round}" data-image_long="{$CHEESE_DATA[$smarty.section.options.index].image_long}" data-empty="false" data-id="{$CHEESE_DATA[$smarty.section.options.index].id}"  data-price="{$CHEESE_DATA[$smarty.section.options.index].item_price}"   data-itemname="{$CHEESE_DATA[$smarty.section.options.index].item_name}" type="checkbox" value="" name="check" id="cheese-check{$smarty.section.options.index+1}">
                    <label {if $CHEESE_DATA[$smarty.section.options.index].premium} class="premium-icon" {/if}  for="cheese-check{$smarty.section.options.index+1}">{$CHEESE_DATA[$smarty.section.options.index].item_name}</label>
                  
                    {if $CHEESE_DATA[$smarty.section.options.index].options_id neq 0}
                    
                    <div class="sub-value"> <a href="#" class="left"></a>
                    <span class="text-box">
                    
                  
                     
                    {section name=subval start=0 loop=$CHEESE_DATA[$smarty.section.options.index].options_id|@count step=1}

                    {assign var = "option_id" value=$CHEESE_DATA[$smarty.section.options.index].options_id[$smarty.section.subval.index].id}
                    
                    {assign var = "display" value = "none"}
                    {assign var =  "current" value =  "false"}
                    
                    {if $smarty.section.subval.index eq 0} 
                    
                    {assign var = "display" value =  "block"}   
                    {assign  var = "current" value = "true"}
                    
                    {/if}
                  
                    <input rel="slide" readonly style="display:{$display}" data-unit="{$CHEESE_DATA[$smarty.section.options.index].options_id[$smarty.section.subval.index].option_unit}" data-current="{$current}" data-optionid="{$option_id}" data-image="" data-image_trapezoid="{$CHEESE_DATA[$smarty.section.options.index].option_images[$option_id].tImg}" data-image_round="{$CHEESE_DATA[$smarty.section.options.index].option_images[$option_id].rImg}" data-image_long="{$CHEESE_DATA[$smarty.section.options.index].option_images[$option_id].lImg}" data-price_mul="{$CHEESE_DATA[$smarty.section.options.index].options_id[$smarty.section.subval.index].price_mult}"  type="text" name="" class="text-box" value="{$CHEESE_DATA[$smarty.section.options.index].options_id[$smarty.section.subval.index].option_name}"/>
                    
                    {/section}
                     
                    </span>
            
                   <a href="#" class="right right-hover"></a> 
				   </div> 
                    
                    {/if}
                  
                  </li>
                  
                 {/section}
                  
                  

                </ul>
              </div>