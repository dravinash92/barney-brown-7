		<div class="saved-address-wrapper">
          <ul class="saved-address-inner">
            <li> <span class="add-new-address-main"> <a href="javascript:void(0);" class="change change-address" ><span>+</span><br />
              ADD NEW<br />
              ADDRESS</a> </span>
            </li>
            {if $addresses|@count gt 0 }
         
            {foreach from=$addresses key=k item=v}
				<li> <span class="address-content">
				  <h3> {$v->address1}</h3>
				  <p>{$v->name}<br />
					{if $v->company ne ""}{$v->company}<br />{/if}
					{$v->address1} {if $v->street ne ""}, {$v->street} {/if}<br />										
					{if $v->cross_streets ne "" }{$v->cross_streets} <br /> {/if}
					New York, {$v->zip}<br />
					{$v->phone}{if $v->extn ne ""} ext.{$v->extn}{/if}</p>
				  {if $v->delivery_instructions ne ""}<p>DELIVERY INSTRUCTIONS:<br/>{$v->delivery_instructions|truncate:35:"..."}</p>{/if}<br/>					
				  </span> 
				<span class="button-holder-address">
					<a data-target="{$v->address_id}" class="edit edit_address">EDIT</a>
					<a data-target="{$v->address_id}" class="remove remove_address">remove</a>
				</span>
				</li>
            {/foreach }
            {/if}
          </ul>
        </div>
      </div>
  </div>
  
 
