        <div class="saved-address-wrapper">
          <ul class="saved-address-inner">
            <li> <span class="add-new-address-main"> <a href="#" data-id="0" class="delivery-address add-credit-card"><span>+</span><br />
              ADD NEW<br />
              CREDIT CARD</a> </span> </li>
           
            
            {foreach from=$card_details key=k item=card}			
			{assign var="cardnos" value=$card->card_number|str_split:4}	
				<li>
					<span class="address-content">
						<h3 style="text-transform: uppercase;">{$card->card_type}</h3>
						<p> 
						
							<span style="text-transform:capitalize;">{$card->card_type}</span> <br/>
							{$card->card_number_masked}<br />
							{$card->card_zip}<br />
							{$card->address1}<br />
							{$card->street}
						</p>
					</span> <span class="button-holder-address"> <a href="javascript:void(0)" data-id="{$card->id}" class="edit edit_card">EDIT</a> <a href="javascript:void(0)" data-id="{$card->id}" class="remove remove_card">remove</a> </span>
				</li>
			{/foreach}
            
          </ul>
        </div>
      </div>
  </div>

