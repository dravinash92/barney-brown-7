<div class="banner"> <img src="{$SITE_URL}app/images/sandwich.png" alt="Banner">
        <h1>CREATE YOUR OWN SANDWICH IN 5 EASY STEPS!</h1>
        <a href="{$SITE_URL}createsandwich" class="placeorder">PLACE ORDER</a>
        <div class="thumbnail"> <a href="#"><img src="{$SITE_URL}app/images/thumbnail-active.png" alt="thumbnail"></a> <a href="#"><img src="{$SITE_URL}app/images/thumbnail.png" alt="thumbnail"></a> <a href="#"><img src="{$SITE_URL}app/images/thumbnail.png" alt="thumbnail"></a> </div>
      </div>
     
    </div>
  </div>
  
 <div class="two-column">
    <div class="two-column-inner">
      <div class="container">
        <div class="past-orders">
          <h2>PAST ORDERS</h2>
          <ul>
            <li>
              <p><span class="delivery">Delivery</span> <span class="date-address">06/04/14   205 E 10TH STREET</span></span><span class="price">$36.00</span></p>
              <a href="#" class="reorder-button">reorder</a></li>
            <li>
              <p><span class="delivery">Pick-Up </span> <span class="date-address">06/05/14   MIDTOWN WEST, NYC</span><span class="price">$36.00</span></p>
              <a href="#" class="reorder-button">reorder</a></li>
            <li>
              <p><span class="delivery">Delivery</span> <span class="date-address">06/06/14   205 E 10TH STREET</span><span class="price">$36.00</span></p>
              <a href="#" class="reorder-button">reorder</a></li>
            <li>
              <p><span class="delivery">Pick-Up </span> <span class="date-address">06/07/14   MIDTOWN WEST, NYC</span><span class="price">$36.00</span></p>
              <a href="#" class="reorder-button">reorder</a></li>
            <li>
              <p><span class="delivery">Pick-Up </span> <span class="date-address">06/08/14   MIDTOWN WEST, NYC</span><span class="price">$36.00</span></p>
              <a href="#" class="reorder-button">reorder</a></li>
          </ul>
          <a href="#" class="view-all">VIEW ALL</a> </div>
        <div class="saved-sandwich">
          <h2>SAVED SANDWICHES</h2>
          <ul>
            <li>
              <p><span class="save-sandwich-text">THE JACKHAMMER</span><span class="price">$10.00</span></p>
              <a href="#" class="addtocart-button">ADD TO CART</a></li>
            <li>
              <p><span class="save-sandwich-text">AFTERNOON SPECIAL</span><span class="price">$10.00</span></p>
              <a href="#" class="addtocart-button">ADD TO CART</a></li>
            <li>
              <p><span class="save-sandwich-text">YUMMY DELIGHT</span><span class="price">$10.00</span></p>
              <a href="#" class="addtocart-button">ADD TO CART</a></li>
            <li>
              <p><span class="save-sandwich-text">MOZZ &amp; PICKELS, YUP</span><span class="price">$10.00</span></p>
              <a href="#" class="addtocart-button">ADD TO CART</a></li>
            <li>
              <p><span class="save-sandwich-text">matt's italian</span><span class="price">$10.00</span></p>
              <a href="#" class="addtocart-button">ADD TO CART</a></li>
          </ul>
          <a href="#" class="view-all">VIEW ALL</a> </div>
      </div>
    </div>
  </div>
  
