<table class="table">
      <thead>
        <tr>
          <th>{if $delivery_type eq 0 } Pick-Up  {else} Delivery {/if}</th>
          <th>Billing</th>
          <th>Date/Time</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{$address->name}<br> 
          {if $address->address1 ne ''} {$address->address1} {/if} {if $address->address2 ne ''}, {$address->address2} {/if}<br> 
          {if $address->street ne ''}{$address->street}<br>{/if}
          New York, NY {if $address->zip ne ''} {$address->zip} {/if}<br>
          {if $address->phone ne ''} {$address->phone} {/if}<br>
          {if $address->delivery_instructions ne ''}
          <span>delivery instructions:</span><br> {$address->delivery_instructions}
          {/if}
          </td>
          <td>{if $billing_type ne '' } {$billing_type} <br/> {/if} {if $billing_card_no ne ''} {$billing_card_no} {/if}</td>
          <td>{$date}<br> {$time}</td>
        </tr>
      </tbody>
    </table>
    <div class="br"></div>
      <table class="table">
      <thead>
        <tr>
          <th>Items</th>
          <th>Qty</th>
          <th class="text-right">Price</th>
        </tr>
      </thead>
      <tbody>
        {section name="orddet" loop=$order_item_detail}
			<tr>
				<td>		
					<span>{$order_item_detail[orddet]->sandwich_name}{$order_item_detail[orddet]->product_name}</span><br>
					{$order_item_detail[orddet]->sandwich_details}
				</td>
				<td class="padding-left">{$order_item_detail[orddet]->qty}</td>
				<td class="text-right">${$order_item_detail[orddet]->price}</td>
			</tr>	
		{/section}		
		<tr>
          <td></td>
          <td class="text-right"><span>Subtotal</span></td>
          <td class="text-right">${$sub_total}</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Delivery</span></td>
          <td class="text-right">${$delivery_fee}</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Tip</span></td>
          <td class="text-right">${$tip}</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Discount</span></td>
          <td class="text-right">${$discount}</td>
        </tr>
        <tr>
          <td></td>
          <td class="total text-right">Total</td>
          <td class="text-right">${$total}</td>
        </tr>
        </tbody>
    </table>
