<div id="menu-list-wrapper-id" class="menu-list-wrapper">
  <div class="new_gallery_count">
    <h1>Sandwiches</h1>
    <p>{$galleryCount}</p>
    <span>shared creations</span>
    <a href="{$SITE_URL}sandwich/gallery">VIEW ALL</a>
  </div>
                     

                     <!-- top sandwiches -->
                     <div class="count-menu-items">
                        <!-- <div class="count-menu-column-items">
                           <input type="hidden" name="countMenuItems" value="0">
                           <h2 class="count-menu-column-items-header">Create a Sandwich</h2>
                           <div class="banner-items" id="first-banner-items">
                              <div class="banner-image">
                                 <img src="{$SITE_URL}images/sandwich_menu/Layer_3_copy_6.png" class="banner-items-one">
                              </div>
                              <div class="banner-caption banner-caption-one">
                                 <h2>Create Your Own Sandwich!</h2>
                                 <a class="banner-place-order" href="{$SITE_URL}createsandwich">Get Started</a>
                              </div>   
                           </div>
                        </div> -->
                        <div id="sandwitch_menu_overview" class="count-menu-column-items">
                           <input type="hidden" name="countMenuItems" value="0">
                           <h2 class="count-menu-column-items-header">Create a Sandwich</h2>
                           <div class="banner-items" id="second-banner-items">
                              <div class="banner-image">
                                 <!-- <img src="{$SITE_URL}images/sandwich_menu/Layer_3_copy_6.png" class="banner-items-two"> -->
                                 <div class="homebannerauto overview_auto">
                                   <img src="{$SITE_URL}app/images/ellipsis.svg" class="homeAnimationLoaderImg AnimationLoaderImgOverview" alt="">
                                      <span class="homepageAutoBanner">
                                      <span class="sandwich-1">
                                        <img src="{$SITE_URL}app/images/overviewpage_animation/sandwich1/1.png" style="display:block" alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/2.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/3.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/4.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/5.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/6.png" style="display:none"  alt="">
                                      </span>
                                      
                                      <span class="sandwich-2">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/7.png"  style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/8.png"  style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/9.png"  style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/10.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/11.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/12.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/13.png" style="display:none"  alt="">
                                      </span>
                                      
                                      <span class="sandwich-3">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/14.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/15.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/16.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/17.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/18.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/19.png" style="display:none"  alt="">
                                        <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/20.png" style="display:none"  alt="">
                                      </span>
                                      </span>
        
                                 </div>
                                 {literal}
                                   <script type="text/javascript">
                                   window.addEventListener('load',function(){ ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
                                   </script>
                                  {/literal}
                              </div>
                              <div class="banner-caption banner-caption-two">
                                 <h1>sandwich customizer</h1>
                                 <h2>Build Your Own in 5 Easy Steps</h2>
                                 <a class="banner-place-order" href="{$SITE_URL}createsandwich">customize</a>
                              </div>   
                           </div>
                        </div>
                     </div>

                     <!-- middle sandwiches -->
                     <div class="count-menu-items-two">
                        <div class="count-menu-column-items" id="count-menu-column-items-one">
                           <input type="hidden" name="countMenuItems" value="0">
                           <div class="count-menu-column-items-header">
                              <h2 class="count-menu-column-items-header-left">My Saved Sandwiches</h2>
                              <span>
                                 <img src="{$SITE_URL}images/sandwich_menu/view-all-small.png">
                                 <a href="{$SITE_URL}sandwich/savedSandwiches"><h2 class="count-menu-column-items-header-right">view all</h2></a>
                              </span>
                           </div>

                           <div class="saved-items-middle menu-listing">
         {if !empty($saved_sandwich_data)}
                              <ul>
                                 
                              {section name=sandwitch start=0 loop=$saved_sandwich_data|@count step=1 }
             
              {assign var = 'bread_name' value=$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0] }
              {assign var = 'prot_data' value = $saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {assign var =  'bread_name' value = $bread_name }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
              
                   
                   
                   {if $order_data.user_sandwich.item_id|@is_array} 
                   {assign var = 'items_id' value = $saved_sandwich_data[$smarty.section.sandwitch.index].id|in_array:$order_data.user_sandwich.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
              
              {if $saved_sandwich_data[$smarty.section.sandwitch.index].by_admin eq 1}
              {assign var="url" value=$ADMIN_URL}
              {else}
              {assign var="url" value=$SITE_URL}
              {/if}
              
              {assign var="sdesc" value=$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_desc}
              {assign var="fname" value=$saved_sandwich_data[$smarty.section.sandwitch.index].first_name}
              {assign var="lname" value=$saved_sandwich_data[$smarty.section.sandwitch.index].last_name}
              {assign var="sprice" value=$saved_sandwich_data[$smarty.section.sandwitch.index].current_price}
              {assign var="sname" value=$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_name}
               {assign var="tname" value=$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_name_trimmed}
               <li class="featured_item" data-id="{$saved_sandwich_data[$smarty.section.sandwitch.index].id}">
                          <input type="hidden" id="featured_sandwich_view_popup" value="1">
                              <div class="saved-items">
                                 <div class="saved-image">
                                    <img title="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_name}" class="view_sandwich" data-href="{$SITE_URL}createsandwich/index/{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}/sandwich_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}_{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}.png">
                                     <img src="{$SITE_URL}app/images/toasted.png" class="toasted" style="{if $saved_sandwich_data[$smarty.section.sandwitch.index].menu_toast eq 1 } display:block; {/if}">
                                 </div>
                                 <div class="saved-caption featured_item_detail">
                                    <ul id="toastID_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" data-sandwich_desc="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_desc_id}"data-flag="{$saved_sandwich_data[$smarty.section.sandwitch.index].flag}" data-menuadds="{$saved_sandwich_data[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}" {if $items_id  eq 1} rel="ADD TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-toast="{$saved_sandwich_data[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$saved_sandwich_data[$smarty.section.sandwitch.index].formated_date}" data-username="{$saved_sandwich_data[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$bread_name}"  data-date="{$saved_sandwich_data[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$saved_sandwich_data[$smarty.section.sandwitch.index].current_price}" data-id="{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" data-name="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$saved_sandwich_data[$smarty.section.sandwitch.index].like_id}" data-likecount="{$saved_sandwich_data[$smarty.section.sandwitch.index].like_count}">
                                        <input type="hidden" name="chkmenuactice{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" value="{$saved_sandwich_data[$smarty.section.sandwitch.index].menu_is_active}" />
                                        <img data-href="{$SITE_URL}createsandwich/index/{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}/sandwich_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}_{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}.png" hidden>
                                        <input type="hidden" name="image" id="sandImg_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" value="{$image_path}{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}/sandwich_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}_{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}.png">
                                       <li>
                                          {assign var=jsonData value=$sandwich->sandwich_data|json_decode:1}
                                          <h2><span>{$sname}</span></h2>
                                          <p>{$sdesc}</p>
                                       </li>
                                        <input type="hidden" name="hidden_uid" id="hidden_uid" value="{$smarty.session.uid}" >
                                        <input class="sandwich_id" type="hidden" value="01">
                                        <input type="hidden" value="product" name="data_type">
                                        <input class="typeSandwich" type="hidden" value="SS">
                                       <li class="saved-list">
                                          <h3>${$sprice}</h3>
                                       </li>
                                       <li class="saved-cart">
                                          <a class="saved-add-cart quickAddToCart" href="#"><img src="{$SITE_URL}images/sandwich_menu/cart.png"><span>Add</span></a>
                                       </li>
                                       <li class="saved-list-button">
                                          <a class="saved-place-order" href="javascript:void(0)">VIEW</a>
                                          <!-- <a class="saved-place-order" href="{$SITE_URL}createsandwich/index/{$saved_sandwich_data[$smarty.section.sandwitch.index].id}">EDIT</a> -->
                                          <a class="saved-place-order" href="#">EDIT</a>
                                          {if $saved_sandwich_data[$smarty.section.sandwitch.index].menu_is_active eq 1}
                                          
                                          <a class="featured_item_btn saved_btn" href="#">SAVED</a>
                                          <input type="hidden" name="saved_tgl{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" value="1">
                                          {else}
                                          <a class="featured_item_btn" id="save_toggle{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" href="#">SAVE</a>
                                          {/if}
                                          {if $saved_sandwich_data[$smarty.section.sandwitch.index].is_public eq 0}
                                          <span><img title="private" src="{$SITE_URL}images/sandwich_menu/lock.png"></span>
                                          {/if}
                                       </li>
                                    
                                 </div>
                                 </li> 
                                 {/section} 
                                 </ul> 
                              
                              
                              <div class="saved-items-view">
                                 <img src="{$SITE_URL}images/sandwich_menu/view-all-bold.png">
                                 <a href="{$SITE_URL}sandwich/savedSandwiches"><h2 class="saved-items-view-h2">view all</h2></a>
                              </div>
               {else}
                  <div class="no-saved-sandwiches">
                     <h3>
                     you currently have no saved sandwiches
                     </h3>
                     <p>EITHER CREATE YOUR OWN CUSTOM SANDWICH, CHOOSE FROM OUR USER-
                     SUBMITTED GALLERY, OR GRAB ONE OF YOUR FRIEND'S SANDWICHES.
                     </p>
                  </div>
               {/if}
                        </div>
                        <div  class="count-menu-items-three">
                        <div style="width: 100%;" class="count-menu-column-items" id="count-menu-column-items-three">
                           <input type="hidden" name="countMenuItems" value="0">
                           <div class="count-menu-column-items-header">
                              <img src="{$SITE_URL}images/sandwich_menu/facebook-round.png" class="count-menu-column-items-header-icon">
                              <h2 class="count-menu-column-items-header-left">Friends' Sandwiches</h2>
                              <span>
                                 <img src="{$SITE_URL}images/sandwich_menu/view-all-small.png">
                                 <a href="{$SITE_URL}menu/friendSandwiches"><h2 class="count-menu-column-items-header-right">Friends</h2></a>
                              </span>
                           </div>
                           {if !empty($fbdata)}
                           <div class="friends-items-bottom">
                            {php}$i = 1;{/php}
                            {foreach key=key item=item from=$fbdata}
                            {php} if ($i <=5){ {/php}
                              <div class="friends-items">
                                 <div class="friends-image">
                                    <img src="https://graph.facebook.com/{$item->friend_uid}/picture?width=57&height=57">
                                 </div>
                                 <div class="friends-caption">
                                    <ul>
                                       <li>
                                          <h2>{$item->first_name} {$item->last_name}</h2>
                                          <h3>{$item->sandwich_count} saved sandwiches</h3>
                                       </li>
                                       <li class="friends-cart">
                                          <a class="friends-add-cart" href="{$SITE_URL}menu/friendsMenu/{$item->uid}">view</a>
                                       </li>
                                    </ul>
                                 </div>   
                              </div>
                              {php}$i++; }{/php}
                              
                              {/foreach}
                              <div class="friends-items-view">

                                 <img src="{$SITE_URL}images/sandwich_menu/view-all-bold.png">
                                 <a href="{$SITE_URL}menu/friendSandwiches"><h2 class="friends-items-view-h2">view all</h2></a>

                              </div>
                           </div>
                        {else}
                           <div class="friends-items-bottom not-connected-fb">
                              <p>CONNECT TO FACEBOOK TO SEE YOUR FRIENDS’ SANDWICHES!</p>
                              <div class="friends-items-view">
                                  <img src="{$SITE_URL}images/sandwich_menu/view-all-bold.png">
                                 <a href="#"><h2 class="friends-items-view-h2 login-facebook">CONNECT</h2></a>
                              </div>
                           </div>
                        {/if}
                        </div>
                     </div>

                        </div>

                        <div class="count-menu-column-items" id="count-menu-column-items-two">
                           <input type="hidden" name="countMenuItems" value="0">
                           <div class="count-menu-column-items-header">
                              <h2 class="count-menu-column-items-header-left">Featured Sandwiches</h2><span class="span-update">UPDATED MONTHLY!</span>
                              <span>
                                 <img src="{$SITE_URL}images/sandwich_menu/view-all-small.png">
                                 <a href="{$SITE_URL}sandwich/featuredSandwiches"><h2 class="count-menu-column-items-header-right">view all</h2></a>
                              </span>
                           </div>

                           <div class="saved-items-middle menu-listing">
                               <ul>
                               {section name=sandwitch start=0 loop=$featured_data|@count step=1 }
             
              {assign var = 'bread_name' value=$featured_data[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0] }
              {assign var = 'prot_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {assign var =  'bread_name' value = $bread_name }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
              
                   
                   
                   {if $order_data.user_sandwich.item_id|@is_array} 
                   {assign var = 'items_id' value = $featured_data[$smarty.section.sandwitch.index].id|in_array:$order_data.user_sandwich.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
              
              {if $featured_data[$smarty.section.sandwitch.index].by_admin eq 1}
              {assign var="url" value=$ADMIN_URL}
              {else}
              {assign var="url" value=$SITE_URL}
              {/if}
              
              {assign var="sdesc" value=$featured_data[$smarty.section.sandwitch.index].sandwich_desc}
              {assign var="fname" value=$featured_data[$smarty.section.sandwitch.index].first_name}
              {assign var="lname" value=$featured_data[$smarty.section.sandwitch.index].last_name}
              {assign var="sprice" value=$featured_data[$smarty.section.sandwitch.index].current_price}
              {assign var="user_name" value=$featured_data[$smarty.section.sandwitch.index].user_name}
              {assign var="sname" value=$featured_data[$smarty.section.sandwitch.index].sandwich_name}
               {assign var="tname" value=$featured_data[$smarty.section.sandwitch.index].sandwich_name_trimmed}
               <li class="featured_item" data-id="{$featured_data[$smarty.section.sandwitch.index].id}">
                          <input type="hidden" id="featured_sandwich_view_popup" value="1">
                              <div class="saved-items">
                                 <div class="saved-image">
                                    <img title="{$featured_data[$smarty.section.sandwitch.index].sandwich_name}" class="view_sandwich" data-href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png">
                                 </div>
                                 <div class="saved-caption featured_item_detail">
                                    <ul data-sandwich_desc="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc_id}" data-flag="{$featured_data[$smarty.section.sandwitch.index].flag}" data-menuadds="{$featured_data[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$featured_data[$smarty.section.sandwitch.index].uid}" {if $items_id  eq 1} rel="ADD TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-toast="{$featured_data[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$featured_data[$smarty.section.sandwitch.index].formated_date}" data-username="{$featured_data[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$bread_name}"  data-date="{$featured_data[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$featured_data[$smarty.section.sandwitch.index].current_price}" data-id="{$featured_data[$smarty.section.sandwitch.index].id}" data-name="{$featured_data[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$featured_data[$smarty.section.sandwitch.index].like_id}" data-likecount="{$featured_data[$smarty.section.sandwitch.index].like_count}">
                                        <input type="hidden" name="chkmenuactice{$featured_data[$smarty.section.sandwitch.index].id}" value="{$featured_data[$smarty.section.sandwitch.index].menu_is_active}" />
                                        <img data-href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png" hidden>
                                        <input type="hidden" name="image" id="sandImg_{$featured_data[$smarty.section.sandwitch.index].id}" value="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png">
                                       <li>
                                         {assign var=jsonData value=$sandwich->sandwich_data|json_decode:1}
                                          <h2><span>{$sname}</span></h2>
                                          <p class="mar-bot-0">{$sdesc}</p>
                                           
                                       </li><p class="created_by">Created by: <span>{$user_name}.</span></p>
                                       <input type="hidden" name="hidden_uid" id="hidden_uid" value="{$smarty.session.uid}" >
                                        <input class="sandwich_id" type="hidden" value="01">
                                        <input type="hidden" value="product" name="data_type">
                                         <input class="typeSandwich" type="hidden" value="FS">
                                       <li class="saved-list">
                                           <h3>${$sprice}</h3>
                                       </li>
                                       <li class="saved-cart">
                                          <a class="saved-add-cart quickAddToCart" href="#"><img src="{$SITE_URL}images/sandwich_menu/cart.png"><span>Add</span></a>
                                       </li>
                                       <li class="saved-list-button">
                                          <a class="saved-place-order" href="javascript:void(0)">VIEW</a>
                                          <!-- <a class="saved-place-order" href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}">EDIT</a> -->
                                          <a class="saved-place-order" href="#">EDIT</a>
                                           {if $featured_data[$smarty.section.sandwitch.index].id|in_array:$saved_sandwich_ids}
                                           
                                          <a class="featured_item_btn saved_btn" href="#">SAVED</a>
                                          <input type="hidden" name="saved_tgl{$featured_data[$smarty.section.sandwitch.index].id}" value="1">
                                          {else}
                                          <a class="featured_item_btn" id="save_toggle{$featured_data[$smarty.section.sandwitch.index].id}" href="#">SAVE</a>
                                          {/if}
                                       </li>
                                    </ul>
                                 </div>
                                 </li>
                                 {/section}   
                              
                              <div class="saved-items-view">
                                 <img src="{$SITE_URL}images/sandwich_menu/view-all-bold.png">
                                 <a href="{$SITE_URL}sandwich/featuredSandwiches"><h2 class="saved-items-view-h2">view all</h2></a>
                              </div>
                           </div>
                        </div>

                     <!-- bottom sandwiches -->
                     
                  </div>