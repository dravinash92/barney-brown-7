<div class="menu-list-wrapper">
  <h1>CATERING</h1>

{foreach from=$cateringdataList key=j  item=list}

{if $list.data|@count gt 0 }

<h2>{$list.cat_name}</h2>
<div class="salads-wrapper">

  {foreach from=$list.data key=k item=data}

  <ul>
    <li> 
      <img title="{$data.product_name}" class="view_sandwich cat-image"  src="{$salad_image_path}{$data.product_image}" style=" min-width: 180px;   max-width: 180px;margin: 5px;max-height: 130px;height: 100%;">
    </li>

    <li class="list1 list1-catering" data-id="{$data.id}" data-product_name="{$data.product_name}" data-description="{$data.description}" data-product_image="{$data.product_image}" data-product_price="{$data.product_price}" data-standard_category_id="{$data.standard_category_id}" data-image_path="{$salad_image_path}" data-uid={$uid} data-product="{$product}" data-spcl_instr="{$data.allow_spcl_instruction}" data-add_modifier="{$data.add_modifier}" data-modifier_desc="{$data.modifier_desc}" data-add_modifier="{$data.add_modifier}" data-modifier_isoptional="{$data.modifier_isoptional}" data-modifier_is_single="{$data.modifier_is_single}" data-modifier_options='{$data.modifier_options}'>
      <h3>{$data.product_name}</h3>
      <p>{$data.description}</p>
      <a class="catering-view catering-listing" href="javascript:void(0);">view</a>
    </li>
    <li class="list2">
      <h4>${$data.product_price}</h4>
    </li>
    <li class="list3">
      <div class="quantity-control"> <a href="javascript:void(0)" class="left noaction"></a>
        <input name="spinnerinput" type="text" class="text-box qty" value="01" readonly>
        <a href="javascript:void(0)" class="right noaction"></a> 
        <input class="sandwich_id" type="hidden" value="{$data.id}">
        <input type="hidden" value="product" name="data_type">
      </div>
    </li>
    <li class=""> 
      <a href="#" class="link {if $data.allow_spcl_instruction eq 1 or $data.add_modifier eq 1} catering-listing {else} common_add_item_cart  {/if} add-cart-catering" data-sandwich="product" data-sandwich_id="{$data.id}" data-uid="{$uid}" ><img src="{$SITE_URL}images/sandwich_menu/cart.png"><span>ADD</span></a>              
    </li>
  </ul>

  {/foreach}

</div>

{/if}

{/foreach}

</div> 
