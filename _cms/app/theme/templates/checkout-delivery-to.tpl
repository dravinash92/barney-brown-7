              <div class="top-buttons-wrapper">
                <ul>
				<!--  Needs to remove width:100% after uncommenting pick-up -->
                  <li style="width:100%;display:none" ><a href="javascript:void(0);" class="active">DELIVERY</a></li>
				  <!-- REMOVED FOR LAUNCH -->
                   <!--  <li><a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)">PICK-UP</a></li> -->
				   <!-- END -->  
                </ul>
              </div>
              <div class="choose-address">
                <p>Delivery Address <a href="javascript:void(0);" onclick="changeCheckoutoption('deliverychooseaddress')" class="change change-address">CHANGE</a> &nbsp; 
</p>
                <h6>{$address->name}<br />
				   {if $address->company ne ""}{$address->company}<br /> {/if}
				  {$address->address1}{if $address->street ne ""}, {$address->street}{/if} <br/>				  
				  {if $address->cross_streets ne ""} {$address->cross_streets} <br />{/if}
				  New York - {$address->zip}<br />
                  {$address->phone}<br />
                  {if $address->delivery_instructions ne ""}
					<span>DELIVERY INSTRUCTIONS :</span><br />                  
					{$address->delivery_instructions|truncate:40:"..."}
                  {/if}                  
                  </h6>
                  <input type="hidden" name="address_selected" class="address_selected" value="{$address->address_id}" />
              </div>
              <input type="hidden" name="ziptransfer" value="{$address->zip}" />
             
