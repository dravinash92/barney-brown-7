      <div class="menu-list-wrapper">
        <h1>CHECKOUT</h1>
        <div class="checkout-wrapper">
          <div class="check-out-left">
            <ul class="checkout-header">
              <li class="header-item1">Items</li>
              <li class="header-item2">Qty</li>
              <li class="header-item3">Price</li>
            </ul>
            <ul>
              <li class="check-list1">
                <h3>CUSTOM SANDWICH 1</h3>
                <p>Ciabatta, Turkey (1), Ham (0.5), Pepperoni (0.5), Cheddar (1), Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado, Mayonnaise (N), Blueberry Jam (S)</p>
              </li>
              <li class="check-list3">
                <div class="quantity-control"> <a class="left" href="javascript:void(0)"></a>
                  <input type="text" value="01" class="text-box" name="">
                  <a class="right" href="javascript:void(0)"></a> </div>
              </li>
              <li class="list2">
                <h4>$12.50</h4>
              </li>
              <li class="check-list4"> <a class="remove link"  href="javascript:void(0)">REMOVE</a> <a class="edit link" href="javascript:void(0)">EDIT</a> </li>
            </ul>
            <ul>
              <li class="check-list1">
                <h3>CUSTOM SANDWICH 2</h3>
                <p>Ciabatta, Turkey (1), Ham (0.5), Pepperoni (0.5), Cheddar (1), Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado, Mayonnaise (N), Blueberry Jam (S)</p>
              </li>
              <li class="check-list3">
                <div class="quantity-control"> <a class="left-hover" href="javascript:void(0)"></a>
                  <input type="text" value="02" class="text-box" name="">
                  <a class="right" href="javascript:void(0)"></a> </div>
              </li>
              <li class="list2">
                <h4>$20.50</h4>
              </li>
              <li class="check-list4"> <a class="remove link" href="javascript:void(0)">REMOVE</a> <a class="edit link" href="javascript:void(0)">EDIT</a> </li>
            </ul>
            <ul>
              <li class="check-list1">
                <h3>CUSTOM SANDWICH 3</h3>
                <p>Ciabatta, Turkey (1), Ham (0.5), Pepperoni (0.5), Cheddar (1), Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado, Mayonnaise (N), Blueberry Jam (S)</p>
              </li>
              <li class="check-list3">
                <div class="quantity-control"> <a class="left" href="javascript:void(0)"></a>
                  <input type="text" value="01" class="text-box" name="">
                  <a class="right" href="javascript:void(0)"></a> </div>
              </li>
              <li class="list2">
                <h4>$10.00</h4>
              </li>
              <li class="check-list4"> <a class="remove link" href="javascript:void(0)">REMOVE</a> <a class="edit link" href="javascript:void(0)">EDIT</a> </li>
            </ul>
            <div class="bottom-list">
              <ul>
                <li class="check-list1">
                  <h3>SALTED CASHEWS</h3>
                </li>
                <li class="check-list3">
                  <div class="quantity-control"> <a class="left" href="javascript:void(0)"></a>
                    <input type="text" value="01" class="text-box" name="">
                    <a class="right" href="javascript:void(0)"></a> </div>
                </li>
                <li class="list2">
                  <h4>$3.00</h4>
                </li>
                <li class="check-list4"> <a class="remove link" href="javascript:void(0)">REMOVE</a> </li>
              </ul>
              <ul>
                <li class="check-list1">
                  <h3>GARLIC DILL PICKLE</h3>
                </li>
                <li class="check-list3">
                  <div class="quantity-control"> <a class="left" href="javascript:void(0)"></a>
                    <input type="text" value="01" class="text-box" name="">
                    <a class="right" href="javascript:void(0)"></a> </div>
                </li>
                <li class="list2">
                  <h4>$3.00</h4>
                </li>
                <li class="check-list4"> <a class="remove link" href="javascript:void(0)">REMOVE</a> </li>
              </ul>
              <ul>
                <li class="check-list1">
                  <h3>BOYLAN'S BLACK CHERRY</h3>
                </li>
                <li class="check-list3">
                  <div class="quantity-control"> <a class="left" href="javascript:void(0)"></a>
                    <input type="text" value="01" class="text-box" name="">
                    <a class="right" href="javascript:void(0)"></a> </div>
                </li>
                <li class="list2">
                  <h4>$3.00</h4>
                </li>
                <li class="check-list4"> <a class="remove link"  link href="javascript:void(0)">REMOVE</a> </li>
              </ul>
            </div>
            <div class="sub-total-wrapper">
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <p>Sales Tax Inclusive</p>
                </li>
                <li class="sub-total-list2">
                  <h3>SUBTOTAL</h3>
                  <h4>$55.00</h4>
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <p>Minimum $3.00 Gratuity Required</p>
                </li>
                <li class="sub-total-list2 tip">
                  <h3>TIP</h3>
                  <div class="selectParent">
                    <select>
                      <option value="1">$4.00</option>
                    </select>
                  </div>
                </li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <p>Promo Code / Gift Card</p>
                </li>
                <li class="sub-total-list2">
                  <input name="" type="text" class="sub-total-text">
                </li>
                <li class="sub-total-list3"><a  class="remove link" href="javascript:void(0)">APPLY</a></li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">&nbsp;</li>
                <li class="sub-total-list2">
                  <h5>Total</h5>
                  <h4>$59.00</h4>
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
            </div>
            <div class="save-meal">
              <input name="" type="text" class="save-meal-text">
              <h3>SAVE THIS MEAL? <sub> Name it</sub></h3>
            </div>
          </div>
          <div class="check-out-right">
            <div class="top-not-done">
              <h3>Not done?</h3>
              <a href="javascript:void(0)" class="keep-shop link">KEEP SHOPPING</a> </div>
            <div class="delivery-details-wrapper">
              <div class="top-buttons-wrapper">
                <ul>
				<!--  Needs to remove width:100% after uncommenting pick-up -->
                  <li style="width:100%;display:none"><a onclick=changeCheckoutoption('deliverychooseaddress') href="javascript:void(0)">DELIVERY</a></li>
				  <!-- REMOVED FOR LAUNCH -->
                  <!-- <li><a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)">PICK-UP</a></li> -->
				  <!-- END --> 
                </ul>
              </div>
              <div class="choose-address">
                <p>Choose a</p>
                <a onclick=changeCheckoutoption('deliveryto') href="javascript:void(0)" class="delivery-address link">DELIVERY ADDRESS</a>
                <p>Or Pick-Up From</p>
                <a onclick=changeCheckoutoption('pickupfrom') href="javascript:void(0)" class="delivery-address link">STORE LOCATION</a> </div>
              <div class="date-time">
                <p>Date / Time</p>
                <input type="radio" class="css-checkbox" id="radio1" name="radiog_lite">
                <label class="css-label" for="radio1">Now</label>
                <input type="radio" class="css-checkbox" id="radio2" checked="checked" name="radiog_lite">
                <label class="css-label" for="radio2">Specific Date/Time</label>
                <div class="date-time-text">
                  <div class="date-picker">
                    <input name="" type="text" class="datepicker date" placeholder="(Today) Oct 9" readonly>
                    
                    </div>
                  <div class="time">
                    <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
                      <option>1:00 PM</option>
                    </select>
                  </div>
                </div>
              </div>
              <p>Billing</p>
              <a href="javascript:void(0)" class="delivery-address add-credit-card link">ADD CREDIT CARD</a> </div>
            <a href="javascript:void(0)" class="place-order">PLACE ORDER</a> </div>
        </div>
      </div>
      <!--Create Sandwich Ending-->
    </div>
  </div>
<!--Outer wrapper Ending-->
<!--Popup Start-->
<div class="popup-wrapper" id="credit-card-details">
  <div class="add-new-address-inner"> <a href="javascript:void(0)" class="close-button">Close</a>
    <div class="title-holder">
      <h1>ADD NEW CREDIT CARD</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Credit Card</p>
        <span class="credit-card">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>Visa</option>
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Card Number</p>
        <input name="" type="text" class="text-box-control" placeholder="">
        </span> <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Expiration Date</p>
        <span class="month">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>01 - January</option>
        </select>
        </span> <span class="year">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>2015</option>
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="" type="text" class="text-box-billing-zip" >
        </span> </li>
      <li> <span class="checkbox-save-bill">
        <input id="check2" type="checkbox"  name="check" value="check2">
        <label for="check2">Save Billing Info <span>(Verisign Encryption)</span></label>
        </span> </li>
      <li> <span class="card-holder-margin">
        <h3>Barney Brown meets all requirements for PCI Compliance, customer information is stored using Authorize.NET's secure servers. Customer information is encrypted using a 256-bit SSL.</h3>
        <a href="javascript:void(0)" class="add-address">ADD</a> </span> </li>
    </ul>
  </div>
</div>
<!--Popup Ending-->
{literal}
<script>
function changeCheckoutoption(checkoutoption)
{
//alert(checkoutoption);
$.ajax({
type: "POST",
url: checkoutoption,

})
.done(function( msg ) {
//alert( "Data Saved: " + msg );
$(".checkout-wrapper").html(msg);
});
}
</script>
<script>

$(document).ready(function() {
    $('.right').on('click', function() {
        alert("test");
    });
});
</script>
{/literal}
