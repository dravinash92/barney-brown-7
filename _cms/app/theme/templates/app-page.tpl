<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link href="{$SITE_URL}app/stylesheets/app-screen.css" rel="stylesheet">
</head>

<body>
<div class="container">
	<div class="app-download-screen">
    	<div class="logo"> 
        	<a href="{$SITE_URL}"><img src="{$SITE_URL}app/images/app-logo.png" alt="{$title_text}"></a> 
        </div>
        <div class="banner-image">
        	<a href="javascript:void(0);"><img src="{$SITE_URL}app/images/banner-app-mobile.png" alt="{$title_text}"></a> 
        </div>
        <div class="download-details">
        	<h1>Delivering in</h1>
            <h2>Midtown Manhattan</h2>
            <p>10AM - 4PM</p>
            <h3>Download the app and get started now!</h3>
        </div>
        <div class="download-button">
        	<div class="iphone">
            	<a href="https://itunes.apple.com/us/app/barney-brown/id1097260883?mt=8" target="_blank"><img src="{$SITE_URL}app/images/app-store.png" alt="{$title_text}"></a>
            </div>
        	<div class="android">
            	<a href="https://play.google.com/store/apps/details?id=com.barneybrown" target="_blank"><img src="{$SITE_URL}app/images/google-play.png" alt="{$title_text}"></a>
            </div>
        </div>
        <div class="view-site">
        <h4><a href="{$SITE_URL}home/index/?noApp=true">Continue to Full Website</a></h4>
        </div>
    
	</div>
</div>
</body>
