         <div class="order-history">
         {if $orderhistory|@count gt 0} 
		        
         {foreach from=$orderhistory key=id item=history}
          
			<ul class="order-history-inner" >
	            <li> <span class="section1">
	              <h3>{$history->delivery_type}</h3>
	              <h4>{$history->date}</h4>
	              <a href="javascript:void(0);" data-order="{$history->order_number}" data-order-id="{$history->order_id}" data-bread="{$history->items[0]->bread}" data-sandwich_desc="{$history->items[0]->desc}" data-sandwich_desc_id="{$history->items[0]->desc_id}" data-id="{$history->items[0]->id}" class="view-details">VIEW DETAILS</a> </span> </li>
	            <li > <span class="section2">
	            <h3>
					  {if $history->address->address1 ne ""}{$history->address->address1}{/if}
				</h3>
	              <p>
					{foreach from=$history->items key=id item=item}  
						({$item->qty}) {$item->name} <br/>
						{if $item->desc ne ""}
						
						<span class="item_details" style="display: none;" data-bread="{$item->bread}" data-sandwich_desc="{$item->desc}" data-sandwich_desc_id="{$item->desc_id}" data-id="{$item->id}"></span>
						{/if}
					{/foreach}
	              </p>
	              </span> </li>
	            <li> <span class="section3">
	              <h3>${$history->total}</h3>
	              </span> </li>
	              {if $history->itemCheck gt 0 }
	            <li> <span class="section4"> <a href="javascript:void(0);" data-order="{$history->order_id}" data-from="order_history" class="reorder reorder-button">REORDER</a> </span> </li>{/if}
			</ul>	
					
         {/foreach}
         
        {else}
             <ul class="order-history-inner">
				
	            <li style="width:100%;"><span><h3 class="no_order_history">No Previous Orders.</h3></span></li>	            
	        </ul>    
			
         {/if}
        </div>
      </div>
</div>
