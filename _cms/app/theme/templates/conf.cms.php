<?php

//Directories
define('DIR_BASE', 'app/base/');
define('DIR_CONF', 'app/conf/');
define('DIR_CONTROLLER', 'app/controller/');
define('DIR_DB', 'app/db/');
define('DIR_MODEL', 'app/model/');
define('PROJECT_BASE', 'api/');
define('USERNAME','hashburyuser');
define('PASSWORD','hashburypass');
define('CMS_URL','http://cms.ha.allthingsmedia.com/');
define('DELETE_ORDER_HOUR', 3);
define('ADMIN_EMAIL','michaelk@allthingsmedia.com');
