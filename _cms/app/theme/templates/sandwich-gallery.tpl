
      <!--Menu Ending-->
      <div class="menu-list-wrapper">
        <h1>Sandwich Gallery
        {if $gal_count gt 0 } <span class="sandwitchitemCount" data-count="{$totalCount}"> {$totalCount} Creations</span> {/if}
        </h1>
        <div class="sandwich-gallery-wrapper">
          <div class="sandwich-gallery-left">
            <p>Search</p>
            <form action="{$SITE_URL}sandwich/gallery" id="sandwich-gallery-search" method="post">
              <div class="selectParent">
                <input class="shared-search" type="text" name="search" value="{$serch_term}">
              </div>
              <button type="submit" class="selectParent searchCustom">SEARCH </button>
            </form>
            <input type="hidden" id="searchTerm" value="{$serch_term}">
            <!-- <div class="selectParent searchCustom">
             SEARCH
            </div> -->
            
            
            
            <div class="bread-protein-list">
              
              {foreach from=$categories key=i item=category}
	            
	            {foreach from=$category key=k item=data}
	              {if $data->category_identifier }
	                <h3>  {$data->category_name} </h3>
	                   <ul>	  
	             
	             {if $data->category_name neq "bread" }
	              <li>
                  <input type="checkbox"  value="null" name="check" id="check_{$k}_00">
                  <label for="check_{$k}_00">No {$data->category_name}</label>
                  </li>   
                 {/if}    
                 
		            {foreach from=$categoryItems key=j item=categoryItem}
		            
		            	{foreach from=$categoryItem key=j item=dataitems}
                     {if $data->id eq $dataitems->category_id}
                <li>
                  <input type="checkbox"  value="check1" name="check" id="check_{$k}_{$j}">
                  <label for="check_{$k}_{$j}">{$dataitems->item_name}</label>
                </li>
                {/if}
              
                  {/foreach}
	                 
            {/foreach}
              </ul>
              {/if}  
               {/foreach}
	                 
            {/foreach}
            </div>
          </div>
          <div class="sandwich-gallery-right">
          <input name="total_item_count" type="hidden" value="{$total_item_count}" />
          <input name="sort_type" type="hidden" value="{$sort_id}" />
            <ul class="menu-listing">
              {section name=sandwitch start=0 loop=$GALLARY_DATA|@count step=1 }
             
              {assign var = 'bread_name' value=$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0] }
              {assign var = 'prot_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {assign var =  'bread_name' value = $bread_name|replace:' ':'##' }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
              
                   
                   
                   {if $order_data.user_sandwich.item_id|@is_array} 
                   {assign var = 'items_id' value = $GALLARY_DATA[$smarty.section.sandwitch.index].id|in_array:$order_data.user_sandwich.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
              
              {if $GALLARY_DATA[$smarty.section.sandwitch.index].by_admin eq 1}
              {assign var="url" value=$ADMIN_URL}
              {else}
              {assign var="url" value=$SITE_URL}
              {/if}
              
              {assign var="sname" value=$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}
               {assign var="tname" value=$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name_trimmed}
                <li data-sandwich_desc="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_desc_id}" data-flag="{$GALLARY_DATA[$smarty.section.sandwitch.index].flag}" data-menuadds="{$GALLARY_DATA[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}" {if $items_id  eq 1} rel="ADD TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-toast="{$GALLARY_DATA[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$GALLARY_DATA[$smarty.section.sandwitch.index].formated_date}" data-username="{$GALLARY_DATA[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$bread_name}"  data-date="{$GALLARY_DATA[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_price}" data-id="{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" data-name="{$GALLARY_DATA[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$GALLARY_DATA[$smarty.section.sandwitch.index].like_id}" data-likecount="{$GALLARY_DATA[$smarty.section.sandwitch.index].like_count}"> 
                <span class="inner-holder" style="position: relative">
                <input type="hidden" name="chkmenuactice{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="{$GALLARY_DATA[$smarty.section.sandwitch.index].menu_is_active}" />
                <img data-href="{$SITE_URL}createsandwich/index/{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" width="124" src="{$image_path}{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}/thumbnails/sandwich_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}_{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}.png"   alt="sandwitchimageview">
                <input type="hidden" name="image" id="sandImg_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="{$image_path}{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}/sandwich_{$GALLARY_DATA[$smarty.section.sandwitch.index].id}_{$GALLARY_DATA[$smarty.section.sandwitch.index].uid}.png">

                {if $GALLARY_DATA[$smarty.section.sandwitch.index].id|in_array:$saved_data}
                               <input type="hidden" name="saved_tgl{$GALLARY_DATA[$smarty.section.sandwitch.index].id}" value="1">
                {/if}
                <h3 title="{$sname}">{$tname}</h3>

                <a href="javascript:void(0);" class="link">VIEW</a>       </span> 
                <input class="typeSandwich" type="hidden" value="FS">
                </li>
                <input type="hidden" name="hidden_uid" id="hidden_uid" value="{$smarty.session.uid}" >      
                {foreach from=$saved_data key=k item=v}
                                  {if $GALLARY_DATA[$smarty.section.sandwitch.index].id eq $v}
                                    <input type="hidden" value="{$is_public_array.$v}" id="isPublic{$GALLARY_DATA[$smarty.section.sandwitch.index].id}"> 
                                      
                                  {/if}
                                   
                              {/foreach}         

              
                {/section}                
                
            </ul>
         
            <div class="loadMoreGallery" style="text-align:center"><img src="{$SITE_URL}app/images/star.png" alt="Loading" /> </div>
            

			
          </div>
          
          
          
        </div>
      </div>
      <!--Menu Ending-->
    </div>
  </div>
  <!--Popup Start-->



<!--If user not logged in.-->

