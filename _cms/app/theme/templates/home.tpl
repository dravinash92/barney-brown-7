<div class="bannernew">
      <!-- The container is used to define the width of the slideshow -->
  <div id="slides">
    <div class="slides_container">
    
    
    
    {if $noslide eq 1 }
     
    
    {if $pastBanner.details.by_admin eq 1}
          {assign var="path" value=$ADMIN_URL}    
    {else}
      {assign var="path" value=$SITE_URL} 
    {/if}
    
     
     <div class="slide-home">
            {if $pastBanner.details.image}
              <span class="banner-img-top_wrap">
          <img class="" src="{$SITE_URL}/app/images/banner-top.png" alt="banner-top">
        </span> 
            <span class="banner-image"><img alt="Slide 1" src="{$image_path}{$pastBanner.details.image}"></span>
            {else}
            <span lass="banner-image  "><img alt="Slide 2" src="app/images/app-logo.png" ></span>
            {/if}
            <div class="banner-caption"><h3>Your Most Recent Order</h3>
            <h4>{$pastBanner.street}</h4>
            <p>{if $pastBanner.details.is_delivery eq 0} PICK-UP {else} DELIVERY {/if} - {$pastBanner.date}</p>
            {if $pastBanner.details.bread ne ""}
            <span class="item_details" data-bread="{$pastBanner.details.bread}" data-sandwich_desc="{$pastBanner.details.desc}" data-sandwich_desc_id="{$pastBanner.details.desc_id}" data-id="{$pastBanner.details.item_id}"></span>
            {/if}
            <ul>
            {section name="orderitms" loop=$pastBanner.details.items}
           <li>{$pastBanner.details.items[orderitms]}</li> 
            {/section}
         
            </ul>
            
            <a class="reorder reorder-button link" data-order="{$order_id}" data-from="home_page" href="javascript:void(0);">REORDER</a>
            
            <a class="placeorder link" href="{$SITE_URL}sandwich/sandwichMenu">PLACE NEW ORDER</a></div>
      </div>
     
     
    {elseif $bannercount le 1}
    
      <!--{$single_banner}-->
     <div class="homebannerauto">
     <img src="{$SITE_URL}app/images/ellipsis.svg" class="homeAnimationLoaderImg" alt="">
        <span class="homepageAutoBanner">
                
        <span class="sandwich-1">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/7.png"  style="display:block"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/8.png"  style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/9.png"  style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/10.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/11.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/12.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/13.png" style="display:none"  alt="">
        </span>
        
        <span class="sandwich-2">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/14.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/15.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/16.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/17.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/18.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/19.png" style="display:none"  alt="">
        </span>
        </span>
        <span class="banner-img-top_wrap">
          <img class="" src="{$SITE_URL}/app/images/banner-top.png" alt="banner-top">
        </span>
      </div>
        <div class="banner-captionAuto">    
      <h2>Sandwiches Your Way!</h2>
      {if $uid neq ''}
        <p>Build your own sandwich in 5 easy steps using a <br>curated selection of the finest local ingredients!</p>
        {else}
        <p>Welcome to Barney Brown, a custom-made social-sandwiching experience. Get started by building your own sandwich in just 5 easy steps.</p>
        {/if}
      <a class="home-place-order" href="{$SITE_URL}createsandwich/">Get Started</a>
          </div>
      
     {literal}
     <script type="text/javascript">
     window.addEventListener('load',function(){ window.ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
     </script>
    {/literal}
    
    {else}
    
    <div class="homebannerauto">
        <span class="homepageAutoBanner">
        </span>
    </div>  
        <div class="banner-captionAuto">    
      <h2>Sandwiches Your Way!</h2>
      {if $uid neq ''}
        <p>Build your own sandwich in 5 easy steps using a <br>curated selection of the finest local ingredients!</p>
        {else}
        <p>Welcome to Barney Brown, a custom-made social-sandwiching experience. Get started by building your own sandwich in just 5 easy steps.</p>
        {/if}
      <a class="home-place-order" href="{$SITE_URL}createsandwich/">Get Started</a>
      </div>
          
     {literal}
     <script type="text/javascript">
     window.addEventListener('load',function(){ mobile_ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
     </script>
    {/literal}
    
    
    
          
          {/if}
        
          </div>
</div>
  <!-- End SlidesJS Required: Start Slides -->
</div> 
</div> <!-- container Ends -->
</div><!-- Top control Ends -->
 <div class="two-column">
    <div class="two-column-inner">
      <div class="container">
          {if $uid} 
         
         <div class="past-orders">
         
          <h2>Order History</h2>
           {if $past_order_history}
          <ul>
         
             {section name="order_history" loop=$past_order_history}
             
            <li>
              <p><span class="delivery">{if $past_order_history[order_history]->delivery eq 1}Delivery {else} Pick-up {/if}</span> 
              <span class="date-address">{if $past_order_history[order_history]->delivery eq 1}{$past_order_history[order_history]->address1}{else} {$past_order_history[order_history]->store_name} {/if}</span></span>
              {if $past_order_history[order_history]->bread ne ""}
              <span class="item_details_past" data-bread="{$past_order_history[order_history]->bread}"  data-sandwich_desc="{$past_order_history[order_history]->desc}" data-sandwich_desc_id="{$past_order_history[order_history]->desc_id}" data-id="{$past_order_history[order_history]->item_id}"></span>
              {/if}
              <span class="price">${$past_order_history[order_history]->total}</span></p>
              {if $past_order_history[order_history]->itemCheck gt 0 }
              <a href="javascript:void(0);" data-order="{$past_order_history[order_history]->order_id}" data-from="past_order_history" class="reorder-button link">reorder</a>{/if} 
            </li>
            
             {/section}   
          </ul>
          <a href="{$SITE_URL}myaccount/index/?view=order" class="view-all">VIEW ALL</a> 
          {else}
            <p>You have not placed any orders yet. <br>  Get started now!</p>
            <a href="{$SITE_URL}sandwich/sandwichMenu" class="get-button link">GET STARTED</a>
          {/if}
          </div>
          
        
        

        <div class="saved-sandwich">
        
          <h2>Saved Sandwiches  </h2>
           {if $saved_sandwiches}
          <ul>
         


{if $smarty.session.uid eq 560}
 
{/if}


         {section name="sandwichs" loop=$saved_sandwiches}

            <li>
              <p><span class="save-sandwich-text">{$saved_sandwiches[sandwichs]->sandwich_name}</span><span class="price">${$saved_sandwiches[sandwichs]->current_price}</span></p>
                <a  class="link common_add_item_cart addtocart-button" data-sandwich="user_sandwich" data-sandwich_id="{$saved_sandwiches[sandwichs]->id}" data-uid="{$smarty.session.uid}" data-bread="{$saved_sandwiches[sandwichs]->bread}" data-sandwich_desc="{$saved_sandwiches[sandwichs]->sandwich_desc}" data-sandwich_desc_id="{$saved_sandwiches[sandwichs]->sandwich_desc_id}"  data-id="{$saved_sandwiches[sandwichs]->id}"  href="javascript:void(0);" >ADD TO CART </a></li>
            <li>
         {/section}   
             
          </ul>
          <a href="{$SITE_URL}sandwich/savedSandwiches" class="view-all">VIEW ALL</a> 
          {else}
            <p>You currently have no saved sandwiches. <br> Create one now!</p>
            <a href="{$SITE_URL}createsandwich/" class="get-button link">GET STARTED</a>

          {/if}
          </div> 
          
        {else}
        

        
      <ul class="sandwich-main-link-box">
          <li>
            <h1>SANDWICH MENU</h1>
            <h2>A Menu of Endless Possibilities</h2>
            <ul class="create-sandwich">
              <li><a href="javascript:void(0);">- SANDWICH CUSTOMIZER</a></li>
              <li><a href="javascript:void(0);">- SAVED SANDWICHES</a></li>
              <li><a href="javascript:void(0);">- FEATURED SANDWICHES</a></li>
              <li><a href="javascript:void(0);">- SANDWICH GALLERY</a></li>
              <li><a href="javascript:void(0);">- FRIENDS’ SANDWICHES</a></li>
            </ul>
            <a class="get-button link" href="{$SITE_URL}sandwich/sandwichMenu">VIEW THE MENU</a>
          </li>
          <li>
            <h1>FEATURED SANDWICHES</h1>
            <h2>Handcrafted by Our Community</h2>
            <ul class="create-sandwich">
              <li><a href="javascript:void(0);">- NEW LINEUP EVERY MONTH</a></li>
              <li><a href="javascript:void(0);">- CREATED BY OUR COMMUNITY</a></li>
              <li><a href="javascript:void(0);">- CHOSEN FROM THOUSANDS</a></li>
              <li><a href="javascript:void(0);">- SAVE THE ONES YOU LIKE!</a></li>
            </ul>
            <a class="view-collection link" href="{$SITE_URL}sandwich/featuredSandwiches/">VIEW THE MENU</a> </li>
            <li>
            <h1>CATERING</h1>
            <h2>Delivery Anywhere in Manhattan</h2>
             <ul class="create-sandwich">
              <li><a href="javascript:void(0);">- BAGGED LUNCHES &amp; PLATTERS</a></li>
              <li><a href="javascript:void(0);">- CORPORATE EVENTS</a></li>
              <li><a href="javascript:void(0);">- PERSONAL OCCASIONS</a></li>
              <li><a href="javascript:void(0);">- HOLIDAYS</a></li>
              <li><a href="javascript:void(0);">- SPORTING EVENTS</a></li>
            </ul>
            <a class="view-menu link" href="{$SITE_URL}catering">VIEW THE MENU</a> 
           </li>
      </ul> 
        
        
        {/if} 

      </div>
    </div>
  </div>
 
