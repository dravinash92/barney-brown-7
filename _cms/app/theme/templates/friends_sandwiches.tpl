<div class="featured_sandwitch-wrapper menu-list-wrapper">
  <h1>FRIENDS' SANDWICHES</h1>
  <input type="hidden" name="countMenuItems" value="0">
  <h2><span class="mr_8"></span>Friends' Sandwiches
    <span class="featured-back-arrow">
     <a href="{$SITE_URL}sandwich/sandwichMenu"><img src="{$SITE_URL}app/images/back-arrow.png">
     <h3 class="">BACK</h3></a>
   </span>
 </h2>
{if $fbCheck == 1}
  {if !empty($data)}
    <div class="friends-list">
        <ul class="">
          {foreach key=key item=item from=$data}
          <li class="">
              <div class="badge_img">
                <img src="https://graph.facebook.com/{$item->friend_uid}/picture?width=107&height=107">
              </div>
              <div class="friend-detail">
                <h4>{$item->first_name} {$item->last_name}</h4>
                <p>{$item->sandwich_count} Sandwiches</p>
                <a href="{$SITE_URL}menu/friendsMenu/{$item->uid}">VIEW</a>                 
              </div>
          </li>
          {/foreach}
        </ul>
    </div>
    {else}
    <div class="friends-share">
    <p>LET YOUR FRIENDS KNOW ABOUT BARNEY BROWN!</p>
    <a href="javascript:void(0);" id="Share-BarneyBrown"><img class="friends-share-img" src="{$SITE_URL}app/images/friends_share.png"></a>
    
  </div>
    {/if}
{else}

<div class="friends-share">
  <p>PLEASE CONNECT TO FACEBOOK TO SEE FRIENDS' SANDWICHES</p>
  <a href=""><img class="friends-share-img" src="{$SITE_URL}app/images/friends_connect.png"></a>
  
</div>
{/if}



</div>