{if $itemsCount gt 0}  
			<ul class="checkout-header">
              <li class="header-item1">Items</li>
              <li class="header-item2">Qty</li>
              <li class="header-item3">Price</li>
            </ul>
             {section name=checkout start=0 loop=$checkout_items|@count step=1 }
                    
            <ul>
              <li class="check-list1">
                <h3>{$checkout_items[$smarty.section.checkout.index].sandwich_name}</h3>  
                <p>{$checkout_items[$smarty.section.checkout.index].data_string}</p>
              </li>
              <li class="check-list3">
                <div class="quantity-control"> <a class="left" href="javascript:void(0)"></a>
                  <input type="text" value="{$checkout_items[$smarty.section.checkout.index].item_qty}" class="text-box qty" name="">
                  <a class="right" href="javascript:void(0)"></a> 
                    <input type="hidden" class="sandwich_id" value="{$checkout_items[$smarty.section.checkout.index].id}" />
                </div>
                
              </li>
              <li class="list2">
                <h4 class="item_price">${$checkout_items[$smarty.section.checkout.index].item_price}</h4>
              </li>
              <li class="check-list4"> 
				<a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="{$checkout_items[$smarty.section.checkout.index].order_item_id}" data-item="{$checkout_items[$smarty.section.checkout.index].item_id}">REMOVE</a> 
				<a class="edit link" href="{$SITE_URL}createsandwich/index/{$checkout_items[$smarty.section.checkout.index].item_id}">EDIT</a> 
			  </li>
            </ul>
            {/section}
            <div class="sub-total-wrapper">
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <p>Sales Tax Inclusive</p>
                </li>
                <li class="sub-total-list2">
                  <h3>SUBTOTAL</h3>
                  <h4>$<span class="sub-total">{$total}</span></h4>
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <p>Minimum $3.00 Gratuity Required</p>
                </li>
                <li class="sub-total-list2 tip">
                  <h3>TIP</h3>
                  <div class="selectParent">
                    <select>
                      <option value="1">$4.00</option>
                    </select>
                  </div>
                </li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <p>Promo Code / Gift Card</p>
                </li>
                <li class="sub-total-list2">
                  <input name="" type="text" class="sub-total-text">
                </li>
                <li class="sub-total-list3"><a  class="remove link" href="javascript:void(0)">APPLY</a></li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">&nbsp;</li>
                <li class="sub-total-list2">
                  <h5>Total</h5>
                  <h4>$<span class="grand-total">{$grandtotal}</span></h4>
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
            </div>
            <div class="save-meal">
              <input name="" type="text" class="save-meal-text">
              <h3>SAVE THIS MEAL? <sub> Name it</sub></h3>
            </div>
             {else}
			<ul>
				<li>
				<h3>No Items in cart.</h3>  
				</li>
			</ul>				
                
            {/if}
