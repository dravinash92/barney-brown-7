
   {assign var="currentURL" value=$smarty.server.HTTP_HOST|cat:$smarty.server.REQUEST_URI}
 
   {if $SITE_URL|strstr:$currentURL}
   {else}
   </div>
  </div>
  {/if}
   

   <div class="push"></div> 
   </div></div>
<div class="footer">
    <div class="footer-inner">
      <div class="container">
        <p class="telephone-number">212.634.9114</p>
        <ul class="footer-link">         
          {foreach from=$footerMenu key=k item=page}
			<li><a href="{$SITE_URL}{$page->slug}">{$page->webpage_name}</a></li>
          {/foreach}          
        </ul>
         <ul class="social-link">
          <li><a href="{$social->instagram}"><img src="{$SITE_URL}app/images/instagram.png" alt="Instagram"></a></li>
          <li><a href="{$social->twitter}"><img src="{$SITE_URL}app/images/twitter.png" alt="Twitter"></a></li>
          <li><a href="{$social->facebook}"><img src="{$SITE_URL}app/images/facebook.png" alt="Facebook"></a></li>          
        </ul>
      </div>
    </div>
  </div>
  
<div class="popup-wrapper" id="regiter-form">
  <form name="signupForm" id="signupForm">
      <div class="login-inner"> <a href="javascript:void(0);" class="close-button">Close</a> <img src="{$SITE_URL}app/images/popup-logo.png" class="popup-logo" alt="logo">
        <h1>Register</h1>
        <span class="rerror_msg"></span>
        <div class="form-holder">
          <input class="text-firstname" id="rfname" name="firstname" type="text" placeholder="First Name">
          <input class="text-lastname" id="rlname" name="lastname" type="text" placeholder="Last Name">
        </div>
        <input class="text-email" id="remail" name="email" type="text" placeholder="Email">
        <input class="text-password" id="rpwd" name="password" type="password" placeholder="Password">

       <a href="javascript:void(0)" id="create_account" class="login-button">CREATE ACCOUNT</a>
        <h6>By creating an account you accept our <a href="{$SITE_URL}site/termsofuse">Terms of Use</a> and <a href="{$SITE_URL}site/privacypolicy">Privacy Policy.</a></h6>
      </div>
  </form>
</div>

<div class="popup-wrapper" id="login-form">
 <div class="login-inner"> <a href="javascript:void(0);" class="close-button">Close</a> <img src="{$SITE_URL}app/images/popup-logo.png" class="popup-logo" alt="logo">
    <span class="error_msg"></span>
    <h1>Login</h1>
   <!-- LOGIN FORM -->
    <form name="loginForm" id="loginForm">
    <input class="text-email" id="lemail" name="email" type="text" placeholder="Email">
    <input class="text-password" id="lpwd" name="password" type="password" placeholder="Password">
   
    <a href="{$SITE_URL}home/loggedin" id="login_btn" class="login-button">LOG IN</a>
     </form>
    <h6><a href="javascript:void(0);" class="bold-text forgot-password">Forgot Password</a></h6>
    <h6>By creating an account you accept our <a href="{$SITE_URL}site/termsofuse">Terms of Use</a> and <a href="{$SITE_URL}site/privacypolicy">Privacy Policy.</a></h6>
  </div>
</div>


{if $user_mail }

<div class="popup-wrapper" id="login-form-cookie">
 <div class="login-inner"> <a href="javascript:void(0);" class="close-button">Close</a> <img src="{$SITE_URL}app/images/popup-logo.png" class="popup-logo" alt="logo">
    <span class="error_msg"></span>
    <h1>Confirm Password</h1>
    <!-- LOGIN FORM -->
    <form name="loginForm_cookie" id="loginForm_cookie" >
    <br>
    <p><span style="text-transform:capitalize;">{$user_fname}</span>, please verify your password to perform this action.</p>
    
    <input class="text-email" value="{$user_mail}" id="lemail_cookie" name="email" type="hidden" placeholder="Email">
    <input class="text-password" id="lpwd_cookie" name="password" type="password" placeholder="Password">
    <!--<input type="submit" id="login_submit" class="login-button" value="LOG IN">-->
   
    <a href="{$SITE_URL}home/loggedin" id="login_btn_cookie" class="login-button">LOG IN</a>
     </form>
    <h6><a href="javascript:void(0);" class="bold-text forgot-password">Forgot Password</a></h6>
    <h6>By creating an account you accept our <a href="{$SITE_URL}site/termsofuse">Terms of Use</a> and <a href="{$SITE_URL}site/privacypolicy">Privacy Policy.</a></h6>
  </div>
</div>

{/if}


<!-- Forgot password -->
<div class="popup-wrapper" id="forgot-form">
  
  <div class="login-inner"> <a href="javascript:void(0);" class="close-button">Close</a> <img src="{$SITE_URL}app/images/popup-logo.png" class="popup-logo" alt="logo">
    <span class="error_msg"></span>
    <h1>Forgot Password</h1>
    <form name="forgotForm" id="forgotForm">
    <input class="text-email" id="femail" name="email" type="text" placeholder="Email">
   

   
    <a href="javascript:void(0)" id="forgot_btn" class="login-button">SUBMIT</a>
     </form>
       <h6><a href="javascript:void(0);" class="bold-text user-login">Login</a></h6>
        <h6>By creating an account you accept our <a href="{$SITE_URL}site/termsofuse">Terms of Use</a> and <a href="{$SITE_URL}site/privacypolicy">Privacy Policy.</a></h6>
    
  </div>
</div>

<div class="popup-wrapper" id="login">
  <div class="please-login"> <a href="javascript:void(0);" class="close-button">Close</a> <img src="{$SITE_URL}app/images/popup-logo.png" class="popup-logo" alt="logo">
    <h1>Login or register to proceed!</h1>
    <!-- <p>Please log in to add to cart. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctu.</p> -->
    <a href="{php} echo _URL; {/php}" id="fbLogin_alt" class="list-buttons connect-color ">CONNECT WITH FACEBOOK <h6>VIEW FRIENDS' SANDWICHES</h6></a> <a href="javascript:void(0);" class="list-buttons user-login">LOGIN</a> <a href="javascript:void(0);" class="list-buttons create-account-btn">CREATE ACCOUNT</a> </div>
</div>

<div class="popup-wrapper" id="facebook-login">
  <div class="log-facebook-inner"> <a href="javascript:void(0);" class="close-button">Close</a> <img src="{$SITE_URL}app/images/popup-logo.png" class="popup-logo" alt="logo">
    <h1>Login with Facebook</h1>
    <p>This action requires you to be logged in with Facebook.</p>
    <a href="{php} echo _URL; {/php}" id="fbLogin" class="facebook-button connect-color ">  CONNECT WITH FACEBOOK</a>
    <h6>By creating an account you accept our <a href="{$SITE_URL}site/termsofuse">Terms of Use</a> and <a href="{$SITE_URL}site/privacypolicy">Privacy Policy.</a></h6>
  </div>
</div>

<div class="popup-wrapper" id="add-new-address">
  <div class="add-new-address-inner"> <a href="javascript:void(0);" class="close-button">Close</a>
	<form class="add_address" method="post" >
    <div class="title-holder">
      <h1>ADD NEW ADDRESS</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Recipient Name</p>
        <input name="recipient" type="text" class="text-box-control required" value="{$smarty.session.uname } {$smarty.session.lname }">
        </span> <span class="text-box-holder">
        <p>Company (optional)</p>
        <input name="company" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Street Address </p>
        <input name="address1" type="text" class="text-box-control required" required >
        </span> <span class="text-box-holder" >

        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control required" required>
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" >
        </span> <span class="text-box-holder">
        <p>Phone Number</p>
        <input name="phone1" id="phone1" type="text" maxlength="3" class="text-box-phone" >
        <input name="phone2" id="phone2" type="text" maxlength="3" class="text-box-phone" >
        <input name="phone3" id="phone3" type="text" maxlength="4" class="text-box-phone margin-none" >
        </span> <span class="text-box-holder1">
        <p>Ext.</p>
        <input name="extn" id="phoneext" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <h2>New York, NY</h2>
        <span class="text-box-zip-holder1">
        <p>Zip Code</p>
        <input name="zip" type="text" class="text-box-zip required" required >
        </span> </span>
        <h3>{$title_text} currently delivers in Manhattan from from Battery Park to 100th St.</h3>
      </li>
      <li> <span class="delivery-instructions">
        <p>Delivery Instructions</p>
        <input name="delivery_instructions" type="text" class="text-box-delivery" >
        </span> </li>
      <li> <a href="javaScript:void(0)" class="add-address save_address">ADD ADDRESS</a> </li>
    </ul>
    </form>
  </div>
</div>

<div class="popup-wrapper" id="credit-card-details">
  <div class="add-new-address-inner"> <a href="javascript:void(0);" class="close-button">Close</a>
    <div class="title-holder">
      <h1>ADD NEW CREDIT CARD</h1>
    </div>
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Credit Card</p>
        <span class="credit-card">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>Visa</option>
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Card Number</p>
        <input name="" type="text" class="text-box-control" placeholder="">
        </span> <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Expiration Date</p>
        <span class="month">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>01 - January</option>
        </select>
        </span> <span class="year">
        <select name="jumpMenu" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>2015</option>
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="" type="text" class="text-box-billing-zip" >
        </span> </li>
      <li> <span class="checkbox-save-bill">
        <input id="check2" type="checkbox"  name="check" value="check2">
        <label for="check2">Save Billing Info <span>(Verisign Encryption)</span></label>
        </span> </li>
      <li> <span class="card-holder-margin">
        <h3>Barney Brown meets all requirements for PCI Compliance, customer information is stored using Authorize.NET's secure servers. Customer information is encrypted using a 256-bit SSL.</h3>
        <a href="javascript:void(0);" class="add-address">ADD</a> </span> </li>
    </ul>
  </div>
</div>

<div class="popup-wrapper" id="edit-address">
  <div class="add-new-address-inner">
	
  </div>
</div>







<div class="popup-wrapper" id="credit-card-edit-details" style="display: none;">
  <div class="add-new-address-inner"> <a href="javascript:void(0)" class="close-button">Close</a>
    <div class="title-holder">
      <h1>EDIT CREDIT CARD</h1>
    </div>
    <span class="error_msg"></span>
     <form id="edit-billForm">
	
    <ul class="from-holder">
   
      <li><span class="text-box-holder">
        <p>Card Number</p>
        <input name="card_number" type="text" class="text-box-control" placeholder="">
        </span>
        <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="card_cvv" type="text" class="text-box-control" >
        </span>
        </li>
      <li> <span class="text-box-holder">
      <p>Credit Card</p>
        <span class="credit-card">
        <select name="card_type">
          <option value="visa">Visa</option>
          <option value="MasterCard">MasterCard</option>
          <option value="Discover">Discover</option>
          <option value="American Express">American Express</option>
        
        </select>
        </span> <br><br><br>
        <p>Expiration Date</p>
        <span class="month">
        <select name="expire_month">
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="04">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <select name="expire_year">
		<option>2015</option>
		<option>2016</option>
		<option>2017</option>
		<option>2018</option>
		<option>2019</option>
		<option>2020</option>
		<option>2021</option>
		<option>2022</option>
		<option>2023</option>
		<option>2024</option>
		<option>2025</option>
		<option>2026</option>
		<option>2027</option>
		<option>2028</option>
		<option>2029</option>
		<option>2030</option>
		<option>2031</option>
		<option>2032</option>
		<option>2033</option>
		<option>2034</option>
		<option>2035</option>
		<option>2036</option>
		<option>2037</option>
		<option>2038</option>
		<option>2039</option>
		<option>2040</option>
		<option>2041</option>
		<option>2042</option>
		<option>2043</option>
		<option>2044</option>
		<option>2045</option>
		<option>2046</option>
		<option>2047</option>
		<option>2048</option>
		<option>2049</option>
		<option>2050</option>
		<option>2051</option>
		<option>2052</option>
		<option>2053</option>
		<option>2054</option>
		<option>2055</option>
		<option>2056</option>
		<option>2057</option>
		<option>2058</option>
		<option>2059</option>
		<option>2060</option>
		<option>2061</option>
		<option>2062</option>
		<option>2063</option>
		<option>2064</option>
         
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="card_zip" type="text" class="text-box-billing-zip">
        </span> </li>
      <input type="hidden" name="id" value="" />
      <input type="hidden" name="uid" value="" />
      <input type="hidden" value="1" name="display_dropdown"/>
      <li> <span class="card-holder-margin">
        <h3>Barney Brown meets all requirements for PCI Compliance, customer information is stored using Authorize.NET's secure servers. Customer information is encrypted using a 256-bit SSL.</h3>
        <a href="javascript:void(0)" class="add-address save-card-detail">Save</a> </span> </li>
    </ul>
    </form>
  </div>
  
</div>

<div class="popup-wrapper" id="ajax-loader" style="display: none;">
		<img src="{$SITE_URL}/app/images/ajax-loader.gif" />
</div>
<div class="popup-wrapper" id="product-descriptor">
  <div class="add-new-address-inner"> <a href="javascript:void(0);" class="close-button">Close</a>
	<form class="add_address" method="post" >
    <div class="title-holder">
      <h1></h1>
    </div>
    <ul class="from-holder">
      
    </ul>
    <div class="hidden_fields"></div>
    <input type="button" class="add_desc" name="add_desc" value="ADD TO CART"/>
    </form>
   </div>
</div>   

<div class="popup-wrapper" id="order-detail-pop">
	<div class="add-new-address-inner"> <a href="javascript:void(0);" class="close-button">Close</a>
	<div class="title-holder">
      <h1>ORDER DETAILS  <a href="javascript:void(0);" data-order="" class="reorder-button reorder-arrow">REORDER</a></h1>
     
    </div>
		<div class="content">
					
		</div>    
	</div>
</div>       
<div class="popup-wrapper" id="warningcheckout">
	<div class="added-menu-sandwiches-inner">
	<a href="javascript:void(0);" class="close-button">Close</a>
		<div class="added-good-news">
	  	<div class="added-good-news-msg">
		    <div class="title-holder">
		      <h1>Good news!</h1>
		    </div>
		    <div class="title2-holder">
		    	<p>Although Barney Brown does not currently offer regular lunch service in this area, same-day orders of $100.00 or more are accepted to this address!</p>
		    </div>
	    </div>
		</div>
	    <div class="button-holder"> 
	      <a href="javascript:void(0);" id="warningOk" class="add-to-cart">OK</a>
	    </div>
	</div>
</div>
<input type="hidden" value="{$SITE_URL}" class="siteurl" />
<input name="recipientName" type="hidden" value="{$smarty.session.uname } {$smarty.session.lname }">

<!-- Snadwhich popup view -->
<div class="popup-wrapper" id="sandwiches-popup" style="display: none;">
  <div class="popup-wrapper quickedit-loader" id="sandwiches-popup_ajax_loader" style="display: none;">
    <img src="{$SITE_URL}app/images/ajax-loader.gif">
  </div>
  <div class="added-menu-sandwiches-inner"> <a href="javascript:void(0);" class="close-button">Close</a> 
    <img src="{$SITE_URL}app/images/added-menu-sandwiches.png" alt="">
    <div class="title-holder">
      <h1>VASANTH'S PEPPERONI TREAT</h1>
      <h2>$10.00</h2>
    </div>

    <p>Whole Wheat Wrap, Pepperoni (1.0), Muenster (N), Cucumbers (H), Dijon Mustard (S)</p>

    <div class="title2-holder">
      <div class="checkbox-holder">
        <input id="check18" type="checkbox" class="menucheck savetousertoastnew" name="check" value="check18">
        <label for="check18">Toast It! <!--<span>Yum!</span>--></label>
      </div>
      <p>Created by:  <span class="username testinggg">vasanth k.</span><br>
        Date Created:   <span class="datefromat">10/15/19</span><br>

              <a class="flagthis" href="javascript:void(0);">flag this sandwich</a>
            </p>
            

            <p></p>
          </div>
          <div class="button-holder">  
            <a href="javaScript:void(0)" class="share-sandwich-popup">Share</a>
            <a href="javaScript:void(0)" class="edit-sandwich-popup">Edit</a> 
            <a href="javaScript:void(0);" class="save-menu save-sandwich-popup saved-sandwich-popup" id="">SAVE</a>
            <div class="checkbox-holder check-sandwich-private">
              <input id="check-sandwich-private" type="checkbox" class="menucheck make_private_check" name="check-sandwich-private" value="check-sandwich-private" >
              <label for="check-sandwich-private">Make Private <!--<span>Yum!</span>--></label>

            </div> 
             <input type="hidden" name="hidden_sandwich_data" id="hidden_sandwich_data" value="user_sandwich">
        <input type="hidden" name="hidden_sandwich_id" id="hidden_sandwich_id" value="">
            <div class="popup_spinner">
              <a href="javascript:void(0);" class="left_spinner"></a>
              <input name="itemQuty" type="text" class="text-box" value="01" readonly="">
              <a href="javascript:void(0);" class="right_spinner"></a>
            </div>
            <a href="javascript:void(0);" data-type="user_sandwich" data-id=" " class="add-to-cart link">ADD TO CART</a> 
          </div>
        </div>
      </div>


<!-- featured sandwich edit popup starts -->
  <div class="popup-wrapper new-popup--wrapper" id="sandwiches_quick_edit" style="display: none;">
    <div class="popup-wrapper quickedit-loader" id="quick_edit_ajax_loader" style="display: none;">
      <img src="{$SITE_URL}app/images/ajax-loader.gif">
    </div>
      <div class="popup-edit-sandwich">
         <a href="javascript:void(0);" class="close-button">Close</a>
         <p class="edit-sandwich--header">EDIT SANDWICH</p>
         <form>
            <div class="bread_images" style="display:none">
            
              <span class="bread_image">
              <div class="image-holder"> <img style="display:none" src="{$SITE_URL}app/images/create-sandwich-bread.png" alt="Instagram"> </div>
              </span>
              
              <span class="protein_image">
              </span>
              
              <span class="cheese_image">
              </span>
              
              <span class="topping_image">
              </span>

              <span class="condiments_image">
              </span>
            
            </div>
            <div class="edit-wrap-input">
               <label>Name</label>
               <input type="text" name="sandwich_name" placeholder="">
            </div>
            <div class="edit-wrap-input">
               <label>Bread</label>
               <select id="bread_drpdown">
                  <option>Ciabatta</option>
                  <option>Ciabatta</option>
                  <option>Ciabatta</option>
               </select>
            </div>

            <div class="editable-sandwich-container protein-select-wrapper">
               <div class="spiner-edit--wrapper">
                  <label>Protein</label>
                  <div class="new_spiner-edit">
                     <p>Turkey</p>
                     <div class="spiner-wrap">
                        <div class=" new_popup-spiner__edit">
                           <a href="javascript:void(0);" class="left_spinner"></a>
                           <input name="itemQuty" type="text" class="text-box" value="Half Portion (0.5)" readonly="">
                           <a href="javascript:void(0);" class="right_spinner"></a>
                        </div>
                        <a href="javascript:void(0);" class="delete-edit">
                           <img src="{$SITE_URL}app/images/edit_sandwich/delete-spiner.png">
                        </a>
                     </div>
                  </div>
                  <div class="new_spiner-edit">
                     <p>Roast Beaf</p>
                     <div class="spiner-wrap">
                        <div class=" new_popup-spiner__edit">
                           <a href="javascript:void(0);" class="left_spinner"></a>
                           <input name="itemQuty" type="text" class="text-box" value="Half Portion (0.5)" readonly="">
                           <a href="javascript:void(0);" class="right_spinner"></a>
                        </div>
                        <a href="javascript:void(0);" class="delete-edit">
                           <img src="{$SITE_URL}app/images/edit_sandwich/delete-spiner.png">
                        </a>
                     </div>
                  </div>
               </div>
               <div class="spiner-edit--wrapper">
                  <label>Cheese</label>
                  <div class="new_spiner-edit">
                     <p>Turkey</p>
                     <div class="spiner-wrap">
                        <div class=" new_popup-spiner__edit">
                           <a href="javascript:void(0);" class="left_spinner"></a>
                           <input name="itemQuty" type="text" class="text-box" value="Normal Portion (N)" readonly="">
                           <a href="javascript:void(0);" class="right_spinner"></a>
                        </div>
                        <a href="javascript:void(0);" class="delete-edit">
                           <img src="{$SITE_URL}app/images/edit_sandwich/delete-spiner.png">
                        </a>
                     </div>
                  </div>
               </div>
               <div class="spiner-edit--wrapper">
                  <label>Toppings</label>
               </div>
               <div class="spiner-edit--wrapper">
                  <label>Condiments</label>
               </div>
            </div>
               <div class="spiner-bottom--btns">
                  <a href="javascript:void(0)" class="open-add-item">add item</a>
                  <a href="javascript:void(0)" id="save-quickedit-sandwich">Save</a>
               </div>
         </form>
      </div>
   </div>
<!-- featured sandwich edit popup ends -->

<!-- Add Item popup from Quick edit sandwich popup --> 
<div class="popup-wrapper new-popup--wrapper" id="sandwiches_quick_edit_add_item" style="display: none;">
  <div class="popup-wrapper quickedit-loader" id="add_item_ajax_loader" style="display: none;">
    <img src="{$SITE_URL}app/images/ajax-loader.gif">
  </div>
      <div class="popup-edit-sandwich">
         <a href="javascript:void(0);" class="close-button">Close</a>
         <p class="edit-sandwich--header">ADD ITEM</p>
         <form>
            <div class="add-edit-popup">
               <div class="edit-wrap-input">
                     <label>Category</label>
                     <select id="add-item-category-select">
                        <option>Protein</option>
                        <option>Ciabatta</option>
                        <option>Ciabatta</option>
                     </select>
                  </div>
            <div class="edit-wrap-input">
               <label>Item</label>
               <select id="add-item-select">
                  <option>Turkey</option>
                  <option>Ciabatta</option>
                  <option>Ciabatta</option>
               </select>
            </div>
            <div class="new_spiner-edit">

                  <div class="spiner-wrap">
                     <div class="new_popup-spiner__edit">
                        <a href="javascript:void(0);" class="left left_spinner"></a>
                        <input name="itemQuty" type="text" class="text-box" value="Double Portion (2.0)" readonly="">
                        <a href="javascript:void(0);" class="right right_spinner"></a>
                     </div>
                  </div>
               </div>
         </div>
               <div class="spiner-bottom--btns">
                  <a href="javascript:void(0)" class="cancel-add-item">CANCEL</a>
                  <a href="javascript:void(0)" class="add-item-to-sandwich">ADD</a>
               </div>
         </form>
      </div>
   </div>
   <!-- End of Add Item popup from Quick edit sandwich popup --> 
<!-- SALAD popup start -->
   <div class="popup-wrapper new-popup--wrapper salad-item--options" id="salad_popup" style="display: none;">
      <div class="popup-edit-sandwich">
         <a href="javascript:void(0);" class="close-button">Close</a>
         <div class="salads_popup--wrap">
            <form>
               <div class="salad_item__image">
                  <img src="images/featured_sandwitch/salad.png">
               </div>
               <div class="salad_item__description">
                  <h4>
                     Asian Crisp Salad
                  </h4>
                  <p>
                     Romaine lettuce, cabbage, mandarin orange, crispy wonton, green onion, shredded carrot, edamame,
                     sesame seeds, and thai peanut dressing
                  </p>
                  <div class="hidden-values">
                    
                  </div>
                  <div class="salad_item__options">
                     <a href="javascript:void(0);" class="collapse-expand add_modifier">
                       <span>+</span> <p class="modifier_desc" style="font-size: unset;margin: 0px;">Add a Protein</p>
                     </a>
                     <div class="salad-protien__checkbox collapse" style="display: none;">
                       <div class="salad-protien--sec">
                      </div>

                     </div>
                     <a href="javascript:void(0);" class="collapse-expand spcl_instruction">
                        <span>+</span> Special Instructions
                     </a>
                     <div class="salad-options-special_instructions collapse" style="display: none">
                        <input type="text" name="spcl_instructions" value="">
                     </div>
                  </div>
               </div>

               <div class="salad-item--bottom">
                  <div class="popup_spinner">
                    <a href="javascript:void(0);" class="left_spinner"></a>
              <input name="itemQuatity" type="text" class="text-box" value="01" readonly="">
              <a href="javascript:void(0);" class="right_spinner"></a>
                  </div>
                  <a href="javascript:void(0);" class="salad-add--cart add-salad-to-cart">Add to Cart</a>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- popup end -->
<div class="popup-wrapper" id="facebookFriendsList" style="display: none;">
  <div class="friends-menu-inner"> <a href="javascript:void(0);" class="close-button">Close</a>
    <h1>Friends' Menus</h1>
    <div class="scroller-wrapper scroll-bar">
      <ul>
        <li style="border: 0px none; text-align: center; padding-top: 163px; height: 204px;">  
        <img src="{$SITE_URL}app/images/friends_menu.gif" />
        </li>
      </ul>
    </div>
  </div>
</div>


<script src="{$SITE_URL}app/js/common.js?v=2.2"></script>

{literal}
<script>
var inp1 = document.getElementById('phone1');
var inp2 = document.getElementById('phone2');
var inp3 = document.getElementById('phone3');
inp1.onkeyup = function() {
  
    if(inp1.value.length==3)
    {
        $('#phone2').focus();
    }
}
inp2.onkeyup = function() {
  
    if(inp2.value.length==3)
    {
        $('#phone3').focus();
    }
}
inp3.onkeyup = function() {
  
    if(inp3.value.length==4)
    {
        $('#phoneext').focus();
    }
}
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92990710-1', 'auto');
  ga('send', 'pageview');

</script>
{/literal}
</body>
</html>
