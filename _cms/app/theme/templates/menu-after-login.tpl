   <div class="menu-list-wrapper">
        <h1>Menu</h1>
        <h2>Sandwiches</h2>
        <div class="friens-list-the-sandwich"> <a href="{$SITE_URL}menu" class="my-menu">MY MENU</a>
          <div class="friens-list-image-content">
            <div class="image-box"><img src="{$SITE_URL}app/images/all-friends.jpg" alt=""></div>
            <div class="content-box">
              <h3>ROBERT SPIERENBURG'S MENU</h3>
              <p>10 Sandwiches</p>
            </div>
          </div>
          <a href="#" class="all-friend">All Friends</a> </div>
        <ul class="menu-listing">
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-4.png" alt="">
            <h3>VIC'S HAM AFFAIR</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-1.png" alt="">
            <h3>MATT'S CORNED BEEF EXPLOSION</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-2.png" alt="">
            <h3>STEPHANIE'S VEGGIE DELIGHT</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-3.png" alt="">
            <h3>JON'S MOZZARELLA MASTERPIECE</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-4.png" alt="">
            <h3>VIC'S HAM AFFAIR</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-1.png" alt="">
            <h3>MATT'S CORNED BEEF EXPLOSION</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-2.png" alt="">
            <h3>STEPHANIE'S VEGGIE DELIGHT</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-3.png" alt="">
            <h3>JON'S MOZZARELLA MASTERPIECE</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-4.png" alt="">
            <h3>VIC'S HAM AFFAIR</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
          <li> <span class="inner-holder"> <img src="{$SITE_URL}app/images/sandwich-1.png" alt="">
            <h3>MATT'S CORNED BEEF EXPLOSION</h3>
            <p>26 likes</p>
            <a href="#">VIEW</a> <a href="#">ADD TO CART</a> </span> </li>
        </ul>
        <h2>Salads</h2>
        <div class="salads-wrapper">
          <ul>
            <li class="list1">
              <h3>WATERMELON &amp; FETA SALAD</h3>
            </li>
            <li class="list2">
              <h4>$8.00</h4>
            </li>
            <li class="list3">
              <div class="quantity-control"> <a href="javascript:void()" class="left"></a>
                <input name="" type="text" class="text-box" value="01">
                <a href="javascript:void()" class="right"></a> </div>
            </li>
            <li class="list4"> <a href="#" class="add-cart">ADD TO CART</a> </li>
          </ul>
        </div>
      </div>
      <!--Menu Ending-->
    </div>
  </div>
 </div>
 <!--Popup Start-->
<div class="popup-wrapper" id="sandwich-view">
  <div class="added-menu-sandwiches-inner"> <a href="#" class="close-button">Close</a> <img src="{$SITE_URL}app/images/added-menu-sandwiches.png" alt="">
    <div class="title-holder">
      <h1>MATT'S TURKEY DILEMNA</h1>
      <h2>$14.50</h2>
    </div>
    <p>Ciabatta, Turkey (1), Ham (0.5), Pepperoni (0.5), Cheddar (1), Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado, Mayonnaise (N), Blueberry Jam (S)</p>
    <div class="title2-holder">
      <p>Created by:  Katie M.<br />
        Date Created:  06/05/14<br />
        <!-- Likes:  10<br />
        Menu Adds:  24<br /> -->
        <a href="#">flag this sandwich</a></p>
      <a href="#" class="save-menu">ADD TO MY MENU</a> </div>
    <div class="button-holder"> <!-- <a href="#" class="like-sandwich">Like</a> --> <a href="#" class="share-sandwich">Share</a>
      <div class="checkbox-holder">
        <input id="check1" type="checkbox" name="check" value="check1">
        <label for="check1">TOAST IT! <span>Yum!</span></label>
      </div>
      <a href="#" class="add-to-cart">ADD TO CART</a> </div>
  </div>
</div>

<div class="popup-wrapper" id="added-to-menu">
  <div class="added-menu-sandwiches-inner"> <a href="#" class="close-button">Close</a> <img src="{$SITE_URL}app/images/added-menu-sandwiches.png" alt="">
    <div class="title-holder">
      <h1>MATT'S TURKEY DILEMNA</h1>
      <h2>$14.50</h2>
    </div>
    <p>Ciabatta, Turkey (1), Ham (0.5), Pepperoni (0.5), Cheddar (1), Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado, Mayonnaise (N), Blueberry Jam (S)</p>
    <div class="title2-holder">
      <p>Created by:  Katie M.<br />
        Date Created:  06/05/14<br />
        <!-- Likes:  10<br />
        Menu Adds:  24<br /> -->
        <a href="#">flag this sandwich</a></p>
      <a href="#" class="save-menu background-color-remove">ADDED TO MY MENU</a> </div>
    <div class="button-holder"> <!-- <a href="#" class="like-sandwich">Like</a> --> <a href="#" class="share-sandwich">Share</a>
      <div class="checkbox-holder">
        <input id="check1" type="checkbox" name="check" value="check1">
        <label for="check1">TOAST IT! <span>Yum!</span></label>
      </div>
      <a href="#" class="add-to-cart">ADD TO CART</a> </div>
  </div>
</div>
<div class="popup-wrapper" id="sandwich-view">
  <div class="added-menu-sandwiches-inner"> <a href="#" class="close-button">Close</a> <img src="{$SITE_URL}app/images/added-menu-sandwiches.png" alt="">
    <div class="title-holder">
      <h1>MATT'S TURKEY DILEMNA</h1>
      <h2>$14.50</h2>
    </div>
    <p>Ciabatta, Turkey (1), Ham (0.5), Pepperoni (0.5), Cheddar (1), Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado, Mayonnaise (N), Blueberry Jam (S)</p>
    <div class="title2-holder">
      <p>Created by:  Katie M.<br />
        Date Created:  06/05/14<br />
        <!-- Likes:  10<br />
        Menu Adds:  24<br /> -->
        <a href="#">flag this sandwich</a></p>
      <a href="#" class="save-menu">REMOVE FROM MENU</a> </div>
    <div class="button-holder"> <!-- <a href="#" class="like-sandwich">Like</a> --> <a href="#" class="share-sandwich">Share</a>
      <div class="checkbox-holder">
        <input id="check1" type="checkbox" name="check" value="check1">
        <label for="check1">TOAST IT! <span>Yum!</span></label>
      </div>
      <a href="#" class="add-to-cart">ADD TO CART</a> </div>
  </div>
</div>

<div class="popup-wrapper" id="cart-added">
  <div class="added-menu-sandwiches-inner"> <a href="#" class="close-button">Close</a> <img src="{$SITE_URL}app/images/added-menu-sandwiches.png" alt="">
    <div class="title-holder">
      <h1>MATT'S TURKEY DILEMNA</h1>
      <h2>$14.50</h2>
    </div>
    <p>Ciabatta, Turkey (1), Ham (0.5), Pepperoni (0.5), Cheddar (1), Tomato (L), Lettuce (N), Red Onion (L), Pickles (L), Peppers (L), Avocado, Mayonnaise (N), Blueberry Jam (S)</p>
    <div class="title2-holder">
      <p>Created by:  Katie M.<br />
        Date Created:  06/05/14<br />
        <!-- Likes:  10<br />
        Menu Adds:  24<br /> -->
        <a href="#">flag this sandwich</a></p>
      <a href="#" class="save-menu">REMOVE FROM MENU</a> </div>
    <div class="button-holder"> <!-- <a href="#" class="like-sandwich">Like</a> --> <a href="#" class="share-sandwich">Share</a>
      <div class="checkbox-holder">
        <input id="check1" type="checkbox" name="check" value="check1">
        <label for="check1">TOAST IT! <span>Yum!</span></label>
      </div>
      <a href="#" class="add-to-cart background-color-remove">ADDED TO CART</a> </div>
  </div>
</div>
<div class="popup-wrapper" id="facebook-friends-list">
  <div class="friends-menu-inner"> <a href="#" class="close-button">Close</a>
    <h1>Friend's Menus</h1>
    <div class="scroller-wrapper scroll-bar">
      <ul>
        <li> <span class="image-box"><img src="{$SITE_URL}app/images/friend-menu-1.png" alt=""></span> <span class="content-box">
          <h2>MATT BAER</h2>
          <p>9 Sandwiches</p>
          </span> <a href="{$SITE_URL}menu/loggedin" class="view-menu">View Menu</a> </li>
        <li> <span class="image-box"><img src="{$SITE_URL}app/images/friend-menu-2.png" alt=""></span> <span class="content-box">
          <h2>ZIGGY BLUMENTHAL</h2>
          <p>9 Sandwiches</p>
          </span> <a href="{$SITE_URL}menu/loggedin" class="view-menu">View Menu</a> </li>
        <li> <span class="image-box"><img src="{$SITE_URL}app/images/friend-menu-3.png" alt=""></span> <span class="content-box">
          <h2>JONATHAN COHEN</h2>
          <p>9 Sandwiches</p>
          </span> <a href="{$SITE_URL}menu/loggedin" class="view-menu">View Menu</a> </li>
        <li> <span class="image-box"><img src="{$SITE_URL}app/images/friend-menu-4.png" alt=""></span> <span class="content-box">
          <h2>ADAM LEVINE</h2>
          <p>9 Sandwiches</p>
          </span> <a href="{$SITE_URL}menu/loggedin" class="view-menu">View Menu</a> </li>
        <li> <span class="image-box"><img src="{$SITE_URL}app/images/friend-menu-1.png" alt=""></span> <span class="content-box">
          <h2>RICHARD MAHARAJ</h2>
          <p>9 Sandwiches</p>
          </span> <a href="{$SITE_URL}menu/loggedin" class="view-menu">View Menu</a> </li>
      </ul>
    </div>
  </div>
</div>