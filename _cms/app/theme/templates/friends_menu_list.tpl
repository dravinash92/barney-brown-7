{if $data|@count gt 0}
{foreach key=key item=item from=$data}
<li> 
<span class="image-box"><img alt="" src="https://graph.facebook.com/{$item->friend_uid}/picture?width=57&height=57"></span> <span class="content-box">
<h2>{$item->first_name} {$item->last_name}</h2>
<p>{$item->sandwich_count} Sandwiches</p>
</span> 
<a class="view-menu link" href="{$SITE_URL}menu/friendsMenu/{$item->uid}">View Menu</a> 
</li> 
{/foreach}
{else}

<ul class="frndpopcont"><li><p style="margin-top:18px;text-align:left"> Looks like you're the first of your friends to join Barney Brown! Let them know what they're missing out on! </p>
<a href="{$SITE_URL}menu/" class="back-to-menu link">BACK TO MY MENU</a>
</li></ul>

{/if}
