      <!--Header Ending-->
      <!--Create Sandwich Ending-->
      
     <div class="appendImages">  
     <!--  {$SANDWICH_IMAGES}  -->
      </div>
      
      <div class="menu-list-wrapper">
        <h1>CREATE A SANDWICH</h1>
        <a class="clear-all-button" href="javascript:void(0)">Clear All</a>
        
        <div id="ovrlayx" style="position:absolute; display:none; width:960px; height:500px; z-index:1000">

          <div style="position:relative" class="create-sandwich-wrapper">
          <div class="create-sandwich-left">
            <div class="finalize-It-heading">
              <h3>Finalize It!</h3>
            </div>
            <div class="name-your-creation-popup">
              <h2>NAME YOUR CREATION</h2>
             <h3><input type="text" value="{$SANDWICH_NAME}" id="namecreation"></h3>
            </div>
            
            <div class="button-holder" style="margin-top:383px">
              <ul>
                <li> <a href="javaScript:void(0)"  class="edit-sandwich">EDIT SANDWICH</a></li>
              </ul>
            </div>
          </div>
          <div class="create-sandwich-right-wrapper">
            <div class="create-sandwich-right fill-color">
              <div class="final-list-wrapper scroll-bar " style="position: relative; overflow: visible;">
               
                <ul id="final_out">
                  
                </ul>
               </div>
            </div>
            <div class="checkbox-holder-final">
              <input type="checkbox" class="menucheck" value="ov_check1" name="check" id="ov_check1">
              <label for="ov_check1" class="toastyum"> Toast It! </label>
            </div>
            <div class="save-button-share">
              <div class="checkbox-holder-final private_check_final">
                <!-- <input type="checkbox" value="ov_check2" name="check"  checked="checked"  id="ov_check2"> -->
                <input type="checkbox" value="ov_check2" name="check" id="ov_check2">
                <label for="ov_check2">Make Private</label>
                <img src="{$SITE_URL}images/sandwich_menu/lock.png">
              </div>
              
            <!-- {$sandwichId} --> 
               <!-- <a class="share-sandwich share_to_fb" rel="" href="#">Share</a> <img  class="sharefbloader" src="{$SITE_URL}app/images/share_loader.gif" />  -->
			   <a class="save-to-my-menu new-save-to-my-menu" href="javascript:void(0);">SAVE</a> </div>
            <div class="bottom-buttons">
              <h4 id="totalPrice">$0.00</h4>
              <a class="am-done"  href="javaScript:void(0)">ADD TO CART</a> 
            
              
             </div>
              <span class="addtocartDesc">By clicking "Add to Cart", sandwich will automatically save to your menu.</span> 
          </div>
        </div>
    
  </div>
        
        
        
        
        
        <div class="create-sandwich-wrapper">
          <div class="create-sandwich-left">
            <div class="create-sandwich-menu" {if $end_screen eq "true" } style="display:none";  {/if} >
              <ul>
                <li><a href="javaScript:void(0)"  class="active">BREAD</a></li>
                <li><a href="javaScript:void(0)" >PROTEIN</a></li>
                <li><a href="javaScript:void(0)" >CHEESE</a></li>
                <li><a href="javaScript:void(0)" >TOPPINGS</a></li>
                <li><a href="javaScript:void(0)" >CONDIMENTS</a></li>
              </ul>
            </div>
           
            
            {if !$smarty.session.temp_sandwich_data } 
            {$LANDING_PAGE}
            {/if}
            
            <div class="bread_images">
            
            <span class="bread_image">
            <div class="image-holder"> <img style="display:none" src="{$SITE_URL}app/images/create-sandwich-bread.png" alt="Instagram"> </div>
            </span>
            
            <span class="protein_image">
            </span>
            
            <span class="cheese_image">
            </span>
            
            <span class="topping_image">
            </span>

            <span class="condiments_image">
            </span>
            
            </div>

          </div>
          <div class="create-sandwich-right-wrapper" {if $end_screen eq "true" } style="display:none";  {/if} >
           <div class="optionLoader"><img src="{$SITE_URL}app/images/optionload.gif" /></div>   
            <div class="create-sandwich-right" id="optionList">     
                   
             {$OPTN_DATA}
            </div>
            <div class="bottom-buttons">
              <h4 class="price">${$BASE_FARE}.00</h4>
              <!-- <a href="javaScript:void(0)" id="finished" class="am-done link">I'M DONE</a> -->  
			  <p class="prmicoTxt"><span class="premium-icon" style="padding-bottom: 4px;">&nbsp;</span> Premium</p>
			       <div class="button-holder" style="display:none">
                    <ul>
                    <li><a href="javaScript:void(0)" class="back">BACK</a></li>
                    <li><a href="javaScript:void(0)" class="next">NEXT</a></li>
                    </ul>
                   </div>  
			</div>
          </div>
        </div>
      </div>
      <!--Create Sandwich Ending-->
    </div>
  </div>
  
{literal}
<script type="text/javascript">
$(document).ready(function(){  
	//ha.sandwichcreator.preload_sandwich_images();	
	$('#namecreation').focus();
});

 
</script>
{/literal}
