			{if $itemsCount gt 0}  
            <ul class="checkout-header">
              <li class="header-item1">Items</li>
              <li class="header-item2">Qty</li>
              <li class="header-item3">Price</li>
            </ul>
			<ul id="messageBar">
				{if $totalNumber < 10 && $pickupOrDelivery neq 0} 
				 
				{math assign="reaminingNumber" equation=10.00-$totalNumber}
				
						<li class="less-than-10">$10.00 SUBTOTAL MINIMUM. ${$reaminingNumber|string_format:"%.2f"} TO GO!</li>
				{/if}
			</ul>
			{foreach from=$sandwiches key=myId item=sandwich}
			<ul>					
              <li class="check-list1 checkoutItemName">
                <h3 title="{$sandwich.sandwich_name}" ><span>{$sandwich.sandwich_name|wordwrap:32:"<br />"}</span><hr/></h3>  
                <p>{$sandwich.data_string}</p>
                  {if $sandwich.toast eq 1}<p class="toasted">Toasted</p>{/if}
              </li>
              <li class="check-list3">
              
                <div class="quantity-control"><a class="left" href="javascript:void(0)"></a>
                  <input type="text" readonly="readonly" value="{$sandwich.item_qty}" class="text-box qty" name="">
                  <a class="right" href="javascript:void(0)"></a>
                  <input type="hidden" class="sandwich_id" value="{$sandwich.id}" />
                  <input type="hidden" class="order_item_id" value="{$sandwich.order_item_id}" />
                  <input type="hidden" name="data_type" value="user_sandwich">
                  <input type="hidden" class="bread_type" name="bread_type" value="{$sandwich.bread_type}"/> 
                </div>
                  
              </li>
              <li class="list2">
                <h4 class="item_price">${$sandwich.item_total}</h4>
              </li>
              <li class="check-list4"> 
				<a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="{$sandwich.order_item_id}" data-item="{$sandwich.item_id}">REMOVE</a> 
			  </li>
            </ul>
            {/foreach}
            
            {foreach from=$products key=myId item=product}
				<ul>
				<li class="check-list1 checkoutItemName">
                <h3 title="{$product->product_name}" ><span>{$product->product_name|wordwrap:32:"<br />"}</span><hr/></h3>
                
				{if $product->description ne ""} <p>{$product->description}</p>{/if}					
				
				{if $product->extra_id ne ""} <p>Modifiers: <strong>{$product->extra_id}</strong></p> {/if}                 
        {if $product->spcl_instructions ne ""} <p>Special Instructions: <strong>{$product->spcl_instructions}</strong></p> {/if}                 
              </li>
              <li class="check-list3">
                <div class="quantity-control"> <a class="left" href="javascript:void(0)"></a>
                  <input type="text" readonly="readonly" value="{$product->item_qty}" class="text-box qty" name="">
                  <a class="right" href="javascript:void(0)"></a>
                  <input type="hidden" class="sandwich_id" value="{$product->id}" />
                  <input type="hidden" class="order_item_id" value="{$product->order_item_id}" />
                  <input type="hidden" name="data_type" value="product">
                </div>
                  
              </li>
              <li class="list2">
                <h4 class="item_price">${$product->item_total}</h4>
              </li>
              <li class="check-list4"> 
				<a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="{$product->order_item_id}" data-item="{$product->item_id}">REMOVE</a> 
			  </li>
            </ul>
            {/foreach}
            
            <div class="sub-total-wrapper">
              <ul class="sub-total small-font">
                <li class="sub-total-list1">
				  <p></p>
                </li>
                <li class="sub-total-list2">
                  <h3 class="small-font">SUBTOTAL</h3>
                  <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="{$totalNumber}">
                  <h4>$<span class="sub-total">{$total}</span></h4>
                </li>
				
			</ul>
			<ul class="sub-total small-font">
				  <li class="sub-total-list1">

				  <p></p>
                </li>
				<li class="sub-total-list2">
                  <h3 class="small-font">TAX</h3>
                  <input type="hidden" name="hidden_tax" id="hidden_tax" value="{$tax}">
                  <h4>$<span class="sub-total">{$tax}</span></h4>
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <a class="tip-image" href="javascript:void(0)" style="visibility:hidden;"><img src="{$SITE_URL}app/images/tip-info.png" alt=""></a>
				  <div class="tip-image-info">
				  <a class="close-button" href="javascript:void(0)">Close</a>
				  <p>$3.00 min. gratuity required for delivery orders. <!-- No gratuity required for pick-up orders. --></p>
				  </div>
                </li>
                <li class="sub-total-list2 tip">
                  <h3>TIP</h3>
                  <div class="selectParent">
		    <input type="hidden" name="hidden_tip" id="hidden_tip" value="">

                    <select onchange="updateTip(this);" id="select_tip_amount">						
						{if $pickupOrDelivery eq 0}
								<option selected value="0.00">$0.00</option>															
								{section name=foo start=1 loop=$MAX_TIP step=1}
									<option {if $tips eq $smarty.section.foo.index || $smarty.section.foo.index lt $tips } selected {/if} value="{$smarty.section.foo.index}">${$smarty.section.foo.index}.00</option>
									{assign var="tipVal" value=$smarty.section.foo.index|cat:'.50'}
								{if $smarty.section.foo.index<10}	<option value="{$smarty.section.foo.index}.50">${$smarty.section.foo.index}.50</option>{/if}
								{/section} 
								 <option {if $tips eq $MAX_TIP} selected {/if} value="{$MAX_TIP}.00">${$MAX_TIP}.00</option>

							
							{else}
								{section name=foo start=$MIN_TIP loop=$MAX_TIP step=1}
									<option {if $tips eq $smarty.section.foo.index || $smarty.section.foo.index lt $tips } selected {/if} value="{$smarty.section.foo.index}">${$smarty.section.foo.index}.00</option>
									{assign var="tipVal" value=$smarty.section.foo.index|cat:'.50'}
							{if $smarty.section.foo.index<10}		<option value="{$smarty.section.foo.index}.25">${$smarty.section.foo.index}.25</option>
									<option value="{$smarty.section.foo.index}.50">${$smarty.section.foo.index}.50</option>
									<option value="{$smarty.section.foo.index}.75">${$smarty.section.foo.index}.75</option>
									{/if}
								{/section}
								 <option {if $tips eq $MAX_TIP} selected {/if} value="{$MAX_TIP}.00">${$MAX_TIP}.00</option>

							{/if}	
                     
                    </select>                  
                  </div>
                </li>
              </ul>
              
               <ul class="sub-total coupon_result" style="display:none;">
                <li class="sub-total-list1">
                  <p></p>
                </li>
                <li class="sub-total-list2">
                  <h3>DISCOUNT</h3>                  
                  <h4>-$<span class="sub-total-discount">683.00</span></h4>
                </li>                
                <li class="sub-total-list3"></li>
              </ul>
              
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <p>Promo Code / Gift Card</p>
                </li>
                <li class="sub-total-list2">
                  <input name="" type="text" id="apply_discount" class="sub-total-text">
                </li>
                <div id="hidden_discount">
                
                </div>
                <li class="sub-total-list3"><a  class="remove link" onclick="applyDiscount()">APPLY</a></li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">&nbsp;</li>
                <li class="sub-total-list2">
                  <h5>Total</h5>
                  <input type="hidden" name="hidden_grand_total" id="hidden_grand_total" value="{$grandtotal}">
                  <h4>$<span class="grand-total">{$grandtotal}</span></h4>
                  {if $total>= 0}
               
                <p class="est-delivery-time">EST. DELIVERY TIME:
                {if $total>=0 && $total<100}30-60 mins
	                {elseif $total>=100 && $total<200}45-75 mins
                  {elseif $total>=200 && $total<300}60-90 mins
	                {elseif $total>=300 && $total<400}1-2 hrs
	                {elseif $total>=400 && $total<500}2-3 hrs
	                {elseif $total>=500}24 hours                
                {/if}</p>
              {/if}
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
			    <ul class="sub-total">
                <li class="sub-total-list1">&nbsp;</li>
                <li class="sub-total-list2">
                
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
            </div>
           
            {else}
			<ul>
				<li>
				<h3>There are currently no items in your shopping cart.</h3>  
				</li>
			</ul>				
                
            {/if}

           <input type="hidden" value="{$order_string}" name="odstring" />
           
