<div class="featured_sandwitch-wrapper menu-list-wrapper">
  <h1>SNACKS &amp; THINGS</h1>
  <input type="hidden" name="countMenuItems" value="0">
  <h2><span class="mr_8"></span>Snacks &amp; Things</h2>
  <div class="featured_items">
    {foreach from=$salads key=k item=v}
    {php}if ($v.id % 2 == 0) { {/php}
    <ul class="drinks_main_ul ">
        <li class="featured_item">
       <div class="featured_img">
          <img title="{$v.product_name}" class="view_sandwich salad-img" data-href="{$v.id}" src="{$salad_image_path}{$v.product_image}">
       </div>
       <div class="featured_item_detail salads-wrapper ">
        <h3 class="drinks_product_name">{$v.product_name}</h3>
          <ul data-id="{$v.id}" data-product_name="{$v.product_name}" data-description="{$v.description}" data-product_image="{$v.product_image}" data-product_price="{$v.product_price}" data-standard_category_id="{$v.standard_category_id}" data-image_path="{$salad_image_path}" data-uid={$uid} data-product={$product} data-spcl_instr="{$v.allow_spcl_instruction}" data-add_modifier="{$v.add_modifier}" data-modifier_desc="{$v.modifier_desc}" data-add_modifier="{$v.add_modifier}" data-modifier_isoptional="{$v.modifier_isoptional}" data-modifier_is_single="{$v.modifier_is_single}" data-modifier_options='{$modifier_options.$k|@json_encode}'>
           <li class="list-one">
             
              <h3 class="snacks-price">${$v.product_price}</h3>

           </li>

           <li class="list-three">
             <div class="quantity-control"> 

              <a href="javascript:void(0);" class="left noaction"></a>
              <input name="spinnerinput" type="text" class="text-box qty" value="01" readonly  >

              <a href="javascript:void(0);" class="right noaction"></a> 
              <input class="sandwich_id" type="hidden" value="{$v.id}">
              <input type="hidden" value="product" name="data_type">

            </div>
          </li>
          <li class="list-four">
           <a href="javascript:void(0);" class="link {if $v.allow_spcl_instruction eq 1 or $v.add_modifier eq 1} salad-listing {else} common_add_item_cart {/if}" data-sandwich="{$product}" data-sandwich_id="{$v.id}" data-uid="{$uid}"><img src="{$SITE_URL}images/sandwich_menu/cart.png"><span>ADD</span></a>
         </li>
       </ul>
     </div>
   </li>
    </ul>
    {php} } else { {/php}
    <ul class="drinks_main_ul snacks_main_ul">
        <li class="featured_item">
       <div class="featured_img">
          <img title="{$v.product_name}" class="view_sandwich salad-img" data-href="{$v.id}" src="{$salad_image_path}{$v.product_image}">
       </div>
       <div class="featured_item_detail salads-wrapper snack_drink">
          <ul data-id="{$v.id}" data-product_name="{$v.product_name}" data-description="{$v.description}" data-product_image="{$v.product_image}" data-product_price="{$v.product_price}" data-standard_category_id="{$v.standard_category_id}" data-image_path="{$salad_image_path}" data-uid={$uid} data-product={$product} data-spcl_instr="{$v.allow_spcl_instruction}" data-add_modifier="{$v.add_modifier}" data-modifier_desc="{$v.modifier_desc}" data-add_modifier="{$v.add_modifier}" data-modifier_isoptional="{$v.modifier_isoptional}" data-modifier_is_single="{$v.modifier_is_single}" data-modifier_options='{$modifier_options.$k|@json_encode}'>
           <li class="list-one">
             <h3>{$v.product_name}</h3>
             <p>{$v.description}</p>
             <p class="created_by"></p>
             <a class="featured_item_btn salad-listing" href="javascript:void(0);">view</a>
           </li>
           <li class="list-two">
             <h4 class="snacks-price">${$v.product_price}</h4>
           </li>
           <li class="list-three">
             <div class="quantity-control"> 

              <a href="javascript:void(0);" class="left noaction"></a>
              <input name="spinnerinput" type="text" class="text-box qty" value="01" readonly  >

              <a href="javascript:void(0);" class="right noaction"></a> 
              <input class="sandwich_id" type="hidden" value="{$v.id}">
              <input type="hidden" value="product" name="data_type">

            </div>
          </li>
          <li class="list-four">
           <a href="javascript:void(0);" class="link {if $v.allow_spcl_instruction eq 1 or $v.add_modifier eq 1} salad-listing {else} common_add_item_cart {/if}" data-sandwich="{$product}" data-sandwich_id="{$v.id}" data-uid="{$uid}"><img src="{$SITE_URL}images/sandwich_menu/cart.png"><span>ADD</span></a>
         </li>
       </ul>
     </div>
   </li>
    </ul>
    {php} } {/php}
    {/foreach}
    
</div>
</div>
</div>
</div>
