<?php
/* Smarty version 3.1.39, created on 2021-03-25 19:29:44
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\cart-list-items.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c9750f215b8_90599195',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e0d8543e9d3431da522d885aeb1c7e71d1310bd6' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\cart-list-items.tpl',
      1 => 1600063289,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c9750f215b8_90599195 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_cms\\app\\smarty\\libs\\plugins\\function.math.php','function'=>'smarty_function_math',),1=>array('file'=>'C:\\wamp64\\www\\hashbury\\_cms\\app\\smarty\\libs\\plugins\\modifier.mb_wordwrap.php','function'=>'smarty_modifier_mb_wordwrap',),));
?>
			<?php if ($_smarty_tpl->tpl_vars['itemsCount']->value > 0) {?>  
            <ul class="checkout-header">
              <li class="header-item1">Items</li>
              <li class="header-item2">Qty</li>
              <li class="header-item3">Price</li>
            </ul>
			<ul id="messageBar">
				<?php if ($_smarty_tpl->tpl_vars['totalNumber']->value < 10 && $_smarty_tpl->tpl_vars['pickupOrDelivery']->value != 0) {?> 
				 
				<?php echo smarty_function_math(array('assign'=>"reaminingNumber",'equation'=>10.00-$_smarty_tpl->tpl_vars['totalNumber']->value),$_smarty_tpl);?>

				
						<li class="less-than-10">$10.00 SUBTOTAL MINIMUM. $<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['reaminingNumber']->value);?>
 TO GO!</li>
				<?php }?>
			</ul>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwiches']->value, 'sandwich', false, 'myId');
$_smarty_tpl->tpl_vars['sandwich']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['myId']->value => $_smarty_tpl->tpl_vars['sandwich']->value) {
$_smarty_tpl->tpl_vars['sandwich']->do_else = false;
?>
			<ul>					
              <li class="check-list1 checkoutItemName">
                <h3 title="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['sandwich_name'];?>
" ><span><?php echo smarty_modifier_mb_wordwrap($_smarty_tpl->tpl_vars['sandwich']->value['sandwich_name'],32,"<br />",false);?>
</span><hr/></h3>  
                <p><?php echo $_smarty_tpl->tpl_vars['sandwich']->value['data_string'];?>
</p>
                  <?php if ($_smarty_tpl->tpl_vars['sandwich']->value['toast'] == 1) {?><p class="toasted">Toasted</p><?php }?>
              </li>
              <li class="check-list3">
              
                <div class="quantity-control"><a class="left" href="javascript:void(0)"></a>
                  <input type="text" readonly="readonly" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['item_qty'];?>
" class="text-box qty" name="">
                  <a class="right" href="javascript:void(0)"></a>
                  <input type="hidden" class="sandwich_id" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['id'];?>
" />
                  <input type="hidden" class="order_item_id" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['order_item_id'];?>
" />
                  <input type="hidden" name="data_type" value="user_sandwich">
                  <input type="hidden" class="bread_type" name="bread_type" value="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['bread_type'];?>
"/> 
                </div>
                  
              </li>
              <li class="list2">
                <h4 class="item_price">$<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['item_total'];?>
</h4>
              </li>
              <li class="check-list4"> 
				<a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['order_item_id'];?>
" data-item="<?php echo $_smarty_tpl->tpl_vars['sandwich']->value['item_id'];?>
">REMOVE</a> 
			  </li>
            </ul>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product', false, 'myId');
$_smarty_tpl->tpl_vars['product']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['myId']->value => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->do_else = false;
?>
				<ul>
				<li class="check-list1 checkoutItemName">
                <h3 title="<?php echo $_smarty_tpl->tpl_vars['product']->value->product_name;?>
" ><span><?php echo smarty_modifier_mb_wordwrap($_smarty_tpl->tpl_vars['product']->value->product_name,32,"<br />",false);?>
</span><hr/></h3>
                
				<?php if ($_smarty_tpl->tpl_vars['product']->value->description != '') {?> <p><?php echo $_smarty_tpl->tpl_vars['product']->value->description;?>
</p><?php }?>					
				
				<?php if ($_smarty_tpl->tpl_vars['product']->value->extra_id != '') {?> <p>Modifiers: <strong><?php echo $_smarty_tpl->tpl_vars['product']->value->extra_id;?>
</strong></p> <?php }?>                 
        <?php if ($_smarty_tpl->tpl_vars['product']->value->spcl_instructions != '') {?> <p>Special Instructions: <strong><?php echo $_smarty_tpl->tpl_vars['product']->value->spcl_instructions;?>
</strong></p> <?php }?>                 
              </li>
              <li class="check-list3">
                <div class="quantity-control"> <a class="left" href="javascript:void(0)"></a>
                  <input type="text" readonly="readonly" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->item_qty;?>
" class="text-box qty" name="">
                  <a class="right" href="javascript:void(0)"></a>
                  <input type="hidden" class="sandwich_id" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
" />
                  <input type="hidden" class="order_item_id" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->order_item_id;?>
" />
                  <input type="hidden" name="data_type" value="product">
                </div>
                  
              </li>
              <li class="list2">
                <h4 class="item_price">$<?php echo $_smarty_tpl->tpl_vars['product']->value->item_total;?>
</h4>
              </li>
              <li class="check-list4"> 
				<a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="<?php echo $_smarty_tpl->tpl_vars['product']->value->order_item_id;?>
" data-item="<?php echo $_smarty_tpl->tpl_vars['product']->value->item_id;?>
">REMOVE</a> 
			  </li>
            </ul>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            
            <div class="sub-total-wrapper">
              <ul class="sub-total small-font">
                <li class="sub-total-list1">
				  <p></p>
                </li>
                <li class="sub-total-list2">
                  <h3 class="small-font">SUBTOTAL</h3>
                  <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="<?php echo $_smarty_tpl->tpl_vars['totalNumber']->value;?>
">
                  <h4>$<span class="sub-total"><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</span></h4>
                </li>
				
			</ul>
			<ul class="sub-total small-font">
				  <li class="sub-total-list1">

				  <p></p>
                </li>
				<li class="sub-total-list2">
                  <h3 class="small-font">TAX</h3>
                  <input type="hidden" name="hidden_tax" id="hidden_tax" value="<?php echo $_smarty_tpl->tpl_vars['tax']->value;?>
">
                  <h4>$<span class="sub-total"><?php echo $_smarty_tpl->tpl_vars['tax']->value;?>
</span></h4>
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <a class="tip-image" href="javascript:void(0)" style="visibility:hidden;"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/tip-info.png" alt=""></a>
				  <div class="tip-image-info">
				  <a class="close-button" href="javascript:void(0)">Close</a>
				  <p>$3.00 min. gratuity required for delivery orders. <!-- No gratuity required for pick-up orders. --></p>
				  </div>
                </li>
                <li class="sub-total-list2 tip">
                  <h3>TIP</h3>
                  <div class="selectParent">
		    <input type="hidden" name="hidden_tip" id="hidden_tip" value="">

                    <select onchange="updateTip(this);" id="select_tip_amount">						
						<?php if ($_smarty_tpl->tpl_vars['pickupOrDelivery']->value == 0) {?>
								<option selected value="0.00">$0.00</option>															
								<?php
$__section_foo_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['MAX_TIP']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_foo_0_start = min(1, $__section_foo_0_loop);
$__section_foo_0_total = min(($__section_foo_0_loop - $__section_foo_0_start), $__section_foo_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_foo'] = new Smarty_Variable(array());
if ($__section_foo_0_total !== 0) {
for ($__section_foo_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] = $__section_foo_0_start; $__section_foo_0_iteration <= $__section_foo_0_total; $__section_foo_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']++){
?>
									<option <?php if ($_smarty_tpl->tpl_vars['tips']->value == (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) || (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) < $_smarty_tpl->tpl_vars['tips']->value) {?> selected <?php }?> value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.00</option>
									<?php $_smarty_tpl->_assignInScope('tipVal', ((isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null)).('.50'));?>
								<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) < 10) {?>	<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.50">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.50</option><?php }?>
								<?php
}
}
?> 
								 <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == $_smarty_tpl->tpl_vars['MAX_TIP']->value) {?> selected <?php }?> value="<?php echo $_smarty_tpl->tpl_vars['MAX_TIP']->value;?>
.00">$<?php echo $_smarty_tpl->tpl_vars['MAX_TIP']->value;?>
.00</option>

							
							<?php } else { ?>
								<?php
$__section_foo_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['MAX_TIP']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_foo_1_start = (int)@$_smarty_tpl->tpl_vars['MIN_TIP']->value < 0 ? max(0, (int)@$_smarty_tpl->tpl_vars['MIN_TIP']->value + $__section_foo_1_loop) : min((int)@$_smarty_tpl->tpl_vars['MIN_TIP']->value, $__section_foo_1_loop);
$__section_foo_1_total = min(($__section_foo_1_loop - $__section_foo_1_start), $__section_foo_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_foo'] = new Smarty_Variable(array());
if ($__section_foo_1_total !== 0) {
for ($__section_foo_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] = $__section_foo_1_start; $__section_foo_1_iteration <= $__section_foo_1_total; $__section_foo_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']++){
?>
									<option <?php if ($_smarty_tpl->tpl_vars['tips']->value == (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) || (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) < $_smarty_tpl->tpl_vars['tips']->value) {?> selected <?php }?> value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.00</option>
									<?php $_smarty_tpl->_assignInScope('tipVal', ((isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null)).('.50'));?>
							<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null) < 10) {?>		<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.25">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.25</option>
									<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.50">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.50</option>
									<option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.75">$<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_foo']->value['index'] : null);?>
.75</option>
									<?php }?>
								<?php
}
}
?>
								 <option <?php if ($_smarty_tpl->tpl_vars['tips']->value == $_smarty_tpl->tpl_vars['MAX_TIP']->value) {?> selected <?php }?> value="<?php echo $_smarty_tpl->tpl_vars['MAX_TIP']->value;?>
.00">$<?php echo $_smarty_tpl->tpl_vars['MAX_TIP']->value;?>
.00</option>

							<?php }?>	
                     
                    </select>                  
                  </div>
                </li>
              </ul>
              
               <ul class="sub-total coupon_result" style="display:none;">
                <li class="sub-total-list1">
                  <p></p>
                </li>
                <li class="sub-total-list2">
                  <h3>DISCOUNT</h3>                  
                  <h4>-$<span class="sub-total-discount">683.00</span></h4>
                </li>                
                <li class="sub-total-list3"></li>
              </ul>
              
              <ul class="sub-total">
                <li class="sub-total-list1">
                  <p>Promo Code / Gift Card</p>
                </li>
                <li class="sub-total-list2">
                  <input name="" type="text" id="apply_discount" class="sub-total-text">
                </li>
                <div id="hidden_discount">
                
                </div>
                <li class="sub-total-list3"><a  class="remove link" onclick="applyDiscount()">APPLY</a></li>
              </ul>
              <ul class="sub-total">
                <li class="sub-total-list1">&nbsp;</li>
                <li class="sub-total-list2">
                  <h5>Total</h5>
                  <input type="hidden" name="hidden_grand_total" id="hidden_grand_total" value="<?php echo $_smarty_tpl->tpl_vars['grandtotal']->value;?>
">
                  <h4>$<span class="grand-total"><?php echo $_smarty_tpl->tpl_vars['grandtotal']->value;?>
</span></h4>
                  <?php if ($_smarty_tpl->tpl_vars['total']->value >= 0) {?>
               
                <p class="est-delivery-time">EST. DELIVERY TIME:
                <?php if ($_smarty_tpl->tpl_vars['total']->value >= 0 && $_smarty_tpl->tpl_vars['total']->value < 100) {?>30-60 mins
	                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 100 && $_smarty_tpl->tpl_vars['total']->value < 200) {?>45-75 mins
                  <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 200 && $_smarty_tpl->tpl_vars['total']->value < 300) {?>60-90 mins
	                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 300 && $_smarty_tpl->tpl_vars['total']->value < 400) {?>1-2 hrs
	                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 400 && $_smarty_tpl->tpl_vars['total']->value < 500) {?>2-3 hrs
	                <?php } elseif ($_smarty_tpl->tpl_vars['total']->value >= 500) {?>24 hours                
                <?php }?></p>
              <?php }?>
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
			    <ul class="sub-total">
                <li class="sub-total-list1">&nbsp;</li>
                <li class="sub-total-list2">
                
                </li>
                <li class="sub-total-list3">&nbsp;</li>
              </ul>
            </div>
           
            <?php } else { ?>
			<ul>
				<li>
				<h3>There are currently no items in your shopping cart.</h3>  
				</li>
			</ul>				
                
            <?php }?>

           <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['order_string']->value;?>
" name="odstring" />
           
<?php }
}
