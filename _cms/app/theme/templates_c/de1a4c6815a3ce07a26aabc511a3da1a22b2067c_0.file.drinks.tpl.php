<?php
/* Smarty version 3.1.39, created on 2021-03-25 17:24:25
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\drinks.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c79f11d99c5_60185675',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'de1a4c6815a3ce07a26aabc511a3da1a22b2067c' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\drinks.tpl',
      1 => 1584530557,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c79f11d99c5_60185675 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="featured_sandwitch-wrapper menu-list-wrapper">
  <h1>DRINKS</h1>
  <input type="hidden" name="countMenuItems" value="0">
  <h2><span class="mr_8"></span>Drinks</h2>
  <div class="featured_items">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['salads']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
    <?php if ($v.id % 2 == 0) { ?>
    <ul class="drinks_main_ul">
        <li class="featured_item">
             <div class="featured_img">
                <img title="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
" class="view_sandwich salad-img" data-href="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['salad_image_path']->value;
echo $_smarty_tpl->tpl_vars['v']->value['product_image'];?>
">
             </div>
             <div class="featured_item_detail salads-wrapper">
                  <h3 class="drinks_product_name"><?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
</h3>
                  <ul data-id="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" data-product_name="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['v']->value['description'];?>
" data-product_image="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_image'];?>
" data-product_price="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_price'];?>
" data-standard_category_id="<?php echo $_smarty_tpl->tpl_vars['v']->value['standard_category_id'];?>
" data-image_path="<?php echo $_smarty_tpl->tpl_vars['salad_image_path']->value;?>
" data-uid=<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
 data-product=<?php echo $_smarty_tpl->tpl_vars['product']->value;?>
 data-spcl_instr="<?php echo $_smarty_tpl->tpl_vars['v']->value['allow_spcl_instruction'];?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['v']->value['add_modifier'];?>
" data-modifier_desc="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_desc'];?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['v']->value['add_modifier'];?>
" data-modifier_isoptional="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_isoptional'];?>
" data-modifier_is_single="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_is_single'];?>
" data-modifier_options='<?php echo json_encode($_smarty_tpl->tpl_vars['modifier_options']->value[$_smarty_tpl->tpl_vars['k']->value]);?>
'>
                     <li class="list-one">
                        <h3>$<?php echo $_smarty_tpl->tpl_vars['v']->value['product_price'];?>
</h3>
                     </li>
                    <li class="list-three">
                      <div class="quantity-control"> 
                        <a href="javascript:void(0);" class="left noaction"></a>
                        <input name="spinnerinput" type="text" class="text-box qty" value="01" readonly  >
                        <a href="javascript:void(0);" class="right noaction"></a> 
                        <input class="sandwich_id" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
">
                        <input type="hidden" value="product" name="data_type">
                      </div>
                    </li>
                    <li class="list-four">
                      <a href="javascript:void(0);" class="link <?php if ($_smarty_tpl->tpl_vars['v']->value['allow_spcl_instruction'] == 1 || $_smarty_tpl->tpl_vars['v']->value['add_modifier'] == 1) {?> salad-listing <?php } else { ?> common_add_item_cart <?php }?>" data-sandwich="<?php echo $_smarty_tpl->tpl_vars['product']->value;?>
" data-sandwich_id="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
images/sandwich_menu/cart.png"><img src=><span>ADD</span></a>
                   </li>
               </ul>
           </div>
       </li>
    </ul>
    <?php  } else { ?>
    <ul class="drinks_main_ul">
        <li class="featured_item">
             <div class="featured_img">
                <img title="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
" class="" data-href="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['salad_image_path']->value;
echo $_smarty_tpl->tpl_vars['v']->value['product_image'];?>
">
             </div>
             <div class="featured_item_detail salads-wrapper">
                  <h3 class="drinks_product_name"><?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
</h3>
                  <ul data-id="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" data-product_name="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_name'];?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['v']->value['description'];?>
" data-product_image="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_image'];?>
" data-product_price="<?php echo $_smarty_tpl->tpl_vars['v']->value['product_price'];?>
" data-standard_category_id="<?php echo $_smarty_tpl->tpl_vars['v']->value['standard_category_id'];?>
" data-image_path="<?php echo $_smarty_tpl->tpl_vars['salad_image_path']->value;?>
" data-uid=<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
 data-product=<?php echo $_smarty_tpl->tpl_vars['product']->value;?>
 data-spcl_instr="<?php echo $_smarty_tpl->tpl_vars['v']->value['allow_spcl_instruction'];?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['v']->value['add_modifier'];?>
" data-modifier_desc="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_desc'];?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['v']->value['add_modifier'];?>
" data-modifier_isoptional="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_isoptional'];?>
" data-modifier_is_single="<?php echo $_smarty_tpl->tpl_vars['v']->value['modifier_is_single'];?>
" data-modifier_options='<?php echo json_encode($_smarty_tpl->tpl_vars['modifier_options']->value[$_smarty_tpl->tpl_vars['k']->value]);?>
'>
                     <li class="list-one">
                        <h3>$<?php echo $_smarty_tpl->tpl_vars['v']->value['product_price'];?>
</h3>
                     </li>
                    <li class="list-three">
                      <div class="quantity-control"> 
                        <a href="javascript:void(0);" class="left noaction"></a>
                        <input name="spinnerinput" type="text" class="text-box qty" value="01" readonly  >
                        <a href="javascript:void(0);" class="right noaction"></a> 
                        <input class="sandwich_id" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
">
                        <input type="hidden" value="product" name="data_type">
                      </div>
                    </li>
                    <li class="list-four">
                      <a href="javascript:void(0);" class="link <?php if ($_smarty_tpl->tpl_vars['v']->value['allow_spcl_instruction'] == 1 || $_smarty_tpl->tpl_vars['v']->value['add_modifier'] == 1) {?> salad-listing <?php } else { ?> common_add_item_cart <?php }?>" data-sandwich="<?php echo $_smarty_tpl->tpl_vars['product']->value;?>
" data-sandwich_id="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
images/sandwich_menu/cart.png"><img src=><span>ADD</span></a>
                   </li>
               </ul>
           </div>
       </li>
    </ul>
    <?php  } ?>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    
</div>
</div>
</div>
</div>
<?php }
}
