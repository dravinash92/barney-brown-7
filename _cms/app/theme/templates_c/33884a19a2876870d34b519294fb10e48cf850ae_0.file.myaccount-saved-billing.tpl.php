<?php
/* Smarty version 3.1.39, created on 2021-03-30 09:53:10
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\myaccount-saved-billing.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6062a7aee5b667_78875707',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '33884a19a2876870d34b519294fb10e48cf850ae' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\myaccount-saved-billing.tpl',
      1 => 1587960782,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062a7aee5b667_78875707 (Smarty_Internal_Template $_smarty_tpl) {
?>        <div class="saved-address-wrapper">
          <ul class="saved-address-inner">
            <li> <span class="add-new-address-main"> <a href="#" data-id="0" class="delivery-address add-credit-card"><span>+</span><br />
              ADD NEW<br />
              CREDIT CARD</a> </span> </li>
           
            
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['card_details']->value, 'card', false, 'k');
$_smarty_tpl->tpl_vars['card']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['card']->value) {
$_smarty_tpl->tpl_vars['card']->do_else = false;
?>			
			<?php $_smarty_tpl->_assignInScope('cardnos', str_split($_smarty_tpl->tpl_vars['card']->value->card_number,4));?>	
				<li>
					<span class="address-content">
						<h3 style="text-transform: uppercase;"><?php echo $_smarty_tpl->tpl_vars['card']->value->card_type;?>
</h3>
						<p> 
						
							<span style="text-transform:capitalize;"><?php echo $_smarty_tpl->tpl_vars['card']->value->card_type;?>
</span> <br/>
							<?php echo $_smarty_tpl->tpl_vars['card']->value->card_number_masked;?>
<br />
							<?php echo $_smarty_tpl->tpl_vars['card']->value->card_zip;?>
<br />
							<?php echo $_smarty_tpl->tpl_vars['card']->value->address1;?>
<br />
							<?php echo $_smarty_tpl->tpl_vars['card']->value->street;?>

						</p>
					</span> <span class="button-holder-address"> <a href="javascript:void(0)" data-id="<?php echo $_smarty_tpl->tpl_vars['card']->value->id;?>
" class="edit edit_card">EDIT</a> <a href="javascript:void(0)" data-id="<?php echo $_smarty_tpl->tpl_vars['card']->value->id;?>
" class="remove remove_card">remove</a> </span>
				</li>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            
          </ul>
        </div>
      </div>
  </div>

<?php }
}
