<?php
/* Smarty version 3.1.39, created on 2021-03-25 19:29:45
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\checkout-choose-delivery-or-pickup.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c97517d73a7_12420013',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bdba410e8ba628aac46918af78d4989dd663428d' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\checkout-choose-delivery-or-pickup.tpl',
      1 => 1592406527,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c97517d73a7_12420013 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_cms\\app\\smarty\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),1=>array('file'=>'C:\\wamp64\\www\\hashbury\\_cms\\app\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
      <div class="menu-list-wrapper">
	  <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu" class="back-to-my-menu-button"> <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/keep-shopping.png"  alt="Back to my menu button" /> </a>
    
        <h1>CHECKOUT</h1>
        <div class="checkout-wrapper">
          <div class="check-out-left">
			
             <?php echo $_smarty_tpl->tpl_vars['cartItemList']->value;?>

			</div>
           
          
          <?php if ($_smarty_tpl->tpl_vars['cart']->value != '') {?>
          <div class="check-out-right">
            <div class="delivery-details-wrapper">
            <div class="checkoutdynamicdiv">
              <?php if ($_smarty_tpl->tpl_vars['order_count']->value > 0) {?>
               <div class="top-buttons-wrapper">
                 <ul>
				 <!--  Needs to remove width:100% after uncommenting pick-up -->
                   <li style="width:100%;display:none"><a onclick=changeCheckoutoption('deliverychooseaddress') href="javascript:void(0)">DELIVERY</a></li>
				    <!-- REMOVED FOR LAUNCH -->
                     <!--  <li><a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)">PICK-UP</a></li> -->
					<!-- END --> 
                  </ul>
                </div>
              
                <div class="choose-address">
                <p>Choose a</p>
                <a onclick=changeCheckoutoption('deliverychooseaddress') href="javascript:void(0)" class="delivery-address link">DELIVERY ADDRESS</a>
                <!-- REMOVED FOR LAUNCH -->
                  <!--
                <p>Or Pick-Up From</p>
                <a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)" class="delivery-address link">STORE LOCATION</a> --> 
				<!-- END --> 
				</div>
                </div>
              <?php } else { ?>
                <div class="choose-address first_order" style="padding-top: 5px;">
                <p style="margin-bottom: 35px;">Delivery Address</p>
                <a onclick=changeCheckoutoption('deliverychooseaddress') href="javascript:void(0)" class="delivery-address link">ADD DELIVERY ADDRESS <!-- DELIVERY ORDER --></a>
                <p class="first_order_text"><?php echo $_smarty_tpl->tpl_vars['DELIVERY_FIRST_ORDER']->value;?>
</p>
				 <!-- REMOVED FOR LAUNCH -->
               <!-- <p style="text-align:center">or</p>
                <a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)" class="delivery-address link">PICK-UP ORDER</a> 
                 <p class="first_order_text"><?php echo $_smarty_tpl->tpl_vars['PICKUP_FIRST_ORDER']->value;?>
</p>
                 -->
				 <!-- END --> 
                </div>
                </div>
              <?php }?>
              <div class="date-time">
                <p>Date / Time</p> 

                <input type="radio" class="css-checkbox" id="radio1"  onclick="hideDate(true)" name="radiog_lite" value="now">
                <label class="css-label" for="radio1">Now</label>
                
                <input type="hidden" name="todays_date" value="<?php  echo date(l); ?>" />
                   <input type="hidden" name="todays_time" value="<?php  echo date('H:00'); ?>" />
                   
                   <input type="hidden" name="delivery_store_id" value="" />
                
                <input type="radio" class="css-checkbox" id="radio2" onclick="hideDate(false)" <?php if ($_smarty_tpl->tpl_vars['currenthour']->value > 16) {?>checked="checked" <?php }?> name="radiog_lite" value="specific">
                <label class="css-label" for="radio2">Specific Date/Time</label>
                <?php if ($_smarty_tpl->tpl_vars['currenthour']->value < 16 && $_smarty_tpl->tpl_vars['currenthour']->value >= 10) {?>  <?php } else { ?> <?php echo '<script'; ?>
>console.log( "TOO LATE MANUAL FILE" ); _GLOBAL_CLOSED = 1; <?php echo '</script'; ?>
><?php }?>

                <div class="date-time-text" style="display: none;">
                     <div class="date-picker">
                    <input name="" type="text" class="datepicker date delivery_date" placeholder="" readonly />
                    <input type="hidden" name="hidden_store_id" id="hidden_store_id" value=""/>
                    <input type="hidden" name="selected_day" id="selected_day" value=""/>
                    <input type="hidden" name="selected_date" id="selected_date" value="0"/>
                       <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addresses']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
            <?php if ($_smarty_tpl->tpl_vars['k']->value == 0) {?>
            <input value="<?php echo $_smarty_tpl->tpl_vars['v']->value->id;?>
" type="hidden" name=="hidden_default_store_id" id="hidden_default_store_id" />
            <?php }?>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                  
                   
                  <div class="time"> <div class="timechange">  <?php echo $_smarty_tpl->tpl_vars['times']->value;?>
</div>
                   

                  </div>
                </div>
              </div>
              <p>Billing
              <a class="change change-billing" style="display:none;">CHANGE</a>

              </p>
              
              
            
              
              <div class="select_address_wrap" <?php if (count($_smarty_tpl->tpl_vars['billingInfo']->value) == 0) {?> style="display:none;"<?php }?>>
			  <select class="billinglist" name="changeBilling" id="changeBilling" >
				<option value="-1">Choose a Credit card</option>
				<?php
$__section_billings_2_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['billingInfo']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_billings_2_start = min(0, $__section_billings_2_loop);
$__section_billings_2_total = min(($__section_billings_2_loop - $__section_billings_2_start), $__section_billings_2_loop);
$_smarty_tpl->tpl_vars['__smarty_section_billings'] = new Smarty_Variable(array());
if ($__section_billings_2_total !== 0) {
for ($__section_billings_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_billings']->value['index'] = $__section_billings_2_start; $__section_billings_2_iteration <= $__section_billings_2_total; $__section_billings_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_billings']->value['index']++){
?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['billingInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_billings']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_billings']->value['index'] : null)]['id'];?>
">
				<?php $_smarty_tpl->_assignInScope('cardname', smarty_modifier_capitalize($_smarty_tpl->tpl_vars['billingInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_billings']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_billings']->value['index'] : null)]['card_type'],true));?>
				<?php if ($_smarty_tpl->tpl_vars['cardname']->value == "American Express") {?>  
				<?php $_smarty_tpl->_assignInScope('cardname', "AMEX");?> 
				<?php } else { ?>
				<?php }?>
				<?php $_smarty_tpl->_assignInScope('cardnos', $_smarty_tpl->tpl_vars['billingInfo']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_billings']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_billings']->value['index'] : null)]['card_number']);?>
				<?php echo $_smarty_tpl->tpl_vars['cardname']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['cardnos']->value;?>

				</option>
				<?php
}
}
?>
				<option value="0">Add New</option>
			</select>
			 </div>
              
              <?php if (count($_smarty_tpl->tpl_vars['billingInfo']->value) == 0) {?>
              <a id="addcreidt_card" href="javascript:void(0)" class="delivery-address link add-credit-card-link"> <?php if ($_smarty_tpl->tpl_vars['order_count']->value > 0) {?>ADD CREDIT CARD <?php } else { ?> ADD BILLING INFO <?php }?></a> 
              <?php }?>
              

              
              <h6 class="billingDetails" rel="" style="display:none">
             

               <span class="card_typ" style="text-transform: uppercase;">Visa</span> 
               <br>
               Ending in <span class="card_no"></span> <br>

               
               <input type="hidden" name="card_id" value="" />
              </h6>
                                <input type = "hidden" name="_card_number_auth" value="" />
							  <input type = "hidden" name="_expiry_month_auth" value="" />
							  <input type = "hidden" name="_expiry_year_auth" value="" />
							              
              </div>
              
            
             <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['order_string']->value;?>
" name="odstring" />
             <a href="javascript:void(0);" class="place-order link">PLACE ORDER</a></div>
        
        <?php }?><!-- if no cart value there -->     
        </div>
      </div>
      <!--Create Sandwich Ending-->
    </div>
  </div>
<!--Outer wrapper Ending-->
<!--Popup Start-->
<div class="popup-wrapper" id="credit-card-details">
  <div class="add-new-address-inner"> <a href="javascript:void(0)" class="close-button">Close</a>
    <div class="title-holder">
      <h1>ADD NEW CREDIT CARD</h1>
    </div>
    <span class="error_msg"></span>
     <form id="billForm">
    <ul class="from-holder">
   
      <li><span class="text-box-holder">
        <p>Card Number</p>
        <input name="cardNo" type="text" class="text-box-control" placeholder="">
        </span> <span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="cardCvv" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
      <p>Credit Card</p>
        <span class="credit-card">
        <select name="cardType"  >
               <option value="visa">Visa</option>
          <option value="MasterCard">MasterCard</option>
          <option value="Discover">Discover</option>
          <option value="American Express">American Express</option>
        </select>
        </span> <br><br><br>
        <p>Expiration Date</p>
        <span class="month">
        <select name="cardMonth"  >
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="04">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <select name="cardYear">
         
          
          
<?php $_smarty_tpl->_assignInScope('currentyear', smarty_modifier_date_format(time(),"%Y"));
$_smarty_tpl->_assignInScope('numyears', 50);
$_smarty_tpl->_assignInScope('totalyears', $_smarty_tpl->tpl_vars['currentyear']->value+$_smarty_tpl->tpl_vars['numyears']->value);
$__section_loopyers_3_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['totalyears']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_loopyers_3_start = (int)@$_smarty_tpl->tpl_vars['currentyear']->value < 0 ? max(0, (int)@$_smarty_tpl->tpl_vars['currentyear']->value + $__section_loopyers_3_loop) : min((int)@$_smarty_tpl->tpl_vars['currentyear']->value, $__section_loopyers_3_loop);
$__section_loopyers_3_total = min(($__section_loopyers_3_loop - $__section_loopyers_3_start), $__section_loopyers_3_loop);
$_smarty_tpl->tpl_vars['__smarty_section_loopyers'] = new Smarty_Variable(array());
if ($__section_loopyers_3_total !== 0) {
for ($__section_loopyers_3_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index'] = $__section_loopyers_3_start; $__section_loopyers_3_iteration <= $__section_loopyers_3_total; $__section_loopyers_3_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index']++){
?>
<option><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_loopyers']->value['index'] : null);?>
</option>
<?php
}
}
?>
         
        </select>
        </span> </span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="cardZip" type="text" class="text-box-billing-zip" >
        </span> </li>

      <li> <span class="checkbox-save-bill">
        <input  type="checkbox" id="save_billing"  name="save_billing" value="save_billing" checked="checked"/>
        <label for="save_billing">Save Billing Info <!--<span>(Verisign Encryption)</span>--></label>
        </span> </li>
      <li> <span class="card-holder-margin">
        <h3>Barney Brown meets all requirements for PCI Compliance, customer information is stored using Authorize.NET's secure servers. Customer information is encrypted using a 256-bit SSL.</h3>
        <a href="javascript:void(0)" class="add-address">ADD</a> </span> </li>
    </ul>
    </form>
  </div>
  
  <form id="_payment_form" action="paymentpage/" method="post" style="display:none">
  <input type = "hidden" name="_card_type" value="" />
  <input type = "hidden" name="_card_number" value="" />
  <input type = "hidden" name="_card_cvv" value="" />
  <input type = "hidden" name="_card_zip" value="" />
  <input type = "hidden" name="_card_name" value="" />
  <input type = "hidden" name="_expiry_month" value="" />
  <input type = "hidden" name="_expiry_year" value="" />
  </form>
  
</div>


<!--Popup Ending-->


<?php }
}
