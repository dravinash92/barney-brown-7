<?php
/* Smarty version 3.1.39, created on 2021-03-25 17:28:51
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\user_sandwiches.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7afbc49004_47586897',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2e20615d2c6656a6d6b2557c904b412a583c9a89' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\user_sandwiches.tpl',
      1 => 1616672379,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7afbc49004_47586897 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="featured_sandwitch-wrapper menu-list-wrapper">
                    <h1>MY SAVED SANDWICHES</h1>
                    <input type="hidden" name="countMenuItems" value="0">
                    <h2><span class="mr_8"></span>
                      My Saved Sandwiches
                      <span class="featured-back-arrow">
                       <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/back-arrow.png">
                       <h3 class="">BACK</h3></a>
                      </span>
                    </h2>
                     <div class="featured_items menu-listing">
                      <?php if (!empty($_smarty_tpl->tpl_vars['featured_data']->value)) {?>
                      <ul>
                        <?php
$__section_sandwitch_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['featured_data']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_0_start = min(0, $__section_sandwitch_0_loop);
$__section_sandwitch_0_total = min(($__section_sandwitch_0_loop - $__section_sandwitch_0_start), $__section_sandwitch_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_0_total !== 0) {
for ($__section_sandwitch_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_0_start; $__section_sandwitch_0_iteration <= $__section_sandwitch_0_total; $__section_sandwitch_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>
             
                        <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0]);?>
                        <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
                        <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
                        <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
                        <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>
                        
                        <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['bread_name']->value);?>
                        
                        <?php 
                        $result = '';
                        $d = $_smarty_tpl->get_template_vars('prot_data');
                        if($d){
                        foreach($d as $d){
                        
                         $d = trim($d);
                         $d = str_replace(' ','##',$d);
                         $result .= ' '.$d;
                        }}
                        ?>
                        
                        <?php 
                        $result_1 = '';
                        $c = $_smarty_tpl->get_template_vars('cheese_data');
                         if($c){
                        foreach($c as $c){
                        
                         $c = trim($c);
                         $c = str_replace(' ','##',$c);
                        
                         $result_1 .= ' '.$c;
                        }}
                        ?>
                        
                        
                        <?php 
                        $result_2 = '';
                        $t = $_smarty_tpl->get_template_vars('topping_data');
                        if($t){
                        foreach($t as $t){
                         $t = trim($t);
                         $t = str_replace(' ','##',$t);
                         $result_2 .= ' '.$t;
                        }}
                        ?>
                        
                        
                        <?php 
                        if($o){       
                        $result_3 = '';
                        $o = trim($o);
                        $o = str_replace(' ','##',$o);
                        $o = $_smarty_tpl->get_template_vars('cond_data');
                        foreach($o as $o){
                         $result_3 .= ' '.$o;
                        }}
                        ?>

                       <?php if (is_array($_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id'])) {?> 
                       <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id']));?>
                       <?php } else { ?>
                       <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
                       <?php }?>
              
                      <?php if ($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['by_admin'] == 1) {?>
                      <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['ADMIN_URL']->value);?>
                      <?php } else { ?>
                      <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>
                      <?php }?>
                      
                      <?php $_smarty_tpl->_assignInScope('sdesc', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc']);?>
                     
                      <?php $_smarty_tpl->_assignInScope('fname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['first_name']);?>
                      <?php $_smarty_tpl->_assignInScope('user_name', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name']);?>
                      <?php $_smarty_tpl->_assignInScope('lname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['last_name']);?>
                      <?php $_smarty_tpl->_assignInScope('sprice', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price']);?>
                      <?php $_smarty_tpl->_assignInScope('sname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name']);?>
                       <?php $_smarty_tpl->_assignInScope('tname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name_trimmed']);?>
                        <li class="featured_item" data-id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">
                          <input type="hidden" id="featured_sandwich_view_popup" value="1">
                           
                         <div class="featured_img">
                           <img title="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" class="view_sandwich" data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png">
                           <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/toasted.png" class="toasted" style="<?php if ($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'] == 1) {?> display:block; <?php }?>">
                         </div>
                         <div class="featured_item_detail salads-wrapper">
                           <ul id="toastID_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-sandwich_desc_id ="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc_id'];?>
"  data-flag="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['flag'];?>
" data-menuadds="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_add_count'];?>
" data-userid ="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
" <?php if ($_smarty_tpl->tpl_vars['items_id']->value == 1) {?> rel="ADD TO CART" <?php } else { ?> rel="ADD TO CART" <?php }?> data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-toast="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'];?>
" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-likeid="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_id'];?>
" data-likecount="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_count'];?>
">
                          
                          
                          <input type="hidden" name="chkmenuactice<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active'];?>
" />

                            <img data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png" hidden>
                            <input type="hidden" name="image" id="sandImg_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png">
                             <li class="list-one">
                              <?php $_smarty_tpl->_assignInScope('jsonData', json_decode($_smarty_tpl->tpl_vars['sandwich']->value->sandwich_data,1));?>
                               <h3><?php echo $_smarty_tpl->tpl_vars['sname']->value;?>
</h3>
                               <p>
                               <?php echo $_smarty_tpl->tpl_vars['sdesc']->value;?>

                              <input type="hidden" name="saved_tgl<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="1">
                             </p>
                             <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['is_public'];?>
" id="isPublic<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">
                               <p class="created_by">Created by: <span style="margin-left: 3px;"><?php echo $_smarty_tpl->tpl_vars['user_name']->value;?>
.</span></p>
                               <a class="featured_item_btn" href="javascript:void(0);">VIEW</a>
                               <a class="featured_item_btn" href="javascript:void(0);">EDIT</a>
                               <?php if ($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active'] == 1) {?>
                               <a class="featured_item_btn saved_btn" href="javascript:void(0);">SAVED</a>
                              <?php } else { ?>
                               <a class="featured_item_btn" id="save_toggle<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" href="javascript:void(0);">SAVE</a>
                              <?php }?>
                              <?php if ($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['is_public'] == 0) {?>
                                          <span><img title="private" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
images/sandwich_menu/lock.png"></span>
                                          <?php }?>
                             </li>
                             <input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $_SESSION['uid'];?>
" >
                             <li class="list-two">
                               <h4>$<?php echo $_smarty_tpl->tpl_vars['sprice']->value;?>
</h4>
                             </li>
                             <li class="list-three">
                               <div class="quantity-control"> 
              
                                  <a href="javascript:void(0);" class="left noaction sandwichQtyLeft"></a>
                                        <input name="sandwichQty<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" type="text" class="text-box qty" value="01" readonly="">
                                 
                                  <a href="javascript:void(0);" class="right noaction sandwichQtyRight"></a>  
                                    
                                  <input class="typeSandwich" type="hidden" value="SS">

                                  <input type="hidden" value="product" name="data_type">
                
                              </div>
                             </li>
                             <li class="list-four">
                               <!-- <a href="javascript:void(0);">ADD</a> -->
                               <a href="javascript:void(0);" class="quickAddToCart"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/cart.png"><span>ADD</span></a>
                             </li>
                           </ul>
                         </div>
                        </li>
                        <?php
}
}
?>
                       
                      </ul>
                      <?php } else { ?>
                        <div class="no-saved-sandwiches">
                           <h3>
                           you currently have no saved sandwiches
                           </h3>
                           <p>EITHER CREATE YOUR OWN CUSTOM SANDWICH, CHOOSE FROM OUR USER-
                           SUBMITTED GALLERY, OR GRAB ONE OF YOUR FRIEND'S SANDWICHES.
                           </p>
                        </div>
                      <?php }?>
                     </div>
                  </div>
<?php }
}
