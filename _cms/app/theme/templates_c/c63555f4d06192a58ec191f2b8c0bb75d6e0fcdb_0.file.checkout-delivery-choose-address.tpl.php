<?php
/* Smarty version 3.1.39, created on 2021-03-25 19:29:48
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\checkout-delivery-choose-address.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c97544cf885_39195807',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c63555f4d06192a58ec191f2b8c0bb75d6e0fcdb' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\checkout-delivery-choose-address.tpl',
      1 => 1587960782,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c97544cf885_39195807 (Smarty_Internal_Template $_smarty_tpl) {
?>             <div class="top-buttons-wrapper">
                <ul>
				 <!--  Needs to remove width:100% after uncommenting pick-up -->
                  <li style="width:100%;display:none" ><a href="javascript:void(0)" class="active">DELIVERY</a></li>
				   <!-- REMOVED FOR LAUNCH -->
                   <!--  <li><a onclick=changeCheckoutoption('pickupstorelocation') href="javascript:void(0)">PICK-UP</a></li> -->
				    <!-- END -->
                </ul>
              </div>
              <div class="choose-address">
                <div class="center-the-button">
                  <p>Choose a Delivery Address</p>

					<div class="select_address_wrap" id="select_address_div">
					<select name="address" class="address select_address" >
						<option value="">Choose a Delivery address</option>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addresses']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>

							<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value->address_id;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->address1;?>
</option>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						<option value="0">Add New</option>
					</select>
					</div>
                </div>
              </div>
              
<?php }
}
