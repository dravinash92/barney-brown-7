<?php
/* Smarty version 3.1.39, created on 2021-03-30 09:55:40
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\myaccount-saved-address.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6062a844013b57_60541188',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3fb5a444a18f490e6c423240d4b312717d4843ab' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\myaccount-saved-address.tpl',
      1 => 1617078321,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062a844013b57_60541188 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_cms\\app\\smarty\\libs\\plugins\\modifier.truncate.php','function'=>'smarty_modifier_truncate',),));
?>
		<div class="saved-address-wrapper">
          <ul class="saved-address-inner">
            <li> <span class="add-new-address-main"> <a href="javascript:void(0);" class="change change-address" ><span>+</span><br />
              ADD NEW<br />
              ADDRESS</a> </span>
            </li>
            <?php if (count($_smarty_tpl->tpl_vars['addresses']->value) > 0) {?>
         
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['addresses']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
				<li> <span class="address-content">
				  <h3> <?php echo $_smarty_tpl->tpl_vars['v']->value->address1;?>
</h3>
				  <p><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
<br />
					<?php if ($_smarty_tpl->tpl_vars['v']->value->company != '') {
echo $_smarty_tpl->tpl_vars['v']->value->company;?>
<br /><?php }?>
					<?php echo $_smarty_tpl->tpl_vars['v']->value->address1;?>
 <?php if ($_smarty_tpl->tpl_vars['v']->value->street != '') {?>, <?php echo $_smarty_tpl->tpl_vars['v']->value->street;?>
 <?php }?><br />										
					<?php if ($_smarty_tpl->tpl_vars['v']->value->cross_streets != '') {
echo $_smarty_tpl->tpl_vars['v']->value->cross_streets;?>
 <br /> <?php }?>
					New York, <?php echo $_smarty_tpl->tpl_vars['v']->value->zip;?>
<br />
					<?php echo $_smarty_tpl->tpl_vars['v']->value->phone;
if ($_smarty_tpl->tpl_vars['v']->value->extn != '') {?> ext.<?php echo $_smarty_tpl->tpl_vars['v']->value->extn;
}?></p>
				  <?php if ($_smarty_tpl->tpl_vars['v']->value->delivery_instructions != '') {?><p>DELIVERY INSTRUCTIONS:<br/><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['v']->value->delivery_instructions,35,"...");?>
</p><?php }?><br/>					
				  </span> 
				<span class="button-holder-address">
					<a data-target="<?php echo $_smarty_tpl->tpl_vars['v']->value->address_id;?>
" class="edit edit_address">EDIT</a>
					<a data-target="<?php echo $_smarty_tpl->tpl_vars['v']->value->address_id;?>
" class="remove remove_address">remove</a>
				</span>
				</li>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php }?>
          </ul>
        </div>
      </div>
  </div>
  
 
<?php }
}
