<?php
/* Smarty version 3.1.39, created on 2021-03-25 17:24:27
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\catering.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c79f34d0c99_20693262',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '360e452f2b5ba08cd417c990774f0940759a6cdc' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\catering.tpl',
      1 => 1592378329,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c79f34d0c99_20693262 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="menu-list-wrapper">
  <h1>CATERING</h1>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cateringdataList']->value, 'list', false, 'j');
$_smarty_tpl->tpl_vars['list']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['list']->value) {
$_smarty_tpl->tpl_vars['list']->do_else = false;
?>

<?php if (count($_smarty_tpl->tpl_vars['list']->value['data']) > 0) {?>

<h2><?php echo $_smarty_tpl->tpl_vars['list']->value['cat_name'];?>
</h2>
<div class="salads-wrapper">

  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value['data'], 'data', false, 'k');
$_smarty_tpl->tpl_vars['data']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->do_else = false;
?>

  <ul>
    <li> 
      <img title="<?php echo $_smarty_tpl->tpl_vars['data']->value['product_name'];?>
" class="view_sandwich cat-image"  src="<?php echo $_smarty_tpl->tpl_vars['salad_image_path']->value;
echo $_smarty_tpl->tpl_vars['data']->value['product_image'];?>
" style=" min-width: 180px;   max-width: 180px;margin: 5px;max-height: 130px;height: 100%;">
    </li>

    <li class="list1 list1-catering" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" data-product_name="<?php echo $_smarty_tpl->tpl_vars['data']->value['product_name'];?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
" data-product_image="<?php echo $_smarty_tpl->tpl_vars['data']->value['product_image'];?>
" data-product_price="<?php echo $_smarty_tpl->tpl_vars['data']->value['product_price'];?>
" data-standard_category_id="<?php echo $_smarty_tpl->tpl_vars['data']->value['standard_category_id'];?>
" data-image_path="<?php echo $_smarty_tpl->tpl_vars['salad_image_path']->value;?>
" data-uid=<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
 data-product="<?php echo $_smarty_tpl->tpl_vars['product']->value;?>
" data-spcl_instr="<?php echo $_smarty_tpl->tpl_vars['data']->value['allow_spcl_instruction'];?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['data']->value['add_modifier'];?>
" data-modifier_desc="<?php echo $_smarty_tpl->tpl_vars['data']->value['modifier_desc'];?>
" data-add_modifier="<?php echo $_smarty_tpl->tpl_vars['data']->value['add_modifier'];?>
" data-modifier_isoptional="<?php echo $_smarty_tpl->tpl_vars['data']->value['modifier_isoptional'];?>
" data-modifier_is_single="<?php echo $_smarty_tpl->tpl_vars['data']->value['modifier_is_single'];?>
" data-modifier_options='<?php echo $_smarty_tpl->tpl_vars['data']->value['modifier_options'];?>
'>
      <h3><?php echo $_smarty_tpl->tpl_vars['data']->value['product_name'];?>
</h3>
      <p><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</p>
      <a class="catering-view catering-listing" href="javascript:void(0);">view</a>
    </li>
    <li class="list2">
      <h4>$<?php echo $_smarty_tpl->tpl_vars['data']->value['product_price'];?>
</h4>
    </li>
    <li class="list3">
      <div class="quantity-control"> <a href="javascript:void(0)" class="left noaction"></a>
        <input name="spinnerinput" type="text" class="text-box qty" value="01" readonly>
        <a href="javascript:void(0)" class="right noaction"></a> 
        <input class="sandwich_id" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">
        <input type="hidden" value="product" name="data_type">
      </div>
    </li>
    <li class=""> 
      <a href="#" class="link <?php if ($_smarty_tpl->tpl_vars['data']->value['allow_spcl_instruction'] == 1 || $_smarty_tpl->tpl_vars['data']->value['add_modifier'] == 1) {?> catering-listing <?php } else { ?> common_add_item_cart  <?php }?> add-cart-catering" data-sandwich="product" data-sandwich_id="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
" ><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
images/sandwich_menu/cart.png"><span>ADD</span></a>              
    </li>
  </ul>

  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

</div>

<?php }?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

</div> 
<?php }
}
