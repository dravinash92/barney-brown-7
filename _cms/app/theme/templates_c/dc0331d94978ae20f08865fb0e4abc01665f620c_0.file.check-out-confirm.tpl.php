<?php
/* Smarty version 3.1.39, created on 2021-03-31 21:23:37
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\check-out-confirm.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_60649b013649c0_32202874',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dc0331d94978ae20f08865fb0e4abc01665f620c' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\check-out-confirm.tpl',
      1 => 1617083905,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60649b013649c0_32202874 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_cms\\app\\smarty\\libs\\plugins\\function.math.php','function'=>'smarty_function_math',),));
?>
<div class="container">
  
  <div id="content-container">
  <h1>Order Confirmation</h1>
<h2 class="your-good">You're good to go!</h2>
	



  
  <table>
    
      <?php if ($_smarty_tpl->tpl_vars['check_now_specific']->value == 0) {?>

  		<p class="delivery-time" >Estimated   Delivery  time</p>
  		<p class='checkout-order-number'><?php if ($_smarty_tpl->tpl_vars['delivery_type']->value) {?> <?php echo $_smarty_tpl->tpl_vars['messege']->value;?>
 <?php } else { ?> 5 - 10 Minutes <?php }?></p>
  		
    <?php }?>
    
        <p class="delivery-time" >ORDER NUMBER</p>
        <p class='checkout-order-number'><?php echo $_smarty_tpl->tpl_vars['order_number']->value;?>
 </p>

    

  <table class="table">
      <thead>
        <tr>
          <th><?php if ($_smarty_tpl->tpl_vars['delivery_type']->value) {?>Delivery<?php } else { ?>Delivery Address<?php }?></th>
          <th>Billing</th>
          <th>Date/Time</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php if ($_smarty_tpl->tpl_vars['delivery_type']->value) {
echo $_smarty_tpl->tpl_vars['address']->value->name;
} else {
echo $_smarty_tpl->tpl_vars['address']->value->store_name;
}?><br> 
          <?php if ($_smarty_tpl->tpl_vars['address']->value->address1 != '') {?> <?php echo $_smarty_tpl->tpl_vars['address']->value->address1;
}
if ($_smarty_tpl->tpl_vars['address']->value->address2 != '') {?>, <?php echo $_smarty_tpl->tpl_vars['address']->value->address2;?>
 <?php }?><br> 
          <?php if ($_smarty_tpl->tpl_vars['address']->value->street != '') {?>(<?php echo $_smarty_tpl->tpl_vars['address']->value->street;?>
)<br><?php }?>
          New York, NY <?php if ($_smarty_tpl->tpl_vars['address']->value->zip != '') {?> <?php echo $_smarty_tpl->tpl_vars['address']->value->zip;?>
 <?php }?><br>
          <?php if ($_smarty_tpl->tpl_vars['address']->value->phone != '') {?> <?php echo $_smarty_tpl->tpl_vars['address']->value->phone;?>
 <?php }?><br>
          <?php if ($_smarty_tpl->tpl_vars['address']->value->delivery_instructions != '') {?>
          <span>delivery instructions:</span><br> <?php echo $_smarty_tpl->tpl_vars['address']->value->delivery_instructions;?>

          <?php }?>
          </td>
          <td> <span><?php if ($_smarty_tpl->tpl_vars['billing_type']->value != '') {?> <?php echo $_smarty_tpl->tpl_vars['billing_type']->value;?>
</span> <br/> <?php }?> <?php if ($_smarty_tpl->tpl_vars['billing_card_no']->value != '') {?> <?php echo $_smarty_tpl->tpl_vars['billing_card_no']->value;?>
 <?php }?> </td>
          <td><?php echo $_smarty_tpl->tpl_vars['date']->value;?>
<br> <?php echo $_smarty_tpl->tpl_vars['time']->value;?>
</td>
        </tr>
      </tbody>
    </table>
    <div class="br"></div>
      <table class="table discounted-price">
      <thead>
        <tr>
          <th>Items</th>
          <th>Qty</th>
          <th class="text-right">Price</th>
        </tr>
      </thead>
      <tbody>
        <?php
$__section_orddet_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['order_item_detail']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_orddet_0_total = $__section_orddet_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_orddet'] = new Smarty_Variable(array());
if ($__section_orddet_0_total !== 0) {
for ($__section_orddet_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] = 0; $__section_orddet_0_iteration <= $__section_orddet_0_total; $__section_orddet_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']++){
?>
			<tr>
				<td>		
					<span><?php echo $_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->sandwich_name;
echo $_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->product_name;?>
</span><br>
					
					
					
					 <?php if ($_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->type == 'user_sandwich') {?>
					 <?php $_smarty_tpl->_assignInScope('sandwichDetails', explode(",",$_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->sandwich_details));?>
           <?php } else { ?>
           <?php $_smarty_tpl->_assignInScope('sandwichDetails', explode(",",$_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->product_description));?>
           <?php }?>
					 
					 <?php $_smarty_tpl->_assignInScope('sandwichDet', '');?>
					 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sandwichDetails']->value, 'foo', false, NULL, 'det', array (
  'index' => true,
));
$_smarty_tpl->tpl_vars['foo']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['foo']->value) {
$_smarty_tpl->tpl_vars['foo']->do_else = false;
$_smarty_tpl->tpl_vars['__smarty_foreach_det']->value['index']++;
?>
					 
					  <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_det']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_det']->value['index'] : null) == 0) {?>    
						  <?php $_smarty_tpl->_assignInScope('sep', '');?>  
						   <?php } else { ?>
						   <?php $_smarty_tpl->_assignInScope('sep', ", ");?>  
						<?php }?>  
					 
					  <?php if ($_smarty_tpl->tpl_vars['foo']->value != ' ') {?>
						<?php $_smarty_tpl->_assignInScope('sandwichDet', (($_smarty_tpl->tpl_vars['sandwichDet']->value).($_smarty_tpl->tpl_vars['sep']->value)).($_smarty_tpl->tpl_vars['foo']->value));?>  
					  <?php }?>
					 <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					 
					 <!-- Sandwich Details with all unwanted commas removed -->
					 <?php echo $_smarty_tpl->tpl_vars['sandwichDet']->value;?>

					 
					<?php if ($_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->toast) {?><br/><span style='font: 18px "lobster_1.4regular";
    color: #342920;'>Toasted</span><?php }?>
				</td>
				<td class="padding-left"><?php echo $_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->qty;?>
</td>
				<!--<td class="text-right">$<?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->price);?>
</td>-->
				<td class="text-right">$<?php echo smarty_function_math(array('equation'=>"x * y",'x'=>$_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->price,'y'=>$_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->qty,'format'=>"%.2f"),$_smarty_tpl);?>
</td>
			</tr>	
		<?php
}
}
?>		
		<tr>
          <td></td>
          <td class="text-right"><span>Subtotal</span></td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['sub_total']->value;?>
</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Tax</span></td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['tax']->value;?>
</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Delivery</span></td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['delivery_fee']->value;?>
</td>
        </tr>
		    <tr>
          <td></td>
          <td class="text-right"><span>Tip</span></td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['tip']->value;?>
</td>
        </tr>

        <?php if ($_smarty_tpl->tpl_vars['discount']->value > 0) {?>
        <tr>
          <td></td>
          <td class="text-right"><span>Discount</span></td>
           <td class="text-right"><?php if ($_smarty_tpl->tpl_vars['discount']->value > 0) {?> - <?php }?>$<?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
</td>
        </tr>
        <?php }?>
        <tr>
          <td></td>
          <td class="total text-right">Total</td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</td>
        </tr>
        </tbody>
    </table>

  
  </div>
  <div id="sidebar">
  <h2 class="your-good">If you have a minute...</h2>
  <div class="accout-info-wrapper">
  <div class="accout-info-form width100">
	   <form id="user_review">
	   

            <p>What can we do to be better?</p>
            <textarea name="message" cols="" rows="" class="text-area-accout-info width100"></textarea>
            <input type="hidden" name="uid" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->uid;?>
">
            <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['user_review']->value->id;?>
">
            <a href="javascript:void(0);" class="submit link" id="submit_review">Submit</a> </div>
			</div>
		</form>	
            <div class="clearfix"></div><div class="clearfix"></div>
 			<h2 class="play">Play Some More...</h2>
				<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/" class="ord-btn">Create a Sandwich</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/gallery/" class="ord-btn">Sandwich Gallery</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/" class="ord-btn">View My Menu</a>
</div>
  </div>
<?php }
}
