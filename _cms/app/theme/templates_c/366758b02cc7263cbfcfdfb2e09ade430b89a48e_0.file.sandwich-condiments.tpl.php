<?php
/* Smarty version 3.1.39, created on 2021-03-30 11:18:37
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\sandwich-condiments.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6062bbb5d4fad4_18891266',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '366758b02cc7263cbfcfdfb2e09ade430b89a48e' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\sandwich-condiments.tpl',
      1 => 1617083153,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062bbb5d4fad4_18891266 (Smarty_Internal_Template $_smarty_tpl) {
?>
  <h2> Choose Your Condiments </h2>
  <h3><!--    CHOOSE UP TO THREE CONDIMENTS. <br>  $0.50 PER EXTRA CONDIMENT. --> </h3>
  <div class="protein-select-wrapper scroll-bar " style="position: relative; overflow: visible;">
  
  <ul>




                
                <?php
$__section_options_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_options_0_start = min(0, $__section_options_0_loop);
$__section_options_0_total = min(($__section_options_0_loop - $__section_options_0_start), $__section_options_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_options'] = new Smarty_Variable(array());
if ($__section_options_0_total !== 0) {
for ($__section_options_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] = $__section_options_0_start; $__section_options_0_iteration <= $__section_options_0_total; $__section_options_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']++){
?>
                
                
            
                
                  <li>
                    <input data-priority = "<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['image_priority'];?>
" data-empty="false" data-id="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['id'];?>
"  data-price="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['item_price'];?>
" data-image_trapezoid="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['image_trapezoid'];?>
" data-image_round="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['image_round'];?>
" data-image_long="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['image_long'];?>
" data-itemname="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['item_name'];?>
" type="checkbox" value="" name="check" id="condiment-check<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)+1;?>
">
                    <label <?php if ($_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['premium']) {?> class="premium-icon" <?php }?> for="condiment-check<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)+1;?>
"><?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['item_name'];?>
</label>
                  
                    <?php if ($_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['options_id'] != 0) {?>
                    
                    <div class="sub-value"> <a href="#" class="left"></a>
                    <span class="text-box">
                    
                  
                     
                    <?php
$__section_subval_1_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['options_id'])) ? count($_loop) : max(0, (int) $_loop));
$__section_subval_1_start = min(0, $__section_subval_1_loop);
$__section_subval_1_total = min(($__section_subval_1_loop - $__section_subval_1_start), $__section_subval_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_subval'] = new Smarty_Variable(array());
if ($__section_subval_1_total !== 0) {
for ($__section_subval_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index'] = $__section_subval_1_start; $__section_subval_1_iteration <= $__section_subval_1_total; $__section_subval_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index']++){
?>

                    <?php $_smarty_tpl->_assignInScope('option_id', $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index'] : null)]['id']);?>
                    
                    <?php $_smarty_tpl->_assignInScope('display', "none");?>
                    <?php $_smarty_tpl->_assignInScope('current', "false");?>
                    
                    <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index'] : null) == 0) {?> 
                    
                    <?php $_smarty_tpl->_assignInScope('display', "block");?>   
                    <?php $_smarty_tpl->_assignInScope('current', "true");?>
                    
                    <?php }?>
                  
                    <input rel="slide" readonly style="display:<?php echo $_smarty_tpl->tpl_vars['display']->value;?>
"  data-unit="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index'] : null)]['option_unit'];?>
" data-current="<?php echo $_smarty_tpl->tpl_vars['current']->value;?>
" data-optionid="<?php echo $_smarty_tpl->tpl_vars['option_id']->value;?>
" data-image="" data-image_trapezoid="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['option_images'][$_smarty_tpl->tpl_vars['option_id']->value]['tImg'];?>
" data-image_round="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['option_images'][$_smarty_tpl->tpl_vars['option_id']->value]['rImg'];?>
" data-image_long="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['option_images'][$_smarty_tpl->tpl_vars['option_id']->value]['lImg'];?>
" data-price_mul="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index'] : null)]['price_mult'];?>
"  type="text" name="" class="text-box" value="<?php echo $_smarty_tpl->tpl_vars['CONDIEMENTS_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_options']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_options']->value['index'] : null)]['options_id'][(isset($_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_subval']->value['index'] : null)]['option_name'];?>
"/>
                    
                    <?php
}
}
?>
                     
                    </span>
            
                   <a href="#" class="right right-hover"></a> 
				   </div> 
                    
                    <?php }?>
                  
                  </li>
                  
                 <?php
}
}
?>
                  
                  

                </ul>
               </div>
           <?php }
}
