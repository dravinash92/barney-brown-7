<?php
/* Smarty version 3.1.39, created on 2021-03-30 09:49:10
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\myaccount-order-history.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6062a6be019be0_63446140',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4f015d4c26eb86971c90f1cfed0469690a7968fc' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\myaccount-order-history.tpl',
      1 => 1602772577,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062a6be019be0_63446140 (Smarty_Internal_Template $_smarty_tpl) {
?>         <div class="order-history">
         <?php if (count($_smarty_tpl->tpl_vars['orderhistory']->value) > 0) {?> 
		        
         <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orderhistory']->value, 'history', false, 'id');
$_smarty_tpl->tpl_vars['history']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['history']->value) {
$_smarty_tpl->tpl_vars['history']->do_else = false;
?>
          
			<ul class="order-history-inner" >
	            <li> <span class="section1">
	              <h3><?php echo $_smarty_tpl->tpl_vars['history']->value->delivery_type;?>
</h3>
	              <h4><?php echo $_smarty_tpl->tpl_vars['history']->value->date;?>
</h4>
	              <a href="javascript:void(0);" data-order="<?php echo $_smarty_tpl->tpl_vars['history']->value->order_number;?>
" data-order-id="<?php echo $_smarty_tpl->tpl_vars['history']->value->order_id;?>
" data-bread="<?php echo $_smarty_tpl->tpl_vars['history']->value->items[0]->bread;?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['history']->value->items[0]->desc;?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['history']->value->items[0]->desc_id;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['history']->value->items[0]->id;?>
" class="view-details">VIEW DETAILS</a> </span> </li>
	            <li > <span class="section2">
	            <h3>
					  <?php if ($_smarty_tpl->tpl_vars['history']->value->address->address1 != '') {
echo $_smarty_tpl->tpl_vars['history']->value->address->address1;
}?>
				</h3>
	              <p>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>  
						(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php echo $_smarty_tpl->tpl_vars['item']->value->name;?>
 <br/>
						<?php if ($_smarty_tpl->tpl_vars['item']->value->desc != '') {?>
						
						<span class="item_details" style="display: none;" data-bread="<?php echo $_smarty_tpl->tpl_vars['item']->value->bread;?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['item']->value->desc;?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['item']->value->desc_id;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"></span>
						<?php }?>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	              </p>
	              </span> </li>
	            <li> <span class="section3">
	              <h3>$<?php echo $_smarty_tpl->tpl_vars['history']->value->total;?>
</h3>
	              </span> </li>
	              <?php if ($_smarty_tpl->tpl_vars['history']->value->itemCheck > 0) {?>
	            <li> <span class="section4"> <a href="javascript:void(0);" data-order="<?php echo $_smarty_tpl->tpl_vars['history']->value->order_id;?>
" data-from="order_history" class="reorder reorder-button">REORDER</a> </span> </li><?php }?>
			</ul>	
					
         <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
         
        <?php } else { ?>
             <ul class="order-history-inner">
				
	            <li style="width:100%;"><span><h3 class="no_order_history">No Previous Orders.</h3></span></li>	            
	        </ul>    
			
         <?php }?>
        </div>
      </div>
</div>
<?php }
}
