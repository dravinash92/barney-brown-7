<?php
/* Smarty version 3.1.39, created on 2021-04-22 15:16:06
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\order-details.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_608145deaa2bb1_91118117',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8495daa01d985fdff934c20fa8eb8c381a89fb37' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\order-details.tpl',
      1 => 1581942018,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608145deaa2bb1_91118117 (Smarty_Internal_Template $_smarty_tpl) {
?><table class="table">
      <thead>
        <tr>
          <th><?php if ($_smarty_tpl->tpl_vars['delivery_type']->value == 0) {?> Pick-Up  <?php } else { ?> Delivery <?php }?></th>
          <th>Billing</th>
          <th>Date/Time</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $_smarty_tpl->tpl_vars['address']->value->name;?>
<br> 
          <?php if ($_smarty_tpl->tpl_vars['address']->value->address1 != '') {?> <?php echo $_smarty_tpl->tpl_vars['address']->value->address1;?>
 <?php }?> <?php if ($_smarty_tpl->tpl_vars['address']->value->address2 != '') {?>, <?php echo $_smarty_tpl->tpl_vars['address']->value->address2;?>
 <?php }?><br> 
          <?php if ($_smarty_tpl->tpl_vars['address']->value->street != '') {
echo $_smarty_tpl->tpl_vars['address']->value->street;?>
<br><?php }?>
          New York, NY <?php if ($_smarty_tpl->tpl_vars['address']->value->zip != '') {?> <?php echo $_smarty_tpl->tpl_vars['address']->value->zip;?>
 <?php }?><br>
          <?php if ($_smarty_tpl->tpl_vars['address']->value->phone != '') {?> <?php echo $_smarty_tpl->tpl_vars['address']->value->phone;?>
 <?php }?><br>
          <?php if ($_smarty_tpl->tpl_vars['address']->value->delivery_instructions != '') {?>
          <span>delivery instructions:</span><br> <?php echo $_smarty_tpl->tpl_vars['address']->value->delivery_instructions;?>

          <?php }?>
          </td>
          <td><?php if ($_smarty_tpl->tpl_vars['billing_type']->value != '') {?> <?php echo $_smarty_tpl->tpl_vars['billing_type']->value;?>
 <br/> <?php }?> <?php if ($_smarty_tpl->tpl_vars['billing_card_no']->value != '') {?> <?php echo $_smarty_tpl->tpl_vars['billing_card_no']->value;?>
 <?php }?></td>
          <td><?php echo $_smarty_tpl->tpl_vars['date']->value;?>
<br> <?php echo $_smarty_tpl->tpl_vars['time']->value;?>
</td>
        </tr>
      </tbody>
    </table>
    <div class="br"></div>
      <table class="table">
      <thead>
        <tr>
          <th>Items</th>
          <th>Qty</th>
          <th class="text-right">Price</th>
        </tr>
      </thead>
      <tbody>
        <?php
$__section_orddet_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['order_item_detail']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_orddet_0_total = $__section_orddet_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_orddet'] = new Smarty_Variable(array());
if ($__section_orddet_0_total !== 0) {
for ($__section_orddet_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] = 0; $__section_orddet_0_iteration <= $__section_orddet_0_total; $__section_orddet_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']++){
?>
			<tr>
				<td>		
					<span><?php echo $_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->sandwich_name;
echo $_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->product_name;?>
</span><br>
					<?php echo $_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->sandwich_details;?>

				</td>
				<td class="padding-left"><?php echo $_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->qty;?>
</td>
				<td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['order_item_detail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orddet']->value['index'] : null)]->price;?>
</td>
			</tr>	
		<?php
}
}
?>		
		<tr>
          <td></td>
          <td class="text-right"><span>Subtotal</span></td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['sub_total']->value;?>
</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Delivery</span></td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['delivery_fee']->value;?>
</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Tip</span></td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['tip']->value;?>
</td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right"><span>Discount</span></td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
</td>
        </tr>
        <tr>
          <td></td>
          <td class="total text-right">Total</td>
          <td class="text-right">$<?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</td>
        </tr>
        </tbody>
    </table>
<?php }
}
