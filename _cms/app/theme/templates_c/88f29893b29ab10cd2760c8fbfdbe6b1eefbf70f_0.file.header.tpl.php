<?php
/* Smarty version 3.1.39, created on 2021-03-25 16:12:25
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c6911317a61_11564770',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '88f29893b29ab10cd2760c8fbfdbe6b1eefbf70f' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\header.tpl',
      1 => 1601900262,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c6911317a61_11564770 (Smarty_Internal_Template $_smarty_tpl) {
$FB = new FB_API();
$access_token  = @$_SESSION['access_token'];

if(!isset($_SESSION['FBRLH_state']) || !$_SESSION['FBRLH_state']  ){

$FB->FB_check_login();
$url =  $FB->FB_get_url();
$_SESSION['tmpUrl'] = $url;
define('_URL',$url);

} else {
define('_URL',$_SESSION['tmpUrl']);

}
 

?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['Description']->value;?>
">
<meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['KeyWords']->value;?>
">
<meta name="author" content="">
<title><?php echo $_smarty_tpl->tpl_vars['HeaderTag']->value;?>
</title>

    <meta property="og:url"           content="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Barney Brown, Inc." />
  <meta property="og:description"   content="Finally, a New York City sandwich service that does it just the way you want it! Check it out at barneybrown.com!" />
  <meta property="og:image"         content="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/bb_logo_wood.jpg" />

<link href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/screen.css?v=2.2" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/jquery-ui.css">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/jquery.jscrollpane.css"> 
<?php echo '<script'; ?>
>
  global_LAST_CLICKED = false;
global_DATE_OVERRIDE = 0;
global_FORCE_24 = 2;
global_CLOSED = 1;
DO_NOT_MESS_WITH_THE_TIME_BUTTON = false;
<?php echo '</script'; ?>
>


<!--<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/slider.css">-->
<!--[if IE 8]>
 <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/ie8.css">
<![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
      <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"><?php echo '</script'; ?>
>
<![endif]-->

 <?php echo '<script'; ?>
 type="text/javascript">
var SITE_URL   = "<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
";
var siteurl    = "<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
";
var API_URL    = "<?php echo $_smarty_tpl->tpl_vars['API_URL']->value;?>
";
var ADMIN_URL  = "<?php echo $_smarty_tpl->tpl_vars['ADMIN_URL']->value;?>
";
var FB_APP_ID = "<?php echo $_smarty_tpl->tpl_vars['FB_APP_ID']->value;?>
";
var SESSION_ID = "<?php if ($_SESSION['uid'] != '') {
echo $_SESSION['uid'];
} else {
echo $_COOKIE['user_id'];
}?>";
var $sandwich_img_live_uri = "<?php echo $_smarty_tpl->tpl_vars['sandwich_img_live_uri']->value;?>
";
var DATA_ID = "<?php echo $_smarty_tpl->tpl_vars['DATA_ID']->value;?>
";
var BASE_FARE = "<?php echo $_smarty_tpl->tpl_vars['BASE_FARE']->value;?>
";
 <?php echo '</script'; ?>
>
 
 
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.ui.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/lozad-1.9.0.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.mCustomScrollbar.concat.min.js"><?php echo '</script'; ?>
>
<!--<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.placeholder.js"><?php echo '</script'; ?>
>-->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.jscrollpane.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/jquery.mousewheel.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/sandwichcreator.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/sandwich_quickedit.js?v=2.2"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/sandwichcart.js?v=2.2"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/checkout.js?v=2.2"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/ha_common.js?v=2.2"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/js/slides.min.jquery.js"><?php echo '</script'; ?>
>
</head>
<body>
    <div class="dynamic_updates_fixed">
      <div class="left-block"><p> 1 item(s) added to cart.</p></div>
      <div class="right-block"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup">CHECKOUT</a></div>
    </div>  
<div id="fb-root"></div> 

<?php echo '<script'; ?>
 src="https://connect.facebook.net/en_US/all.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
FB.init({
    appId: '557060384864429',
    status: true, 
    cookie: true, 
    xfbml: true
});    
<?php echo '</script'; ?>
>


<div id="outer-wrapper">
 <div id="inner-wrapper">
  <div class="top-control"> 
  
  <a class="deliver-button" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup">Manhattan Delivery</a>
  <p class="timing"> Mon - Fri, 10:00AM - 4:00PM</p>
 <!--  <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup" class="deliver-button link">delivery <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/or.png" width="11" height="22"> Pick-up</a> -->
    <div class="container">
      <!--Header Starting-->
      <div class="header">
        <div class="logo"> <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['title_text']->value;?>
"></a> </div>
        <div class="navigation">
          <?php $_smarty_tpl->_assignInScope('currentURL', ($_SERVER['HTTP_HOST']).($_SERVER['REQUEST_URI']));?> 
          <ul>
              <li class="sandwich-menu"><a class="menu-head" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu" >SANDWICHES</a>
                            <div class="rollover">
                              <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu">MENU</a>
                              <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich">CUSTOMIZE</a>
                              <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/savedSandwiches">SAVED</a>
                              <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/featuredSandwiches">FEATURED</a>
                              <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/gallery">ALL</a>
                              
                              <!-- <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/friendsSandwiches">FRIENDS' SANDWICHES</a> -->
                              <!--<a   href="<?php  echo _URL; ?>" id="showFriendsSandwich"   data-fbrel="true" rel="<?php if ($_SESSION['uid']) {?>true<?php } else { ?>false<?php }?>" class="buttons facebook-login"><span class="links">FRIENDS</span> </a>-->
                              <?php   if(isset($_SESSION['fbnoaccess'])) { ?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/friendSandwiches" class="buttons"><span class="links">FRIENDS</span> </a>
                              <?php  } else { ?>
                                <a href="javascript:void(0);" data-fbrel="true" rel="<?php if ($_SESSION['uid']) {?>true<?php } else { ?>false<?php }?>" class="buttons login-facebook facebook-login"><span class="links">FRIENDS</span> </a>
                              <?php  } ?>
                            </div>
                           </li>
            </li>

            <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/salads"  <?php if (strstr($_smarty_tpl->tpl_vars['currentURL']->value,"salads")) {?> class="active" <?php }?>>SALADS</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/snacks"  <?php if (strstr($_smarty_tpl->tpl_vars['currentURL']->value,"snacks")) {?> class="active" <?php }?>>SNACKS</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/drinks"  <?php if (strstr($_smarty_tpl->tpl_vars['currentURL']->value,"drinks")) {?> class="active" <?php }?>>DRINKS</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
catering"  <?php if (strstr($_smarty_tpl->tpl_vars['currentURL']->value,"catering")) {?> class="active" <?php }?>>CATERING</a></li>
          </ul>


        </div>
       <div class="facebook-container"><div class="facebook-container-right">
        <?php  /* if( $validtoken === true ) { */  ?>      
        
<!--             <div class="login-name-holder">
              <h1>Hi, test <span><?php echo $_SESSION['uname'];?>
</span></h1>
              <a href="<?php echo $_smarty_tpl->tpl_vars['signOutURL']->value;?>
">Sign Out</a> 
            </div>
        <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
myaccount" class="account-profile">MY ACCOUNT</a>
        </div>
          
          <div class="facebook-container-left"> <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup" class="shopping-cart"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/shopping-cart.png" alt="cart"> 
      <span><?php echo $_smarty_tpl->tpl_vars['cart']->value;?>
</span>
          </a> 
      </div>
    <div class="dynamic_updates">
      <div class="left-block"></div>
      <div class="right-block"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup" class="  ">CHECKOUT</a></div>
    </div> -->
      
       <?php  /* } else */ if(isset($_SESSION['uid'])) { ?>
       
       
       <div class="login-name-holder">
            <h1>Hi, <span><?php echo $_SESSION['uname'];?>
</span></h1>
            <a href="<?php echo $_smarty_tpl->tpl_vars['signOutURL']->value;?>
">Sign Out</a> 
          </div>
           <?php   if(isset($_SESSION['fbnoaccess'])) { ?>
          <div class="main-account-profile">
           <img src="https://graph.facebook.com/<?php  echo $_SESSION['fb_id']; ?>/picture?width=57&height=57">
           <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
myaccount" class="account-profile">MY ACCOUNT</a>
           </div>

            <?php   } else{ ?>

           <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
myaccount" class="account-profile" style="width: 100%">MY ACCOUNT</a>
            <?php   }  ?>
           
          </div>
          
          <div class="facebook-container-left"> <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup" class="shopping-cart"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/shopping-cart.png" alt="cart">  <span><?php echo $_smarty_tpl->tpl_vars['cart']->value;?>
</span></a> 
      </div>
    <div class="dynamic_updates">
      <div class="left-block"></div>
      <div class="right-block"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup" class="">CHECKOUT</a></div>
    </div>
       
       <?php  } else if($_COOKIE['user_mail'] && $_COOKIE['user_fname'] && $_COOKIE['user_lname'] )  { ?>
      
       <div class="login-name-holder">
            <h1>Hi, <span><?php  echo $_COOKIE['user_fname']; ?></span></h1>
            <a href="<?php echo $_smarty_tpl->tpl_vars['signOutURL']->value;?>
">Sign Out</a> 
          </div> 
          <div class="main-account-profile">
           <!-- <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/user-img.png"> -->
            <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
myaccount" style="width: 100%" class="account-profile">MY ACCOUNT</a>
           </div>
         
         
          </div>
          <div class="facebook-container-left"> <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup" class="shopping-cart"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/shopping-cart.png" alt="cart">  <span><?php echo $_smarty_tpl->tpl_vars['cart']->value;?>
</span></a> 
       
      </div>
    <div class="dynamic_updates">
      <div class="left-block"></div>
      <div class="right-block"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup" class="">CHECKOUT</a></div>
    </div>
       <?php  } else  { ?>
      <a href="javascript:void(0);" class="login-facebook facebook-login">login with facebook</a> <a href="javascript:void(0);" class="login link">login</a>  <a href="javascript:void(0);" class="create-account">Create Account</a>
           
          </div>
          <div class="facebook-container-left"> <a href="javascript:void(0);" class="shopping-cart login link"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/shopping-cart.png" alt="cart">  <span><?php echo $_smarty_tpl->tpl_vars['cart']->value;?>
</span></a>
      </div>
      
      <div class="dynamic_updates" style="display: none;">
      <div class="left-block"></div>
      <div class="right-block"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/choosedeliveryorpickup" class="  ">CHECKOUT</a></div>
    </div>
        <?php  } ?>
        
      </div>
      
      
      <!-- <div class="facebook-likes">
      <div class="fb-like" data-href="http://www.barneybrown.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
      </div> -->
      
      
      </div>
      
<?php }
}
