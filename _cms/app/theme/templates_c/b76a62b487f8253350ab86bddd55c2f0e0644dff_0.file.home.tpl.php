<?php
/* Smarty version 3.1.39, created on 2021-03-25 16:15:54
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c69e226ce28_63381779',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b76a62b487f8253350ab86bddd55c2f0e0644dff' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\home.tpl',
      1 => 1616668339,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c69e226ce28_63381779 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="bannernew">
      <!-- The container is used to define the width of the slideshow -->
  <div id="slides">
    <div class="slides_container">
    
    
    
    <?php if ($_smarty_tpl->tpl_vars['noslide']->value == 1) {?>
     
    
    <?php if ($_smarty_tpl->tpl_vars['pastBanner']->value['details']['by_admin'] == 1) {?>
          <?php $_smarty_tpl->_assignInScope('path', $_smarty_tpl->tpl_vars['ADMIN_URL']->value);?>    
    <?php } else { ?>
      <?php $_smarty_tpl->_assignInScope('path', $_smarty_tpl->tpl_vars['SITE_URL']->value);?> 
    <?php }?>
    
     
     <div class="slide-home">
            <?php if ($_smarty_tpl->tpl_vars['pastBanner']->value['details']['image']) {?>
              <span class="banner-img-top_wrap">
          <img class="" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/banner-top.png" alt="banner-top">
        </span> 
            <span class="banner-image"><img alt="Slide 1" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['pastBanner']->value['details']['image'];?>
"></span>
            <?php } else { ?>
            <span lass="banner-image  "><img alt="Slide 2" src="app/images/app-logo.png" ></span>
            <?php }?>
            <div class="banner-caption"><h3>Your Most Recent Order</h3>
            <h4><?php echo $_smarty_tpl->tpl_vars['pastBanner']->value['street'];?>
</h4>
            <p><?php if ($_smarty_tpl->tpl_vars['pastBanner']->value['details']['is_delivery'] == 0) {?> PICK-UP <?php } else { ?> DELIVERY <?php }?> - <?php echo $_smarty_tpl->tpl_vars['pastBanner']->value['date'];?>
</p>
            <?php if ($_smarty_tpl->tpl_vars['pastBanner']->value['details']['bread'] != '') {?>
            <span class="item_details" data-bread="<?php echo $_smarty_tpl->tpl_vars['pastBanner']->value['details']['bread'];?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['pastBanner']->value['details']['desc'];?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['pastBanner']->value['details']['desc_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['pastBanner']->value['details']['item_id'];?>
"></span>
            <?php }?>
            <ul>
            <?php
$__section_orderitms_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['pastBanner']->value['details']['items']) ? count($_loop) : max(0, (int) $_loop));
$__section_orderitms_0_total = $__section_orderitms_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_orderitms'] = new Smarty_Variable(array());
if ($__section_orderitms_0_total !== 0) {
for ($__section_orderitms_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_orderitms']->value['index'] = 0; $__section_orderitms_0_iteration <= $__section_orderitms_0_total; $__section_orderitms_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_orderitms']->value['index']++){
?>
           <li><?php echo $_smarty_tpl->tpl_vars['pastBanner']->value['details']['items'][(isset($_smarty_tpl->tpl_vars['__smarty_section_orderitms']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_orderitms']->value['index'] : null)];?>
</li> 
            <?php
}
}
?>
         
            </ul>
            
            <a class="reorder reorder-button link" data-order="<?php echo $_smarty_tpl->tpl_vars['order_id']->value;?>
" data-from="home_page" href="javascript:void(0);">REORDER</a>
            
            <a class="placeorder link" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu">PLACE NEW ORDER</a></div>
      </div>
     
     
    <?php } elseif ($_smarty_tpl->tpl_vars['bannercount']->value <= 1) {?>
    
      <!--<?php echo $_smarty_tpl->tpl_vars['single_banner']->value;?>
-->
     <div class="homebannerauto">
     <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/ellipsis.svg" class="homeAnimationLoaderImg" alt="">
        <span class="homepageAutoBanner">
                
        <span class="sandwich-1">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/7.png"  style="display:block"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/8.png"  style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/9.png"  style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/10.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/11.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/12.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/13.png" style="display:none"  alt="">
        </span>
        
        <span class="sandwich-2">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/14.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/15.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/16.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/17.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/18.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/19.png" style="display:none"  alt="">
        </span>
        </span>
        <span class="banner-img-top_wrap">
          <img class="" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/banner-top.png" alt="banner-top">
        </span>
      </div>
        <div class="banner-captionAuto">    
      <h2>Sandwiches Your Way!</h2>
      <?php if ($_smarty_tpl->tpl_vars['uid']->value != '') {?>
        <p>Build your own sandwich in 5 easy steps using a <br>curated selection of the finest local ingredients!</p>
        <?php } else { ?>
        <p>Welcome to Barney Brown, a custom-made social-sandwiching experience. Get started by building your own sandwich in just 5 easy steps.</p>
        <?php }?>
      <a class="home-place-order" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/">Get Started</a>
          </div>
      
     
     <?php echo '<script'; ?>
 type="text/javascript">
     window.addEventListener('load',function(){ window.ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
     <?php echo '</script'; ?>
>
    
    
    <?php } else { ?>
    
    <div class="homebannerauto">
        <span class="homepageAutoBanner">
        </span>
    </div>  
        <div class="banner-captionAuto">    
      <h2>Sandwiches Your Way!</h2>
      <?php if ($_smarty_tpl->tpl_vars['uid']->value != '') {?>
        <p>Build your own sandwich in 5 easy steps using a <br>curated selection of the finest local ingredients!</p>
        <?php } else { ?>
        <p>Welcome to Barney Brown, a custom-made social-sandwiching experience. Get started by building your own sandwich in just 5 easy steps.</p>
        <?php }?>
      <a class="home-place-order" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/">Get Started</a>
      </div>
          
     
     <?php echo '<script'; ?>
 type="text/javascript">
     window.addEventListener('load',function(){ mobile_ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
     <?php echo '</script'; ?>
>
    
    
    
    
          
          <?php }?>
        
          </div>
</div>
  <!-- End SlidesJS Required: Start Slides -->
</div> 
</div> <!-- container Ends -->
</div><!-- Top control Ends -->
 <div class="two-column">
    <div class="two-column-inner">
      <div class="container">
          <?php if ($_smarty_tpl->tpl_vars['uid']->value) {?> 
         
         <div class="past-orders">
         
          <h2>Order History</h2>
           <?php if ($_smarty_tpl->tpl_vars['past_order_history']->value) {?>
          <ul>
         
             <?php
$__section_order_history_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['past_order_history']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_order_history_1_total = $__section_order_history_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_order_history'] = new Smarty_Variable(array());
if ($__section_order_history_1_total !== 0) {
for ($__section_order_history_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] = 0; $__section_order_history_1_iteration <= $__section_order_history_1_total; $__section_order_history_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']++){
?>
             
            <li>
              <p><span class="delivery"><?php if ($_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->delivery == 1) {?>Delivery <?php } else { ?> Pick-up <?php }?></span> 
              <span class="date-address"><?php if ($_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->delivery == 1) {
echo $_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->address1;
} else { ?> <?php echo $_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->store_name;?>
 <?php }?></span></span>
              <?php if ($_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->bread != '') {?>
              <span class="item_details_past" data-bread="<?php echo $_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->bread;?>
"  data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->desc;?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->desc_id;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->item_id;?>
"></span>
              <?php }?>
              <span class="price">$<?php echo $_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->total;?>
</span></p>
              <?php if ($_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->itemCheck > 0) {?>
              <a href="javascript:void(0);" data-order="<?php echo $_smarty_tpl->tpl_vars['past_order_history']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_order_history']->value['index'] : null)]->order_id;?>
" data-from="past_order_history" class="reorder-button link">reorder</a><?php }?> 
            </li>
            
             <?php
}
}
?>   
          </ul>
          <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
myaccount/index/?view=order" class="view-all">VIEW ALL</a> 
          <?php } else { ?>
            <p>You have not placed any orders yet. <br>  Get started now!</p>
            <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu" class="get-button link">GET STARTED</a>
          <?php }?>
          </div>
          
        
        

        <div class="saved-sandwich">
        
          <h2>Saved Sandwiches  </h2>
           <?php if ($_smarty_tpl->tpl_vars['saved_sandwiches']->value) {?>
          <ul>
         


<?php if ($_SESSION['uid'] == 560) {?>
 
<?php }?>


         <?php
$__section_sandwichs_2_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['saved_sandwiches']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwichs_2_total = $__section_sandwichs_2_loop;
$_smarty_tpl->tpl_vars['__smarty_section_sandwichs'] = new Smarty_Variable(array());
if ($__section_sandwichs_2_total !== 0) {
for ($__section_sandwichs_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index'] = 0; $__section_sandwichs_2_iteration <= $__section_sandwichs_2_total; $__section_sandwichs_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index']++){
?>

            <li>
              <p><span class="save-sandwich-text"><?php echo $_smarty_tpl->tpl_vars['saved_sandwiches']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index'] : null)]->sandwich_name;?>
</span><span class="price">$<?php echo $_smarty_tpl->tpl_vars['saved_sandwiches']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index'] : null)]->current_price;?>
</span></p>
                <a  class="link common_add_item_cart addtocart-button" data-sandwich="user_sandwich" data-sandwich_id="<?php echo $_smarty_tpl->tpl_vars['saved_sandwiches']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index'] : null)]->id;?>
" data-uid="<?php echo $_SESSION['uid'];?>
" data-bread="<?php echo $_smarty_tpl->tpl_vars['saved_sandwiches']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index'] : null)]->bread;?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['saved_sandwiches']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index'] : null)]->sandwich_desc;?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['saved_sandwiches']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index'] : null)]->sandwich_desc_id;?>
"  data-id="<?php echo $_smarty_tpl->tpl_vars['saved_sandwiches']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwichs']->value['index'] : null)]->id;?>
"  href="javascript:void(0);" >ADD TO CART </a></li>
            <li>
         <?php
}
}
?>   
             
          </ul>
          <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/savedSandwiches" class="view-all">VIEW ALL</a> 
          <?php } else { ?>
            <p>You currently have no saved sandwiches. <br> Create one now!</p>
            <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/" class="get-button link">GET STARTED</a>

          <?php }?>
          </div> 
          
        <?php } else { ?>
        

        
      <ul class="sandwich-main-link-box">
          <li>
            <h1>SANDWICH MENU</h1>
            <h2>A Menu of Endless Possibilities</h2>
            <ul class="create-sandwich">
              <li><a href="javascript:void(0);">- SANDWICH CUSTOMIZER</a></li>
              <li><a href="javascript:void(0);">- SAVED SANDWICHES</a></li>
              <li><a href="javascript:void(0);">- FEATURED SANDWICHES</a></li>
              <li><a href="javascript:void(0);">- SANDWICH GALLERY</a></li>
              <li><a href="javascript:void(0);">- FRIENDS’ SANDWICHES</a></li>
            </ul>
            <a class="get-button link" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu">VIEW THE MENU</a>
          </li>
          <li>
            <h1>FEATURED SANDWICHES</h1>
            <h2>Handcrafted by Our Community</h2>
            <ul class="create-sandwich">
              <li><a href="javascript:void(0);">- NEW LINEUP EVERY MONTH</a></li>
              <li><a href="javascript:void(0);">- CREATED BY OUR COMMUNITY</a></li>
              <li><a href="javascript:void(0);">- CHOSEN FROM THOUSANDS</a></li>
              <li><a href="javascript:void(0);">- SAVE THE ONES YOU LIKE!</a></li>
            </ul>
            <a class="view-collection link" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/featuredSandwiches/">VIEW THE MENU</a> </li>
            <li>
            <h1>CATERING</h1>
            <h2>Delivery Anywhere in Manhattan</h2>
             <ul class="create-sandwich">
              <li><a href="javascript:void(0);">- BAGGED LUNCHES &amp; PLATTERS</a></li>
              <li><a href="javascript:void(0);">- CORPORATE EVENTS</a></li>
              <li><a href="javascript:void(0);">- PERSONAL OCCASIONS</a></li>
              <li><a href="javascript:void(0);">- HOLIDAYS</a></li>
              <li><a href="javascript:void(0);">- SPORTING EVENTS</a></li>
            </ul>
            <a class="view-menu link" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
catering">VIEW THE MENU</a> 
           </li>
      </ul> 
        
        
        <?php }?> 

      </div>
    </div>
  </div>
 
<?php }
}
