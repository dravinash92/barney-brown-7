<?php
/* Smarty version 3.1.39, created on 2021-03-25 16:27:22
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\create_sandwich.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c6c926190d2_63654860',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5281c16d20435f84f3c8631dfdfcd9ad55901981' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\create_sandwich.tpl',
      1 => 1582633614,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c6c926190d2_63654860 (Smarty_Internal_Template $_smarty_tpl) {
?>      <!--Header Ending-->
      <!--Create Sandwich Ending-->
      
     <div class="appendImages">  
     <!--  <?php echo $_smarty_tpl->tpl_vars['SANDWICH_IMAGES']->value;?>
  -->
      </div>
      
      <div class="menu-list-wrapper">
        <h1>CREATE A SANDWICH</h1>
        <a class="clear-all-button" href="javascript:void(0)">Clear All</a>
        
        <div id="ovrlayx" style="position:absolute; display:none; width:960px; height:500px; z-index:1000">

          <div style="position:relative" class="create-sandwich-wrapper">
          <div class="create-sandwich-left">
            <div class="finalize-It-heading">
              <h3>Finalize It!</h3>
            </div>
            <div class="name-your-creation-popup">
              <h2>NAME YOUR CREATION</h2>
             <h3><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['SANDWICH_NAME']->value;?>
" id="namecreation"></h3>
            </div>
            
            <div class="button-holder" style="margin-top:383px">
              <ul>
                <li> <a href="javaScript:void(0)"  class="edit-sandwich">EDIT SANDWICH</a></li>
              </ul>
            </div>
          </div>
          <div class="create-sandwich-right-wrapper">
            <div class="create-sandwich-right fill-color">
              <div class="final-list-wrapper scroll-bar " style="position: relative; overflow: visible;">
               
                <ul id="final_out">
                  
                </ul>
               </div>
            </div>
            <div class="checkbox-holder-final">
              <input type="checkbox" class="menucheck" value="ov_check1" name="check" id="ov_check1">
              <label for="ov_check1" class="toastyum"> Toast It! </label>
            </div>
            <div class="save-button-share">
              <div class="checkbox-holder-final private_check_final">
                <!-- <input type="checkbox" value="ov_check2" name="check"  checked="checked"  id="ov_check2"> -->
                <input type="checkbox" value="ov_check2" name="check" id="ov_check2">
                <label for="ov_check2">Make Private</label>
                <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
images/sandwich_menu/lock.png">
              </div>
              
            <!-- <?php echo $_smarty_tpl->tpl_vars['sandwichId']->value;?>
 --> 
               <!-- <a class="share-sandwich share_to_fb" rel="" href="#">Share</a> <img  class="sharefbloader" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/share_loader.gif" />  -->
			   <a class="save-to-my-menu new-save-to-my-menu" href="javascript:void(0);">SAVE</a> </div>
            <div class="bottom-buttons">
              <h4 id="totalPrice">$0.00</h4>
              <a class="am-done"  href="javaScript:void(0)">ADD TO CART</a> 
            
              
             </div>
              <span class="addtocartDesc">By clicking "Add to Cart", sandwich will automatically save to your menu.</span> 
          </div>
        </div>
    
  </div>
        
        
        
        
        
        <div class="create-sandwich-wrapper">
          <div class="create-sandwich-left">
            <div class="create-sandwich-menu" <?php if ($_smarty_tpl->tpl_vars['end_screen']->value == "true") {?> style="display:none";  <?php }?> >
              <ul>
                <li><a href="javaScript:void(0)"  class="active">BREAD</a></li>
                <li><a href="javaScript:void(0)" >PROTEIN</a></li>
                <li><a href="javaScript:void(0)" >CHEESE</a></li>
                <li><a href="javaScript:void(0)" >TOPPINGS</a></li>
                <li><a href="javaScript:void(0)" >CONDIMENTS</a></li>
              </ul>
            </div>
           
            
            <?php if (!$_SESSION['temp_sandwich_data']) {?> 
            <?php echo $_smarty_tpl->tpl_vars['LANDING_PAGE']->value;?>

            <?php }?>
            
            <div class="bread_images">
            
            <span class="bread_image">
            <div class="image-holder"> <img style="display:none" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/create-sandwich-bread.png" alt="Instagram"> </div>
            </span>
            
            <span class="protein_image">
            </span>
            
            <span class="cheese_image">
            </span>
            
            <span class="topping_image">
            </span>

            <span class="condiments_image">
            </span>
            
            </div>

          </div>
          <div class="create-sandwich-right-wrapper" <?php if ($_smarty_tpl->tpl_vars['end_screen']->value == "true") {?> style="display:none";  <?php }?> >
           <div class="optionLoader"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/optionload.gif" /></div>   
            <div class="create-sandwich-right" id="optionList">     
                   
             <?php echo $_smarty_tpl->tpl_vars['OPTN_DATA']->value;?>

            </div>
            <div class="bottom-buttons">
              <h4 class="price">$<?php echo $_smarty_tpl->tpl_vars['BASE_FARE']->value;?>
.00</h4>
              <!-- <a href="javaScript:void(0)" id="finished" class="am-done link">I'M DONE</a> -->  
			  <p class="prmicoTxt"><span class="premium-icon" style="padding-bottom: 4px;">&nbsp;</span> Premium</p>
			       <div class="button-holder" style="display:none">
                    <ul>
                    <li><a href="javaScript:void(0)" class="back">BACK</a></li>
                    <li><a href="javaScript:void(0)" class="next">NEXT</a></li>
                    </ul>
                   </div>  
			</div>
          </div>
        </div>
      </div>
      <!--Create Sandwich Ending-->
    </div>
  </div>
  

<?php echo '<script'; ?>
 type="text/javascript">
$(document).ready(function(){  
	//ha.sandwichcreator.preload_sandwich_images();	
	$('#namecreation').focus();
});

 
<?php echo '</script'; ?>
>

<?php }
}
