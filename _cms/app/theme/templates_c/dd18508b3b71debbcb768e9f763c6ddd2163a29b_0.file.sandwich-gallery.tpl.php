<?php
/* Smarty version 3.1.39, created on 2021-03-25 17:15:24
  from 'C:\wamp64\www\hashbury\_cms\app\theme\templates\sandwich-gallery.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c77d43daf61_68775208',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dd18508b3b71debbcb768e9f763c6ddd2163a29b' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_cms\\app\\theme\\templates\\sandwich-gallery.tpl',
      1 => 1616672704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c77d43daf61_68775208 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_cms\\app\\smarty\\libs\\plugins\\modifier.replace.php','function'=>'smarty_modifier_replace',),));
?>

      <!--Menu Ending-->
      <div class="menu-list-wrapper">
        <h1>Sandwich Gallery
        <?php if ($_smarty_tpl->tpl_vars['gal_count']->value > 0) {?> <span class="sandwitchitemCount" data-count="<?php echo $_smarty_tpl->tpl_vars['totalCount']->value;?>
"> <?php echo $_smarty_tpl->tpl_vars['totalCount']->value;?>
 Creations</span> <?php }?>
        </h1>
        <div class="sandwich-gallery-wrapper">
          <div class="sandwich-gallery-left">
            <p>Search</p>
            <form action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/gallery" id="sandwich-gallery-search" method="post">
              <div class="selectParent">
                <input class="shared-search" type="text" name="search" value="<?php echo $_smarty_tpl->tpl_vars['serch_term']->value;?>
">
              </div>
              <button type="submit" class="selectParent searchCustom">SEARCH </button>
            </form>
            <input type="hidden" id="searchTerm" value="<?php echo $_smarty_tpl->tpl_vars['serch_term']->value;?>
">
            <!-- <div class="selectParent searchCustom">
             SEARCH
            </div> -->
            
            
            
            <div class="bread-protein-list">
              
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category', false, 'i');
$_smarty_tpl->tpl_vars['category']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->do_else = false;
?>
	            
	            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['category']->value, 'data', false, 'k');
$_smarty_tpl->tpl_vars['data']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->do_else = false;
?>
	              <?php if ($_smarty_tpl->tpl_vars['data']->value->category_identifier) {?>
	                <h3>  <?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
 </h3>
	                   <ul>	  
	             
	             <?php if ($_smarty_tpl->tpl_vars['data']->value->category_name != "bread") {?>
	              <li>
                  <input type="checkbox"  value="null" name="check" id="check_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
_00">
                  <label for="check_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
_00">No <?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
</label>
                  </li>   
                 <?php }?>    
                 
		            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryItems']->value, 'categoryItem', false, 'j');
$_smarty_tpl->tpl_vars['categoryItem']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['categoryItem']->value) {
$_smarty_tpl->tpl_vars['categoryItem']->do_else = false;
?>
		            
		            	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryItem']->value, 'dataitems', false, 'j');
$_smarty_tpl->tpl_vars['dataitems']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['dataitems']->value) {
$_smarty_tpl->tpl_vars['dataitems']->do_else = false;
?>
                     <?php if ($_smarty_tpl->tpl_vars['data']->value->id == $_smarty_tpl->tpl_vars['dataitems']->value->category_id) {?>
                <li>
                  <input type="checkbox"  value="check1" name="check" id="check_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
">
                  <label for="check_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['dataitems']->value->item_name;?>
</label>
                </li>
                <?php }?>
              
                  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	                 
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </ul>
              <?php }?>  
               <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	                 
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
          </div>
          <div class="sandwich-gallery-right">
          <input name="total_item_count" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['total_item_count']->value;?>
" />
          <input name="sort_type" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['sort_id']->value;?>
" />
            <ul class="menu-listing">
              <?php
$__section_sandwitch_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['GALLARY_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_0_start = min(0, $__section_sandwitch_0_loop);
$__section_sandwitch_0_total = min(($__section_sandwitch_0_loop - $__section_sandwitch_0_start), $__section_sandwitch_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_0_total !== 0) {
for ($__section_sandwitch_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_0_start; $__section_sandwitch_0_iteration <= $__section_sandwitch_0_total; $__section_sandwitch_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>
             
              <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0]);?>
              <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>
              
              <?php $_smarty_tpl->_assignInScope('bread_name', smarty_modifier_replace($_smarty_tpl->tpl_vars['bread_name']->value,' ','##'));?>
              
              <?php 
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              ?>
      
      
              
                   
                   
                   <?php if (is_array($_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id'])) {?> 
                   <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id']));?>
                   <?php } else { ?>
                   <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
                   <?php }?>
              
              <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['by_admin'] == 1) {?>
              <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['ADMIN_URL']->value);?>
              <?php } else { ?>
              <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>
              <?php }?>
              
              <?php $_smarty_tpl->_assignInScope('sname', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name']);?>
               <?php $_smarty_tpl->_assignInScope('tname', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name_trimmed']);?>
                <li data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc_id'];?>
" data-flag="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['flag'];?>
" data-menuadds="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_add_count'];?>
" data-userid ="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
" <?php if ($_smarty_tpl->tpl_vars['items_id']->value == 1) {?> rel="ADD TO CART" <?php } else { ?> rel="ADD TO CART" <?php }?> data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-toast="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'];?>
" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-likeid="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_id'];?>
" data-likecount="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_count'];?>
"> 
                <span class="inner-holder" style="position: relative">
                <input type="hidden" name="chkmenuactice<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active'];?>
" />
                <img data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" width="124" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/thumbnails/sandwich_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png"   alt="sandwitchimageview">
                <input type="hidden" name="image" id="sandImg_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png">

                <?php if (in_array($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['saved_data']->value)) {?>
                               <input type="hidden" name="saved_tgl<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="1">
                <?php }?>
                <h3 title="<?php echo $_smarty_tpl->tpl_vars['sname']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['tname']->value;?>
</h3>

                <a href="javascript:void(0);" class="link">VIEW</a>       </span> 
                <input class="typeSandwich" type="hidden" value="FS">
                </li>
                <input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $_SESSION['uid'];?>
" >      
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['saved_data']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
                                  <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'] == $_smarty_tpl->tpl_vars['v']->value) {?>
                                    <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['is_public_array']->value[$_smarty_tpl->tpl_vars['v']->value];?>
" id="isPublic<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
"> 
                                      
                                  <?php }?>
                                   
                              <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>         

              
                <?php
}
}
?>                
                
            </ul>
         
            <div class="loadMoreGallery" style="text-align:center"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/star.png" alt="Loading" /> </div>
            

			
          </div>
          
          
          
        </div>
      </div>
      <!--Menu Ending-->
    </div>
  </div>
  <!--Popup Start-->



<!--If user not logged in.-->

<?php }
}
