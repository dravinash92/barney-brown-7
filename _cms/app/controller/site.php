<?php

class Site extends Base
{
	public $model;
	
	public function __construct(){
		$this->model = $this->load_model('cmsModel');
		$this->sitemodel = $this->load_model('siteModel');
	}
		
	function index(){
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
	}
	
	function location(){
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('content',"cms-location.tpl");
	}
	
	function catering(){
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="CATERING";
		$model = $this->model;
		$webpageCateringData =  $model->get_webpagesdata_data(API_URL,$data);		
		$this->smarty->assign('webpagedata',$webpageCateringData);	
		$this->smarty->assign('content',"cms-catering.tpl");
	}
	
	function aboutus(){
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="ABOUT US";
		$model = $this->model;
		$webpageAboutUsData =  $model->get_webpagesdata_data(API_URL,$data);			
		$this->smarty->assign('webpagedata',$webpageAboutUsData);	
		$this->smarty->assign('content',"cms-about-us.tpl");
	}
	
	function jobs(){
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);		
		$data['webpage_name'] ="JOBS";
		$model = $this->model;
		$webpageCateringData =  $model->get_webpagesdata_data(API_URL,$data);
		$this->smarty->assign('webpagedata',$webpageCateringData);		
		$this->smarty->assign('content',"cms-jobs.tpl");
	}
	
	function feedback(){
	
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="FEEDBACK";
		$model = $this->model;
		$webpageCateringData =  $model->get_webpagesdata_data(API_URL,$data);
		$this->smarty->assign('webpagedata',$webpageCateringData);
		$this->smarty->assign('content',"cms-feedback.tpl");
	}
	
	function contact(){
	
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);		
		$data['webpage_name'] ="CONTACT";
		$model = $this->model;
		$webpageCateringData =  $model->get_webpagesdata_data(API_URL,$data);
		$this->smarty->assign('webpagedata',$webpageCateringData);		
		$this->smarty->assign('content',"cms-contact.tpl");
	
	}
	
	function termsofuse(){
    
        $this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="TERMS OF USE";
		$model = $this->model;
		$webpageCateringData =  $model->get_webpagesdata_data(API_URL,$data);
		$this->smarty->assign('webpagedata',$webpageCateringData);
        $this->smarty->assign('content',"cms-terms-of-use.tpl");
    }
    
    function privacypolicy(){
    
        $this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="PRIVACY POLICY";
		$model = $this->model;
		$webpageCateringData =  $model->get_webpagesdata_data(API_URL,$data);
		$this->smarty->assign('webpagedata',$webpageCateringData);
        $this->smarty->assign('content',"cms-privacy-policy.tpl");
    
    }
    
    public function resetPassword(){
		
		$model = $this->model;
		$empData = new stdClass();
		if(isset($_GET['token']) AND isset($_GET['tokenid'])){
		$_POST =  $_GET;
		$response = $this->sitemodel->isTokenValid($_POST);
		if($response && $response > 0){
			$this->smarty->assign('valid_token',true);  
			 
		} else {
			$this->smarty->assign('valid_token',false);
		}
		$empData->tokenid  = $_GET['tokenid']; 
		$empData->token  = $_GET['token'];
		$data = $empData;
		}else{
			$data  = $empData;
		}
		
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);	
		$this->smarty->assign('data',$data);	
		$this->smarty->assign('content',"reset_password.tpl");
		
	}
	
	public function setPassword(){
		$model = $this->sitemodel;
		
		if(empty($_POST['password'])){
			echo '{"status":"failed","message":"Please fill the new password"}';
		}
		if($_POST['c_password'] != $_POST['password']){
			
			echo '{"status":"failed","message":"Confirm password not matched"}';
		} else if(strlen($_POST['password'])<6) {
			
			echo '{"status":"failed","message":"At least six character required"}';
			
		} else  {
		 
			$response = $model->setPassword($_POST);
			
			if($response && $response > 0){
				   echo '{"status":"success","message":"Success"}';  
				 
			} else {
				echo '{"status":"failed","message":"Token invalid!"}';
			}
		}
		
		exit;
	}
}