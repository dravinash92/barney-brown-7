<?php
//ini_set('display_errors',2);
ini_set('max_execution_time', 300);
class Home extends Base
{
	public $model;
	
	public function __construct(){
		
		$this->model = $this->load_model('homeModel');
	
	}
	
	function index(){
		$this->title = TITLE;
		$this->meta_desc='A New York City sandwich shop that does it just the way you want it. Barney Brown specializes in custom sandwich delivery in Manhattan 7 days a week from 10:00AM - 4:00PM.';
		$this->meta_kw='Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand.';
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('ADMIN_URL',ADMIN_URL);
		
		$data = "";
		$model = $this->model;
		$webpagedata =  $model->get_webpages_homepage_data(API_URL,$data);
		

		$uid = "";
		$single_banner = $this->smarty->fetch("single_banner.tpl");
		$uid  = @$_SESSION['uid']?@$_SESSION['uid']:@$_COOKIE['user_id'];
	    if(isset($uid))
		{
			$saved_sandwiches=$model->get_saved_sandwiches($uid,3);
			$this->smarty->assign('saved_sandwiches',$saved_sandwiches->Data);

			$past_order_history=$model->get_past_order_history($uid,3);
			
			$this->smarty->assign('past_order_history',$past_order_history->Data);
		}
		// echo "<pre>";print_r($past_order_history->Data);exit;
		$bannercount = count($webpagedata);
					
		$this->smarty->assign('uid',$uid);
		$this->smarty->assign('webpagedata',$webpagedata);
		$this->smarty->assign('bannercount',$bannercount);
		
		if($bannercount==1){
			
			$single_banner = $this->smarty->fetch("single_banner_dyn.tpl");					
		}
		
		//Now Enable the static banner
		 		
		
		$this->smarty->assign('single_banner',$single_banner);
		
		$order_detail = $this->pastOrders();
		$uid = @$_SESSION['uid']?@$_SESSION['uid']:@$_COOKIE['user_id'];

		//echo "<pre>";print_r($order_detail);exit;
		
		if(isset($order_detail->order_id)){
			$this->smarty->assign('noslide',1); 

			$dataOut = $this->model->prepOrderDetail($order_detail,$uid);
			//echo "<pre>";print_r($dataOut);exit;
			
			$this->smarty->assign('pastBanner',$dataOut);
			//if( !isset($dataOut['details']['image']) ) $this->smarty->assign('noslide',0);
			$this->smarty->assign('order_id',$order_detail->order_id);
			//$this->smarty->assign('itemsAvailable',$order_detail->itemCheck->items);
		}
		
		$this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
		$this->smarty->assign('content',"home.tpl");
	}
	
	
	function OrderSandwichBannerImage(){
		$uid = @$_SESSION['uid']?@$_SESSION['uid']:@$_COOKIE['user_id'];
		$out = $this->model->OrderSandwichBannerImage($uid);
		if($out) return $out->Data;
	}
	
	function sessions(){
		echo "<pre>";
		print_r($_SESSION);
		exit;
	}
	
	function pastOrders(){
		$uid = @$_SESSION['uid']?@$_SESSION['uid']:@$_COOKIE['user_id'];
		$out = $this->model->pastOrders($uid);
		if($out) return $out->Data;
	}
	
	function loggedin()
	{
		$this->title = TITLE;
	
		$this->smarty->assign('SITE_URL',SITE_URL);
	
		$this->smarty->assign('LOGGED_IN',true);
	
		$data ="";
		$model = $this->model;
		$webpagedata =  $model->get_webpages_homepage_data(API_URL,$data);
		
		print_r($webpagedata);exit;
		$this->smarty->assign('content',"home.tpl");	
	}
	
	function slider(){
	
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('content',"slider.tpl");
	
	}
	
	// For testing purpose only. (Displays date and time on the server).
	function showServerTime(){
	
		echo "date: "; echo date('M-d-Y');
		echo "<br>";
		echo  "time: "; echo date('h:i:s A');
		exit;
	
	}
	
	function app(){
		
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->display('app-page.tpl');
		exit;
	}

	function renameItemName()
    {
        $query1  = "SELECT `id`,`sandwich_data` FROM `user_sandwich_data` WHERE `sandwich_items` LIKE '%Salami%'";

        $run_query1 = $this->db->Query($query1);
        $sandwich = $this->db->FetchAllArray($run_query1);
        
        foreach ($sandwich as $skey => $svalue) 
        {
            $sandwich_itemdata = json_decode($svalue['sandwich_data'], true); 
            
            if(!empty($sandwich_itemdata['PROTEIN']['item_name']))
            {
                foreach ($sandwich_itemdata['PROTEIN']['item_name'] as $key => $value) 
                {
                    if ( $value == "Salami") 
                    {
                        $sandwich_itemdata['PROTEIN']['item_name'][$key] = str_replace("Salami", "Genoa Salami", $value);
                    } 
                }

                foreach ($sandwich_itemdata['PROTEIN']['item_qty'] as $key => $value) 
                {
                	if ( $key == "Salami")
                	{
                		$sandwich_itemdata['PROTEIN']['item_qty']['Genoa Salami'] = $sandwich_itemdata['PROTEIN']['item_qty'][$key];
                		unset($sandwich_itemdata['PROTEIN']['item_qty'][$key]);
                		 
                	}
                }
               
            }
            $sandwich_data = json_encode($sandwich_itemdata,true);
            $query  = "UPDATE `user_sandwich_data` SET sandwich_data = '$sandwich_data' WHERE id = '".$svalue['id']."' ";
            $result = $this->db->Query($query);
        }
        echo "Success"; exit;
    }

    function renameSandwichItems()
    {
    	$query1  = "SELECT `id`,`sandwich_items` FROM `user_sandwich_data` WHERE `sandwich_items` LIKE '%Salami%'";

        $run_query1 = $this->db->Query($query1);
        $sandwich = $this->db->FetchAllArray($run_query1);
        foreach ($sandwich as $skey => $svalue) 
        {
             $newItems = explode(',', $svalue['sandwich_items']);
             foreach ($newItems as $key => $value) 
             {
             	if($value == "Salami")
             	{
             		$newItems[$key] = "Genoa Salami";
             	}
             }
            $sandwich_items = implode(',', $newItems);
            $query  = "UPDATE `user_sandwich_data` SET sandwich_items = '$sandwich_items' WHERE id = '".$svalue['id']."' ";
            $result = $this->db->Query($query);
        }
        echo "Success"; exit;
    }

    public function getTransactionLog()
    {
    	$result = array();
    	$res = $this->model->getTransactionLog();

    	foreach ($res as $key => $value) 
    	{
    		$value = json_decode(json_encode($value),true);
    		
    		$resl = json_decode($value['log'],true);
    		echo "<pre>";print_r($resl);
    		
    	}exit;
    }

    public function orderItemHistory()
    {
    	$res = $this->model->orderItemHistory($_SESSION['uid']);
		echo "<pre>";print_r($res);exit;
    }
 
	
}