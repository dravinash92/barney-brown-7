<?php

class facebook extends Base
{
	
	
	public $model;
	
	public function __construct(){
	
		$this->model = $this->load_model('sandwichModel');
	
	}
	
	
	public function mailtest(){
		
		
		unset($_SESSION['access_token']);
		$FB = new FB_API();
		$FB->secret     = 'ce730ac5747a39a2f100cefa30213f21';
		$FB->appid      = '130094774050876';
		$FB->redirURI   = 'http://www.barneybrown.com/facebook/mailtest/';
		$FB->FB_check_login();
		$url =  $FB->FB_get_url();
		echo  '<a href="'.$url.'">Test login with FB</a>';
		
		 
			
        if(isset($_SESSION['access_token']) && $_SESSION['access_token']){ 
		
        echo "<br><br>";	
        	
        echo "<b>Access Token:</b>";
        echo "<br>";
        echo "<textarea style='width:500px;height:180px;' >".$_SESSION['access_token']."</textarea>";	
        
        echo "<br><br>";
        echo "<b>Response:</b>";
        
        echo "<br>";
        
		echo "<pre>";
				$user_data = file('https://graph.facebook.com/me?fields=first_name,last_name,email,id,friends&access_token='.$_SESSION['access_token']);
		var_export(json_decode($user_data[0]));
		
		 
		unset($_SESSION['access_token']); 
        }
		exit;
			 
	}
	
	
	public function data(){
		
		if(isset($_GET['state']) && $_GET['state'] ) $_SESSION['FBRLH_state']  = $_GET['state'];
		$state =  $this->FB_check_login();
		 
		if(isset($_SESSION['access_token'])){
		$user_data     = $this->FB_get_user($_SESSION['access_token']);
		$_SESSION['fbnoaccess'] =  $this->input_users($user_data);
		}
		
		$log_string = 	"STATE - $state \n".
						"USER DATA: " . $user_data . "\n".
						"GET: \n" . var_export( $_GET, true ) . " \n\n".
						"SESS: \n" . var_export( $_SESSION, true ) . " \n".
						"###\n\n";

		file_put_contents('app/log/fb_data.log', $log_string,  FILE_APPEND );
		 
		// DM Changed this whole mess to fix the redirection problem on fb half login. 
		if($state)
		{ 
			
    		if(  isset( $_COOKIE['redirect'] ) && $_COOKIE['redirect'] != '' && $_COOKIE['redirect'] != '0' )
			{
				$redirLoc = $_COOKIE['redirect'];
				echo "<div align='center'>"; echo "Login Success"; echo "</div>";  
				echo '<script type="text/javascript">'; 
	    		echo 'window.opener.location.href = "'.$redirLoc.'";';
	    	//				echo 'window.opener.location.reload();'; 
	    	}
	    	else
	        {
	    			echo "<div align='center'>"; echo "Login Success"; echo "</div>";  
					echo '<script type="text/javascript">'; 
	    			echo 'window.opener.location.reload();'; 
	   		}
	   		echo 'window.close(window.top);';
			echo '</script>';  
	   	}
	   	else
	   	{

		   	echo '<script type="text/javascript">';
	    	echo 'window.opener.location.reload();'; 
		    echo 'window.close(window.top);';
			echo '</script>';  
		}
		exit;
	}
	

   
	
	
	public function input_users($user_data){
	 	$result         = true;
	 	$uid            = $user_data['uid'];
	 	$fname          = $user_data['first_name'];
	 	$lname          = $user_data['last_name'];
	 	$email          = $user_data['email'];
	 	
	 	if(!$email){
	 		echo '<script type="text/javascript">';
	 		echo 'window.opener.alert("Please allow access Email privileges from your facebook Auth login.");';
	 		echo 'window.close(window.top);';
	 		echo '</script>';
	 		exit;
	 	}
	 	
	 	$token          = $_SESSION['access_token'];
	 	$friend_data    = $user_data['friend_data'];
	 	if(isset($friend_data['name'])) $count_friend  = count($friend_data['name']);
	 	else  $count_friend  = 0;
	 	$num            = $this->db->Query("SELECT username,first_name,last_name,uid,fb_id,fb_auth_token FROM users WHERE username = '$email' OR  fb_id = '$uid'");
	 	$count          = $this->db->Rows();
	 	$data           = $this->db->FetchArray($num); 
	 	$_SESSION['fb_id'] = $uid;
	 	
	 	$cookietime = time()+60*60*24*60;
	 	if($count == 0 && !@$_SESSION['uid']){ 
	 		$result_usr = $this->db->Query(" INSERT INTO users (username,fb_id,fb_auth_token,first_name,last_name) VALUES ('$email','$uid','$token','$fname','$lname') ");
	 		$insid      = $this->db->insertId();
	 		$_SESSION['fb_id'] = $uid;
	 		$_SESSION['uid']   = $insid;
	 		$_SESSION['uname'] = $fname;
	 		$_SESSION['lname'] = $lname;
	 		$uidz              = $insid;
	 		setcookie("user_mail",$email,$cookietime,'/',NULL);
	 		setcookie("user_fname",$fname,$cookietime,'/',NULL);
	 		setcookie("user_lname",$lname,$cookietime,'/',NULL);
	 		setcookie("user_id",$uidz,$cookietime,'/',NULL);
	 		setcookie("facebookOnlyUser",true,$cookietime,'/',NULL);
	 	}
	 	else if($count && !@$_SESSION['uid']) {   
	 	 if((isset($_COOKIE['user_id']) && $_COOKIE['user_id'] )  && $_COOKIE['user_id'] != $data['uid']){
	 	 	$result         = false;   
	 		} else if(!@$_COOKIE['user_id'] || !@$_SESSION['uid']){
	 			$_SESSION['fb_id'] = $data['fb_id'];
	 			$_SESSION['uid']   = $data['uid'];
	 			$_SESSION['uname'] = $data['first_name'];
	 			$_SESSION['lname'] = $data['last_name'];
	 			setcookie("user_mail",$data['username'],$cookietime,'/',NULL);
	 			setcookie("user_fname",$data['first_name'],$cookietime,'/',NULL);
	 			setcookie("user_lname",$data['last_name'],$cookietime,'/',NULL);
	 			setcookie("user_id",$data['uid'],$cookietime,'/',NULL);
	 			$fbs = $this->model->isFbOnlyUser($data['uid']);
	 			 
	 			if($fbs->Data->count > 0){
	 				setcookie("facebookOnlyUser",true,$cookietime,'/',NULL);
	 			}
	 		}
	 		$uidz              = $data['uid'];
	 		if( $data['fb_auth_token'] != $token ){ $this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' "); }
	 	  
	 	} else if(!$count && $_SESSION['uid']){  
	 		$uidz              = $_SESSION['uid'];
	 		$fbs = $this->model->isFbOnlyUser($uidz);
	 		if($fbs->Data->count > 0){
	 			setcookie("facebookOnlyUser",true,$cookietime,'/',NULL);
	 		}
	 		
	 		if( $data['fb_auth_token'] != $token ){ $this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' "); }
	 	 
	 	} else if($count && $_SESSION['uid']){
	 		
	 		 
	 		
	 		$uidz              = $data['uid'];
	 		if($_SESSION['uid'] != $data['uid']){
	 			$result         = false;
	 		}
	 		
	 		$uidz              = $_SESSION['uid'];
	 		$fbs = $this->model->isFbOnlyUser($uidz);
	 		if($fbs->Data->count > 0){
	 			setcookie("facebookOnlyUser",true,$cookietime,'/',NULL);
	 		}
	 		
	 		if( $data['fb_auth_token'] != $token ){ $this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' "); }
	 	
	 	 
	 	}
	 	
	 	
	 	for($i = 0; $i< $count_friend; $i++ ){
	 		$frnd_data = $this->db->Query("SELECT friend_uid FROM friend_data WHERE friend_uid = '".$friend_data['id'][$i]."' AND parent_uid = '$uidz' ");
	 		$count     = $this->db->Rows();
	 		if($count == 0){
	 			$this->db->Query("INSERT INTO friend_data (friend_name,friend_uid,parent_uid) VALUES ('".$friend_data['name'][$i]."','".$friend_data['id'][$i]."','$uidz') ");
	 		}
	 	}
	 	
	 	
	 	
		return $result;
	 }
	
	public function post_to_FB(){
		
		
		$sandwichData = $this->model->retriveMostSoldSandwich();
		 
 
           
	 	   $token             = @$_SESSION['access_token'];
	 	  
		
		   $Array             = array(

				'link'        => $sandwichData['link'],
				'message'     => $sandwichData['sandwich'].' sandwich '.$sandwichData['purchase'].' purchases',
				'caption'     => $sandwichData['sandwich'],
				'picture'     => $sandwichData['image'],
				'name'        => $sandwichData['sandwich'],
				'description' => $sandwichData['details'], 
				
		);
		   
		 
		
		$this->setSession();
	 	$post_data     = $this->FB_post_fb($token,$Array);
		if(isset($post_data) && $post_data) { $this->db->Query("INSERT INTO post_data (fb_post_id,fb_post_msg,fb_post_link) VALUES ('$post_data','$msg','$link') "); }
		
		 
		
	   }
     }
