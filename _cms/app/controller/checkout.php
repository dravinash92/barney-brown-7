<?php
class Checkout extends Base {

    public $model;

    public function __construct() {
        $this->model = $this->load_model('checkoutModel');
    }

    function index() {
        if (!@$_SESSION['uid']) {
            header('Location: ' . SITE_URL);
            exit;
        }

        if (isset($_SESSION['uid'])) {
            $this->title = TITLE;

            $this->smarty->assign('SITE_URL', SITE_URL);

            $this->smarty->assign('LOGGED_IN', true);

            $model = $this->model;
            $data = $model->get_user_checkout_data(@$_SESSION['uid']);
            $billing_data = $model->get_Billinginfo(array(
                'uid' => @$_SESSION['uid']
            ));
            $billing_data = $model->convert_to_Array($billing_data->Data);
            
            if(count($billing_data) > 0)
            {
                foreach ($billing_data as $key => $value) {
                  $billing_data[$key]->card_number = "XXXX-XXXX-XXXX-".substr($value->card_number, -4);
                }

            }
            $user["user_id"] = $_SESSION['uid'];
            $shopitems = $model->cartItems($user);
            $shopitems = $this->getCartItemList($shopitems);
            $this->smarty->assign('cartItemList', $shopitems);
            $this->smarty->assign('billingInfo', $billing_data);
            $this->smarty->assign('content', "checkout-choose-delivery-or-pickup.tpl");
        }
    }

    function getLastOrder() {
        $uid = @$_SESSION['uid'];
        $this->model->getLastOrder($uid);
        exit;
    }

    function choosedeliveryorpickup() {
        
        if (!@$_SESSION['uid']) {
            header('Location: ' . SITE_URL);
            exit;
        }
        if (isset($_SESSION['uid'])) {

            $this->title = TITLE;
            $this->smarty->assign('SITE_URL', SITE_URL);
            $this->smarty->assign('LOGGED_IN', true);
            $model = $this->model;
            $order_count = $model->get_order_count(@$_SESSION['uid']);
            $this->smarty->assign('order_count', $order_count->order_count);
            $data = $model->get_user_checkout_data(@$_SESSION['uid']);
            $billing_data = $model->get_Billinginfo(array(
                'uid' => @$_SESSION['uid']
            ));
            
            
            $billing_data = $model->convert_to_Array($billing_data->Data);
            
            if(count($billing_data) > 0)
            {
                foreach ($billing_data as $key => $value) {
                  $billing_data[$key]['card_number'] = "XXXX-XXXX-XXXX-".substr($billing_data[$key]['card_number'], -4);
                }
                
            }
            $user["user_id"] = $_SESSION['uid'];

            $shopitems = $model->cartItems($user);


            $shopitems = $this->getCartItemList($shopitems);
            $this->smarty->assign('cartItemList', $shopitems);
            $this->smarty->assign('billingInfo', $billing_data);

            // added for date
            $currenthour = date('H');
            $currentmin = date('i');
            if ($currentmin < 15)
                $currentmin = 0;
            else if ($currentmin >= 15 && $currentmin < 30)
                $currentmin = 15;
            else if ($currentmin >= 30 && $currentmin < 45)
                $currentmin = 30;
            else if ($currentmin >= 45 && $currentmin < 60)
                $currentmin = 45;
            $currentdate = $currenthour . ":" . $currentmin;

            $from = date('H:i:s', strtotime($currentdate));
            $todate = date('H:i:s', strtotime('10pm'));


            sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
            $lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $lower = $lower + 900;
            sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
            $upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $upper = $upper - 900;


            $times = "<select class='timedropdown'>";
            if (empty($format)) {
                $format = 'g:i a';
            }
            $step = 900;
            foreach (range($lower, $upper, $step) as $increment) {
                $increment = gmdate('H:i', $increment);

                list($hour, $minutes) = explode(':', $increment);
                $date = new DateTime($hour . ':' . $minutes);
                $date = $date->format($format);

                $times .= "<option value=" . $hour . ':' . $minutes . ">" . $date . "</option>";
            }
            $times .= "</select>";
            $this->smarty->assign('times', $times);
            $addresses = $model->getPickupStores(array("enabled" => 1));
            //echo "<pre>";print_r($addresses);exit;
            $this->smarty->assign('addresses', $addresses);
            $this->smarty->assign('currenthour', $currenthour);
            $this->smarty->assign('PICKUP_FIRST_ORDER', PICKUP_FIRST_ORDER);
            $this->smarty->assign('DELIVERY_FIRST_ORDER', DELIVERY_FIRST_ORDER);
            $this->smarty->assign('content', "checkout-choose-delivery-or-pickup.tpl");

//            $this->model->pp( $_COOKIE, true );
        }
    }

    function deliveryto() {
        $this->title = TITLE;
        $model = $this->model;
        if(  $_SESSION['uid'] )
            $_POST['uid'] = $_SESSION['uid'];
        
        $result = $model->editAddress($_POST);
        $address = $result[0];
        $this->smarty->assign('address', $address);
        $this->smarty->assign('SITE_URL', SITE_URL);
        $output = $this->smarty->fetch("checkout-delivery-to.tpl");
        echo $output;
        exit;
    }

    function pickupfrom() {
        $this->title = TITLE;
        $this->smarty->assign('SITE_URL', SITE_URL);
        $output = $this->smarty->fetch("checkout-pickup-from.tpl");
        echo $output;
        exit;
    }

    function addnewaddress() {
        $this->title = TITLE;
        $this->smarty->assign('SITE_URL', SITE_URL);
        $output = $this->smarty->fetch("checkout-add-new-address.tpl");
        echo $output;
        exit;
    }

    function addnewcreditcard() {
        $this->title = TITLE;
        $this->smarty->assign('SITE_URL', SITE_URL);
        $output = $this->smarty->fetch("checkout-add-new-credit-card.tpl");
        echo $output;
        exit;
    }

    function deliverychooseaddress() {
        $model = $this->model;
        $addresses = $model->listSavedAddress($_SESSION['uid']);
        $this->smarty->assign('addresses', $addresses);
        $this->title = TITLE;
        $this->smarty->assign('SITE_URL', SITE_URL);
        $output = $this->smarty->fetch("checkout-delivery-choose-address.tpl");
        print_r($output);
        exit;
    }

    function pickupstorelocation() {
        $this->title = TITLE;

        $model = $this->model;
        $addresses = $model->getPickupStores(array("enabled" => 1));
        $this->smarty->assign('addresses', $addresses);
        $this->smarty->assign('SITE_URL', SITE_URL);
        $output = $this->smarty->fetch("checkout-pickup-store-location.tpl");
        echo $output;
        exit;
    }

    // cms controller
    function place_order() {
        $model = $this->model;
        $model->place_order_api();
        unset($_SESSION['orders']);
        exit;
    }

    function pickupAddressInfo() {
        $this->title = TITLE;
        $model = $this->model;
        $result = $model->getPickupStoreAddress($_POST);
        $address = $result[0];
        $this->smarty->assign('address', $address);
        $this->smarty->assign('SITE_URL', SITE_URL);
        $output = $this->smarty->fetch("checkout-pickup-from.tpl");
        echo $output;
        exit;
    }

    function updateCartItems() {
        $this->title = TITLE;

        $response = new stdClass;
        if (!isset($_SESSION['uid'])) {
            $response->status = false;
            $response->message = "Session is expired. Please login again.";
            echo json_encode($response);
            exit;
        } else {
            $model = $this->model;
            $_POST['user_id'] = $_SESSION['uid'];
            $deliveryOrPickup = @$_POST['deliveryOrPickup'];
            $last_tip = @$_POST['last_tip'];
            $whichArrow = @$_POST['whichArrow'];
            $result = $model->updateItemToCart($_POST);
            $output = $this->getCartItemList($result, $deliveryOrPickup, $last_tip, $whichArrow);
            $response->status = true;
            $response->message = $output;
            echo json_encode($response);
            exit;
        }
    }

    function getCartItemQtyCount() {
        $model = $this->model;
        $_POST['user_id'] = $_SESSION['uid'];
        $result = $model->getCartItemQtyCount($_POST);

        if ($result->count) {
            $result = $result->count;
        } else {
            $result = 0;
        }
        print_r($result);
        exit;
    }
    
    function checkIfEmpty() {
        //make sure the user's items haven't been purged from the cart due to inactivity
        $model = $this->model;
        $_POST['user_id'] = $_SESSION['uid'];
        $result = $model->checkIfEmpty($_POST);
        
        if ($result->count) {
            $result = $result->count;
        } else {
            $result = 0;
        }
        print_r($result);
        exit;
    }

    function saveBillinginfo() {
        $model = $this->model;
        $checkout_items = $model->save_billing_info($_POST);
        exit;
    }

    function chekcvvValid() {
        $model = $this->model;
        $chk = $model->chekcvvValid($_POST);
        exit;
    }

    function get_billing() {
        $id = @$_POST['id'];
        $model = $this->model;
        $model->get_Billinginfo(array(
            'id' => $id,
            'uid' => @$_SESSION['uid']
                ), true);
        exit;
    }

    public function getCartItemList($result, $deliveryOrPickup = 1, $last_tip = 0, $whichArrow = "left") {
        $model = $this->model;

        if (count($result->sandwiches) > 0) {
            $sandwiches = $model->processSandwiches($result->sandwiches);
        } else {
            $sandwiches = array();
        }

        $total = 0;
        $o = '';

        foreach ($sandwiches as $key => $sum) {
            $sum['item_total'] = number_format((float) $sum['item_price'] * $sum['item_qty'], 2, '.', '');
            $total = $total + ($sum['item_price'] * $sum['item_qty']);
            $o .= $sum['order_item_id'] . ' ';
            $sandwiches[$key] = $sum;
        }

        $products = $result->products;
        // /echo '<pre>';print_r($products);exit;
        foreach ($products as $key => $product) {
            $total += $product->product_price * $product->item_qty;

            $extra_price = $product->item_qty * $product->extra_cost;
            $total += $extra_price;
            $product->item_price = ($product->product_price * $product->item_qty) + ($product->item_qty * $product->extra_cost);
            $product->item_price = number_format((float) $product->item_price, 2, '.', '');
            $product->item_total = $product->item_price;
            $product->item_total = number_format((float) $product->item_total, 2, '.', '');
            $products[$key] = $product;

            $o .= $product->order_item_id . ' ';
        }

        $o = trim($o);
        $ordString = str_replace(' ', ':', $o);

        $subtotalNumber = $totalNumber = $total;

        $subtotal = number_format((float) $total, 2, '.', '');
        $total = 3 + number_format((float) $total, 2, '.', '');

        $subtotal = number_format($subtotal, 2);
        $total = number_format($total, 2);


        //If deliveryOrPickup is pickup 0 then no tip
        if ($deliveryOrPickup == 0) {
            $tips = 0;
        } else {
            //default 15% of subtotal
            $tips = ($subtotalNumber * 15) / 100;
            $tips = round($tips);

            if ($tips < MIN_TIP)
                $tips = MIN_TIP;

            if ($tips > MAX_TIP)
                $tips = MAX_TIP;
        }

        if ($last_tip > $tips && $whichArrow == "right")
            $tips = $last_tip;

        $tax = $subtotalNumber * TAX;
        $tax = number_format($tax, 2);

        $this->smarty->assign('pickupOrDelivery', $deliveryOrPickup);

        $this->smarty->assign('tips', $tips);

        $total = $subtotalNumber + $tips + $tax;
        $totalNumber = $total;
        $total = number_format((float) $total, 2, '.', '');

        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('MAX_TIP', MAX_TIP);
        $this->smarty->assign('MIN_TIP', MIN_TIP);
        $this->smarty->assign('sandwiches', $sandwiches);
        $this->smarty->assign('products', $products);

        $this->smarty->assign('itemsCount', count($products) + count($sandwiches));
        $this->smarty->assign('order_string', $ordString);
        $this->smarty->assign('total', $subtotal);
        $this->smarty->assign('tax', $tax);
        $this->smarty->assign('totalNumber', $subtotalNumber);
        $this->smarty->assign('grandtotal', $total);
        $output = $this->smarty->fetch("cart-list-items.tpl");

        return $output;
    }

    function applyDiscount() {

        $this->title = TITLE;
        $model = $this->model;
        $result = $model->geDiscountDetails($_POST);
        echo json_encode($result);
        exit;
    }

    function checkout_confirm() {
        ini_set("display_errors", 1);
        $this->title = TITLE;
        $model = $this->model;

        //if no uid checkout_confirm page not available
        $uid = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
        if (!$uid) {
            header('Location: ' . SITE_URL);
            exit;
        }

        $order_det = $model->check_out_confirm( $this->UrlArray[2] );
        $deliveryTime = $model->deliveryTime( $order_det->Data->sub_total );
        $time = $deliveryTime->Data;
        $messege = $time->message;

//print_r( $order_det );die;
        $this->smarty->assign('cart', 0);
        $this->smarty->assign('date', @$order_det->Data->date);
        $this->smarty->assign('delivery_type', @$order_det->Data->delivery_type);
        $this->smarty->assign('check_now_specific', @$order_det->Data->check_now_specific);
        $this->smarty->assign('time', @$order_det->Data->time);
        $this->smarty->assign('billing_type', @$order_det->Data->billing_type);
        $this->smarty->assign('billing_card_no', @$order_det->Data->billing_card_no);
        $this->smarty->assign('address', @$order_det->Data->address);
        $this->smarty->assign('delivery_instructions', @$order_det->Data->delivery_instructions);
        $this->smarty->assign('total', @$order_det->Data->total);
        $this->smarty->assign('sub_total', @$order_det->Data->sub_total);
        $this->smarty->assign('tip', @$order_det->Data->tip);
        $this->smarty->assign('delivery_fee', @$order_det->Data->delivery_fee);
        $this->smarty->assign('tax', @$order_det->Data->tax);

        $this->smarty->assign('order_number', htmlspecialchars($this->UrlArray[2]) );

        $this->smarty->assign('order_item_detail', @$order_det->Data->order_item_detail);
        $this->smarty->assign('discount', @$order_det->Data->discount);
        $this->smarty->assign('orderid', $this->UrlArray[2]);
        $user = $model->getUserDetails($uid);
        $this->smarty->assign('user', $user);
        $review_content = $model->get_all_review_contents();
        $this->smarty->assign('review_content', $review_content);
        
        $data['uid'] = $uid;
       
        $user_review = $model->get_user_review($data);
        $this->smarty->assign('user_review', $user_review);

        $count = array(
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        );
        $this->smarty->assign('count', $count);
        $this->smarty->assign('messege', $messege);
        $this->smarty->assign('content', "check-out-confirm.tpl");
        setcookie('specific_date_exists', null, -1, '/');
        setcookie('specific_date_sel', null, -1, '/');
        setcookie('specific_date_time_sel', null, -1, '/');
        unset($_SESSION['uid']);
    }


     function getSpecificDayForStore() {
        $model = $this->model;
        if( empty( $_POST  ) ){
            echo json_encode(array(  ));
            exit(  );
        }
        $storeId = $this->clean_for_sql( $_POST['storeId'] );
        $store_info = $model->getspecificday($storeId);

        $result = array(  );
        $result['specific_message'] = $store_info->specificday_message;
        $result['specific_day'] = $store_info->specificday;

        echo json_encode( $result );
        exit(  );
     }


    function changeTimeOnStoreSelection() {

        $storeID = @$_POST['store_id'];
        $selectedDay = @$_POST['selectedDay'];
        $Currenttime = @$_POST['current_time'];
        $isCurrentDay = @$_POST['isCurrentDay'];
        $isDelivery = @$_POST['isDelivery'];
        $model = $this->model;

        if (!empty($_POST['current_day'])) {
            $datenow = $_POST['current_day'];
            $selectedDay = date('l', strtotime($datenow));
        } else
            $datenow = date("Y-m-d");

        $nowdate = date("n/j/Y", strtotime($datenow));
        $specific = $model->getspecificday($storeID);
        $specificDate = date("n/j/Y", strtotime($specific->specificday));

        if (strtotime($nowdate) == strtotime($specificDate) && $specific->specificday_active == 1) {
            $message = $specific->specificday_message;
            $Smessage['message'] = $message;
            $Smessage['date'] = $specificDate;
            $output = json_encode($Smessage);
        } else {
            $result = $model->getTimeOnStoreSelection($storeID, $selectedDay, $Currenttime, 0, $isCurrentDay, $isDelivery);

            if ($_POST['isCurrentDay'] == 1 && $result['day'] == $selectedDay) {

                $currenthour = date('H');
                $currentmin = date('i');
                if ($currentmin < 15)
                    $currentmin = 0;
                else if ($currentmin >= 15 && $currentmin < 30)
                    $currentmin = 15;
                else if ($currentmin >= 30 && $currentmin < 45)
                    $currentmin = 30;
                else if ($currentmin >= 45 && $currentmin < 60)
                    $currentmin = 45;

                $currentdate = $currenthour . ":" . $currentmin;

                if (strtotime($currentdate) < strtotime($result['store_details']->close) && strtotime($currentdate) > strtotime($result['store_details']->open)) {

                    $from = date('H:i:s', strtotime($currentdate));
                } else {

                    $from = date('H:i:s', strtotime($result['store_details']->open));
                }
            } else {
                if (isset($result['store_details']->open) && $result['store_details']->open) {
                    $from = date('H:i:s', strtotime($result['store_details']->open));
                } else {
                    exit("error");
                }
            }

            if (isset($result['store_details']->close) && $result['store_details']->close) {

                $todate = date('H:i:s', strtotime($result['store_details']->close));
            } else {
                exit("error");
            }

            sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
            $lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $lower = $lower + 900;

            sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
            $upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $upper = $upper - 900;
            $times = "<select class='timedropdown'>";
            if (empty($format)) {
                $format = 'g:i a';
            }
            $step = 900;
            foreach (range($lower, $upper, $step) as $increment) {
                $increment = gmdate('H:i', $increment);

                list($hour, $minutes) = explode(':', $increment);

                $date = new DateTime($hour . ':' . $minutes);
                $date = $date->format($format);

                if ($hour == '12' && $minutes == '00')
                    $s = 'selected="selected"';
                else
                    $s = '';

                $times .= "<option " . $s . " value=" . $hour . ':' . $minutes . ">" . $date . "</option>";
            }
            $times .= "</select>";
            $dateHid = "<input type='hidden' name='updatedDate' value='" . date("n/j/Y", strtotime($datenow)) . "' />";
            if ($result['run_counts'] > 0) {

                if ($_POST['selectedDay'] == $result['day']) {

                    $dateHid = "<input type='hidden' name='updatedDate' value='" . date("n/j/Y", strtotime($datenow)) . "' />";
                } else {
                    $result_date = date("n/j/Y", strtotime($datenow . " +" . $result['run_counts'] . " day"));
                    //echo "result".$result_date;
                  if ($result_date == $specificDate && $specific->specificday_active == 1) {  //echo "coming";
                        
                      
                       // DM
                    
                      $result_date = date("n/j/Y", strtotime($result_date ));
                       
                      $this->changeTimeOnStoreSelectionOnSpecificDay($storeID, $result_date, $Currenttime, 0, $isDelivery);
                      exit;
                    }else{
                        
                    }
                    $dateHid = "<input type='hidden' name='updatedDate' value='" . $result_date . "' />";
                    
                }
            }
            $times .= $dateHid;
            $this->smarty->assign('times', $times);
            
            // dm this got this part working
            if( !isset( $message ) )$message='';
            $this->smarty->assign('message', $message);
            $output = $this->smarty->fetch("pickup-time-select.tpl");
            $array_output = json_encode(array("html" => $times, "message" => ''));
            $output = $array_output;
        }
        echo $output;

        exit;
    }

    function changeTimeOnStoreSelectionOnSpecificDay($storeID, $selectedDay, $Currenttime, $isCurrentDay, $isDelivery) {

        $storeID = $storeID;
        $selectedDay = $selectedDay;
        $Currenttime = $Currenttime;
        $isCurrentDay = $isCurrentDay;
        $isDelivery = $isDelivery;
        $model = $this->model;

       
        $selectedDay = date('l', strtotime($selectedDay));
            $datenow = $selectedDay;

       
        $nowdate = date("n/j/Y", strtotime($datenow));
      
            $result = $model->getTimeOnStoreSelection($storeID, $selectedDay, $Currenttime, 0, $isCurrentDay, $isDelivery);
            
                if (isset($result['store_details']->open) && $result['store_details']->open) {
                    $from = date('H:i:s', strtotime($result['store_details']->open));
                } else {
                    exit("error");
                }
      
            if (isset($result['store_details']->close) && $result['store_details']->close) {

                $todate = date('H:i:s', strtotime($result['store_details']->close));
            } else {
                exit("error");
            }



            sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
            $lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $lower = $lower + 900;

            sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
            $upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $upper = $upper - 900;
            $times = "<select class='timedropdown'>";
            if (empty($format)) {
                $format = 'g:i a';
            }
            $step = 900;
            foreach (range($lower, $upper, $step) as $increment) {
                $increment = gmdate('H:i', $increment);

                list($hour, $minutes) = explode(':', $increment);

                $date = new DateTime($hour . ':' . $minutes);
                $date = $date->format($format);

                if ($hour == '12' && $minutes == '00')
                    $s = 'selected="selected"';
                else
                    $s = '';

                $times .= "<option " . $s . " value=" . $hour . ':' . $minutes . ">" . $date . "</option>";
            }
            $times .= "</select>";
            $dateHid = "<input type='hidden' name='updatedDate' value='" . date("n/j/Y", strtotime($datenow)) . "' />";
            if ($result['run_counts'] > 0) {

                if ($selectedDay == $result['day']) {

                    $results_date = date("n/j/Y", strtotime($datenow));
                } else {
                    $result_date = date("n/j/Y", strtotime($datenow . " +" . $result['run_counts'] . " day"));

                   
                    $dateHid = "<input type='hidden' name='updatedDate' value='" . $result_date . "' />";
                    
                }
            }
            $times .= $dateHid;
            $this->smarty->assign('times', $times);
            $this->smarty->assign('message', $message);
            $output = $this->smarty->fetch("pickup-time-select.tpl");
            $array_output = json_encode(array("html" => $times, "message" => ''));
            $output = $array_output;
       
        echo $output;

        exit;
    }

    public function getCurrentHour() {

        $response = array();
        $response['hour'] = date("H:i");
        $response['day'] = date("w");
        $response['date_time'] = date("d-m-y H:i:s");
        $response['date_time_js'] = date("Y,m,d,H,i,s");
        $response['timezone'] = date_default_timezone_get();
        echo json_encode($response);
        exit;
        // $dt = new DateTime('now', new DateTimezone('America/New_York'));
        // //$dt = new DateTime('now', new DateTimezone('Asia/Kolkata'));
        // $response                 = array();
        // $response['hour']         = $dt->format("H:i");
        // $response['day']          = $dt->format("w");
        // $response['date_time']    = $dt->format("d-m-y H:i:s");
        // $response['date_time_js'] = $dt->format("Y,m,d,H,i,s");
        // $response['timezone']     = $dt->getTimezone();

        // echo json_encode($response);
        // exit;
      
    }

    public function getCurrentStoreTimeSlot() {
        $response = $this->model->getCurrentStoreTimeSlot($_POST);
        echo json_encode($response);
        exit;
    }

    public function updatePickupOrDelivery() {
        if (isset($_SESSION['uid'])) {
            $model = $this->model;
            $user["user_id"] = $_SESSION['uid'];

            $deliveryOrPickup = $_POST['deliveryOrPickup'];

            $shopitems = $model->cartItems($user);
            $output = $this->getCartItemList($shopitems, $deliveryOrPickup);

            $response = new stdClass();
            $response->status = true;
            $response->message = $output;
            echo json_encode($response);
            exit;
        }
    }

    public function getCartCount() {
        $count = 0;
        if (isset($_SESSION['uid']) || isset($_COOKIE['user_id'])) {
            $uid = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
            $model = $this->model;
            $data = $model->get_user_checkout_data($uid);

            if (isset($data) && $data) {


                $outdata = json_decode($data);
                foreach ($outdata->Data as $counts) {
                    $count = $count + $counts->item_qty;
                }
            }
        }

        return $count;
    }

    function checkZip() {
        $model = $this->model;

        $result = $model->checkZip($_POST);
        //echo $result;
        print_r(json_encode($result));
        exit;
    }

    function getAllZips( )
    {
        $model = $this->model;

        $result = $model->getAllZips( );
        echo $result;
        exit;
    }

    public function get_storeClosed_days()
    {
       $model = $this->model;

        $result = $model->get_storeClosed_days($_POST);
        print_r(json_encode($result));
        exit;
    }

    public function cronToDeleteItems()
    {
        $model = $this->model;

        $result = $model->cronToDeleteItems();
        print_r(json_encode($result));
        exit;
    }

}
