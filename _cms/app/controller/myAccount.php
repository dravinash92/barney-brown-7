<?php

class myaccount extends Base
{
	public $model;
	
	public function __construct(){
				
		$this->model = $this->load_model("myAccountModel");
	}
	
	function index(){
		
		
		
		if(isset($_GET['view'])) { 	$this->smarty->assign('orderview',1);}
		else {  	$this->smarty->assign('orderview',0); }
		if(isset($_SESSION['uid']))
			$uid = $_SESSION['uid'];
		else{
			echo "<script>window.location='".SITE_URL."';</script>";
			exit;
		}	
		
		
		$model = $this->model;
		$this->title = TITLE;
		
		$this->smarty->assign('SITE_URL',SITE_URL);
		
		$this->smarty->assign('LOGGED_IN',true);
		$user = $model->getUserDetails($_SESSION['uid']);
		$this->smarty->assign('user',$user);
		$review_content = $model->get_all_review_contents();
		$this->smarty->assign('review_content',$review_content);
		$data['uid'] = $_SESSION['uid'];
		$user_review = $model->get_user_review($data);
		$this->smarty->assign('user_review',$user_review);
		$count = array(1,2,3,4,5,6,7,8,9,10);
		$this->smarty->assign('count',$count);
		
		$this->smarty->assign('content',"myaccount-profile.tpl");
		
	}
	
    function loginCheck()
    {
        $model = $this->model;        
        $data = $model->loginCheck($_POST);
        
        if(count($data)==1)
        {  
            $_SESSION['uid']=$data[0]->uid;
            $_SESSION['start_time'] = strtotime("now");
            $_SESSION['uname']=$data[0]->first_name;
            $_SESSION['lname']=$data[0]->last_name;
            $result["state"] = "true";
            $values = array();
            $mail   = $data[0]->username;
            $fname  = $data[0]->first_name;
            $lname  = $data[0]->last_name;
            $uid    = $data[0]->uid;
            $cookietime = time()+60*60*24*60;
            setcookie("user_mail",$mail,$cookietime,'/',NULL);
            setcookie("user_fname",$fname,$cookietime,'/',NULL);
            setcookie("user_lname",$lname,$cookietime,'/',NULL);
            setcookie("user_id",$uid,$cookietime,'/',NULL);
            
        }
        else{
          
            $result["state"]="false";
            $result["msg"]="Invalid Username or Password";
        }
        echo json_encode($result);
        
        exit;
    }
    
  function is_session(){
		$data = new stdClass ();
		if (! isset ( $_SESSION ['uid'] ) && ! isset ( $_COOKIE ['user_id'] )) {
			$state = false;
			$this->model->deleteAll ( $_POST );
		} else {
			session_start();
			if (! isset ( $_SESSION ['uid'])  && isset($_COOKIE ['user_id']))
			{
				$_SESSION ['uid'] = $_COOKIE ['user_id'];
				$_SESSION['uname'] = $_COOKIE ['user_fname'];
				$_SESSION['lname'] = $_COOKIE ['user_lname'];
			}
			

			$state = true;
			$out = $this->model->checkUserAvailability ();
			$out = json_decode ( $out );
			if (isset ( $out ) && isset ( $out->Data )) {
				$data->userAvail = $out->Data [0]->count;
			}
		}
		
		if (isset ( $_COOKIE ['facebookOnlyUser'] ) && $_COOKIE ['facebookOnlyUser'])
			$fbonly = true;
		else
			$fbonly = false;
		
		$data->session_state = $state;
		$data->facebook_only_user = $fbonly;
		echo json_encode ( $data );
		exit ();
  	
  }
    
    function createAccount(){
        $model = $this->model;        
        $result = $model->createAccount($_POST);
        if($result->state){
			$_SESSION['uid']=$result->msg->uid;
            $_SESSION['uname']=$result->msg->first_name;
            $_SESSION['lname']=$result->msg->last_name;
            $mail   = $result->msg->username;
            $fname  = $result->msg->first_name;
            $lname  = $result->msg->last_name;
            $uid    = $result->msg->uid;
            $cookietime = time()+60*60*24*60;
            setcookie("user_mail",$mail,$cookietime,'/',NULL);
            setcookie("user_fname",$fname,$cookietime,'/',NULL);
            setcookie("user_lname",$lname,$cookietime,'/',NULL);
            setcookie("user_id",$uid,$cookietime,'/',NULL);
		}
        echo json_encode($result);
        exit;   
    }
    
	function profile(){
		
		$model = $this->model;
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('LOGGED_IN',true);
		$user = $model->getUserDetails($_SESSION['uid']);
		$this->smarty->assign('user',$user);		
		$review_content = $model->get_all_review_contents();
		$this->smarty->assign('review_content',$review_content);
		$data['uid'] = $_SESSION['uid'];
		$user_review = $model->get_user_review($data);
		$this->smarty->assign('user_review',$user_review);
		$count = array(1,2,3,4,5,6,7,8,9,10);
		$this->smarty->assign('count',$count);
	    $output = $this->smarty->fetch("myaccount-profile.tpl");
		echo $output;exit;		
	}
	
	function orderhistory(){
	
		$model  = $this->model;
		$data['user_id'] = $_SESSION['uid'];
		$orderhistory = $model->orderhistory($data);
		//echo "<pre>";print_r($orderhistory);exit;
		$this->smarty->assign('orderhistory',$orderhistory);
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$output = $this->smarty->fetch("myaccount-order-history.tpl");
		echo $output;
		exit;
	}
	
	function savedaddress(){
	
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$model  = $this->model;
		$addresses = $model->listSavedAddress($_SESSION['uid']);
		$this->smarty->assign('addresses',$addresses);
		$output = $this->smarty->fetch("myaccount-saved-address.tpl");
		echo $output;exit;			
	}
	
	function savedbilling(){
		$model  = $this->model;
		$data['uid'] = $_SESSION['uid'];
		$card_details = $model->savedBilling($data);
		

		foreach( $card_details as $card_key => $card_val  )
		{	
			
			
			if( ! (strpos( $card_val->card_number, 'XXXX-XXXX-XXXX' ) > -1)   )	{		// Does NOT start w/ xxxx-xxxx...
				$card_number_masked = "XXXX-XXXX-XXXX-" . strpos( $card_val->card_number, -4 );
				
			}
			else {
				$card_number_masked  = $card_val->card_number ;	
			}
			$card_details[$card_key]->card_number_masked = $card_number_masked;
		}
	
		if(count($card_details) > 0)
            {
                foreach ($card_details as $key => $value) {
                  $card_details[$key]->card_number = "XXXX-XXXX-XXXX-".substr( $value->card_number, -4 );
                }
            }
		$this->smarty->assign('card_details',$card_details);
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$output = $this->smarty->fetch("myaccount-saved-billing.tpl");
		echo $output;exit;
	
	}
	
    function signout()
    {
       
        session_destroy();
        if(count($_COOKIE) > 0){
			$past = time() - 3600;
			foreach ( $_COOKIE as $key => $value )
			{
			setcookie($key, $value, $past, '/',NULL);
			}
        }
           
        header("Location:".SITE_URL);
        exit;
    }
    
    function savingAddress(){
    	ini_set("display_errors", 1);
		$model  = $this->model;
		$_POST['uid'] = $_SESSION['uid'];
		$result = $model->savingUserAddress($_POST);
		echo $result;
		exit;	
	}
	
	function removeAddress(){
		$model  = $this->model;
		$_POST['uid'] = $_SESSION['uid'];
		$result = $model->removeAddress($_POST);
		echo $result;
		exit;	
	}
	
	function editAddress(){
		$model  = $this->model;
		$_POST['uid'] = $_SESSION['uid'];
		$result = $model->editAddress($_POST);
		$address = $result[0];
		$this->smarty->assign('SITE_URL',SITE_URL);
		if($address->address_id){
			$phone = explode("-", $address->phone);	
			$this->smarty->assign('phone1',$phone[0]);	
			$this->smarty->assign('phone2',$phone[1]);	
			$this->smarty->assign('phone3',$phone[2]);	
			$this->smarty->assign('address',$address);	
			$output = $this->smarty->fetch("edit-address.tpl");
		}else{
			$output = "No address found";
		}	
			echo $output; 
		exit;	
	}
	
	function userNameExist(){
		$model  = $this->model;
		$data = $model->userNameExist($_POST);
		echo json_encode($data);
		exit;
	}
	
	function savingCustomerInfo(){
		$model  = $this->model;
		 $data = $model->savingCustomerInfo($_POST);
		echo json_encode($data);
		exit;
	}
	
	function addUserReview(){
		$model  = $this->model;
		$data = $model->addUserReview($_POST);
	   echo  json_encode($data);
		exit;
	}
	
	function cardAddEditEvent(){
		$model  = $this->model;
		$data = $model->cardAddEditEvent($_POST);
		echo json_encode($data);
		exit;
	}
	
	function cardRemoveEvent(){
		$model  = $this->model;
		$_POST['uid'] = $_SESSION['uid'];
		$data = $model->cardRemoveEvent($_POST);
		echo $data;
		exit;
	}
	
	public function forgotPassword(){
		$model = $this->model;
		$result = $model->forgotPassword($_POST);
		echo json_encode($result);
		exit;
	}
	
	public function getAllzipcodes(){
		$model = $this->model;
		$result = $model->getAllzipcodes($_POST);
		$result = json_decode($result);
		$arr = array();
		if(count($result->Data>0)){
			foreach($result->Data as $data){
				$arr[] = $data->zipcode;
			}
			$arr = array_unique($arr);
		}
		
		echo json_encode($arr);
		
		exit;
	}
	
	public function myOrderReorder(){
		$model = $this->model;
		$response = $model->myOrderReorder($_POST);
		echo $response;
		exit;
	}
	
	public function myOrderDetails(){
	
		$model = $this->model;
        $order_det = $model->myOrderDetails($_POST['order_id']);
        
		        
        $this->smarty->assign('date',@$order_det->Data->date);
        $this->smarty->assign('time',@$order_det->Data->time);
        $this->smarty->assign('billing_type',@$order_det->Data->billing_type);
        $this->smarty->assign('billing_card_no',@$order_det->Data->billing_card_no);
        $this->smarty->assign('address',@$order_det->Data->address); 
        $this->smarty->assign('delivery_instructions',@$order_det->Data->delivery_instructions);
        $this->smarty->assign('total',@$order_det->Data->total);
        $this->smarty->assign('sub_total',@$order_det->Data->sub_total);
        $this->smarty->assign('tip',@$order_det->Data->tip);
        $this->smarty->assign('delivery_fee',@$order_det->Data->delivery_fee);
        $this->smarty->assign('order_item_detail',@$order_det->Data->order_item_detail);
        $this->smarty->assign('discount',@$order_det->Data->discount);
        $this->smarty->assign('orderid',$this->UrlArray[2]);
        $this->smarty->assign('delivery_type',@$order_det->Data->delivery_type); 
        $user = $model->getUserDetails(@$_SESSION['uid']);
		$this->smarty->assign('user',$user);
        $review_content = $model->get_all_review_contents();
		$this->smarty->assign('review_content',$review_content);
		$data['uid'] = @$_SESSION['uid'];
		$user_review = $model->get_user_review($data);
		$this->smarty->assign('user_review',$user_review);
        
        $count = array(1,2,3,4,5,6,7,8,9,10);
		$this->smarty->assign('count',$count);
        $data = $this->smarty->fetch("order-details.tpl");
        echo $data;
        exit;
	
	}
	
	public function getStoreFomZip(){
		$model = $this->model;
        echo json_encode($model->getStoreFomZip());
        exit;
	}
	
	public function getUserAccessToken(){
		$out = array();
		$data = $this->model->getUserAccessToken();
		$data = json_decode($data);
		
		if(isset($data->Data)){
			if(isset($data->Data[0]->fb_auth_token)){
				$toc = $data->Data[0]->fb_auth_token;
			} else $toc = null;
		} else $toc = null;
     
		if($toc){  
                            $this->setSession();
			$validToc     = $this->FB_is_valid_token($toc);
               $user_data = $this->FB_get_user($toc); 
			  $out['fbstate'] = $this->input_FB_users($user_data);
			
			 $out['state'] = true;
			         $out['Response']['status-code'] = $data->Response->{'status-code'};
        
		}
		
		else {  $out['state'] = false;  $out['fbstate'] = false; }
		echo json_encode($out);
		exit;
	}
	
	function input_FB_users($user_data){
		$result         = true;
	 	$uid            = $user_data['uid'];
	 	$fname          = $user_data['first_name'];
	 	$lname          = $user_data['last_name'];
	 	$email          = $user_data['email'];
	 	$token          = @$_SESSION['access_token'];
	 	$friend_data    = $user_data['friend_data'];
	 	if(isset($friend_data['name'])) $count_friend  = count($friend_data['name']);
	 	else  $count_friend  = 0;
	 	
	  
	 	$num            = $this->db->Query("SELECT username,first_name,uid,fb_auth_token FROM users WHERE username = '$email' OR  fb_id = '$uid'");
	 	$count          = $this->db->Rows();
	 	$data           = $this->db->FetchArray($num);
	 	
	 	
	 	if($count == 0 && !@$_SESSION['uid']){
	 		$result_usr = $this->db->Query(" INSERT INTO users (username,fb_id,fb_auth_token,first_name,last_name) VALUES ('$email','$uid','$token','$fname','$lname') ");
	 		$insid      = $this->db->insertId();
	 		$_SESSION['uid']   = $insid;
	 		$_SESSION['uname'] = $fname;
	 		$_SESSION['lname']=$lname;
	 		$uidz              = $insid;
	 	}
	 	else if($count && !@$_SESSION['uid']) {
	 		if( (isset($_COOKIE['user_id']) && $_COOKIE['user_id'] )  && $_COOKIE['user_id'] != $data['uid']){
	 	 	$result         = false;
	 		} else if(!@$_COOKIE['user_id'] && !@$_SESSION['uid']){
	 			$_SESSION['uid']   = $data['uid'];
	 			$_SESSION['uname'] = $data['first_name'];
	 			$_SESSION['lname']=$data['last_name'];
	 			$cookietime = time()+60*60*24*60;
	 			setcookie("user_mail",$data['username'],$cookietime,'/',NULL);
	 			setcookie("user_fname",$data['first_name'],$cookietime,'/',NULL);
	 			setcookie("user_lname",$data['last_name'],$cookietime,'/',NULL);
	 			setcookie("user_id",$data['uid'],$cookietime,'/',NULL);
	 		}
	 		$uidz              = $data['uid'];
	 		if( $data['fb_auth_token'] != $token ){ $this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' "); }
	 	} else if(!$count && $_SESSION['uid']){
	 		$uidz              = $_SESSION['uid'];
	 		if( $data['fb_auth_token'] != $token ){ $this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' "); }
	 	} else if($count && $_SESSION['uid']){
	 		$uidz              = $data['uid'];
	 		if($_SESSION['uid'] != $data['uid']){
	 			$result = false;
	 		}
	 		if( $data['fb_auth_token'] != $token ){ $this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' "); }
	 	}
	 	
	 	
	 	for($i = 0; $i< $count_friend; $i++ ){
	 		$frnd_data = $this->db->Query("SELECT friend_uid FROM friend_data WHERE friend_uid = '".$friend_data['id'][$i]."' AND parent_uid = '$uidz' ");
	 		$count     = $this->db->Rows();
	 		if($count == 0){
	 			
	 			$this->db->Query("INSERT INTO friend_data (friend_name,friend_uid,parent_uid) VALUES ('".$friend_data['name'][$i]."','".$friend_data['id'][$i]."','$uidz') ");
	 		}
	 	}
		return $result;
	 }	
}
