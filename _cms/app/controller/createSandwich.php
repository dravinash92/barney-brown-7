<?php

class CreateSandwich extends Base
{
    public $model;
    
    public function __construct()
    {
        $this->model = $this->load_model("createSandwichModel");
    }
    
    function index()
    {   
    	
        if (is_numeric($this->UrlArray[2])){
          if(!isset($_SESSION['temp_sandwich_data']) && !@$_SESSION['temp_sandwich_data'])  {
          	$dataid = $this->UrlArray[2];
          }
          else {
          	$dataid = '';
          }
        }
        else {
            $dataid = '';
        }
        $uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
        $model       = $this->model;
        $bread_data  = $model->load_data(1);
        $this->title = TITLE;
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('BASE_FARE', BASE_FARE);
        $this->smarty->assign('BREAD_DATA', $bread_data);
        $this->smarty->assign('LANDING_PAGE', $this->smarty->fetch('sandwich-landing.tpl'));
        $this->smarty->assign('OPTN_DATA', $this->smarty->fetch('sandwich-bread.tpl'));
        $data     = json_decode($model->get_user_sanwich_data($dataid, $uid));
        $usrFname = $model->get_user_firstName($uid);
        $usrFname = $usrFname ? $usrFname . "'s protein " : ' ';
        $this->smarty->assign('DATA_ID', $dataid);
        $sandwich_name = json_decode($model->get_random_name());
        
        $sandwich_name =  $sandwich_name ;
        if($dataid){
         $this->smarty->assign('sandwichId',$dataid);
        }
        
        if (@$data->Data[0]->sandwich_name)
            $this->smarty->assign('SANDWICH_NAME', htmlspecialchars( @$data->Data[0]->sandwich_name) );
        else
            $this->smarty->assign('SANDWICH_NAME', $usrFname .htmlspecialchars( $sandwich_name->Data->name ) ) ;
        $this->smarty->assign('IS_PUB', @$data->Data[0]->is_public);
        if (isset($_COOKIE['sandwich_end_screen']))
            $this->smarty->assign('end_screen', $_COOKIE['sandwich_end_screen']);
        $this->smarty->assign('content', "create_sandwich.tpl");
    }
    

    function quickEditSandwich(){
        ini_set("display_errors", 1);
        $post = $_POST;

        $dataid = $post['id'];
        $result_data = array();

        $uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
        $model       = $this->model;
         $bread_data  = $model->load_data(1); 

         $result_data['bread_data'] = $bread_data;
         $result_data['base_fare'] = BASE_FARE;
        $data     = json_decode($model->get_user_sanwich_data($dataid, $uid));
        
        
        if (@$data->Data[0]->sandwich_name)
            $result_data['sandwich_name'] = $data->Data[0]->sandwich_name;

        
        print_r(json_encode($result_data));
        exit;

    }
    
    function getRandom_sandwich_name()
    {
        $sandwich_name = json_decode($this->model->get_random_name());
        echo $sandwich_name->Data->name;
        exit;
    }
    
    
    
     function preload_images()
    {
        $model  = $this->model;
        $images = $model->get_all_images();
        echo json_encode($images); 
        exit;
    }
    
    function get_category_items_data()
    {
        ini_set("display_errors", 1);
        $ids    = $_POST['ids'];
        $json   = $this->model->get_category_items_data($ids);
        print_r($json);
        exit;
    }
    
    
    function user_input()
    {
        $model = $this->model;
        $data  = array(
            'id' => @$_POST['id'],
            'data' => @$_POST['data'],
            'uid' => @$_POST['uid'],
            'price' => @$_POST['price'],
            'is_pub' => @$_POST['is_pub'],
            'name' => @$_POST['name'],
            'sandwich_items' => @$_POST['items_names'],
            'tempname' => @$_POST['tempname'],
            'menu_active' => @$_POST['menu_active'],
            'toast' => @$_POST['toast'],
            'is_saved' => @$_POST['is_saved']
        );
        //echo "<pre>";print_r($data);exit;
        echo $model->input_user_sanwich_data($data);
        $this->unlinkCacheFile();
        unset($_SESSION['temp_sandwich_data']);
        exit;
    }
    
    function temp_session_input()
    {
        if(isset($_POST['data']) && !empty($_POST['data'])) {
            $_SESSION['temp_sandwich_data'] = $_POST['data'];
            exit;
        }
    }
    
    function get_sandwich_data()
    {
        $model = $this->model;
        $uid   = $_POST['uid'];
        $id    = $_POST['id'] != ''?$_POST['id']:'*';
        $source = $_POST['source'];
        
       // if ( empty($id)) {
            if (isset($_SESSION['temp_sandwich_data']) && $source == 'edit' && empty($id)) {
                echo $_SESSION['temp_sandwich_data'];
            //}  
        } else {
            echo $model->get_user_sanwich_data($id, $uid);
        }
        exit;
    }
    
    
    function load_bread_data()
    {
        $model       = $this->model;
        $bread_data  = $model->load_data(1);
        $this->title = TITLE;
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('BREAD_DATA', $bread_data);
        $data = $this->smarty->fetch('sandwich-bread.tpl');
        echo $data;
        exit;
    }
    
    function load_protien_data()
    {
        $model        = $this->model;
        $protien_data = $model->load_data(2);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('PROTIEN_DATA', $protien_data);
        //echo '<pre>';print_r($protien_data);exit;
        $data = $this->smarty->fetch('sandwich-protein.tpl');
        echo $data;
        
        exit;
    }
    
    function load_cheese_data()
    {
        $model       = $this->model;
        $cheese_data = $model->load_data(3);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('CHEESE_DATA', $cheese_data);
        $data = $this->smarty->fetch('sandwich-cheese.tpl');
        echo $data;
        exit;
    }
    
    
    function load_toppings_data()
    {
        $model         = $this->model;
        $toppings_data = $model->load_data(4);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('TOPPINGS_DATA', $toppings_data);
        $data = $this->smarty->fetch('sandwich-toppings.tpl');
        echo $data;
        exit;
    }
    
    
    function load_condiments_data()
    {
        $model           = $this->model;
        $condiments_data = $model->load_data(5);
        $this->smarty->assign('LOGGED_IN', true);
        $this->smarty->assign('CONDIEMENTS_DATA', $condiments_data);
        $data = $this->smarty->fetch('sandwich-condiments.tpl');
        echo $data;
        exit;
    }
    
    
    
    function filter_words()
    {
        echo $this->model->filter_words();
        exit;
    }
    
    
    function save_image()
    {
        
        $width             = @$_POST['width'];
        $height            = @$_POST['height'];
        $json_image_string = @$_POST['json_string'];
        $file              = @$_POST['file_name'];
        if (isset($_POST['path']))
            $path = SANDWICH_UPLOAD_DIR . $_POST['path'];
        else
            $path = SANDWICH_UPLOAD_DIR;
        
        
        $img = json_decode($json_image_string);

        if (!extension_loaded('gd')) {
            exit('GD requires to do this operation');
        }
        
        
        if (!is_dir($path)) {
            if (!mkdir($path, 0777, true)) {
                exit('error creating path folder');
            }
        }
        if (!is_dir($path . "thumbnails/")) {
            if (!mkdir($path . "thumbnails/", 0777, true)) {
                exit('error creating thumbnail path folder');
            }
        }
        
        
        $layers = array();
        $width  = array();
        $height = array();
        foreach ($img->images as $images) {
            $layers[]  = imagecreatefrompng(ADMIN_URL . 'upload/' . $images);
            $data      = getimagesize(ADMIN_URL . 'upload/' . $images);
            $widths[]  = $data[0];
            $heights[] = $data[1];
        }
        
        $width  = max($widths);
        $height = max($heights);
        
        $image = imagecreatetruecolor($width, $height);
        $thumb_image = imagecreatetruecolor(250, 150); // for thumbnail
        
        // to make background transparent
        imagealphablending($image, false);
        $transparency = imagecolorallocatealpha($image, 0, 0, 0, 127);
        imagefill($image, 0, 0, $transparency);
        imagesavealpha($image, true);

        // to make thumbnail background transparent
        imagealphablending($thumb_image, false);
        $transparency = imagecolorallocatealpha($thumb_image, 0, 0, 0, 127);
        imagefill($thumb_image, 0, 0, $transparency);
        imagesavealpha($thumb_image, true);
        
        /* if you want to set background color
        $white = imagecolorallocate($image, 255, 255, 255);
        imagefill($image, 0, 0, $white);
        */
        
        imagealphablending($image, true);
        for ($i = 0; $i < count($layers); $i++) {
            imagecopy($image, $layers[$i], 0, 0, 0, 0, $width, $height);
        }
        imagealphablending($image, false);
        imagesavealpha($image, true);

        //for thumbnail
        imagealphablending($thumb_image, true);
        for ($i = 0; $i < count($layers); $i++) {
            imagecopy($thumb_image, $layers[$i], 0, 0, 0, 0, $width, $height);
        }
        imagealphablending($thumb_image, false);
        imagesavealpha($thumb_image, true);


        if(file_exists($path . $file)) unlink($path . $file);
        echo imagepng($image, $path . $file);
        // for thumbnail
        if(file_exists($path . "thumbnails/". $file)) unlink($path . "thumbnails/". $file);
        imagecopyresampled($thumb_image, $image, 0, 0, 26, 6, 250, 150, 597, 354);
        echo imagepng($thumb_image, $path . "thumbnails/". $file);
        
        exit;
        
    }
    
    function getAllCookie(){
    	
    	echo "<pre>";
    	print_r($_COOKIE);
    	exit;
    	
    }
    
    // clear session 'temp_sandwich_data' which holds all sandwich menuitem selections, prices data etc.
    function clear_temp_session(){
    	unset($_SESSION['temp_sandwich_data']);
    	exit;
    }

    //get all the sandwich categories
    function getSandwichCategories(){
        $model           = $this->model;
        $data     = $model->getSandwichCategories();

        print_r(json_encode($data));
        exit();
    }

    //to get the sandwich category items by category id
    function getSandwichCategoryItems(){

        $category_id = $_POST['category_id'];
        $model           = $this->model;
        $data     = $model->load_data( $category_id);

        print_r(json_encode($data));
        exit();
    }

    function getLastSandwichId()
    {
        $id = $this->model->getLastSandwichId($_POST['id']);
        print_r(json_encode($id));
        exit();
    }
    private function unlinkCacheFile()
    {
        $files1 = scandir(DIR_THEME.'/templates_c');
        $matches = preg_grep("/savedSandwiches/", $files1);
        if(!empty($matches))
        {
            $t = implode("", $matches);
            unlink(DIR_THEME.'/templates_c/'.$t);
        }
    }
}
