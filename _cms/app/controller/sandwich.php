<?php
// CONTROLLER SANDWICH - 

class Sandwich extends Base
{
	public $model;
	
	public function __construct(){
		$this->model = $this->load_model('sandwichModel');
	}
	
	function index()
	{	
		$this->title = TITLE;		
		$this->smarty->assign('SITE_URL',SITE_URL);
	}
	
	function gallery()
	{
		$this->title = TITLE;
	    $this->smarty->assign('SITE_URL',SITE_URL);
	    $this->smarty->assign('ADMIN_URL',ADMIN_URL);
	    $model = $this->model;
	    
	    $limit = 50;
	    $page = empty($_GET['page']) ? 1 : (int)$_GET['page'];
	    $start = $page == 1 ? 0 : (($page - 1) * $limit - 1);
	    
	    if(isset($_GET['sortid']))
	    	$sort_id=$_GET['sortid'];
	    else 
	    	$sort_id=0;
	    
	    $data['sort_id'] =  $sort_id;
	    $data['start'] =  $start;
	    $data['limit'] =  $limit;

	    $serch_term    = $_POST['search'];

	    	    	    
	    $link = SITE_URL."sandwich/gallery/?sortid=".$sort_id;
	    $this->smarty->assign('link',$link);
	    $this->smarty->assign('page',$page);
	   	if(!empty($serch_term))
	   	{
	   		$data = array('name'=>$serch_term, 
							'limit'=>$limit,
							'start'=>$start,
							'sort_id'=>$sort_id);
	   		$totalCount = $model->get_searchItem_count($data);
	   	}
	   	else
	   	{
	   		$data = array( 'limit'=>50,'start'=>0,'sort_id'=>$sort_id );
	   		$totalCount = $model->get_gallery_data_count($data);
	   	}
		if( $totalCount == '' || $totalCount == 'undefined' )
		{
	 		$totalCount = 0;
		}

	    $this->smarty->assign('totalCount',$totalCount);
	    
	    $lastPage = (int)($totalCount/$limit); 
	    $remain = $totalCount%$limit;
	    if($remain) $lastPage = $lastPage+1;
	    $this->smarty->assign('lastPage',$lastPage);
	    
	    $sandwiches = $model->gallery_load_data($data);


	    /* get saved sandwiches data based on uid */
	    $uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
		if ($uid) 
		{
            $getSavedSandwiches = $this->model->get_user_sandwich_ids($uid);
        } else {
            $getSavedSandwiches = array();
        }

        $is_public_array = array();
	     foreach ($getSavedSandwiches as $key => $value) {
	     	if($value['menu_added_from'] != 0)
	    		$is_public_array[$value['menu_added_from']] = $value['is_public'];
	    	else
	    		$is_public_array[$value['id']] = $value['is_public'];
         }

        $menu_added_from_array = array_column($getSavedSandwiches, 'menu_added_from');
        $id_array = array_column($getSavedSandwiches, 'id' );

        $getSavedSandwiches = array_merge($menu_added_from_array, $id_array);
      
	   
	    if(count($sandwiches)>0){  
	    	$sandwiches['data_from'] = 'gallery';
	     $gallery_datas = $model->process_user_data($sandwiches);
	     $gallery_data = $gallery_datas['data'];
	     unset($gallery_data['data_from']);
	    } else {
	    	$gallery_data = array();
	    }
	     //echo '<pre>';print_r($gallery_data);exit;
	    $order_data  = $this->model->get_order_data();

	    $categories=$model->getSandwichCategories();
	    $categoryItems=$model->getSandwichCategoryItems();
		   
	    $this->smarty->assign('categoryItems',$categoryItems);
	    $this->smarty->assign('categories',$categories);
	    $this->smarty->assign('sort_id',$sort_id);
	    $this->smarty->assign('serch_term',$serch_term);
	    
	    $this->smarty->assign('saved_data',$getSavedSandwiches);
	    $this->smarty->assign('is_public_array',$is_public_array);

	    $this->smarty->assign('order_data',$order_data);
	    $this->smarty->assign('GALLARY_DATA',$gallery_data);
	    
	    if(isset($gallery_datas['count'])) $this->smarty->assign('gal_count',$gallery_datas['count']);

	    $this->smarty->assign('LOGGED_IN',true);
	    $this->smarty->assign('total_item_count',$totalCount);
	    $this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
        $this->smarty->assign('content',"sandwich-gallery.tpl");
	}
	function filters()
	{
		//echo "<pre>";print_r($_POST);exit;
		$filter = @$_POST['filter'];
		$sName = @$_POST['searchTerm'];
		$filter = json_decode($filter);
		$sandwich_items = array(); 
		foreach ($filter as $key => $value) 
		{ 
		    if (is_array($value) && !empty($value)) 
		    { 
		      foreach ($value as $val) 
		      {
		      	$sandwich_items[] = $val;
		      }
		    } 

		}
		$data = array("s_items"=>$sandwich_items, "sortBy"=> @$_POST['sortBy']);
		if(!empty($sName))
		{
			$data['name'] = $sName;
		}
		
		$sandwich_data = $this->model->sandwich_filter($data);
		$sandwich_data_count = $this->model->sandwich_filter_count($data);
		
		 if (isset($sandwich_data) && count(@$sandwich_data) > 0) 
		 {
		 	$gallery_datas = (array) $this->model->process_user_data($sandwich_data);
		 	
		 	$gallery_data  = (array)$gallery_datas['data'];
			
		 } else {
		 	$gallery_data = array();
		 }
		$totalCount = $sandwich_data_count;
		if( $totalCount == '' || $totalCount == 'undefined' )
		 	$totalCount = 0;

		 $this->smarty->assign('sandwich_count', $totalCount );
		 $this->smarty->assign('GALLARY_DATA', $gallery_data);
		 $this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
		 echo $this->smarty->fetch("sandwich-gallery-ajax-search.tpl");
		 exit; 
	}

	function get_individual_sandwich_data()
	{
		$id   = $_REQUEST['id'];
		$uid  = $_POST['user_id'];

		$data = $this->model->get_individual_sandwich_data($id,$uid);

		$data = json_decode($data);
		
		if(isset($data->Data)){  $data = $data->Data;  } else { $data = array(); }
		echo json_encode($this->model->process_individual_sandwich_data($data));
		exit;
	}
		/**
	 *
	 * View sandwich gallery more
	 *
	 */
	function get_more_gallery()
	{
		//echo "<pre>";print_r($_POST);exit;
		$model           = $this->model;
		$data['sort_id']  = (!empty($_POST['sort_id']))?$_POST['sort_id']:$_POST['sortBy'];
		$data['name']  		= $_POST['searchTerm'];
		$count           = $_POST['count'];
		$limit           = 50;
		$page            = round($count / $limit);
		$data['limit']   = $limit;
		if ($page == 0)
			$data['start'] = $count;
		else {
			$data['start'] = $limit * $page;
		}
		if (!empty($_POST['filter']))
		{
			$filter = json_decode($_POST['filter']);
			$sandwich_items = array(); 
			foreach ($filter as $key => $value) 
			{ 
			    if (is_array($value) && !empty($value)) 
			    { 
			      foreach ($value as $val) 
			      {
			      	$sandwich_items[] = $val;
			      }
			    } 

			}
			$data['s_items'] = $sandwich_items;
			$sandwich_data = $model->more_sandwich_filter($data);
		}
		else
		{
			$sandwich_data = $model->gallery_load_data($data);
		}

		if (count($sandwich_data) > 0) {
			$gallery_datas = $model->process_user_data($sandwich_data);
			$gallery_data  = $gallery_datas['data'];
		} else {
			$gallery_data = array();
		}
		//$totalCount = $this->model->sandwich_filter_count($data);
		$this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
		$this->smarty->assign('GALLARY_DATA', $gallery_data);
		$this->smarty->assign('SITE_URL', SITE_URL);
		$this->smarty->assign('ADMIN_URL', ADMIN_URL);
		echo $this->smarty->fetch("sandwich-gallery-more.tpl");
		exit;
	}

	// CONTROLLER: SANDWICH. CMS.
	function add_to_cart()
	{
	
		if(!isset($_SESSION['uid'])) exit("0");
		
		 $model                = $this->model;
		 $item_id              = @$_POST['itemid'];
		 $data_type            = @$_POST['data_type'];
		 $user_id              = @$_POST['user_id'];
		if(!$user_id){
			$user_id = $_SESSION['uid'];
		}	 
		$toast=0;
		 if(isset($_POST['toast'])){
		 	$toast=$_POST['toast'];
		 }
		 
		 $qty = @$_POST['qty'];
		 
		$descriptor_id = $extra_id = "";
		$spcl_instructions ="";
		 
		
		if(isset($_POST['spcl_instructions']))
			$spcl_instructions = $_POST['spcl_instructions'];

		if(isset($_POST['clickedItems'])){
			
			$extra_id = $_POST['clickedItems'];
			
		}

		
		$item = $model->create_order($item_id, $user_id, $data_type, $qty, $toast, $spcl_instructions, $extra_id);
		
		 if($item){
		 	
		 	$_SESSION['orders']['item_id'][]  = $item_id;
		 	$_SESSION['orders']['order_item_id'][]  = $item->order_id;
		 	$_SESSION['orders']['item_qty'] = $item->count;
		 
		 }
		 
		 if(isset($_SESSION['orders']['item_qty'])) {
			echo $_SESSION['orders']['item_qty'];
		 }
		 else { echo 0;
		 }
		exit;
	}
	
	function remove_cart_item()
	{  
		
		$model  = $this->model;
		$order_item_id = @$_POST['order_item_id'];
		$item_id = @$_POST['itemid'];
		 
		$result = $model->remove_order($order_item_id,$_SESSION['uid']);
		
		if($result){
				
				if(isset($_SESSION['orders']['item_id'])){ 
					
					if(in_array($item_id,@$_SESSION['orders']['item_id'])){
						$key = array_search($item_id, $_SESSION['orders']['item_id']);
						unset($_SESSION['orders']['item_id'][$key]) ; 
					}
				} else { 
					 
				}
				 
				 
				if(isset($_SESSION['orders']['order_item_id'])){
					 
					if(!in_array($order_item_id,@$_SESSION['orders']['order_item_id'])){
						$key = array_search($order_item_id, $_SESSION['orders']['order_item_id']);
						unset($_SESSION['orders']['order_item_id'][$key]) ; 
					}
				}
				
			
		} 		
		
		
		
		 if(isset($_SESSION['orders']['item_id'])) {
			echo count($_SESSION['orders']['item_id']);
		 }
		 else { echo 0;
		 }
		exit;
	}
	
	function remove_menu()
	{
		
		$model  = $this->model;
		$data   = array('id'=>$_POST['id'],'uid'=>$_POST['id']);
		$model->remove_menu($data);
		exit;
	}

	function remove_saved()
	{
		
		$model  = $this->model;
		$data   = array('id'=>$_POST['id'],'uid'=>$_POST['uid'],'sType'=>$_POST['sType']);
		$stype = $model->remove_saved($data);
		echo $stype;
		exit;
	}
	
	
	function add_menu_multi()
	{
		$model  = $this->model;
		
		if(!$_POST['uid'])
		{
			$_POST['uid'] = $_SESSION['uid'];
		}
		$data   = array('id'=>$_POST['id'],'uid'=>$_POST['uid'],'toast_sandwich' => $_POST['toast_sandwich']);
		$model->add_menu_multi($data);
		exit;
	}
	
	function make_sandwich_private()
	{
		
		$model  = $this->model;
		$data   = array('id'=>$_POST['id'],'uid'=>$_POST['uid'],'is_public' => $_POST['is_public'],'sType'=>$_POST['sType']);
		$model->make_sandwich_private($data);
		exit;
	}

	function get_cart_count()
	{
		if(isset($_SESSION['orders']['item_id'])) { echo count($_SESSION['orders']['item_id']);  }
		else { echo 0; }  
		exit;
	}
	
	
	function show()
	{
		print_r($_SESSION['orders']); exit;
	}
	
	function addLike()
	{
		$model  = $this->model;
		$data   = array('id'=>$_POST['id'],'uid'=>$_POST['uid']);
		echo $model->addLike($data);
		exit;
	}
	
	function set_flag()
	{
		$model                = $this->model;
        
        $model->send_flag_mail();
		echo $model->toggle_flag();
		exit;
	}
	function updateToastmenu()
	{
		$model  = $this->model;
		$data   = array('item_id'=>$_POST['item_id'],'toast'=>$_POST['toast']);
		echo $model->updateToastmenu($data);
		
		exit;
	}
	
	function getAllproductsExtras()
	{
		$model        = $this->model;
		$model->getAllproductsExtras();
		exit;
	}

	function featuredSandwiches()
	{
		$this->smarty->assign('LOGGED_IN',true);
	    $this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
	    $this->smarty->assign('SITE_URL',SITE_URL);
		$model        = $this->model;
		$sandwich_data = $model->getSandwichOfTrending();
		$uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
		if ($uid) 
		{
           // $menuData = array('uid'=>$uid, 'start' => 0, 'limit' => '' ); 
            $getSavedSandwiches = $this->model->get_user_sandwich_ids($uid);
        } else {
            $getSavedSandwiches = array();
        }
        

        $menu_added_from_array = array_column($getSavedSandwiches, 'menu_added_from');
        $id_array = array_column($getSavedSandwiches, 'id' );
        //$is_public_array = array_column($getSavedSandwiches, 'id' ,'is_public');
        $is_public_array = array();
         foreach ($getSavedSandwiches as $key => $value) {
         	if($value['menu_added_from'] != 0)
        		$is_public_array[$value['menu_added_from']] = $value['is_public'];
        	else
        		$is_public_array[$value['id']] = $value['is_public'];
        }

        $getSavedSandwiches = array_merge($menu_added_from_array, $id_array);

		if(count($sandwich_data)>0){  
	     $featured_datas = $model->process_user_data($sandwich_data);
	     $featured_data = $featured_datas['data'];
	    } else {
	    	$featured_data = array();
	    }
	    
	    $this->smarty->assign('featured_data',$featured_data);
	    $this->smarty->assign('saved_data',$getSavedSandwiches);
	    $this->smarty->assign('is_public_array',$is_public_array);
		
        $this->smarty->assign('content',"featured_sandwiches.tpl");
	}
	function savedSandwiches()
	{
		$uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
		if ($uid) 
		{
            $menuData = array('uid'=>$uid, 'start' => 0, 'limit' => '' );
            $getSavedSandwiches = $this->model->get_user_sanwich_data($menuData);
        } else {
            $getSavedSandwiches = array();
        }
        
        if(count($getSavedSandwiches)>0){  
	     $saved_sandwich_datas = $this->model->process_user_data($getSavedSandwiches);
	     $saved_sandwich_data = $saved_sandwich_datas['data'];
	    } else {
	    	$saved_sandwich_data = array();
	    }
	   	// echo "<pre>";print_r($saved_sandwich_data);exit;
		$this->smarty->assign('LOGGED_IN',true);
	    $this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
	    $this->smarty->assign('featured_data',$saved_sandwich_data);
        $this->smarty->assign('content',"user_sandwiches.tpl");
	}

	function sandwichMenu()
	{
		$this->title = TITLE;
	    $this->smarty->assign('SITE_URL',SITE_URL);
	    $this->smarty->assign('time',"S MENU");
		$this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);

		$galleryCount = $this->model->get_sandwich_shared_creations_count();
		$this->smarty->assign('galleryCount', number_format($galleryCount));

		$menuModel = $this->load_model('menuModel');


		$uid = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];

		if ($uid) {
			$paramData = array('uid'=>$uid, 'start' => 0, 'limit' => 10 );


			$getSavedSandwiches= $this->model->get_user_sanwich_data($paramData);
			$getSavedSandwichIds = $this->model->get_user_sandwich_ids($uid);
	    }
        else {
            $getSavedSandwiches = array();
            $getSavedSandwichIds = array();
        }

        $menu_added_from_array = array_column($getSavedSandwichIds, 'menu_added_from');
        $id_array = array_column($getSavedSandwichIds, 'id');

        $getSavedSandwichIds = array_merge($menu_added_from_array, $id_array);

       if(count($getSavedSandwiches)>0){  
	     $saved_sandwich_datas = $this->model->process_user_data($getSavedSandwiches);
	     $saved_sandwich_data = $saved_sandwich_datas['data'];
	    } else {
	    	$saved_sandwich_data = array();
	    }

	    //to get featured sandwiches
	    $trndData = array('limit' => 10 );
	    $sandwich_data = $this->model->getSandwichOfTrending($trndData);
		if(count($sandwich_data)>0){  
	     $featured_datas = $this->model->process_user_data($sandwich_data);
	     $featured_data = $featured_datas['data'];
	    } else {
	    	$featured_data = array();
	    }

	    
        if(isset($_SESSION['uid']))
        {

        	$fdata = $this->model->getFBfriendsMenu();
        	
	        if (isset($fdata->Data)) 
	        {
	            $fdata = $this->model->_process($fdata->Data);
	        }
	        
	    }
        else
        {
        	$fdata = array();
        }
        
        $this->smarty->assign('saved_sandwich_data',$saved_sandwich_data);
        $this->smarty->assign('featured_data',$featured_data);
        $this->smarty->assign('saved_sandwich_ids',$getSavedSandwichIds);
        $this->smarty->assign('fbdata', $fdata);

        $this->smarty->assign('content',"sandwich_menu.tpl");
	}
	/*
	*
	to dump sandwichdata into new colum
	*/


	function getCurrentprice(){
		$desc = $_POST['pdata']['sandwich_desc'];
		$desc_id = $_POST['pdata']['sandwich_desc_id'];
		$brd = $_POST['pdata']['bread'];
		$pid = $_POST['pdata']['id'];
		$price = $this->model->get_sandwich_current_price($pid, $brd,$desc,$desc_id);

		echo $price;exit;
	}

	function dumpData()
	{
		$res = $this->model-> getAllSandwiches();
		foreach ($res as $data) 
		{
			$sData = json_decode($data['sandwich_data']);
			$sData = json_decode(json_encode($sData), true);

			$final = array_column($sData, 'item_name');
			$final_array = array();
			$i = 0;
			foreach ($final as $key => $value) 
			{
				if(!count($value))
				{
					if($i == 1)
					{
						$final_array = array_merge($final_array, array("No Protein"));
					}
					else if($i == 2)
					{
						$final_array = array_merge($final_array, array("No Cheese"));
					}
					else if($i == 3)
					{
						$final_array = array_merge($final_array, array("No Toppings"));
					}
					else if($i == 4)
					{
						$final_array = array_merge($final_array, array("No Condiments"));
					}
				}else
				{
					$final_array = array_merge($final_array, $value);
				}
				$i++;
			}
			$sandwich_items = implode(',',$final_array);
			$this->model->updateData($sandwich_items,$data['id']);
		}

		echo "Successfully Dumped";exit;
	}

	function thumbnailCreate()
	{
		$path = SANDWICH_UPLOAD_DIR;
		$results = scandir($path);
		foreach ($results as $result) 
		{
		    if ($result === '.' or $result === '..') continue;
		    if (is_dir($path . '/' . $result))
		    {
		        $folderPath = $path.$result;
		        $thm = scandir($folderPath);
		        //echo "<pre>";print_r(empty($thm[2]));exit;
		        if(!empty($thm[2]))
		        {
					$tmp = array_search("thumbnails",$thm);
					if (empty($tmp))mkdir($folderPath."/thumbnails", 0777, true);
				    
		    		$supported_image = array('png');
		    		foreach ($thm as $thumb) 
					{
					    if ($thumb === '.' or $thumb === '..') continue;
					    if(!file_exists(CMS_URL.'sandwich_uploads/'.$result."/thumbnails/".$thumb))
					    {
					    	$ext = strtolower(pathinfo($thumb, PATHINFO_EXTENSION));
						    if (in_array($ext, $supported_image)) 
							{
								$source = imagecreatefrompng(CMS_URL.'sandwich_uploads/'.$result."/".$thumb);
								$nthumb = imagecreatetruecolor(250, 150);
								imagealphablending($nthumb, false);
						        $transparency = imagecolorallocatealpha($nthumb, 0, 0, 0, 127);
						        imagefill($nthumb, 0, 0, $transparency);
						        imagesavealpha($nthumb, true);						
								imagecopyresized($nthumb, $source, 0, 0, 26, 6, 250, 150, 597, 354);
						        imagepng($nthumb, SANDWICH_UPLOAD_DIR.$result."/thumbnails/".$thumb);
				    		}
					    }    
					}
		        }		
		    }
		}

		echo "Success";exit;
		
	}

}
