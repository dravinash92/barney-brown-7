<?php
session_start();

require_once 'fb_sdk_v5/src/Facebook/autoload.php';


use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\Entities\AccessToken;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;


class FB_API 
{
    public  $scope      =  array('email,user_friends'); //publish_actions
    public  $helper;
    public  $redirURI   = FB_REDIR_URL;
    public  $appid      = FB_APP_ID; 
    public  $secret     = FB_APP_SECRET;
    public  $isValidTkn = false;
    public  $redirpaths = '';
    private $fb = null;
    
    
    public function setSession()
    {
    	# The v5 new-hotness way
      $this->fb = new Facebook\Facebook([
          'app_id'     =>  $this->appid ,
          'app_secret' =>  $this->secret ,
          'default_graph_version' => 'v2.5'
      ]);
    }

    private function check_FB_status( )
    {
        if(!$this->fb)
            $this->setSession(  );
    }

    public function FB_get_url(  )
    {
        $this->check_FB_status( );
        $helper = $this->fb->getRedirectLoginHelper();
        $permissions = ['email', 'user_posts']; // optional
        $loginUrl = $helper->getLoginUrl( $this->redirURI, $permissions );
        return $loginUrl;
    }

    public function FB_check_login()
    {
        $this->check_FB_status( );
        $helper = $this->fb->getRedirectLoginHelper();
        try
        {
            $accessToken = $helper->getAccessToken();
        } 
        catch( Facebook\Exceptions\FacebookSDKException $e ) 
        {
            echo $e->getMessage();
            exit;
        }

        if (isset($accessToken)) 
        {
            // User authenticated your app!
            // Save the access token to a session and redirect
            $_SESSION['access_token'] = (string) $accessToken;
            // Log them into your web framework here . . .
            // Redirect here . . .
            return true;
        }
        elseif ($helper->getError()) 
        {
            var_dump($helper->getError());
            return false; 
        // The user denied the request
        // You could log this data . . .
        /*var_dump($helper->getError());
        var_dump($helper->getErrorCode());
        var_dump($helper->getErrorReason());
        var_dump($helper->getErrorDescription());
        // You could display a message to the user
        // being all like, "What? You don't like me?"
        exit;*/
        }

      // If they've gotten this far, they shouldn't be here
    //http_response_code(400);
    echo "Fatal error...off the beaten path";
    exit;
}


function get_logout_URL()
{

/*   FacebookSession::setDefaultApplication($this->appid,$this->secret);
   if(isset($_SESSION['access_token']) && $_SESSION['access_token']){
      $session = new FacebookSession($_SESSION['access_token']);
      $this->helper = new FacebookRedirectLoginHelper($this->redirURI);
      $this->helper->disableSessionStatusCheck();
      return $logoutUrl = $this->helper->getLogoutUrl($session, SITE_URL.'myaccount/signout');
  } else {
      return SITE_URL.'myaccount/signout';
  }
*/
}


    function FB_create_accessToken()
    {
        $this->check_FB_status( );
        $helper = $this->fb->getCanvasHelper();
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // There was an error communicating with Graph
          // Or there was a problem validating the signed request
            echo $e->getMessage();
            exit;
        }
        if ($accessToken) {
         // Logged in.
            $_SESSION['access_token'] = (string) $accessToken;
        } 
    }


    function FB_is_valid_token($token)
    {
        $this->check_FB_status( );
        $helper = $this->fb->getCanvasHelper();

        if( !$token )
          return false;
        try
        {
            $this->fb->setDefaultAccessToken($token);
        }
        catch(Facebook\Exceptions\FacebookResponseException $e) 
        {
            echo 'Facebook Response error: ' . $e->getMessage();
            return false;
        }
         catch(Facebook\Exceptions\FacebookSDKException $e) 
        {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }
        if( $this->fb->getValue() )
            return true;
    }

    //for retriveing user date
    function FB_get_user($token)
    {
        $this->check_FB_status( );
        try 
        {
            $request = $this->fb->get('/me');
        }
        catch(Facebook\Exceptions\FacebookResponseException $e) 
        {
            // When Graph returns an error
            if ($e->getCode() == 190) 
            {
                unset($_SESSION['access_token']);
                //$helper = $fb->getRedirectLoginHelper();
                //$loginUrl = $helper->getLoginUrl('https://apps.facebook.com/APP_NAMESPACE/', $permissions);
                return false;
            }
            return false;
        } 
        catch(Facebook\Exceptions\FacebookSDKException $e) 
        {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }
        // getting basic info about logged-in user
        try {
            $request = $fb->get('/me?fields=first_name,last_name,email,id,friends');
            $data = $request->getGraphNode()->asArray();
        }
        catch(Facebook\Exceptions\FacebookResponseException $e) 
        {
            echo 'Returned an error: ' . $e->getMessage();
            return false;
        }
        catch(Facebook\Exceptions\FacebookSDKException $e) 
        {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }
        echo $profile['name'];
        foreach ($data['friends'] as $friend) 
        {

            $list['user_frnd_count'] = $data['friends']->summary->total_count;
            foreach ($friend as $fr) 
            {
                if (is_object($fr)) 
                {
                    if ($fr->name) 
                    {
                        $list['name'][] = $fr->name;
                    }
                    if ($fr->id) {
                        $list['id'][] = $fr->id;
                    }
                }
            }
        }

        $main['uid']               = $data['id'];
        $main['first_name']        = $data['first_name'];
        $main['last_name']         = $data['last_name'];
        $main['email']             = $data['email'];
        $main['friend_data'] = $list;

        return $main;
    }


   //for posting data to facebook 
    public  function FB_post_fb( $token, $supplied_post_array )
    {
        $this->check_FB_status( );
        $valid_fields = array( 'message', 'link','place','tags','privacy', 'object_attachment');

        if( $this->FB_is_valid_token($token) !== true )
            return false;
        
        $validated_post_array = array(  );
        foreach( $supplied_post_array as $k=>$v )
        {
            if( inarray($k, $valid_fields ) )
                $validated_post_array[$k] = $v;
        }
     
       try
       {
            $response = $this->fb->post( '/me/feed', $validated_post_array, $token  );
        }

        catch(Facebook\Exceptions\FacebookResponseException $e) 
        {
            echo 'Graph returned an error: '.$e->getMessage();
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) 
        {
            echo 'Facebook SDK returned an error: '.$e->getMessage();
            return false;
        }
        $graphNode = $response->getGraphNode();

        if(isset($response)){

            if(is_object($response)){	
                return $response->getProperty('id');

            } else { return false; }
            } else return false; 
    }
}
