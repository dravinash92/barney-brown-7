<?php 

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 0);

class Model extends INSPIRE {
  public $db;
  public function __construct(){
  //setting up db object
  return $this->create_db();
  }

 //create db connection
  public function create_db(){
	require(DIR_CONF.'conf.my_db.php');
        return $db = new BaseClass($b_type,$Cfg_host,$Cfg_user,$Cfg_password,$Cfg_db);
  }
  
  
  public function receive_data($url,$params=array(), $test= 0 ){
   	$params['api_username'] = API_USERNAME;
  	$params['api_password'] = API_PASSWORD;
  	$params['ENVIRONMENT']  = ENVIRONMENT;

  	$params                  = $this->process_input($params);
  	 //url-ify the data for the POST
    $params = http_build_query($params);
    $ch = null;
 

  	//invoking curl
  	$ch = curl_init();

    if(  $test == 1 )
      curl_setopt($ch, CURLOPT_VERBOSE, 1);


  	//set the url, number of POST vars, POST data
  	curl_setopt($ch,CURLOPT_URL,$url);
  	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  	curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
    curl_setopt($ch,CURLINFO_HEADER_OUT, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'BB_DATA: cms'
      ));

  	//execute post
    $result = curl_exec($ch);
    //close connection
  	$info = curl_getinfo( $ch );
    if(  $test == 1 )
    {

		$error =  curl_error($ch);
		}
    curl_close($ch);


   
    if(  $test == 1 )
    {
      print "<pre>\n";
      print " URL : $url\n";
      print "POST PARAMS - \n";
      $params_for_post =  $this->stringify_post_params( $params );
      print "$params_for_post\n";
      print "\nREQUEST:\n";
			var_dump($error);
      var_dump($info);
      print "\n" . "-"*80  . "\n";

    }

    return $result;
  }

  private function stringify_post_params( $paramArray )
  {
    $buffer = array(  );
    foreach( $paramArray as $key=>$val )
    {
      array_push( $buffer, "$key=$val" );
    }
    $ret = implode(  "&", $buffer );
    return $ret;
  }
    
  public function pp( $x, $quit = false )
  {
    print "<pre>";
    
    if( is_array( $x ) )
      var_dump( $x  );
    else
      print $x;
    
    print "</pre>";
    if( $quit == true )
      exit(  );
  }


  public function sendMail($params){
  
  	if(!isset($params['from'])) $params['from'] = 'noreply@cms.ha.allthingsmedia.com';
  
  	$to      = $params['to'];
  	$subject = $params['subject'];
  	$message = $params['message'];
  	$headers  = array(
  
  			"From: ".TITLE." <".$params['from'].">",
  			//"Reply-To: ". $params['from'],
  			"X-Mailer: PHP/" . phpversion(),
  
  			//"X-Originating-IP: ". $_SERVER['SERVER_ADDR'],
  			"MIME-Version: 1.0",
  	);
  	// Send
  	if(	mail($to, $subject, $message, implode("\r\n", $headers) ) ) return true; else  return false;
  
  
  
  }
  
  /*
   * mobile device detection code. 
   * */
  public static function isMobile() {
  	$userAgent = $_SERVER["HTTP_USER_AGENT"];
  	return preg_match("/(iPhone|iPod|blackberry|Android|BlackBerry)/i",$userAgent);
  }
  
  private function process_input($data){
  	 foreach($data as $key => $value){
        //If $value is an array.
        if(is_array($value)){
            //We need to loop through it.
            $data[$key] = $this->process_input($value);
        } else{
            $data[$key] = urlencode($value);
        }
    }
    return $data;
  }

  public function clean_for_sql( $var )
    {
        return mysqli_real_escape_string($this->db->connection, $var );
    }

}