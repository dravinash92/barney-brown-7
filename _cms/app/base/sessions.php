<?php 
function _open() {
	global $Cfg_host;
	global $Cfg_user;
	global $Cfg_password;
	global $Cfg_db;	
    global $_sess_db;
		
    if ($_sess_db = mysqli_connect($Cfg_host, $Cfg_user, $Cfg_password)){
		
		return  mysqli_select_db($_sess_db, $Cfg_db);
	}
	
	return FALSE;
}
 
function _close()
{
	global $Cfg_host;
	global $Cfg_user;
	global $Cfg_password;
	global $Cfg_db;	
    global $_sess_db;
 	
	if($_sess_db){
      mysqli_close($_sess_db);
	}
	
}

function _read($id)
{
    global $_sess_db;
    global $Cfg_db;
    $id = $this->clean_for_sql($id);
 
    $sql = "SELECT data
            FROM   sessions
            WHERE  id = '$id'";
 	if ($result = mysqli_query($_sess_db,$sql)) {
        if (mysqli_num_rows($result)) {
            $record = mysqli_fetch_assoc($result);
 
            return $record['data'];
        }
    }
 
    return '';
}

function _write($id, $data)
{
	global $Cfg_host;
	global $Cfg_user;
	global $Cfg_password;
	global $Cfg_db;	
    global $_sess_db;
 	
    $access = time();
	
    $id = $this->clean_for_sql($id);
    $access = $this->clean_for_sql($access);
    $data = $this->clean_for_sql($data);
 	
    $sql = "REPLACE INTO sessions VALUES  ('$id', '$access', '$data')";
	
    return mysqli_query($_sess_db, $sql);
 		
}


function _destroy($id)
{
	global $Cfg_host;
	global $Cfg_user;
	global $Cfg_password;
	global $Cfg_db;	
    global $_sess_db;
 
    $id = $this->clean_for_sql($id);
 
    $sql = "DELETE FROM sessions WHERE  id = '$id'";
 
    return mysqli_query($_sess_db, $sql);
}

function _clean($max)
{
	global $Cfg_host;
	global $Cfg_user;
	global $Cfg_password;
	global $Cfg_db;	
    global $_sess_db;	
 
    $old = time() - $max;
    $old = $this->clean_for_sql($old);
 
    $sql = "DELETE FROM sessions WHERE access < '$old'";
 	
    return mysqli_query($_sess_db, $sql);
}
    	    
// Set the save handlers
session_set_save_handler('_open', '_close', '_read', '_write', '_destroy', '_clean');

//session_start();
?>