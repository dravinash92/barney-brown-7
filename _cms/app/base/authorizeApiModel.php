<?php
include_once( DIR_BASE.'customExceptionTransaction.php' );
class authorizeApiModel extends Model
{
    public $db; //database connection object

    private $test_mode;
    private $log_path = 'app/log/';
    private $post;

    private $testResponseOverride = null;
    /**
     * invoke database connection object
     */
    public function __construct() {
        $this->db = parent::__construct();
        $this->authObject = new authorizenetCIM();
        set_error_handler( array($this, "exception_error_handler"), E_ALL & ~E_NOTICE & ~E_USER_NOTICE & ~E_WARNING ) ;
    }

    function exception_error_handler($errno, $errstr, $errfile, $errline ) {
        if(  $errno == E_NOTICE ||   $errno == E_USER_NOTICE ||         $errno == E_WARNING ||       $errno == E_USER_WARNING  || $errno == E_DEPRECATED )
        {
         //   throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
        }
        else    
            throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
    }

    public function setTestResponseOverride( $xml )
    {
        $this->testResponseOverride = $xml;
        $this->authObject->setTestResponseOverride( $xml );
    }

    public function clearTestResponseOverride(  )
    {
        $this->testResponseOverride = null;
        $this->authObject->clearTestResponseOverride(  );
    }

    /**
     * Entry point for edit/add card from MyAccount Controller
     * @param  [type] $post [description]
     * @return [type]       [description]
     */
    public function cardAddEditEvent( $post )
    {
        $this->post = $post;
        return $this->do_controller_action( $post, 'cardAddEditEvent_wrapped' );
    }

    /**
     * Entry point for the credit card add/edit via the CHECKOUT controller.
     * @param  array    $post 
     * @return int       credit card insert ID from orders_billinginfo
     */
    public function savebillinginfo( $post ) 
   {
        return $this->do_controller_action( $post, 'savebillinginfo_wrapped' );
   }

    /**
     * Entry point for cardRemoveEvent - deletes card via the myAccount controller
     * @param  array    $post 
     * @return boolean  true if works.
     */
    public function cardRemoveEvent($post)
    {
        return $this->do_controller_action( $post, 'cardRemoveEvent_wrapped' );
    }


    /**
    * The entry point to conduct the actual transaction.  
    * @param  array     $post  
    * @return int       Transaction ID
    */
    public function insert_checkout_placeorder_data( $post )
    {
        $order_id = $this->do_controller_action( $post, 'do_order' );
        return $order_id;
    }


    /**
    * The entry point to do the admin transaction.  This code uses the exact same code pretty much as standard order.   
    * @param  array     $post  
    * @return int       Transaction ID
    */
    public function insert_admin_checkout_placeorder( $post )
    {
        return $this->do_controller_action( $post, 'do_order' );
    }

            
    private function issue_response( $status_code, $channel, $exception, $mysqlLogId = null )
    {
        $code = $e_transaction->getCode(  );
        $text = $e_transaction->getMessage(  );
        $file = $e_transaction->getFile(  );
        $line = $e_transaction->getLine(  );
        $fl = "$file:$line";
        $platform = htmlspecialchars( $this->platform_from_header(  ) );
        $dbIndex = "-";
        
        if( $mysqlLogId ) $dbIndex = $mysqlLogId;

        $resp['response']['status_code'] = "$status_code";
        $resp['response']['message'] = $text;
        $resp['data'] = array( 'error_code'=>$code, 'log_channel'=>$channel, 'location'=>$fl, 'platform'=>$platform,  'index'=>$dbIndex );

        return json_encode( $resp );
    }

    /**
     * The core of the module; every call from the big 4 functions runs through this function.  
     * all errors wind their way up to this module and it logs results as appropriate.
     * @param  array              $post     
     * @param  array|callback     $function       array( $this, "name of function" )
     * @return various            Generally returns a string.
     */
    private function do_controller_action( $post, $function )
    {
        $this->test_mode = false;
        $this->post = $post;
        $platform = $this->platform_from_header(  );
        $function_name_root = preg_replace( '/_wrapped/', '', $function );

        try
        {
            $this->save_uid( $post );

            // DO IT!!!  The core command. 
            $result = $this->$function( $post );    
            
            // If we get here we had no problems...           
            $transaction_log_id = @$this->log_transaction_db( 'OK', $function );  
            $this->log_success( "Transaction OK: Transaction log id:$transaction_log_id - Platform:$platform - Order ID: $result", 'transaction' );
            
            // Tell requester that all is good and return the info it needs. 
            echo $this->issue_response( '200', 'OK', $result );
            return $result;
        }
        catch( CustomExceptionTransaction $e_transaction )  // DECLINED or transaction faild in some way.
        {
            $logChannel = "Transaction:$function";
            $transaction_log_id = @$this->log_transaction_db( 'TRANSERR', $function );
            
            $this->log_error( $e_transaction, $logChannel, $transaction_log_id );
            echo $this->issue_response( '204', $logChannel, $e_transaction, $transaction_log_id );
            exit(  );
        }
        catch( CustomExceptionApi $e_api )  // Error in the API, something happend generally with a non-transaction but in logic
        {
            $logChannel = "API:$function";
            $transaction_log_id = @$this->log_transaction_db( 'APIERR', $function );
            
            $this->log_error( $e_api, $logChannel, $transaction_log_id );
            echo $this->issue_response( '204', $logChannel, $e_api, $transaction_log_id );

            exit(  );
        }

        catch( customException $e_error )    // System error.  Forgot a semicolon or something.
        {
            $logChannel = "RUNTIME:$function";
             
            $this->log_error( $e_error, $logChannel );
            echo $this->issue_response( '204', $logChannel, $e_error ); 
            exit(  );
        }

        catch( Exception $e_error )    // Generic Catch All
        {
            $logChannel = "EXCEPTION:$function";
             
            $this->log_system_error( $e_error, $logChannel );
            echo $this->issue_response( '204', $logChannel, $e_error ); 
            exit(  );
        }
    }

    private function save_uid(  $post )
    {
        if( isset(  $post['user_id'] ) ) $this->user_id = $post['user_id'];
        else if( isset(  $post['uid'] ) ) $this->user_id = $post['uid'];
        else if( isset(  $_SESSION['uid'] ) ) $this->user_id = $post['uid'];
        else if( isset(  $_SESSION['user_id'] ) ) $this->user_id = $post['uid'];
        //else throw new CustomException( ORDER_ERROR_EMPTY_VARIABLE, 'User ID' );
    }

   /* *****     *****       *****       *****
    *   
    *           logging Functions           *
    *           
    * *****     *****       *****       *****/
   private function platform_from_header(  )
   {
        $headers = apache_request_headers();
        if( isset( $headers['BB_DATA'] ) )
        {
            $platform = $headers['BB_DATA'] ;
        }
        else
        {
            $platform = "N/A";
        }

        $platform = $this->clean_for_sql( $platform );
        return $platform;
   }

   private function doLog( $level, $textArr )
   {
        $path  = $this->log_path;
        
        $file_date = "LOG__". date( "Y-m-d" );

        $file_result =  $path."LOG__$level.log";
        $file_combined =  $path."LOG__ALL.log";
        $file_by_date = "$path/DAILY/$file_date.log";

        $write_singular = json_encode( $textArr , JSON_PRETTY_PRINT );
        
        $write_combined = json_encode( array("RESULT"=>$level, 'LOG'=>$textArr ) );


        file_put_contents( $file_combined, $write_combined, FILE_APPEND );
        file_put_contents( $file_by_date, $write_singular, FILE_APPEND);
        file_put_contents( $file_result, $write_singular, FILE_APPEND);
   }

   /**
    * Saves the transaction result into database.  
    * @param  string     $result    The result of the transaction..
    * @return Returns the insert ID
    */
    private function log_transaction_db( $result, $function )
    {
        $user_id = $this->user_id;
        if(  $this->authObject->get_parsed_response(  )  )
            $parsedresponse = get_object_vars( $this->authObject->get_parsed_response(  ) );
        else
            $parsedresponse = 'NO RESPONSE';
        $parsedresponse = mysql_escape_string( json_encode( $parsedresponse ) );
        $platform = $this->clean_for_sql( $this->platform_from_header(  ) ).":".$function;

        //$result   = $this->post['platform'];
        if( $result == 'OK' && $this->order_number )
            $order_id = "'" . $this->order_number . "'";
        else
            $order_id = "NULL";


        $query = "INSERT INTO `transaction_log` ( `user_id`, `datetime`, `raw_response`, `order_id`, `platform`, `function`, `result` ) VALUES 
                    ( '$user_id' , NOW(), '$parsedresponse', $order_id, '$platform', '$function', '$result' ) ";

       // print "$query\n\n";

        $this->db->Query($query);    
        return $this->db->insertId( );
    }


    private function log_success( $text, $channel  )
    {
       
       if( isset( $this->post['card_number'] ) )  $this->post['card_number'] = $this->mask_card( $this->post['card_number'] );
        if( isset( $this->post['credit_card'] ) )  $this->post['credit_card'] = $this->mask_card( $this->post['credit_card'] );
        if( isset( $this->post['_card_number_auth'] ) )  $this->post['_card_number_auth'] = $this->mask_card( $this->post['_card_number_auth'] );
        
        $log_string = array(  );
        $log_string['datetime'] =  date( "Y-m-d H:i:s",time( ) );
        $log_string['post'] = var_export( $this->post, true );
        $log_string['platform'] = $this->platform_from_header(  );
        $log_string['UID'] = $this->user_id;
        $log_string['channel'] = $channel;

        $this->doLog( "SUCCESS", $log_string );
    }

    private function log_system_error( $exception, $channel )
    {
        $file = $this->log_path . "SYS_ERROR__".$action .".log";
        
        if( isset( $this->post['card_number'] ) )  $this->post['card_number'] = $this->mask_card( $this->post['card_number'] );
        if( isset( $this->post['credit_card'] ) )  $this->post['credit_card'] = $this->mask_card( $this->post['credit_card'] );
        if( isset( $this->post['_card_number_auth'] ) )  $this->post['_card_number_auth'] = $this->mask_card( $this->post['_card_number_auth'] );
        
        $log_string = array(  );
        $log_string['datetime'] =  date( "Y-m-d H:i:s",time( ) );
        $log_string['post'] = var_export( $this->post, true );
        $log_string['platform'] = $this->platform_from_header(  );
        $log_string['UID'] = $this->user_id;
        $log_string['channel'] = $channel;
        $log_string['error']['trace'] = $this->error_format_backtrace( $exception );
        $log_string['error']['message'] = $exception->getMessage(  ) ;
        $log_string['error']['code'] = $exception->getCode(  );
        $log_string['error']['location'] = $exception->getFile(  ) .":".$exception->getLine(  );
        
        $this->doLog( "ERROR_SYSTEM", $log_string );
    }


    public function log_error( $exception, $channel, $mysql_id = null )
    {
        $platform = $this->platform_from_header(  );
        
        $file = $this->log_path . "ERROR__" .$channel .".log";
       
        // Don't push cc's onto the web server log files!
        if( isset( $this->post['card_number'] ) )  $this->post['card_number'] = $this->mask_card( $this->post['card_number'] );
        if( isset( $this->post['credit_card'] ) )  $this->post['credit_card'] = $this->mask_card( $this->post['credit_card'] );
        if( isset( $this->post['_card_number_auth'] ) )  $this->post['_card_number_auth'] = $this->mask_card( $this->post['_card_number_auth'] );
       
        $log_string = array(  );
        $log_string['datetime'] =  date( "Y-m-d H:i:s",time( ) ); 
        $log_string['post'] = var_export( $this->post, true );

        // This would be the transaction id, only if we have it from a database. 
        if(  $mysql_id ) $log_string['transactionId'] = $mysql_id;
        $exceptionArray = $exception->getErrorArray(  );

        @$log_string['platform'] = $platform;
        @$log_string['UID'] = $this->user_id;
        @$log_string['channel'] = $channel;
        @$log_string['errorInfo'] = $exceptionArray;
        @$log_string['authNet'] = 
            array( 
                "address"=> var_export( $this->authObject->get_address(  ), true ),
                'response'=> var_export( $this->authObject->getParsedResponse(  ), true ),
                'request' => var_export( $this->authObject->getRequest(  ), true ) );

        $this->doLog( "ERROR_TRANSACTION", $log_string );
    }

    public function mask_card( $card )
    {
        if( !$card )
            return "####-####-####-none";

        if(  strlen( $card ) == 16 ){
            $card = "####-####-####-".substr( $card, -4 );
        }
        else
        {
            $card = "####-####-".substr( $card, -4 );
        }
        return $card;
    }

    
    

   /* *****     *****       *****       *****
    *   
    *           cardAddEditEvent            *
    *           
    * *****     *****       *****       *****/
    /**
     * Execution point for cardAddEditEvent
     * @param  array    $post 
     * @return boolean  true if OK
     */
    public function cardAddEditEvent_wrapped( $post )
    {
        $post = $this->process_input($post);
        unset($post['api_username']);
        unset($post['api_password']);
        $uid = $post['uid'];

        // TODO - address is still blank...
        $user_details = $this->get_user_info_from_user_id( $uid );
        $address = $this->get_address_structure( $post, $user_details );
       

        $customerProfileId = $user_details['authorizenet_profile_id'];

        if( $customerProfileId == 0 || !$customerProfileId )
        {
            $customerProfileId = $this->create_profile_id( $user_details['username'], $uid );
            $this->assert_test( $customerProfileId );
            
            if( $customerProfileId == 0 ) throw new customExceptionApi( ORDER_ERROR_CREATING_PAYMENT_PROFILE, "Failed to create in cardAddEditEvent_wrapped", $post );
            $query     = "UPDATE users SET authorizenet_profile_id = $customerProfileId  WHERE uid = $uid ";
            $this->db->Query($query);
        }

        $query_card_update_insert = $this->cardAddEditEvent_create_or_update_card( $customerProfileId, $post , $address );
        
        $this->db->Query( $query_card_update_insert );
        //return $this->db->insertId(  );
        return true;
        //exit(  );
        //return 
    }

    private function cardAddEditEvent_create_or_update_card( $customerProfileId, $post, $address  )
    {
        $paymentProfileId = 0;
        if ( $post['id'] )  // card id -editing a card = have id, update.  no id = new card.
        { //card id present then update
            $paymentProfileId = $this->get_profile_id_from_db_billinginfo( $post['id'] );

            if( $paymentProfileId != 0 ){
                $new_paymentProfileId = 
                    $this->authObject->updatePaymentProfile(    $paymentProfileId, $customerProfileId, 
                                                                $post['card_number'], $post['expire_year'], $post['expire_month'], $address );
            }
            else
            {
                $new_paymentProfileId = 
                    $this->authObject->createPaymentProfile(    $customerProfileId, 
                                                                $post['card_number'], $post['expire_year'], $post['expire_month'], $address );
            }

           $this->assert_test( $new_paymentProfileId  );
            $query_card_update_insert = $this->create_sql_update_card( $new_paymentProfileId, $post  );

        } 
        else    // Card id not provided.
        {
            $paymentProfileId=0;
            $new_paymentProfileId = 
                $this->authObject->createPaymentProfile(    $customerProfileId, 
                                                            $post['card_number'], $post['expire_year'], $post['expire_month'], $address );
            $values = array();
            $cols   = array();
            
            $this->assert_test( $new_paymentProfileId  );
            $query_card_update_insert = $this->create_sql_insert_card(  $new_paymentProfileId, $post  );
        }
        return $query_card_update_insert;
    }


    private function create_sql_update_card( $paymentProfileId,  $post )
    {
        $id = $post['id'];
        $uid = $post['uid'];
        foreach ($post as $key => $value) 
            {
                if ( $key != "id"   &&   $key !="card_cvv"   &&   $key != 'card_number')
                {
                    $update_items[] = " $key = '" . $this->clean_for_sql( $value ) . "'";
                }
                elseif( $key == "card_cvv" ){
                    $update_items[] = "card_cvv = 'XXX'";
                }
                elseif( $key == "card_number" ){
                    $update_items[] = "card_number = " . "'XXXX-XXXX-XXXX-".substr( $value,-4 )."'";
                }
            }
            $update_joined = implode(",", $update_items);
            $query = "UPDATE order_billinginfo SET $update_joined WHERE id = $id AND uid = $uid";
            return $query;
    }

    private function create_sql_insert_card( $paymentProfileId, $post )
    {
         foreach ($post as $key => $value) 
            {
                if ( $key != "id" &&    $key !="card_cvv"    &&    $key !="card_number" )
                {
                    $cols[]   = $key;
                    $values[] = "'" . $this->clean_for_sql( urldecode($value) ) . "'";
                }
                elseif($key =="card_cvv")
                {
                    $cols[]   = 'card_cvv';
                    $values[] = "'XXX'";
                }
                elseif( $key == "card_number" )
                {
                    $cols[]   = 'card_number';
                    $values[] =  "'XXXX-XXXX-XXXX-".substr( $value,-4 )."'";
                }
            }

            $cols[]='authorizenet_payment_profile_id';
            $values[]=$paymentProfileId;

            if( $paymentProfileId == 0 ) throw customExceptionApi( ORDER_ERROR_CREATING_PAYMENT_PROFILE, "Empty payment profile id passed to create_sql_insert_card", $post );

            $columns_joined = implode(",", $cols);
            $values_joined  = implode(",", $values);
            $query = "INSERT INTO order_billinginfo ( $columns_joined ) VALUES ( $values_joined  )";
            return $query;
    }

  

    /* *****     *****       *****       *****
    *   
    *           saveBillingInfo              *
    *           
    * *****     *****       *****       *****/
    /**
    * The execution entry point for savebillinginfo
    * @param  array     $post 
    * @return int       credit card insert ID from orders_billinginfo
    */
    public function savebillinginfo_wrapped( $post ) 
    {
        $post = $this->process_input($post);
        $customerProfileId = 0;
        $paymentProfileId = 0;

        //$this->authObject = new authorizenetCIM();

        $user_details = $this->get_user_info_from_user_id( $post['uid'] );
        $address_info = $this->get_address_structure( $post, $user_details );
       
        $customerProfileId = $user_details['authorizenet_profile_id'];

        if(  $customerProfileId == 0 || !isset( $customerProfileId ) || $customerProfileId =='' ){
            $customerProfileId = $this->create_profile_id( $user_details['username'], $post['uid'] );
        }

        $post['card_nocheck']='XXXX-XXXX-XXXX-'.substr( $post['card_no'],-4 );
        $query = "SELECT * FROM order_billinginfo WHERE uid = '" . $post['uid'] . "' AND card_number = '" . $post['card_nocheck'] . "' AND card_type = '" . $post['card_type'] . "'";

        $result = $this->db->Query($query);
        $credit_card_info = $this->db->FetchArray($result);

        if ( $credit_card_info )     // The card exists...get the profile and update database
        {
            $paymentProfileId = 0;
            $post['credit_card_id'] = $credit_card_info['id'];
            $paymentProfileId = $this->register_credit_card_with_authorize(  $customerProfileId, $address_info, $post, $credit_card_info );
            $this->update_billinginfo_table_with_new_card( $paymentProfileId, $post );
            
            return $post['credit_card_id'];
        }
        else    // Does not exist, create and INSERT into db.
        {
            /*print "new card";
            print "Customer id: $customerProfileId\n";
            print "POST\n";
            print_r( $post );*/
            $card_no = $post['card_no'];
            $expire_year = $post['expiry_month'];
            $expire_month = $post['expiry_year'];

            $paymentProfileId=0;
            $paymentProfileId =
                $this->authObject->createPaymentProfile(  $customerProfileId, $card_no, $expire_year, $expire_month, $address_info );

            return $this->insert_billinginfo_table_with_new_card(  $paymentProfileId, $post );
        }
    }

    // Looks for payment profile ID.  
    // IF it doesn't have it, then it creates a new profile.
    // If it has the profileID, then it updates it with the card.
    // Then it updates the sql database with the information.
    private function  register_credit_card_with_authorize( $customerProfileId, $address, $post, $credit_card_info  )
    {           
        $paymentProfileId = 0;
        $paymentProfileId = $credit_card_info['authorizenet_payment_profile_id'];

         //$post['_card_number_auth'], $post['_expiry_month_auth'], $post['_expiry_year_auth'];
         //print_r( $credit_card_info );
         //exit(  );
        if( $paymentProfileId == 0 )        {
            
            $paymentProfileId = $this->authObject->createPaymentProfile(  $customerProfileId, $post['card_no'], $post['expiry_year'], $post['expiry_month'], $address );
            
            if(  $paymentProfileId == 0 ){
                throw new CustomExceptionApi( ORDER_ERROR_CREATE_PAYMENT_PROFILE, 'Error creating a payment profile; No Profile ID.', 
                    array(customerProfileId, $post['card_no'], $post['expiry_year'], $post['expiry_month'], $address, 'register_credit_card_with_authorize' ) );
            }
        }
        else        
        {
            try
            {
                $updateProfileDetails =
                    $this->authObject->updatePaymentProfile(  $paymentProfileId, $customerProfileId, $post['card_no'], $post['expiry_year'], $post['expiry_month'], 
                        $address );
                return  $paymentProfileId;
                
            }
            catch( CustomException $e )
            {
                if(  $e->getExtra(  ) == 'E00040' ){
                    throw new CustomExceptionApi( ORDER_ERROR_CREATING_PAYMENT_PROFILE, "DUPE card entered"); 
                    exit(  );
                    $credit_card_info['authorizenet_payment_profile_id'] = 0;
                    return $this->register_credit_card_with_authorize( $customerProfileId, $address, $post, $credit_card_info  );
                }
                else
                    throw $e;
            }
        }
    }

    // SQL to update the new credit card information
    private function update_billinginfo_table_with_new_card( $paymentProfileId, $card_info )
    {
        if( $paymentProfileId == 0  )
        {
            throw new customExceptionApi( ORDER_ERROR_CREATING_PAYMENT_PROFILE, "Empty payment profile ID in update_billinginfo_table_with_new_card", $card_info);
        }


        $query = "UPDATE order_billinginfo SET 
                card_name = '" . $card_info['card_name'] . "' , ".
                "card_number = '" . $card_info['card_nocheck'] . "' , ".
                "card_type = '" . $card_info['card_type'] . "' , ".
                "card_zip = '" . $card_info['card_zip'] . "' , ".
                "expire_month = '" .$card_info['expiry_month']. "' , ".
                "expire_year =  '" .$card_info['expiry_year']. "', ".
                "display_dropdown='" . $card_info['display_in_menu'] . "' , ".
                "authorizenet_payment_profile_id='" . $paymentProfileId . "' ".
                "  WHERE id = '" . $card_info['credit_card_id'] . "' ";

        $this->db->Query($query);       
    }

    // SQL to insert the new credit card.
    private function insert_billinginfo_table_with_new_card( $paymentProfileId, $post )
    {
        if( $paymentProfileId == 0  )
        {
            throw new customExceptionApi( ORDER_ERROR_CREATING_PAYMENT_PROFILE, "Empty payment profile ID in insert_billinginfo_table_with_new_card", $card_info);
        }


        $query = "INSERT INTO order_billinginfo (\n".
            'card_name,' .
            'card_number,' .
            'card_type,' .
            'card_zip,' .
            'expire_month,' .
            'expire_year,' .
            'uid,' .
            'display_dropdown,' .
            'card_cvv,' .
            'address1,' .
            'street,' .
            'authorizenet_payment_profile_id' .
        "\n) VALUES ( '\n" . 
            $post['card_name'] . "','" . 
            $post['card_nocheck'] . "','" . 
            $post['card_type'] . "','" . 
            $post['card_zip'] . "','" .
            $post['expiry_month']. "','" .
            $post['expiry_year']. "','" . 
            $post['uid'] . "','" . 
            $post['display_in_menu'] . "',
            '" . 'XXX' . "','" . 
            $post['address1'] . "','" . 
            $post['street'] . "','" . 
            $paymentProfileId . "') ";

        $this->db->Query($query);
        return $this->db->insertId();
    }

   
    /* *****     *****       *****       *****
    *   
    *           card remove event            *
    *           
    * *****     *****       *****       *****/
    /**
     * Execution point for deletion of cards from the myaccount controller.
     * @param  array    $post
     * @return boolean  true on OK
     */
    public function cardRemoveEvent_wrapped( $post )
    {
        //$this->authObject = new authorizenetCIM();
        $postdelete=array();
        
        $id = $this->clean_for_sql( $post['id'] );

        $query="SELECT * FROM order_billinginfo WHERE id= $id ";

        $result    = $this->db->query($query);
        $card_details = $this->db->FetchArray($result);

        if(  !$card_details ){
            throw new CustomExceptionApi( ORDER_ERROR_SQL_GET_BILLINGINFO, "Did not receive any information for card id# $id" );
        }

        $postdelete['customerPaymentProfileId'] = $card_details['authorizenet_payment_profile_id'];
        
        $query = "SELECT * FROM users WHERE uid = ".$card_details['uid']." ";
        $result    = $this->db->query($query);
        $user_details = $this->db->FetchArray($result);
        $postdelete['customerProfileId']=$user_details['authorizenet_profile_id'];


        try{
            $deletePaymentProfile=$this->authObject->deletePaymentProfile( $postdelete );
        }
        catch( CustomException $e_except ) 
        {
            if( $e_except->getExtra(  ) == 'E00040' )
            {

            }
            else
                throw $e_except;
        }
        
        
        if($deletePaymentProfile)
        {
            $query = "DELETE FROM order_billinginfo WHERE id= $id ";
        }
        else{
            $query = "UPDATE order_billinginfo set display_dropdown=0 WHERE id= $id ";
        }
        $this->db->Query($query);
        return true;
    }



   



    /* ******************************************************
    *   AUTHORIZENET INTERFACES
    *   These are direct interfaces with Authorize, they get/set id and connect directly with authorizenet
    *
    * ******************************************************/
    private function create_profile_id( $email, $user_id )
    {
       //create a profile id in case the profile id is empty
        $msg='';
        $profileID = 0;
    
        $profileID = $this->authObject->createProfile( $email, $user_id );
        
        if( $profileID == 0 ){
            throw new customExceptionApi( ORDER_ERROR_CREATING_PROFILE_PROFILE, 'Could not create a *profile* ID.' );
        }

        $this->set_authorize_profile_id_with_uid( $profileID, $user_id );
        return $profileID;   
    }


    private function create_shipping_profile_id( $profileID, $order_address_info )
    {
        $shippingProfileId = 0;
        $shippingProfileId = $this->authObject->createShippingProfile( $profileID, $order_address_info );
        $address_id = $order_address_info['address_id'];

        if( $shippingProfileId != 0 )
        {
            // HERE was an if for delivery or pickup.
                $query = "UPDATE order_address SET authorizenet_shipping_profile_id = $shippingProfileId WHERE address_id = $address_id ";
                $this->db->Query($query);
        }
        else {       // DEFAULT case, send the results because they are ok.
            throw new customExceptionApi( ORDER_ERROR_CREATE_SHIPPING_PROFILE, 'Could not create a shipping ID.' );
        }
        return  $shippingProfileId;
    }


    private function set_authorize_profile_id_with_uid( $profileID, $user_id )
    {
        if( $profileID == 0  )
        {
            throw new customExceptionApi( ORDER_ERROR_CREATING_PAYMENT_PROFILE, 'Failed to create profile id with set_authorize_profile_id_with_uid', "userid - ". $user_id );
        }
        $query     = "UPDATE users SET authorizenet_profile_id = '" . $profileID . "'  WHERE uid = '" . $user_id . "' ";            

        $this->db->Query($query);
        return $profile_id;
    }


    private function create_payment_profile_id( $profileID, $post, $billing_address )
    {
        $post = $this->validate_auth_fields( $post );

        $credit_card_id = $this->clean_for_sql( $post['credit_card_id'] );
        $card_auth = $this->clean_for_sql( $post['_card_number_auth'] );
        $month_auth = $this->clean_for_sql( $post['_expiry_month_auth'] );
        $year_auth = $this->clean_for_sql( $post['_expiry_year_auth'] );


        //print "profileID - $profileID, card_auth - $card_auth, month_auth - $month_auth, year_auth - $year_auth, billing_address - \n";
        //print_r($billing_address );
        $paymentProfileId = $this->authObject->createPaymentProfile($profileID, $card_auth, $month_auth, $year_auth, $billing_address );
        //$paymentProfileId = 0;

        if( $paymentProfileId ==  0 ){
            $query            = "DELETE FROM order_billinginfo WHERE id = $credit_card_id LIMIT 1 ";
            throw new CustomExceptionApi( ORDER_ERROR_CREATING_PAYMENT_PROFILE, 'Failed creating payment profile ID' );
        }

        $query            = "UPDATE order_billinginfo SET authorizenet_payment_profile_id = $paymentProfileId WHERE id = $credit_card_id ";
        $this->db->Query($query);

        return $paymentProfileId;
    }
    

    private function validate_prices( $order_items, $post )
    {
        $running_talley = 0;
        $this->assert_test( isset( $post['sub_total'] ) );
        foreach( $order_items as $item )
        {
            if( isset( $item['item_qty'] ) && isset( $item['item_price'] ) ){
                $running_talley += $item['item_qty'] * $item['item_price'];
            }
        }

        if( (int) $running_talley !== (int) $post['sub_total'] )
            throw new CustomExceptionTransaction( ORDER_ERROR_SUBTOTALS_DONT_MATCH, "Subtotals don't match.  Should be [$running_talley] but is [${post['sub_total']}]"  );

        $running_plus_tax_tip = $running_talley + ( int )$post['tax'] + ( int )$post['tip'];
        $posted_total = $post['total'];

        if( (int) $running_plus_tax_tip !== (int) $posted_total )
            throw new CustomExceptionTransaction( ORDER_ERROR_TOTALS_DONT_MATCH, "TOTALS don't match.  Should be [$running_plus_tax_tip] but is [$posted_total]"  );

    }

    /* *****     *****       *****       *****
    *   
    *          THE TRANSACTION               *
    *           
    * *****     *****       *****       *****/
    /**
     * Do the whole transaction process from start to finish
     * @param  array    $post   everything needed.
     * @return int      Transaction ID from Authorizenet
     */
    private function do_order( $post )
    {
        $post = $this->process_input($post);
        $this->validate_order_fields( $post );
        //starting of authorize
        //$this->authObject = new authorizenetCIM();
        
        $order_items    = $this->process_order_items( $post['order_item_id']  );        
        $delivery_time  = $this->get_delivery_time( $post );           
        
        if( $post['credit_card_id'] != 'no' && $post['credit_card_id'] != 'NO')
        {
          //  $this->validate_prices( $order_items, $post );
            $post           = $this->prepare_post_for_purchase( $post );         
            $transaction    = $this->do_transaction( $post, $order_items ); 
        }            

        $sql_order      = $this->build_sql_for_orders_table( $post, $transaction, $delivery_time );
        $insert_id      = $this->add_the_order_to_database( $sql_order );
        $order_number   = $this->insert_order_id_with_offset( $insert_id );
                          $this->save_meal_name( $insert_id, $post );
        $order_item_sql = $this->explode_order_items( $post );
                          $this->set_order_items_to_order_id( $insert_id, $order_item_sql );
                          $this->increment_sandwich_count( $order_item_sql );

        $this->order_number = $order_number;       
        return $order_number;
    }

    private function process_order_items(  $order_list )
    {
        $order_items = explode( ":", $order_list );
        $i          = 0;
        $return_array = array(  );

        foreach ($order_items as $order_item_id) {
            $query  = "SELECT o.item_id,o.item_qty,o.item_price,SUBSTRING(t.sandwich_name, 1, 30) as sandwich_name,SUBSTRING(t.description, 1, 30) as description  FROM `orders_items` as o LEFT JOIN `user_sandwich_data` as t on o.item_id = t.id WHERE o.item_type='user_sandwich' AND order_item_id = '" . $order_item_id . "' ";
            
            $result = $this->db->Query($query);
            $result = $this->db->FetchArrayAssoc($result);

            if (!empty($result)) {
                $return_array[$i] = $result;
                $i++;
            }
        }

        foreach ($order_items as $order_item_id) 
        {
            $query  = "SELECT o.item_id,o.item_qty,o.item_price,SUBSTRING(t.product_name, 1, 30) as product_name,SUBSTRING(t.description, 1, 30) as description FROM `orders_items` as o LEFT JOIN `standard_category_products` as t on o.item_id = t.id WHERE o.item_type='product' AND order_item_id = '" . $order_item_id . "' ";
        
            $result = $this->db->Query($query);
            $result = $this->db->FetchArrayAssoc($result);
        
            if (!empty($result)) {
                $return_array[$i] = $result;
                $i++;
            }
        }
        return $return_array;
    }

    private function get_delivery_time( $post )
    {
        $time = 0;

        if ( $post['now_or_specific'] == "now" ) {
            $date = date("Y-m-d H:i:s");
            $date = "'$date'";
            $time = 0;
        } else  if ( $post['now_or_specific'] == "specific" ) {
            $date = date("Y-m-d H:i:s", strtotime($post['date'] . " " . $post['time'] . ":00"));
            $date = "'$date'";
            $time = 1;
        }
        else
        {
            throw new CustomExceptionApi( ORDER_ERROR_PICKING_TIME_NOW_SPECIFIC, "Could not pick a time because neither now nor specific selected: ".
                        htmlspecialchars( $post['now_or_specific']) );
        }

        return array( "date"=>$date, 'time'=>$time );
    }

    private function prepare_post_for_purchase( $post )
    {       
        $post = $this->process_input($post);
 
        // Get clean ID's
        $user_id                = filter_var( $post['user_id'] , FILTER_SANITIZE_NUMBER_INT);
        $address_id             = filter_var( $post['address_id'] , FILTER_SANITIZE_NUMBER_INT);
        $credit_card_id         = filter_var( $post['credit_card_id'] , FILTER_SANITIZE_NUMBER_INT );

        // Get sql results for each - user, address, credit card.
        $user_info              = $this->get_user_info_from_user_id(  $user_id );
        if( $credit_card_id ){
            $billing_info           = $this->get_billing_info_by_user_id_and_card_id( $user_id, $credit_card_id );
        }
        else{
            $billing_info           = $this->get_billing_info_by_user_id( $user_id );
        }

        $order_address_info     = $this->get_order_address_by_id( $address_id );

        // Initialize variables to be used by Authorizenet
        $paymentProfileId   = $billing_info['authorizenet_payment_profile_id'];
        $shippingProfileId  = $order_address_info['authorizenet_shipping_profile_id'];
        $profileID          = $user_info['authorizenet_profile_id'];

        // Save the id's in the proper structure for ease of passing.
        $order_address_info['address_id']   = $address_id;
        $user_info['user_id']               = $user_id;
        $billing_info['card_id']            = $credit_card_id;

        // This indicates one use card or saved card use.
        if( isset( $billing_info['display_dropdown'] ) )
            $save_card_was_checked_when_entered = $billing_info['display_dropdown'];
        else
            $save_card_was_checked_when_entered = 0;
        
        if( $save_card_was_checked_when_entered == 0 )
        {
            // This basically clears the shipping, payment and profile ids.
            $post                       = $this->init_post_for_transaction_no_save(  $post );

            // One time use, basically don't save anything, just build a transaction with the card and address info...
            $this->clear_authnet_db_ids_by_address_id( $address_id );

            // New billing address built from order info ( base variable ) clobbered by billing_info
            $billing_address            = $this->build_billing_address_from_user_and_billing_info( $order_address_info   , $billing_info );
            $this->transaction_type = 'one_time';
        }
        else    // Save card WAS CHECKED = 1 when entered, so save the card.  
        {   
            // if profileID is 0 or not set, then create it. else we'll keep using the one we were init with.
            //print "Profile ID: $profileID\n";
             if( $this->isEmpty( $profileID )   ){     
                $profileID = $this->create_profile_id( $user_info['email'], $user_info['user_id']  );            
           // }
            }
            
            if( $post['delivery'] == 0 )
            {
                $result = $this->get_pickup_shipping( $post );
                $shippingProfileId = $result[0];
                $shippingresult_store = $result[1];
            }
        
            // Do the same with the shipping - check and if 0 then create.
            if ( $this->isEmpty( $shippingProfileId ) ) {
                $shippingProfileId = $this->create_shipping_profile_id( $profileID, $order_address_info );
            }
        
            //    print "Shipping Profile ID: $shippingProfileId\n";
            // HERE - the profile is bad. 
            // Always update the shipping with latest shipping information.  Just easier this way than checking for a change.  
            $shippingProfile = $this->authObject->updateShippingProfile( $shippingProfileId, $profileID, $order_address_info );
            $billing_address = $this->build_billing_address_from_user_and_billing_info( $order_address_info , $billing_info );
            
            // And finally the paymentprofileID - check and if 0 then create.
           if ( $this->isEmpty( $paymentProfileId )  ) {
                $paymentProfileId = $this->create_payment_profile_id( $profileID, $post, $billing_address );
           }
            
           // print "===\n";
            /*
            if( $paymentProfileId ){
                    $this->authObject->updatePaymentProfile(    $paymentProfileId, $profileID, 
                                                                $billing_info['card_number'], $billing_info['expire_year'], $billing_info['expire_month'], $billing_address );
                }*/

            // Last test before sending them on their way
            $assert_err = ORDER_ERROR_ASSERTION_ERROR;
            $this->assert_test( $paymentProfileId, $assert_err, 'Payment Profile ID is empty' );
            $this->assert_test( $shippingProfileId, $assert_err, 'Shipping Profile ID is empty' );
            $this->assert_test( $profileID, $assert_err, 'Profile ID is empty' );

           // print "+++\n";
            // print "profileID - $profileID, paymentProfileId - $paymentProfileId, shippingProfileId- $shippingProfileId\n";
            $post = $this->init_post_for_transaction_with_values( $post, $profileID, $paymentProfileId, $shippingProfileId );
            // if we get an error here ^^^ then we need to test - is there xxxx-xxxx-xxxx then we are screwed; if we are ####...
            // then we need to create the payment profile id again and then try transaction again.
            // Maybe if we do the updatePaymentProfile testing xxxx... we can run it.
            $this->transaction_type = 'normal';
        }

        // Save these to the class.
        $this->paymentProfileId = $paymentProfileId;
        $this->shippingProfileId = $shippingProfileId;
        $this->profileID = $profileID;
        $this->billing_address = $billing_address;
        $this->delivery_address = $order_address_info;
        $this->post = $post;
                $this->verify_credit_card_to_user( $post );

        return $post;
    }

    private function verify_credit_card_to_user(  $post )
    {
        //print_r( $post );exit(  );
        // $post['credit_card_id'];
        // $q = "SELECT uid FROM order_billinginfo WHERE id = ${post['credit_card_id']}";
        // exit(  );
    }

    private function get_pickup_shipping( $post )
    {
        $query_store          = "SELECT * FROM `order_billinginfo` WHERE id = '" . $post['credit_card_id'] . "' ";
            $result_store         = $this->db->Query($query_store);
            $shippingresult_store = $this->db->FetchArray($result_store);
            $query                = "SELECT * FROM `order_address` WHERE name = '" . $first_name . "' AND address1 = '" . $shippingresult_store['address1'] . "' AND street = '" . $shippingresult_store['street'] . "' AND zip = '" . $shippingresult_store['zip'] . "' AND uid='" . $post['user_id'] . "' ";
            $result               = $this->db->Query($query);
            $shippingresult       = $this->db->FetchArray($result);
            if( isset( $shippingresult['authorizenet_shipping_profile_id'] ) )
            {
                return(  array( $shippingresult['authorizenet_shipping_profile_id'], $shippingresult_store ) );
            }
            else
            {
                $query = "SELECT * FROM pickup_stores WHERE id = {$post['store_id']}";
                $result               = $this->db->Query($query);
                $shippingresult_store       = $this->db->FetchArray($result);
                
                $query                = "SELECT * FROM `order_address` WHERE name = '" . $first_name . "' AND company = '".$shippingresult_store['store_name']."' AND address1 = '" . $shippingresult_store['address1'] . "' AND street = '" . $shippingresult_store['street'] . "' AND zip = '" . $shippingresult_store['zip'] . "' AND uid='" . $post['user_id'] . "' ";
                $result               = $this->db->Query($query);
                $shippingresult       = $this->db->FetchArray($result);
            }
            if(is_array($shippingresult))
            {
                return(  array( $shippingresult['authorizenet_shipping_profile_id'], $shippingresult_store ) );
            }
            else
            {
                    $query             = "INSERT INTO order_address (name,company,phone,address1,address2,zip,authorizenet_shipping_profile_id,street,cross_streets,extn,delivery_instructions,uid) VALUES('" . $first_name . "','" . $shippingresult_store['store_name'] . "','','" . $shippingresult_store['address1'] . "','','" . $shippingresult_store['zip'] . "',0,'" . $shippingresult_store['street'] . "','" . $shippingresult_store['cross_streets'] . "','','','" . $post['user_id'] . "')";

                    $shippingProfileId = 0;

                    $result         = $this->db->Query($query);
                    $address_insid  = $this->db->insertId();
                    $query          = "SELECT * FROM `order_address` WHERE address_id = '" . $address_insid . "' ";
                    $result         = $this->db->Query($query);
                    $shippingresult = $this->db->FetchArray($result);
            }  
            if( !isset( $shippingresult['authorizenet_shipping_profile_id'] ) )
                throw new CustomExceptionApi( ORDER_ERROR_CREATE_SHIPPING_PROFILE, 'Creating shipping profile  for pickup' );
            return(  array( $shippingresult['authorizenet_shipping_profile_id'], $shippingresult_store ) );

    }


    private function validate_auth_fields( $post )
    {
        //print_r( $post );
        if( !$post['_card_number_auth'] ){
            $post['_card_number_auth'] = $post['credit_card'];
        }
        
        if( !$post['_expiry_month_auth'] )
            $post['_expiry_month_auth'] = $post['exp_mth'];
        
        if( !$post['_expiry_year_auth'] )
            $post['_expiry_year_auth'] = $post['exp_yr'];

        return $post;
    }

     private function init_post_for_transaction_with_values( $post, $profileID, $paymentID, $shippingID )
    {
        $post['customerProfileId']        =  $profileID;
        $post['customerPaymentProfileId'] = $paymentID;     // wtf why is this zeroed out???
        $post['customerShippingAddressId'] = $shippingID;
        return $post;
    }

    private function init_post_for_transaction_no_save( $post )
    {
        $post['customerProfileId']        = 0;
        $post['customerPaymentProfileId'] = 0;      // wtf why is this zeroed out???
        $post['customerShippingAddressId'] = 0;
        return $post;
    }


    private function do_transaction( $post, $orderItems )
    {
        if( $this->transaction_type == 'one_time' ){
            return $this->authObject->createTransactionRequest( $post, $orderItems, $this->billing_address, $this->delivery_address );
            
        }
        else    {
             return $this->authObject->createTransaction($post, $orderItems);        
        }

    }

    private function build_sql_for_orders_table( $post, $transaction_id, $delivery_time )
    {
        $post = $this->process_input($post);
        $time = $delivery_time['time'];
        $date = $delivery_time['date'];
        
        $is_admin_col_val   = $this->get_by_admin_col_val( $post );
        $off_value_col_val  = $this->get_off_value_col_val( $post );
        $manual_discount    = $this->get_manual_discount_val( $post );
        $coupon_code        = $this->get_coupon_code_val( $post );
        $sub_total          = $this->get_sub_total_val( $post );

        $date_time = date("Y-m-d H:i:s");
       // print "[$date_time]";
        $query = "\nINSERT INTO orders (
                user_id, 
                store_id,
                address_id,
                billinginfo_id,
                order_item_id,
                sub_total,
                tip,
                coupon_code,
                total,
                tax,
                delivery,
                date, 
                manual_discount,
                time, 
                authorizenet_transaction_id, 
                order_date 
                $is_admin_col_val[0]
                $off_value_col_val[0]
            ) VALUES (" . 
                $post['user_id']        . ",  \n " . 
                $post['store_id']       . ",  \n " . 
                $post['address_id']     . " , \n '" . 
                $post['credit_card_id'] . "' , \n '" . 
                $post['order_item_id']  . "' , \n'" . 
                $sub_total              . "' ,\n'" . 
                $post['tip']            . "' ,\n'" . 
                $coupon_code            . "', \n'" . 
                $post['total']          . "', \n'" . 
                $post['tax']            . "', \n" . 
                $post['delivery']       . ", \n".
                $date                   . ", \n'" .      // ok
                $manual_discount        . "', \n'" .      // ok
                $time                   . "', \n'" .      // ok
                $transaction_id         . "', \n'" .      // ok
                $date_time              . "'   " .      // ok
                $is_admin_col_val[1]    . "    " .      // ok
                $off_value_col_val[1]   . "    " .      // ok
            ")\n";

      //  print "\n\n$query\n\n";exit(  );
        return $query;
    }


    private function add_the_order_to_database( $sql_order )
    {
        $result = $this->db->Query($sql_order);
        $insert_id  = $this->db->insertId();
        return $insert_id;
    }

    private function insert_order_id_with_offset( $insert_id )
    {
        $order_number = ORDER_ID_BASE_NUMBER + $insert_id;
        $update = "UPDATE orders SET order_number = '$order_number' WHERE order_id = $insert_id";
        $result = $this->db->Query($update);
        return $order_number;
    }


    private function save_meal_name( $insert_id, $post )
    {
        if ( !$insert_id )
            return;
        if( isset($post['mealname'])   AND   strlen($post['mealname']) > 0 ) 
        {
            $save_meal = "INSERT INTO saved_meals (order_id,meal_name) VALUES ('" . $insert_id . "','" . $post['mealname'] . "') ";
            $this->db->Query( $save_meal );
        }
    }

    private function explode_order_items( $post )
    {
        $items_list = explode( ":", $post['order_item_id'] );
        $items_sql = implode( ",", $items_list );
        return $items_sql;
    }

    private function set_order_items_to_order_id( $insert_id, $order_item_sql )
    {
        $query  = "UPDATE orders_items SET ordered = 1, orderid = $insert_id WHERE order_item_id IN ( $order_item_sql )";
        $result = $this->db->Query($query);
    }

    private function increment_sandwich_count( $order_item_sql )
    {
        $query  = "UPDATE `user_sandwich_data` SET purchase_count = purchase_count + 1 
                    WHERE id IN (SELECT item_id FROM orders_items WHERE item_type='user_sandwich' AND order_item_id IN ( $order_item_sql ) )";
        $result = $this->db->Query($query);
    }

    private function calc_extra_post_values_for_order( $post )
    {
        if ( isset($post['coupon_code'] ) ) {
            $query = "UPDATE  discounts SET uses_amount = uses_amount - 1 WHERE id = '" . $post['coupon_code'] . "' AND uses_type = 0 ";
            $this->db->Query($query);
        }

        if (!isset($post['by_admin']) && !isset($post['coupon_code'])) {
            $post['coupon_code'] = "";
        }
    }


    private function get_by_admin_col_val( $post )
    {
        if( isset( $post['by_admin'] ) ){
            $is_admin = ", 1";
            $by_admin = ", by_admin"; }
        else{
            $is_admin = "";
            $by_admin = "";
        }
        return array(  $by_admin, $is_admin  );
    }

    private function get_off_value_col_val( $post )
    {
        if ( isset($post['off_type']) && isset($post['off_amount']) ) 
        {
            $off_col   = ", off_type, off_amount";
            $off_value = ", '{$post['off_type']}', {$post['off_amount']}";
        }
        else if (  isset( $post['off_amount'] ) && $post['off_amount'] != "" ) 
        {  
            $off_col   = ",  off_amount";
            $off_value = ", {$post['off_amount']}";
        }
        else
        {
            $off_col = '';
            $off_value = '';
        }
        return array(  $off_col, $off_value  );
    }

    private function get_manual_discount_val( $post )
    {
        if (isset($post['manual_discount']))
            $manual_discount = $post['manual_discount'];
        else
            $manual_discount = 0;
        return $manual_discount;
    }

    private function get_coupon_code_val( $post )
    {
        $coupon_code = "";
        if (isset($post['coupon_code'])) {
            $query = "UPDATE  discounts SET uses_amount = uses_amount - 1 WHERE id = '" . $post['coupon_code'] . "' AND uses_type = 0 ";
            $this->db->Query($query);
            $coupon_code = $post['coupon_code'];
        }
        if (!isset($post['by_admin']) && !isset($post['coupon_code'])) {
            $coupon_code = "";
        }
        return $coupon_code;
    }

    private function get_sub_total_val( $post )
    {
        $sub_total = str_replace( ",", "", $post['sub_total'] );
        $sub_total = floatval( $sub_total );
        return $sub_total;
    }

    private function test_for_checkout( $post, $enabled = 0 )
    {
        if( $enable !== 0 )
            return $post;
        
        var_dump( $post );

        return $post;
    }

    private function process_transaction_result( $transaction )
    {
        if (  isset($transaction['error']) && $tarnsaction['error'] == 1 )
        {
            if( $transaction['data'] == "The payment gateway account is in Test Mode. The request cannot be processed." )
            {
                throw new customExceptionApi( ORDER_ERROR_TEST_MODE, 'Gateway is in Test Mode.' );
            }
            else 
                throw new customExceptionApi( ORDER_ERROR_TRANSACTION, 'Transaction Error.' );
        }

        return $transaction['data'];
    }

    private function validate_order_fields( $post )
    {
        $validate = array(  'address_id', 'user_id' );
        foreach( $validate as $keyname )
        {
            if( !isset( $post[$keyname] ) || !$post[$keyname] || $post[$keyname] == '' )
                throw new customExceptionApi( ORDER_ERROR_EMPTY_VARIABLE, "$keyname Variable was empty." );
        }
    }



     /* ******************************************************
    *   MySQL Access & General Structure ( like for address )
    *   These are sql getters, used by various functions in this module.
    *
    * ******************************************************/
    private function get_user_details_from_uid( $uid )
    {
        $query= "SELECT username, authorizenet_profile_id, first_name, last_name, phone, company FROM users WHERE uid =  $uid ";
        $result = $this->db->Query($query);
        $user_details = $this->db->FetchArray($result);
        return $user_details;
    }

    private function get_address_structure( $post, $user_details )
    {
        $address = array("phone"=>"","name"=>"","last_name"=>"","company"=>"","zip"=>"","address1"=>"","address2"=>"");
        $address['name'] = $user_details['first_name'];
        $address['last_name'] = $user_details['last_name'];
        $address['phone'] = $user_details['phone'];
        $address['company'] = $user_details['company'];
        if( isset($post['card_zip']) ) $address['zip'] = $post['card_zip'];
        return $address;
    }

    private function get_profile_id_from_db_billinginfo( $id )
    {
        $query = "SELECT authorizenet_payment_profile_id FROM order_billinginfo WHERE id = $id";
        $result    = $this->db->query($query);
        $prof_details = $this->db->FetchArray($result);
        return $prof_details['authorizenet_payment_profile_id'];
    }

     private function get_billing_info_by_user_id_and_card_id( $user_id, $card_id )
    {
        $query = "SELECT  * FROM `order_billinginfo` WHERE id = $card_id AND uid = $user_id ";
        //print $query;exit(  );
        $result           = $this->db->Query($query);
        $result           = $this->db->FetchArray($result);
        return $result;
    }

    private function get_billing_info_by_user_id( $user_id )
    {
        $query = "SELECT  * FROM `order_billinginfo` WHERE uid = $user_id ORDER BY id DESC LIMIT 1";
        $result           = $this->db->Query($query);
        $result           = $this->db->FetchArray($result);
        return $result;
    }

    private function get_order_address_by_id( $id )
    {
        $query             = "SELECT * FROM `order_address` WHERE address_id = '" . $id . "' ";
        $result            = $this->db->Query($query);
        $shipping_result    = $this->db->FetchArray($result);
        $shipping_result['address2'] = $shipping_result['street'];
        //$shipping_profile_id = $shipping_result['authorizenet_shipping_profile_id'];
        return $shipping_result;
    }

    private function build_billing_address_from_user_and_billing_info( $address_info, $billing_info )
    {
        $billing_address                =   $address_info;
        if( isset( $billing_info['card_zip'] ) )
            $billing_address["zip"]         = $billing_info['card_zip'];
        $billing_address["address1"]    = $billing_info['address1'];
        $billing_address["street"]      = $billing_info['street'];
        $billing_address["address2"]    = $billing_info['street'];
        return $billing_address;
    }

    private function get_user_info_from_user_id( $user_id )
    {
        $query      = "SELECT first_name, last_name, username, authorizenet_profile_id, company, phone FROM `users` WHERE uid = $user_id ";
        $result     = $this->db->Query($query);
        $users     = $this->db->FetchArray($result);
        $users['email'] = $users['username'];
        return $users;
    }

    private function compile_billing_address_from_order_address( $shipping_result )
    {
        $billing_address = $shippingresult;
        $billing_address["zip"] = $result['card_zip'];
        $billing_address["address1"] = $result['address1'];
        $billing_address["street"] = $result['street'];
        $billing_address["address2"] = $result['street'];
        return $billing_address;
    }

    private function clear_authorize_net_SHIPPING_by_address_id( $address_id )
    {
        $this->db->Query( "UPDATE order_address SET authorizenet_shipping_profile_id = 0 WHERE address_id = $address_id LIMIT 1 " );
    }

    private function clear_authnet_db_ids_by_address_id( $address_id )
    {
        $this->db->Query( "UPDATE order_address SET authorizenet_shipping_profile_id = 0 WHERE address_id = $address_id LIMIT 1 " );
        // DM 3-6-19 fix for the bad billing issue
        //   $this->db->Query( "UPDATE order_billinginfo SET authorizenet_payment_profile_id = 0 WHERE id = $address_id LIMIT 1 " );
    }

   

    /**
     * [assert_test description]
     * @param  $expression        What you want to test. Complex stuff put in params.
     * @param  int $code          constant code for message, defaults to ASSERSTION_ERROR
     * @param  string $message    Message passed to error
     
     */
    private function assert_test( $expression, $code = ORDER_ERROR_ASSERTION_ERROR, $message="" )
    {
        if( !$expression )
        {
            $exception_message = "Assert failed @".$this->get_caller_text(  );
            if( $message ){
                $exception_message = "$exception_message ($message)";
            }
            throw new CustomExceptionApi( $code, $exception_message  );
        }
    }

    private function get_caller_text(  )
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
        $caller = array_pop( $trace );
        $caller_name = $caller['function'];
        $caller_line = $caller['line'];
        return "$caller_name: $caller_line";
    }


    /* **************************************
    *
    *       HELPER FUNCTION                 *
    *
       **************************************/
    /**
     * Cleans out the POST values with this function.
     */
    private function process_input($data) {
        array_walk($data, function(&$val, $key) {
            $val = urldecode($val);
            $val = $this->clean_for_sql($val);
        });
        return $data;
    }
    
    private function error_headline(  $exception )
    {
        return "Error #".$exception->getCode(  ) ." - ". $exception->getMessage(  )."\n";
    }

    private function error_format_backtrace( $exception )
    {
        $backtrace = $exception->getTrace(  );
        $return = '';
        
        for( $i=0; $i<3; $i++ )
        {
            if(  isset( $backtrace[$i]['file'] ) ){
                $return .= basename( $backtrace[$i]['file'] ) . " (".$backtrace[$i+1]['line'].") ". ": " . $backtrace[$i+1]['function'];
                if( $i == 0 )
                    $return .= "  ". $exception->getLine( );
                $return .= "\n";
            }
            else
                $return .= basename( $backtrace[$i]['class'] ) . " (".$backtrace[$i+1]['line'].") ". ": " . $backtrace[$i+1]['function']  . "\n";
        }
        //array_shift( $backtrace )
        //$output .= $entry['file'] . ':' . $entry['line'] . ' - ' . $func . PHP_EOL;
       return $return ;
    }

    private function isEmpty( $test )
    {
        if ( !isset($test) || $test == 0 || !$test   )
            return true;
        else
            return null;
    }

}
