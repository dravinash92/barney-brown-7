ha.sandwichcart = window.ha.sandwichcart || {

    init: function() {
        
        var self = this;



        $('.place-order').unbind('click').on('click', function(e) 
        {
            e.preventDefault();
            session = ha.common.check_session_state();
            if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                   ha.common.login_popup();
                 } else {
                   ha.common.facebook_login();
                 }
            }
			
            
            if(ha.common.checkBreadType() == "error"){
        		return false;
        	}

            if ($(this).hasClass("disabled")) return false;

            var order_string = $('input[name="odstring"]').val();
            if (typeof order_string == "undefined" || !order_string) {
                alert("Please check your cart items. It can't be empty.");
                return false;
            }
			
            var delvry = -1;

            if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
                delvry = 1;
            } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
                delvry = 0;
            }

            var order_string = $('input[name="odstring"]').val();
            var address_id = $('input[name="address_selected"]').val();
            var pickup_address_id = $('input.pickup_address_id').val();
            var total = $('.grand-total').text();
            var delivery_fee = 0;
            if($('#hidden_delivery_fee').val()){
                var delivery_fee = $('#hidden_delivery_fee').val();
            }
            

            var now_or_specific = $('input[name="radiog_lite"]:checked').val();
            var date_val = $(".delivery_date").val();
            var time_val = $('div.timechange select').val();

            var tax = $('#hidden_tax').val();
            var coupon_code = $('#hidden_discount_id').val();
            var discount_amount = $('#hidden_discount_amount').val();
            var tip = $('#select_tip_amount').val();
            var sub_total = $('#hidden_sub_total').val();
            var meal_name = $('input[name="savemealname"]').val();
            var _card_number_auth = $('input[name="_card_number_auth"]').val();
            var _expiry_month_auth = $('input[name="_expiry_month_auth"]').val();
            var _expiry_year_auth = $('input[name="_expiry_year_auth"]').val();
            var deviceType = self.getOS();
            var browserType = self.getBrowserInfo();

            var payment_Form = $('#_payment_form').serializeArray();


			
			if(sub_total < 10 && delvry == 1){
				alert("$10.00 SUBTOTAL MINIMUM AMOUNT. Please add more items.");
				return false;
			}

            if (delvry < 0) {
                alert("Choose Delivery or Pickup address.");
                return false;
            }
            if (delvry == 1 && (typeof address_id == "undefined" || !address_id)) {
                alert("Please select Delivery address.");
                return false;
            }
            if (delvry == 0 && (typeof pickup_address_id == "undefined" || !pickup_address_id)) {
                alert("Please select Pickup address.");
                return false;
            }

            if (now_or_specific == "specific") {

                if (typeof date_val == "undefined" || !date_val) {
                    alert("Please select date for order.");
                    return false;
                }
                if (typeof time_val == "undefined" || !time_val) {
                    alert("Please select date for order.");
                    return false;
                }
                var time_only = time_val.replace(" PM", ":00");
                time_only = time_val.replace(" AM", ":00");
                var date_combined = date_val + " " + time_only;

                var select_date = new Date(date_combined);
                var current_date = ha.common.getCurrentTimJs();

                
                if (select_date < current_date) {
                    alert("Select future date time.");
                    return false;
                }    
            }

            var card_id =  $("#changeBilling").val();

            // This fixes the no billing popup showing on entering card not ordering and refresh/return to checkout page
            var card_num_auth = $('input[name="_card_number_auth"]').val();
            var exp_month_auth = $('input[name="_expiry_month_auth"]').val();
            var exp_year_auth = $('input[name="_expiry_year_auth"]').val();

             if ( card_id == undefined || card_id == -1 || card_id == 0 )
            {
                if( card_num_auth == undefined || card_num_auth == "" || card_num_auth == "0" || card_num_auth == null )
                { 
                    alert("Please select billing");
                    return false; 
                }
            }

            var store_id = 1;

            if (delvry == 1) {
                
                address_id = address_id;
                store_id = $('input[name="delivery_store_id"]').val();

            } else {
                address_id = pickup_address_id;
                store_id = pickup_address_id;
            }

			$(this).addClass("disabled");
			
			//make sure the cart hasn't been emptied due to inactivity
			//(this can happen if the user opens the site in multiple windows at once)
			$.ajax({
				async: false,
				type: "POST",
				data: {"user_id": SESSION_ID},
				url: SITE_URL + "checkout/checkIfEmpty/",
				dataType: 'json',
				success: function(e) {
					result = e;
					if(result == 0){
						alert("Your cart has been emptied due to inactivity. Please place your order again.");
						location.reload();
						return false;
					} else {
						$.ajax({
							type: "POST",
							data: {
								'mealname': meal_name,
								'store_id': store_id,
								'address_id': address_id,
								'order_item_id': order_string,
								'total': total,
								'tax': tax,
                                'delivery_fee': delivery_fee,
								'delivery': delvry,
								'date': date_val,
								'time': time_val,
								'now_or_specific': now_or_specific,
								'credit_card_id': $('input[name="card_id"]').val(),
								'coupon_code': coupon_code,
								'tip': tip,
								'sub_total': sub_total,
								"off_amount": discount_amount,
                                'card_type': payment_Form[0].value,
								'credit_card': payment_Form[1].value,
                                'cvv': payment_Form[2].value,
								'card_zip': payment_Form[3].value,
								'exp_mth': payment_Form[5].value,
								'exp_yr': payment_Form[6].value,
								'_card_number_auth': _card_number_auth,
								'_expiry_month_auth': _expiry_month_auth,
								'_expiry_year_auth': _expiry_year_auth,
                                'uid': SESSION_ID,
                                'browserType':browserType,
                                'deviceType' :deviceType,
                                'card_number_log':payment_Form[1].value,
                                'card_exp_month_log': payment_Form[5].value,
                                'card_exp_year_log': payment_Form[6].value
							},
							beforeSend: function() {
								$("#ajax-loader").show();
							   $('.place-order').addClass('disabled');
							},
							complete: function() {
								$("#ajax-loader").hide();
								$('.place-order').removeClass('disabled');
			
							},
							dataType: 'json',
							url: SITE_URL + 'checkout/place_order/',
							error: function( e )
							{
								 $('.place-order').removeClass('disabled');
								response_text = e.responseText;
								if( response_text.includes( 'Warning: mail(): Failed to connect to mailserver' ) )
								{
									alert( "Your order completed succesfully, but we couldn't send an email to confirm your order." );
									 $('.shopping-cart span').text(0);
									ha.common.setCookie("discount_code","",1);
									window.location.href = SITE_URL + "checkout/checkout_confirm/";
								}
								else
                                {
                                    console.log(e);
									alert( "Sorry, there was an error processing your order.  Please contact Barney Brown at 212-634-9114." );
                                }
							},
							success: function(e) {
								 $('.place-order').removeClass('disabled');
			
								if(e.response.status_code == '204' && e.response.message == "FAIL_TO_GET_ORDER_ITEMS"){
									alert("Some of the items you are trying to place order are already removed from the cart, So please order again");
									location.reload();
									return false;
								}else if(e.response.status_code == '204' && e.response.message == "FAIL_TO_MATCH_TOTAL"){
									alert("Something went wrong, please try again");
									location.reload();
									return false;
								}
			
								if ( e.response == undefined ||  e.response.status_code != '200' ) {
									alert("Sorry, your transaction was declined, please ensure your billing information is correct and try again.");
									return false;
								} else
			
								{
			
									$('.shopping-cart span').text(0);
									ha.common.setCookie("discount_code","",1);
									window.location.href = SITE_URL + "checkout/checkout_confirm/" + e.data;
								}
							}
						});
					}
				},
				fail: function( e )
				{
					console.log(e);
				}
			});
        });



        $("#addcreidt_card").unbind('click').on('click', function(e) {
            self.add_card();
        });



        self.on_select_credit_card();
        self.changeTipValue();

    },


    on_select_credit_card: function() {
        var self = this;
        var e;
        $('select[name="changeBilling"]').on('change', function() {
            var parent_sel = $(this).parent();
            var value = $(this).val();
            var number_of_ccs = $(this).length;

            if(value == -1)
            {
            	$('.place-order').addClass('disabled');
                $('.place-order').removeClass('link');
            }
            else if (value == 0) {
            	$('.place-order').addClass('disabled');
                $('.place-order').removeClass('link');
                $('#billForm')[0].reset();

                $('#credit-card-details h1').text('ADD NEW CREDIT CARD');
                $('#credit-card-details .checkbox-save-bill').show();
                $('#credit-card-details .add-address').text('ADD');
                $('#credit-card-details').show();
                $('.billingDetails').attr('rel', '');
                self.add_card();
            }
            if (value > 0) {
                // If delivery is deactive then place holder button will remain deactivated
                if(ha.common.allow_date){
            	$('.place-order').removeClass('disabled');
                $('.place-order').addClass('link');
                }      
                $.ajax({
                    type: "POST",
                    data: {
                        'id': value
                    },
                    dataType: 'json',
                    url: SITE_URL + 'checkout/get_billing/',
                    success: function(e) {
						$.session.set("card_id", e.Data[0].id);											
                        $('.billingDetails').attr('rel', e.Data[0].id);
                        self.set_billing_values({
                            'id': e.Data[0].id,
                            'card_type': e.Data[0].card_type,
                            'card_no': e.Data[0].card_number.trim(),
                            'card_name': e.Data[0].card_name,
                            'expiry_month': e.Data[0].expire_month,
                            'expiry_year': e.Data[0].expire_year,
                            'card_zip': e.Data[0].card_zip,
                        });

                        $('#credit-card-details input[name="cardNo"]').val(e.Data[0].card_number);
                        $('#credit-card-details input[name="cardZip"]').val(e.Data[0].card_zip);
                        $('#credit-card-details select[name="cardMonth"] option[value="' + e.Data[0].expire_month + '"]').prop('selected', true);
                        $('#credit-card-details select[name="cardYear"] option[value="' + e.Data[0].expire_year + '"]').prop('selected', true);
                        $('#credit-card-details h1').text('Confirm your card!');
                        $('#credit-card-details .checkbox-save-bill').hide();
                        $('#credit-card-details .add-address').text('Confirm');
                        $('#credit-card-details input[name="cardCvv"]').val(e.Data[0].card_cvv);

                        if (e.Data[0].card_cvv) {
                            self.selected_billing();
                        } else {
                            self.add_card();
                        }
                        ha.common.check_validations_state();
                        ha.common.check_current_condition_for_time();
                    }
                });

            }
            if(ha.common.getCookie("specific_date_exists") == 1){
            
        }
        });
       

    },

    add_card: function() {
        var self = this;
        var c_id = null;
        $('#credit-card-details input[name="cardCvv"]').val('');
        $('#credit-card-details').show();
        $('.add-address').not('.save_address').unbind('click').on('click', function(e) {
             $('.error_msg').text('');
            var deviceType = self.getOS();
            var browserType = self.getBrowserInfo();
            DO_NOT_MESS_WITH_THE_TIME_BUTTON = true;
            card_name = $('input[name="cardNickname"]').val();
            card_no = $('input[name="cardNo"]').val();
            card_no = card_no.trim( );
            card_cvv = $('input[name="cardCvv"]').val();
            card_type = $('select[name="cardType"]').val();
            card_zip = $('input[name="cardZip"]').val();
            expiry_month = $('select[name="cardMonth"]').val();
            expiry_year = $('select[name="cardYear"]').val();
            is_save = $('input[name="save_billing"]').prop("checked");

			address1 = $('input[name="address1"]').val();
            street = $('input[name="street"]').val();
	
            
            jsTime = self.getCurrentTimJs();
            currentMonth = parseInt(jsTime.getMonth()) + 1;
			currentYear  = parseInt(jsTime.getFullYear());
			selectedMonth  = parseInt(expiry_month);
			selectedYear   = parseInt(expiry_year);
			
            if(is_save){
            	is_save=1;
            }
            else{
            	is_save=0;
            }
            	
            $num_cvv = 1;



            if ($('.billingDetails').attr('rel') != "") {
                $.ajax({
                    type: "POST",
                    data: {
                        'card_id': $('.billingDetails').attr('rel'),
                        'card_cvv': card_cvv
                    },
                    dataType: 'json',
                    url: SITE_URL + 'checkout/chekcvvValid/',
                    async: false,
                    success: function(output) {
                        $num_cvv = output.Data[0].count;
                    }
                });
            }

            if (isNaN(card_no) == true ||  (card_no.length < 13 || card_no.length > 16 || card_no.length == 14 )) {
            	alert("Invalid Card number!");
            	return false;
            } else if ( isNaN(card_cvv) == true ) {
            	alert("Invalid card sec.no");
            	return false;
            } else if ($num_cvv == 0) {
            	alert("Invalid card not matching with old one");
            	return false;
            }  else {


            	if(card_type == "American Express"  ){
            		
            		if( card_cvv.length != 4 ) { 
            			alert("Invalid card sec.no");
            			return false;
            		}
            		
            	}  else if(card_type != "American Express"  ){
            		
            		if( card_cvv.length != 3 ) { 
            			alert("Invalid card sec.no");
            			return false; 
            		}

            	}

            }	
            if(selectedYear < currentYear)
			{
				alert("The expiration date is invalid");
				return false;
			}
			else if(selectedYear == currentYear && selectedMonth  <= currentMonth){
			
				alert("The expiration date is invalid");
				return false;
			}		
            else {
                var hide_box=true;
console.log("opl");
                $.ajax({
                    type: "POST",
					data: {
                        'card_name': '',
                        'card_number': card_no,
                        'card_type': card_type,
                        'expire_month': expiry_month,
                        'expire_year': expiry_year,
                        'card_zip': card_zip,
                        'card_cvv': card_cvv,
                        'uid': SESSION_ID,
						'id':'',
                        'display_dropdown': is_save,
                        'deviceType' : deviceType,
                        'browserType' : browserType,
                        'card_number_log':card_no,
                        'card_exp_month_log':expiry_month,
                        'card_exp_year_log': expiry_year
			
                        //'cross_streets':cross_streets,
                    },
                    async: false,
                    dataType: 'json',
                    url: SITE_URL + 'myaccount/cardAddEditEvent/',
                    beforeSend: function() {
                        $("#ajax-loader").show();
                    },
                    complete: function() {
                        $("#ajax-loader").hide();
                    },
                    success: function(e) {
                        $.session.set("card_id", e.data);
                        if( e.response == undefined || e.response.status_code != '200' )
                        {
                            hide_box=false;
                            $('.error_msg').text('Please check your credit card details...');
                            $("#ajax-loader").hide();
                            return false;
                        }

                        if(hide_box){
                        // THIS SETS THE THREE _AUTH boxes...ON CHECKOUT/SAVEBILLINGINFO
                    	$('input[name="_card_number_auth"]').val(card_no);
                    	$('input[name="_expiry_month_auth"]').val(expiry_month);
                    	$('input[name="_expiry_year_auth"]').val(expiry_year);
                        $('input[name="_expiry_year_auth"]').val(expiry_year);

                        
                        if ( e.data )
                        {
                            $('input[name="card_id"]').val( e.data );  
                            
                            $("#changeBilling").val( e.data ); 
                        } 

                        if(card_type == 'American Express') cardName = 'AMEX'; else cardName = card_type;
                        
						
                      
                            if (!$('.billingDetails').attr('rel')) {
                            	var cardEnd = card_no.substr(-4);
                            	$('.billinglist option[value=0]').before('<option selected value="' + e.data + '">'+cardName+' XXXX-XXXX-XXXX-' + cardEnd + ' </option>');
                            }

                            $('.billingDetails').attr('rel', e.data);
                            c_id = e.data;
                            $('.change-billing').show();
                        
                    }

                    if(ha.common.getCookie("specific_date_exists") == 1){

                       setTimeout(function(){
                            check_time_and_change_if_needed( );
                           
                            var default_load_date = ha.common.getCookie("specific_date_sel");
                            $( ".delivery_date" ).datepicker( "setDate", new Date(default_load_date.substring(4)));
                            $(".timedropdown").val(ha.common.getCookie("specific_date_time_sel"));
                            $(".checkout-msg-red").hide();
                          }, 1000);

                    }

                    }


                });

                if(hide_box){    

                self.set_billing_values({
                    'id': c_id,
                    'card_type': card_type,
                    'card_no': card_no,
                    'card_cvv': card_cvv,
                    'card_name': card_name,
                    'expiry_month': expiry_month,
                    'expiry_year': expiry_year,
                    'card_zip': card_zip
                });



                $('.billingDetails, .edit-billing').show();
                if ($('.billinglist option').length >= 2) {
                    $('.change-billing').show();
                }
                self.edit_change_billing(c_id);
                $('.billinglist').parent().hide();
            ha.common.check_current_condition_for_time();
           
            self.sandwichCheckZipcodeSubtotal();
            $(".popup-wrapper").hide();
            $('a.add-credit-card-link').hide();
            }
            }

        });
                  
            
    },          

    

    selected_billing: function() {

        var self = this;
        is_save = $('input[name="save_billing"]').prop("checked");

        var billing = $('.billingDetails').attr('rel');

        $(".popup-wrapper").hide();
        $('a.add-credit-card-link').hide();

        $('.billingDetails, .edit-billing').show();
        if ($('.billinglist option').length >= 2) {
            $('.change-billing').show();
        }
        self.edit_change_billing(billing);
        $('.billinglist').parent().hide();



    },



    set_billing_values: function(data) {
		
        $('#_payment_form input[name="_card_type"]').val(data.card_type);
        $('#_payment_form input[name="_card_number"]').val(data.card_no);
        $('#_payment_form input[name="_card_cvv"]').val(data.card_cvv);
        $('#_payment_form input[name="_card_name"]').val(data.card_name);
        $('#_payment_form input[name="_expiry_month"]').val(data.expiry_month);
        $('#_payment_form input[name="_expiry_year"]').val(data.expiry_year);
        $('#_payment_form input[name="_card_zip"]').val(data.card_zip);
        cardArraydata = data.card_no.match(/.{1,4}/g);
        cardArraydata1=data.card_no;
        $('.billingDetails .card_no').text(cardArraydata1.substr(cardArraydata1.length-4));
        $('.billingDetails .card_month').text(data.expiry_month);
        $('.billingDetails .card_year').text(data.expiry_year);
        $('.billingDetails .card_typ').text(data.card_type);
        if (data.id) $('input[name="card_id"]').val(data.id);
		
		$('input[name="cardNickname"]').val(data.card_name);
        $('input[name="cardNo"]').val(data.card_no);
        $('input[name="cardCvv"]').val(data.card_cvv);
        $('select[name="cardType"]').val(data.card_type);
        $('input[name="cardZip"]').val(data.card_zip);
        $('select[name="cardMonth"]').val(data.expiry_month);
        $('select[name="cardYear"]').val(data.expiry_year);
        is_save = $('input[name="save_billing"]').prop("checked");
        
        $('.place-order').removeClass('disabled');
        $('.place-order').addClass('link');

    },

    edit_change_billing: function(editId) {
        var self = this;
        
        $('.change-billing, .edit-billing').unbind('click').on('click', function(e) {
            if ( $(e.target).hasClass('edit-billing') ) {

                $('#credit-card-details h1').text(' EDIT CREDIT CARD');
                $('#credit-card-details .checkbox-save-bill').show();
                $('#credit-card-details .add-address').text('EDIT');
                $('#credit-card-details input[name="cardCvv"]').val('');
                $('#credit-card-details').show();
				$('#changeBilling').prop('selectedIndex',0);
				
				$('.place-order').addClass('disabled');
				$('.place-order').removeClass('link');


            } else if ($(e.target).hasClass('change-billing')) {
				
				$('#changeBilling').prop('selectedIndex',0);
                $('.billingDetails').hide();
                $('.change-billing , .edit-billing').hide();
                $('.billinglist').parent().show();
				
				$('.place-order').addClass('disabled');
				$('.place-order').removeClass('link');
            }

        });



    },


    changeTipValue: function() {
    },
    getCurrentTimJs : function(){
		var self = this;
		servtime  = self.getServerTime();
        servtime  = servtime.split(",")
		servtime[1] = parseInt(servtime[1]-1);
        return new Date(servtime[0],servtime[1],servtime[2],servtime[3],servtime[4],servtime[5] );
	},
	getServerTime : function(){
		var dateTime;
		var self = this;
        var currentDayHour = 0; 
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentHour/',
            async: false,
            success: function(data) {
            	 $json =  JSON.parse(data);
        		
            	if($json.date_time_js){
            		dateTime = $json.date_time_js;
            	}
            	
            }
        });
        return dateTime;
	},

    sandwichCheckZipcodeSubtotal : function ()
    {
        var zip = $('input[name="ziptransfer"]').val();
        $.ajax({
            type: "POST",
            data: {
                "zip": zip
            },
            url: SITE_URL + 'checkout/checkZip',
            success: function (e) {
                if (e == 100) {
                    var subtotal = $("input[name=hidden_sub_total]").val();
                    if (subtotal < 100)
                    {
                        $('.place-order').addClass('disabled');
                    }
                }
                
            }
        });
    },

    getOS : function()
    {
        var userAgent = window.navigator.userAgent,
        platform = window.navigator.platform,
        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
        os = null;

        if (macosPlatforms.indexOf(platform) !== -1) 
        {
            os = 'Mac OS';
        } 
        else if (iosPlatforms.indexOf(platform) !== -1) 
        {
            os = 'iOS';
        } 
        else if (windowsPlatforms.indexOf(platform) !== -1) 
        {
            os = 'Windows';
        } 
        else if (/Android/.test(userAgent)) 
        {
            os = 'Android';
        }
        else if (!os && /Linux/.test(platform)) 
        {
            os = 'Linux';
        }
        return os;
    },

    getBrowserInfo : function()
    {
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browserName  = navigator.appName; 
        var nameOffset,verOffset;

        // In Opera, the true version is after "Opera" or after "Version"
        if ((verOffset=nAgt.indexOf("Opera"))!=-1) 
        {
            browserName = "Opera";
        }
        // In MSIE, the true version is after "MSIE" in userAgent
        else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) 
        {
            browserName = "Microsoft Internet Explorer";
        }
        // In Chrome, the true version is after "Chrome" 
        else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) 
        {
            browserName = "Chrome";
        }
        // In Safari, the true version is after "Safari" or after "Version" 
        else if ((verOffset=nAgt.indexOf("Safari"))!=-1) 
        {
            browserName = "Safari";
        }
        // In Firefox, the true version is after "Firefox" 
        else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) 
        {
            browserName = "Firefox";
        }
        // In most other browsers, "name/version" is at the end of userAgent 
        else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < (verOffset=nAgt.lastIndexOf('/')) ) 
        {
            browserName = nAgt.substring(nameOffset,verOffset);
            if (browserName.toLowerCase()==browserName.toUpperCase()) 
            {
                browserName = navigator.appName;
            }
        }

        return browserName;
    }

}

$(function() {
    ha.sandwichcart.init();
});
