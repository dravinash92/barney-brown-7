var ha = {};
ha.sandwichcreator = window.ha.sandwichcreator || {
    vars: {
        temp_id: 0,
        cheese_min: 0,
        AjaxURL: {
            BREAD: "createsandwich/load_bread_data/",
            PROTEIN: "createsandwich/load_protien_data/",
            CHEESE: "createsandwich/load_cheese_data/",
            TOPPINGS: "createsandwich/load_toppings_data/",
            CONDIMENTS: "createsandwich/load_condiments_data/"
        },
        manual_mode: false,
        current_sliced_image: null,
        ACTIVE_MENU: $('.create-sandwich-menu ul li a[class="active"]').text(),
        selection_data: null,
        validators: ["BREAD", "PROTEIN", "CHEESE"],
        bread_type: null,
        image_path: ADMIN_URL + 'upload/',
        current_price: 0,
        individual_price: {},

        ingredient_details: {
            BREAD: {
                item_name: [],
                item_price: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                type: 0, // either 3 foot or 6 foot or normal [0-normal,1-3-foot,2-6-foot]
                shape: 0, // either long,round,trapezoid [0-long,1-3-round,2-6-trapezoid]
                actual_price: {}
            },
            PROTEIN: {
                // manual_mode: false,
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            CHEESE: {
                // manual_mode: false,
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            TOPPINGS: {
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            },
            CONDIMENTS: {
                item_name: [],
                item_price: [],
                item_priority: [],
                item_qty: {},
                item_id: [],
                item_image: [],
                actual_price: {},
                option_default_Choice_first: 1,
                option_default_Choice_second: 0,
            }
        },
        
        appliedDiscount: 0
    },



    init: function() {
        var e = this;
        e.vars.bread_type = e.get_url_params('view');
        if (e.get_url_params('view')) $('.landing-page').hide();
        
        $(window).load(function() {
            if(window.location.href.indexOf('sandwich/gallery') == -1 )
            e.retrive_data_onload();
        });
        e.ingredient_enable();
        e.slide_function();
        $("#finished").unbind("click").on("click", function() {



            error = e.validate($(this).text());
            if (error == true) {
                return false
            } else {
                if (SESSION_ID == "" || SESSION_ID == null) {
                    ha.common.setCookie('sandwich_end_screen', true, 1);
                    if(ha.common.getCookie("facebookOnlyUser") == true){ ha.common.facebook_login(); } 
                    else{  ha.common.login_popup(); }
                   
                } else
                    e.show_end();
            }
        });

        e.listing_click();
        e.add_product_descriptor_to_cart();
        e.clearAllSandwiches();
    },
    
    clearAllSandwiches: function(){
        
        var self = this;
        $(".clear-all-button").click(function(){
            self.clear_all_selections();
        });
    },
    
    destroy_endscreen_var: function() {
        ha.common.setCookie('sandwich_end_screen', false, 1);
    },

   preload_sandwich_images: function() {
        $('.optionLoader').show();
        var images = [];
        $.ajax({
            type: "POST",
            dataType: "json",
            url: SITE_URL + 'createsandwich/preload_images/',
            success: function(e) {
                
                  if(e.length>0){
                   e.forEach(function(i,j){
                     $img = ADMIN_URL+"/upload/"+i;
                     images[j] = new Image()
                     images[j].src = $img;
                   });
                  }
                $('.optionLoader').hide();
            }
        });
        
    },

    get_url_params: function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        return results == null ? null : results[1];
    },

    
    gb: 0,

    listing_click: function() 
    {
        var self = this;

        $('.savetousertoast').on('click', function() {
            session.session_state = ha.common.check_session_state();
            if (session == false) {
                if ($(this).is(':checked')) {
                    $(this).prop('checked', false);
                } else {
                    $(this).prop('checked', true);
                }
                ha.common.login_popup($(this));
                return;
            }


            var item_id = $("#hidden_sandwich_id").val();
            var sandwich_ = $('li[data-id="' + item_id + '"]');
            var toast = 0;

            if ($(this).is(':checked')) {
                var toast = 1;
            }

            if (toast == 1) {
                $(sandwich_).find('.toasted').css('display', 'block');
            } else $(sandwich_).find('.toasted').css('display', 'none');

            $.ajax({
                type: "POST",
                data: {
                    "toast": toast,
                    "item_id": item_id
                },
                url: SITE_URL + 'sandwich/updateToastmenu',
                dataType: "json",
                success: function(e) {
                    if (e) {
                        sandwich_.data('toast', e.Data);
                    }
                }

            });

        });

        $('.popup-wrapper a:contains(Like)').unbind('click').on('click', function(e) 
        {
            var u_id = $("#hidden_uid").val();
            e.preventDefault();
            var sandwich_id;
            sandwich_id = $('.like-sandwich').parent().find('#hidden_sandwich_id');
            sandwich_id = $(sandwich_id).val();

            session = ha.common.check_session_state();
            if (session.session_state == false) 
            {
                if(session.facebook_only_user == false)
                {
                   ha.common.login_popup($(this));
                } 
                else 
                {
                   ha.common.facebook_login($(this));
                }
                return;
            }
            
            var sandwich_ = $('li[data-id="' + sandwich_id + '"]');
            var like_count;


            if ($(this).text() == 'Liked') return;

            current_like_count = $('.like-sandwich').parent().parent().find('.likescount').text();
            current_like_count = parseInt(current_like_count);
            like_count = current_like_count + 1;
            $('.like-sandwich').parent().parent().find('.likescount').text(like_count);
            $('.menu-listing li[data-id="' + sandwich_id + '"]').find('.inner-holder p').text(like_count + "  likes  ");

            $.ajax({
                type: "POST",
                data: {
                    'id': sandwich_id,
                    "uid": SESSION_ID
                },
                url: SITE_URL + 'sandwich/addLike/',
                beforeSend: function() {
                    $("#ajax-loader").show();
                },
                complete: function() {
                    $("#ajax-loader").hide();
                },
                success: function(e) {
                    $(".like-sandwich").text("Liked");
                    if (e) {
                        $data = JSON.parse(e);
                        id = $data.Data;
                        sandwich_.data('likeid', id);
                        sandwich_.data('likecount', like_count);
                    }
                }
            });

        });
        $('.menu-listing a:contains(EDIT)').unbind('click');
        $(document).on('click','.menu-listing a:contains(EDIT)',function(x) 
        {
            x.preventDefault();
            self.clear_temp_session();
            session = ha.common.check_session_state();
            if (session.session_state == false) 
            {
                $("#sandwiches-popup").hide();
                if(session.facebook_only_user == false)
                {
                    ha.common.login_popup($(this));
                } 
                else 
                {
                    ha.common.facebook_login($(this));
                }
                return;
            }
            var parent;
            parent = $(this).parent().parent();
            src = parent.find('img').attr('src');
            sType = parent.find('.typeSandwich').val();
            var isSaved = $(this).next('a').text();
            
            pdata = parent.data();
            $.ajax({
                type: "POST",
                url: SITE_URL + 'sandwich/getCurrentprice',
                data: {'pdata': pdata},
                success: function(price) {
                   //window.location.href = SITE_URL+"createsandwich/index/"+pdata.id;
                   pdata.price = price;
                }
            })

            var id = pdata.id;

            var bread = pdata.bread;

            console.log("bread", bread);
            $.ajax({
                type: "POST",
                url: SITE_URL + 'createsandwich/quickEditSandwich',
                data: {'id': id},
                success: function(data) {
                    
                    data = JSON.parse(data);
                    console.log("data", data);
                    var bread_options_html = '';

                    console.log("sandwich_name", data.sandwich_name);

                    $.each(data.bread_data, function(index, bread_data){
                       var selected =  bread_data.item_name == bread ? "selected" : "";

                        bread_options_html += '<option '+ selected +' data-shape="'+ bread_data.bread_shape +'" data-bread_type="'+ bread_data.bread_type +'" data-type="replace" data-id="'+ bread_data.id +'" data-itemname="'+ bread_data.item_name +'" value="'+ bread_data.id +'" data-price="'+ bread_data.item_price +'" data-categoryid="'+ bread_data.category_id +'" data-image="'+ bread_data.item_image +'" data-item_image_sliced="'+ bread_data.item_image_sliced
                     +'">'+ bread_data.item_name +'</option>';
                    })
                    $("#sandwiches_quick_edit #bread_drpdown").html(bread_options_html);
                    $("#sandwiches_quick_edit input[name='sandwich_name']").val(data.sandwich_name);
                    $("#sandwiches_quick_edit #save-quickedit-sandwich").attr("data-issaved", isSaved);
                }
            })

            
            self.retrive_data_on_quickEdit(id);
            self.slide_function_on_quick_edit();
        });

        /*
        ,.menu-listing a:contains(SAVE / SAVED)
        */
        $('.menu-listing a:contains(VIEW),.menu-listing img,.menu-listing a:contains(ADD TO CART)').unbind('click');
        $(document).on('click','.menu-listing a:contains(VIEW),.menu-listing img,.menu-listing a:contains(ADD TO CART)',function(x) 
        {
            x.preventDefault();
            var parent;
            if($(this).hasClass('view_sandwich')){
                parent = $(this).parent().next().find('ul');
            }else{
                parent = $(this).parent().parent();
            }
            pdata = parent.data();
            
            //src = parent.find('img').attr('src');
            src = $("#sandImg_"+pdata.id).val();
            sType = parent.find('.typeSandwich').val();
            
            

            //console.log(pdata.toast);return;

            $ptxt = ""
            breadTxt = pdata.bread;
            breadTxt = breadTxt.trim();
            breadTxt = breadTxt.replace(/##/g, " ");
            $ptxt = breadTxt;

            session = ha.common.check_session_state();
            

             $('#sandwiches_quick_edit_add_item a.cancel-add-item').click(function(){
                $("#sandwiches_quick_edit_add_item").hide();
                //$("#sandwiches_quick_edit").hide();
             })


            $('.popup_spinner .left_spinner').unbind('click').on('click', function() 
            {
                input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            $('.popup_spinner .right_spinner').unbind('click').on('click', function() 
            {
                input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });


            if (pdata.protien != "") 
            {
                protiens = pdata.protien.replace(/ /g, ', ')
                protiens = protiens.trim();
                protiens = protiens.replace(/##/g, " ");
                if ($ptxt) $dl = ', ';
                else $dl = '';
                $ptxt += $dl + protiens;
            }
            if (pdata.cheese != "") 
            {
                chesses = pdata.cheese.replace(/ /g, ', ')
                chesses = chesses.trim();
                chesses = chesses.replace(/##/g, " ");
                $ptxt += chesses;
            }
            if (pdata.topping != "") 
            {
                toppings = pdata.topping.replace(/ /g, ', ')
                toppings = toppings.trim();
                toppings = toppings.replace(/##/g, " ");
                if ($ptxt) $dl = ', ';
                else $dl = '';
                $ptxt += toppings;
            }
            if (pdata.cond != "") 
            {
                conds = pdata.cond.replace(/ /g, ', ')
                conds = conds.trim();
                conds = conds.replace(/##/g, " ");
                $ptxt += conds;
            }

            txt = $(this).text();
            if ($(this).attr("class") == "view_sandwich") 
            {
                txt = 'VIEW';
            }

            var alttxt = ""
            if (txt == "") 
            {
                alttxt = $(this).attr("alt");
            }
            if (txt === 'ADD TO CART') 
            {
                return;
            }

            

            if (txt === 'VIEW' || txt === 'ADD' || alttxt === 'sandwitchimageview' ) 
            {
                if (pdata.flag == 0) 
                {
                    $('.flagthis').css('text-decoration', 'underline');
                } 
                else 
                {
                    $('.flagthis').css('text-decoration', 'none');
                }

                $('.share-sandwich-popup').unbind('click').on('click', function() 
                {
                    session = ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        $("#sandwiches-popup").hide();
                        if(session.facebook_only_user == false)
                        {
                            ha.common.login_popup($(this));
                        } 
                        else 
                        {
                            ha.common.facebook_login($(this));
                        }
                        return;
                    }
                    main = $(this).parent().parent();
                    main = $(main);
                    sandwichname = main.find('.title-holder h1').text();
                    sandwichname = sandwichname.toUpperCase();
                    sandwichDesc = main.find('p:first').text()
                    $img = main.find('img:first').attr('src');
                    sandwichID = $(main.find('.save-menu')).attr('rel');

                    var feed = {
                        method: 'feed',
                        app_id: FB_APP_ID,
                        name: sandwichname,
                        description: sandwichDesc,
                        link: SITE_URL + 'sandwich/gallery/?galleryItem=' + sandwichID,
                        picture: $img,
                        display: 'popup'
                    };
                    //console.log(feed);return;
                    FB.ui(feed,  function ( response ){ }  );
                });

                $("#sandwiches-popup").find('.savetousertoastnew').prop('checked', false);
                $("#sandwiches-popup").find('#hidden_uid').val(pdata.id);
                $("#sandwiches-popup").find('#hidden_sandwich_id').val(pdata.id);

                if ((pdata.toast == 1)) 
                {
                    $("#sandwiches-popup").find('.savetousertoastnew').prop('checked', true);
                }

                $("#sandwiches-popup").find('.username').text(pdata.username+'.');
                $("#sandwiches-popup").find('.likescount').text(pdata.likecount);
                $("#sandwiches-popup").find('#likecountid').text(pdata.likecount);
                $("#sandwiches-popup").find('#menuadds').text(pdata.menuadds);
                $("#sandwiches-popup").find('.text-box').val('01');

                if (pdata.flag == 1) 
                {
                    $('.flagthis').text('This sandwich has been flagged');
                    $('.flag-sandwich').text('This sandwich has been flagged');
                    $('.flag-sandwich').css('text-decoration', 'none');
                    $('.flag-sandwich').css('cursor', 'auto')
                } 
                else 
                {
                    $('.flagthis').text('flag this sandwich');
                    $('.flag-sandwich').text('flag this sandwich');
                    $('.flag-sandwich').css('text-decoration', 'underline');
                    $('.flag-sandwich').css('cursor', 'pointer')
                }

                $('.flagthis').unbind('click').on('click', function(e) 
                {
                    e.preventDefault();
                    $parent = $(this).parent().parent().parent();
                    $txt = $(this).text();
                    id = $($parent).find('input[name="hidden_sandwich_id"]');

                    if ($txt == 'flagged') {

                    } else {
                        $(this).text('This sandwich has been flagged');
                        $(this).css('text-decoration', 'none');
                        $(this).css('cursor', 'auto');
                        self.set_flag_state({
                            'state': 1,
                            'id': id.val()
                        });
                    }
                });

                if (pdata.likeid)
                    $(".like-sandwich").text("Liked");
                else $(".like-sandwich").text("Like");

                $("#sandwiches-popup").find('.datefromat').text(pdata.formatdate);
                image = $("#sandwiches-popup").find('img:nth-child(2)');
                $("#sandwiches-popup").find('h1').text(pdata.name);
                $.ajax({
                    type: "POST",
                    url: SITE_URL + 'sandwich/getCurrentprice',
                    data: {'pdata': pdata},
                    success: function(data) {
                        $("#sandwiches-popup").find('h2').text('$' + data);
                    }
                })
                /*$("#sandwiches-popup").find('h2').text('$' + pdata.price);*/
                $($("#sandwiches-popup p")[0]).text(pdata.sandwich_desc);
                $(image).attr({
                    'src': src,
                    'width': '350',
                    'height': '194'
                });
                $("#sandwiches-popup .add-to-cart").text($(parent).attr('rel'));
                $("#sandwiches-popup .edit-sandwich-popup").attr('rel', pdata.id);
                $("#sandwiches-popup .save-menu").attr({rel: pdata.id, sType: sType});

                if (SESSION_ID) session = SESSION_ID;
                else session = ha.common.getCookie("user_id");
                session = parseInt(session);
                //if ($('input[name="chkmenuactice' + pdata.id + '"]').length > 0 && session == parseInt(pdata.userid))
                if ($('input[name="chkmenuactice' + pdata.id + '"]').length > 0) 
                {
                    if ($('input[name="chkmenuactice' + pdata.id + '"]').val() == 1)
                    {
                        // if (window.location.href.indexOf("menu") != -1) 
                        // {
                        //     $('.save-menu').text('SAVED');
                        //     $('.save-menu').addClass('remove_menu');
                        // } 
                        // else 
                        // {
                        //     $('.save-menu').text('SAVED');
                        // }
                        $('.save-menu').text('SAVED');
                        $('.save-menu').addClass('remove_menu');
                    } 
                    else 
                    {
                        $('.save-menu').text('SAVE');
                        $('.save-menu').removeClass('remove_menu');
                    }
                }

                if ($('input[name="saved_tgl' + pdata.id + '"]').val() != "undefined" && $('input[name="saved_tgl' + pdata.id + '"]').val() > 0)
                {
                        $('.save-menu').text('SAVED');
                        $('.save-menu').addClass('remove_menu');
                        $('.check-sandwich-private').show();
                } 
                else 
                {
                    $('.save-menu').text('SAVE');
                    $('.save-menu').removeClass('remove_menu');
                    $('.check-sandwich-private').hide();

                }

                var isPub = $("#isPublic" + pdata.id).val();
                //console.log("isPub "+isPub);
                if(isPub == 0)
                {
                    $('.make_private_check').prop('checked', true);
                }
                else
                {
                     $('.make_private_check').prop('checked', false);
                }


                $("#sandwiches-popup").show();
                var toast_sandwich = pdata.toast;
                $(".menucheck").unbind('change').on('change', function(e)
                {
                   toast_sandwich = $('.menucheck').is(':checked')?1:0;
                   if(sType == "SS")
                   {
                        session = ha.common.check_session_state();
                        if (session.session_state == false) 
                        {
                            if(session.facebook_only_user == false)
                            {
                               ha.common.login_popup($(this));
                            } 
                            else 
                            {
                               ha.common.facebook_login($(this));
                            }
                            return;
                        }
                        var sandwich_ = $('li[data-id="' + pdata.id + '"]');
                        $.ajax({
                            type: "POST",
                            data: {
                                "toast": toast_sandwich,
                                "item_id": pdata.id
                            },
                            url: SITE_URL + 'sandwich/updateToastmenu',
                            dataType: "json",
                            success: function(e) 
                            {
                                $("#toastID_"+pdata.id).data('toast',toast_sandwich);
                                if (toast_sandwich == 1) 
                                {
                                    $(sandwich_).find('.toasted').css('display', 'block');
                                } 
                                else 
                                {
                                    $(sandwich_).find('.toasted').css('display', 'none');
                                }
                            }

                        });
                   }
                 
                });
                $(".save-menu").unbind('click').on('click', function(e)
                {
                    e.preventDefault();
                    session = ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        $("#sandwiches-popup").hide();
                        if(session.facebook_only_user == false)
                        {
                            ha.common.login_popup($(this));
                        } 
                        else 
                        {
                            ha.common.facebook_login($(this));
                        }
                        return;
                    }
                    $('input[name="chkmenuactice' + pdata.id + '"]').val('1');
                    $('#save_toggle'+pdata.id).text("SAVED");
                    $('#save_toggle'+pdata.id).addClass('saved_btn');
                    

                    
                    if ($(this).text().indexOf('SAVED') != -1) return;

                    session = ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        if(session.facebook_only_user == false)
                        {
                           ha.common.login_popup($(this));
                        } 
                        else 
                        {
                           ha.common.facebook_login($(this));
                        }
                        return;
                    }

                    id = $(this).attr('rel');
                    var element = this;
                    
                    if ($(".save-menu").hasClass('removemenu') == false) 
                    {
                        $.ajax({
                            type: "POST",
                            data: {
                                'id': id,
                                "uid": SESSION_ID,
                                "toast_sandwich":toast_sandwich
                            },
                            url: SITE_URL + 'sandwich/add_menu_multi/',
                            beforeSend: function() {
                                $("#ajax-loader").show();
                            },
                            complete: function() {
                                $("#ajax-loader").hide();
                            },
                            success: function(e) {
                                if(window.location.href.indexOf('friends_menu')>-1){
                                    //$("#sandwiches-popup").hide(); 
                                }else{
                                    $(element).text("SAVED");
                                    //$("#sandwiches-popup").hide(); 
                                    
                                }
                            }
                        });
                    }
                });

                $(".remove_menu").unbind('click').on('click', function(e)
                {
                    e.preventDefault();
                    session = ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        $("#sandwiches-popup").hide();
                        if(session.facebook_only_user == false)
                        {
                            ha.common.login_popup($(this));
                        } 
                        else 
                        {
                            ha.common.facebook_login($(this));
                        }
                        return;
                    }
                    id = $(this).attr('rel');
                    sType = $(this).attr('sType');
                    //console.log("id "+id+" sType "+sType);return;
                    $.ajax({
                        type: "POST",
                        data: {
                            'id': id,
                            "uid": SESSION_ID,
                            "sType": sType
                        },
                        url: SITE_URL + 'sandwich/remove_saved/',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete: function() {
                            $("#ajax-loader").hide();
                        },
                        success: function(e) {}
                    });

                    
                    if ($('input[name="chkmenuactice' + pdata.id + '"]').length == 0) 
                    {
                        $("#sandwiches-popup").hide();
                        $('.menu-listing li[data-id="' + id + '"]').remove();

                    } 
                    else 
                    {
                        $('.save-menu').text('SAVE');
                        $('.save-menu').removeClass('remove_menu');
                        $("#sandwiches-popup .save-menu").removeAttr('rel', pdata.id);
                        $('input[name="chkmenuactice' + pdata.id + '"]').remove();
                        $("#sandwiches-popup").hide();
                        $('.menu-listing li[data-id="' + id + '"]').remove();
                    }
                });

                $(".make_private_check").unbind('change').on('change', function(e)
                {
                    session = ha.common.check_session_state();
                    if (session.session_state == false) 
                    {
                        $("#sandwiches-popup").hide();
                        if(session.facebook_only_user == false)
                        {
                            ha.common.login_popup($(this));
                        } 
                        else 
                        {
                            ha.common.facebook_login($(this));
                        }
                        return;
                    }
                   var ck_val = (this.checked)?0:1;
                   var id = pdata.id;
                   var sType = parent.find(".typeSandwich").val();
                   $.ajax({
                            type: "POST",
                            data: {
                                "id": id,
                                "uid": SESSION_ID,
                                "is_public":ck_val,
                                "sType":sType
                            },
                            url: SITE_URL + 'sandwich/make_sandwich_private/',
                            success: function(e) 
                            {
                            }
                        });
                })

                $("#sandwiches-popup").find('.add-to-cart').unbind('click').on('click', function(e) 
                {
                    var toast = 0;
                    if ($(".menucheck").is(":checked")) 
                    {
                        toast = 1;
                    }
                    e.preventDefault();

                    qty = $('input[name="itemQuty"]').val();
                    if (toast) 
                    {
                        self.add_toast_sandwich_to_cart(pdata.id, toast, null, null, qty, $(this));
                    } 
                    else 
                    {                        
                        self.add_sandwich_to_cart(pdata.id, null, null, qty, $(this));
                    }
                    $("#sandwiches-popup").hide();
                });
            }
        });

        $('.edit-sandwich-popup').unbind('click').on('click', function(e) 
        {
            e.preventDefault();
            session = ha.common.check_session_state();
            if (session.session_state == false) 
            {
                $("#sandwiches-popup").hide();
                if(session.facebook_only_user == false)
                {
                    ha.common.login_popup($(this));
                } 
                else 
                {
                    ha.common.facebook_login($(this));
                }
                return;
            }
            var id = $(this).attr('rel');

            var bread = $("#sandwiches-popup .title-holder").next().text().split(',');
            var isSaved = $(this).next('a').text();

            console.log("bread", bread);
            $.ajax({
                type: "POST",
                url: SITE_URL + 'createsandwich/quickEditSandwich',
                data: {'id': id},
                success: function(data) {
                    
                    data = JSON.parse(data);
                    console.log("data", data);
                    var bread_options_html = '';

                    console.log("sandwich_name", data.sandwich_name);

                    $.each(data.bread_data, function(index, bread_data){
                       var selected =  bread_data.item_name == bread[0] ? "selected" : "";

                        bread_options_html += '<option '+ selected +' data-shape="'+ bread_data.bread_shape +'" data-bread_type="'+ bread_data.bread_type +'" data-type="replace" data-id="'+ bread_data.id +'" data-itemname="'+ bread_data.item_name +'" value="'+ bread_data.id +'" data-price="'+ bread_data.item_price +'" data-categoryid="'+ bread_data.category_id +'" data-image="'+ bread_data.item_image +'" data-item_image_sliced="'+ bread_data.item_image_sliced
                     +'">'+ bread_data.item_name +'</option>';
                    })
                    $("#sandwiches_quick_edit #bread_drpdown").html(bread_options_html);
                    $("#sandwiches_quick_edit input[name='sandwich_name']").val(data.sandwich_name);
                    $("#sandwiches_quick_edit #save-quickedit-sandwich").attr("data-issaved", isSaved);
                }
            })

            
            self.retrive_data_on_quickEdit(id);
            self.slide_function_on_quick_edit();
           /*$("#sandwiches_quick_edit").show();
           $("#sandwiches-popup").hide();*/
        })
        
        $('.menu-listing a:contains(SAVE),.menu-listing a:contains(SAVED)').unbind('click');
        $(document).on('click','.menu-listing a:contains(SAVE),.menu-listing a:contains(SAVED)',function(x) 
        {

            x.preventDefault();
            var parent;
            parent = $(this).parent().parent();            
            pdata = parent.data();
            session = ha.common.check_session_state();
                if (session.session_state == false) 
                {
                    if(session.facebook_only_user == false)
                    {
                        ha.common.login_popup($(this));
                    } 
                    else 
                    {
                        ha.common.facebook_login($(this));
                    }
                    return;
                }
            id = pdata.id;
            var toast_sandwich=0;
            var txt = $(this).text();
            var element = this;
            var sType = parent.find(".typeSandwich").val();
           if(txt == "SAVE")
           {
               $.ajax({
                            type: "POST",
                            data: {
                                    "id": id,
                                    "uid": SESSION_ID,
                                    "toast_sandwich":toast_sandwich
                                },
                            url: SITE_URL + 'sandwich/add_menu_multi/',
                            beforeSend: function() {
                                $("#ajax-loader").show();
                            },
                            complete: function() {
                                    $("#ajax-loader").hide();
                                },
                                success: function(e) {
                                    if(window.location.href.indexOf('friends_menu')>-1){
                                    }else{
                                        $(element).text("SAVED");
                                        $(element).addClass('saved_btn');

                                    }
                                }
                        });
           }
           if(txt == "SAVED")
           {

                    $.ajax({
                        type: "POST",
                        data: {
                            "id": id,
                            "uid": SESSION_ID,
                            "sType":sType
                        },
                        url: SITE_URL + 'sandwich/remove_saved/',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete: function() {
                            $("#ajax-loader").hide();
                        },
                        success: function(e) 
                        {
                            if(e == "SS")
                            {
                                $('input[name="chkmenuactice' + pdata.id + '"]').remove();
                                $('.menu-listing li[data-id="' + id + '"]').remove();
                            }
                            $(element).text("SAVE");
                            $(element).removeClass('saved_btn');
                        }
                    });

           }
            
        });

        $('#Share-BarneyBrown').unbind('click').on('click', function() 
        {

            session = ha.common.check_session_state();
            if (session.session_state == false) 
            {
                $("#sandwiches-popup").hide();
                if(session.facebook_only_user == false)
                {
                    ha.common.login_popup($(this));
                } 
                else 
                {
                    ha.common.facebook_login($(this));
                }
                return;
            }
            

            var feed = {
                method: 'feed',
                app_id: FB_APP_ID,
                name: "Barney Brown",
                link: SITE_URL
            };
            
            FB.ui(feed,  function ( response ){ }  );
        });


        $('.sandwichQtyLeft,.sandwichQtyRight').unbind('click');
        $(document).on('click','.sandwichQtyRight',function(x) 
        {
             input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
        });

        $(document).on('click','.sandwichQtyRight',function(x) 
        {
             input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
        });
        

        $(".quickAddToCart").unbind('click').on('click', function(e) 
        {
            e.preventDefault();
            var parent;
            parent = $(this).parent().parent();            
            pdata = parent.data();
            /*console.log(pdata);return;*/
            qty = $('input[name="sandwichQty'+ pdata.id + '"]').val();
            //self.update_data_to_db(pdata.id);
            $.ajax({
                type: "POST",
                url: SITE_URL + 'sandwich/getCurrentprice',
                data: {'pdata': pdata},
                success: function(data) {
                    console.log(data);
                }
            })

            
            self.add_sandwich_to_cart(pdata.id, null, null, qty, $(this));
   
        });

        $(".salad-listing, .featured_item .salad-img").unbind('click').on('click', function(e) 
        {
            var parent;

            if($(this).hasClass('view_sandwich')){
                parent = $(this).parent().next().find('ul');
            }else{
                parent = $(this).parent().parent();
            }
            
            //parent = $(this).parent().parent();            
            pdata = parent.data();
            //console.log("pdata ",pdata);return;
            $("input[name='itemQuatity']").val("01");
            if (!pdata.product_image) 
            {
                image = $("#salad_popup").find('img:nth-child(1)').hide();
            }
            else
            {
                image = $("#salad_popup").find('img:nth-child(1)');
                $(image).attr({
                    'src':pdata.image_path+pdata.product_image,
                    'width': '350',
                    'height': '194'
                });
            }
            session = ha.common.check_session_state();
                if (session.session_state == false) 
                {
                    if(session.facebook_only_user == false)
                    {
                        ha.common.login_popup($(this));
                    } 
                    else 
                    {
                        ha.common.facebook_login($(this));
                    }
                    return;
                }


            $("#salad_popup").find('#hidden_sandwich_id').val(pdata.id);
            $("#salad_popup").find('#hidden_sandwich_price').val(pdata.product_price);
            $("#salad_popup").find('h4').text(pdata.product_name);
            $($("#salad_popup p")[0]).text(pdata.description);
            if(pdata.add_modifier != 1 || pdata.add_modifier == 0){
                $("#salad_popup").find('.add_modifier').hide();
            }else{
                $("#salad_popup").find('.add_modifier').show();
                $(".salad-protien--sec").find('div').remove();
                var chklen = pdata.modifier_options.length;
                var html = '';
                $.each( pdata.modifier_options, function( key, value ) {
                    //console.log("value", value);
                  /*$.each( value, function( ke, val ) {*/
                    //console.log(ke +':'+ val);
                    html += '<div class="checkbox-holder">';
                    html += '<input id="check_p_'+key+'" type="checkbox" class="menucheck savetousertoastnew" name="checkoptions" value="'+value.option+','+value.price+'">';
                    html += '<label for="check_p_'+key+'">'+value.option + ' ($' + value.price + ')</label>';
                    html += '</div>';   
                  /*});*/
                });

                $(".salad-protien--sec").html(html);
            }
            if(pdata.spcl_instr != 1 || pdata.spcl_instr == 0){
                $("#salad_popup").find('.spcl_instruction').hide();
            }else{
                $("#salad_popup").find('.spcl_instruction').show();
            }
            
            $("#salad_popup").find('.modifier_desc').html(pdata.modifier_desc);
            $("#salad_popup").show();
            $('.popup_spinner .left_spinner').unbind('click').on('click', function() 
            {
                input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            $('.popup_spinner .right_spinner').unbind('click').on('click', function() 
            {
                input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });
            
            $(".add-salad-to-cart").unbind('click').on('click', function()
            {
                var atLeastOneIsChecked = $('input:checkbox').is(':checked');
                var chkboxlen = $(":checkbox:checked").length;
                //console.log($('input[name="checkoptions"]:checked').serialize());
                var clickedItems = [];
                i = 0;
                $('input[name="checkoptions"]:checked').each(function() {
                   clickedItems[i++] = this.value;
                });
        
                if(pdata.add_modifier == 1 && pdata.modifier_isoptional == 'no' && atLeastOneIsChecked==false){
                    alert('Please select one '+pdata.modifier_desc);
                }else if(pdata.add_modifier == 1 && pdata.modifier_is_single == 'yes' && chkboxlen > 1){
                    alert('You can select only one item');
                }else{
                    qty = $('input[name="itemQuatity"]').val();
                    $(".hidden-values").find('input').remove();
                    var hidden_fields = '<input type="hidden" name="itemid" value="' + pdata.id + '" />';
                    hidden_fields += '<input type="hidden" name="data_type" value="' + pdata.product + '" />';
                    hidden_fields += '<input type="hidden" name="user_id" value="' + pdata.uid + '" />';
                    hidden_fields += '<input type="hidden" name="qty" value="' + qty + '" />';
                    hidden_fields += '<input type="hidden" name="clickedItems" value="' + clickedItems + '" />';
                    hidden_fields += '<input type="hidden" name="toast" value="0" />';
                    $('.hidden-values').append(hidden_fields);

                    var data = $(this).parent().parent().serialize();

                    $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'sandwich/add_to_cart',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete: function() {
                            $("#ajax-loader").hide();
                            $("#sandwiches-popup").hide();
                        },
                        success: function(e) {
                            self.added_to_cart_transition(e);
                            $('.shopping-cart span').text(e);
                            $("#product-descriptor").hide();
                            $("#sandwiches-popup").hide();

                        }
                    });
                    //self.add_sandwich_to_cart(pdata.id, pdata.uid, pdata.product, qty, $(this));
                }
                
            })
            $("#salad_popup").find('.close-button').click(function(){
                $(".salad-protien--sec").find('div').remove();
                $("#salad_popup").hide();
            });
        });
    
        $(".catering-listing, .cat-image").unbind('click').on('click', function(e) 
        {
            var parent;
            parent = $(this).parent();
            if($(this).hasClass('view_sandwich'))
            {
                parent = $(this).parent().next();
            }
            if($(this).hasClass('add-cart-catering'))
            {
               parent = $(this).parent().prev().prev().prev();
            }
            
            $(".salad_item__description").addClass("catering_tem_description");
               
            pdata = parent.data();
            var des = pdata.description;
            
           // return;
            
            $("input[name='itemQuatity']").val("01");
            if (!pdata.product_image) 
            {
                image = $("#salad_popup").find('img:nth-child(1)').hide();
            }
            else
            {
                image = $("#salad_popup").find('img:nth-child(1)');
                $(image).attr({
                    'src':pdata.image_path+pdata.product_image,
                    'width': '350',
                    'height': '194'
                });
            }
            session = ha.common.check_session_state();
                if (session.session_state == false) 
                {
                    if(session.facebook_only_user == false)
                    {
                        ha.common.login_popup($(this));
                    } 
                    else 
                    {
                        ha.common.facebook_login($(this));
                    }
                    return;
                }


            $("#salad_popup").find('#hidden_sandwich_id').val(pdata.id);
            $("#salad_popup").find('#hidden_sandwich_price').val(pdata.product_price);
            $("#salad_popup").find('h4').text(pdata.product_name);
            $($("#salad_popup p")[0]).html($.parseHTML(des));

            if(pdata.add_modifier != 1 || pdata.add_modifier == 0)
            {
                $(".salad-protien--sec").find('div').remove();
                $("#salad_popup").find('.add_modifier').hide();
            }
            else
            {
                $("#salad_popup").find('.add_modifier').show();
                $(".salad-protien--sec").find('div').remove();
                var chklen = pdata.modifier_options.length;
                var html = '';
                $.each( pdata.modifier_options, function( key, value ) {
                    //console.log("value", value);
                  /*$.each( value, function( ke, val ) {*/
                    //console.log(ke +':'+ val);
                    html += '<div class="checkbox-holder">';
                    html += '<input id="check_p_'+key+'" type="checkbox" class="menucheck savetousertoastnew" name="checkoptions" value="'+value.option+','+value.price+'">';
                    html += '<label for="check_p_'+key+'">'+value.option + ' ($' + value.price + ')</label>';
                    html += '</div>';   
                  /*});*/
                });

                $(".salad-protien--sec").append(html);
            }
            if(pdata.spcl_instr != 1 || pdata.spcl_instr == 0){
                $("#salad_popup").find('.spcl_instruction').hide();
            }else{
                $("#salad_popup").find('.spcl_instruction').show();
            }
            
            $("#salad_popup").find('.modifier_desc').html(pdata.modifier_desc);
            $("#salad_popup").show();
            $('.popup_spinner .left_spinner').unbind('click').on('click', function() 
            {
                input = $(this).next();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value > 1) {
                    value = value - 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });

            $('.popup_spinner .right_spinner').unbind('click').on('click', function() 
            {
                input = $(this).prev();
                value = parseInt($(input).val());
                if (isNaN(value) == true) value = 1;
                if (value < 999) {
                    value = value + 1;
                }
                if (value < 9) txt = "0" + value;
                else txt = value;
                $(input).val(txt);
            });
            
            $(".add-salad-to-cart").unbind('click').on('click', function()
            {
                var atLeastOneIsChecked = $('input:checkbox').is(':checked');
                var chkboxlen = $(":checkbox:checked").length;
                //console.log($('input[name="checkoptions"]:checked').serialize());
                var clickedItems = [];
                i = 0;
                $('input[name="checkoptions"]:checked').each(function() {
                   clickedItems[i++] = this.value;
                });
        
                if(pdata.add_modifier == 1 && pdata.modifier_isoptional == 'no' && atLeastOneIsChecked==false){
                    alert('Please select one '+pdata.modifier_desc);
                }else if(pdata.add_modifier == 1 && pdata.modifier_is_single == 'yes' && chkboxlen > 1){
                    alert('You can select only one item');
                }else{
                    qty = $('input[name="itemQuatity"]').val();
                    $(".hidden-values").find('input').remove();
                    var hidden_fields = '<input type="hidden" name="itemid" value="' + pdata.id + '" />';
                    hidden_fields += '<input type="hidden" name="data_type" value="' + pdata.product + '" />';
                    hidden_fields += '<input type="hidden" name="user_id" value="' + pdata.uid + '" />';
                    hidden_fields += '<input type="hidden" name="qty" value="' + qty + '" />';
                    hidden_fields += '<input type="hidden" name="clickedItems" value="' + clickedItems + '" />';
                    hidden_fields += '<input type="hidden" name="toast" value="0" />';
                    $('.hidden-values').append(hidden_fields);

                    var data = $(this).parent().parent().serialize();

                    $.ajax({
                        type: "POST",
                        data: data,
                        url: SITE_URL + 'sandwich/add_to_cart',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete: function() {
                            $("#ajax-loader").hide();
                            $("#sandwiches-popup").hide();
                        },
                        success: function(e) {
                            self.added_to_cart_transition(e);
                            $('.shopping-cart span').text(e);
                            $("#product-descriptor").hide();
                            $("#sandwiches-popup").hide();

                        }
                    });
                    //self.add_sandwich_to_cart(pdata.id, pdata.uid, pdata.product, qty, $(this));
                }
                
            })
        });

    },
    
    set_flag_state: function(data) {
        $.ajax({
            type: "POST",
            data: {
                'id': data.id,
                'status': data.state
            },
            url: SITE_URL + 'sandwich/set_flag/',

            success: function(e) {

            }
        });
    },
    
    //~sandwich sider on home page
    set_sandwich_maker_slider: function() {
        var i           = 0; 
        var j           = 0;
        var self        = this;
        var sliderSpans = $('.homepageAutoBanner span');
        var spanCount   = sliderSpans.length - 1;
        
        var interval    = setInterval(function() {
            
            if(i <= spanCount){
                
                
                siders   = sliderSpans[i];
                imgs     = $(siders).find('img');
                imgs     = $(imgs);
                imgCount = imgs.length - 1;
                
                  if(j <= imgCount){
                      
                      $('.homeAnimationLoaderImg').hide(10,function(){
                          $(imgs[1]).delay(500).fadeIn(500);
                      }); 
                      
                      $(imgs[j]).fadeIn(500);
                     
                      j++;
                  } else {
                      j=0;
                      $(siders).find('img').delay( 1800 ).fadeOut(500);
                      i++;
                  }
                  
                 
            } else {
                       i = 0;
                       $(sliderSpans.find('img')[0]).fadeIn(500);
            }
            
            
            
        },2000);
        
        
        

    },

    add_toast_sandwich_to_cart: function(id, toast, uid, datatype, Qty, ClickedItem) {
        var self = this;
 
        
        session = ha.common.check_session_state();
        if (session.session_state == false) {
             if(session.facebook_only_user == false){
               ha.common.login_popup(ClickedItem);
             } else {
               ha.common.facebook_login(ClickedItem);
             }
            return;
        }

        var user_id = $("#hidden_uid").val();
        if (uid) {
            user_id = uid;
        }
        var data_type = $("#hidden_sandwich_data").val();
        if (datatype) {
            data_type = datatype
        }

        var qty;
        if (Qty) qty = Qty;
        else qty = 1;

        $.ajax({
            type: "POST",
            data: {
                'itemid': id,
                "data_type": data_type,
                "user_id": user_id,
                "qty": qty,
                "toast": toast
            },
            url: SITE_URL + 'sandwich/add_to_cart',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
                $("#sandwiches-popup").hide();
            },
            success: function(e) {
                self.added_to_cart_transition(e);
                $('.shopping-cart span').text(e);
                $("#sandwiches-popup").hide();

            }
        });

    },

    add_sandwich_to_cart: function(id, uid, datatype, Qty, ClickedItem) {

        var self = this;
       
        
        session = ha.common.check_session_state();
        if (session.session_state == false) {
             if(session.facebook_only_user == false){
               ha.common.login_popup(ClickedItem);
             } else {
               ha.common.facebook_login(ClickedItem);
             }
            return;
        }

        var user_id = $("#hidden_uid").val();
        if (uid) {
            user_id = uid;
        }
        var data_type = $("#hidden_sandwich_data").val();
        if (datatype) {
            data_type = datatype
        }

        var toast;
        parent = $(ClickedItem).parent().parent();
        
         //console.log(parent);return;       
        if (ClickedItem) {

            if ($(parent).get(0).tagName == 'DIV') {
                if ($(".menucheck").is(':checked')) toast = 1;
                else toast = 0;
            } else {
                pmaindata = $(parent).data();
                toast = pmaindata.toast;
            }
        } else {
            if ($(".menucheck").is(':checked')) toast = 1;
            else toast = 0;
        }




        var qty;
        if (Qty) qty = Qty;
        else qty = 1;

        if (datatype == 'product') {

            var prod_descriptor = ha.common.product_extras.Data[id];


            if (typeof(prod_descriptor) == "undefined") {


            } else {

                $("#product-descriptor ul.from-holder").html("");

                for (i = 0; i < prod_descriptor.length; i++) {
                    var prod_desc = prod_descriptor[i];
                    var options = "";
                    for (j = 0; j < prod_desc.extra.length; j++) {
                        var option = prod_desc.extra[j];
                        options += "<option value='" + option.id + "'>" + option.name + "</option>";
                    }

                    $("#product-descriptor .title-holder h1").html(prod_desc.desc);
                    var content = '<li><span class="text-box-holder"><select name="extra_id" class="extra_id listextra">' + options + '</select></span> </li><input type="hidden" name="descriptor_id" value="' + prod_desc.id + '"/>';
                    $("#product-descriptor ul.from-holder").append(content);
                }

                var hidden_fields = '<input type="hidden" name="itemid" value="' + id + '" />';
                hidden_fields += '<input type="hidden" name="data_type" value="' + data_type + '" />';
                hidden_fields += '<input type="hidden" name="user_id" value="' + user_id + '" />';
                hidden_fields += '<input type="hidden" name="qty" value="' + qty + '" />';
                hidden_fields += '<input type="hidden" name="toast" value="' + toast + '" />';


                $("#product-descriptor form .hidden_fields").html(hidden_fields);
                $("#product-descriptor input.add_desc").val('ADD TO CART');
                $("#product-descriptor").show();
                return false;
            }

        }


        $.ajax({
            type: "POST",
            data: {
                'itemid': id,
                "data_type": data_type,
                "user_id": user_id,
                "qty": qty,
                "toast": toast
            },
            async: false,
            url: SITE_URL + 'sandwich/add_to_cart',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
                $("#sandwiches-popup").hide();
            },
            success: function(e) {              
                self.added_to_cart_transition(e);
                $(".popup-wrapper").hide();
                $("#sandwiches-popup").hide();
            }
        });


    },

    add_product_descriptor_to_cart: function() {

        var self = this;

        $("#product-descriptor input.add_desc").click(function() {

            var data = $(this).parent().serialize();

            $.ajax({
                type: "POST",
                data: data,
                url: SITE_URL + 'sandwich/add_to_cart',
                beforeSend: function() {
                    $("#ajax-loader").show();
                },
                complete: function() {
                    $("#ajax-loader").hide();
                    $("#sandwiches-popup").hide();
                },
                success: function(e) {
                    self.added_to_cart_transition(e);
                    $('.shopping-cart span').text(e);
                    $("#product-descriptor").hide();
                    $("#sandwiches-popup").hide();

                }
            });
        });

    },

    retrive_data_onload: function() {
        var self = this;
        var ID = 0;
        urlArray = window.location.href.split('/');
        if(urlArray.length >= 6){
            if(urlArray[5]) ID = urlArray[5]; else ID = DATA_ID;
        }  else {
            ID = DATA_ID;
        }

        $.ajax({
            type: "POST",
            data: {
                'id': ID,
                'uid': SESSION_ID,
                'source': 'edit'
            },
            url: SITE_URL + 'createsandwich/get_sandwich_data/',
            dataType: 'json',
            beforeSend: function() {
                
            },
            complete: function() {
                $("#ajax-loader").hide();
                if (self.vars.ingredient_details["BREAD"].item_name.length == 0) self.auto_selection();
                if (self.vars.ingredient_details["BREAD"].item_name.length > 0) {
                    $('.landing-page').hide();
                   if(ha.common.getCookie("sandwich_end_screen") != "true"){
                    $('.button-holder').show();
                   }
                }
            },
            success: function(e) {
 
                self.auto_selection();
                if (e.Response['status-code'] == 200) {
                    if (e.Data[0].id) {
                        self.temp_id = e.Data[0].id;
                        self.temp_name = e.Data[0].sandwich_name;

                    } else {
                        self.temp_id = '';
                    }
                    $sanwch = e.Data[0].sandwich_data;
                    price = parseFloat(e.Data[0].sandwich_price);
                    if ($sanwch) {
                        json_data = JSON.parse($sanwch);

                        if (json_data) {
                            
                            console.log("json_data", json_data)
                            $keyz = Object.keys(json_data);
                            
                            $keyz.forEach(function(e){
                                $kys = Object.keys(json_data[e]);
                                $kys.forEach(function(k){ 
                                    self.vars.ingredient_details[e][k] = json_data[e][k];
                                 });
                            });
                            
                            self.load_all_data(json_data,true);
                            self.reload_state();
                        }
                    }

                    if (ha.common.getCookie('sandwich_end_screen') == 'true') {
                        self.show_end();
                    }

                }


            }
        });


    },

    retrive_data_on_quickEdit: function(ID) {

        ha.sandwichcreator.vars = {
            temp_id: 0,
            cheese_min: 0,
            AjaxURL: {
                BREAD: "createsandwich/load_bread_data/",
                PROTEIN: "createsandwich/load_protien_data/",
                CHEESE: "createsandwich/load_cheese_data/",
                TOPPINGS: "createsandwich/load_toppings_data/",
                CONDIMENTS: "createsandwich/load_condiments_data/"
            },
            manual_mode: false,
            current_sliced_image: null,
            ACTIVE_MENU: $('.create-sandwich-menu ul li a[class="active"]').text(),
            selection_data: null,
            validators: ["BREAD", "PROTEIN", "CHEESE"],
            bread_type: null,
            image_path: ADMIN_URL + 'upload/',
            current_price: 0,
            individual_price: {},

            ingredient_details: {
                BREAD: {
                    item_name: [],
                    item_price: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    type: 0, // either 3 foot or 6 foot or normal [0-normal,1-3-foot,2-6-foot]
                    shape: 0, // either long,round,trapezoid [0-long,1-3-round,2-6-trapezoid]
                    actual_price: {}
                },
                PROTEIN: {
                    // manual_mode: false,
                    item_name: [],
                    item_price: [],
                    item_priority: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    actual_price: {},
                    option_default_Choice_first: 1,
                    option_default_Choice_second: 0,
                },
                CHEESE: {
                    // manual_mode: false,
                    item_name: [],
                    item_price: [],
                    item_priority: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    actual_price: {},
                    option_default_Choice_first: 1,
                    option_default_Choice_second: 0,
                },
                TOPPINGS: {
                    item_name: [],
                    item_price: [],
                    item_priority: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    actual_price: {},
                    option_default_Choice_first: 1,
                    option_default_Choice_second: 0,
                },
                CONDIMENTS: {
                    item_name: [],
                    item_price: [],
                    item_priority: [],
                    item_qty: {},
                    item_id: [],
                    item_image: [],
                    actual_price: {},
                    option_default_Choice_first: 1,
                    option_default_Choice_second: 0,
                }
            },
            
            appliedDiscount: 0
        }

        console.log("ID", ID);
        DATA_ID = ID;
        var self = this;

        $.ajax({
            type: "POST",
            data: {
                'id': ID,
                'uid': SESSION_ID,
                'source': 'quick_edit'
            },
            url: SITE_URL + 'createsandwich/get_sandwich_data/',
            dataType: 'json',
            beforeSend: function() {
                $('#sandwiches-popup_ajax_loader').show();
            },
            complete: function() {
                if (self.vars.ingredient_details["BREAD"].item_name.length == 0) self.auto_selection();
            },
            success: function(e) {

              //  console.log("e", e);
 
                self.auto_selection();
                if (e.Response['status-code'] == 200) {
                    if (e.Data[0].id) {
                        self.temp_id = e.Data[0].id;
                        self.temp_name = e.Data[0].sandwich_name;

                    } else {
                        self.temp_id = '';
                    }
                    $sanwch = e.Data[0].sandwich_data;
                    price = parseFloat(e.Data[0].sandwich_price);
                    if ($sanwch) {
                        json_data = JSON.parse($sanwch);

                        if (json_data) {
                            
                            
                            $keyz = Object.keys(json_data);
                            
                            $keyz.forEach(function(e){
                                $kys = Object.keys(json_data[e]);
                                $kys.forEach(function(k){ 
                                    self.vars.ingredient_details[e][k] = json_data[e][k];
                                 });
                            });

                            console.log("self.vars", self.vars);
                            
                            self.load_all_data_quickEdit(json_data,true);
                            self.reload_state_quick_edit();
                        }
                    }

                    /*if (ha.common.getCookie('sandwich_end_screen') == 'true') {
                        self.show_end();
                    }*/

                }


            }
        });


    },
    auto_selection: function() {
        var self = this;
        if (self.get_url_params('view') != null) {
            switch (self.get_url_params('view')) {

                case '3foot':
                    $3foot = $('#optionList input[data-bread_type="1"]');
                    if ($3foot.length == 0) {
                        alert("Please add a 3 foot bread!");
                        window.location.href = SITE_URL + 'catering/';
                    } else {
                        $3foot.trigger('click');
                        self.vars.ingredient_details["BREAD"].type = 1;
                        $('.landing-page').hide();
                    }
                    break;
                case '6foot':
                    $6foot = $('#optionList input[data-bread_type="2"]');

                    if ($6foot.length == 0) {
                        alert("Please add a 6 foot bread!");
                        window.location.href = SITE_URL + 'catering/';
                    } else {
                        $6foot.trigger('click');
                        self.vars.ingredient_details["BREAD"].type = 2;
                        $('.landing-page').hide();
                    }
                    break;
            }
        }

    },

    load_all_data: function($data,pageload) {
        console.log("$data",$data);
        var $html = '';
        var self = this;
        var itms = {};
        var htmlELm = {}
        Object.keys($data).forEach(function(e) {
            var priorityOb = {};
            if ($data[e].item_name != "NULL") {
                $images = $data[e].item_image;
                $names = $data[e].item_name;
                $priority = $data[e].item_priority;

                $len = $names.length;
                for (i = 0; i < $len; i++) {
                    if (e != 'BREAD') {
                        $rel = $names[i];

                    } else {
                        $rel = '';
                    }

                    $img = self.vars.image_path + $images[i];
                    if (e != 'BREAD') {
                        if($priority != undefined)
                            priorityOb[$priority[i]] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                        else
                            priorityOb[i] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    } else {

                        priorityOb[0] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    }



                }

                htmlELm[e] = priorityOb;
                $html += self.sandwich_image_wrapping(e, htmlELm);
            }
        });
        
        
        pcount  = self.vars.ingredient_details.PROTEIN.item_id.length;
        ccount  = self.vars.ingredient_details.CHEESE.item_id.length;
        cocount = self.vars.ingredient_details.CONDIMENTS.item_id.length;
        tcount  = self.vars.ingredient_details.TOPPINGS.item_id.length;

 
        if(pageload == true && self.vars.ACTIVE_MENU == "BREAD"){
            $(".bread_images").html("" + $html + "");
                $(".bread_images .image-holder img").load(function(){
                    $(".bread_images .image-holder img").fadeIn(300); 
                });
                
        }
        else if (pcount > 0 || ccount > 0 || cocount  > 0 || tcount > 0) {
        $(".bread_images").html("" + $html + "");
        
        $(".bread_images .image-holder img").each(function(){
            
            $(this).load(function(){ 
                $(this).fadeIn(400); 
            });
            
        });
        
       }
    },

    load_all_data_quickEdit: function($data,pageload) {
        var $html = '';
        var self = this;
        var itms = {};
        var htmlELm = {}
        Object.keys($data).forEach(function(e) {
            var priorityOb = {};
            if ($data[e].item_name != "NULL") {
                $images = $data[e].item_image;
                $names = $data[e].item_name;
                $priority = $data[e].item_priority;

                $len = $names.length;
                for (i = 0; i < $len; i++) {
                    if (e != 'BREAD') {
                        $rel = $names[i];

                    } else {
                        $rel = '';
                    }

                    $img = self.vars.image_path + $images[i];
                    console.log(e+" priority", $priority);
                    if (e != 'BREAD') {
                        if($priority != undefined)
                            priorityOb[$priority[i]] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                        else
                            priorityOb[i] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    } else {

                        priorityOb[0] = '<div rel="' + $rel + '" class="image-holder"><img alt="" src="' + $img + '" style="display: none"></div>';
                    }



                }

                htmlELm[e] = priorityOb;
                $html += self.sandwich_image_wrapping(e, htmlELm);
            }
        });
        
        
        pcount  = self.vars.ingredient_details.PROTEIN.item_id.length;
        ccount  = self.vars.ingredient_details.CHEESE.item_id.length;
        cocount = self.vars.ingredient_details.CONDIMENTS.item_id.length;
        tcount  = self.vars.ingredient_details.TOPPINGS.item_id.length;

 
        if(pageload == true && self.vars.ACTIVE_MENU == "BREAD"){
            $(".bread_images").html("" + $html + "");
                $(".bread_images .image-holder img").load(function(){
                    $(".bread_images .image-holder img").fadeIn(300); 
                });
                
        }
        else if (pcount > 0 || ccount > 0 || cocount  > 0 || tcount > 0) {
        $(".bread_images").html("" + $html + "");
        
        $(".bread_images .image-holder img").each(function(){
            
            $(this).load(function(){ 
                $(this).fadeIn(400); 
            });
            
        });
        
       }
    },


    sandwich_image_wrapping: function(type, html) {
 
        var finalData = {
            'BREAD': '',
            'PROTEIN': '',
            'CHEESE': '',
            'TOPPINGS': '',
            'CONDIMENTS': ''
        };
        $data = html[type];
        $keys = Object.keys($data);
        $keys.forEach(function(keys) {
            finalData[type] += $data[keys];
        });


        switch (type) {
            case 'BREAD':
                $data = '<span class="bread_image">' + finalData[type] + '</span>';
                break;
            case 'PROTEIN':
                $data = '<span class="protein_image">' + finalData[type] + '</span>';
                break;
            case 'CHEESE':
                $data = '<span class="cheese_image">' + finalData[type] + '</span>';
                break;
            case 'TOPPINGS':
                $data = '<span class="topping_image">' + finalData[type] + '</span>';
                break;
            case 'CONDIMENTS':
                $data = '<span class="condiments_image">' + finalData[type] + '</span>';
                break;
        }



        return $data;
    },




    
    calculate_price: function() {
        var final_price = 0;
        var self = this;
        self.get_min_cheese_price();
        $data = self.vars.ingredient_details;
        Object.keys($data).forEach(function(e) {
            $price = $data[e].item_price;
            temp = 0;
            $name = $data[e].item_name;

            $name.forEach(function(t, i) {


                if ($price[i] !== undefined && isNaN($price[i]) != true) {
                    temp += parseFloat($price[i]);
                    self.vars.individual_price[e] = temp.toFixed(2);
                }


            });

            if ($name.length == 0) self.vars.individual_price[e] = 0;



        });

        if (self.vars.ingredient_details["BREAD"].type != 0) {
            self.sum_individual();
            self.vars.current_price = self.vars.ingredient_details["BREAD"].item_price[0];
        } else {
            self.vars.current_price = self.sum_individual();
            self.vars.current_price = parseFloat(self.vars.current_price)+parseFloat(BASE_FARE);
            self.vars.current_price=self.vars.current_price.toFixed(2);

        }

        console.log("current price", "$" + self.vars.current_price);
        $(".create-sandwich-right-wrapper .price").text("$" + self.vars.current_price);
        if($('#sandwiches_quick_edit:visible').length == 0) self.save_to_temp_session();
    },

    adjust_cheese_price: function() {
        var self = this;
        var $sum = 0;
        if (self.vars.ingredient_details.CHEESE.item_qty) {
            $keys = Object.keys(self.vars.ingredient_details.CHEESE.item_qty)
            $len = $keys.length;
            if ($len > 0) {
                $keys.forEach(function(ei) {
                    $sum += self.vars.ingredient_details.CHEESE.item_qty[ei][2];
                });
            }
        }

        if ($sum > 1) {
            cheese_price = parseFloat(self.vars.individual_price.CHEESE);
            cheese_price = cheese_price - self.vars.cheese_min;
        } else {
            cheese_price = 0;
        }
        return cheese_price;
    },
    get_min_cheese_price: function() {
        var self = this;
        var array = [];
        self.vars.ingredient_details.CHEESE.item_name.forEach(function(e) {
            if (self.vars.ingredient_details.CHEESE.actual_price[e] > 0) array.push(self.vars.ingredient_details.CHEESE.actual_price[e]);
        });
        min = Math.min.apply(Math, array);
        if (min) {
            self.vars.cheese_min = min;
        }

    },

    sum_individual: function() {
        $data = this.vars.individual_price;
        var finalprice = 0;
        Object.keys($data).forEach(function(e) {
            finalprice += parseFloat($data[e]);
        });
        return finalprice.toFixed(2);
    },


    save_to_temp_session: function() {

        var self = this;
        self.enable_scrolbar();
        user_data = JSON.stringify(self.vars.ingredient_details);

        $obj = {
            'Response': {
                'status-code': '200',
                'message': 'OK'
            },
            'Data': [{

                'id': '',
                'uid': '',
                'sandwich_name': '',
                'date_of_creation': '',
                'description': '',
                'sandwich_data': user_data,
            }]
        };

        $obj = JSON.stringify($obj)

        $.ajax({
            type: "POST",
            data: {
                'data': $obj
            },
            url: SITE_URL + 'createsandwich/temp_session_input/',
            dataType: 'json',
            beforeSend: function() {
                $(".optionLoader").show();
            },
            complete: function() {
                $(".optionLoader").hide();
            },
            success: function(e) {

            }

        });
    },


    sync_data_to_db : function(is_pub, sandwichname, menu_active, tempname, returnid, thisitm, from, is_saved) {
        var self = this;
        var toast = 0;
        if ($(".menucheck").is(':checked')) {
            toast = 1;
        }
        var self = this;
        var state;

        is_saved = is_saved != '' ? is_saved : "SAVE"; 
       

        if (!sandwichname) {
            alert("Name your sandwich and continue!");
            return false;

        } else {

            $.ajax({
                type: "POST",
                data: {
                    'word': sandwichname,
                },
                async: false,
                url: SITE_URL + 'createsandwich/filter_words/',
                dataType: 'json',
                success: function(e) {
                    jso = JSON.parse(e);
                    state = jso.Response["status-code"];
                }
            });
            if (state == 500) {
                if(from == 'quick_edit'){
                   thisitm.removeClass('disable');
                }else{
                   $(".am-done").removeClass('disable');
                   $(".save-to-my-menu").removeClass('disable');
               }
                alert("Sorry, this name is not allowed due to inappropriate content.  Please try again.");
                return false;
            }

        }

        // for fixing multiple items are getting added to cart while creating sandwich...
        if (thisitm) {
            if(returnid == true){ 
                if(from == 'quick_edit') thisitm.text('Saving...');
                else thisitm.text('Adding to cart...');
            } else {
                thisitm.text('Adding to Saved Sandwiches...');
            }
            
            thisitm.addClass('disable');
        }

        var items_names = [];
        
        $.each(self.vars.ingredient_details, function(k, ingredient ) 
        {
            if(ingredient.item_name == '' && k == 'PROTEIN')
            {
                items_names = items_names.concat("No Protein");
            }
            else if(ingredient.item_name == '' && k == 'CHEESE')
            {
                items_names = items_names.concat("No Cheese");
            }
            else if(ingredient.item_name == '' && k == 'TOPPINGS')
            {
                items_names = items_names.concat("No Toppings");
            }
            else if(ingredient.item_name == '' && k == 'CONDIMENTS')
            {
                items_names = items_names.concat("No Condiments");
            }
            else
            {
                items_names = items_names.concat(ingredient.item_name);
            }
            
        })
        items_names = items_names.toString();
        user_data = JSON.stringify(self.vars.ingredient_details);
        if (tempname != sandwichname) {
            tempname = 1;
        } else {
            tempname = 0;
        }

        var $sandid = 0;
        console.log(self.temp_id);
        if ($('.share_to_fb').attr('rel')) {

                if (returnid == "share") {
                    $sandid = $('.share_to_fb').attr(rel);
                } else if (returnid == true) {

                     self.add_sandwich_to_cart($('.share_to_fb').attr('rel'), SESSION_ID, "user_sandwich");
                     window.open(SITE_URL + 'checkout/choosedeliveryorpickup', '_self');

                } else {
     
                    
                     window.open(SITE_URL + 'menu', '_self');
                }

        } else {
             
            $.ajax({
                type: "POST",
                data: {
                    'tempname': tempname,
                    'menu_active': menu_active,
                    'id': self.temp_id,
                    'uid': SESSION_ID,
                    'data': user_data,
                    'items_names': items_names,
                    'price': self.vars.current_price,
                    'is_pub': is_pub,
                    'name': sandwichname,
                    'toast': toast,
                    'is_saved': is_saved
                },
                async: false,
                url: SITE_URL + 'createsandwich/user_input/',
                dataType: 'json',
                beforeSend: function() {
                    $("#ajax-loader").show();
                },
                complete: function() {
                    $("#ajax-loader").hide();
                    self.clear_temp_session();
                },
                success: function(e) {
                    if (e.Response['status-code'] == 200 || e.Response['status-code'] == 201) {
                        if(from != 'quick_edit') self.destroy_endscreen_var();

                        if (e.Data) {
                            $id = e.Data;
                        }else {
                            $id = DATA_ID;
                        }

                        /*if(from != 'quick_edit')*/ imgStore = self.create_sandwitch_image(463, 344, $id, from);
                       /* else if(from == 'quick_edit')  imgStore = true;*/
                       

                        if (imgStore == true) {


                            if (returnid == 'share') {

                                $sandid = $id;
                            } else if (returnid == true) {
                                console.log("e.Data", e.Data);
                                console.log("$id", $id);

                                if(from == 'quick_edit') {
                                    location.reload(true);
                                }else{
                                    self.add_sandwich_to_cart($id, SESSION_ID, "user_sandwich");
                                    window.open(SITE_URL + 'checkout/choosedeliveryorpickup', '_self');
                                }
                                

                            } else {
                                 //window.open(SITE_URL + 'sandwich/savedSandwiches', '_self');
                                 //window.open(SITE_URL + 'menu', '_self');
                                 thisitm.text('SAVED');
                            }


                        }

                    }
                }

            });
        }

        return $sandid;

    },

    create_sandwitch_image: function(width, height, id, from) {
        var _imgs = [];
        var $final;
        var ret = false;
        if(from != 'quick_edit'){
            $('.image-holder img:visible').each(function() {

                src = $(this).attr('src');
                Arr = src.split('/');
                img = Arr[Arr.length - 1];
                _imgs.push(img);

            });
        }else if(from == 'quick_edit'){
            $('.image-holder img').each(function() {

                src = $(this).attr('src');
                Arr = src.split('/');
                img = Arr[Arr.length - 1];
                _imgs.push(img);

            });
        }

        $final = JSON.stringify({
            images: _imgs
        });




        $.ajax({
            type: "POST",
            data: {
                'json_string': $final,
                'width': width,
                'height': height,
                'file_name': 'sandwich_' + id + '_' + SESSION_ID + '.png',
                'path': SESSION_ID + '/'
            },
            url: SITE_URL + 'createsandwich/save_image/',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            async: false,
            success: function(e) {
                ret = true;
            }
        });


        return ret;
    },


    ingredient_enable: function() {
        var e = this;
        $selector = $(".create-sandwich-menu ul li a");
        $selector.unbind("click").on("click", function() {
            error = e.validate($(this).text());
            if (error == true) {
                return false
            }

            if ($(this).text() != 'BREAD') e.turn_bread_image();

            e.vars.ACTIVE_MENU = $(this).text();
            e.load_side_menu();
            $selector.removeClass("active");
            $(this).addClass("active")
        });
        this.enable_nav_buttons();
        this.user_selections()
    },



    slide_function: function() {
        var e = this;
        $("#optionList .left, #optionList .right").unbind("click").on("click", function(t) {
            console.log("here")
            t.preventDefault();
            $parent = $(this).parent();
            e.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode = true;
            $ptext = $parent.parent().find('input[type="checkbox"]').data();

            $pele = $parent.parent().find('input[type="checkbox"]');
            $p_priority = $ptext.priority;

            $total = $parent.find("input").length - 1;
            $inputs = $parent.find("input");
            $curr_index = $parent.find("input:visible").index();
            $curr_index_t = $curr_index + 1;
            if ($curr_index > 0) {
                $prv_index = $curr_index - 1
            } else {
                $prv_index = 0
            }
            if ($curr_index < $total) {
                $nxt_index = $curr_index + 1
            } else {
                $nxt_index = $total
            }

            if (t.target.className.indexOf("right") != -1) {
                $parent.find("input:visible").hide();
                $($parent.find("input")[$nxt_index]).show();
                $ind = $parent.find("input:visible").index();

                if ($ind == $total) {

                    $(this).removeClass("right-hover");
                    $(this).parent().find('.left').addClass("left-hover");
                }
                if ($ind > 0) {
                    $(this).parent().find('.left').addClass("left-hover");
                }


            } else {
                $parent.find("input:visible").hide();
                $($parent.find("input")[$prv_index]).show();
                $ind = $parent.find("input:visible").index();

                if ($ind == 0) {

                    $(this).removeClass("left-hover");
                    $(this).parent().find('.right').addClass("right-hover");
                }

                if ($ind < $total) {
                    $(this).parent().find('.right').addClass("right-hover");
                }

            }



            $arr = [];
            $value = $parent.find("input:visible").val();
            $data = $parent.find("input:visible").data();

            //createsandwichslideimage
            $num = e.ing_availaility_check();
            $image = e.select_image_by_shape($ptext, $num, $data);
            console.log("$image", $image);
            image_url = e.vars.image_path + $image;
            if($image == "")
            {
                image_url = ""; 
            }
            

            $arr[0] = $value;
            $arr[1] = $data.unit;
            $arr[2] = $data.price_mul;
            $arr[3] = $data.image;
            $arr[4] = $data.optionid;
            e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$ptext.itemname] = $arr
            $itn_id = $ptext.id;
            new_price = parseFloat($ptext.price) * parseFloat($arr[2]);
            $p_index = $.inArray($itn_id, e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_id);
            if ($p_index != -1) {
                e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_price[$p_index] = new_price;
                e.calculate_price();
            }

            e.set_images_in_position($p_priority, image_url, $pele);
            e.on_complete_actions(false, $ptext.itemname, $ptext.price, $image, $ptext.id, $p_priority);
            e.auto_select_option_fix();

        });

        e.slider_button_fix();

    },


    slide_function_on_quick_edit: function() {
        var e = this;
        $(document).find("#sandwiches_quick_edit_add_item .left, #sandwiches_quick_edit_add_item .right, #sandwiches_quick_edit .left, #sandwiches_quick_edit .right").unbind("click").on("click", function(t) {
         
            t.preventDefault();
            $parent = $(this).parent();
            var category = $('#add-item-category-select option:selected').text();
            e.vars.ingredient_details[category].manual_mode = true;
            $ptext = $('#add-item-select option:selected').data();
            $total = $parent.find("input").length - 1;
            $inputs = $parent.find("input");
            $curr_index = $parent.find("input:visible").index();
            console.log("curr_index", $curr_index);
            $curr_index_t = $curr_index + 1;
            $curr_index = $curr_index - 1;
            if ($curr_index > 0) {
                $prv_index = $curr_index - 1
            } else {
                $prv_index = 0
            }
            if ($curr_index < $total) {
                $nxt_index = $curr_index + 1
            } else {
                $nxt_index = $total
            }

            if (t.target.className.indexOf("right") != -1) {
                $parent.find("input:visible").hide();
                $parent.find("input:eq("+ $nxt_index +")").show();
                $ind = $parent.find("input:visible").index();
            } else {
                $parent.find("input:visible").hide();
                $parent.find("input:eq("+ $prv_index +")").show();
                $ind = $parent.find("input:visible").index();
            }

        });

        //e.slider_button_fix();
    },

    load_side_menu: function() {
        var e;
        var t = this;
        $.ajax({
            type: "GET",
            url: SITE_URL + t.vars.AjaxURL[t.vars.ACTIVE_MENU],
            success: function(e) {
                $datax = $(e).find('input').not('.sub-value input');


                $datax.each(function() {
                    itmDat = $(this).data();
                    if (itmDat.itemname != "NULL") {
                        t.vars.ingredient_details[t.vars.ACTIVE_MENU].actual_price[itmDat.itemname] = itmDat.price;
                    }
                });

                $("#optionList").html(e);
                t.user_selections();
                t.enable_scrolbar();
                t.reload_state();
                t.slide_function();
            }
        })
    },


    reload_state: function() {
        var e = this;
        var t;
        var n;
        var r;
        if (!e.vars.ACTIVE_MENU) {
            e.vars.ACTIVE_MENU = 'BREAD';

        }
        t = e.vars.ingredient_details[e.vars.ACTIVE_MENU];
        if (e.vars.ACTIVE_MENU == 'BREAD' && t.item_id) {
            $data = $('.main-category-list ul li input[data-id="' + t.item_id + '"]').data();
            if ($data) e.vars.current_sliced_image = $data.item_image_sliced;
        }
        t.item_id.forEach(function(e) {
        
            $('input[data-id="' + e + '"]').prop("checked", true);
            $datz  = $('input[data-id="' + e + '"]').data();
            n = e;

            $(".sub-value").each(function() {
                if ($(this).parent().find("input:checked").length > 0) {
                    $(this).show();
                    r = t.item_qty[$datz['itemname']];
                   
                    if (r != undefined && $(this).parent().find("input[data-id='" + n + "']").length > 0) {
                        $(this).find("input").hide();
                        $(this).find('input[value="' + r[0] + '"]').show()
                    }
                }
            });
        });
        e.calculate_price();
        e.slider_button_fix();
    },

    reload_state_quick_edit: function() {
        var e = this;
        var t;
        var n;
        var r;
        var html = '';
        var items_data = '';

        var items_array = [];

        $.each(e.vars.ingredient_details, function(k, ingredient ) {
            console.log("ingredient.item_id", ingredient.item_id)
            if(k != 'BREAD')
            items_array = items_array.concat(ingredient.item_id);
        })

        //$item_ids = items_array.length > 0 ? items_array.join(',') : '';
        if(items_array.length > 0){
            $item_ids = items_array.join(',');
             $.ajax({
            type: "POST",
            data: {
                'ids': $item_ids
            },
            dataType: 'json',
            beforeSend: function(){
                $(".editable-sandwich-container").mCustomScrollbar('destroy');
            },
            url: SITE_URL + 'createsandwich/get_category_items_data/',
            success: function(data) {  
                console.log("item_data", data);
                console.log("data.length", Object.keys(data).length);
                if(Object.keys(data).length > 0) items_data = data;
                
                $.each(e.vars.ingredient_details, function(k, ingredient ) {
        
                    console.log("ingredient", k);
          
                    if(k != 'BREAD')
                    {
                        var t = e.vars.ingredient_details[k];
                        html += '<div class="spiner-edit--wrapper '+ k.toLowerCase() +'-items"><label>'+ k.charAt(0) + k.slice(1).toLowerCase() +'</label>';
                        t.item_id.forEach( function(v, i) {
                            var item_name = t.item_name[i];
                            var item_qty = t.item_qty[item_name];
                            var item_id = t.item_id[i];
                            
                            var id = (k + '-check'+ (i+1)).toLowerCase();

                            html += '<div class="new_spiner-edit"><input class="cat-item" data-category_name="'+ k.toLowerCase() +'" data-empty="false" data-priority="'+ t.item_priority[i] +'" data-id="'+ t.item_id[i] +'" data-price="'+ t.item_price[i] +'" data-image="'+ t.item_image[i] +'" data-itemname="'+ item_name +'" type="checkbox" value="" name="check" id="'+ id +'">';
                            html += '<p>'+ item_name + '</p><div class="spiner-wrap">';
                                
                            html += '<div class="new_popup-spiner__edit"><a href="javascript:void(0)" class="left left_spinner" onclick="editQuantity_quickEdit(this)" data-from="edit-sandwich"></a>';
                            
                            $.each(items_data[item_id].options_id, function(index, option)
                            {
                                var tImg = "", rImg = "", lImg = "";
                                var option_id = option.id;
                                
                                if((typeof item_qty !== 'undefined') && (option.option_name.toLowerCase() == item_qty[0].toLowerCase() || item_qty[0].toLowerCase() == option.option_name.toLowerCase() + ' (' + option.option_unit +')' ))
                                {
                                    display = "block";
                                    current = "true";
                                }
                                else
                                {
                                    display = "none";
                                    current = "false";
                                }
                                if(items_data[item_id].option_images.hasOwnProperty(option_id)){
                                    if(typeof items_data[item_id].option_images[option_id]['tImg'] !== "undefined"){
                                        tImg = items_data[item_id].option_images[option_id]['tImg'];
                                    }
                                    if(typeof items_data[item_id].option_images[option_id]['rImg'] !== "undefined"){
                                        rImg = items_data[item_id].option_images[option_id]['rImg'];
                                    }
                                    if(typeof items_data[item_id].option_images[option_id]['lImg'] !== "undefined"){
                                        lImg = items_data[item_id].option_images[option_id]['lImg'];
                                    }
                                }
                                 
                                if(k == "PROTEIN"){
                                    html += '<input rel="slide" readonly="" style="display: '+ display +';" data-unit="'+ option.option_unit +'" data-optionid="'+ option_id +'" data-current="'+ current +'" data-image="" data-image_trapezoid="'+ tImg +'" data-image_round="'+ rImg +'" data-image_long="'+ lImg +'" data-price_mul="'+ option.price_mult +'" type="text" name="" class="text-box" value="'+ option.option_name.toLowerCase() + ' (' + option.option_unit +')">';
                                }else{
                                    html += '<input rel="slide" readonly="" style="display: '+ display +';" data-unit="'+ option.option_unit +'" data-optionid="'+ option_id +'" data-current="'+ current +'" data-image="" data-image_trapezoid="'+ tImg +'" data-image_round="'+ rImg +'" data-image_long="'+ lImg +'" data-price_mul="'+ option.price_mult +'" type="text" name="" class="text-box" value="'+ option.option_name.toLowerCase() +'">';
                                }

                                if(display == "block")  e.vars.ingredient_details[k].item_qty[item_name][4] = option_id;
                             })
                
                            html += '<a href="javascript:void(0)" class="right right_spinner" onclick="editQuantity_quickEdit(this)" data-from="edit-sandwich"></a></div>';
                            html += '<a href="javascript:void(0)" onclick="removeItems_quickedit(this)" class="delete-edit" data-id="'+ t.item_id[i] +'" data-category="'+ k +'"><img src="'+ SITE_URL +'app/images/edit_sandwich/delete-spiner.png"></a></div></div>';

                            console.log(e);
                        });
                        html += '</div>';
                    }
                })

                $('#sandwiches_quick_edit .editable-sandwich-container').html(html);
                
                 
                $("#sandwiches_quick_edit").show();
                $("#sandwiches-popup").hide();
                $('#sandwiches_quick_edit_add_item').hide();
                $('#add_item_ajax_loader').hide();
               // $('#sandwiches-popup_ajax_loader').hide();
                e.enable_mscrolbar();
                e.calculate_price();
                //     e.slider_button_fix();
            }
        });
        }else{
               $.each(e.vars.ingredient_details, function(k, ingredient ) 
               {
                    if(k != 'BREAD')
                    {
                        var t = e.vars.ingredient_details[k];
                        html += '<div class="spiner-edit--wrapper '+ k.toLowerCase() +'-items"><label>'+ k.charAt(0) + k.slice(1).toLowerCase() +'</label>';
                        html += '</div>';
                    }
                });

                $('#sandwiches_quick_edit .editable-sandwich-container').html(html);
                $("#sandwiches_quick_edit").show();
                $("#sandwiches-popup").hide();
                $('#sandwiches_quick_edit_add_item').hide();
                $('#add_item_ajax_loader').hide();
                 e.calculate_price();
        }
       // console.log("e.vars.ingredient_details", e.vars.ingredient_details)
    },

    enable_nav_buttons: function() {
        var e = this;
        $(".back,.next").unbind("click").on("click", function(t) {
            e.turn_bread_image();
            $( ".save-to-my-menu" ).text("SAVE").removeClass('disable');
            $total = $(".create-sandwich-menu ul li").length - 1;
            $slector = $('.create-sandwich-menu ul li a[class="active"]').parent();
            $length = $slector.index();
            if ($(t.target).attr("class") == "back") {
                if ($length != 0) $slector.prev().find("a").trigger("click")
            } else {
                if ($length < $total) $slector.next().find("a").trigger("click");
                if ($length == $total) {
                    e.show_end();
                }
            }
        })
    },

    randomBetween: function(min, max) {
        if (min < 0) rand = min + Math.random() * (Math.abs(min) + max);
        else rand = min + Math.random() * max;
        return rand = Math.floor(rand);
    },

    show_end: function() {
        var self = this;
        ha.common.setCookie('sandwich_end_screen', true, 1);
        var funWord;
        $.ajax({
            type: "POST",
            async: false,
            url: SITE_URL + 'createsandwich/getRandom_sandwich_name/',
            success: function(e) {
                funWord = e;
            }
        });
        session = ha.common.check_session_state();
       
        if (session.session_state == false) {
             if(session.facebook_only_user == false){
               ha.common.login_popup($(this));
             } else {
               ha.common.facebook_login($(this));
             }
 
            return;
        }

        $rand = self.randomBetween(0, self.vars.ingredient_details.PROTEIN.item_name.length);

        var $prot;

        proteins = self.vars.ingredient_details.PROTEIN.item_name;
        cheeses = self.vars.ingredient_details.CHEESE.item_name;

        $noFirstName = false;

        if (proteins.length > 2) {
            $noFirstName = false;
            $sandwh = 'Protein'
        } else if (proteins.length == 2) {
            $noFirstName = true;
            $sandwh = proteins[0] + ' & ' + proteins[1];

        } else if (proteins.length == 1) {
            $noFirstName = false;
            $sandwh = proteins[0];
        } else if (proteins.length == 0) {

            if (cheeses.length > 2) {
                $sandwh = 'Cheesy';
                $noFirstName = false;
            } else if (cheeses.length == 2) {
                $sandwh = cheeses[0] + ' & ' + cheeses[1];
                $noFirstName = true;
            } else if (cheeses.length == 1) {
                $noFirstName = false;
                $sandwh = cheeses[0];
            }
        }

        if (proteins.length == 0 && cheeses.length == 0) {
            $sandwh = 'Veggie';
        }

        $sandwichName = $('#namecreation').val();
        $sandwichName = $sandwichName.toUpperCase();
        $sandwichName_split = $sandwichName.split(' ');
        $ln = $sandwichName_split.length - 1;

        $last = $sandwichName_split[$ln];

        $first = ha.common.getCookie('user_fname');
        if (!$first) $first = $('.login-name-holder h1 span').text();
        if ($first[0]) $first = $first[0].toUpperCase() + $first.slice(1) + "'s";
        else $first = '';
        if (funWord) $last = funWord;
        $sandwichNamePart2 = $sandwh + " " + $last;
        $nameLen = parseInt($first.length) + parseInt($sandwichNamePart2.length);


        if ($noFirstName == false && $nameLen < 35 && $first) $sandwichName = $first + " " + $sandwichNamePart2
        else $sandwichName = $sandwichNamePart2;
        
        $sandwichName = $sandwichName.toUpperCase();
                
        $('#namecreation').val($sandwichName);


        $data = this.prepare_endScreen();
        if(self.vars.ingredient_details["BREAD"].type == 0)
            $("#final_out").html('<li style="margin-top:0px"></li> <li style="border-bottom: 1px solid;margin-bottom: 8px;"><h4 style="text-decoration: none !important;">BASE PRICE <span>+$'+BASE_FARE+'.00</span></h4></li>' + $data);
        else
            $("#final_out").html('<li style="margin-top:0px"></li>' + $data);


        $('#totalPrice').html('$' + self.vars.current_price);
        $(".create-sandwich-menu").hide();


        $(".bread_images").css("margin", "36px 0 0 0");
        $(".create-sandwich-right-wrapper").hide();
        $("#ovrlayx").show();
        $("#ovrlayx").find(".create-sandwich-right-wrapper").show();
        $(".back").parent().parent().parent().hide();

        $(".edit-sandwich").unbind("click").on("click", function() {
            $("#ovrlayx").hide();
            $(".create-sandwich-menu").show();
            $(".bread_images").css("margin", "");
            $(".create-sandwich-right-wrapper").show();
            $(".back").parent().parent().parent().show();
            self.enable_scrolbar();
        });

        $('#namecreation').focus();
        self.enable_scrolbar();


        $('.save-to-my-menu,.am-done').not('#finished').unbind('click').on('click', function(e) {

            e.preventDefault();

            if ($(this).hasClass('disable') && $(this).hasClass('save-to-my-menu')) 
            {
                alert("This item is already saved!");
                return false;
            }

            var active;
            $(".am-done").addClass('disable');
            $(".save-to-my-menu").addClass('disable');
            
            if ($(this).hasClass('save-to-my-menu')) {
                active = 1;
                retchk = false;
            } else if ($(this).hasClass('am-done')) {
                active = 1;
                retchk = true;
            } else {
                active = 1;
                retchk = true;
            }
          
            is_private = $('#ov_check2:checked').length;

            is_pub = is_private == 1 ? 0 : 1;

            name_cr = $('#namecreation').val();
            name_cr = name_cr.toUpperCase();
            str = $( ".save-to-my-menu" ).text();
            if(str == "SAVE")
            {
                self.sync_data_to_db(is_pub, name_cr, active, self.temp_name, retchk, $(this), '', '');
            }
            else
            {
                $.ajax({
                type: "POST",
                url: SITE_URL + 'createsandwich/getLastSandwichId/',
                data : {'id':SESSION_ID},
                dataType: 'json',
                success: function(id) {
                    self.add_sandwich_to_cart(id, SESSION_ID, "user_sandwich");
                    window.open(SITE_URL + 'checkout/choosedeliveryorpickup', '_self');

                    }
                });
            }
        });

       
        $('.share_to_fb').unbind("click").on("click", function(e) {
              e.preventDefault();
              
            is_pub = $('#ov_check2:checked').length;
            name_cr = $('#namecreation').val();
            name_cr = name_cr.toUpperCase();
            $('.sharefbloader').show();
            if ($(this).attr("rel")) {
                sanwid = $(this).attr("rel");
            } else {
                sanwid = self.sync_data_to_db(is_pub, name_cr, 0, self.temp_name, 'share');
                 
            }

            if (sanwid) {
                self.add_to_sandwich_menu(sanwid);
                $('.sharefbloader').hide();
                $(this).attr('rel', sanwid);
               

                var feed = {
                    method: 'feed',
                    name: name_cr,
                    caption: name_cr,
                    description: self.get_desc_from_sandwich(),
                    link: SITE_URL + 'sandwich/gallery/?galleryItem=' + sanwid,
                    picture: $sandwich_img_live_uri + SESSION_ID + '/sandwich_' + sanwid + '_' + SESSION_ID + '.png',
                    display: 'popup'
                };
              

                FB.ui(feed);
            }

        });

    },

    
    get_desc_from_sandwich : function(){
        
        var self = this;
        itmArray = [];
        ings = self.vars.ingredient_details;
        kys  = Object.keys(ings);
        kys.forEach(function(e){
         if(e){
             
            items =  self.vars.ingredient_details[e].item_name
            
            items.forEach(function(i){
             
                if(self.vars.ingredient_details[e].item_qty[i] && self.vars.ingredient_details[e].item_qty[i].length){
                  qty = self.vars.ingredient_details[e].item_qty[i];    
                  itmArray.push(i+"("+qty[1]+")");
                } else {
                    itmArray.push(i);
                }
            });
            
         }      
            
        });
        
         
        return itmArray.join(", ");
        
        
    },

    prepare_endScreen: function() {
        $out = "";
        $i = 0;
        var self = this;
        self.calculate_price();
        $data = self.vars.ingredient_details;

        Object.keys($data).forEach(function(e) {



            if (e != "BREAD") {

            }

            if (self.vars.individual_price[e] < 0) sign = '-';
            else sign = '+';
            vals = Math.abs(self.vars.individual_price[e]);
            
            $price = vals.toFixed(2);
            
            if (e == "BREAD") {
                if($price >= parseInt(BASE_FARE)){
                    $price = $price; 
                } else {
                    
                       $price = $price; 
                }
            }
            $price = parseFloat($price);
            $price = $price.toFixed(2);
            
            $out += "<li>";
            $out += "<h4>" + e + " <span>" + sign + "$" + $price + "</span></h4>";

            if (e == "BREAD") {
                $out += "<p>" + $data[e].item_name[0] + "</p>"
            } else {
                $sub = "";
                $en_ln = $data[e].item_name.length;
                $qty = $data[e].item_qty;



                $data[e].item_name.forEach(function(e, i) {

                    if (!e) {
                        $final_name = "None"
                    }

                    if ($qty[e] != undefined) {
                        if (e != "NULL") $final_name = e + " (" + $qty[e][1] + ")";
                        else $final_name = "None"
                    } else {
                        if (e != "NULL") $final_name = e + " (1)";
                        else $final_name = "None"
                    }

                    if (i < $en_ln - 1) {
                        delimit = ", "
                    } else {
                        delimit = ""
                    }

                    $sub += $final_name + delimit + " "
                });

                if ($data[e].item_name.length > 0) {
                    $out += "<p>" + $sub + "</p>";
                } else {
                    $out += "<p>" + "None" + "</p>";
                }

            }
            $out += "</li>"
        });
        return $out
    },

    
    clear_all_selections : function(){
    
        var self = this;
         ha.common.setCookie('sandwich_end_screen', false, 1);
         sessionclre = self.clear_temp_session();
         
        if(sessionclre == true){ 
        
         locationSplit = window.location.href.split("/");
       
         if(locationSplit.indexOf("createsandwich") != -1 ){
            
            if( locationSplit.indexOf("index")  != -1 ){
                
                idIndex = locationSplit.indexOf("index");
                idIndex = idIndex+1;
             
                if( locationSplit[idIndex] ){
                     window.location.href = SITE_URL+"createsandwich/";
                } else {
                     window.location.reload();
                }
                
            } else {
                
             window.location.reload();
           } 
            
         } else {
            
             window.location.reload();
        } }
       

    },
    
     
    /*
     * Clear all temp session data for  the sandwich, including bread, protien selections. prices etc. 
     */
    
    clear_temp_session : function(){
        
        var self = this;
        var ret  = false; 
        $.ajax({
            type: "POST",
            async:false,
            url: SITE_URL + 'createsandwich/clear_temp_session/',
            success:function(){  ret = true; },
            });
        return ret;
    },
    

    user_selections: function() {
        var e = this;
        $selectors = $("#optionList input");
        $selectors.unbind("click").not('.sub-value input').on("click", function(t) {

            if ($("#optionList #null:checked").length) {} else {
                if ($(this).not("#null").length == 0) e.calculate_price();
            }

            var option_data;

            if ($(this).parent("li").find(".sub-value:visible").length > 0) {
                $(this).parent("li").find(".sub-value").find('input').hide();
                $(this).parent("li").find(".sub-value").hide();
            } else {
                var sub_index = $('.sub-value').find('input[value~="NORMAL"]').index();
                if (sub_index == -1 ) sub_index = 0;
                var sub_index1 = $('.sub-value').find('input[value~="Full Portion"]').index();
                if(sub_index1 == -1){
                    sub_index = 1;
                } 
                $(this).parent("li").find(".sub-value").find('input').hide();
                $($(this).parent("li").find(".sub-value").find('input')[sub_index]).show();
                $(this).parent("li").find(".sub-value").show();
            }
            e.slider_button_fix();
            console.log(e.vars)
            option_data = $(this).parent().find(".sub-value").find('input:visible').data();
            console.log(option_data)
            data = $(this).data();
            
            $num = e.ing_availaility_check();
            if (e.vars.ACTIVE_MENU == 'BREAD' || e.vars.ACTIVE_MENU == "") {
                e.vars.ingredient_details["BREAD"].type = data.bread_type;
                e.vars.ingredient_details["BREAD"].shape = data.shape;

                if ($num > 0) {
                    e.vars.ingredient_details["BREAD"].item_image[0] = data.item_image_sliced;
                }
                e.update_json_data('create_edit');

            }
            $('.landing-page').hide();
            $('.button-holder').show();

            $image = e.select_image_by_shape(data, $num, option_data);

            image_url = e.vars.image_path + $image;
            console.log("image",$image)
            $name = data.itemname;
            $price = data.price;
            $sliced_image = data.item_image_sliced;
            $id = data.id;
            $priority = data.priority;

            className = e.Get_imageWrapperClassName();

            if (data.type == "replace") {
                
                itm_price = parseFloat(data.price);
                e.vars.current_sliced_image = $sliced_image;
                var  bread_img = $($(className + " .image-holder")[0]).find("img");
                
                $(bread_img).hide(0);
                $(bread_img).attr("src", image_url);
                $(bread_img).load(function(){
                     $(bread_img).fadeIn(300);
                });
                
                e.on_complete_actions(true, $name, $price, $image, $id)
                
            } else {
                if (t.target.checked == true) {
                    if (data.empty == true) {

                        e.on_complete_actions(false, "NULL", "0", '', '', '');
                        if ($("#optionList").find("input:checked").not("#null").length > 0) {


                            $("#optionList").find("input:checked").not("#null").each(function() {
                                data_fe = $(this).data();


                                $(className + '.image-holder[rel="' + data_fe.itemname + '"]').find('img').fadeOut(300, function() {
                                    $(className + '.image-holder[rel="' + data_fe.itemname + '"]').remove();
                                });


                            });
                            $("#optionList").find("input:checked").not("#null").prop("checked", false);
                            ACTIVE_TAB = e.vars.ACTIVE_MENU;
                            e.vars.ingredient_details[ACTIVE_TAB].item_name = ["NULL"];
                            e.vars.ingredient_details[ACTIVE_TAB].item_price = [0];
                            if (e.vars.ingredient_details[ACTIVE_TAB].item_priority) e.vars.ingredient_details[ACTIVE_TAB].item_priority = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_id = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_image = [];
                            e.vars.ingredient_details[ACTIVE_TAB].item_qty = {}
                            e.calculate_price();

                        }
                    } else {


                        if ($("#optionList #null:checked").length > 0) {
                            $("#optionList #null").prop("checked", false);
                            ACTIVE_TAB = e.vars.ACTIVE_MENU;
                            $name_index = $.inArray("NULL", e.vars.ingredient_details[ACTIVE_TAB].item_name);
                            $price_index = $.inArray(0, e.vars.ingredient_details[ACTIVE_TAB].item_price);
                            e.vars.ingredient_details[ACTIVE_TAB].item_name.splice($name_index, 1);
                            e.vars.ingredient_details[ACTIVE_TAB].item_price.splice($price_index, 1);
                        }

                        e.dynamic_option_selection($(this), true);

                        im = [e.vars.current_sliced_image];
                        if (im != null && im != '' && e.vars.ACTIVE_MENU != "BREAD") {
                            if (e.vars.ingredient_details["BREAD"].item_image[0] != im) {
                                e.vars.ingredient_details["BREAD"].item_image = im;
                                e.turn_bread_image();
                            }
                        }

                        $select = $(this).parent().find(".sub-value");
                        if ($select.length > 0) {
                            $dat = $select.find("input:visible").data();
                            if ($dat) {
                                $arr = [];
                                $arr[0] = $select.find("input:visible").val();
                                $arr[1] = $dat.unit;
                                $arr[2] = $dat.price_mul;
                                $arr[3] = $dat.image;
                                $arr[4] = $dat.optionid;
                                e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$name] = $arr;
                            }
                        }

                        $pv = $priority;
                        e.set_images_in_position($priority, image_url, $(this));
                        e.on_complete_actions(false, $name, $price, $image, $id, $pv);
                        e.auto_select_option_fix();
                    }
                } else {
                    e.remove_items($id);
                    e.dynamic_option_selection($(this), false);
                    e.auto_select_option_fix();
                }
            }
        })
    },

    user_selections_on_quick_edit: function() {
        var e = this;

        $('#sandwiches_quick_edit_add_item a.add-item-to-sandwich').unbind("click").on("click", function(t) {
            $('#add_item_ajax_loader').show();

            $selectors = $('#add-item-select option:selected');
            e.vars.ACTIVE_MENU = $('#add-item-category-select option:selected').text();
            
            data = $('#add-item-select option:selected').data();
            $select = $('#sandwiches_quick_edit_add_item .new_popup-spiner__edit');
            $dat = $select.find("input:visible").data();

            $num = e.ing_availaility_check();

            $image = e.select_image_by_shape(data, $num, $dat);

            image_url = e.vars.image_path + $image;
            $name = data.itemname;
            $price = data.price;
            $sliced_image = data.item_image_sliced;
            $id = data.id;
            $priority = data.priority;

            className = e.Get_imageWrapperClassName();
                
                if ($dat) {
                    $arr = [];
                    $arr[0] = $select.find("input:visible").val();
                    if(e.vars.ACTIVE_MENU == 'PROTEIN') $arr[0] = $select.find("input:visible").val().split(' (')[0];
            else $arr[0] = $select.find("input:visible").val();
                    $arr[1] = $dat.unit;
                    $arr[2] = $dat.price_mul;
                    $arr[3] = $dat.image;
                    $arr[4] = $dat.optionid;
                    console.log("e.vars.ACTIVE_MENU", e.vars.ACTIVE_MENU)
                    
                    if(Object.keys(e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty).length == 0)
                    {
                        e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty = {};
                    }
                    e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$name] = $arr;
                    console.log("e.vars.ingredient_details", e.vars.ingredient_details)
                }

            $pv = $priority;
            
            e.on_complete_actions(false, $name, $price, $image, $id, $pv);
            e.reload_state_quick_edit();
            setTimeout(function(){
                e.set_images_in_position($priority, image_url, $selectors);
            }, 3000);
            
            e.auto_select_option_fix_on_quickedit();

            
                  
        })
    },

    turn_bread_image: function() {
        var self = this;
        var $img = self.vars.image_path + self.vars.current_sliced_image;
        var imgsedisp = $(".bread_images .bread_image div:first").find('img');
        current = $(imgsedisp).attr('src');

        if (self.vars.current_sliced_image && current.indexOf($img) == -1) {
            $(imgsedisp).fadeOut(300, function() {
                if ($img) $(imgsedisp).attr('src', $img);
                $(imgsedisp).load(function(){
                    $(imgsedisp).fadeIn(300);
                });
            });
        }
    },

    ing_availaility_check: function() {
        var self = this;
        count = 0;
        Object.keys(self.vars.ingredient_details).forEach(function(e) {
            if (e != 'BREAD') count += self.vars.ingredient_details[e].item_id.length;
        });
        return count;
    },
    dynamic_option_selection: function(selector, dataAdding) {

        var self = this;
        ACTIVE_TAB = self.vars.ACTIVE_MENU;
        if (dataAdding == true) inc = 1;
        else inc = 0
        len = self.vars.ingredient_details[ACTIVE_TAB].item_name.length + inc;

        if (self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode != undefined) {

            parent = $(selector).parent();


            sel = $(parent).find('.sub-value');

            if (self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode != true) {



                inputs = $(sel).find('input');



                if (len > 1) {
                    if ($(inputs[0]).length > 0) {
                        $('.sub-value').find('input').hide();
                        $('.sub-value').each(function(e, j) {
                            $($(j).find('input')[self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_second]).show();
                        });
                        $(inputs[self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_second]).show();

                    }

                } else {
                    if ($(inputs[self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_first]).length > 0) {
                        $(sel).find('input').hide();
                        $(inputs[self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].option_default_Choice_first]).show();

                    }

                }
            }
        }

        self.slider_button_fix();
    },


    slider_button_fix: function() {
        var self = this;
      
        actLen    =  $('.sub-value:visible').length;
        itemIndex = $('.sub-value:visible .text-box input:visible').index();
        
        if(!self.vars.ACTIVE_MENU){
            self.vars.ACTIVE_MENU = "BREAD";
        } 
        defaultChoice =  self.vars.ingredient_details[self.vars.ACTIVE_MENU].option_default_Choice_first;
        console.log(defaultChoice)
        
        
        if( self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode != undefined ) 
              modeManual = self.vars.ingredient_details[ha.sandwichcreator.vars.ACTIVE_MENU].manual_mode;
        else  modeManual = false;
        
        if( actLen == 1 && modeManual == false && itemIndex != defaultChoice ){
               $inputs  =  $('.sub-value:visible').find('input');
               $($inputs).hide();              
               $($inputs[defaultChoice]).show();
               
        }


        $('.sub-value').each(function(e) {
            count = $(this).find('input').length;
            count = count - 1;
            index = $(this).find('input:visible').index();
            if (index == 0) {
                $(this).find('.left').removeClass('left-hover');
                $(this).find('.right').addClass('right-hover');
            } else if ((index > 0) && (index < count)) {
                $(this).find('.left').addClass('left-hover');
                $(this).find('.right').addClass('right-hover');
            } else if (index == count) {
                $(this).find('.right').removeClass('right-hover');
                $(this).find('.left').addClass('left-hover');
            }

        });

    },

    auto_select_option_fix: function() {
        var e = this;

        $('.sub-value').find('input:visible').each(function() {
            $ptext = $(this).parent().parent().parent().find('input[type="checkbox"]').data();
            $arr = [];
            $value = $(this).val();
            $data = $(this).data();

            $arr[0] = $value;
            $arr[1] = $data.unit;
            $arr[2] = $data.price_mul;
            $arr[3] = $data.image;
            $arr[4] = $data.optionid;

            e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$ptext.itemname] = $arr;

            $itn_id = $ptext.id;
            new_price = parseFloat($ptext.price) * parseFloat($data.price_mul);
            $p_index = $.inArray($itn_id, e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_id);
            if ($p_index != -1) {

                if (new_price > 0) e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_price[$p_index] = new_price;

            }
        });

        e.calculate_price();

    },

    auto_select_option_fix_on_quickedit: function(){
        var e = this;

            $optionEle = $('#sandwiches_quick_edit_add_item .new_popup-spiner__edit input:visible');
            $ptext = $('#add-item-select option:selected').data();
            $arr = [];
            $value = $optionEle.val();
            $data = $optionEle.data();

            $arr[0] = $value;
            $arr[1] = $data.unit;
            $arr[2] = $data.price_mul;
            $arr[3] = $data.image;
             $arr[4] = $data.optionid;

            e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_qty[$ptext.itemname] = $arr;

            $itn_id = $ptext.id;
            new_price = parseFloat($ptext.price) * parseFloat($data.price_mul);
            $p_index = $.inArray($itn_id, e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_id);
            if ($p_index != -1) {

                if (new_price > 0) e.vars.ingredient_details[e.vars.ACTIVE_MENU].item_price[$p_index] = new_price;

            }

        e.calculate_price();
    },

    set_images_in_position: function(thisPos, image, currentObj ) {
        console.log("image_path", image)
        var self = this;
        var $pos = -1;
        var prev, next;
        var priorityArry = [];
    //For normal edit sandwich 
    if(document.getElementById("optionList")){
        $('#optionList li input:checked').each(function() {
            $data = $(this).data();
            if ($data) priorityArry.push($data.priority);
        });
    }else{ // for quick sandwich edit
        var cat_name = $(currentObj).data('category_name');
        $('#sandwiches_quick_edit .'+ cat_name +'-items input[class="cat-item"]').each(function() {
            $data = $(this).data();
            if ($data) priorityArry.push($data.priority);
        });
    }

        priorityArry = priorityArry.sort(function(a, b) {
            return a - b;
        });


        console.log($data)
        if (priorityArry.length > 0) length = priorityArry.length - 1;
        else length = 0;
        mypos = $.inArray(thisPos, priorityArry);

        if (length == 0) {
            $meth = 'append';
            $priority = priorityArry[mypos];

        } else {

            if (mypos > 0) {
                $meth = 'after';
                $priority = priorityArry[mypos - 1];

            } else {
                $meth = 'before';
                $priority = priorityArry[mypos + 1];
            }


        }

        if(document.getElementById("optionList")){
            itemDat = $('#optionList li input[data-priority="' + $priority + '"]').data();
            currentDat = $('#optionList li input[data-priority="' + thisPos + '"]').data();
        }else{
            itemDat = $('#sandwiches_quick_edit .'+ cat_name +'-items input[class="cat-item"][data-priority="' + $priority + '"]').data();
            currentDat = $('#sandwiches_quick_edit .'+ cat_name +'-items input[class="cat-item"][data-priority="' + thisPos + '"]').data();
        }
        console.log("itemDat", itemDat)
        console.log("currentDat", currentDat)

        current_data = $(currentObj).data();

        currentName = currentDat.itemname;
        $Itemname = current_data.itemname;
        prev_name = itemDat.itemname;


        className = self.Get_imageWrapperClassName();
        //remove if image of that itemname already exists to render new image
        
       // $(".bread_images " + className + " .image-holder[rel='" + currentDat.itemname + "']").remove();
       var old_item_image = $(".bread_images " + className + " .image-holder[rel='" + currentDat.itemname + "']");
       old_item_image.attr("rel", "old_item_image");
        switch ($meth) {
            case 'append':

                $(".bread_images " + className).append('<div  rel="' + $Itemname + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
                break;
            case 'after':

                $(".bread_images " + className + " .image-holder[rel='" + prev_name + "']").after('<div  rel="' + currentName + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
                break;
            case 'before':

                $(".bread_images " + className + " .image-holder[rel='" + prev_name + "']").before('<div  rel="' + currentName + '" class="image-holder"> <img style="display: none;" src="' + image + '" alt=""> </div>');
        }
        
        var image_ready = $(".bread_images " + className + " .image-holder[rel='" + currentDat.itemname + "']").find('img');
        $(image_ready).load(function(){
            $(image_ready).fadeIn(600);
            old_item_image.fadeOut(400, function(){
                old_item_image.remove();
            });  
        });

        if(image == "")
        {
            old_item_image.fadeOut(400, function(){
                old_item_image.remove();
            }); 
        }
    },


    Get_imageWrapperClassName: function() {
        var self = this;

        if (self.vars.ACTIVE_MENU == "BREAD") className = '.bread_image';
        else if (self.vars.ACTIVE_MENU == "PROTEIN") className = '.protein_image';
        else if (self.vars.ACTIVE_MENU == "CHEESE") className = '.cheese_image';
        else if (self.vars.ACTIVE_MENU == "TOPPINGS") className = '.topping_image';
        else if (self.vars.ACTIVE_MENU == "CONDIMENTS") className = '.condiments_image';
        else className = '.bread_image';
        return className;
    },

    update_json_data: function(from) {

        var self = this;
        var $data;
        var tmp = [];
        $data = self.vars.ingredient_details;
        Object.keys($data).forEach(function(e) {

            if (e != "BREAD") {

                if ($data[e].item_id.length > 0) {
                    $data[e].item_id.forEach(function(item_ids) {
                        tmp.push(item_ids);
                    });
                }
            }

        });

        $ids = tmp.join(",");
        
       

        $.ajax({
            type: "POST",
            data: {
                'ids': $ids
            },
            dataType: 'json',
            url: SITE_URL + 'createsandwich/get_category_items_data/',
            success: function(e) {
                

                if (Object.keys(e) != null && Object.keys(e).length > 0 ) {
                    //e.forEach(function(iData) {
                    $.each(e, function(index, iData) {
                        Object.keys(self.vars.ingredient_details).forEach(function(key) {

                            if (self.vars.ingredient_details[key].item_id.length > 0) {
                                self.vars.ingredient_details[key].item_id.forEach(function(iKey, iIndex) {

                                    if (iData.id == iKey) {
                                        $option_id = self.vars.ingredient_details[key].item_qty[iData.item_name][4];
                                        switch (self.vars.ingredient_details["BREAD"].shape) {
                                            case 0:
                                                $image = typeof iData.option_images[$option_id] !== 'undefined' ? iData.option_images[$option_id].lImg : iData.image_long;
                                                break;

                                            case 1:
                                                $image = typeof iData.option_images[$option_id] !== 'undefined' ? iData.option_images[$option_id].rImg : iData.image_round;
                                                break;

                                            case 2:
                                                $image = typeof iData.option_images[$option_id] !== 'undefined' ? iData.option_images[$option_id].tImg : iData.image_trapezoid;
                                                break;

                                        }

                                        self.vars.ingredient_details[key].item_image[iIndex] = $image;
                                    }

                                });
                            }

                        });
                    });
                }

                if(from == 'create_edit'){
                    self.load_all_data(self.vars.ingredient_details);
                    self.save_to_temp_session();
                }else if(from == 'quick_edit'){
                    self.load_all_data_quickEdit(self.vars.ingredient_details);
                }
                
            }
        });



    },

    select_image_by_shape: function(data, num, option_data) {
          console.log("option_data", option_data);
        self = this;
        $img = '';
        if (self.vars.ACTIVE_MENU == "BREAD" || self.vars.ACTIVE_MENU == "") {

            if (num > 0) return data.item_image_sliced;
            else return data.image;
        }

        if(option_data){
            switch (self.vars.ingredient_details["BREAD"].shape) {
                case 0:
                    $img = option_data.image_long;
                    break;
                case 1:
                    $img = option_data.image_round;
                    break;
                case 2:
                    $img = option_data.image_trapezoid;
                    break;
            }
        }

        return $img;
    },

    remove_items: function($id) {
        var r = this;
        ACTIVE_TAB = r.vars.ACTIVE_MENU;
        var $item_name;
        if ($id) {
            $id_index = $.inArray($id, r.vars.ingredient_details[ACTIVE_TAB].item_id);
            if ($id_index != -1) {
                $item_name = r.vars.ingredient_details[ACTIVE_TAB].item_name[$id_index];

                r.vars.ingredient_details[ACTIVE_TAB].item_name.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_price.splice($id_index, 1);
                if (r.vars.ingredient_details[ACTIVE_TAB].item_priority) r.vars.ingredient_details[ACTIVE_TAB].item_priority.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_id.splice($id_index, 1);
                r.vars.ingredient_details[ACTIVE_TAB].item_image.splice($id_index, 1);
                delete r.vars.ingredient_details[ACTIVE_TAB].item_qty[$item_name];

                className = r.Get_imageWrapperClassName();

                $(className + ' .image-holder[rel="' + $item_name + '"]').find('img').fadeOut(500, function() {
                    $(className + ' .image-holder[rel="' + $item_name + '"]').remove();

                });

                r.calculate_price();
            }
        } else if (r.vars.ingredient_details[ACTIVE_TAB].item_name.length > 0) {
            if (r.vars.ingredient_details[ACTIVE_TAB].item_name[0] == "NULL") {
                r.vars.ingredient_details[ACTIVE_TAB].item_name.splice(0, 1);
            }
        }
    },

    on_complete_actions: function(e, t, n, w, z, s) {
        var i = this;
        $sel = $(".create-sandwich-menu ul li").find("a:contains(" + i.vars.ACTIVE_MENU + ")").parent().next().find("a");
        $txt = $($sel).text();


        if (e == true) {
            if (z) {
                i.vars.ingredient_details.BREAD.item_name[0] = t;
                i.vars.ingredient_details.BREAD.item_price[0] = n;
                i.vars.ingredient_details.BREAD.item_image[0] = w;
                i.vars.ingredient_details.BREAD.item_id[0] = z;
            }
        } else {
            if (z) {


                if (i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name.length == 0 && i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price.length == 1) {
                    i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price = [];
                }

                if ($.inArray(z, i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_id) == -1) {
                    id_index = i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_id.push(z)
                    id_index = id_index - 1;
                    if (t) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name[id_index] = t;
                    if (n) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_price[id_index] = n;
                    if (s) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_priority[id_index] = s;
                    if (w) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_image[id_index] = w;

                }else{
                    id_index = i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_id.indexOf(z);
                    if (w) i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_image[id_index] = w;
                }



            } else if (t == "NULL") {
                if ($.inArray(t, i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name) == -1) {
                    i.vars.ingredient_details[i.vars.ACTIVE_MENU].item_name.push(t)
                }
            }
        }
        i.calculate_price();
    },

    validate: function(e) {
        var t = this;
        var n = [];
        var r = false;
        if (e) {
            $active = e
        } else {
            $active = t.vars.ACTIVE_MENU
        }
        t.vars.validators.forEach(function(e) {
            selections = t.vars.ingredient_details[e].item_name.length;


            switch (e) {
                case "BREAD":
                    if (selections == 0) {
                        n.push("Select a bread to continue");
                    }
                    break;
                case "PROTEIN":

                    if (selections == 0) {

                    }

                    break;
                case "CHEESE":

                    if (selections == 0) {


                    } else {

                    }
                    break
            }
        });
        switch (n.length) {
            case 3:
                if ($active != t.vars.validators[0]) {
                    alert(n[0]);
                    r = true
                }
                break;
            case 2:
                if ($active != t.vars.validators[0] && $active != t.vars.validators[1]) {
                    alert(n[0]);
                    r = true;
                }
                break;
            case 1:
                if (t.vars.ingredient_details["BREAD"].item_name.length == 0) {
                    alert("Select a bread to continue");
                    r = true;
                } else {}
                break;
            default:
                r = false;
                break;
        }
        return r;
    },
    
    add_to_sandwich_menu : function(id){
        
        var menuAdd = false;
        
        $.ajax({
            type: "POST",
            data: {
                'id': id,
                "uid": SESSION_ID
            },
            async : false,
            url: SITE_URL + 'sandwich/add_menu_multi/',
            success: function(e) {
               menuAdd = true;
            }
        });
        
        return menuAdd;
    },

    enable_scrolbar: function() {
        
    $('.scroll-bar').jScrollPane();
            setTimeout(function(){
                 pane = $('.scroll-bar').data('jsp');
                 pane.reinitialise()
            },100);
       
    },
    enable_mscrolbar: function() {
       // if ($(".editable-sandwich-container").length > 0) {
             //   $(".editable-sandwich-container").mCustomScrollbar('destroy');
                $(".editable-sandwich-container").mCustomScrollbar();
           
       // }
    },
    
    tmpTransAnim : 0, 

    added_to_cart_transition: function(e) {
     
        var currentCount = $('.shopping-cart span').text();
        
        var difference = 0;
        if(parseInt(e) == 1 && parseInt(currentCount) == 1){
            difference = 1;             
        }else{
             difference = parseInt(e) - parseInt(currentCount);
        }    
        
       if($(".popup-wrapper:visible").length > 0){ 
        $(".close-button:visible").trigger("click");
       }
        
        
        $("div.dynamic_updates div.left-block").html("<p>" + difference + " item(s) added to cart.</p>");
        $("div.dynamic_updates").slideDown("slow");
        $('.shopping-cart span').text(e);
        
        setTimeout(function() {
            $("div.dynamic_updates").slideUp("slow",function(){  ha.sandwichcreator.tmpTransAnim = 1; });
        }, 4000);

        var scrollHeight = $(window).scrollTop();
        if (scrollHeight > 100) {
            $("div.dynamic_updates_fixed div.left-block").html("<p>" + difference + " item(s) added to cart.</p>");
            $("div.dynamic_updates_fixed").slideDown("slow");
            
            setTimeout(function() {
                $("div.dynamic_updates_fixed").slideUp("slow",function(){  ha.sandwichcreator.tmpTransAnim = 1; });
            }, 4000);
        }

        $("input.text-box.qty").val("01");
    }
};

$(function() {
    ha.sandwichcreator.init();
    loc = window.location.href;
    arr = loc.split('/');




    $(document).on("keyup", function(e) {
        e.preventDefault();
        if (e.keyCode == 13) {

            if ($("#forgotForm:visible").length > 0) {
                
                 $('#forgot_btn').trigger('click');
                 
            } else {
                
                 $('#login_btn').trigger('click');
            }

            if ($("#login-form-cookie:visible").length > 0) {
                $('#login_btn_cookie').trigger('click');
            }

        }

    });

    if ($.inArray('myaccount', arr) != -1 && !SESSION_ID) {
        $("#login").show(0, function() {
            $('.close-button').unbind('click').on('click', function() {
                window.open(SITE_URL, '_self');
            });
        });
    }

    $('.add-cart').unbind('click').on('click', function(e) {
        e.preventDefault();

    });

    $(".flag-sandwich").click(function() {

        var san_id = $("#hidden_sandwich_id").val();
        var state = $(this).data("flag");
        $.ajax({
            type: "POST",
            data: {
                'id': san_id,
                'status': state
            },
            url: SITE_URL + 'sandwich/set_flag/',
            success: function(e) {
                if (state == 1) {
                    $(".flag-sandwich").text('This sandwich has been flagged');
                    $('.flag-sandwich').css('text-decoration', 'none');
                    $('.flag-sandwich').css('cursor', 'auto');

                } else {

                }
            }
        });

    });
});

function applyDiscount() {

    var discount_id = $("#apply_discount").val();
    var discount_code = "";
    
    if(discount_id){
        
    } else {
        discount_id = ha.common.getCookie("discount_code")
        if(discount_id.length && discount_id != "undefined"){
            $("#apply_discount").val(discount_id)
            ha.sandwichcreator.vars.appliedDiscount = discount_id ;
        }else{
            discount_id = 0;
        }
    }
    
    if(!discount_id){
        return false;
    }
    
    discount_code = discount_id;
    
    
    $.ajax({
        type: "POST",
        data: {
            "code": discount_id,
            "user_id": SESSION_ID
        },
        url: SITE_URL + 'checkout/applyDiscount',
        dataType: "json",
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        },
        success: function(e) {

            if (e == "") {
                ha.common.setCookie("discount_code","",-1);
                alert("Your Gift card is not valid! Please check!");
                return false;
            }
            
            if(e.status=="error" && e.message == "single use per user"){
                ha.common.setCookie("discount_code","",-1);
                alert("Woops! Looks like this code was already used.");
                $("#apply_discount").val("");
                return false;
            }
            
            if(e.status=="error" && e.message == "not alive"){
                    ha.common.setCookie("discount_code","",-1);
                    alert("This promo code is not valid. Please try again!");
                    $("#apply_discount").val("");
                    return false;
            }
            
            

            var discount = e;
            var discount_id = discount.id;
            
            ha.common.setCookie("discount_code",discount_code,1);
            ha.sandwichcreator.vars.appliedDiscount = discount_code ;
            $("#apply_discount").val(discount_code)

            var newtextbox = '<input id="hidden_discount_id" type="hidden" value="' + discount_id + '"/>';
            newtextbox += '<input id="hidden_discount_amount" type="hidden" value="0"/>';
            $("#hidden_discount").append(newtextbox);
            
            var tax = $("#hidden_tax").val();

            if (discount.applicable_to == 1) {

                var select_tip_amount = $("#select_tip_amount").val();

                if (discount.type == 0) {
                    var per = parseFloat(discount.discount_amount) / 100;
                    var hidden_grand_total = $("#hidden_grand_total").val().replace(",", "");
                    hidden_grand_total = parseFloat(hidden_grand_total);


                    if (discount.minimum_order == 1) {

                        if (hidden_grand_total < parseFloat(discount.minimum_order_amount)) {

                            $("input#apply_discount").val("");
                            ha.common.setCookie("discount_code","",-1);
                            alert(" Sorry, this discount code can only be used for orders of $" + discount.minimum_order_amount + " or more.");
                            //alert("Minimum amount for coupon is $" + discount.minimum_order_amount);
                            return false;

                        }

                    }
                    if (discount.maximum_order == 1) {

                        if (hidden_grand_total > parseFloat(discount.maximum_order_amount)) {

                            $("input#apply_discount").val("");
                            ha.common.setCookie("discount_code","",-1);
                            alert("Sorry, this discount code can only be used for orders up to $" + discount.maximum_order_amount+ ".") ;
                            return false;

                        }

                    }

                    var amount = hidden_grand_total * per;
                    amount = parseFloat(amount.toFixed(2));
                    
                    var value_remain = hidden_grand_total - amount;
                    
                    
                    
                    $(".coupon_result").find('.sub-total-discount').text(amount);
                    $(".coupon_result").show();
                    
                    
                   
                    var final_cal = parseFloat(value_remain) ;
                    final_cal = parseFloat(final_cal) + parseFloat(tax);
                    
                    final_cal = final_cal.toFixed(2);
                    $(".sub-total").find('.grand-total').text(final_cal);
                    $("#hidden_grand_total").val(final_cal)

                    $('#hidden_discount_id').val(discount.id);
                    $('#hidden_discount_amount').val(amount);


                } else {
                    var hidden_grand_total = $("#hidden_grand_total").val().replace(",", "");
                    hidden_grand_total = parseFloat(hidden_grand_total);

                    if (discount.minimum_order == 1) {

                        if (hidden_grand_total < parseFloat(discount.minimum_order_amount)) {

                            $("input#apply_discount").val("");
                            ha.common.setCookie("discount_code","",-1);
                            //alert("Minimum amount for coupon is $" + discount.minimum_order_amount);
                            alert(" Sorry, this discount code can only be used for orders of $" + discount.minimum_order_amount + " or more.");
                            return false;

                        }

                    }
                    if (discount.maximum_order == 1) {

                        if (hidden_grand_total > parseFloat(discount.maximum_order_amount)) {

                            $("input#apply_discount").val("");
                            ha.common.setCookie("discount_code","",-1);
                            alert("Sorry, this discount code can only be used for orders up to $" + discount.maximum_order_amount+ ".") ;
                            return false;

                        }

                    }

                    var value_remain = hidden_grand_total - parseFloat(discount.discount_amount);

                    var amount = parseFloat(discount.discount_amount);
                    amount = amount.toFixed(2);

                    $(".coupon_result").find('.sub-total-discount').text(amount);
                    $(".coupon_result").show();


                    var final_cal = parseFloat(value_remain);
                    final_cal = parseFloat(final_cal) + parseFloat(tax);
                    
                    final_cal = final_cal.toFixed(2);
                    $(".sub-total").find('.grand-total').text(final_cal);
                    $("#hidden_grand_total").val(final_cal)
                    $('#hidden_discount_id').val(discount.id);
                    $('#hidden_discount_amount').val(value_remain);
                }

            }

            if (discount.applicable_to == 0) {

                if (discount.type == 0) {
                    try{
                        var select_tip_amount = $("#select_tip_amount").val();
                        var hidden_sub_total = $("#hidden_sub_total").val().replace(",", "");
                        hidden_sub_total = parseFloat(hidden_sub_total);

                        if (discount.minimum_order == 1) {

                            if (hidden_sub_total < parseFloat(discount.minimum_order_amount)) {

                                $("input#apply_discount").val("");
                                ha.common.setCookie("discount_code","",-1);
                                //alert("Minimum amount for coupon is $" + discount.minimum_order_amount);
                                alert("Sorry, this discount code can only be used for orders of $" + discount.minimum_order_amount + " or more.");
                                return false;

                            }

                        }
                        if (discount.maximum_order == 1) {

                        if (hidden_sub_total > parseFloat(discount.maximum_order_amount)) {

                            $("input#apply_discount").val("");
                            ha.common.setCookie("discount_code","",-1);
                            alert("Sorry, this discount code can only be used for orders up to $" + discount.maximum_order_amount+ ".") ;
                            return false;

                        }

                    }

                        var per = parseFloat(discount.discount_amount) / 100;

                        var amount = hidden_sub_total * per;
                        var value_remain = (hidden_sub_total - amount).toFixed(2);
                        
                        
                        amount = amount.toFixed(2);
                        $(".coupon_result").find('.sub-total-discount').text(amount);
                        $(".coupon_result").show();

                        var final_cal = parseFloat(value_remain) + parseFloat(select_tip_amount);
                        
                        final_cal = parseFloat(final_cal) + parseFloat(tax);
                        
                        final_cal = final_cal.toFixed(2);
                        $(".sub-total").find('.grand-total').text(final_cal);
                        
                        $("#hidden_grand_total").val(final_cal);

                        $('#hidden_discount_id').val(discount.id);
                        $('#hidden_discount_amount').val(amount);
                    }
                    catch( e ){} 

                } else {
                    var select_tip_amount = $("#select_tip_amount").val();
                    var hidden_sub_total = $("#hidden_sub_total").val().replace(",", "");

                    hidden_sub_total = parseFloat(hidden_sub_total);

                    if (discount.minimum_order == 1) {

                        if (hidden_sub_total < parseFloat(discount.minimum_order_amount)) {

                            $("input#apply_discount").val("");
                            ha.common.setCookie("discount_code","",-1);
                            //alert("Minimum amount for coupon is $" + discount.minimum_order_amount);
                            alert(" Sorry, this discount code can only be used for orders of $" + discount.minimum_order_amount + " or more.");
                            return false;

                        }

                    }
                    if (discount.maximum_order == 1) {
                        if (hidden_sub_total > parseFloat(discount.maximum_order_amount)) {

                            $("input#apply_discount").val("");
                            ha.common.setCookie("discount_code","",-1);
                            alert("Sorry, this discount code can only be used for orders up to $" + discount.maximum_order_amount+ ".") ;
                            return false;
                        }
                    }

                    var discount_amount = discount.discount_amount;
                    discount_amount = parseFloat(discount_amount).toFixed(2);

                    var amount = hidden_sub_total - discount.discount_amount;
                    var value_remain = (hidden_sub_total - amount).toFixed(2); 

                    $(".coupon_result").find('.sub-total-discount').text(discount_amount);
                    $(".coupon_result").show();

                    var final_cal = parseFloat(hidden_sub_total) + parseFloat(select_tip_amount) - discount.discount_amount;
                    final_cal = parseFloat(final_cal) + parseFloat(tax);
                    
                    final_cal = final_cal.toFixed(2);
                    $(".sub-total").find('.grand-total').text(final_cal);
                    
                    $("#hidden_grand_total").val(final_cal)

                    $('#hidden_discount_id').val(discount.id);
                    $('#hidden_discount_amount').val(discount_amount);
                }
            }
            return false;
        }
    });
}

$(window).load(function(){
    
    $discode = ha.common.getCookie("discount_code");
    if(window.location.href.indexOf('checkout') != -1 && $discode.length){
        setTimeout(function(){
         $("#apply_discount").val($discode);
         applyDiscount();
        }, 400);
        
        $(".close-button").click(function(e) {
            $("#apply_discount").val($discode);
            applyDiscount();
        });
    }
    
}); 