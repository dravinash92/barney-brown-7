
    (function() {
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

        Date.prototype.getMonthName = function() {
            return months[ this.getMonth() ];
        };
        Date.prototype.getDayName = function() {
            return days[ this.getDay() ];
        };
    })();


    console.trace = function(  ){};

    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };



    $(document).ready(function () {

    // Navigation menu 

    $('.navigation li.sandwich-menu, .menu-head').hover(function(){
        $(this).next('.rollover').show();
        $(this).css({"background-color":"#2f221b","color":"#dab37f"});
    })
    $('.navigation li.sandwich-menu').mouseleave(function(){
        $(this).find('.rollover').hide();
        $(this).css("background-color","transparent");
        $(this).find('.menu-head').css({"background-color":"transparent","color":"#342920"});
    })

    VALID_USER_PICKED_RECENTLY = 0
    LAST_TIME_PICK = '';
    USER_LAST_TIME = '';
    $.session.remove('deliveryFee');


    $(".datepicker").val(new Date().toLocaleDateString());
    
    $("select.delevry_time").html(getTimings());


    itemCountArrowEvent();


    if ($(".save-menu").length > 0) {
        $(".save-menu").click(function () {
            $(".popup-wrapper").hide();
            $("#added-to-menu").show();
        });
    }



    if ($(".change-address").length > 0) {
        $(".change-address").click(function () {
            $(".popup-wrapper").hide();
            $("#add-new-address").show();
        });
    }

    if ($(".all-friend").length > 0) {
        $(".all-friend").click(function () {
            $(".popup-wrapper").hide();
            $("#facebook-friends-list").show();
        });
    }


    if ($(".close-button").length > 0) {
        $(".close-button").click(function (e) {
            e.preventDefault();
            var formid = $(this).parent().parent().attr("id");
            $(".rerror_msg").empty();
            $(".error_msg").empty();
            $('#' + formid + ' input').not('input[type="hidden"]').removeClass("register");
            $('#' + formid + ' input').not('input[type="hidden"]').val("");
            $(".popup-wrapper").hide();
            $('body').css('overflow', 'auto');
        });
    }



    $('#warningOk').on('click', function (e) {
        e.preventDefault();


        $('#warningcheckout').hide();

        $('.error_msg').hide();
    });


    $('.scroll-bar').jScrollPane();


    function add() {
        if (jQuery(this).val() === '') {
            jQuery(this).val(jQuery(this).attr('placeholder')).addClass('placeholder');
        }
    }

    function remove() {
        if (jQuery(this).val() === jQuery(this).attr('placeholder')) {
            jQuery(this).val('').removeClass('placeholder');
        }
    }

    
    if (!('placeholder' in jQuery('<input>')[0])) {
     
        jQuery('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
        jQuery('form').submit(function () {
            jQuery(this).find('input[placeholder], textarea[placeholder]').each(remove);
        });
    }

    //Function contains code regarding if user not logged in.
    cardEditRemoveEvents();
    bindMyAccountEvents();
    removeCartItems();
    itemArrowEvent();

    checkout.checkDateVariables( );

    

});
    (function ($) {

        $.session = {
            _id: null,
            _cookieCache: undefined,
            _init: function () {
                if (!window.name) {
                    window.name = Math.random();
                }
                this._id = window.name;
                this._initCache();
                
            // See if we've changed protcols

            var matches = (new RegExp(this._generatePrefix() + "=([^;]+);")).exec(document.cookie);
            if (matches && document.location.protocol !== matches[1]) {
                this._clearSession();
                for (var key in this._cookieCache) {
                    try {
                        window.sessionStorage.setItem(key, this._cookieCache[key]);
                    } catch (e) {
                    }
                    ;
                }
            }

            document.cookie = this._generatePrefix() + "=" + document.location.protocol + ';path=/;expires=' + (new Date((new Date).getTime() + 120000)).toUTCString();

        },
        _generatePrefix: function () {
            return '__session:' + this._id + ':';
        },
        _initCache: function () {
            var cookies = document.cookie.split(';');
            this._cookieCache = {};
            for (var i in cookies) {
                var kv = cookies[i].split('=');
                if ((new RegExp(this._generatePrefix() + '.+')).test(kv[0]) && kv[1]) {
                    this._cookieCache[kv[0].split(':', 3)[2]] = kv[1];
                }
            }
        },
        _setFallback: function (key, value, onceOnly) {
            var cookie = this._generatePrefix() + key + "=" + value + "; path=/";
            if (onceOnly) {
                cookie += "; expires=" + (new Date(Date.now() + 120000)).toUTCString();
            }
            document.cookie = cookie;
            this._cookieCache[key] = value;
            return this;
        },
        _getFallback: function (key) {
            if (!this._cookieCache) {
                this._initCache();
            }
            return this._cookieCache[key];
        },
        _clearFallback: function () {
            for (var i in this._cookieCache) {
                document.cookie = this._generatePrefix() + i + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            }
            this._cookieCache = {};
        },
        _deleteFallback: function (key) {
            document.cookie = this._generatePrefix() + key + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            delete this._cookieCache[key];
        },
        get: function (key) {
            return window.sessionStorage.getItem(key) || this._getFallback(key);
        },
        set: function (key, value, onceOnly) {
            try {
                window.sessionStorage.setItem(key, value);
            } catch (e) {
            }
            this._setFallback(key, value, onceOnly || false);
            return this;
        },
        'delete': function (key) {
            return this.remove(key);
        },
        remove: function (key) {
            try {
                window.sessionStorage.removeItem(key);
            } catch (e) {
            }
            ;
            this._deleteFallback(key);
            return this;
        },
        _clearSession: function () {
            try {
                window.sessionStorage.clear();
            } catch (e) {
                for (var i in window.sessionStorage) {
                    window.sessionStorage.removeItem(i);
                }
            }
        },
        clear: function () {
            this._clearSession();
            this._clearFallback();
            return this;
        }

    };

    $.session._init();

})(jQuery);

equalheight = function(container){
    var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = new Array(),
    $el,
    topPosition = 0;
    $(container).each(function() {

     $el = $(this);
     $($el).height('auto')
     topPostion = $el.position().top;

     if (currentRowStart != topPostion) {
       for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
         rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
 } else {
   rowDivs.push($el);
   currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
}
for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
   rowDivs[currentDiv].height(currentTallest);
}
});
}



function getTimings(selectedDate) {
    if (selectedDate)
        var date_selected = new Date(selectedDate);
    else
        var date_selected = new Date();

    var date_today = new Date();

    if (date_today.getDate() == date_selected.getDate()) {
        var cur_hour = new Date().getHours();
        var cur_min = new Date().getMinutes();
        var options = "";

        for (temp_hr = cur_hour; temp_hr <= 21; temp_hr++) {

            var h_in = temp_hr;
            var am_pm = " AM";
            if (h_in > 12) {
                h_in = temp_hr - 12;
                am_pm = " PM";
            }

            if (cur_min + 15 > 59)
                cur_min = (cur_min + 15) - 59;

            if (cur_min > 0 && cur_min <= 15) {
                cur_min = 30;
            } else if (cur_min > 15 && cur_min <= 30) {
                cur_min = 45;
            } else if (cur_min >= 45 && cur_min <= 59)
            cur_min = 0;

            for (temp_min = cur_min; temp_min <= 59; temp_min = temp_min + 15) {
                options = options + "<option value='" + temp_hr + ":" + temp_min + "'>" + h_in + ":" + temp_min + am_pm + "</option>";
            }

        }
    } else if (date_today.getDate() < date_selected.getDate()) {

        var options = "";
        var open_time = 10;
        for (temp_hr = open_time; temp_hr <= 21; temp_hr++) {

            var h_in = temp_hr;
            var am_pm = " AM";
            if (h_in > 12) {
                h_in = temp_hr - 12;
                am_pm = " PM";
            }

            for (temp_min = 15; temp_min <= 59; temp_min = temp_min + 15) {
                options = options + "<option value='" + temp_hr + ":" + temp_min + "'>" + h_in + ":" + temp_min + am_pm + "</option>";
            }
        }
    }

    return options;
}

function itemArrowEvent() {

    $(".right, .right-hover").unbind('click');
    $(document).on("click", ".right, .right-hover", function () {

        if (window.location.href.indexOf("createsandwich") != -1 || $('#sandwiches_quick_edit').is(":visible")) {
            return;
        }

        var sandwich_id = $(this).parent("div").find(".sandwich_id").val();
        var order_item_id = $(this).parent("div").find(".order_item_id").val();

        var count = $(this).prev(".text-box").val();
        count = +count + +01;
        var counts = count;
        var counts = counts.toString().length;

        if (count > 1) {
            $(this).prev().prev().removeClass("left");
            $(this).prev().prev().addClass("left-hover");
        }
        if (count < 1000) {
            if (counts == 1) {
                count = "0" + count;
            }
            selspin = $(this).parent("div").find('input[name="spinnerinput"]');
            $(selspin).val(count);

            if (count > 998) {
                $(this).removeClass("right");
                $(this).addClass("right-hover");
            }
            if ($(this).hasClass("noaction"))
                return false;

            updateCartContent(this, sandwich_id, count, order_item_id, "right");
            ha.common.checkForAllConditionsPlaceOrderButton();
        }
        checkZipcodeSubtotal();
        ha.common.checkForAllConditionsPlaceOrderButton();
        ha.common.checkForAllConditionsPlaceOrderButton();
        ha.common.check_validations_state(  );
    });

    $(".left, .left-hover").unbind('click');
    $(document).on("click", ".left, .left-hover", function () {

        if (window.location.href.indexOf("createsandwich") != -1 || $('#sandwiches_quick_edit').is(":visible")) {
            return;
        }

        var sandwich_id = $(this).parent("div").find(".sandwich_id").val();

        var order_item_id = $(this).parent("div").find(".order_item_id").val();

        var count = $(this).next().val();
        count = +count - +01;
        var counts = count;
        var counts = counts.toString().length;
        if (count < 2) {
            $(this).removeClass("left-hover");
            $(this).addClass("left");
        } else if (count == 998) {
            $(this).next().next().removeClass("right-hover");
            $(this).next().next().addClass("right");
        }
        if (count < 1) {
        } else {
            if (counts == 1) {
                count = "0" + count;
            }
            
            selspin = $(this).parent("div").find('input[name="spinnerinput"]');
            $(this).next().val(count);
            if ($(this).hasClass("noaction"))
                return false;
            updateCartContent(this, sandwich_id, count, order_item_id, "left");
            ha.common.checkForAllConditionsPlaceOrderButton();
        }
        checkZipcodeSubtotal();
        
        ha.common.checkForAllConditionsPlaceOrderButton();
        
        ha.common.check_validations_state(  );
        
    });
}

// The polling function
function poll(fn, timeout, interval) {
    var endTime = Number(new Date()) + (timeout || 2000);
    interval = interval || 100;

    var checkCondition = function(resolve, reject) {
        var result = fn();
        if(result) {
            resolve(result);
        }
        else if (Number(new Date()) < endTime) {
            setTimeout(checkCondition, interval, resolve, reject);
        }
        else {
            reject(new Error('timed out for ' + fn + ': ' + arguments));
        }
    };

    return new Promise(checkCondition);
}


function checkZipNeeds100(  )
{
   check_this_zip( zip );
   zip_code_return =  ha.common.getZipStatus_return(  );
   console.log( zip_code_return );


   var zip = $('input[name="ziptransfer"]').val();
   if(zip == null || zip == undefined) {
    var form = $("a.save_address").parent("li").parent("ul").parent("form.add_address");
    zip = $(form).find("input[name=zip]").val();
}

function check_this_zip( zip )
{
 var ret_val = -1;

 $.ajax({
    type: "POST",
    async:false,
    data: {
        "zip": zip
    },
    url: SITE_URL + 'checkout/checkZip',
    beforeSend: function() {
        $("#ajax-loader").show();
    },
    complete: function() {
        $("#ajax-loader").hide();
    },
    success: function (e) {
        if (e == 100) {
            ha.common.setZipStatus( "1" );
        }
        else if( e == 0 || e == 10 )
        {
            ha.common.setZipStatus( "0" );
        }
        else
        {
                ha.common.setZipStatus( "-1" );  // FAIL.
            }
        } //success
    });
 
}
}

function checkZipcodeSubtotal()
{
    var zip = $('input[name="ziptransfer"]').val();


    $.ajax({
        type: "POST",
        data: {
            "zip": zip
        },
        url: SITE_URL + 'checkout/checkZip',
        success: function (e) {
            e = JSON.parse(e);
            if (e.min_order == 100) {
                var subtotal = $("input[name=hidden_sub_total]").val();
                if (subtotal < 100)
                {
                    var remain = parseFloat(100.00 - subtotal).toFixed(2);
                    ha.common.setNotEnough( 1 );
                    var msg = '$100.00 SUBTOTAL MINIMUM. $' + remain + '  TO GO!';
                    if ($('#messageBar').find('li').length > 0) {
                        $('.less-than-10').html(msg);
                    } else {
                        d = document.createElement('li');
                        $(d).addClass('less-than-10').html(msg).appendTo($("#messageBar"));
                    }
                }
                else
                {
                    ha.common.setNotEnough( 0 );
                }

            }
            else
            {
                ha.common.setNotEnough( 0 );
            }

        }
    });

    if($.session.get("deliveryFee"))
    {
        
        var delivery_fee_amount = $.session.get("deliveryFee");

        $('.sub-total-wrapper ul.delivery_fee_in_total').remove();
        $('.sub-total-wrapper ul:nth-child(2)').after('<ul class="sub-total small-font delivery_fee_in_total"><li class="sub-total-list1"><p></p></li><li class="sub-total-list2"><h3 class="small-font">DELIVERY</h3><input type="hidden" name="hidden_delivery_fee" id="hidden_delivery_fee" value="'+ delivery_fee_amount +'"><h4>$<span class="sub-total">'+ delivery_fee_amount +'</span></h4></li><li class="sub-total-list3">&nbsp;</li></ul>');
        
        var grand_tot = $("input[name=hidden_grand_total]").val();
        var tot_with_delivery_fee = parseFloat(parseFloat(grand_tot) + parseFloat(delivery_fee_amount)).toFixed(2);
        
        $("input[name=hidden_grand_total]").val(tot_with_delivery_fee);
        $("h4 span.grand-total").text(tot_with_delivery_fee);
    }
    else
    {
        var delivery_fee_amount = 0;
    }
}
function itemCountArrowEvent() {

    $("input.qty").each(function () {
        if ($(this).val() > 1) {
            $(this).prev().removeClass("left");
            $(this).prev().addClass("left-hover");
        }
    });


    $("input.qty").keydown(function (e) {
        e = (e) ? e : window.event;
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            e.preventDefault();
            return false;
        }
    });

    $("input.qty").keyup(function (e) {

        session = ha.common.check_session_state();
        if (session.session_state == false) {
            if (session.facebook_only_user == false) {
                ha.common.login_popup($(this));
            } else {
                ha.common.facebook_login($(this));
            }
            return;
        }

        var count = $(this).val();
        var sandwich_id = $(this).parent("div").find(".sandwich_id").val();
        var order_item_id = $(this).parent("div").find(".order_item_id").val();
        if (e.keyCode == 13) {
            updateCartContent(this, sandwich_id, count, order_item_id);
        }

    });


}

function removeCartItems() {

    $(document).on("click", "a.remove_cart_item", function () {
        var obj = this;
        var order_item_id = $(this).attr("data-target");
        var item_id = $(this).attr("data-item");
        var parent_ul = $(this).parent().parent();
        var qty_input_box = $(this).parent().parent().find(".qty")
        var left_arrow = $(this).parent().parent().find(".left");



        $.ajax({
            type: "POST",
            data: {
                "order_item_id": order_item_id,
                'itemid': item_id
            },
            url: SITE_URL + 'sandwich/remove_cart_item',
            beforeSend: function () {
                
            },
            complete: function () {
             
            },
            success: function (e) {
                $("a.remove_cart_item").unbind("click");
                $(parent_ul).slideUp("slow").remove();

                if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
                    ha.common.updatePickupOrDeliveryInCart(1);
                } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
                    ha.common.updatePickupOrDeliveryInCart(0);
                } else {
                    ha.common.updatePickupOrDeliveryInCart(1);
                }

                updateCartQuantityBox();
                checkZipcodeSubtotal();
            }
        });


        $('.checkout-msg-red').remove();
        checkout.check_time_and_change_if_needed(  );
    });

}

function updateTip(obj) {

    if($.session.get("deliveryFee"))
    {
        //alert($.session.get("deliveryFee"));
        var delivery_fee_amount = $.session.get("deliveryFee");
    }
    else
    {
        var delivery_fee_amount = 0;
    }

    var discount = 0;
    var subtotal = $("#hidden_sub_total").val();
    var tax = $("#hidden_tax").val();
    var tip = $(obj).val();

    if ($("#hidden_discount_amount").val()) {
        discount = $("#hidden_discount_amount").val();
    }

    $("#hidden_tip").val(tip);
    $.session.set("hidd_tip", tip);
    var net_tot = parseFloat(tip) + parseFloat(subtotal) + parseFloat(tax) + parseFloat(delivery_fee_amount);
    $(".grand-total").html(net_tot.toFixed(2));
    $("#hidden_grand_total").html(net_tot.toFixed(2));
    //
    $("#hidden_grand_total").val(net_tot.toFixed(2));
}


function updateCartContent(obj, sandwich_id, count, order_item_id, whichArrow) {
    var qty = count;
    var data_id = sandwich_id;
    var data_type = $(obj).parent().find("input[name=data_type]").val();
    var last_tip = $("#select_tip_amount").val();
    var delvry = -1;

    if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
        delvry = 1;
    } else if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
        delvry = 1;
    }

    var data = {
        "qty": qty,
        "data_id": data_id,
        "data_type": data_type,
        "order_item_id": order_item_id,
        "deliveryOrPickup": delvry,
        "last_tip": last_tip,
        "whichArrow": whichArrow
    };

    $.ajax({
        type: "POST",
        data: data,
        url: SITE_URL + 'checkout/updateCartItems/',
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        },
        success: function (e) {

            if (!e)
                return;
            var data = JSON.parse(e);

            if (data.status) {
                $(".check-out-left").html(data.message);
                window.ha.common.popup_info_checkout();

                updateCartQuantityBox();

                itemCountArrowEvent();

                ha.common.checkForAllConditionsPlaceOrderButton();

            } else {

                alert("Session has been expired. login again.");
                window.location = SITE_URL;
            }
        }
    });


}

function updateCartQuantityBox() {

    $.ajax({
        type: "POST",
        data: {},
        url: SITE_URL + 'checkout/getCartItemQtyCount/',
        async: false,
        success: function (e) {
            $("a.shopping-cart span").text(e);
            var subtotal = $("#hidden_sub_total").val();
        }
    });


    setTimeout(function () {

        if (ha.sandwichcreator.vars.appliedDiscount) {
            $("#apply_discount").val(ha.sandwichcreator.vars.appliedDiscount);
            applyDiscount();
        }
    }, 300);
    checkZipcodeSubtotal();
}

function changeUserpage(pagename) {

    $(".my-account-menu>ul>li>a.active").removeClass("active");
    $("." + pagename).addClass('active');
    
    $.ajax({
        type: "POST",
        url: SITE_URL + "myaccount/" + pagename,
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        }
    })
    .done(function (msg) {
        
        if (pagename == 'profile') {
            $(".menu-list-wrapper").html(msg);
            ha.common.add_edit_user_review();
        } else {
            
            if(  msg )
            {
                $(".accout-info-wrapper").html(msg);
                ha.common.add_edit_user_review();
            }

        }
        bindMyAccountEvents();
        cardEditRemoveEvents();
        
        if (pagename == 'savedaddress') {
            equalheight('.saved-address-inner li');
        }
        
    });

    bindMyAccountEvents();
    cardEditRemoveEvents();
}


function bindMyAccountEvents() {
    $(".change-address").on('click', function () {
        $(".popup-wrapper").hide();
        $("#add-new-address").show();
    });
    $(".add-credit-card").on('click', function () {
        $(".popup-wrapper").hide();
        $("#credit-card-details").show();
    });
    $("a.save_address").unbind("click");
    $("a.save_address").click(function () {
        var form = $(this).parent("li").parent("ul").parent("form.add_address");
        var recipient = $(form).find("input[name=recipient]").val();
        var company = $(form).find("input[name=company]").val();
        var address1 = $(form).find("input[name=address1]").val();
        var address2 = $(form).find("input[name=address2]").val();
        var zip = $(form).find("input[name=zip]").val();
        var phone1 = $(form).find("input[name=phone1]").val();
        var phone2 = $(form).find("input[name=phone2]").val();
        var phone3 = $(form).find("input[name=phone3]").val();
        var crossStrt = $(form).find("input[name=cross_streets]").val();


        if (!recipient || recipient.length < 1) {
            alert("Recipient name can't be empty.");
            return false;
        }

        if (!crossStrt || crossStrt.length < 1) {
            alert("Cross Streets can't be empty.");
            return false;
        }

        if (!phone1 || phone1.length < 1 || phone1.length > 3 || !phone2 || phone2.length < 1 || phone2.length > 3 || !phone3 || phone3.length < 1 || phone3.length > 4) {
            alert("Phone number should be valid.");
            return false;
        }

        if (!zip) {
            alert("Zip Code required! ");
            return false;
        } else {
            if ($.inArray(zip, ha.common.zipcodes) == -1) {
                alert("Sorry, delivery is not available in this area! Barney Brown currently delivers in Manhattan from Battery Park to 100th St.");
                return false;
            }
        }



        var data = form.serialize();



        $.ajax({
            type: "POST",
            data: data,
            url: SITE_URL + 'myaccount/savingAddress',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                $.session.set("address_id",e);
                $(form).find("input[type=text], textarea").val("");
                $(".popup-wrapper").hide();
                $.ajax({
                    type: "POST",
                    data: {
                        "zip": zip
                    },
                    url: SITE_URL + 'checkout/checkZip',
                    success: function (e) {
                        
                        e = JSON.parse(e);
                        if (e.min_order == 100) {

                            var subtotal = $("input[name=hidden_sub_total]").val();
                            $('#warningcheckout').show();
                            if (subtotal < 100)
                            {
                             
                                var remain = parseFloat(100.00 - subtotal).toFixed(2);

                                var msg = '$100.00 SUBTOTAL MINIMUM. $' + remain + '  TO GO!';
                                if ($('#messageBar').find('li').length > 0) {
                                    $('.less-than-10').html(msg);
                                } else {
                                    d = document.createElement('li');
                                    $(d).addClass('less-than-10').html(msg).appendTo($("#messageBar"))
                                }
                            }
                        }
                        if(e.delivery_fee != ''){

                            $.session.remove('deliveryFee');
                            $.session.set("deliveryFee",e.delivery_fee);

                            $('.sub-total-wrapper ul.delivery_fee_in_total').remove();
                            $('.sub-total-wrapper ul:nth-child(2)').after('<ul class="sub-total small-font delivery_fee_in_total"><li class="sub-total-list1"><p></p></li><li class="sub-total-list2"><h3 class="small-font">DELIVERY</h3><input type="hidden" name="hidden_delivery_fee" id="hidden_delivery_fee" value="'+ e.delivery_fee +'"><h4>$<span class="sub-total">'+ e.delivery_fee +'</span></h4></li><li class="sub-total-list3">&nbsp;</li></ul>');
                            
                            var grand_tot = $("input[name=hidden_grand_total]").val();
                            var tot_with_delivery_fee = parseFloat(parseFloat(grand_tot) + parseFloat(e.delivery_fee)).toFixed(2);
                            
                            $("input[name=hidden_grand_total]").val(tot_with_delivery_fee);
                            $("h4 span.grand-total").text(tot_with_delivery_fee);
                        }
                    }
                });
                
                $(form).find("input[name=recipient]").val(recipient);
                changeUserpage('savedaddress');
                ha.common.check_current_condition_for_time();
                checkout.check_time_and_change_if_needed(  );
                ha.common.check_validations_state();
                ha.common.checkForAllConditionsPlaceOrderButton();
            }
        });
    });

    $("a.remove_address").unbind("click");
    $("a.remove_address").click(function () {
        var address_id = $(this).attr("data-target");

        $.ajax({
            type: "POST",
            data: {
                "address_id": address_id
            },
            url: SITE_URL + 'myaccount/removeAddress',
            success: function (e) {
                changeUserpage('savedaddress');
            }
        });

    });

    $("a.edit_address").unbind("click");
    $("a.edit_address").click(function () {
        var address_id = $(this).attr("data-target");

        $.ajax({
            type: "POST",
            data: {
                "address_id": address_id
            },
            url: SITE_URL + 'myaccount/editAddress',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                $("#edit-address div.add-new-address-inner").html(e);
                $("#edit-address").show();
                $("#edit-address div.add-new-address-inner .close-button").click(function () {
                    $("#edit-address").hide();
                });
                bindMyAccountEvents();
            }
        });

    });


    $("a.cancelIt").unbind('click').click(function () {

        $("div.account-edit-inner").slideUp(400, function () {
            $("div.account-show-inner").slideDown(400);
        });
    });
    $("a.edit_mydetails").unbind("click");
    $("a.edit_mydetails").click(function () {
        $("div.account-show-inner").slideUp(400, function () {
            $("div.account-edit-inner").slideDown(400);
        });
    });

    $("a.save_mydetails").unbind("click");
    $("a.save_mydetails").click(function () {
        var form = $("form#myaccount_detail_form");
        var userdata = form.serialize();

        $.ajax({
            type: "POST",
            data: userdata,
            url: SITE_URL + 'myaccount/userNameExist/',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == "true") {
                    return false;
                }

                $.ajax({
                    type: "POST",
                    data: userdata,
                    url: SITE_URL + 'myaccount/savingCustomerInfo/',
                    beforeSend: function () {
                        $("#ajax-loader").show();
                    },
                    complete: function () {
                        $("#ajax-loader").hide();
                    },
                    success: function (data) {
                        window.location.reload();
                    }
                });
            }
        });
    });

    
    ha.common.myaccount_reorder();
}

function resetCartList(checkoutoption) {

    if (checkoutoption == "pickupstorelocation") {
        checkoutoptionOption = 0;
    } else {
        checkoutoptionOption = 1;
    }

    $.ajax({
        type: "POST",
        url: SITE_URL + "checkout/" + checkoutoption,
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        }

    })
    .done(function (msg) {
        $(".checkoutdynamicdiv").html(msg);
        $(".change-address").on('click', function () {
            $(".popup-wrapper").hide();
            $("#add-new-address").show();
        });

        bindCheckoutPageEvents();
        ha.common.check_validations_state();
        ha.common.updatePickupOrDeliveryInCart(checkoutoptionOption);
        ha.common.popup_info_checkout();

    });

}

function changeCheckoutoption(checkoutoption) {

    if (checkoutoption == "pickupstorelocation") {
        checkoutoptionOption = 0;
    } else {
        checkoutoptionOption = 1;
    }

    $.ajax({
        type: "POST",
        url: SITE_URL + "checkout/" + checkoutoption,
        beforeSend: function () {
            $("#ajax-loader").show();
        },
        complete: function () {
            $("#ajax-loader").hide();
        }

    })
    .done(function (msg) {
        $(".checkoutdynamicdiv").html(msg);
        $(".change-address").on('click', function () {
            $(".popup-wrapper").hide();
            $("#add-new-address").show();
        });

        bindCheckoutPageEvents();
        ha.common.check_validations_state();
        ha.common.updatePickupOrDeliveryInCart(checkoutoptionOption);
        ha.common.popup_info_checkout();

        if (checkoutoption == "deliverychooseaddress")
            ha.common.checkForUserAdressesAvailable();
    });

    setTimeout(function () {
        applyDiscount();
    }, 300);


}



function hideDate( hideValue, source ) 
{

    if ( source == null ) {
        source = 'User';
    }
    
    if( source == 'User')
    {
        ha.common.setDateChanged( 0 );
        ha.common.setUserLastPick( 1 );
    }
    else
    {
        ha.common.setUserLastPick( 0 );
    }

    off_msg = $('#radio1').data('err-msg' );

    if( off_msg != "" && off_msg != 'undefined' &&  off_msg != undefined )
    {
        $('.checkout-msg-red').remove();
        off_msg = "<span class='checkout-msg-red'>" + off_msg + "</span>";
        $("div.delivery-details-wrapper div.date-time > p").after(off_msg);
        hideValue = false;
    }
    else
    {
        $('.checkout-msg-red').remove();                                
    }
    

    function showNow(  )
    {

        $(".date-time-text").hide();
        $("#radio1").prop( "disabled", false );
        $("#radio1").prop( "checked", true );
        $("#radio2").prop( "checked", false );
        $(".date-time-text").hide();
        ha.common.setTimepicked( 0 );

        if( source == 'User'){
            ha.common.clearUserTimeTemp(  );
            ha.common.clearUserTimeValidated(  );
        }
    } 
    

    function showDatePicker(  )
    {
        console.log( "UNHIDE" );
        if( source == 'User'){
            ha.common.setSpecificDateTimeFromElements(  );
            ha.common.setTimepicked( 1 );
        }
        
        $("#radio1").prop( "checked", false );
        $("#radio2").prop( "checked", true );
        $(".date-time-text").show();
    }
    
    if( ha.common.getNowDisabled(  ) == 1 )
    {
        showDatePicker(  );
        checkout.set_checkout_message(  $("#radio2").data( 'err-msg' ) );
        return;
    }

    if ( hideValue == true ) {
        ha.common.clearUserTimeTemp(  );
        ha.common.clearUserTimeValidated(  );
        showNow(  );

    } else {
        showDatePicker(  );
         checkout.check_time_and_change_if_needed( 1 ); 
    }
}



function bindCheckoutPageEvents() 
{
    $("select.select_address").unbind("change");
    $("select.select_address").on("change", function () 
    {
        var add_id = $(this).val();
        if (add_id > 0) 
        {
            $.ajax({
                type: "POST",
                data: {
                    "address_id": add_id
                },
                url: SITE_URL + 'checkout/deliveryto',
                beforeSend: function () {
                    $("#ajax-loader").show();
                },
                complete: function () {
                    $("#ajax-loader").hide();
                },
                success: function (e) 
                {
                    $(".checkoutdynamicdiv").html(e);
                    bindCheckoutPageEvents();
                    $.session.set("address_id",add_id);
                    zipdata = $(e);
                    zipdata = zipdata[zipdata.length - 1];
                    zip = $(zipdata).val();

                    $.ajax({
                        type: "POST",
                        data: {
                            "zip": zip
                        },
                        url: SITE_URL + 'checkout/checkZip',
                        beforeSend: function() {
                            $("#ajax-loader").show();
                        },
                        complete: function() {
                            $("#ajax-loader").hide();
                        },
                        success: function (e) 
                        {
                            //console.log(e);
                            e = JSON.parse(e);
                            if (e.min_order == 100) 
                            {
                                var subtotal = $("input[name=hidden_sub_total]").val();
                                if (subtotal < 100)
                                {
                                    //$('#warningcheckout').show();

                                    var remain = parseFloat(100.00 - subtotal).toFixed(2);
                                    ha.common.setNotEnough ( 1 );
                                    var msg = '$100.00 SUBTOTAL MINIMUM. $' + remain + '  TO GO!';
                                    if ($('#messageBar').find('li').length > 0) {
                                        $('.less-than-10').html(msg);
                                    } else {
                                        d = document.createElement('li');
                                        $(d).addClass('less-than-10').html(msg).appendTo($("#messageBar"))
                                    }
                                }
                                else
                                    {ha.common.setNotEnough ( 0 );}
                            }

                            if(e.delivery_fee != '')
                            {

                                $.session.remove('deliveryFee');

                                $.session.set("deliveryFee",e.delivery_fee);

                                $('.sub-total-wrapper ul.delivery_fee_in_total').remove();
                                $('.sub-total-wrapper ul:nth-child(2)').after('<ul class="sub-total small-font delivery_fee_in_total"><li class="sub-total-list1"><p></p></li><li class="sub-total-list2"><h3 class="small-font">DELIVERY</h3><input type="hidden" name="hidden_delivery_fee" id="hidden_delivery_fee" value="'+ e.delivery_fee +'"><h4>$<span class="sub-total">'+ e.delivery_fee +'</span></h4></li><li class="sub-total-list3">&nbsp;</li></ul>');
                                
                                var grand_tot = $("input[name=hidden_grand_total]").val();
                                var tot_with_delivery_fee = parseFloat(parseFloat(grand_tot) + parseFloat(e.delivery_fee)).toFixed(2);
                                
                                $("input[name=hidden_grand_total]").val(tot_with_delivery_fee);
                                $("h4 span.grand-total").text(tot_with_delivery_fee);
                            }

                            //to refresh timings after checkking with zipcode of the user's address
                             checkout.check_time_and_change_if_needed( );
                            checkout.checkTimingsRealtedConditions( );
                        }

                    });

                    storeid = ha.common.getStore_id_using_zip(zip);
                    $('input[name="delivery_store_id"]').val(storeid);
                    setInterval( ha.common.check_validations_state(), 300 );

                }
            });
        }
        else if (add_id == "0") 
        {
            $("#add-new-address").show();
            DO_NOT_MESS_WITH_THE_TIME_BUTTON = true;
            var name = $('input[name="recipientName"]').val();
            $('input[name="recipient"]').val(name);
            setInterval( ha.common.check_validations_state(), 300 );
        }
        ha.common.updatePickupOrDeliveryInCart(1);
    });

    $("a.edit_address").unbind("click");
    $("a.edit_address").click(function () 
    {
        var address_id = $(this).attr("data-target");

        $.ajax({
            type: "POST",
            data: {
                "address_id": address_id
            },
            url: SITE_URL + 'myaccount/editAddress',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                $("#edit-address div.add-new-address-inner").html(e);
                $("#edit-address").show();
                $("#edit-address div.add-new-address-inner .close-button").click(function () {
                    $("#edit-address").hide();
                });
                bindCheckoutPageEvents();
            }
        });

    });

    $("a.save_address").unbind("click");
    $("a.save_address").click(function (e)
    {
        
        DO_NOT_MESS_WITH_THE_TIME_BUTTON = true;
        var form = $(this).parent("li").parent("ul").parent("form.add_address");

        var recipient = $(form).find("input[name=recipient]").val();
        var company = $(form).find("input[name=company]").val();
        var address1 = $(form).find("input[name=address1]").val();
        var address2 = $(form).find("input[name=address2]").val();
        var zip = $(form).find("input[name=zip]").val();
        var phone1 = $(form).find("input[name=phone1]").val();
        var phone2 = $(form).find("input[name=phone2]").val();
        var phone3 = $(form).find("input[name=phone3]").val();
        var crossStrt = $(form).find("input[name=cross_streets]").val();

        if (!recipient || recipient.length < 1) {
            alert("Recipient name can't be empty.");
            return false;
        }

        if (!crossStrt || crossStrt.length < 1) {
            alert("Cross Streets can't be empty.");
            return false;
        }

        if (!phone1 || phone1.length < 1 || phone1.length > 3 || !phone2 || phone2.length < 1 || phone2.length > 3 || !phone3 || phone3.length < 1 || phone3.length > 4) {
            alert("Phone number should be valid.");
            return false;
        }

        if (!zip) {
            alert("Zip Code required! ");
            return false;
        } else {
            if ($.inArray(zip, ha.common.zipcodes) == -1) {
                alert("Sorry, delivery is not available in this area! Barney Brown currently delivers in Manhattan from Battery Park to 100th St. ");
                return false;
            }
        }

        
        checkZipNeeds100(  );
        
        var data = form.serialize();

        $.ajax({
            type: "POST",
            data: data,
            url: SITE_URL + 'myaccount/savingAddress',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) 
            {
                $.session.set("address_id",e);
                $(form).find("input[type=text], textarea").val("");
                $(".popup-wrapper").hide();
                $.ajax({
                    type: "POST",
                    data: {
                        "zip": zip
                    },
                    url: SITE_URL + 'checkout/checkZip',
                    beforeSend: function() {
                        $("#ajax-loader").show();
                    },
                    complete: function() {
                        $("#ajax-loader").hide();
                    },
                    success: function (e) 
                    {
                        //console.log(e);
                        e = JSON.parse(e);
                        if (e.min_order == 100) 
                        {
                            var subtotal = $("input[name=hidden_sub_total]").val();
                            $('#warningcheckout').show();
                            if (subtotal < 100)
                            {
                                var subtotal = $("input[name=hidden_sub_total]").val();
                                var remain = parseFloat(100.00 - subtotal).toFixed(2);

                                var msg = '$100.00 SUBTOTAL MINIMUM. $' + remain + '  TO GO!';
                                if ($('#messageBar').find('li').length > 0) {
                                    $('.less-than-10').html(msg);
                                } else {
                                    d = document.createElement('li');
                                    $(d).addClass('less-than-10').html(msg).appendTo($("#messageBar"))
                                }
                            }
                        }
                        if(e.delivery_fee != '')
                        {
                            $.session.remove('deliveryFee');

                            $.session.set("deliveryFee",e.delivery_fee);

                            $('.sub-total-wrapper ul.delivery_fee_in_total').remove();
                            $('.sub-total-wrapper ul:nth-child(2)').after('<ul class="sub-total small-font delivery_fee_in_total"><li class="sub-total-list1"><p></p></li><li class="sub-total-list2"><h3 class="small-font">DELIVERY</h3><input type="hidden" name="hidden_delivery_fee" id="hidden_delivery_fee" value="'+ e.delivery_fee +'"><h4>$<span class="sub-total">'+ e.delivery_fee +'</span></h4></li><li class="sub-total-list3">&nbsp;</li></ul>');
                            
                            var grand_tot = $("input[name=hidden_grand_total]").val();
                            var tot_with_delivery_fee = parseFloat(parseFloat(grand_tot) + parseFloat(e.delivery_fee)).toFixed(2);
                            
                            $("input[name=hidden_grand_total]").val(tot_with_delivery_fee);
                            $("h4 span.grand-total").text(tot_with_delivery_fee);
                        }
                    }
                });
                if (e) 
                {
                    var addressid = e = parseInt(e.trim());
                    $.ajax({
                        type: "POST",
                        data: {
                            "address_id": e
                        },
                        url: SITE_URL + 'checkout/deliveryto',
                        beforeSend: function () {
                            $("#ajax-loader").show();
                        },
                        complete: function () {
                            $("#ajax-loader").hide();
                        },
                        success: function (e) {
                            $(".checkoutdynamicdiv").html(e);
                            bindCheckoutPageEvents();
                            storeid = ha.common.getStore_id_using_zip(zip)
                            $('input[name="delivery_store_id"]').val(storeid);
                            ha.common.check_validations_state();
                            $.session.set("address_id", addressid);
                        }
                    });
                } 
                else 
                {
                    changeCheckoutoption('deliverychooseaddress');
                }

                if(ha.common.getCookie("specific_date_exists") == 1)
                {
                    setTimeout(function(){
                        checkout.check_time_and_change_if_needed(  );
                    }, 1000);
                }
                else
                {
                    checkout.check_time_and_change_if_needed(  );
                }
                checkout.checkTimingsRealtedConditions( );
                ha.common.check_current_condition_for_time();
                ha.common.check_validations_state();
                ha.common.checkForAllConditionsPlaceOrderButton();
            }
        });
    });

}

function cardEditRemoveEvents() {

    $('a.remove_card').unbind('click');
    $('a.remove_card').click(function () {
        var value = $(this).attr("data-id");
        $.ajax({
            type: "POST",
            data: {
                'id': value
            },
            url: SITE_URL + 'myaccount/cardRemoveEvent/',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {
                changeUserpage('savedbilling');
            }
        });
    });


    $('a.add-credit-card, a.edit_card').unbind('click');
    $('a.add-credit-card, a.edit_card').click(function () {
        $('.error_msg').empty();
        var value = $(this).attr("data-id");

        if (value == 0) {

            $('#credit-card-edit-details h1').text('ADD CREDIT CARD');
            $('#credit-card-edit-details .add-address').text('Add');
            $('#credit-card-edit-details input[name="uid"]').val(SESSION_ID);
            $('#credit-card-edit-details').show();

        } else if (value > 0) {

            $.ajax({
                type: "POST",
                data: {
                    'id': value
                },
                dataType: 'json',
                url: SITE_URL + 'checkout/get_billing/',
                beforeSend: function () {
                    $("#ajax-loader").show();
                },
                complete: function () {
                    $("#ajax-loader").hide();
                },
                success: function (e) {
                    $.session.set("card_id", e.Data[0].id);
                    $('#credit-card-edit-details input[name="id"]').val(e.Data[0].id);
                    $('#credit-card-edit-details input[name="uid"]').val(SESSION_ID);
                    $('#credit-card-edit-details input[name="card_number"]').val(e.Data[0].card_number);
                    $('#credit-card-edit-details input[name="card_cvv"]').val('');
                    $('#credit-card-edit-details input[name="card_zip"]').val(e.Data[0].card_zip);
                    $('#credit-card-edit-details select[name="card_type"] option[value="' + e.Data[0].card_type + '"]').prop('selected', true);
                    $('#credit-card-edit-details select[name="expire_month"] option[value="' + e.Data[0].expire_month + '"]').prop('selected', true);
                    $('#credit-card-edit-details select[name="expire_year"] option:contains("' + e.Data[0].expire_year + '")').prop('selected', true);
                    $('#credit-card-edit-details input[name="address1"]').val(e.Data[0].address1);
                    $('#credit-card-edit-details input[name="street"]').val(e.Data[0].street);
                    $('#credit-card-edit-details .add-address').text('Save');

                    $('#credit-card-edit-details').show();

                }
            });

        }
        ha.common.check_current_condition_for_time();
    });

    $("#credit-card-edit-details a.save-card-detail").unbind("click");
    $("#credit-card-edit-details a.save-card-detail").click(function () {
        var form = $("form#edit-billForm");
        arryData = form.serializeArray();
        if ($(this).text() == "Add") {
            arryData.forEach(function (e) {
                if (e.name == "id" && e.value) {
                    form.find("input[name='id']").val('');
                }
            });
        }

        console.log( "Have added a credit card." );

        var card_data = form.serialize();
        var card_number = $('#credit-card-edit-details input[name="card_number"]').val();
        var card_cvv = $('#credit-card-edit-details input[name="card_cvv"]').val();
        var card_zip = $('#credit-card-edit-details input[name="card_zip"]').val();
        var card_type = $('#credit-card-edit-details select[name="card_type"]').val();
        var expire_month = $('#credit-card-edit-details select[name="expire_month"]').val();
        var expire_year = $('#credit-card-edit-details select[name="expire_year"]').val();
        var address = $('#credit-card-edit-details input[name="address1"]').val();
        var street = $('#credit-card-edit-details input[name="street"]').val();

        var deviceType = ha.sandwichcart.getOS();
        var browserType = ha.sandwichcart.getBrowserInfo();
        var card_number_log = card_number;
        var card_exp_month_log = expire_month;
        var card_exp_year_log = expire_year;

        card_data += '&uid='+SESSION_ID+'&deviceType='+deviceType+'&browserType='+browserType+'&card_number_log='+card_number_log+'&card_exp_month_log='+card_exp_month_log+'&card_exp_year_log='+card_exp_year_log;

        jsTime = ha.sandwichcart.getCurrentTimJs();
        currentMonth = parseInt(jsTime.getMonth()) + 1;
        currentYear = parseInt(jsTime.getFullYear());
        selectedMonth = parseInt(expire_month);
        selectedYear = parseInt(expire_year);

        if (selectedYear < currentYear)
        {
            alert("The expiration date is invalid");
            return false;
        } else if (selectedYear == currentYear && selectedMonth <= currentMonth) {

            alert("The expiration date is invalid");
            return false;
        }
        //console.log(card_data);return;
        $.ajax({
            type: "POST",
            data: card_data,
            dataType: 'json',
            url: SITE_URL + 'myaccount/cardAddEditEvent/',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
                ha.common.check_validations_state(  );
            },
            success: function (e) {
                console.log("sassss ",e.data);

                if( e.response == undefined || e.response.status_code != '200' )
                {
                   
                    $('.error_msg').text('Please check your credit card details...');
                    $("#ajax-loader").hide();
                    return false;
                }
                $.session.set("card_id", e.data);
                $('#credit-card-edit-details').hide();
                $( $('#credit-card-edit-details').find('form') )[0].reset();
                changeUserpage('savedbilling');
                
            }
        });

    });
}

jQuery(document).ready(function () {

    $("input.reset_password").click(function () {

        var form = $(this).parent("form");
        var password = $(form).find("[name=password]").val();
        var c_password = $(form).find("[name=c_password]").val();
        var data = form.serialize();
        if (!password) {
            alert("Please fill the new password."); 
            return false;
        }

        if (!c_password) {
            alert("Please fill the confirm new password.");
            return false;
        }


        if (password != c_password) {
            alert("Confirm password not matched.");
            return false;
        }


        $.ajax({
            type: "POST",
            data: data,
            url: SITE_URL + 'site/setPassword',
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            success: function (e) {

                json = JSON.parse(e);
                if (json) {

                    if (json.status == "success") {
                        alert("Password changed successfully!");
                        window.location = SITE_URL;
                    } else {
                        alert(json.message);
                    }

                } else {
                    alert("Changing password failed!");
                }
            }
        });

    });
    $(".sortby").change(function () {
        var sort_id = $(this).val();
        window.location.assign("" + SITE_URL + "sandwich/gallery/?sortid=" + sort_id + "");
    });

});

function tooLateToOrder(  )
{
    $('.checkout-msg-red').remove();
    ha.common.setClosed(1);
    var msg = "<span class='checkout-msg-red'>SORRY, WE&apos;RE CURRENTLY CLOSED. <br/> PLEASE SCHEDULE A DELIVERY TIME BELOW.</span>";
    $("div.delivery-details-wrapper div.date-time > p").after(msg);

    
    setTimeout(function(){
        checkout.check_time_and_change_if_needed(  );
    });
}


function on_the_purchase_page() {
    current_time = ha.common.getCurrentTimJs(  );
    jsTime = ha.sandwichcart.getCurrentTimJs();

    currentMonth = parseInt(jsTime.getMonth()) + 1;
    currentYear = parseInt(jsTime.getFullYear());

    if( validate_date_against_cc( currentMonth, currentYear ) == false )
    {
        show_card_expired(  );
        return;
    }


    selectedMonth = parseInt(expire_month);
    selectedYear = parseInt(expire_year);  
};

function validate_date_against_cc( $current_month, $current_year )
{

    var $expire_month = $('#credit-card-edit-details select[name="expire_month"]').val();
    var $expire_year = $('#credit-card-edit-details select[name="expire_year"]').val();
    
    if( $current_year < $expire_year )
        return true;
    else if( $current_year == $expire_year )
    {
        if( $current_month <= $expire_month )
            return true;
    }
    else
        return false;
}



function check_for_big_enough_bill(  )
{
    return;
    if( ha.common.getNotEnough(  ) == 1 ){
        console.warn( "Disabled - not enough cash" );
        $('.place-order').addClass('disabled');
        $('.place-order').removeClass('link');
    }
    else
    {
        $('.place-order').removeClass('disabled');
        $('.place-order').addClass('link');
    }
}

function set_datepicker_to_now(  )
{
    setManual( new Date(  ), new Date(  ) );
}

function check_if_should_skip(  )
{
    return 0;
    if( ha.common.getTimepicked(  ) == 1 )
        return 0;
    
    if( checkout.check_if_closed(  ) != 0 )
        closed = 0;
    if( ha.common.checkBreadSimple(  ) )

        return 0
    if( mode === last_mode && timeStateArray.user !== 'UserPick' )
    {

        return;
    }
}





function disableSpecificDates( date )
{
    if(  ha.common.specific_day  == "" )
    {
        return 1;
    }

    var bad_date = new Date(  ha.common.specific_day );


    var m_bad = bad_date.getMonth();
    var d_bad = bad_date.getDate();
    var y_bad = bad_date.getFullYear();
    

    var m = date.getMonth();
    var d = date.getDate();
    var y = date.getFullYear();
    
     // First convert the date in to the mm-dd-yyyy format 
     // Take note that we will increment the month count by 1 
     var currentdate = (m + 1) + '-' + d + '-' + y ;
     var bad_date_formatted =  (m_bad + 1) + '-' + d_bad + '-' + y_bad ;
      // We will now check if the date belongs to disableddates array 
      if( currentdate == bad_date_formatted  )
      {
        return 0;
    }
    else
    {
        return 1;
    }
}



function account_for_user_time_pick(  )
{

    is_user_pick_valid  = 0;
    if( ha.common.getTimepicked(  ) == 1 ) 
    {
        is_user_pick_valid = checkout.validate_user_pick( timeFloor );        
    }


    adduserpick =0;
    if(  is_user_pick_valid == 1 )
    {
        nowHidden = true;
        adduserpick = 'UserPick';
    }
    
    if( adduserpick == 'UserPick' )
        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":adduserpick, "userTime": set_date_time( ha.common.getSpecificDate(  ), ha.common.getSpecificTime(  ) ) };
    else
        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0 };
}




    function setManual( time, floor )
    {
        setDatePicker( new Date( ha.common.getSpecificDate( ) ),  ha.common.getSpecificTime( ) );
    }

    function setDatepickerAndTimePicker( timeState )
    {
        floorDate = timeState['timeFloor'];
        if( timeState == 'Closed' )
            overrideWithNoon = true;
        else
            overrideWithNoon = false;



        $(".delivery_date").datepicker("option",{ minDate: new Date(floorDate)});
        $(".delivery_date").datepicker('option', 'minDate', floorDate );
        $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', floorDate));
        
        if( overrideWithNoon == true )
            $("div.timechange select > option[value='12:00']").attr("selected", "selected");
        else
        {
            min_time = new Date( floorDate );
            hour = pad( min_time.getHours(), 2 );
            minute = pad( min_time.getMinutes(), 2 );
            $("div.timechange select > option[value='"  +hour +":" + minute + "']").attr("selected", "selected");
        }
    }

    function pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    

// Supplement to the ha_common date picker select event
function dmDaterPickerSelected(  )
{
    timestate = ha.common.getTimeStateArray( );

    currentSelected = new Date( $(".delivery_date").val( ) ); 
    currentSelected.setHours(0,0,0,0);
    
    if( !timestate.timeFloor )
        return;

    floorDate = new Date( timestate.timeFloor );

    floorDate.setHours(0);
    floorDate.setMinutes(0);
    floorDate.setSeconds(0);

    if( currentSelected <= floorDate )
        return -1;

    if( currentSelected == floorDate )
    {
        floorTime = new Date( timestate.floorDate );
        remove_from_time_select_before( floorTime );
    }

}

function remove_from_time_select_before( theFloor )
{
    minTime = theFloor.getTime(  );
    
}

// Set the buttons - now and pick date/time
function setup_the_datepicker( timeState  )
{
    if( timeState )
    {

    }
    if( timeState['nowDisabled'] == true )
    {   
        $("#radio1").prop( "disabled", true );
    }
    else
    {
        $("#radio1").prop( "disabled", false );
    }
    if( timeState['nowHidden'] == true )
    {
        $(".date-time-text").show();
        $("#radio1").prop( "checked", false );
        $("#radio2").prop( "checked", true );
    }
    else
    {
        $(".date-time-text").hide();
        $("#radio1").prop( "checked", true );
        $("#radio2").prop( "checked", false );
    }
}



function setDatePicker( newDate, newTime )
{   
    $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', newDate ));
    checkout.set_time_html( newTime );
}

function checkValidTime(  )
{
    if( ha.common.getTimepicked(  ) == 1 )
    {
        the_picked_date = ha.common.getSpecificDate(  );
        the_picked_time = ha.common.getSpecificTime(  );

        if( ha.common.getToday( ) == 1 && ha.common.get24HoursRequired( ) == 0)
        {
            return false;
        }
        return true;
    }
}


function dateAdd( d, daysToAdd )
{
    if( !daysToAdd )
        daysToAdd = 1;
    
    result = new Date( d );
    result.setDate( result.getDate() + daysToAdd );
    return result;
}





function calcFloor(  )
{
    current = checkout.getCurrentDayHour(  );
    newDateMin = checkout.date_plus_1( current );
    newDateMin = checkout.set_date_time( newDateMin, ha.common.getOpenTime( ) );
    
    return newDateMin;
}


function format_date_time_for_display ( d )
{
   self = this;
   if( checkout.is_empty( d ) == 1 )
    return undefined; 

date_string = this.format_date( d );
d = new Date( d );
hours = d.getHours(  );
min = d.getMinutes( );
std_hours = military_hours_to_standard(  hours );

ampm = get_am_pm_from_military_hour( hours );

time_string = std_hours + ":" + this.pad( min, 2 ) + ampm;
return date_string + " " + time_string;
}


function military_hours_to_standard( display_hours )
{
    display_hours = parseInt(  display_hours ) ;
    if( display_hours > 12 )
        display_hours = display_hours - 12;
    return display_hours;
}






function getTimeFloor_closed_only(  )
{
    // ADD SAT SUN CLOSED HERE.
    newDateMin = checkout.getCurrentDayHour(  );
    if(  checkout.check_if_closed(  ) == 1 )
    {
        newDateMin = calcFloor(  );
    }
    
    newDateMin = checkout.roundMinutes( newDateMin  );
    return newDateMin;
}






function set_date_time( date, time  )
{
    day = new Date( date );
    time_split = time.split(/\:|\-/g);
    day.setHours(time_split[0]);
    day.setMinutes(time_split[1]);
    return day;
}


function disableNow( trueToDisable )
{
    checkout.setNowDisabled( trueToDisable );
}

function dynamic_hide_date( trueToPickNow )
{
    function showNow(  )
    {
      console.log( "*+ SHOW NOW" );
      if( ha.common.getClosed(  ) == 1 )
      {
        showDatePicker( );
        return;
    }
    $(".date-time-text").hide();
    $("#radio1").prop( "disabled", false );
    $("#radio1").prop( "checked", true );
    $("#radio2").prop( "checked", false );
    $(".date-time-text").hide();
    ha.common.check_validations_state(  );
} 

function showDatePicker(  )
{
    $("#radio1").prop( "checked", false );
    $("#radio2").prop( "checked", true );
    $(".date-time-text").show();

    $('.timechange').unbind('change').on('change', function () {
        if (ha.common.checkBreadType() == "error") {
            return false;
        }
        else
        {
            ha.common.setSpecificDateTime( $( '.delivery_date' ).val(  ) , $( '.timedropdown' ).val(  ) );
            
        }
    });
    
    time_state_array = ha.common.getTimeStateArray(  );

    if(  ha.common.getNowDisabled( ) == 1 )
    {
        showDatePicker(  );
        return;
    }

    if ( trueToPickNow == true ) 
    {
        showNow(  );
    } 
    else
    {
        showDatePicker(  );
        ha.common.onSpecificDateCallavailTime( hideValue );
    }
}





}







    $('.list-one a:nth-child(4)').click(function()
    {
      $("#sandwiches-popup").show();
  })
    $('.close-button').click(function(){
        $('.popup-wrapper').hide();
    })
         // $('.list-one a:nth-child(5)').click(function(){
         //    $("#sandwiches_quick_edit").show();
         // })
         

         

         $(".collapse-expand").click(function()
         {
            $(this).find('span').text(($(this).find('span').text() == '+')?'-':'+');
            $(this).next('.collapse').toggle();
        })
