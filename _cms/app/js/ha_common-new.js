ha.common = window.ha.common || {
    
    errChkVars: {
        choose_shop_type: false,
        choose_address: false,
        choose_date: false,
        choose_billing: false,
    },
    allow_date:true,
    zipcodes: [],
    product_extras: null,
    tempSlector: null,
    specific_exec : false,
    validation_blocked: 0,
    floor_date: 0,
    tip_set:null,
    zip_database: [],
    time_state_array: [],

    filterArray : [],

    init: function() 
    {
        var self = this;
        
        self.login();
        self.create_account();
        self.forgot_password();
        self.load_slides();
        self.fb_popup();
        self.cookie_login();
        self.common_button_click_events();
        self.add_standard_menu_item_cart();
        self.add_edit_user_review();        
        self.gallery_search();
        if (window.location.href.indexOf("checkout") !== -1 )
        {
            self.download_zip_database(  );
            self.get_all_zip_codes();
        }
        if(window.location.href.indexOf('createsandwich') == -1 && window.location.href.indexOf('sandwich/gallery') == -1)
        { 
            self.get_all_product_extra();
        }
        

        if (window.location.href.indexOf("checkout") !== -1 )
        {
            $("#order-type").val( 1 );
            $('.checkout-msg-red').remove();
            checkout.getSpecificDayForStore( $("#hidden_default_store_id").val() );
            self.setCheckoutFirstRun(  );
        }
        
        
        if(window.location.href.indexOf('sandwich') != -1 && window.location.href.indexOf('gallery') != -1 )
        { 
            $('.sandwich-gallery-left input[type="checkbox"]').prop('checked', false);
        }
        
        cookieAddCartData = ha.common.getCookie('addCartData');
        isFbOnlyUser      = ha.common.getCookie('facebookOnlyUser');
        if(isFbOnlyUser == "1")
        { 
            if(cookieAddCartData != "" && cookieAddCartData != "false")
            {
                session = ha.common.check_session_state();
                if (session.session_state == false) 
                {
                    if(session.facebook_only_user == false)
                    {
                        ha.common.login_popup($(this));
                    } 
                    else 
                    {
                        ha.common.facebook_login($(this));
                    }
                    return;
                }

                outJSon = JSON.parse(cookieAddCartData);
                if(outJSon.type == 'reorder')
                {
                    $(window).load(function()
                    { 
                        $("a[data-order='"+outJSon.sandwichId+"']").trigger("click");
                    });
                }
                else
                {
                    $(window).load(function()
                    { 
                        if(outJSon.type == 'product') ftype = 'product'; else ftype = 'user_sandwich';
                        if(!outJSon.uid) uid = SESSION_ID; else uid = outJSon.uid;                          
                        ha.sandwichcreator.add_sandwich_to_cart(outJSon.sandwichId,uid,ftype, outJSon.qty); 
                    });
                } 
                ha.common.setCookie("addCartData",'false',1);
            }
        }
        
        
        self.myaccount_reorder();
        self.order_view_details();
        $("#loginForm_cookie").submit(function(e) 
        {
            e.preventDefault();
        });

        $("#loginForm").submit(function(e) 
        {
            e.preventDefault();
        });
        
        $init_prior = false;
        if (window.location.href.indexOf("checkout") !== -1 )
        { 
            $init_prior = self.check_out_autoSelect();
            if( $init_prior == false )   
            self.common_init(  );
            checkout.check_time_and_change_if_needed(  );    
        }
        
        self.lazy_load_sandwiches();

        if (self.getCookie("sandwich_end_screen") == "true") 
        {
            session = ha.common.check_session_state();
            if (session.session_state == false){}
       
        }
        self.enable_friend_sandwiches();
        self.show_friends_popup_onload();
        self.show_sandwich_detail_onload();
        if (window.location.href.indexOf("checkout") !== -1 )
        {
            self.popup_info_checkout();
        }
        self.lazyLoadMenuSandwiches();
        if (window.location.href.indexOf("checkout") !== -1 )
        {
            self.checkout_time_drop_down();
        }

        if (window.location.href.indexOf("checkout") !== -1 )
        {
            self.look_for_date_change(  );
            checkout.checkTimingsRealtedConditions(  );

            checkout.getTimeAvilableSlots(  );
            checkout.check_time_and_change_if_needed(  ); 
            self.check_validations_state();
        }
    },

    download_zip_database: function(  )
    {
        self = this;
        $.ajax({
            context: this,
            type: "POST",
            url: SITE_URL + 'checkout/getAllZips',
            success: function (e) {
                this.zip_database =  JSON.parse(e) ;
            } 
        });
    
    },
    

    checkout_time_drop_down:function()
    {
        $(document).on('change', ".timedropdown", function() {
        });
        
    },

    popup_info_checkout : function()
    { 
        var self = this;
        setTimeout(function()
        {
            $('.tip-image img').hover(function()
            {                           
                pos = $(this).offset();
                $('.tip-image-info').offset({ top: pos.top , left: pos.left-220});
                $('.tip-image-info').show();
                $('.tip-image-info .close-button').unbind('click').on('click',function(){
                    $('.tip-image-info').hide();
                });
            },function(){});
        
        }, 3000);
        setTimeout(function(){          
            $('.tip-image img').hover(function(){                           
                pos = $(this).offset();
                $('.tip-image-info').offset({ top: pos.top , left: pos.left-220});
                $('.tip-image-info').show();
                $('.tip-image-info .close-button').unbind('click').on('click',function(){
                    $('.tip-image-info').hide();
                });
            },function(){   });
        
        }, 200);
        
        $('.tip-image img').hover(function(){           
                
            pos = $(this).offset();
            $('.tip-image-info').offset({ top: pos.top , left: pos.left-220});
            $('.tip-image-info').show();
            $('.tip-image-info .close-button').unbind('click').on('click',function(){
                $('.tip-image-info').hide();
            });
        },function(){   });
    },

    show_sandwich_detail_onload: function() 
    {
        $(window).ready(function() 
        {
            sandwichId = ha.sandwichcreator.get_url_params("galleryItem");
            if (window.location.href.indexOf('gallery') != -1 && sandwichId) 
            {
                sandwichId = parseInt(sandwichId);
                if($('.menu-listing').find("[data-id='" + sandwichId + "']").length > 0)
                {
                    $('.menu-listing').find("[data-id='" + sandwichId + "']").find('a:contains(VIEW)').trigger('click');
                }
                else
                {
                    var user_id = (ha.common.getCookie('user_id'))?ha.common.getCookie('user_id'):0;
                    $.ajax({
                        type: "POST",
                        data: {
                            'id': sandwichId,
                            'user_id':user_id
                        },
                        url: SITE_URL + 'sandwich/get_individual_sandwich_data/',
                        async: false,
                        success: function(data) 
                        {
                           json = JSON.parse(data);
                           if (json.state == false) 
                           {
                                alert("Sorry sandwich you are looking for has been removed!")
                            } 
                            else 
                            {
                                var append = '';
                                append = '<li data-sandwich_desc="'+json.sandwich_desc+'" data-sandwich_desc_id="'+json.sandwich_desc_id+'" data-flag="'+json.flag+'" data-menuadds="'+json.menu_add_count+'" data-userid ="'+json.uid+'"  data-cheese="" data-topping="" data-cond="" data-toast="'+json.menu_toast+'" data-formatdate="'+json.formated_date+'" data-username="'+json.user_name+'" data-protien="" data-bread=""  data-date="'+json.date_of_creation+'" data-price="'+json.sandwich_price+'" data-id="'+json.id+'" data-name="'+json.sandwich_name+'" data-likeid="'+json.like_id+'" data-likecount="'+json.like_count+'">';
                                append += '<span class="inner-holder" style="position: relative">';
                                append += '<input type="hidden" name="chkmenuactice'+json.id+'" value="'+json.menu_is_active+'" />';
                                append += '<img data-href="'+SITE_URL+'"createsandwich/index/"'+json.id+'" width="124" src="'+ json.imagepath +'" alt="sandwitchimageview">';
                                append += '<h3 title="'+json.sandwich_name+'">'+json.sandwich_name+'</h3>';
                                append += '<a href="javascript:void(0);" class="link">VIEW</a></span><input type="hidden" name="saved_tgl'+json.id+'" value="'+json.saved+'">';
                                append += '<input class="typeSandwich" type="hidden" value="FS"></li>';
                                console.log(append);
                                $('ul.menu-listing').append(append);
                                
                                $('.menu-listing').find("[data-id='" + sandwichId + "']").find('a:contains(VIEW)').trigger('click');
                            }
                        }
                    });
                }
            }
        });

    },


    //friend sandwiches 
    enable_friend_sandwiches: function() 
    {
        var self = this;
        
        $('#show_friends_sandwich').unbind('click').on('click', function(e) 
        {
           e.preventDefault();
           text = $(this).text();
           if(text.indexOf("FRIENDS' SANDWICHES") != -1 && $(this).attr('id') != 'fbLogin_alt' ) 
           { 
              
                session = ha.common.check_session_state();
                if (session.session_state == false) 
                {
                    ha.common.setCookie("reopen_fb_pop", true, 1);
                    ha.common.facebook_login();
                    return;
                }
                else if(session.facebook_only_user == false)
                {
                    self.show_friends_popup();
                    self.set_friends_popup_data();
                } 
                else 
                {
                    valid_toc = self.check_valid_User_AccessToken(true);
                    if (valid_toc.Response['status-code'] == '200') 
                    {
                        self.show_friends_popup();
                        self.set_friends_popup_data();
                    }
                    else 
                    {
                        ha.common.setCookie("reopen_fb_pop", true, 1);
                        self.facebook_login();
                    }
                }
           }
        });

        $('#showFriendsSandwich').unbind('click').on('click', function(e) 
        {
            $("#ajax-loader").show();
            e.preventDefault();
            text = $(this).text();  
            
            if(text.indexOf("FRIENDS' SANDWICHES") != -1 && $(this).attr('id') != 'fbLogin_alt' ) 
            { 
                session = ha.common.check_session_state();

                if (session.session_state == false) 
                {
                    $("#ajax-loader").hide();
                    ha.common.setCookie("reopen_fb_pop", true, 1);
                    ha.common.facebook_login();
                    return;
                }
                else if(session.facebook_only_user == false)
                {
                     $("#ajax-loader").hide();
                    self.check_friends_popup_data();
                } 
                else 
                {
                    valid_toc = self.check_valid_User_AccessToken(true);
                    if (valid_toc.Response['status-code'] == '200') 
                    {
                       
                        window.location = SITE_URL + "menu/friendSandwiches";
                    }
                    else 
                    {
                        $("#ajax-loader").hide();
                        ha.common.setCookie("reopen_fb_pop", true, 1);
                        self.facebook_login();
                    }
                }
            }
        });
    },

   show_friends_popup: function() {
        $("#facebook-friends-list").show();
    },


    set_friends_popup_data: function() 
    {
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'menu/getFBfriendsMenu',
            success: function(data) {
                $('body').css('overflow', 'hidden');
                $("#facebook-friends-list ul").html(data);
                ha.sandwichcreator.enable_scrolbar();
                //on clicking back to menu button on friends menu pop-up
                $('.back-to-menu').on('click',function(e){
                    e.preventDefault();
                    $('.close-button').trigger('click');
                });
            }
        });
    },

    check_friends_popup_data: function() 
    {
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'menu/getFBfriendsList',
            success: function(data) 
            {
                if(data == 1)
                {
                    window.location = SITE_URL + "menu/friendSandwiches";
                }
                else if(data == 2)
                {
                    $("#facebookFriendsList").show();
                    $("#facebookFriendsList ul").html("Looks like you're the first of your friends to join Barney Brown! Let them know what they're missing out on!");
                }
                else
                {
                    $("#facebookFriendsList").show();
                    $("#facebookFriendsList ul").html("Sorry you need to be logged in with Facebook to use this feature!");
                }
            }
        });
    },

    show_friends_popup_onload: function() {
        
        var self = this;
        if (window.location.href.indexOf("menu") != -1) {
            $(document).ready(function() {
                
                if (self.getCookie("reopen_fb_pop") == "true") {

                    valid_toc = self.check_valid_User_AccessToken(true);
                    if (valid_toc.state == true) {
                        self.show_friends_popup();
                        self.set_friends_popup_data();
                    }
                    self.setCookie("reopen_fb_pop", false, 1);
                }
            });
        }  else {
            self.setCookie("reopen_fb_pop", false, 1);
        }
    },
    
    //Lazy loading for User Menu Sandwiches
    lazyLoadMenuSandwiches : function(){
 
        var self = this; var set = 0;
        $(window).load(function(){
            if(window.location.href.indexOf("menu") != -1 && window.location.href.indexOf("friends_menu") == -1){       
                $(window).scroll(self.menu_bind_scrolls);
            }
        });
    },
    
    Menu_More_Item : true,    
    
    menu_bind_scrolls : function()
    {
        var self = this;
        //filter Scroll Prevention
        if(ha.common.Menu_More_Item == true && ha.common.Ajax_busy == false){
            
            ha.common.load_more_menu_sandwiches();  
        }   
     
    },
    
    load_more_menu_sandwiches : function(){
        
        var self = this;
        self.Ajax_busy = true;
        var noScroll = false;
        totalMenuitems = $('input[name="countMenuItems"]').val();
        $('.loadMoreGallery').show();
        $current_count = $(".menu-listing li[data-id]").length; 
        if(totalMenuitems == $current_count) return false;
        if( parseInt($current_count)){ 
            $.ajax({
                    type: "POST",
                    data:{ "count" : $current_count-1 },
                    url: SITE_URL+'menu/lazyLoadMoreUserSandwich',
                    success: function(e){
                        if(e.trim() != "None"){
                            $('.menu-listing li:last').after(e);                        
                            self.Ajax_busy = false;
                            $('.loadMoreGallery').hide();
                            ha.sandwichcreator.listing_click();
                            self.add_standard_menu_item_cart();
                        }else{
                            ha.common.Menu_More_Item    = false;
                            $('.loadMoreGallery').hide();
                        }   
                    }
            }); 
            
        } else {  $('.loadMoreGallery').hide(); } 
         
    },
    
    check_valid_User_AccessToken: function() {
        var self = this;
        var out;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'myaccount/getUserAccessToken',
            async: false,
            dataType: 'json',
            success: function(data) {
                out = data;
            }
        });
        return out;
    },

    get_from_local_zip_database: function(  zip_code_given, current_store_id )
    {
        zip_db = this.zip_database;
        for (var key in zip_db) 
        {
            if (!zip_db.hasOwnProperty(key)) continue;
             
             var zip_item = zip_db[key];

             if( String(zip_item.zipcode) == String(zip_code_given) )
             {
                var min_order_amount = zip_item.min_order;
                var store_id = zip_item.store_id;
                 if( current_store_id === null || current_store_id === undefined )
                    return {"invalid":0,"minimum":min_order_amount,"store_id":store_id};
                else
                {
                    if(  store_id == current_store_id )
                        return {"invalid":0,"minimum":min_order_amount,"store_id":store_id};
                }
            }
        }
        return {"invalid":1, "minimum":min_order_amount, "store_id":store_id };
    },

    get_active_zip: function ()
    {
        var self = this;
        if ( !$('input[name="ziptransfer"]').val() ) {
            return setTimeout(self.get_active_zip, 1000);
        }
        return $('input[name="ziptransfer"]').val();
    },

    set_validation_blocker: function (  )
    {
        this.validation_blocked = 1;
    },
    clear_validation_blocker : function (  )
    {
        this.validation_blocked = 0;
    },

    check_validations_state: function() {
        var self = this;

        setTimeout(function(){
            
            if (window.location.href.indexOf("checkout") == -1) return;
            var i = 0;

            zipcode_selected = self.get_active_zip( );
            zipcode_info = self.get_from_local_zip_database( zipcode_selected );


            var payment_Form = $('#_payment_form').serializeArray();

           var temporary_card_entered = false;
           var address_selected = false;
           var billing_selected = false;
           var bad_zip_so_fail = false;
           var need_amount = 0;
           var has_enough_to_continue =  false;

            subtotal = parseInt($('h4 >.sub-total').first().text(  ), 10);
           
           if(  zipcode_info.invalid === 0 )
           {
                need_amount = parseInt( zipcode_info.minimum, 10 );
                bad_zip_so_fail = false;
           }
           else
           {
             bad_zip_so_fail = true;
           }

           if( subtotal < need_amount  ){
               has_enough_to_continue = false;
            }
            else{
                has_enough_to_continue = true;
            }
            
           
            try{
           if( payment_Form[1].value )  
           {
                temporary_card_entered = true;
           }
           else{
                temporary_card_entered = false;
           }
           }
            catch (e){
                    temporary_card_entered = true;
            }

           if ( $('input[name="address_selected"]').val( ) == undefined )       {
                address_selected = false;
           }
           else{
                address_selected = true;
           }

            var change_billing_val = $("#changeBilling").val();
      
            if ( change_billing_val == undefined || change_billing_val == -1 || change_billing_val == 0 ) {
                billing_selected = false;
            }
            else {
                billing_selected = true;
            }

            if( ( billing_selected == false && temporary_card_entered == false ) || address_selected == false )
                enable_purchase_button = false;
            else
                enable_purchase_button = true;

            if ( bad_zip_so_fail == true ) {
                enable_purchase_button = false;
            }

            if(has_enough_to_continue == false )
                enable_purchase_button = false;   

            if( self.validation_blocked == 1 )
                enable_purchase_button = false;                   
            
            if ( enable_purchase_button == true ) {
              $('.place-order').removeClass('disabled');
                $('.place-order').addClass('link');
            } else {
                  $('.place-order').addClass('disabled');
                $('.place-order').removeClass('link');

                
            }
        },600);
        applyDiscount();
    },

    get_all_product_extra: function() {
        
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'sandwich/getAllproductsExtras/',
            async: false,
            dataType: 'json',
            success: function(data) {
                self.product_extras = data;
            }
        });

    },

    gallery_search: function() {
        var Array = {};

        Array['BREAD'] = [];
        Array['PROTEIN'] = [];
        Array['CHEESE'] = [];
        Array['TOPPINGS'] = [];
        Array['CONDIMENTS'] = [];

        var $protien, $condiments, $toppings, $cheese;


        var self = this;
        var $sel;
        var parent;
        $('.sandwich-gallery-left input[type="checkbox"]').unbind('click').on('click', function() {

            $('.menu-listing li').show();
            $txt = $(this).next().text();
            $txt = $txt.trim();
           
            parent = $(this).parent().parent();
            $key = $(parent).prev('h3').text()
            $key = $key.trim();
            $key = $key.toUpperCase();

            switch ($key) {

                case 'BREAD':

                   
                    if ($(this).prop('checked') == true) {
                        $(this).parent().siblings().children(":checkbox").prop('checked',false);
                        if ($.inArray($txt, Array['BREAD']) == -1) {
                            Array['BREAD'] = [];
                            Array['BREAD'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['BREAD']) != -1) {
                            Array['BREAD'].splice($.inArray($txt, Array['BREAD']), 1);
                        }
                    }
                    break;


                case 'PROTEIN':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['PROTEIN']) == -1) {
                            Array['PROTEIN'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['PROTEIN']) != -1) {
                            Array['PROTEIN'].splice($.inArray($txt, Array['PROTEIN']), 1);
                        }
                    }
                    break;

                case 'CHEESE':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['CHEESE']) == -1) {
                            Array['CHEESE'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['CHEESE']) != -1) {
                            Array['CHEESE'].splice($.inArray($txt, Array['CHEESE']), 1);
                        }
                    }
                    break;

                case 'TOPPINGS':
                    if ($(this).prop('checked') == true) {
                        if ($.inArray($txt, Array['TOPPINGS']) == -1) {
                            Array['TOPPINGS'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['TOPPINGS']) != -1) {
                            Array['TOPPINGS'].splice($.inArray($txt, Array['TOPPINGS']), 1);
                        }
                    }
                    break;

                case 'CONDIMENTS':
                    if ($(this).prop('checked') == true) {

                        if ($.inArray($txt, Array['CONDIMENTS']) == -1) {
                            Array['CONDIMENTS'].push($txt);
                        }
                    } else {
                        if ($.inArray($txt, Array['CONDIMENTS']) != -1) {
                            Array['CONDIMENTS'].splice($.inArray($txt, Array['CONDIMENTS']), 1);
                        }
                    }
                    break;

            }

            // check to make sure, if no filters selected, then the massive data of all sandwiches is in play, which takes special consideration, so pop back to our base page. 
            if( Array['BREAD'].length == 0 && Array['TOPPINGS'].length == 0 && Array['CONDIMENTS'].length == 0 &&  Array['CHEESE'].length == 0 &&  Array['PROTEIN'].length == 0 )
            {

                window.location.href = 'sandwich/gallery';
                window.location.reload ( true );
                return;
            }
            
            self.filterArray = Array;
           
            var sortBy = $('.sortby').val();
            var searchTerm = $("#searchTerm").val();
            var $string = JSON.stringify(Array);
            //console.log("$string",$string);
            $.ajax({
                   type: "POST",
                   data: {   'filter' : $string , 'sortBy' : sortBy, 'searchTerm':searchTerm },
                   url: SITE_URL + 'sandwich/filters/',
                   beforeSend: function() {
                        $("#ajax-loader").show();
                    },
                 /*   complete: function() {
                        $("#ajax-loader").hide();
                    },*/
                   success: function(e) { 
                       $('.menu-listing').html(e);
                       var sandwich_count = $("#sandwich_count").val();
                       $(".sandwitchitemCount").html(sandwich_count+" Creations");
                       $(".sandwitchitemCount").data('count', sandwich_count);
                       $("#ajax-loader").hide();
                           ha.common.Ajax_busy = false;
                       ha.common.bind_scrolls();
                   }
               });       
            
        });

    },

    lazy_load_sandwiches: function() {

        var self = this;
        var set = 0;
        $(window).load(function() {
            if (window.location.href.indexOf("gallery") != -1) {
                $(window).scroll(self.bind_scrolls);
            }
        });
    },

    Ajax_busy: false,

    bind_scrolls: function() {

        var $current_count = parseInt($('.menu-listing li').length);
        var $total_items = parseInt($(".sandwitchitemCount").data('count'));  
        
        console.log("total sandwiches "+$total_items)

        if ($('.menu-listing li').length < 20) {
            if ($(window).scrollTop() > 500) {
                if (ha.common.Ajax_busy == false ) ha.common.load_more_sandwiches();
            }
        } else {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) 
            {
                setTimeout(function() {

                    if (ha.common.Ajax_busy == false && ($current_count < $total_items) /*&& $('input[name="search"]').length == 0*/) 
                    {
                        ha.common.load_more_sandwiches();
                    }
                }, 300);
            }
        }
    },

    load_more_sandwiches: function() {
        var self = this;
        var Array = {};


        Array  = self.filterArray;

        var searchTerm = $("#searchTerm").val();
        
        var sortBy = $('.sortby').val();
        var $string = "";
        if(Object.keys(Array).length > 0)
        {
            $string = JSON.stringify(Array);
        }
        
        self.Ajax_busy = true;
        $('.loadMoreGallery').show();
        $current_count = $('.menu-listing li').length;
        $total_items = $('input[name="total_item_count"]').val();
        sort_id = $('input[name="sort_type"]').val();

        if (parseInt($current_count) < parseInt($total_items)) {
            $.ajax({
                type: "POST",
                data: {
                    "count": $current_count,
                    'filter' : $string ,
                    'sortBy' : sortBy,
                    "sort_id": sort_id,
                    "searchTerm": searchTerm
                },
                url: SITE_URL + 'sandwich/get_more_gallery',
                success: function(e) {
                    $('.menu-listing li:last').after(e);
                    ha.sandwichcreator.listing_click();
                    self.Ajax_busy = false;
                    //$('.sandwitchitemCount').text($('.menu-listing li').length + " Creations");
                    $('.loadMoreGallery').hide();
                }
            });
        } else {
            $('.loadMoreGallery').hide();
        }

    },
    disableSpecificDates: function( date )
    {
        if(  ha.common.specific_day  == "" )
        {
            return [1];
        }

        var bad_date = new Date(  ha.common.specific_day );
        var m_bad = bad_date.getMonth();
        var d_bad = bad_date.getDate();
        var y_bad = bad_date.getFullYear();
         

        var m = date.getMonth();
        var d = date.getDate();
        var y = date.getFullYear();
         
         // First convert the date in to the mm-dd-yyyy format 
         // Take note that we will increment the month count by 1 
         var currentdate = (m + 1) + '-' + d + '-' + y ;
         var bad_date_formatted =  (m_bad + 1) + '-' + d_bad + '-' + y_bad ;
          // We will now check if the date belongs to disableddates array 
         if( currentdate == bad_date_formatted  )
         {
            return [false];
         }
         else
         {
            return [1];
         }
    },   

    init_date_picker: function(  )
    {
        self = this;
        $(".delivery_date").datepicker({
            beforeShowDay: disableSpecificDates,
            showOn: "button",
            buttonImage: siteurl + "/app/images/date-picker.png",
            buttonImageOnly: true,
            buttonText: "Select date",
            defaultDate: new Date(),
            minDate: 0,
            dateFormat: 'D, M d, yy' 
        });
    },


    // look for date change in checkout page
    look_for_date_change: function() 
    {

        var self = this;
        
        $("select.select_pickup").unbind("click");

        $(document).on('change', "select.select_pickup", function() 
        {
            var add_id = $(this).val();
            var isCurrentDay = $("#selected_date").val();
            var selectedDay = $("#selected_day").val();
            if (selectedDay == null || selectedDay == '') selectedDay = $('input[name="todays_date"]').val();
            $("#hidden_store_id").val(add_id);
            checkout.getSpecificDayForStore( $("#hidden_store_id").val(  ) );
            if (add_id > 0) {
                self.getAddressInfo(add_id);
            }
            self.updatePickupOrDeliveryInCart(0);
            self.check_current_condition_for_time();
            self.popup_info_checkout();            
             
        });
        

        $(".select_address").unbind("change");
        $(document).on('change', "select.select_address", function() 
        {  
         
            self.check_current_condition_for_time();
            self.popup_info_checkout();
             checkout.check_time_and_change_if_needed(  );     
        });

        var default_load_date = new Date();
        
        if(self.getCookie("specific_date_exists") == 1)
        {
           setTimeout(function(){
                var default_load_date = self.getCookie("specific_date_sel");
              }, 6000);
            
        }

        $(".delivery_date").datepicker({
            showOn: "button",
            buttonImage: siteurl + "/app/images/date-picker.png",
            buttonImageOnly: true,
            buttonText: "Select date",
            defaultDate: new Date(),
            minDate: 0,
            dateFormat: 'D, M d, yy',
            beforeShowDay: self.disableSpecificDates,
            onSelect: function(selectedDate) 
            {
                checkout.check_time_and_change_if_needed(  );
            }
        });
        
        $( ".delivery_date" ).datepicker( "setDate", new Date());
              
        $( '.date-time-text' ).on( 'change' , function( e )
        { 
            if( e.which )
            {
                
            }
            else
            {
                
            }
            ha.common.setTimepicked( 1 );
            ha.common.setSpecificDateTimeFromElements(  );
            self.check_current_condition_for_time();
        });
        
        $('.delivery_date').datepicker('option', 'onSelect', function( selectedDate )  
        { 
            ha.common.clear_checkout_message(  );
            pick_test  = checkout.set_date_time( $('.datepicker').val(  ), '08:00' );
            
            checkout.limit_select_times_by_date(  );
            ha.common.setTimepicked( 1 );
            ha.common.setSpecificDateTimeFromElements(  );
            ha.common.check_validations_state(  );
                
            dmDaterPickerSelected(  );
            $('.delivery_date').click(function()
            {
                $('.delivery_date').datepicker('show');
            });
        });  
    },

    set_html_date: function( date )
    {
        d = new Date(  date  );
        $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', d));
    },
    
    getCurrentTimJs : function()
    {
        var self = this;
        servtime  = self.getServerTime();
        servtime  = servtime.split(",");
        servtime[1] = parseInt(servtime[1]-1);
        return new Date(servtime[0],servtime[1],servtime[2],servtime[3],servtime[4],servtime[5] );
    },
    
    getAlphaDay : function(day)
    {
        var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        return days[day];
    },
    
    getAlphaMonth : function(month)
    {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return monthNames[month];
    }, 
    
    onSpecificDateCallavailTime : function( target_date ) // not returning anything
    {
        var self = this;
        return;

        if( !target_date )
          target_date  = self.getCurrentTimJs();
      
        datepickerDate = new Date( $( '.delivery_date' ).datepicker( "getDate" ) );
   

        var current_day = self.getAlphaDay( datepickerDate.getDay() )+", "+self.getAlphaMonth(datepickerDate.getMonth())+" "+datepickerDate.getDate()+", "+datepickerDate.getFullYear();


        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        selectedDay = $("#selected_day").val();
        $("#selected_day").val(selectedDay);
        var add_id = $("#hidden_store_id").val();
        if(add_id=="")
        {
            add_id=$("#hidden_default_store_id").val();
        }
        
        var is_delivery = -1;
        var order_type = $("#order-type").val();
        if(order_type){
          
            is_delivery  = 1;
        } else {
            is_delivery  = 0;
        }
        
     
         var isCurrentDay = 1;
         var selectedDay  = $("#selected_day").val();
         var currentTime;
         currentTime = currentDate.getHours() + ":" + currentDate.getMinutes();
        
        if(currentDate.withoutTime() < datepickerDate.withoutTime()){
            
            dateSel          = days[datepickerDate.getDay()];
            
            $obj = { "isDelivery": is_delivery , "current_time" : currentTime , "current_day" : current_day , "store_id" : add_id , "selectedDay" : dateSel , "isCurrentDay" : 0  };
        }else{
            
            dateSel          = days[currentDate.getDay()];
            $obj = { "isDelivery": is_delivery , "current_time" : currentTime , "current_day" : current_day , "store_id" : add_id , "selectedDay" : dateSel , "isCurrentDay" : isCurrentDay  }; 
        }
        self.setCurrentDate( current_day );
    },

    getAddressInfo: function(add_id) 
    {
        $.ajax({
            type: "POST",
            data: {
                "store_id": add_id
            },
            url: SITE_URL + 'checkout/pickupAddressInfo',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) {
                $(".checkoutdynamicdiv").html(e);
                bindCheckoutPageEvents();
                ha.common.check_validations_state();
            }
        });
    },

    setZipStatus : function( threshold )
    {
         ha.common.setCookie( "zip_status", threshold , 1);
    },

    getZipStatus_return : function(  )
    {
        return ha.common.getCookie("zip_status");
    },

    clearZipStatus : function(  )
    {
        ha.common.setCookie( "zip_status", null );
    },



    getZipStatus : function( threshold )
    {
        $.when( checkZipNeeds100(  ) ).done( function(  )
        {

        });
         return ha.common.getCookie("zip_status");
    },


    setDateChanged: function( dateChanged )
    {
        ha.common.setCookie( "date_changed", dateChanged );
    },

    getDateChanged: function(  )
    {
        if( ha.common.getCookie("date_changed") )
            return ha.common.getCookie("date_changed");
        else
            return 0;

    },


    setToday: function( today )
    {
        ha.common.setCookie( "today",today );
    },

    getToday: function(  )
    {
        if( ha.common.getCookie("today") )
            return ha.common.getCookie("today");
        else
            return 0;
    },

    setDelivery: function( trueForDelivery )
    {
        ha.common.setCookie( "is_delivery",trueForDelivery );
    },

    getDelivery: function(  )
    {
          if( ha.common.getCookie("is_delivery") )
            return ha.common.getCookie("is_delivery");
        else
            return 0;

    },

    clearUserTimeTemp : function(  )
    {

        this.setTimepicked( 0 );
        this.setSpecificDateTime( 0 );
    },

    clearUserTimeValidated : function(  )
    {
        this.setUserTimeValidated( 0 );
    },

    setUserTimeValidated: function( user_date )
    {
        if( user_date != 0  )
            date_pack = JSON.stringify(  user_date );
        else
            date_pack = 0;
        this.setCookie( "user_date_validated", date_pack , 1);
    },

    getUserTimeValidated: function(  )
    {
        if( ha.common.getCookie("user_date_validated") )
            return JSON.parse( ha.common.getCookie("user_date_validated") );
        else
            return 0;
    },

    setSpecificDateTimeFromElements: function(  )
    {
        this.setTimepicked( 1 );
        ha.common.setCookie( "newDate", $( ".delivery_date" ).val(  ) );
        ha.common.setCookie( "newTime", $('.timedropdown').val(  ) );
    },

    setSpecificDateTime: function( newDate, newTime )
    {
        if( newDate == 0 ){
            ha.common.setCookie( "newDate", 0,1 );
            ha.common.setCookie( "newTime", 0, 1);
        }
        else
           ha.common.setSpecificDateTimeFromElements(  );
       
    },

    getSpecificDate: function(  )
    {
        if( ha.common.getCookie("newDate") )
            return ha.common.getCookie("newDate");
        else
            return 0;
    },

    getSpecificTime: function(  )
    {
        if( ha.common.getCookie("newTime") )
            return ha.common.getCookie("newTime");
        else
            return 0;
    },

    setOpenTime: function ( openTime )
    {
        
        ha.common.setCookie( "open_time", openTime );
    },

    setClosedTime: function ( closedTime )
    {
        ha.common.setCookie( "closed_time", closedTime );
    },

    setCurrentTime: function ( time_set )
    {
        ha.common.setCookie( "current_time", JSON.stringify( time_set ) );
    },

    

    getCurrentTime: function (  )
    {
        if( ha.common.getCookie("current_time") ){
            return JSON.parse(  ha.common.getCookie("current_time") );
        }
        else
        {
            return this.getServerTime(  );
        }
        
    },






    format_date: function( d )
    {
        self = this;
        if( checkout.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var date_string = self.getAlphaDay(date.getDay())+", "+self.getAlphaMonth(date.getMonth())+" "+date.getDate()+", "+date.getFullYear();
        return date_string;
    },

    military_to_regular: function( timeString, showMeridian )
    {
        var time_split = timeString.split(/\:|\-/g);
        var display_hours = parseInt(  time_split[0] ) ;
        var minutes = time_split[1];
        var meridin
        if( display_hours > 12 )  {
            meridian = 'PM';
            display_hours = display_hours - 12;
        }
        else if ( display_hours == 12 )
        {
            meridian = 'PM';
            display_hours = display_hours;   
        }
        else
        {
            meridian = 'AM';
            if ( display_hours == 0 )
                 display_hours = 12;
        }

        if( showMeridian )
            return display_hours +":" +minutes +" " + meridian;  
        else
            return display_hours +":" +minutes ;  
    },

    format_date_time: function( d )
    {
        self = this;
        if( checkout.is_empty( d ) == 1 )
            return undefined; 

        date_string = this.format_date( d );
        time_string = checkout.format_time( d );

        return date_string + " " + time_string;
    },

   
    get_date_difference: function( start_date, end_date )
    {
        var date1 = new Date( start_date );
        var date2 = new Date( end_date ); 
        var start = Math.floor(date1.getTime() / (3600 * 24 * 1000)); 
        var end = Math.floor(date2.getTime() / (3600 * 24 * 1000)); 
        var daysDiff = end - start; 
        return daysDiff;
    },


    pad : function (num, size) 
    {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    },


   
    

    getClosedTime: function (  )
    {
        if( ha.common.getCookie("closed_time") )
            return ha.common.getCookie("closed_time");
        else
        {
            checkout.checkTimingsRealtedConditions(  );
            return this.getCookie("closed_time");
        }
        
    },

   
    getOpenTime: function( ) 
    {
        if( ha.common.getCookie("open_time") )
            return ha.common.getCookie("open_time");
        else
        {
            checkout.checkTimingsRealtedConditions(  );
            return this.getCookie("open_time");
        }
    },

    placeOrderDisabled: function (  )
    {
        $(".place-order").addClass("disabled");
    },

    placeOrderEnabled: function (  )
    {
        $(".place-order").removeClass("disabled");
    },
    


    getNowDisabled : function(   )
    {
         if( ha.common.getCookie("trueToDisableNow") == true )
            return 1;
        else
            return 0;
    },


    setCurrentDate : function( currentDate )
    {
        ha.common.setCookie( "current_date",currentDate );
    },

    getCurrentDate : function(  )
    {
        if( ha.common.getCookie("current_date") ){
              current_date = ha.common.getCookie("current_date");
              var day = new Date(current_date);
            var current_day = this.getAlphaDay(day.getDay())+", "+this.getAlphaMonth(day.getMonth())+" "+ ( day.getDate() ) +", "+day.getFullYear();
            return current_day;
        }
        else
        {
            var day = new Date();
            var current_day = this.getAlphaDay(day.getDay())+", "+this.getAlphaMonth(day.getMonth())+" "+ ( day.getDate() ) +", "+day.getFullYear();
            return current_day;
        }
    },

    getCurrentDatePlus1 : function(  )
    {
            var result = new Date( );
            result.setDate( result.getDate() + 1 );
         
            var current_day = this.getAlphaDay(result.getDay(  ) )+", "+this.getAlphaMonth(result.getMonth())+" "+ ( result.getDate() ) +", "+result.getFullYear();
            return current_day;
    },


    pad : function (num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    },

    formatTimeBB: function( d )
    {
        if( !d ) 
            return;

        day = new Date( d );
        hour = this.pad( day.getHours(), 2 );
        minute = this.pad( day.getMinutes(), 2 );

        return hour +":"+minute;
    }, 

    formatDateTimeBB: function( datetime )
    {
        return(  this.formatDateBB( datetime ) + " " + this.formatTimeBB( datetime ) );
    },

    formatDateBB: function( d )
    {
        self = this;
        day = new Date( d );
        if( !day ) 
            return;

        return(  self.getAlphaDay(day.getDay())+", "+self.getAlphaMonth(day.getMonth())+" "+day.getDate() +", "+day.getFullYear() );
    },
    getNoon: function( )
    {

    },


    setCheckoutFirstRun : function(  )
    {
        ha.common.setCookie( "checkout_first_run", 1 );
    },

    getCheckoutFirstRun : function(  )
    {
        if( ha.common.getCookie("checkout_first_run") )
            return ha.common.getCookie("checkout_first_run");
        else
            return 0;
    },

    clearCheckoutFirstRun : function(  )
    {
         ha.common.setCookie( "checkout_first_run", 0 );
    },


    setUserLastPick : function(  wasUser )
    {
        ha.common.setCookie( "last_pick", wasUser );
    },

    getUserLastPick : function(   )
    {
         if( ha.common.getCookie("last_pick") )
            return ha.common.getCookie("last_pick");
        else
            return 0;
    },

    setTimeStateArray: function ( timestate )
    {
       this.time_state_array = timestate;
    },

    getTimeStateArray: function (  )
    {
        return this.time_state_array;

    },

    setNotEnough: function ( notEnough )
    {

        ha.common.setCookie( "not_enough",notEnough );
    },

    getNotEnough: function ( notEnough )
    {
        if( ha.common.getCookie("not_enough") )
            return ha.common.getCookie("not_enough");
        else
            return 0;
    },

    setTimepicked: function ( timePicked )
    {
        ha.common.setCookie( "time_picked",timePicked );
    },

    getTimepicked: function ( )
    {
        if( ha.common.getCookie("time_picked") )
            return ha.common.getCookie("time_picked");
        else
            return 0;
    },

    set24HoursRequired: function ( is24Hours )
    {
         ha.common.setCookie( "requires_24_hours",is24Hours);
    },

    get24HoursRequired: function ( )    
    {
        if( ha.common.getCookie("requires_24_hours") )
            return ha.common.getCookie("requires_24_hours");
        else
            return 0;
    },

    setOpenHour: function( hour )
    {
        ha.common.setCookie( "open_hour", hour);
    },

    setCloseHour: function( hour )
    {
        ha.common.setCookie( "close_hour", hour);
    },

    getCloseHour: function ( )    
    {
        if( ha.common.getCookie("close_hour") )
            return ha.common.getCookie("close_hour");
        else
            return 0;
    },

    getOpenHour: function ( )    
    {
        if( ha.common.getCookie("open_hour") )
            return ha.common.getCookie("open_hour");
        else
            return 0;
    },


    setClosed: function( isClosed )
    {
        ha.common.setCookie( "store_is_closed", isClosed);
    },

    getClosed: function( )
    {
        if( ha.common.getCookie("store_is_closed") )
            return ha.common.getCookie("store_is_closed");
        else
            return 0;
    },

   
    getServerTime : function()
    {
        var dateTime;
        var self = this;
        var currentDayHour = 0;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentHour/',
            async: false,
            success: function(data) {
                 $json =  JSON.parse(data);
                
                if($json.date_time_js){
                    dateTime = $json.date_time_js;
                }
                
            }
        });
        return dateTime;
    },

    dumpTimeParams: function(  )
    {
        return;
    },

    conditional_clear_checkout_message: function( $inner_text  )
    {
        if( $('.checkout-msg-red').text(  ) == $inner_text )
            clear_checkout_message(  );
    },

    clear_checkout_message: function(  )
    {
        $('.checkout-msg-red').remove();
    },


    check_out_autoSelect: function() 
    {
        var self = this;
        var dataObj = null;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getLastOrder/',
            async: false,
            dataType: 'json',
            success: function(data) 
            {
                dataObj = data;
            }
        });

        if (dataObj) $dat = dataObj.Data;
        else $dat = null;

        var $init_prior = false;

        if ($dat)
        {
            $init_prior = true;
            if ($dat.delivery == 1)
            {
                setTimeout(function() {
                    $('a:contains(DELIVERY)').trigger('click');
                    setTimeout(function() 
                    {
                        var address_id = $.session.get('address_id')
                        if(address_id)
                        {
                            $('.select_address').val(address_id).trigger('change');
                        }
                        else
                        {
                            $('.select_address').val($dat.address_id).trigger('change');
                        }
                        

                        if( $('#changeBilling').length>0 )
                        {
                          // $('#changeBilling > option:eq(2)').prop( 'selected',true );

                            var card_id = $.session.get('card_id') != null || $.session.get('card_id') != 'null' ? $.session.get('card_id') : 1;
                            $('#changeBilling').val(card_id).prop( 'selected', true ).trigger('change');
                        }

                        if( $('#changeBilling option[value='+$dat.billinginfo_id+']').length>0 )
                        {
                            // this fixes the thing where with a card but on first use the system shows the popup...
                            var valid_card = -1;
                            $('#changeBilling option').each(
                                function()
                                {
                                    if ( jQuery(this).val() != '-1' && jQuery(this).val() != '0')
                                    {
                                        valid_card = jQuery(this).val();
                                        return valid_card;
                                    }
                                }
                            );
                            
                            if( valid_card != -1 )
                            {
                                $('#changeBilling option[value='+valid_card+']')
                                $('#changeBilling').val(valid_card).prop( 'selected', true );
                                $('#changeBilling').val(valid_card).trigger('change');
                            }
                            else
                            {
                                $('#changeBilling').val($dat.billinginfo_id).prop( 'selected', true );
                                $('#changeBilling').val($dat.billinginfo_id).trigger('change');
                            }
                        }
                        else
                        {
                            $('#changeBilling').val(-1).trigger('change');    
                        }
                        ha.common.check_validations_state();
                        self.check_current_condition_for_time();
                        $('input[name="delivery_store_id"]').val($dat.store_id);
                        $("#hidden_store_id").val($dat.store_id);
                    }, 1000);
                }, 1000);
            }
            else
            {
                setTimeout(function() 
                {
                    $('a:contains(PICK-UP)').trigger('click');
                    setTimeout(function()
                    {
                        $('.select_pickup').val($dat.address_id).trigger('change');
                        self.getAddressInfo($dat.address_id);
                        if($('#changeBilling option[value='+$dat.billinginfo_id+']').length>0)
                        {
                            $('#changeBilling').val($dat.billinginfo_id).trigger('change');
                        }
                        else
                        {
                            $('#changeBilling').val(-1).trigger('change');    
                        }
                        ha.common.check_validations_state();
                        self.check_current_condition_for_time();
                        $("#hidden_store_id").val($dat.store_id);
                        self.updatePickupOrDeliveryInCart(0);
                        self.check_current_condition_for_time();
                    }, 1000);
                }, 1000);
            }
            
            if( self.getUserTimeValidated(  ) == 0  && self.getTimepicked(  ) == 0)
                $($("input[name='radiog_lite'] ")[0]).trigger("click");
            return true;
        }
        else
        {
            setTimeout(function() {
               // $('a:contains(DELIVERY)').trigger('click');
                setTimeout(function() {
                    session = self.check_session_state();
                    if( session.session_state != false)
                    {
                        var address_id = $.session.get('address_id') != null || $.session.get('address_id') != 'null' ? $.session.get('address_id') : '';
                        var card_id = $.session.get('card_id') != null || $.session.get('card_id') != 'null' ? $.session.get('card_id') : '';
                        $('.select_address').val(address_id).trigger('change');
                        $('.a.add-credit-card, a.edit_card').attr("data-id", card_id);
                        $('.a.add-credit-card, a.edit_card').trigger('click');
                    }
                }, 1000); 
              }, 1000); 
        }
       
        return false;
    },
    
    common_init: function(  )
    {
        var self = this;

        if( $('#changeBilling').length > 0 )
        {
            var card_id = $.session.get('card_id') != null || $.session.get('card_id') != 'null' ? $.session.get('card_id') : 1;
            $('#changeBilling').val(card_id).prop( 'selected', true ).trigger('change');
        }
        else
        {
            $('#changeBilling').val(-1).trigger('change');    
        }
        
        ha.common.check_validations_state();
        self.check_current_condition_for_time(  );
                

        if( self.getUserTimeValidated(  ) == 0  && self.getTimepicked(  ) == 0)
        {
            $( $("input[name='radiog_lite'] ")[0] ).trigger("click");
            
        }

    },

    getTimeFromTimeChangeElement: function(  )
    {
        var time_val = $('div.timechange select').val();
        if( time_val == undefined || time_val == 'undefined' ){
            time_val = "12:00PM";
        }
        var time_only = time_val.replace(" PM", "");
        time_only = time_val.replace(" AM", "");
        time_only = time_only.split(":");
    },


    

   

    check_current_condition_for_time : function(  )
    {

    },

    checkNowStoreIsOpened: function()
    {
    },
    
    checkForAllConditionsPlaceOrderButton: function(){
        
    } ,

    get_all_zip_codes: function() 
    {
        var self = this;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'myaccount/getAllzipcodes/',
            async: false,
            dataType: 'json',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(data) {
                self.zipcodes = [];
                Object.keys(data).forEach(function(e) {
                    self.zipcodes.push(data[e]);
                });
            }
        });
    },


    getStore_id_using_zip: function(zip) 
    {
        var self = this;
        var store_id;
        $.ajax({
            type: "POST",
            data: {
                'zip': zip
            },
            url: SITE_URL + 'myaccount/getStoreFomZip/',
            async: false,
            dataType: 'json',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(data) {
                if (data[0]) {
                    store_id = data[0].store_id;
                } else {
                    store_id = 0;
                }
            }
        });

        return store_id;
    },

    
    common_button_click_events: function() 
    {
        var self = this;
        //facebook login poup
        $(".login-facebook").unbind('click').on('click', function(e) {
            e.preventDefault();
            self.facebook_login();
        });
        //login popup
        $(".login").unbind('click').on('click', function(e) {
            $(".error_msg").empty();
            e.preventDefault();
            self.login_popup();
        });
        // user login popup
        $(".user-login").unbind('click').on('click', function(e) {
            $(".error_msg").empty();
            e.preventDefault();
            self.user_login_popup();
        });
        //forgot password
        $(".forgot-password").unbind('click').on('click', function(e) {
            $(".error_msg").empty();
            e.preventDefault();
            self.forgot_password_popup();
        });
        //create account
        $(".create-account,.create-account-btn").unbind('click').on('click', function(e) {
            $(".error_msg").empty();
            e.preventDefault();
            self.register_popup();
        });

       
        $('.account-profile,.shopping-cart, .deliver-button').unbind('click').on('click', function(e) {
            $(".error_msg").empty();
            e.preventDefault();
            session = ha.common.check_session_state();
            
            if (session.session_state == false) {
                ha.common.setShortCookie("redirect", $(this).attr('href') ,1);
                 if(session.facebook_only_user == false){
                   ha.common.login_popup($(this));
                 } else {
                   ha.common.facebook_login($(this));
                 }
                return;
            } else {
                var redirect = ha.common.getCookie( "redirect" );
                if( redirect == null || redirect == "" || redirect == undefined )
                    redirect =  $(this).attr('href') ;
                window.location.href = redirect;
            }
        });

    },

    
    add_edit_user_review: function() 
    {
        var self = this;

        $("a#submit_review").unbind("click").on("click", function() {

            var form = $("form#user_review");
            var how_know = $("form#user_review [name=how_know]").val();
            var rating_order = $("form#user_review [name=rating_order]").val();
            var rating_customer_service = $("form#user_review [name=rating_customer_service]").val();
            var rating_food = $("form#user_review [name=rating_food]").val();
            var message = $("form#user_review [name=message]").val();
            var userdata = form.serialize();
            var userdataArray = form.serializeArray();
            userdataArray.forEach(function(a) {
                if (a.value > 0) {
                    parent = $("select[name='" + a.name + "']").parent();
                    ptag = $(parent).prev();
                    $(parent).hide(0);
                    $(ptag).hide(0);
                }
            });

            if(message){
                $.ajax({
                    type: "POST",
                    data: userdata,
                    url: SITE_URL + 'myaccount/addUserReview/',
                    beforeSend: function() {
                        $("#ajax-loader").show();
                    },
                    complete: function() {
                        $("#ajax-loader").hide();
                    },
                    success: function(data) {
                        if (data) {
                            $( ".review-submit" ).remove();
                            $( "<p class='review-submit'>Your review saved. Thanks.</p>" ).insertBefore( "#submit_review" );                                            
                            document.getElementById("user_review").reset();
                        }
                    }
                });
            }else{
                $( ".review-submit" ).remove();
                $( "<p class='review-submit'>Please Write Review before submiting.</p>" ).insertBefore( "#submit_review" ); 
            }
        });

    },

   
    add_standard_menu_item_cart: function() 
    {
        var self = this;
        $(".common_add_item_cart").unbind('click').on('click', function(e) 
        {
            e.preventDefault();
            $data = $(this).data();
            $.ajax({
                type: "POST",
                url: SITE_URL + 'sandwich/getCurrentprice',
                data: {'pdata': $data},
                success: function(data) {
                   console.log(data);
                }
            })
            ob = $(this);
            liData = $(this).parent().parent().data();
            prev = $(this).parent().prev();
            input = $(prev).find('input[name="spinnerinput"]');
            qtyInp = $(input).val();
            if (qtyInp) qty = qtyInp;
            else qty = 1;
            
            if($data.sandwich == 'product'){
                 ha.sandwichcreator.add_sandwich_to_cart($data.sandwich_id, $data.uid, $data.sandwich, qty, $(this));
            } else if (liData.toast == 0) {
                
                ha.sandwichcreator.add_sandwich_to_cart($data.sandwich_id, $data.uid, $data.sandwich, qty, $(this));
            } else {
                
                ha.sandwichcreator.add_toast_sandwich_to_cart($data.sandwich_id, liData.toast, $data.uid, $data.sandwich, qty, $(this));
            }
        });
    },

    login_popup: function(clkitem) 
    {
        self = this;
        self.tempSlector = clkitem;

        if ($('#login-form-cookie').length > 0) $('#login-form-cookie').show();
        else { $("#login").show();  
        
        if(clkitem){
            
            if(clkitem.hasClass('save-menu')) return;
            isProd = clkitem.hasClass('common_add_item_cart');
            
            data   = clkitem.data();
            
            var qty = 1;
            
            if(data.sandwich == "product") prdChk = true;  else prdChk = false;
            if(isProd && prdChk){
                type       = 'product'; 
                sandwichId = data.sandwich_id;
                                                    
                prev = $(clkitem).parent().prev();
                input = $(prev).find('input[name="spinnerinput"]');
                qtyInp = $(input).val();
                if (qtyInp) qty = qtyInp;
                else qty = 1;
                
            } else if(clkitem.hasClass('reorder-button')){
                
                type       = 'reorder'; 
                sandwichId = data.order;
                
            } else {
                
                type       = 'sandwich';
                if(data.sandwich_id){
                    sandwichId = data.sandwich_id
                } else {
                    
                   sandwichId = $(clkitem.parent()).find('input[name="hidden_sandwich_id"]').val();
                }
            }
            
            uid  = ha.common.getCookie('user_id');
            
            if( type && sandwichId){ 
            finalDobject = { 'uid' : '' , 'type' : type , 'sandwichId' : sandwichId, "qty" : qty   }
            
            ha.common.setCookie("addCartData",JSON.stringify(finalDobject),1);
             }
        }
         
       }
    },

    facebook_login: function(clkitem) 
    {
        self = this;
        $("#facebook-login").show();
        
        if(clkitem)
        {
            
            if(clkitem.hasClass('save-menu')) return;
            isProd = clkitem.hasClass('common_add_item_cart');
            
            data   = clkitem.data();            
            var qty = 1;
            
            if(data.sandwich == "product") prdChk = true;  else prdChk = false;
            if(isProd && prdChk)
            {
                type       = 'product'; 
                sandwichId = data.sandwich_id;
                                                    
                prev = $(clkitem).parent().prev();
                input = $(prev).find('input[name="spinnerinput"]');
                qtyInp = $(input).val();
                if (qtyInp) qty = qtyInp;
                else qty = 1;
                
            } else if(clkitem.hasClass('reorder-button'))
            {
                
                type       = 'reorder'; 
                sandwichId = data.order;
                
            } else {
                
                        type       = 'sandwich';
                        if(data.sandwich_id)
                        {
                            sandwichId = data.sandwich_id
                        } else {
                    
                            sandwichId = $(clkitem.parent()).find('input[name="hidden_sandwich_id"]').val();
                        }
                 
                    }
        
            uid  = ha.common.getCookie('user_id');
            
            
            if(uid && type && sandwichId)
            { 
                finalDobject = { 'uid' : uid , 'type' : type , 'sandwichId' : sandwichId, "qty" : qty   }

                ha.common.setCookie("addCartData",JSON.stringify(finalDobject),1);
            }
        }
        
    },

    user_login_popup: function() 
    {
        self = this;
        $("#login").hide();
        $("#forgot-form").hide();
        if ($('#login-form-cookie').length > 0) $('#login-form-cookie').show();
        else $("#login-form").show();
    },

    forgot_password_popup: function() 
    {
        self = this;
        $("#login").hide();
        $("#forgot-form").show();
    },

    register_popup: function() 
    {
        self = this;
        $("#login").hide();
        $("#regiter-form").show();
    },

    blank_time: function( d )
    {
        d = new Date(  d );
        d.setHours( 0 );
        d.setMinutes( 0 );
        d.setSeconds( 0 );
        d.setMilliseconds( 0 );
        return d;
    },

 



    


    login: function() 
    {

        $("#login_btn").on('click', function(e) {
            e.preventDefault();
            
            if($('#login-form').css('display') == 'none')
            {
                return;
            }
            
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            $(".register").removeClass('register');
            $(".error_msg").html('');
            var lemail = $("#lemail").val();
            var lpwd = $("#lpwd").val();

            if (lemail == "") {

                $("#lemail").addClass('register').focus();
                $(".error_msg").html("Please enter a valid email address");
            } else if (!filter.test(lemail)) {
                $("#lemail").addClass('register').focus();
                $(".error_msg").html("Please enter a valid email address");


            } else if (lpwd == "") {

                $("#lpwd").addClass('register').focus();
                $(".error_msg").html("Please enter a password");
            } else if (lpwd.length < 6) {
                $("#lpwd").addClass('register').focus();
                $(".error_msg").html("Your password must be at least 6 characters long");
            } else {
                
                
                var target_uri = "";
                if( window.location.href.indexOf("createsandwich") > -1 && window.location.href.indexOf("view") > -1) 
                {
                    target_uri = SITE_URL + 'createsandwich/';
                }
                else
                {
                    target_uri =  window.location.href;
                }

     
                $.post(SITE_URL + "myaccount/loginCheck", $("#loginForm").serialize(),
                    function(data, status) {
                        data = JSON.parse(data);
                
                        if (data.state == "true") {
                            var tempClick = 0;
                            var accounturl;
                            loc = target_uri;
                            arr = loc.split(SITE_URL);
                            accounturl = SITE_URL + arr[1].replace('#', '');
                            
                           if (self.tempSlector != null) {
                                
                                className  = $(self.tempSlector).attr('class');
                                IdName     = $(self.tempSlector).attr('id');
                                text       = $(self.tempSlector).text();
                                if(className) className  = className.toLowerCase(); else className = '';
                                if(IdName) IdName     = IdName.toLowerCase(); else IdName = '';
                                if(text) text       = text.toLowerCase(); else text = '';
                                
                                $(self.tempSlector).trigger('click');
                                self.tempSlector = null;
                                
                             if(className.indexOf('cart') != -1 || IdName.indexOf('cart') != -1 || text.indexOf('cart') != -1)
                              {
                                    var intV = setInterval(function(){ 
                                     
                                    if( ha.sandwichcreator.tmpTransAnim == 1 ){ 
                                      window.location = accounturl;
                                      clearInterval(intV);
                                    }else {
                                        window.location = accounturl; 
                                        clearInterval(intV); 
                                    }
                                },10);
                                 
                              }  else {
                                  $(".popup-wrapper").hide()
                                  window.location = accounturl;
                              }
                                 
                                   
                            } else {
                                $(".popup-wrapper").hide()
                                 window.location = accounturl;
                            }
                                    
                        } else {
                           $(".error_msg").html(data.msg);
                        }
                    }
                );
            }
        });
    },

    cookie_login: function() 
    {
        var self = this;
        $("#login_btn_cookie").on('click', function(e) {
            e.preventDefault();
            
            if($('#login-form-cookie').css('display') == 'none')
            {
                return;
            }

            var lemail = $("#lemail_cookie").val();
            var lpwd = $("#lpwd_cookie").val();


            if (lpwd == "") {
                $("#lpwd_cookie").addClass('register').focus();
                $(".error_msg").html("Please enter a password");
            } else if (lpwd.length < 6) {
                $("#lpwd_cookie").addClass('register').focus();
                $(".error_msg").html("Your password must be at least 6 characters long");
            } else {

                $.post(SITE_URL + "myaccount/loginCheck", $("#loginForm_cookie").serialize(),
                    function(data, status) {
                        data = JSON.parse(data);
                        locationSplit = window.location.href.split("/")
                        if(locationSplit.indexOf("choosedeliveryOrPickup")!=-1){
                            window.location.reload();
                        }

                        if (data.state == "true") {
                            if ($(".create-sandwich-wrapper").length == 2) {
                                $('.create-sandwich-menu').hide();
                                $('.create-sandwich-right-wrapper').hide();
                                $("#login-form-cookie").hide(0, function() {
                                    var accounturl;
                                    loc = window.location.href;
                                    arr = loc.split(SITE_URL);
                                    accounturl = SITE_URL + arr[1].replace('#', '');
                                    window.location = accounturl;
                                });
                            } else {
                                
                                 if (self.tempSlector != null) {
                                
                                className  = $(self.tempSlector).attr('class');
                                IdName     = $(self.tempSlector).attr('id');
                                text       = $(self.tempSlector).text();
                                if(className) className  = className.toLowerCase(); else className = '';
                                if(IdName) IdName     = IdName.toLowerCase(); else IdName = '';
                                if(text) text       = text.toLowerCase(); else text = '';
                                
                                $(self.tempSlector).trigger('click');
                                self.tempSlector = null;
                                
                             if(className.indexOf('cart') != -1 || IdName.indexOf('cart') != -1 || text.indexOf('cart') != -1)
                              {
                                    var intV = setInterval(function(){ 
                                     
                                     if( ha.sandwichcreator.tmpTransAnim == 1 ){ 
                                      window.location = accounturl;
                                      clearInterval(intV);
                                     }
                                },10);
                                 
                              }  else {
                                  $(".popup-wrapper").hide()
                                  window.location = accounturl;
                              }
                                 
                                   
                            } else {
                                $(".popup-wrapper").hide()
                                 window.location = accounturl;
                            }

                            }
                        } else {
                            $(".error_msg").html(data.msg);
                        }
                    }
                );

            }
        });
    },

    setShortCookie: function(cname, cvalue, exdays, path) 
    {
        var d = new Date();
        if(!path) path = "/";
        d.setTime(d.getTime() + ( 30 * 1000 ));
        var expires = "expires="+d.toUTCString();
        var path    =  "path="+path; 
        document.cookie = cname + "=" + cvalue + "; " + expires+";  "+path+";"; 
    },


    setCookie: function(cname, cvalue, exdays, path) 
    {
        
        var d = new Date();
        if(!path) path = "/";
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        var path    =  "path="+path; 
        document.cookie = cname + "=" + cvalue + "; " + expires+";  "+path+";"; 
        
    },

    getCookie: function(cname) 
    {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0){ 
                var string = c.substring(name.length, c.length);
                string = decodeURI(string);
                string = string.replace("+", " ");
                return string;
            }
        }
        return "";
    },

    check_session_state: function() 
    {
        var self = this;
        var result;
        $.ajax({
            async: false,
            type: "POST",
            data: {"user_id": SESSION_ID},
            url: SITE_URL + "myaccount/is_session",
            dataType: 'json',
            success: function(e) {
                result = e;
               
            },
            fail: function( e )
            {
                
            }
        });
        
        if(result.userAvail == 0){
            alert("User has been removed!");
            if($('.login-name-holder a').attr('href')){ 
            window.location.href = $('.login-name-holder a').attr('href');
            }
            return
        }
        
        return result;
    },

    forgot_password: function() 
    {
        $("#forgot_btn").unbind('click').unbind('keyup').on('click keyup', function(e) {
            e.preventDefault();
             var fbtn = $(this);
            $(".register").removeClass('register');
            $(".error_msg").html('');
            var femail = $("#femail").val();
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (femail == "") {

                $("#femail").addClass('register').focus();
                $(".error_msg").html("Please enter a valid email address");

            } else if (!filter.test(femail)) {
                $("#femail").addClass('register').focus();
                $(".error_msg").html("Please enter a valid email address");
            } else {
                fbtn.addClass('disabled');
                $.ajax({
                        type: "POST",
                        url: SITE_URL + "myaccount/forgotPassword",
                        data: $("#forgotForm").serialize()
                    })
                    .done(function(data) {
                        data = JSON.parse(data);
                         
                        if (data.state == false) {
                            $(".error_msg").html(data);
                            $( ".f-link-email-send" ).remove();
                            $( "<p class='f-link-email-send'>"+data.msg+"</p>" ).insertAfter( "#femail" );
                            fbtn.removeClass('disabled');
                        }
                        
                        if (data.state == true) {
                            $( ".f-link-email-send" ).remove();
                            $( "<p class='f-link-email-send'> A link has been sent to your email id to reset the password.</p>" ).insertAfter( "#femail" );
                            fbtn.removeClass('disabled');
                        }

                    });
            }
        });
        $("#femail").on('keypress', function(e) {
            if(e.keyCode==13)
                {
                        e.preventDefault();
                        $(".register").removeClass('register');
                        $(".error_msg").html('');
                        var femail = $("#femail").val();
                        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        if (femail == "") {

                            $("#femail").addClass('register').focus();
                            $(".error_msg").html("Please enter a valid email address");

                        } else if (!filter.test(femail)) {
                            $("#femail").addClass('register').focus();
                            $(".error_msg").html("Please enter a valid email address");
                        } else {
                            $.ajax({
                                    type: "POST",
                                    url: SITE_URL + "myaccount/forgotPassword",
                                    data: $("#forgotForm").serialize()
                                })
                                .done(function(data) {
                                    data = JSON.parse(data);
                                     
                                    if (data.state == false) {
                                        $(".error_msg").html(data);
                                        $( ".f-link-email-send" ).remove();
                                        $( "<p class='f-link-email-send'>"+data.msg+"</p>" ).insertAfter( "#femail" );
                                    }
                                    
                                    if (data.state == true) {
                                        $( ".f-link-email-send" ).remove();
                                        $( "<p class='f-link-email-send'> A link has been sent to your email id to reset the password.</p>" ).insertAfter( "#femail" );
                                    }

                                });
                        }
                }
            });

    },

    create_account: function() 
    {
        $("#create_account").on('click', function(e) {
            e.preventDefault();

            $(".register").removeClass('register');
            $(".rerror_msg").html('');
            var rfname = $("#rfname").val();
            var rlname = $("#rlname").val();
            var remail = $("#remail").val();
            var rpwd = $("#rpwd").val();
            var rphone = $("#rphone").val();
            var rcompany = $("#rcompany").val();
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
            if (rfname == "") {

                $("#rfname").addClass('register').focus();
            } else if (rlname == "") {

                $("#rlname").addClass('register').focus();
            } else if (remail == "") {

                $("#remail").addClass('register').focus();


            } else if (!filter.test(remail)) {
                $("#remail").addClass('register').focus();
                $(".rerror_msg").html("Please enter a valid email address");
            } else if (rpwd == "") {

                $("#rpwd").addClass('register').focus();
            } else if (rpwd.length < 6) {
                $("#rpwd").addClass('register').focus();
                $(".rerror_msg").html("Your password must be at least 6 characters long");
            } else {
                $.ajax({
                    type: "POST",
                    url: SITE_URL + "myaccount/createAccount",
                    data: $("#signupForm").serialize(),
                    success: function(data) {
                        data = JSON.parse(data);
                        console.log(data);
                        if (data.state == false) {
                            $("#regiter-form div.login-inner span.rerror_msg").html(data.msg);
                           
                        } else {
                            if (window.location.href.indexOf("createsandwich") != -1) {
                                var accounturl = SITE_URL + "createsandwich";
                            } else {
                                var accounturl = SITE_URL;
                            }


                            if (self.tempSlector != null) {
                                
                                className  = $(self.tempSlector).attr('class');
                                IdName     = $(self.tempSlector).attr('id');
                                text       = $(self.tempSlector).text();
                                if(className) className  = className.toLowerCase(); else className = '';
                                if(IdName) IdName     = IdName.toLowerCase(); else IdName = '';
                                if(text) text       = text.toLowerCase(); else text = '';
                                
                                $(self.tempSlector).trigger('click');
                                self.tempSlector = null;
                                
                                if(className.indexOf('cart') != -1 || IdName.indexOf('cart') != -1 || text.indexOf('cart') != -1)
                                {
                                    var intV = setInterval(function(){ 
                                    
                                        if( ha.sandwichcreator.tmpTransAnim == 1 ){ 
                                        window.location = accounturl;
                                        clearInterval(intV);
                                        }
                                    },10);
                                 
                                } else {
                                    $(".popup-wrapper").hide()
                                    window.location = accounturl;
                                }
                                 
                                   
                            } else {
                                $(".popup-wrapper").hide()
                                 window.location = accounturl;
                            }

                        }
                    }
                });

            }
        });
        
        $("#rpwd").on('keypress', function(e) {

            if(e.keyCode==13)
            {
                $('#create_account').click();

            }
        });

    },

    load_slides: function() 
    {
        var length_slides = $('#slides div.slides_container div.slide').length;
        if (length_slides > 1) {
            $('#slides').slides({
                preload: false,

                play: 5000,
                pause: 2500,

                hoverPause: true,
                animationStart: function(current) {
                    $('.caption').animate({
                        bottom: -35
                    }, 100);
                    
                },
                animationComplete: function(current) {
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                    
                },
                slidesLoaded: function() {
                    $("div.slides_control div.slide:first").css("left", "960px");
                    $('.caption').animate({
                        bottom: 0
                    }, 200);
                }
            });
        }
    },

    fb_popup: function() 
    {
        if(document.getElementById("fbLogin_alt").addEventListener){
            document.getElementById("fbLogin_alt").addEventListener("click", function(e) {
                e.preventDefault();
                window.open(this.getAttribute('href'), '_blank', 'width=1065,height=637');
            });

            document.getElementById("fbLogin").addEventListener("click", function(e) {
                e.preventDefault();
                window.open(this.getAttribute('href'), '_blank', 'width=1065,height=637');
            });
        }else{
            
            document.getElementById("fbLogin_alt").attachEvent("click", function(e) {
                e.preventDefault();
                window.open(this.getAttribute('href'), '_blank', 'width=1065,height=637');
            });

            document.getElementById("fbLogin").attachEvent("click", function(e) {
                e.preventDefault();
                window.open(this.getAttribute('href'), '_blank', 'width=1065,height=637');
            });
            
        }   
    },

    myaccount_reorder: function() 
    {

        var self = this;
        $("a.reorder-button").click(function() {

            session = ha.common.check_session_state();
            if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                   ha.common.login_popup($(this));
                 } else {
                   ha.common.facebook_login($(this));
                 }
                return;
            } else {

                var order_id = $(this).data("order");
                var reorder_from = $(this).data("from");
                
                pdataArray = [];
                if(reorder_from =='order_history'){
                    $(this).parent().parent().parent().find(".item_details").each(function(){
                        pdataArray.push($(this).data());
                    });
                }else if(reorder_from =='home_page'){
                    $(this).parent().find(".item_details").each(function(){
                        pdataArray.push($(this).data());
                    });
                }else if(reorder_from =='past_order_history'){
                    $(this).parent().find(".item_details_past").each(function(){
                        pdataArray.push($(this).data());
                    });
                }
                 
                if(pdataArray != 'undefined')
                {
                    $.each(pdataArray,function(index, value){
                        $.ajax({
                            type: "POST",
                            url: SITE_URL + 'sandwich/getCurrentprice',
                            data: {'pdata': value},
                            success: function(data) {
                                console.log(data);
                            }
                        })
                    });    
                }
                $.ajax({
                    type: "POST",
                    data: {
                        'order_id': order_id
                    },
                    dataType: 'json',
                    url: SITE_URL + 'myaccount/myOrderReorder/',
                    beforeSend: function() {
                        $("#ajax-loader").show();
                    },
                    complete: function() {
                        $("#ajax-loader").hide();
                    },
                    success: function(e) {
                        window.location = SITE_URL + "checkout/choosedeliveryOrPickup";
                    }
                });

            }
        });
    },

    save_floor:function( floor_date )
    {
        if( floor_date )
        {
            d = new Date( floor_date );
            this.floor_date = d;
        }
    },

    get_floor: function(  )
    {
        return this.floor_date;
    },

    order_view_details: function() 
    {
        $(document).on("click", "div.accout-info-wrapper a.view-details", function() {

             
            session = ha.common.check_session_state();
            if (session.session_state == false) {
                 if(session.facebook_only_user == false){
                   ha.common.login_popup($(this));
                 } else {
                   ha.common.facebook_login($(this));
                 }
                return;
            } else {
                
                var orderId = $(this).data("order");
                var order = $(this).data("order-id");

                pdataArray = [];
                $(this).each(function(){
                        pdataArray.push($(this).data());
                    });
                
                if(pdataArray != 'undefined')
                {
                    $.each(pdataArray,function(index, value){
                        $.ajax({
                            type: "POST",
                            url: SITE_URL + 'sandwich/getCurrentprice',
                            data: {'pdata': value},
                            success: function(data) {
                                console.log(data);
                            }
                        })
                    });    
                }

                $.ajax({
                    type: "POST",
                    data: {
                        'order_id': orderId
                    },
                    url: SITE_URL + 'myaccount/myOrderDetails/',
                    beforeSend: function() {
                       
                        $("#order-detail-pop").removeAttr("style");
                    },
                    complete: function() {

                    },
                    success: function(data) {
                        $("#order-detail-pop").find("div.content").html(data);
                         
                        $("#order-detail-pop").show();
                        $("#order-detail-pop h1 .reorder-button").attr("data-order", order);
                        
                        if ($("#order-detail-pop div.content").length > 0) {
                          
                            $('#order-detail-pop div.content').jScrollPane();
                        }
                    }
                });

            }

        });

    },
    
    updatePickupOrDeliveryInCart : function(deliveryOrPickup)
    {
        var data = {'deliveryOrPickup': deliveryOrPickup};
        
        var last_tip = $("#select_tip_amount").val();       
         var grandTotal= $("#hidden_grand_total").val();
        $.ajax({
            type: "POST",
            data:data,
            url: SITE_URL+'checkout/updatePickupOrDelivery',
            beforeSend: function() {
                
            },
            complete:function() {
                
            },
            success: function(data){                
                data = JSON.parse(data);
                if(data.status == true){                    
                    $(".check-out-left").html(data.message);
                    
                    $.ajax({
                        type: "POST",
                        data:data,
                        url: SITE_URL+'checkout/getCartItemQtyCount/',
                        success: function(e){
                            $("div.cart-top h1").text(e);
                            var subtotal = $("#hidden_sub_total").val();
                            var tip = $("#select_tip_amount").val();                                
                        }
                    });
                    
                    $('select#select_tip_amount option').each( function(  )
                    {
                        if( $(this).val() == last_tip ){
                             $(this).prop( 'selected', true );
                        };
                    } );

                    var newGrandTotal = parseFloat( $( '#hidden_tax' ).val(  ) ) + parseFloat(  $("#hidden_sub_total").val() ) + parseFloat( last_tip );
                    newGrandTotal = newGrandTotal.toFixed( 2 );
                    $("#hidden_grand_total").val( newGrandTotal );
                    $("div.amount-done-wrapper h3").text($("div.grand-total h2").text());       
                    $('span.grand-total').text( newGrandTotal );
                }
            }
        });      
        
    },
    
   
    //~checking if any 3-foot of 6-foot is existing in order. if there is then you get a TRUE else a FALSE.
    checkBreadSimple : function()
    {
        var self = this;
        var cateredBread = false

        $('.bread_type').each(function(){ 
        
            if( $(this).val() > 0) {  
                cateredBread = 1;
            } 
        });
        if( cateredBread == 1 )
            return 1;
        else
            return 0;
    },


    //~checking if any 3-foot of 6-foot is existing in order if any order will be available after 24 hr only. if everything fine this function will return true
    checkBreadType : function()
    {
        
        var self = this;
        var bradTypeAvilable = false
        
        $('.bread_type').each(function(){  
            
            if($(this).val() > 0) {  
                
                bradTypeAvilable = true
            } 
        });
        
 
        
       if(bradTypeAvilable == true){
            self.set24HoursRequired( 1 ); 
           if( $('#radio1:checked').length > 0 ){
                this.set24HoursRequired( 1 );
                alert("Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you! (1)");   
                return 'error';
                
            } else { 
            
            currentTime     = self.getServerTime();
          
            currentTime     = currentTime.split(',');
                        
            $date           = $('.delivery_date').val();
            $time           = $('.timechange select').val();
            
            $date = new Date($date);
            $date = $date.toLocaleDateString();
            
            $dateSplits     = $date.split('/');
            
            $timeSplits     = $time.split(':');
            $month          = parseInt($dateSplits[0]) - 1;
            $dateSelectd    = new Date(parseInt($dateSplits[2]),$month,parseInt($dateSplits[1]),parseInt($timeSplits[0]),parseInt($timeSplits[1]));
            ccdmonth  = parseInt(currentTime[1])-1
            $currentTime    = new Date(parseInt(currentTime[0]), ccdmonth , parseInt(currentTime[2]) , parseInt(currentTime[3]) , parseInt(currentTime[4]) , 0 );
            
            diffMs          =  Math.abs($dateSelectd - $currentTime);
            hours           = Math.floor(diffMs/3600000);
        
             
            if(hours<24){
            /*  this.set24HoursRequired( 1 );
                alert("Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you! (2)");
                return 'error';*/
            }
            else
            {
            
                
            }
            
          }
            
        }  
        else
        {
            this.set24HoursRequired( 0 );
        }
       return true;     
    },
    
   
    getServerTime : function()
    {
        var dateTime;
        var self = this;
        var currentDayHour = 0;
        $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentHour/',
            async: false,
            success: function(data) {
                 $json =  JSON.parse(data);
                
                if($json.date_time_js){
                    dateTime = $json.date_time_js;
                }
                
            }
        });
        return dateTime;
    },
    
    //Check if User have Address or not if not then popup Add New Adress
    checkForUserAdressesAvailable: function()
    {
        
        if($(".select_address option").length <= 2){
            
            $('.select_address').val(0).trigger('change');              
        }
    
    }
    
}

Date.prototype.withoutTime = function () 
{
    var d = new Date(this);
    d.setHours(0, 0, 0, 0, 0);
    return d
}


$(document).ready(function() 
{
    document.cookie = 'redirect' + '=;expires=Thu, 01 Jan 2020 00:00:01 GMT;';
    ha.common.init();
    if(window.location.href.indexOf('sandwich/gallery') != -1 ){
        var el = document.querySelectorAll('ul.menu-listing li img');
        var observer = lozad(el);
        observer.observe();
    }
})

function disableSpecificDates( date )
{
    
    return ha.common.disableSpecificDates( date );
}