ha.sandwichcart = window.ha.sandwichcart || {
	
	init : function(){
		
		//$('.datepicker').datepicker();
		var self = this;
	
		
		  $('.place-order').unbind('click').on('click',function(e){
			  e.preventDefault();
			  
			  var order_string = $('input[name="odstring"]').val();
				if(typeof order_string == "undefined" || !order_string){
					alert("Please check your cart items. It can't be empty.");
					return false;
				}
			  
			  var delvry = -1;
			 
			  if($('.top-buttons-wrapper li .active').text() == "DELIVERY"){
				 delvry  = 1;
			  } else  if($('.top-buttons-wrapper li .active').text() == "PICK-UP"){
				  delvry  = 0;
			  }
			  
			  var order_string = $('input[name="odstring"]').val();
			  var address_id = $('input[name="address_selected"]').val();
			  //var address_id	=$("#changeBilling").val();
			  var pickup_address_id = $('input.pickup_address_id').val();
			  var total        = $('.grand-total').text();
			  var now_or_specific = $('input[name="radiog_lite"]:checked').val();
			  var date_val        = $(".delivery_date").val();
			  var time_val        = $('div.timechange select').val();
			  
			  var coupon_code	  = $('#hidden_discount_id').val();
			  var discount_amount	  = $('#hidden_discount_amount').val();
			  var tip			  = $('#select_tip_amount').val();
			  var sub_total		  = $('#hidden_sub_total').val();
			  var meal_name       = $('input[name="savemealname"]').val();
		
			  var payment_Form    = $('#_payment_form').serializeArray();
			  
			  
				if(delvry < 0){
					alert("Choose Delivery or Pickup address.");
					return false;
				}
				if(delvry == 1 && (typeof address_id == "undefined" || !address_id)){
					alert("Please select Delivery address.");
					return false;
				}
				if(delvry == 0 && (typeof pickup_address_id == "undefined" || !pickup_address_id)){
					alert("Please select Pickup address.");
					return false;
				}				
				
				if(now_or_specific == "specific"){
					
					if(typeof date_val == "undefined" || !date_val ){
						alert("Please select date for order.");
						return false;
					}
					if(typeof time_val == "undefined" || !time_val ){
						alert("Please select date for order.");
						return false;
					}
					var time_only = time_val.replace(" PM", ":00");
					time_only = time_val.replace(" AM", ":00");
					var date_combined = date_val+" " +time_only;
					
					var select_date = new Date(date_combined);
					var current_date = new Date();
					
					if(select_date <  current_date){
						alert("Select future date time.");
						return false;
					}
				}
				
				if(payment_Form[1].value == "" &&  payment_Form[2].value == "" && payment_Form[5].value == "" &&  payment_Form[6].value == "")
                {
					alert("Please select billing");
					return false;
                }
				
				var store_id = 1;
				
				if(delvry==1){
					//order address
					address_id = address_id;					
				}else{
					address_id = pickup_address_id;
					store_id = pickup_address_id;
				}
				
				
				//alert(date_val); return false;
				
				
				$.ajax({
			        type: "POST",
			        data:{ 'mealname' : meal_name,  'store_id' : store_id , 'address_id' : address_id , 'order_item_id' : order_string , 'total' : total , 'tax' : 0 , 'delivery' : delvry, 'date' : date_val, 'time' : time_val ,  'now_or_specific' : now_or_specific , credit_card_id : $('input[name="card_id"]').val(),
			        		'coupon_code':coupon_code,'tip':tip,'sub_total':sub_total , "off_amount":discount_amount , 'credit_card' :payment_Form[1].value, 'cvv':payment_Form[2].value, 'exp_mth' :payment_Form[5].value, 'exp_yr':payment_Form[6].value },
			        beforeSend: function() {
						$("#ajax-loader").show();
					},
					complete:function() {
						$("#ajax-loader").hide();
					},
					dataType:'json',
			        url: SITE_URL+'checkout/place_order/',
						success: function(e) { 
							
									$('.shopping-cart span').text(0); 
									window.location.href = SITE_URL+"checkout/checkout_confirm/"+e.Data;
							
						} 
				});
			  
			 
		  });
		  
		  
			
			$("#addcreidt_card").unbind('click').on('click',function(e){
					 self.add_card();												
				 });
				
		
			
		self.on_select_credit_card();
		self.changeTipValue();
		
	},
	
	
	on_select_credit_card : function(){
	var self = this;
	$('select[name="changeBilling"]').on('change',function(){ 
	var parent_sel = $(this).parent();
	var value      = $(this).val();
	if(value == 0){  $('#billForm')[0].reset(); 
	
    $('#credit-card-details h1').text('ADD NEW CREDIT CARD');
    $('#credit-card-details .checkbox-save-bill').show();
    $('#credit-card-details .add-address').text('ADD');
	$('#credit-card-details').show(); 
	$('.billingDetails').attr('rel','');
	self.add_card();
	} else if(value > 0) {  
		
		

		$.ajax({
	        type: "POST",
	        data: { 'id' : value },
	        dataType : 'json',
	        url: SITE_URL+'checkout/get_billing/',
			success: function(e) { 
			 
				$('.billingDetails').attr('rel', e.Data[0].id); 
			     self.set_billing_values({
			    	'id'   : e.Data[0].id,
					'card_type' : e.Data[0].card_type,
					'card_no' : e.Data[0].card_number,
					'card_name' : e.Data[0].card_name,
					'expiry_month' : e.Data[0].expire_month,
					'expiry_year': e.Data[0].expire_year,
					'card_zip' : e.Data[0].card_zip,
				});
			     
            
			    
			     
			     $('#credit-card-details input[name="cardNo"]').val(e.Data[0].card_number);
			     $('#credit-card-details input[name="cardZip"]').val(e.Data[0].card_zip);
			     $('#credit-card-details select[name="cardMonth"] option[value="'+e.Data[0].expire_month+'"]').prop('selected', true);
			     $('#credit-card-details select[name="cardYear"] option[value="'+e.Data[0].expire_year+'"]').prop('selected', true);
			     $('#credit-card-details h1').text('Confirm your card!');
                 $('#credit-card-details .checkbox-save-bill').hide();
			     $('#credit-card-details .add-address').text('Confirm');
			     $('#credit-card-details input[name="cardCvv"]').val('');
			    if(e.Data[0].card_cvv){ 
			    	self.selected_billing();
			    }  else {
			    	self.add_card();
			    } 
			    
			    
			} 
		});
		
	}
	});
		
	},
	
	add_card : function(){
	var self = this;
	var c_id = null;
	 $('#credit-card-details input[name="cardCvv"]').val('');
	 $('#credit-card-details').show();
	 $('.add-address').unbind('click').on('click',function(e){  
	
		card_name      = $('input[name="cardNickname"]').val();
		card_no        = $('input[name="cardNo"]').val();
		card_cvv       = $('input[name="cardCvv"]').val();
		card_type      = $('select[name="cardType"]').val();
		card_zip       = $('input[name="cardZip"]').val();
		expiry_month   = $('select[name="cardMonth"]').val();
		expiry_year    = $('select[name="cardYear"]').val();
		is_save        = $('input[name="save_billing"]').prop("checked");
		$num_cvv       = 1;
		
		
		
		if( $('.billingDetails').attr('rel') != ""){
			
		$.ajax({
	        type: "POST",
	        data: { 'card_id' : $('.billingDetails').attr('rel') , 'card_cvv' : card_cvv  },
	        dataType : 'json',
	        url: SITE_URL+'checkout/chekcvvValid/',
	        async : false,
	        success: function(output) { 
	         $num_cvv = output.Data[0].count;
			}
		});  
		
		
		
		}
		 if(isNaN(card_no) == true || card_no.length != 16){
			alert("Invalid Card number!");
			return false;
		}
		else if(isNaN(card_cvv) == true || card_cvv.length != 3){
			alert("Invalid card sec.no");
			return false;
		} else if($num_cvv == 0){
			alert("Invalid card not matching with old one");
			return false;
		} else {
			
			//ajax checkout/saveBillinginfo
			//if(is_save == true){  }
			$.ajax({
		        type: "POST",
		        data: {
		            'card_name': '',
		            'card_no': card_no,
		            'card_type': card_type,
		            'expiry_month': expiry_month,
		            'expiry_year': expiry_year,
		            'card_zip': card_zip,
		            'cvv' : card_cvv,
		            'uid': SESSION_ID,
		            'crd_id': $('.billingDetails').attr('rel'),
		            'display_in_menu' : is_save,
		        },
		        async : false,
		        dataType : 'json',
		        url: SITE_URL+'checkout/saveBillinginfo/',
		        beforeSend: function() {
					$("#ajax-loader").show();
				},
				complete:function() {
					$("#ajax-loader").hide();
				},
				success: function(e) { 
					
					console.log(e); 
									
					if($('.billinglist option').length>=2){
						if(! $('.billingDetails').attr('rel') ){
		            	 $('.billinglist').append('<option value="'+e.Data+'">Visa XXXX-XXXX-XXXX-'+card_no.substr(-4)+' </option>');
						}
		            
		            	 $('.billingDetails').attr('rel',e.Data); 
		            	 c_id = e.Data;
					} 
					
		        }
		        	            
		              
			});  
			
			
			self.set_billing_values({
				'id' : c_id,
				'card_type' : card_type,
				'card_no' : card_no,
				'card_cvv' : card_cvv,
				'card_name' : card_name,
				'expiry_month' : expiry_month,
				'expiry_year': expiry_year,
				'card_zip' : card_zip
			});
			

			
			$('.billingDetails, .edit-billing').show();
			if($('.billinglist option').length>=2){ $('.change-billing').show(); }
			self.edit_change_billing(c_id);
			 $('.billinglist').parent().hide();
		}
		
			$(".popup-wrapper").hide();	
			$('.delivery-address').hide();
		 
	 });
	}, 
	
	
	selected_billing : function(){
	
			var self = this;
			  
				card_name      = $('input[name="cardNickname"]').val();
				card_no        = $('input[name="cardNo"]').val();
				card_cvv       = $('input[name="cardCvv"]').val();
				card_type      = $('select[name="cardType"]').val();
				card_zip       = $('input[name="cardZip"]').val();
				expiry_month   = $('select[name="cardMonth"]').val();
				expiry_year    = $('select[name="cardYear"]').val();
				is_save        = $('input[name="save_billing"]').prop("checked");
				
				var billing = $('.billingDetails').attr('rel');
				self.set_billing_values({
					'id' : billing,
					'card_type' : card_type,
					'card_no' : card_no,
					'card_cvv' : card_cvv,
					'card_name' : card_name,
					'expiry_month' : expiry_month,
					'expiry_year': expiry_year,
					'card_zip' : card_zip
				});
				
				$(".popup-wrapper").hide();	
				$('.delivery-address').hide();
				
				$('.billingDetails, .edit-billing').show();
				if($('.billinglist option').length>=2){ $('.change-billing').show(); }
				self.edit_change_billing(billing);
				$('.billinglist').parent().hide();
			
				
		
	},
	
	
	
	set_billing_values : function(data){
		
		$('#_payment_form input[name="_card_type"]').val(data.card_type);
		$('#_payment_form input[name="_card_number"]').val(data.card_no);
		$('#_payment_form input[name="_card_cvv"]').val(data.card_cvv);
		$('#_payment_form input[name="_card_name"]').val(data.card_name);
		$('#_payment_form input[name="_expiry_month"]').val(data.expiry_month);
		$('#_payment_form input[name="_expiry_year"]').val(data.expiry_year);
		$('#_payment_form input[name="_card_zip"]').val(data.card_zip);
		//$('.billingDetails .card_name').text(data.card_name);
		$('.billingDetails .card_no').text(data.card_no.substr(-4));
		$('.billingDetails .card_month').text(data.expiry_month);
		$('.billingDetails .card_year').text(data.expiry_year);
		$('.billingDetails .card_typ').text(data.card_type);
		if(data.id) $('input[name="card_id"]').val(data.id);
		
		//console.log(data);
		
	},

	edit_change_billing : function(editId){
		var self = this;
		$('.change-billing, .edit-billing').unbind('click').on('click',function(e){ 
			
			if($(e.target).hasClass('edit-billing')){
				  
				     $('#credit-card-details h1').text(' EDIT CREDIT CARD');
	                 $('#credit-card-details .checkbox-save-bill').show();
				     $('#credit-card-details .add-address').text('EDIT');
				     $('#credit-card-details input[name="cardCvv"]').val('');
				     $('#credit-card-details').show();
				     
				
			} else if($(e.target).hasClass('change-billing')){ 
				
				$('.billingDetails').hide(); 
				 $('.change-billing , .edit-billing').hide(); 
				$('.billinglist').parent().show();
			}
			
		});
		
		self.add_card();
		
	},
	
	
	changeTipValue: function(){
		
	}
	
}

$(function(){
ha.sandwichcart.init();    
});
