var checkout = {};
var VALID_USER_PICKED_RECENTLY = 0;
var currentDateTime = null;
var currentStoreTimeSlot = null;
checkout =  {
    check_time_and_change_if_needed : function ( force_show )
    {
        var self = this;
        var day_time_minumum = 0;

        // skip = check_if_should_skip(  );
        self.check_user_time_info(  );  
        // check_for_big_enough_bill(  );  
        ha.common.init_date_picker( );

        var timeStateArray = self.pick_time_state( );
        
        var timeStateArray_now = self.pick_time_state_now( );

        self.getSpecificDayForStore(timeStateArray.store_id);
        
        mode = timeStateArray['state'];
        
        ha.common.setTimeStateArray( timeStateArray );      // This saves our state array that we built.
        
        self.checkDateVariables( );                              // This just dumps all the date and time info.

        self.handle_user_time_pick_first_run( timeStateArray  );
        
        timeStateArray = self.last_chance_specific_day( timeStateArray );
        var mesg;
        if (timeStateArray.msg) 
        {
            mesg = timeStateArray.msg;
        }

        while(timeStateArray.closedTodayOverride)// if the store is closed for next days, add days to it.
        {

            timeStateArray = self.last_chance_specific_day( timeStateArray );

        }
        self.limit_datepicker_min_date( mode, timeStateArray );

        timeStateArray.msg = mesg;
        
        set_date_time = self.pick_the_actual_time_to_set_in_datetime_picker( timeStateArray );
        

        if( ha.common.getUserTimeValidated(  ) == 0 )
        {
            set_date_time = self.noon_or_greater( self.get_min_early_entry(  set_date_time ) );   
        }
        else
        {
            timeStateArray['user_validated_time'] = ha.common.getUserTimeValidated(  );
        }
        

        timeStateArray['set_date_time'] = set_date_time;
        timeStateArray_now['set_date_time'] = set_date_time;
        
        timeStateArray = self.figure_out_element_states( timeStateArray_now );

        timeStateArray = self.set_open_floor( timeStateArray ); 

        
        if( self.get_min_early_entry_test(   ) == 1 )
        {
            timeStateArray['nowHidden'] = true;
            timeStateArray['nowDisabled'] = true;
        }

        if( force_show == 1)
        {
            timeStateArray['nowHidden'] = true;
        }

        timeStateArray = self.no_now_on_specific_day( timeStateArray  );

        self.set_date_in_html_element( timeStateArray );
        self.set_time_in_html_element( timeStateArray );

        self.set_cater_time_estimate( timeStateArray );
        
        self.change_html_elements_for_time(  timeStateArray );

        self.limit_select_times_by_date(  timeStateArray.timeFloor );
        self.limit_datepicker_on_closed_days(timeStateArray);
        if (timeStateArray.msg) 
        {
            timeStateArray.msg = mesg;
        }
        ha.common.check_validations_state(  );
        
        self.set_date_in_html_element( timeStateArray );
        self.set_time_in_html_element( timeStateArray );
        ha.common.setTimeStateArray( timeStateArray );      // This saves our state array that we built.
    },

    check_user_time_info : function(  ) 
    {
        if( ha.common.getTimepicked(  )==1 )
        {
            if ( ha.common.getSpecificDate(  ) == "undefined" || !ha.common.getSpecificTime(  ) == "undefined"  || !ha.common.getSpecificDate(  ) || !ha.common.getSpecificTime(  ))
            {
                ha.common.setTimepicked( 0 );
            }
        }
    },

    pick_time_state : function(  ) 
    {
        var self = this;
        var store_closed = self.check_if_closed(  );
        var store_catered = ha.common.checkBreadSimple(  );

        var currentTime = self.getCurrentDateTime(  );
        var storeOpenToday = self.getOpenCloseFromDate( currentTime );
        var storeID = storeOpenToday.store_id;
        
        if( store_closed  == 1 &&   store_catered == 1  )
        {
            timeFloor = self.get_hours_cater_and_closed(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'ClosedCater';
        }

        else if(  store_closed == 1 &&  store_catered  == 0  )
        {
            timeFloor = self.get_hours_closed(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'Closed';
        }

        else if( store_closed == 0 &&  store_catered == 1  )
        {
            timeFloor = self.get_hours_cater(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'Cater';
        }
        else
        {
            state = 'None';
            timeFloor = self.get_hours_regular(  );
            nowDisabled = false;
            nowHidden = false;
        }
        
        $too_early = null;
        if( store_closed == 1 && store_catered == 0 )
        {
            if( self.get_min_early_entry_test( ) == 1 )
            {
                timeFloor = self.get_hours_regular(  );
                $too_early = 1;
            }
        }

        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0, "tooEarly":$too_early, "store_id":storeID };
        return ret;
    },

    pick_time_state_now : function(  ) 
    {
        var self = this;
        var store_closed = self.check_if_closed_now(  );
        var store_catered = ha.common.checkBreadSimple(  );
        var currentTime = self.getCurrentDateTime(  );
        var storeOpenToday = self.getOpenCloseFromDate( currentTime );
        var storeID = storeOpenToday.store_id;

        if( store_closed  == 1 &&   store_catered == 1  )
        {
            timeFloor = self.get_hours_cater_and_closed(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'ClosedCater';
        }

        else if(  store_closed == 1 &&  store_catered  == 0  )
        {
            timeFloor = self.get_hours_closed(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'Closed';
        }

        else if( store_closed == 0 &&  store_catered == 1  )
        {
            timeFloor = self.get_hours_cater(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'Cater';
        }
        else
        {
            state = 'None';
            timeFloor = self.get_hours_regular(  );
            nowDisabled = false;
            nowHidden = false;
        }
        
        $too_early = null;
        if( store_closed == 1 && store_catered == 0 )
        {
            // Too early!
            if( self.get_min_early_entry_test( ) == 1 )
            {
                timeFloor = self.get_hours_regular(  );
                $too_early = 1;
            }
        }

        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0, "tooEarly":$too_early, "store_id":storeID };
        return ret;
    },

    check_if_closed : function(  ) 
    {
        var self = this;
        openTime = ha.common.getOpenTime(  );
        closedTime = ha.common.getClosedTime(  );
        currentTime = self.getCurrentDateTime(  );
        var hour = currentTime.getHours();
        var newDate = currentTime.setHours(hour + 1);
        var browser_check=0;
        storeOpenToday = self.getOpenCloseFromDate( currentTime );
        
        if(storeOpenToday.store_closed_on_day == 1)
        {
            if( self.compareTimes( new Date(newDate), openTime ) == '<')
            {
                return 1;
            }

            if( self.compareTimes( new Date(newDate), closedTime ) == '>' || self.compareTimes( new Date(newDate), closedTime ) == "=" )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }    
    },

    check_if_closed_now : function(  )
    {
        var self = this;
        openTime = ha.common.getOpenTime(  );
        closedTime = ha.common.getClosedTime(  );
        currentTime = self.getCurrentDateTime(  );
        var browser_check = 0;
        storeOpenToday = self.getOpenCloseFromDate( currentTime );
        
        if(storeOpenToday.store_closed_on_day == 1)
        {

            if( self.compareTimes( currentTime, openTime ) == '<')
            {
                return 1;
            }

            if( self.compareTimes( currentTime, closedTime ) == '>' || self.compareTimes( currentTime, closedTime ) == "=" )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }
    },

    get_hours_cater_and_closed :function(  ) 
    {
        floor = this.getTimeFloor_closed_and_cater(  );
        return floor;
    },

    getTimeFloor_closed_and_cater : function( )
    {
        now = this.getCurrentDayHour( );
        newDateMin  = now;
        
        if(  self.check_if_closed(  ) == 1 )
        {
            newDateMin = self.calcFloor(  );
        }
        
        var check_24_hours_too_late = this.test24HoursTooLate(  );
        if( check_24_hours_too_late == 1 )
        {
            newDateMin = this.date_plus_1( newDateMin );
        }
        else
        {
            // 24 hours from now is ok. since we are already tomorrow, set the time then for now, which would be 24 hours. 
            newDateMin = this.set_date_time( newDateMin, now );
        }
        
        newDateMin = this.roundMinutes( newDateMin  );
        return newDateMin;
    },

    get_hours_closed : function(  )
    {
        floor = this.getTimeFloor_closed_only(  );
        return floor;
    },

    getTimeFloor_closed_only : function(  ) 
    {
        // ADD SAT SUN CLOSED HERE.
        newDateMin = this.getCurrentDayHour(  );
        if(  this.check_if_closed(  ) == 1 )
        {
            newDateMin = this.calcFloor(  );
        }
        
        newDateMin = this.roundMinutes( newDateMin  );
        return newDateMin;
    },

    calcFloor : function(  ) 
    {
        current = this.getCurrentDayHour(  );
        newDateMin = this.date_plus_1( current );
        newDateMin = this.set_date_time( newDateMin, ha.common.getOpenTime( ) );
        
        return newDateMin;
    },

    roundMinutes : function( dateObject ) 
    {
        now = dateObject;
        var mins = now.getMinutes();
        var quarterHours = Math.round(mins/15);
        if (quarterHours == 4)
        {
            now.setHours(now.getHours()+1);
        }   
        var rounded = (quarterHours*15)%60;
        now.setMinutes(rounded);
        return now;
    },

    get_hours_cater : function(  ) 
    {
        floor = this.getTimeFloor_cater_only(  );
        return floor;
    },

    getTimeFloor_cater_only : function(  ) 
    {
        newDateMin = this.getCurrentDayHour(  );
        
        if(  this.check_if_closed(  ) == 0 )
        {
            newDateMin = this.add24Hours( newDateMin );
        }
        else
        {
            return this.getTimeFloor_closed_and_cater(  )
        }
        
        newDateMin = this.roundMinutes( newDateMin  );
        return newDateMin;
    },

    add24Hours : function( date ) 
    {
        twenty_four_hours_later = new Date( date.getTime() + 60 * 60 * 24 * 1000 );   
        twenty_four_hours_later = this.roundMinutes( twenty_four_hours_later  );
        return twenty_four_hours_later 
    },

    getTimeFloor_closed_and_cater : function(  ) 
    {
        // ADD SAT SUN CLOSED HERE.
        var self = this;
        now = self.getCurrentDayHour(  );
        newDateMin  = now;
        
        if(  self.check_if_closed(  ) == 1 )
        {
            newDateMin = self.calcFloor(  );
        }
        
        var check_24_hours_too_late = self.test24HoursTooLate(  );
        if( check_24_hours_too_late == 1 )
        {
            newDateMin = self.date_plus_1( newDateMin );
        }
        else
        {
            newDateMin = self.set_date_time( newDateMin, now );
        }
        
        newDateMin = self.roundMinutes( newDateMin  );
        return newDateMin;
    },

    test24HoursTooLate : function(  ) 
    {
        closedTime = ha.common.getClosedTime(  );
        currentTime = this.getCurrentDateTime(  );

        if( this.compareTimes( currentTime, closedTime ) == '>' )
            return 1;
        else
            return 0;
    },

    compareTimes : function( t1, t2 ) 
    {
        if(this.is_empty( t1 ) == 1 )
            return undefined;

        if( this.is_empty( t2 ) == 1 )
            return undefined;

        var temp = t2.toString();
        t2= temp.replace("GMT ","GMT+");
        date1 = new Date( t1 );
        date2 = new Date( t2 );
        
        date1 = new Date( t1 );
        date2 = new Date( t2 );
        
        date_comp_1 = new Date( date1 );
        date_comp_2 = new Date( date1 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2 = this.blank_time( date_comp_2 );

        date_comp_1.setHours( date1.getHours(  ) );
        date_comp_1.setMinutes( date1.getMinutes(  ) );

        date_comp_2.setHours( date2.getHours(  ) );
        date_comp_2.setMinutes( date2.getMinutes(  ) );

        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";

        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        
        else
            result = "?";
        
        return result;
    },

    get_hours_regular : function(  ) 
    {
        return new Date(  );
    },

    get_min_early_entry_test : function(   ) 
    {
        var open_time = ha.common.getOpenTime(  );
        var now = new Date(  );

        vs_open = this.compareTimes( now, open_time  );
        if( vs_open == "<" )
        {
            return 1;
        }
        else
        {
            return 0;
        }
    },

    checkDateVariables: function(  )
    {
        ha.common.dumpTimeParams(  );
    },

    handle_user_time_pick_first_run : function( tsa )
    {
        if( ha.common.getTimepicked(  ) == 0 )
            return;
        
        valid_time_or_zero = this.get_valid_user_time_pick( tsa );

        this.validation_to_cookies( valid_time_or_zero );      
        this.validation_to_global( valid_time_or_zero );
    },

    get_valid_user_time_pick: function( timeStateArray )
    {
        user_date = ha.common.getSpecificDate(  );
        user_time = ha.common.getSpecificTime(  );
        
        if( this.is_empty( user_date ) == 1 || this.is_empty( user_time ) == 1 )
        {
            validation = 0;
        }
        else
        {
            validation = this.validate_user_pick( user_date, user_time, timeStateArray );
        }

        return validation;
    },

    validate_user_pick : function( date, time, timeStateArray ) 
    {
        var self = this;
        cookie_date = self.set_date_time( date, time );
        
        date_cookie     = cookie_date;
        var now_date = self.getCurrentDateTime( );
        var date_open;
        var date_closed;
        if( self.compareDates( date_cookie, new Date(  ) ) == '=' )
        {
            date_open       = ha.common.getOpenTime(  );
            date_closed     = ha.common.getClosedTime(  );
        }
        else
        {
            times = self.getOpenCloseFromDate( date_cookie );
            date_open = times.open_time;
            date_closed = times.close_time;
        }

        date_current    = self.getCurrentDayHour(  );
        date_floor      = timeStateArray.timeFloor;

        if(  self.is_empty( date_floor ) == 1 )
            date_floor = ha.common.getOpenTime(  );

        vs_date_floor  =  self.compareDates( date_cookie, date_floor );
        vs_time_floor  =  self.compareTimes( date_cookie, date_floor );

        if( vs_date_floor == "<" )
            return 0;
        if( vs_date_floor == "=" )
        {
            if( vs_time_floor == "<" )
                return 0;
        }

        vs_current_date = self.compareDates( date_cookie, date_current );
        vs_open_time = self.compareTimes( date_cookie, date_open );
        vs_closed_time = self.compareTimes( date_cookie, date_closed );


        if( vs_current_date == "<" )        // This is before today, so not works. 
            return 0;
        if( vs_open_time == "<" || vs_open_time == "=")
            return 0;
        if( vs_closed_time == ">" || vs_closed_time == "=" )
            return 0;


        var vs_current_time = '';
        if( vs_current_date == "=" )
        {
            vs_current_time = self.compareTimes( date_cookie, date_current );
            if( vs_current_time == "<" )
            {
                return 0;
            }
        }

        if( vs_current_date == "?" || vs_open_time == "?" || vs_closed_time == "?" || vs_current_time == "?")
        {
            return 0;
        }

        return cookie_date;
    },

    compareDates : function( d1, d2 )
    {
        if( this.is_empty( d1 ) == 1 ){
            return undefined;
        }

        if( this.is_empty( d2 ) == 1 )
        {
            return undefined;
        }

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2  = this.blank_time( date_comp_2 );

        date_comp_1.setHours( 0 );
        date_comp_1.setMinutes( 0 );
        date_comp_1.setSeconds( 0 );
        date_comp_1.setMilliseconds( 0 );

        date_comp_2.setHours( 0 );
        date_comp_2.setMinutes( 0 );
        date_comp_2.setSeconds( 0 );
        date_comp_2.setMilliseconds( 0 );

        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";
        
        return result;
    },

    validation_to_cookies : function( valid_time_or_zero ) 
    {
        ha.common.setUserTimeValidated( valid_time_or_zero );
        ha.common.clearUserTimeTemp(  );
    },

    validation_to_global : function( valid_time_or_zero ) 
    {
        if(  valid_time_or_zero != 0 )
            VALID_USER_PICKED_RECENTLY = valid_time_or_zero;
        else
            VALID_USER_PICKED_RECENTLY = 0;
    },

    last_chance_specific_day : function( timeStateArray ) 
    {
        var self = this;
        timeStateArray.closedTodayOverride = 0;
        picked_date_vs_bad_date = self.compareDates( timeStateArray.timeFloor, ha.common.specific_day  );
        
        if( picked_date_vs_bad_date == "=" )
        {
            timeStateArray.closedTodayOverride = 1;
            timeStateArray.timeFloor = self.set_date_time( self.add24Hours( timeStateArray.timeFloor ), ha.common.getOpenTime(  ) );
        }
        
        times = self.getOpenCloseFromDate( timeStateArray.timeFloor );
        
        open_time = times.open_time;
        close_time = times.close_time;
        store_open_today = times.store_closed_on_day;
        
        if(  store_open_today == 0 )
        {
            timeStateArray.timeFloor = self.set_date_time( self.add24Hours( timeStateArray.timeFloor ), ha.common.getOpenTime(  ) );
            
            if( times.store_delivery_off_message == 1 )
            {
                timeStateArray.closedTodayOverride = 1;
                self.set_checkout_message( 'APOLOGIES, DUE TO HIGH ORDER VOLUME, WE CANNOT ACCEPT ADDITIONAL DELIVERY ORDERS AT THIS TIME.' );
                timeStateArray.msg = 'APOLOGIES, DUE TO HIGH ORDER VOLUME, WE CANNOT ACCEPT ADDITIONAL DELIVERY ORDERS AT THIS TIME.' ;
            }
            else if ( times.store_delivery_off_message == 0 )
            {
                self.set_checkout_message( "Sorry, we're currently closed. Please choose a future order time."  );
                timeStateArray.closedTodayOverride = 1; 
                timeStateArray.msg = "Sorry, we're currently closed. Please choose a future order time."  ;
            }

            timeStateArray.day_closed = times.day_of_week;
          
            if( timeStateArray.state == 'Cater' )
                timeStateArray.state = "ClosedCater";
            else
                timeStateArray.state = "Cater";        
        }

        return timeStateArray;
    },

    limit_datepicker_min_date : function( time_state_mode, timeStateArray ) 
    {
        floorDate = timeStateArray.timeFloor;

        if( time_state_mode =='Cater' || time_state_mode == 'ClosedCater' ||  time_state_mode == 'Closed') 
        {
            if( timeStateArray.tooEarly == 1 &&  timeStateArray.closedTodayOverride !== 1)
            {

            }
            else {
                $('.delivery_date').datepicker('option', {minDate: new Date(floorDate)});
            }
        }
        else
        {
            $('.delivery_date').datepicker('option', {minDate: new Date(floorDate)});
        }
    },

    pick_the_actual_time_to_set_in_datetime_picker : function( time_state_array ) 
    {
        var self = this;
        floor_time = time_state_array.timeFloor;
        user_time = ha.common.getUserTimeValidated(  );
        now_time = self.getCurrentDateTime(  );
        
        if( user_time != 0 ){
            return user_time;
        }
        else
        {
            if( floor_time != 0 )
            {
                return self.noon_or_greater( floor_time );
            }
            else 
            {
                return self.noon_or_greater( now_time );
            }
            
        }
    },

    noon_or_greater : function( day_time ) 
    {
        user_time = new Date( day_time );
        noon_time = new Date( day_time );
        return_time = new Date( day_time );
        close_time = ha.common.getClosedTime(  );

        noon_time = this.set_date_time( noon_time, "12:00" );

        user_vs_noon = this.compare_times( user_time, noon_time );
        close_time = ha.common.getClosedTime(  );
        

        if( user_vs_noon == ">" ){
            return_time = this.set_date_time( day_time, user_time );
        }
        else{
            if( this.compareTimes( day_time,close_time ) == ">" )
                return_time = this.set_date_time( day_time, close_time );
            else
                return_time = this.set_date_time( day_time, "12:00" );
        }

        return return_time;
    },

    get_min_early_entry : function(  date_time ) 
    {
        var open_time = ha.common.getOpenTime(  );
        var now = new Date(  date_time );

        if( this.is_empty( date_time ) )
            return open_time;
        else
        {
            vs_open = this.compareTimes( now, open_time  );
            if( vs_open == "<" )
                return open_time;
            else
                return now;
        }
    },

    figure_out_element_states : function( time_state_array ) 
    {
        if(  time_state_array.state == "None" )
        {
            time_state_array.nowDisabled = false;
            if( ha.common.getUserTimeValidated(  ) != 0 )
            {
                time_state_array.nowHidden = true;
            }
            else
            {
                time_state_array.nowHidden = false;
            }
        }
        else
        {
            time_state_array.nowDisabled = true;
            time_state_array.nowHidden = true;

            if( time_state_array.state == 'Cater' || time_state_array.state == 'ClosedCater'  )
            {
                if( !time_state_array.msg )
                    time_state_array.msg = 'Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you!';  
            }

            if( time_state_array.state == 'Closed'  )
            {
                var formated_open_time= this.format_time( this.convert_datetime_to_gmt_format(ha.common.getOpenTime(  )) );
                var formated_close_time= this.format_time( this.convert_datetime_to_gmt_format(ha.common.getClosedTime(  )) );
                var msg = "Sorry, we&apos;re currently closed. <br/>Please choose a future order time." ;

                if( !time_state_array.msg )
                    time_state_array.msg = msg;
            }
        }

        if( time_state_array.nowDisabled == true && time_state_array.msg)
            $('#radio1').data('err-msg', time_state_array.msg ); //setter

        return time_state_array;
    },

    set_open_floor : function( time_state_array ) 
    {
        if(  this.is_empty( time_state_array.timeFloor ) )
            time_state_array.timeFloor = ha.common.getOpenTime(  );
        return time_state_array;
    },

    no_now_on_specific_day : function( timeStateArray ) 
    {
        today_date_vs_bad_date = this.compareDates( new Date( ), ha.common.specific_day  );

        if(  today_date_vs_bad_date == "=" )
        {
            timeStateArray.closedTodayOverride = 1;
            timeStateArray.nowDisabled = true;
            timeStateArray.nowHidden = true;
        }
        return timeStateArray;
    },

    set_date_in_html_element : function( timeStateArray ) 
    {
        this.set_date_html(   timeStateArray.set_date_time );
    },

    set_date_html : function( newDate ) 
    {
        d = new Date( newDate );
        $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', d ));
    },

    set_time_in_html_element : function(  time_state_array ) 
    {   
        this.set_time_html( time_state_array.set_date_time );
    },

    set_time_html : function( newTime ) 
    {
        if( !newTime )
            return;

        var d = new Date( newTime );
        var time_only = this.get_time_from_date(d)
        
        $('.timedropdown option[value="' + time_only + '"]').prop('selected', true);
        
        if( $(".timedropdown").val(  )  == null || $(".timedropdown").val(  )  == "null"){}
    },

    set_cater_time_estimate : function(  timeStateArray ) 
    {
        if(  timeStateArray.state == 'Cater' ||  timeStateArray.state == 'ClosedCater' )
            $('.est-delivery-time').text( '24+ hours' );
    },

    change_html_elements_for_time : function( time_state_array ) 
    {
        if( time_state_array.nowHidden == true )   // HIDE now.
        {
            $("#radio1").prop( "checked", false );      // radio 1 is the now button
            $("#radio2").prop( "checked", true );       // radio 2 is the specific button
            $(".date-time-text").show();    
        }
        else        
        {
            $("#radio1").prop( "checked", true );
            $("#radio2").prop( "checked", false );
            $(".date-time-text").hide();    
        }

        if( time_state_array.nowDisabled == false )   // is active
        {
            
            $('#radio1').data('err-msg', '' ); //setter
            this.setNowDisabled( 0 );
        }
        else
        {
            this.setNowDisabled( 1 );
            $('#radio1').data('err-msg', time_state_array.msg ); //setter
        }

        
        if( ha.common.specific_message && this.compareDates( ha.common.specific_day, new Date(  ) ) == "=" )
        {

            this.set_checkout_message( ha.common.specific_message );
            $("#radio2").data( 'err-msg', ha.common.specific_message  );
        }
        else
        {
            if ( time_state_array.msg )
            {
               $("#radio2").data( 'err-msg', time_state_array.msg  ) ;
               this.set_checkout_message( time_state_array.msg );
            }
            else
            {
                ha.common.clear_checkout_message(  );
            }
        }
    },

    set_checkout_message: function( txt ) 
    {
        $('.checkout-msg-red').remove();
        if( !txt || txt == undefined )
            return;
        var msg = "<span class='checkout-msg-red'>"+txt+"</span>";
        $("div.delivery-details-wrapper div.date-time > p").after(msg);
    },

    checkTimingsRealtedConditions:  function( selectedDate )
    {
        var self = this;

        if( selectedDate == undefined )
        {
            selectedDate = $( ".delivery_date" ).val(  );
        }

        //var today_Date = new Date ( self.getCurrentDateFromServer(  ) );
        var today_Date = (currentDateTime != null)?new Date (currentDateTime):new Date ( self.getCurrentDateFromServer(  ) );
        
        var today_DayOfWeek = today_Date.getDayName(  );
      
        var is_delivery = -1;

        if ($('.top-buttons-wrapper li .active').text() == "DELIVERY") {
            is_delivery = 1;
            ha.common.setDelivery( 1 );
        } else if ($('.top-buttons-wrapper li .active').text() == "PICK-UP") {
            is_delivery = 0;
            ha.common.setDelivery( 0 );
        }   
        
        var is_delivery = 1;    
        
      
        if ( is_delivery == 0 )
        {
             deliveryOrPickup = "pickup";
            var pickupStoreId = $("input.pickup_address_id").val();
            var address_id = pickupStoreId;
        }
        else
        {
            deliveryOrPickup = "delivery";
            var deliveryAddressId = $("input.address_selected").val();
            var address_id = deliveryAddressId;
        }
            
            
        var store_timeslot_response = this.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
        
        if(store_timeslot_response != null)
        {
            store_timeslot_response_text = JSON.parse( store_timeslot_response.responseText  );
        
            ha.common.setOpenTime( self.set_date_time(  today_Date,  store_timeslot_response_text['open'] ) );
            ha.common.setClosedTime( self.set_date_time(  today_Date,  store_timeslot_response_text['close'] ) );

            ha.common.setOpenHour( store_timeslot_response_text['open'] );
            ha.common.setCloseHour( store_timeslot_response_text['close'] );
            
            if( store_timeslot_response_text == false || store_timeslot_response_text == null ){
                return false;
            };

            $("#hidden_store_id").val( store_timeslot_response.store_id );
        }
             
    }, 

    limit_select_times_by_date : function( floor_date_time ) 
    {
        var self = this;
        this.getTimeAvilableSlots(  );
        var picked_date = new Date( $( ".delivery_date" ).val(  ) );
        
        var now_date = this.getCurrentDateTime( );
        var open_time;
        var close_time;
        
        var times;
        var store_open_today=1;
       
        if( self.compareDates( picked_date, new Date(  ) ) == '=' )
        {
           open_time = ha.common.getOpenTime(  );
           close_time = ha.common.getClosedTime(  );
           
            if( self.compareTimes(  now_date, open_time ) == ">" )
            {
                var hour = now_date.getHours();
                var newDate = now_date.setHours(hour + 1);
                open_time = newDate;
            }
            else if(self.compareTimes(  now_date, open_time ) == "<")
            {
                open_time = open_time.replace("GMT ", "GMT+");
                open_time = new Date(open_time);
            }
        }
        else
        {
            times = self.getOpenCloseFromDate( picked_date );
            open_time = times.open_time;
            close_time = times.close_time;
            store_open_today = times.store_closed_on_day;
        }

        if(  store_open_today == 0 )
        {
            if( times.store_delivery_off_message == 1 )
            {
                self.set_checkout_message( 'APOLOGIES, DUE TO HIGH ORDER VOLUME, WE CANNOT ACCEPT ADDITIONAL DELIVERY ORDERS AT THIS TIME.' );
            }
            else if ( times.store_delivery_off_message == 0 )
            {
                self.set_checkout_message( "Sorry, we're currently closed. Please choose a future order time." );
            }
            ha.common.set_validation_blocker(  );
        }
        else
        {
            ha.common.clear_validation_blocker(  );
        }

        var timeStateArray  = ha.common.getTimeStateArray( );
            
        if( floor_date_time == undefined ||  floor_date_time == 'undefined'  ||  floor_date_time == ''  )
        {
            if( self.compare_dates( picked_date, timeStateArray.timeFloor ) == "=" )
            {
                floor_date_time =  timeStateArray.timeFloor;   
            }
            else
            {
                floor_date_time = self.set_date_time( picked_date, self.format_time( open_time )  );
            }
        }
        else
        {
            if( timeStateArray.state == 'Cater' ||  timeStateArray.state == 'ClosedCater' )
            {
                // because if we are using catering, then we can't use the "getTimeStateArray" open time as a floor.  But we have already calc'd a floor so...
                
            }
            else
            {
                floor_date_time = self.set_date_time( floor_date_time, self.format_time( open_time )  );
            }
        }

        var floor_date_time = new Date( floor_date_time );      

        if( picked_date.getHours(  ) == 0 )
        {
            picked_date = self.set_date_time( picked_date, self.format_time( floor_date_time )  );
        }

        if(  self.is_empty(floor_date_time) == 1 )
        {           
            return;
        }

        if( self.compareDates( picked_date , floor_date_time ) == "<" )
        {
            self.set_html_date( floor_date_time );
            self.limit_select_times_by_date( floor_date_time  );
            picked_date = $( ".delivery_date" ).val(  );
        }
        else if( this.compareDates( picked_date , floor_date_time ) == "=" )
        {
            $(".timedropdown option").each(function()
            {
                var index_time = $(this).val();

                var index_date_time = self.set_date_time( picked_date, index_time );
                var vs_index_time_and_floor_time = self.compareTimes( index_date_time, floor_date_time );
                var vs_index_time_and_open_time = self.compareTimes( index_date_time, open_time );
                
                if( vs_index_time_and_floor_time == "<" )
                {
                    $( this ).remove(  );
                }

                if( vs_index_time_and_open_time == "<" )
                {
                    $( this ).remove(  );
                }


                var vs_index_time_and_closed_time = self.compareTimes( index_date_time, close_time );
                
                if( vs_index_time_and_closed_time == ">" )
                {
                    $( this ).remove(  );
                }
            });
        }

        else if( self.compareDates( picked_date , floor_date_time ) == ">" )
        {
            $(".timedropdown option").each(function()
            {
                var index_time = $(this).val();
                var index_date_time = self.set_date_time(picked_date, index_time );
                
                
                vs_index_time_and_open_time = self.compareTimes( index_date_time, open_time );
                
                if( vs_index_time_and_open_time == "<" )
                {
                    $( this ).remove(  );
                }

                vs_index_time_and_closed_time = self.compareTimes( index_date_time, close_time );
                if( vs_index_time_and_closed_time == ">" )
                {
                    $( this ).remove(  );
                }
            });            
        }

        $( 'select.timedropdown option' ).each( function(  )
        {
            if( $($(this)).val(  ) == "12:00" )
                $($(this)).prop( 'selected',true );
        } );       
    },

    limit_datepicker_on_closed_days : function(timeStateArray) 
    {
        var self = this;
        var store_id = timeStateArray.store_id;
        if(store_id){
            this.getSpecificDayForStore(store_id);
        }
        

        $.ajax({
            type: "POST",
            data: {
                "store_id": store_id
            },
            dataType:'json',
            url: SITE_URL + 'checkout/get_storeClosed_days',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) 
            {
                if(e != false)
                {
                    $('.delivery_date').datepicker('option',
                    {
                        beforeShowDay: function(d) 
                        {
                            var is_specficHoliday = self.disableSpecificDates(d);
                            if(is_specficHoliday){
                                var day = d.getDay();
                                return [e.indexOf(day) == -1];
                            }else{
                                return [false];
                            }
                            
                        }
                    });
                }
            }
        });
    },

    getCurrentDayHour: function(  ) 
    {
        var self = this;
        //date = ha.common.getCurrentDateFromServer(  );
        date = (currentDateTime != null)?currentDateTime:self.getCurrentDateFromServer(  );
        
        date = new Date( date );

        return date;
    },
    getCurrentDateTime: function(  ) 
    {
        date = this.getCurrentDayHour(  );
        return date;
    },

    getCurrentDateFromServer: function(  )
    {
        $.ajax({
                type: "POST",
                url: SITE_URL + 'checkout/getCurrentHour/',
                async: false,                   
                success: function(data)
                {
                    currentDayHour = JSON.parse(data);
                }
            });
            var currentHour = parseInt(currentDayHour.hour);
            var currentDay = parseInt(currentDayHour.day);
            var currentDate = currentDayHour.date_time_js;
            
            
            currentDate = currentDate.split(",");
            var current_date = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);
            currentDateTime = current_date;
            return current_date;
    },

    get_store_timeslot_ajax: function( dayOfWeekName, address_id, delivery_pickup )
    {
        if(address_id)
        {
            currentStoreTimeSlot =  $.ajax({
                type: "POST",
                url: SITE_URL + 'checkout/getCurrentStoreTimeSlot/',
                async: false,
                data: {
                    deliveryOrPickup: delivery_pickup,
                    id: address_id,
                    day: dayOfWeekName
                },
                beforeSend: function() {
                    $("#ajax-loader").show();
                },
                complete: function() {
                    $("#ajax-loader").hide();
                },
                success: function( ajax_response ) {
                    if(ajax_response == "false")
                    {
                        return false;
                    }
                    else
                    {
                        store_timing = JSON.parse(ajax_response);
                        self.timeslot_information = store_timing ;
                        return   store_timing;
                    }
                    
                }
            });
            return currentStoreTimeSlot ;
        }
        else
        {
            return currentStoreTimeSlot ;
        }
    },

    date_plus_1 : function( d ) 
    {
        var date = new Date( d );
        date.setDate( date.getDate() + 1 );
        return date;
    },

    is_empty: function( thing ) 
    {
        if( thing == 'undefined'    ||
            thing == undefined      ||
            thing == null           ||
            thing == ''             ||
            thing === false )
            return 1;
        else
            return 0;
    },

    blank_time: function( d ) 
    {
        d = new Date(  d );
        d.setHours( 0 );
        d.setMinutes( 0 );
        d.setSeconds( 0 );
        d.setMilliseconds( 0 );
        return d;
    },

    getOpenCloseFromDate: function( date ) // get open close store time from timefloor // done
    {
        date = new Date( date );
        var today_DayOfWeek = date.getDayName(  );

        var deliveryOrPickup = "delivery";
        var deliveryAddressId = $("input.address_selected").val();
        var address_id = deliveryAddressId;
        var store_timeslot_response = this.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
        var ret = {};
        if(store_timeslot_response != null)
        {
            store_timeslot_response = JSON.parse( store_timeslot_response.responseText);
            //console.log(store_timeslot_response);

            var open_time =  store_timeslot_response['open'] ;
            var close_time =  store_timeslot_response['close'];
            var open_d = this.set_date_time(  date,  open_time );
            var close_d = this.set_date_time(  date,  close_time );
       

            if(  store_timeslot_response['delivery_active'] == 1 ){     // active 1 means it's off 
                store_delivery_off_message =  1;
            }
            else{
                store_delivery_off_message = 0;
            }

            ret = {
                    "day_of_week" :store_timeslot_response['day'],
                    "store_closed_on_day":  store_timeslot_response['store_active'],
                    "store_delivery_off_message": store_delivery_off_message,
                    "open_time": open_d,
                    "close_time": close_d,
                    "store_id":store_timeslot_response['store_id']
            };
        }
        return ret;
    },

    set_date_time : function( date_passed, time_passed  )
    {
        if( this.is_empty(time_passed ) || time_passed == null )
        {
            return date_passed;
        }

        if( typeof(  time_passed  ) == "object" ) 
        {
            time_string_composer = this.pad( time_passed.getHours(  ) , 2 ) + ":" + this.pad( time_passed.getMinutes(  ), 2 );
            time_passed = time_string_composer;
        }
        else
        {
            if(  time_passed == undefined  ){
                time_passed = '12:00';
            }
            if(  !time_passed  ){
                time_passed = '12:00';
            }
            if(  time_passed == undefined  ){
                time_passed = '12:00';
            }
            if(  time_passed == 'null'  ){
                time_passed = '12:00';
            }

            var timeRegex = /([01]\d|2[0-3]):([0-5]\d)/;
            var match = timeRegex.exec( time_passed );
            time_passed = match[0];
        }
        time_split = time_passed.split(/\:|\-/g);
        
        new_day = new Date( date_passed );
        new_day.setSeconds( 0 );
        new_day.setMilliseconds( 0 );

        new_day.setHours( time_split[0] );
        new_day.setMinutes( time_split[1] );

        return new_day;
    },

    compare_times: function( d1, d2 ) 
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date1 = new Date( d1 );
        date2 = new Date( d2 );

        date_comp_1 = new Date( date1 );
        date_comp_2 = new Date( date1 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2 = this.blank_time( date_comp_2 );

        date_comp_1.setHours( date1.getHours(  ) );
        date_comp_1.setMinutes( date1.getMinutes(  ) );
        date_comp_1.setSeconds( date1.getMinutes(  ) );
        date_comp_1.setMilliseconds( date1.getMinutes(  ) );

        date_comp_2.setHours( date2.getHours(  ) );
        date_comp_2.setMinutes( date2.getMinutes(  ) );
        date_comp_2.setSeconds( date2.getMinutes(  ) );
        date_comp_2.setMilliseconds( date2.getMinutes(  ) );

        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) == date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

        return result;
    },

    format_time: function( d )  
    {
        self = this;
        if( this.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var time_string = this.pad( date.getHours(), 2) + ":" + this.pad( date.getMinutes(), 2);
        
        return time_string;
    },

    convert_datetime_to_gmt_format : function(passed_time) 
    {
       var ptemp = passed_time.toString();
       passed_time= passed_time.replace("GMT ","GMT+");
       return passed_time;
    },

    get_time_from_date : function( date_time ) 
    {
        if( typeof( date_time ) == "object" )
        {

            result = ( ha.common.pad( date_time.getHours(  ), 2 ) + ":" + ha.common.pad(date_time.getMinutes(  ),2) );
            return result;
        }
        else
        {
            var timeRegex = /([01]\d|2[0-3]):([0-5]\d)/;
            var match = timeRegex.exec( date_time );
            time = match[0];
            return time;
        }
    },

    setNowDisabled : function( trueToDisableNow ) //done
    {
        ha.common.setCookie( "trueToDisableNow", trueToDisableNow );
    },

    getTimeAvilableSlots: function(oBj)  
    {
        time_list = this.get_all_possible_times(  );
         $(".timechange").html("");
         current_value = $(".timechange").val();
        $(".timechange").html(
            '<select class="timedropdown">' +
            time_list +
            "</select>"
            );
        this.set_time_html( current_value );

        return;
    },

    get_all_possible_times: function(  ) 
    {
        buffer = '';
        for( hour = 8; hour < 24; hour ++ )
        {
            for( minute = 0; minute <60; minute = minute + 15  )
            {
                time_text = ha.common.pad( hour, 2 ) + ":" + this.pad( minute, 2);
                time_new = this.tConvert( time_text );
                buffer = buffer + '<option value="' + time_text  + '">' +  time_new +  '</option>';
            }
        }
        return buffer;
    },

    tConvert: function (time) 
    {
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) 
        { 
            
          time = time.slice (1);  // Remove full string match value
          time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
          time[0] = +time[0] % 12 || 12; // Adjust hours
        }
       
        return time[0] + ":" + time[2] + time[5];
    },

    compare_dates: function( d1, d2 ) 
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2  = this.blank_time( date_comp_2 );


        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

        return result;
    },

    set_html_date: function( date )  
    {
        d = new Date(  date  );
        $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', d));
    },

    getSpecificDayForStore: function( storeId )
    {
        var self = this;
        $.ajax({
            type: "POST",
            dataType: 'json',
            async: false,
            data: {
                "storeId": storeId
            },
            url: SITE_URL + 'checkout/getSpecificDayForStore',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) {
                var message  = e.message;
               
                if(message !='')
                { 
                    self.specific_day = e.specific_day;
                    self.specific_message = e.specific_message;
                   
                    if( !self.specific_day )
                    {
                        self.specific_message = '';
                    }
                }
            }
        });
    },

    disableSpecificDates : function( date )
    {
        if(  ha.common.specific_day  == "" )
        {
            return 1;
        }

        var bad_date = new Date(  ha.common.specific_day );


        var m_bad = bad_date.getMonth();
        var d_bad = bad_date.getDate();
        var y_bad = bad_date.getFullYear();
        

        var m = date.getMonth();
        var d = date.getDate();
        var y = date.getFullYear();
        
         // First convert the date in to the mm-dd-yyyy format 
         // Take note that we will increment the month count by 1 
         var currentdate = (m + 1) + '-' + d + '-' + y ;
         var bad_date_formatted =  (m_bad + 1) + '-' + d_bad + '-' + y_bad ;
          // We will now check if the date belongs to disableddates array 
          if( currentdate == bad_date_formatted  )
          {
            return 0;
        }
        else
        {
            return 1;
        }
    },

    pad : function(num, size) 
    {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }
}

$(document).ready(function () 
{
    VALID_USER_PICKED_RECENTLY = 0;
});