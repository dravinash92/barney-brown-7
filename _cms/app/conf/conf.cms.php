<?php

if($_SERVER['REQUEST_SCHEME'])
  $scheme = $_SERVER['REQUEST_SCHEME']; 	
else 
  $scheme = 'http';
 
//Directories
define('DIR_BASE', 'app/base/');
define('DIR_CONF', 'app/conf/');
define('DIR_CONTROLLER', 'app/controller/');
define('DIR_DB', 'app/db/');
define('DIR_MODEL', 'app/model/');
define('DIR_SMARTY', 'app/smarty/');
define('DIR_THEME', 'app/theme/');
define('PROJECT_BASE', '_cms/');
define('SITE_URL', $scheme.'://cms.ha/');
define('LOG_IN_TIME', 86400); //in seconds 1800 = 30 min (max inactive time user allowed to log in)
define('TITLE','BARNEY BROWN');

define('CMS_URL','http://cms.ha/');
define('API_URL','http://api.ha/');
define('API_USERNAME','hashburyuser');
define('API_PASSWORD','hashburypass');

define('ADMIN_URL','http://admin.ha/');
define('MOBILE_URL','http://m.ha/');
//facebook api config..

define('DIR_FBAPI','app/FB_API/');
 define('FB_APP_ID','557060384864429'); //unque app id from Facebook app
define('FB_APP_SECRET','40b8c2c1383e883f4e660d3a07fec320'); //unque app secret key from Facebook app
// define('FB_REDIR_URL','http://www.barneybrown.com/facebook/data/'); //url to which facbook redirect after login
define('FB_REDIR_URL','http://cms.ha/facebook/data/'); //url to which facbook redirect after login
define('STORE_START_TIME','10:00');
define('STORE_END_TIME','16:00');

//define('DELIVERY_FIRST_ORDER','Delivering to Midtown Manhattan between 23rd Street and 59th Street.');
define('DELIVERY_FIRST_ORDER','Barney Brown currently delivers in Manhattan from 14th Street to 42nd Street');
define('PICKUP_FIRST_ORDER','235 East 43rd Street, Btw 6th and 7th Ave');
define('SANDWICH_UPLOAD_DIR','../sandwich_uploads/');
define('SANDWICH_IMAGE_PATH','http://cms.ha/sandwich_uploads/');
define('SALAD_IMAGE_PATH','http://admin.ha/upload/products/');
define("ENVIRONMENT", "STAGING");
define('MAX_TIP', 100);
define("MIN_TIP", 2);
define("TAX",  0.08875);
define("BASE_FARE",7);

$metatag = array();

$metatag['home']['HeaderTag'] = 'Barney Brown - Custom Sandwich Delivery & Catering NYC';
$metatag['home']['Description'] = 'Barney Brown specializes in premium sandwich delivery and catering in New York City. Create the perfect custom sandwich from a selection of over 70 ingredients.';
$metatag['home']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand.';


$metatag['menu']['HeaderTag'] = 'Barney Brown – My Menu';
$metatag['menu']['Description'] = 'Barney Brown, Custom Sandwich Shop NYC. Save your own or other users’ sandwiches to your menu for easy future reordering. Login with Facebook, and see your friends’ menus and creations.';
$metatag['menu']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, menu, personalization, easy, reordering, saved creations.';

$metatag['createsandwich']['HeaderTag'] = 'Barney Brown – Create a Sandwich';
$metatag['createsandwich']['Description'] = 'Barney Brown, Custom Sandwich Shop NYC. Create your own sandwich using our sandwich customizer, which includes a selection of over 70 ingredients. See your sandwich come to life while you’re building it, name it, and save it for easy future reordering.';
$metatag['createsandwich']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, ingredients, visual builder, name generator, local breads.';

$metatag['sandwich']['HeaderTag'] = 'Barney Brown – Sandwich Gallery';
$metatag['sandwich']['Description'] = 'Barney Brown, Custom Sandwich Shop NYC. Our user-generated gallery of sandwich creations features thousands of creations submitted by Barney Brown customers. Sort sandwiches by likes or filter by ingredients.';
$metatag['sandwich']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, shared creations, likes, facebook, sort, sorting, open source, gallery, library.';

$metatag['catering']['HeaderTag'] = 'Barney Brown – Catering';
$metatag['catering']['Description'] = 'A delectable selection of sandwich platters, bagged lunches, large format salads, sides, and drinks for your next catering event.';
$metatag['catering']['Keywords'] = 'A delectable selection of sandwich platters, bagged lunches, large format salads, sides, and drinks for your next catering event.';

$metatag['checkout']['HeaderTag'] = 'Barney Brown – Checkout';
$metatag['checkout']['Description'] = 'Use our one-page checkout for an easy hassle-free ordering experience.';
$metatag['checkout']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, checkout, easy, reordering.';

$metatag['site']['checkout_confirm']['HeaderTag'] = 'Barney Brown - Order Confirmation';
$metatag['site']['checkout_confirm']['Description'] = 'Thank you for choosing Barney Brown!';

$metatag['myaccount']['HeaderTag'] = 'Barney Brown – My Account';
$metatag['myaccount']['Description'] = 'Barney Brown, Custom Sandwich Shop NYC. Take advantage of our account features, which allow you to view past orders, save delivery addresses, save billing info, and much more! ';
$metatag['myaccount']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, my account, order history, past orders, saved addressed, saved billing.';

$metatag['site']['aboutus']['HeaderTag'] = 'Barney Brown – About Us';
$metatag['site']['aboutus']['Description'] = "Barney Brown is the first socially-integrated sandwich platform of its kind. From our visual sandwich customizer to our gallery of thousands of customer creations to choose from, you won't find another sandwich shopping experience like it.";
$metatag['site']['aboutus']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand.';

$metatag['site']['jobs']['HeaderTag'] = 'Barney Brown – Jobs';
$metatag['site']['jobs']['Description'] = 'Barney Brown, Custom Sandwich Shop NYC. Barney Brown is always on the hunt for new members to join our team of weirdos! Apply for Delivery Rider, Sandwich Master, Catering Coordinator, and General Manager Positions. ';
$metatag['site']['jobs']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, jobs, positions, apply, delivery rider, sandwich master, catering coordinator, general manager.';

$metatag['site']['contact']['HeaderTag'] = 'Barney Brown – Contact';
$metatag['site']['contact']['Description'] = 'Barney Brown, Custom Sandwich Shop NYC. The Barney Brown Team can be contacted any day between 10:00AM - 4:00PM regarding all general inquiries, catering inquiries, suggestions, or technical support.';
$metatag['site']['contact']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, contact, phone number, inquiries, report, questions, general, catering, suggestions, support, technical, bug.';

$metatag['site']['termsofuse']['HeaderTag'] = 'Barney Brown – Terms of Use';
$metatag['site']['termsofuse']['Description'] = 'Barney Brown, Custom Sandwich Shop NYC. Questions about our Terms of Use? Check out the Barney Brown Terms of Use page which explains it all!';
$metatag['site']['termsofuse']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, terms of use.';

$metatag['site']['privacypolicy']['HeaderTag'] = 'Barney Brown – Privacy Policy';
$metatag['site']['privacypolicy']['Description'] = 'Barney Brown, Custom Sandwich Shop NYC. Questions about our Privacy Policy? Check out the Barney Brown Privacy Policy page which explains it all!';
$metatag['site']['privacypolicy']['Keywords'] = 'Barney Brown, sandwich, sandwiches, best, nyc, new york, manhattan, delivery, custom, customized, build your own, on demand, privacy, policy.';


$metatag['site']['sandwichMenu']['HeaderTag'] = 'Barney Brown - Sandwiches On Demand
';
$metatag['site']['sandwichMenu']['Description'] = 'Customize a sandwich, browse through thousands of customer creations, see your friends’ sandwiches, and easily save them for future reordering.';

$metatag['site']['savedSandwiches']['HeaderTag'] = 'Barney Brown – My Saved Sandwiches';
$metatag['site']['savedSandwiches']['Description'] = 'Easily name and save sandwiches for future reordering. Use our app for 2-click reordering.';

$metatag['site']['featuredSandwiches']['HeaderTag'] = 'Barney Brown – Featured Sandwiches';
$metatag['site']['featuredSandwiches']['Description'] = 'A monthly spotlight of 10 customer sandwich creations chosen from our gallery of thousands of user-submitted sandwiches.';

$metatag['site']['friendSandwiches']['HeaderTag'] = 'Barney Brown – Friends’ Sandwiches';
$metatag['site']['friendSandwiches']['Description'] = '';

$metatag['site']['friendsMenu']['HeaderTag'] = 'Barney Brown – Friends’ Sandwiches';
$metatag['site']['friendsMenu']['Description'] = "Connect with Facebook to see your friends' sandwiches and easily save them to your menu for future reordering.";

$metatag['site']['salads']['HeaderTag'] = 'Barney Brown – Salads';
$metatag['site']['salads']['Description'] = 'Choose from a selection of fresh salads prepared daily.';

$metatag['site']['snacks']['HeaderTag'] = 'Barney Brown – Snacks';
$metatag['site']['snacks']['Description'] = 'A delightful assortment of snacks including fruits, baked goods, pickles, chips, and more.';

$metatag['site']['drinks']['HeaderTag'] = 'Barney Brown – Drinks';
$metatag['site']['drinks']['Description'] = 'A selection of classic sodas, teas, and sparkling waters to choose from.';

$metatag['site']['gallery']['HeaderTag'] = 'Barney Brown - Sandwich Gallery';
$metatag['site']['gallery']['Description'] = 'An ever-growing collection of thousands of customer sandwich creations. Sort by bread, protein, cheese, topping, or condiment to find the perfect sandwich for you.';