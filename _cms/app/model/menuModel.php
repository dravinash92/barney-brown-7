<?php
class MenuModel  extends Model
{

	public $db;
	 //database connection object
	
	public function __construct()
	{
		$this->db = parent::__construct();  
	}
	
	public function getTotalSanwichMenuCount($uid){
		$get_url     = API_URL.'sandwich/getTotalSanwichMenuCount/';
		$json        = $this->receive_data($get_url,array('uid'=>$uid));
		return  json_decode($json);
	}
	
	public function get_user_sanwich_data($data){
		 $get_url     = API_URL.'sandwich/get_menu_data/';
		$json        = $this->receive_data($get_url,$data);
		return $json;
	}
	
	public function get_user($id){
	    $get_url     = API_URL.'user/get/';
	    $json        = $this->receive_data($get_url,array('user_id'=>$id));
		$data        = json_decode($json);
        if(count($data->Data)>0){ 
        $lastname    =  $data->Data[0]->last_name;
        return $name   = $data->Data[0]->first_name.' '.$lastname[0]; }
        else  return '';
	}
	
	
	public function get_sandwich_like_count($id)
	{
		$get_url     = API_URL.'sandwich/get_sandwich_like_count/';
		$json        = $this->receive_data($get_url,array('id'=>$id));
		return  json_decode($json);
	}
	
	public function get_sandwich_like($uid=null,$id=null)
	{
		$data_array = array('uid'=>$uid,'id'=>$id);
		$get_url     = API_URL.'sandwich/get_sandwich_like/';
		$json        = $this->receive_data($get_url,$data_array);
		return  json_decode($json);
	}	
	
	function  process_user_data($data)
	{
		$self  = $this;
		
		    array_walk($data,function(&$value,$key) use($self){
			$value = get_object_vars($value);
			$data  = json_decode($value['sandwich_data']);
			if(isset($data)){
			$array = get_object_vars($data);
			array_walk($array,function(&$val,$key){
				$val = get_object_vars($val);
			}); 
			} else { $array = array();  }
			
			$like = $self->get_sandwich_like(@$_SESSION['uid'],$value['id']);
			$like_counts  = $self->get_sandwich_like_count($value['id']);
			if(isset($like->Data[0])) $like_id = $like->Data[0]->id; else $like_id = 0;
			 
			$bread     = @$array['BREAD']['item_name'][0];
			$protein   = $self->getCombinedString(@$array['PROTEIN']['item_qty']); 
			$cheese    = $self->getCombinedString(@$array['CHEESE']['item_qty']);  
			$topping   = $self->getCombinedString(@$array['TOPPINGS']['item_qty']); 
			$condiment = $self->getCombinedString(@$array['CONDIMENTS']['item_qty']);

			// item ids 
			$bread_id     = $array['BREAD']['item_id'][0];
			$protein_id   = implode(', ',$array['PROTEIN']['item_id']); 
			$cheese_id    = implode(', ',$array['CHEESE']['item_id']);  
			$topping_id   = implode(', ',$array['TOPPINGS']['item_id']); 
			$condiment_id = implode(', ',$array['CONDIMENTS']['item_id']);  
			    
		    $desc      = $bread.", ".$protein.", ".$cheese.", ".$topping.", ".$condiment; 
			$descData  = explode(', ',$desc);
			$descData  = array_filter($descData);
		    $desc      = implode(', ',$descData);

		    $desc_id = $bread_id.", ".$protein_id.", ".$cheese_id.", ".$topping_id.", ".$condiment_id;
		    $descDataId  = explode(', ',$desc_id);
			$descDataId  = array_filter($descDataId);
		    $desc_id      = implode(', ',$descDataId);

		    $brd = $bread;
			$total = $this->get_sandwich_current_price_menu($brd,$desc,$desc_id);
			   
		    $value['sandwich_name_trimmed'] = $self->word_processor($value['sandwich_name']);
			$value['like_id']       = $like_id;
			$value['sandwich_data'] = $array;
			$value['sandwich_desc'] = $desc;
			$value['sandwich_desc_id'] = $desc_id;
			$value['current_price'] = $total;
			$value['user_name']     = $self->get_user($value['uid']);
			$value['formated_date'] = date('m/d/y',strtotime($value['date_of_creation']));
		});
		return $data;
	}

	public function get_sandwich_current_price_menu($bread,$desc,$desc_id)
	{
		
		$params      = array('bread'=>$bread,'desc'=>$desc,'desc_id'=>$desc_id);
		$get_url     = API_URL.'sandwich/get_sandwich_current_price_menu/';
		$json    = $this->receive_data($get_url,$params);
		$finalData = json_decode($json,true);
        return $finalData['Data'];
	}
	
	
	function word_processor($word){
		 
		$string = wordwrap($word,17,"<br>");
	    $count  = substr_count($string, '<br>');
	    
	    if($count > 1){
	    	
	    	$pieces = explode('<br>',$string);
	    	$finalString  =  $pieces[0]."<br>".$pieces[1]."...";
	    	
	    } else{
	    	
	    	$finalString  = $string;
	    }
	    
		return $finalString;
	}
 
	public function getCombinedString($data)
	{
		$arr = array();
		
		if(is_object($data)){
			$data = get_object_vars($data);
		} else $data = array();
		
		foreach ($data as $key => $data) {
			$arr[] = $key . " (" . $data[1] . ")";
		}
		return implode(', ', $arr);
	}
	
	
	public function getStandardCategories(){
	
		$get_url     = API_URL.'categoryitems/getStandardCategories/';
		$json        = $this->receive_data($get_url,array());
		$data        = json_decode($json);
		return $data->Data;
	}
	
	public function getStandardCategoryProducts($id){
		$data['standard_category_id']	= $id;
		$get_url     = API_URL.'categoryitems/getStandardCategoryProducts/';
		$json        = $this->receive_data($get_url,$data);
		$data        = json_decode($json);
		return $data->Data;
	}
	
	function get_order_data(){
	
		$url       = API_URL.'sandwich/get_checkout_data/';
		$json      = $this->receive_data($url,array('uid'=>@$_SESSION['uid']));
		$data      = json_decode($json);
		if(is_object($data)){
			
			$final_data = $this->process_data($data->Data);
			
		
			
			
			$result_ids = array();
			foreach ($final_data as $final_data ){
				
				$result_ids[$final_data['item_type']]['order_item_id'][] = $final_data['order_item_id'];
				$result_ids[$final_data['item_type']]['item_id'][] = $final_data['item_id'];
				$result_ids[$final_data['item_type']]['quatity'][$final_data['item_id']] = $final_data['item_qty']; 
				
			}
			

			return $result_ids;
			
		} return  null;
		
	}
	
	function process_data($data){
		if(is_array($data)){
			array_walk($data,function(&$val,$key){
				$val  = get_object_vars($val);
			});
		}
		return $data;
	}
	
	function getFBfriendsMenu()
	{
		$get_url     = API_URL.'menu/getFBfriendsMenu/';
		if(isset($_SESSION['uid'])) $id = $_SESSION['uid']; else $id = 0;
		$json    = $this->receive_data($get_url,array('id'=>$id));
		//$json    = $this->receive_data($get_url,array('id'=>1576));
		return  json_decode($json);
	}

	function checkFBLogin()
	{
		$get_url     = API_URL.'menu/checkFBLogin/';
		if(isset($_SESSION['uid'])) $id = $_SESSION['uid']; else $id = 0;
		$json    = $this->receive_data($get_url,array('id'=>$id));
		return  json_decode($json);
	}
	
	function getfriendSandwichCount($uid){
		$get_url     = API_URL.'menu/getfriendSandwichCount/';
		 $json       = $this->receive_data($get_url,array('uid'=>$uid));  
	     $count      = json_decode($json);
	     if(isset($count->Data[0]->COUNT)) return $count->Data[0]->COUNT;
	     else return 0;
	     
	}
	
	
	function _process($data)
	{
		$_this = $this; 
		array_walk($data,function(&$val,$key) use ($_this)
		{ 
			$val->sandwich_count = $_this->getfriendSandwichCount($val->uid);
		});
	 	return $data;
	}
	
	public function get_user_firstName($id)
	{
		$get_url     = API_URL.'user/get/';
		$json        = $this->receive_data($get_url,array('user_id'=>$id));
		$data        = json_decode($json);
		if(count($data->Data)>0)  return $name   = $data->Data[0]->first_name;
		else  return '';
	}

	public function get_this_user($id)
	{
		$get_url     = API_URL.'user/get/';
		$json        = $this->receive_data($get_url,array('user_id'=>$id));
		$data        = json_decode($json);
		if(count($data->Data)>0)  return $name   = $data->Data[0];
		else  return '';
	}
	
}
