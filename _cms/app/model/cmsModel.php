<?php
class CmsModel  extends Model
{

	public $db; 
	//database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}
	
	public function get_webpagesdata_data($apiUrl, $data){
		$url     = $apiUrl.'myaccount/get_webpagesdata_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
}