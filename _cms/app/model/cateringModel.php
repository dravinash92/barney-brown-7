<?php
class CateringModel  extends Model
{

	public $db; 
	//database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}
		
	function get_catering_catgory_items($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_catering_catgory_items';
		$json    = $this->receive_data($url,$data);
		return $json;
	}
	
	function get_standard_catgory_items_list($apiUrl, $data ){
		$url     = $apiUrl.'categoryitems/get_standard_catgory_items_list_front';
		$json    = $this->receive_data($url,$data);
		return $json;
	}

}