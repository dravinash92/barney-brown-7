<?php
class MyAccountModel  extends Model
{

	public $db; 
	//database connection object

	public function __construct()
	{
		$this->db = parent::__construct();
	}
	
	public function listSavedAddress($uid){ 
		$get_url     = API_URL.'myaccount/listSavedAddress/';	
		$json          = $this->receive_data($get_url,array("uid"=>$uid));
		 $finalData = @json_decode($json);
	     return $finalData->Data;
	   
	}
	
	public function savingUserAddress($data){
		$get_url     = API_URL.'myaccount/saveUserAddress/';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function removeAddress($data){
		$get_url     = API_URL.'myaccount/removeAddress/';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function editAddress($data){
		$get_url     = API_URL.'myaccount/editAddress/';	
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
	    return $finalData->Data;
	}
	
	public function get_webpages_homepage_data($apiUrl, $data){
		$url     = $apiUrl.'myaccount/get_webpages_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	public function get_webpagesdata_data($apiUrl, $data){
		$url     = $apiUrl.'myaccount/get_webpagesdata_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	
	public function loginCheck($post){
		$url     = API_URL.'myaccount/loginCheck';
		$json    = $this->receive_data($url,$post);
		$finalData = json_decode($json);
		return $finalData->Data;            
	}
	
	public function createAccount($post){
		$url     = API_URL.'myaccount/createAccount';
		$json    = $this->receive_data($url,$post);
		$finalData = json_decode($json);
		return $finalData->Data;            
	}
	public function orderhistory($post){
		$url     = API_URL.'myaccount/orderhistory';
		$json    = $this->receive_data($url,$post);
		$finalData = json_decode($json);
		return $finalData->Data;            
	}
	
	public function getUserDetails($uid){
		$data['user_id'] = $uid;
		$get_url     = API_URL.'user/get/';
		$json          = $this->receive_data($get_url,$data);
		$json = json_decode($json);
		return $json->Data[0];
	
	}
	
	public function userNameExist($post){
		$get_url     = API_URL.'customer/userNameExist/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;	
	}
	
	public function savingCustomerInfo($post){
		$get_url     = API_URL.'customer/savingCustomerInfo/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;	
	}
	
	public function addUserReview($post){
		$get_url     = API_URL.'myaccount/addUserReview/';
	    $json          = $this->receive_data($get_url,$post);
	    $json = json_decode($json);
		if( isset($json->Data) ) return $json->Data;	
	}
	
	public function savedBilling($post){
		$get_url     = API_URL.'myaccount/savedBilling/';
	
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;	
	}
	
	// AUTHNET API
	public function cardAddEditEvent($post){
		$get_url     = API_URL.'myaccount/cardAddEditEvent/';
		$json          = $this->receive_data($get_url,$post);
		echo $json;
		exit(  );
	}
	
	public function cardRemoveEvent($post){
		$get_url     = API_URL.'myaccount/cardRemoveEvent/';
		$json          = $this->receive_data($get_url,$post );

		
		echo $json;
		exit(  );
	}
	
	public function get_all_review_contents(){
		$get_url       = API_URL.'customer/get_all_review_contents/';
		$json          = $this->receive_data($get_url,array());
		$json          = json_decode($json);
		return $json->Data;	
	}
	
	public function get_user_review($post){
		$get_url       = API_URL.'customer/get_user_reviews/';
		$json          = $this->receive_data($get_url,$post);
		$json          = json_decode($json);
		if(count($json->Data))
			return $json->Data[0];
		else
			return array();				
	}
	
	public function forgotPassword($post){
		$get_url       = API_URL.'myaccount/forgotpassword/';
		$json          = $this->receive_data($get_url,$post);
		$json          = json_decode($json);
		return $json->Data;
	}
	
	public function getAllzipcodes($post){
		$get_url       = API_URL.'myaccount/getAllzipcodes/';
	    return $json     = $this->receive_data($get_url,array());
	 }
	 
	 public function deleteAll(){
	 	$get_url = API_URL.'customer/delete_all_orders/';
	 	return $json     = $this->receive_data($get_url,array());
	 }
	
	public function myOrderReorder($data){
		
		if(!isset($_SESSION['uid'])){ $uid =  $_COOKIE['user_id'];  } else { $uid = $_SESSION['uid'];  }
		$delUrl        = API_URL.'customer/delete_all_orders/';
                $this->receive_data($delUrl,array("user_id"=>$uid)); 
		$get_url       = API_URL.'myaccount/myOrderReorder/';
                //exit;
	    return $json   = $this->receive_data($get_url,$data);
		
	}
	
	public function myOrderDetails($id){
	
		$order_data_url      = API_URL.'sandwich/get_checkout_order_success_data/';
		$data = $this->receive_data($order_data_url,array('order_id'=>$id));
		return json_decode($data);
		
	}
	
	public function getStoreFomZip(){
		if(isset($_POST['zip'])) $zip = $_POST['zip']; else $zip = 0;
		$data = $this->receive_data(API_URL.'myaccount/getStoreFomZip/',array('zip'=>$zip));
		$data = json_decode($data);
		return $data->Data;
	}
	
	
	public function getUserAccessToken(){
		
	  if(isset($_SESSION['uid']) && $_SESSION['uid']){	
	  	$uid = $_SESSION['uid']; 
		
	  	return $data = $this->receive_data(API_URL.'myaccount/getUserAccessToken/',array('uid'=>$uid));
	  } else return '{"Response":{"status-code":"404","message":"invalid"},"Data":["null"]}';
		 
	}
	
	public function get_username($id){
		$get_url     = API_URL.'user/get/';
		$json        = $this->receive_data($get_url,array('user_id'=>$id));
		$data        = json_decode($json);
		if(count($data->Data)>0)  return   $data->Data[0]->first_name." ".$data->Data[0]->last_name;
		else  return '';
	}
	
	public function checkUserAvailability(){
		
		$uid         = (isset($_SESSION['uid']))?@$_SESSION['uid']:@$_COOKIE['user_id'];
		$umail       = $_COOKIE['user_mail'];
		$get_url     = API_URL.'myaccount/checkUserAvailability/';
		return $json   = $this->receive_data($get_url,array('uid'=>$uid, 'umail' => $umail));
		exit;
	}
	 	
}  
