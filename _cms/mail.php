<?php

error_reporting(E_ALL); ini_set('display_errors', '1');

Class MailSender{
	
	private $headers;
	private $footer;
	private $body;
	private $subject;
	private $mailTo;
	private $mailFrom = "noreply@allthingsmedia.com";
	private $mailFromName = "Barney Brown";
	//private $logoPath = "http://www.allthingsmedia.com/wp-content/uploads/2013/03/P-US-HTI_img1.jpg";
	private $logoPath = "http://cms.ha.allthingsmedia.com/logo.jpg";
	
	public function __construct(){
			
		$this->setHeaders();	
	
	}
	
	public function setMailFrom($mailFrom = "noreply@allthingsmedia.com"){
		
		$this->mailFrom = $mailFrom;
	
	}
		
	public function setHeaders(){
		
		$this->headers = "MIME-Version: 1.0" . "\r\n";
		$this->headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$this->headers .= "From: ". $this->mailFromName ." <". $this->mailFrom . ">\r\n" .		
		"X-Mailer: PHP/" . phpversion();	
	}
	
	public function setSubject($subject = "Subject not set yet."){
		
		$this->subject = $subject;	
			
	}	
	
	public function setBody($body="Body not set yet."){
		
		/*$this->body = " <html style='height: 100%;'>
	                           <body style='height: 100%;'>	                           
								$body
				   </body>
				</html>	
			";*/

		$this->body = $body;
	
	}
	
	public function setMailTo($mailTo = "surveer-op@allthingsmedia.com"){
		
		$this->mailTo = $mailTo;	
	
	}
	
	public function sendMail(){
	/* 	echo $this->mailTo;
		echo $this->subject;
		echo $this->body;
		echo $this->headers; */
		return mail($this->mailTo,$this->subject,$this->body,$this->headers);
		//return mail($this->mailTo,$this->subject,$this->body);
	
	}
	
	public function setWelcomeBody(){
		
		<html>
			<head>
<title>Barney Brown | Welcome</title>
<style type="text/css"> 
<!--
.ReadMsgBody { width: 100%;}
.ExternalClass {width: 100%;}
.ExternalClass * {line-height: 125%}
-->
</style>

<style type="text/css">
div, p, a, li, td { -webkit-text-size-adjust:none; }
</style> 
</head>
<body bgcolor="#ffffff">

<table width="100%" bgcolor="#ffffff">
	<tr>
		<td>
			<table align="center" border="0" width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" style="font-size: 11px; color: #262626; font-family: helvetica, arial, sans-serif;">
				<tr>
					<td>
						<table align="center" border="0" width="600"  cellpadding="0" cellspacing="0" >
							<tr>
							<td align="left" valign="top" style="text-align: center;"><a href="#"><img src="http://cms.ha.allthingsmedia.com/app/e-images/logo.png" alt="logo"/></a></td>
							</tr>
						</table>
						
						<table align="center" border="0" width="600"  cellpadding="0" cellspacing="0" style="font-size: 12px; color: #000000; font-family: helvetica, arial, sans-serif; padding-left: 15px; padding-top: 40px;">
							<tr><td>Hi Matt,<br/><br/>
							Welcome to Barney Brown, where all of your sandwich dreams can finally become a reality!   In case you missed them, these are some of our interactive features which we think you may like:<br/><br/>
							<strong style="text-decoration: underline; font-size: 14px;">CUSTOM SANDWICH BUILDER</strong><br/>
							Choose from a curated selection of over 70 premium ingredients to create whatever sandwich comes to your imagination!  Use this cool visual builder and see your creation come to life right in front of your eyes!
							<br/>
							<br/>
							<strong style="text-decoration: underline; font-size: 14px;">PERSONALIZED MENU</strong><br/>
							Don\'t want to keep building that same sandwich over and over again every time you come back?  Don\'t worry, we got you covered.  All sandwiches you create will be saved to your own personal menu for easy future reordering.
							<br/>
							<br/>
							<strong style="text-decoration: underline; font-size: 14px;">SHARED CREATIONS</strong><br/>
							Have an idea for an awesome sandwich?  Feel free to share it with the rest of the Barney Brown community.  Our Shared Sandwiches Collection features an endless selection of original creations by our users.
							<br/>
							<br/>
							<strong style="text-decoration: underline; font-size: 14px;">FRIENDS\' SANDWICHES</strong><br/>
							Login with Facebook and see what kinds of sandwiches your friends are creating too!  If you like one of them, just add it right to your own menu!
							<br/>
							<br/>
							Thanks again for taking part in our social sandwiching experience!  We\'re glad to have you onboard!
							<br/><br/>
							
							With love and extra mayo,<br/>The Barney Brown Team

							
							</td></tr>
						</table>
						
						
						<table width="280" border="0" align="center" cellpadding="0" cellspacing="0"  style="padding-top: 70px;">
	        				<tr>
	          					<td><a href="#"><img src="http://cms.ha.allthingsmedia.com/app/e-images/app-store.png" alt=""/></a></td>
	          					<td><a href="#"><img src="http://cms.ha.allthingsmedia.com/app/e-images/google-play.png" alt=""/></a></td>
	          				</tr>
          				</table>
						
						<table width="150" border="0" align="center" cellpadding="0" cellspacing="0"  style="padding-top: 15px;">
	        				<tr>
								
	          					<td><a href="#"><img src="http://cms.ha.allthingsmedia.com/app/e-images/fb.png" alt=""/></a></td>
	          					<td><a href="#"><img src="http://cms.ha.allthingsmedia.com/app/e-images/instagram.png" alt=""/></a></td>
	          					<td><a href="#"><img src="http://cms.ha.allthingsmedia.com/app/e-images/twitter.png" alt=""/></a></td>
	          					
	          				</tr>
          				</table>
						
						<table align="center" border="0" width="600"  cellpadding="0" cellspacing="0" style="font-size: 12px; color: #000000; font-family: helvetica, arial, sans-serif;padding-top: 10px; font-weight: bold;">
	        				<tr>
	          					<td style="text-align: center;"><a style="color: #000; text-decoration: none;" href="#">www.barneybrown.com</a></td>
	          				</tr>
          				</table>
						
	      				<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" >
	        				<tr>
	          					<td>&nbsp;</td>
	          				</tr>
          				</table>
          			</td>
      			</tr>
    		</table>
    	</td>
    </tr>
</table>

</body>
</html>	
		';	
		
		$this->body = $body;
	}

	
}

	$mailObject = new MailSender();
	
	//Set Header after if you setMailFrom
	
	//$mailObject->setMailFrom();
	
	$mailObject->setSubject();
	$mailObject->setWelcomeBody();
	//$mailObject->setBody();
	$mailObject->setMailTo();
	$response = $mailObject->sendMail();
	
	echo "Get results";

	var_dump( $response );	
?>
