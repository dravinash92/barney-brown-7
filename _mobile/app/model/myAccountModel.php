<?php
class MyAccountModel  extends Model
{
	public $db; //database connection object
	/**
	 * invoke database connection object
	 */
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * List Saved Address
	 */
	
	public function listSavedAddress($uid){
		$get_url     = API_URL.'myaccount/listSavedAddress/';
		$json          = $this->receive_data($get_url,array("uid"=>$uid));
		$finalData = @json_decode($json);
		return $finalData->Data;

	}
	/**
	 * List Saved Address
	 */
	public function savingUserAddress($data){
		$get_url     = API_URL.'myaccount/saveUserAddress/';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * Remove Saved Address
	 */
	public function removeAddress($data){
		$get_url     = API_URL.'myaccount/removeAddress/';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * Update Saved Address
	 */
	public function editAddress($data){
		$get_url     = API_URL.'myaccount/editAddress/';
		$json          = $this->receive_data($get_url,$data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * List webpages homepage data
	 */
	public function get_webpages_homepage_data($apiUrl, $data){
		$url     = $apiUrl.'myaccount/get_webpages_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	/**
	 * List webpages data
	 */
	public function get_webpagesdata_data($apiUrl, $data){
		$url     = $apiUrl.'myaccount/get_webpagesdata_data';
		$json    = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	/**
	 * Login check
	 */
	public function loginCheck($post){
		$url     = API_URL.'myaccount/loginCheck';
		$json    = $this->receive_data($url,$post);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	/**
	 * Create account
	 */
	public function createAccount($post){
		$url     = API_URL.'myaccount/createAccount';
		$json    = $this->receive_data($url,$post);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	/**
	 * List order histrory
	 */
	public function orderhistory($post){
		$url     = API_URL.'myaccount/orderhistory';
		$json    = $this->receive_data($url,$post);
		$finalData = json_decode($json);
		return $finalData->Data;
	}
	/**
	 * List user details
	 */
	public function getUserDetails($uid){
		$data['user_id'] = $uid;
		$get_url     = API_URL.'user/get/';
		$json          = $this->receive_data($get_url,$data);
		$json = json_decode($json);
		return $json->Data[0];

	}
	/**
	 * Check user name exist
	 */
	public function userNameExist($post){
		$get_url     = API_URL.'customer/userNameExist/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;
	}
	/**
	 * Add customer information
	 */
	public function savingCustomerInfo($post){
		$get_url     = API_URL.'customer/savingCustomerInfo/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;
	}
	/**
	 * Add customer review
	 */
	public function addUserReview($post){
		$get_url     = API_URL.'myaccount/addUserReview/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		if( isset($json->Data) ) return $json->Data;
	}
	/**
	 * Add billing information
	 */
	public function savedBilling($post){
		$get_url     = API_URL.'myaccount/savedBilling/';
		$json          = $this->receive_data($get_url,$post);
		$json = json_decode($json);
		return $json->Data;
	}
	/**
	 * Add / Edit card
	 */
	public function cardAddEditEvent($post){
		$get_url     = API_URL.'myaccount/cardAddEditEvent/';
		$json          = $this->receive_data($get_url,$post);
		echo $json;
		exit( );
	}
	/**
	 * Remove card information
	 */
	public function cardRemoveEvent($post){
		$get_url     = API_URL.'myaccount/cardRemoveEvent/';
		$json          = $this->receive_data($get_url,$post);
		echo $json;
		exit( );
		
	}
	/**
	 * List all review content
	 */
	public function get_all_review_contents(){
		$get_url       = API_URL.'customer/get_all_review_contents/';
		$json          = $this->receive_data($get_url,array());
		$json          = json_decode($json);
		return $json->Data;
	}
	/**
	 * List user review
	 */
	public function get_user_review($post){
		$get_url       = API_URL.'customer/get_user_reviews/';
		$json          = $this->receive_data($get_url,$post);
		$json          = json_decode($json);
		if(count($json->Data))
			return $json->Data[0];
		else
			return array();
	}
	/**
	 * Add forgot password
	 */
	public function forgotPassword($post){
		$get_url       = API_URL.'myaccount/forgotpassword/';
		$json          = $this->receive_data($get_url,$post);
		$json          = json_decode($json);
		return $json->Data;
	}
	/**
	 * List all zipcode
	 */
	public function getAllzipcodes($post){
		$get_url       = API_URL.'myaccount/getAllzipcodes/';
		return $json     = $this->receive_data($get_url,array());
	}
	/**
	 * Delete all orders
	 */
	public function deleteAll(){
		$get_url = API_URL.'customer/delete_all_orders/';
		return $json     = $this->receive_data($get_url,array());
	}
	/**
	 * Add myOrder Reorder
	 */
	public function myOrderReorder($data){
		if(!isset($_SESSION['uid'])){
			$uid =  $_COOKIE['user_id'];
		} else { $uid = $_SESSION['uid'];
		}
		$delUrl        = API_URL.'customer/delete_all_orders/';
		$this->receive_data($delUrl,array("user_id"=>$uid));
		$get_url       = API_URL.'myaccount/myOrderReorder/';
		return $json   = $this->receive_data($get_url,$data);

	}
	/**
	 * List order details
	 */
	public function myOrderDetails($id){

		$order_data_url      = API_URL.'sandwich/get_checkout_order_success_data/';
		$data = $this->receive_data($order_data_url,array('order_id'=>$id));
		return json_decode($data);

	}
	/**
	 * List store from zip
	 */
	public function getStoreFomZip(){
		if(isset($_POST['zip'])) $zip = $_POST['zip']; else $zip = 0;
		$data = $this->receive_data(API_URL.'myaccount/getStoreFomZip/',array('zip'=>$zip));
		$data = json_decode($data);
		return $data->Data;
	}
	/**
	 * List User Access Token
	 */
	public function getUserAccessToken(){

		if(isset($_SESSION['uid']) && $_SESSION['uid']){
			$uid = $_SESSION['uid'];
			return $data = $this->receive_data(API_URL.'myaccount/getUserAccessToken/',array('uid'=>$uid));
		} else return '{"Response":{"status-code":"404","message":"invalid"},"Data":["null"]}';
			
	}
	
	public function checkUserAvailability(){
	
		$uid   = (isset($_SESSION['uid']))?@$_SESSION['uid']:@$_COOKIE['user_id'];
		$umail       = @$_COOKIE['user_mail'];
		$get_url     = API_URL.'myaccount/checkUserAvailability/';
		return $json   = $this->receive_data($get_url,array('uid'=>$uid, 'umail' => $umail));
		exit;
	}
}
