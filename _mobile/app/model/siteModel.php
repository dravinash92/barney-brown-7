<?php
class SiteModel  extends Model
{
	public $db; 

	public function __construct()
	{
		$this->db = parent::__construct();
	}
	
	public function resetPassword($post){
		$get_url       = API_URL.'myaccount/resetPassword/';
		echo $json          = $this->receive_data($get_url,$post);
		$json          = json_decode($json);
		return $json->Data;
	}
	
	public function setPassword($post){
		$get_url       = API_URL.'myaccount/setPassword/';
		$json          = $this->receive_data($get_url,$post);
		$json          = json_decode($json);
		return $json->Data;
	}
	
}