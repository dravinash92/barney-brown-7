<?php


class CheckoutModel extends Model
{

	public $db; //database connection object
	
	public $tmpData;
	/**
	* invoke database connection object
	*/
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * List user checkout data
	 */
	
	public function get_user_checkout_data($uid)
	{

		$get_url = API_URL . 'sandwich/get_checkout_data/';
		$json    = $this->receive_data($get_url, array(
				'uid' => $uid
		));
		return $json;
	}
	/**
	 * Checkout confirm
	 */
	public function check_out_confirm($id)
	{

		$order_data_url = API_URL . 'sandwich/get_checkout_order_success_data/';
		$data           = $this->receive_data($order_data_url, array(
				'order_id' => $id
		));
		return json_decode($data);

	}
	/**
	 * List last order
	 */
	public function getLastOrder($uid)
	{

		if (!$uid)
			return false;
		$url = API_URL . 'sandwich/getLastOrder/';
		echo $json = $this->receive_data($url, array(
				'uid' => $uid
		));
		
	}
	/**
	 * Process user data
	 */
	function process_user_data($data)
	{
		$self = $this;
		array_walk($data, function(&$value, $key) use ($self)
		{
			$value = get_object_vars($value);
			$array = get_object_vars(json_decode($value['sandwich_data']));
			array_walk($array, function(&$val, $key)
			{
				$val = get_object_vars($val);
			});
			
			$format_data = $array;

			$sx = '';
			foreach ($array as $fval) {

				$str = $fval['item_name'];

				foreach ($str as $st) {
					$sx .= $st . ' ';
				}
			}

			$sx          = trim($sx);
			$format_data = str_replace(' ', ',', $sx);



			$value['data_string']   = $format_data;
			$value['item_qty']      = str_pad($value['item_qty'], 2, '0', STR_PAD_LEFT);
			$value['formated_date'] = date('m/d/y', strtotime($value['date_of_creation']));
		});
		return $data;
	}
	/**
	 * confirm cart
	 */
	public function confirm_cart()
	{

		$_POST['user_id'] = $_SESSION['uid'];
		$_POST['application'] = 'Mobile';
		$get_url          = API_URL . 'sandwich/insert_checkout_placeorder_data/';
		$json        = $this->receive_data($get_url,$_POST); 
		echo $json;
		exit;
	}
	/**
	 * List pickup store
	 */
	public function getPickupStores($data=array())
	{
		$get_url   = API_URL . 'cart/listPickupAddress/';
		$data['enabled']=1;
		$json      = $this->receive_data($get_url, $data);
		$finalData = @json_decode($json);
		if( isset($finalData->Data) && $finalData->Data ){
			return $finalData->Data;
		}
	}
	/**
	 * List pickup store address
	 */
	public function getPickupStoreAddress($data)
	{
		$get_url   = API_URL . 'cart/pickupAddress/';
		$json      = $this->receive_data($get_url, $data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * Update cart item
	 */
	public function updateCartItems($data)
	{
		$get_url   = API_URL . 'cart/updateCartItems/';
		$json      = $this->receive_data($get_url, $data);
		$finalData = @json_decode($json);
		if( isset($finalData->Data) && $finalData->Data ){
			return $finalData->Data;
		}
	}
	/**
	 * Insert billing information
	 */
	public function save_billing_info($data)
	{
		$get_url   = API_URL . 'cart/savebillinginfo/';
		$json      = $this->receive_data($get_url, $data);
		$finalData = @json_decode($json);
		echo $json;
	}
	/**
	 * Check cvv valid
	 */
	public function chekcvvValid($data)
	{
		$get_url   = API_URL . 'cart/check_cvv/';
		$json      = $this->receive_data($get_url, $data);
		$finalData = @json_decode($json);
		echo $json;
	}
	/**
	 * List billing information
	 */
	public function get_Billinginfo($data, $print = false)
	{
		$get_url = API_URL . 'cart/getBillinginfo/';
		$json    = $this->receive_data($get_url, $data);
		if ($print == true) {
			echo $json;
		} else {
			return @json_decode($json);
		}
	}
	/**
	 * Convert to array
	 */
	function convert_to_Array($data)
	{
		array_walk($data, function(&$value, $key)
		{
			$value = get_object_vars($value);
		});
		return $data;
	}
	/**
	 * List Discount Details
	 */
	public function geDiscountDetails($data)
	{
		$get_url   = API_URL . 'cart/geDiscountDetails/';
		$json      = $this->receive_data($get_url, $data);
		
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * List cart item
	 */
	public function cartItems($data)
	{
		$get_url   = API_URL . 'cart/cartItems/';
		$json      = $this->receive_data($get_url, $data);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * List cart Details
	 */
	public function addItemToCart($post)
	{

		$get_url   = API_URL . 'cart/addItemToCart/';
		$json      = $this->receive_data($get_url, $post);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * Update cart item
	 */
	public function updateItemToCart($post)
	{
		$get_url   = API_URL . 'cart/updateItemToCart/';
		$json      = $this->receive_data($get_url, $post);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * Remove cart item
	 */
	public function removeCartItem($post)
	{
		$get_url   = API_URL . 'cart/removeCartItem/';
		$json      = $this->receive_data($get_url, $post);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * process Sandwiches
	 */
function processSandwiches($data)
	{

		$self = $this;

		if (is_array($data)) {
			array_walk($data, function(&$value, $key) use ($self)
			{
				$value = @get_object_vars($value);
				$dt    = json_decode($value['sandwich_data']);
				if (is_object($dt) && isset($dt))
					$array = get_object_vars($dt);
				else
					$array = array();

				array_walk($array, function(&$val, $key)
				{
					$val = get_object_vars($val);
				});
				
				$format_data = $array;

				$sx = '';
								
				//Bread type for Foot Breads
				$bread_types = "";
				
				foreach ($array as $ikey => $fval) {
					$str = $fval['item_name'];
					
					if($ikey == "BREAD")
						$bread_types = $fval['type'];					
						
						
					
					
					foreach ($str as $st) {
					
						if (isset($fval['item_qty']->{$st}[1])) {
							$sx .= $st . ' (' . $fval['item_qty']->{$st}[1] . ')' . '#';
						} else {
							$sx .= $st . '#';
						}
					}
				}

				$sx                     = trim($sx);
				$format_data            = str_replace('#', ', ', $sx);
				$format_data            = rtrim($format_data, ", ");
				$value['data_string']   = $format_data;
				$value['bread_type'] = $bread_types;
				$value['formated_date'] = date('m/d/y', strtotime($value['date_of_creation']));
			});
			
			
			
			return $data;
		}
	}
	
	/**
	 * Update address
	 */
	public function editAddress($data)
	{
		$get_url = API_URL . 'myaccount/editAddress/';
		$json    = $this->receive_data($get_url, $data);
		print_r($json);
		exit;
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * List saved address
	 */
	public function listSavedAddress($uid)
	{
		$get_url   = API_URL . 'myaccount/listSavedAddress/';
		$json      = $this->receive_data($get_url, array(
				"uid" => $uid
		));
		$finalData = @json_decode($json);
		return $finalData->Data;
		
	}
	/**
	 * List time on store selection
	 */
	
	public function getTimeOnStoreSelection($storeID, $selectedDay, $Currenttime, $inc = 0, $isCurrentDay = 0 , $isDelivery)
	{  
		
		
		 
		
	    if($inc == 0){
			$this->tmpData  = json_decode($this->receive_data(API_URL .'cart/getAllCloseTimes/', array("storeID" => $storeID )));
	    }
		
	 
	    $min = explode(":",$Currenttime);
	    
		$Currenttime    = strtotime($Currenttime . ':00');
		$CurrentMin     = $min[1];
		$Currenttime    = intval(date('H', $Currenttime));
	    if($CurrentMin > 0 && $Currenttime > 12) { $Currenttime = $Currenttime+1; }
		 
		
		$get_url        = API_URL . 'cart/changeTimeOnStoreSelection/';
		$json           = $this->receive_data($get_url, array(
				"storeID" => $storeID,
				"selectedDay" => $selectedDay,
				"currentTime" => $Currenttime,
				"isCurrentDay" => $isCurrentDay,
				"isDelivery" => $isDelivery
		));
		
		 
		
		$finalData      = @json_decode($json);
		$val            = @$finalData->Data;
		
		
	    

		if ($Currenttime >= @$val->open && $Currenttime <= @$val->close)
			$store_state = true;
		
		else if($Currenttime < @$val->open)
			$store_state = true;
		
		else 
		    $store_state = false;
		 
		
		if (isset($val) && !$val)
			$store_state = false;
		
		 
		 

		if ($store_state == false) {
			$inc++;
			$days  = array(
					'Sunday',
					'Monday',
					'Tuesday',
					'Wednesday',
					'Thursday',
					'Friday',
					'Saturday'
			);
			
			$index = array_search($selectedDay, $days);
			if ($index == 6)
				$day = $days[0];
			else
				$day = $days[$index + 1];
			
			 
			
			$result['run_counts'] = $inc;
			if ($inc < 8) {
				
				if( (isset($this->tmpData->Data) && $this->tmpData->Data) && (isset($this->tmpData->Data->{$day}) && $this->tmpData->Data->{$day}  ) ){
					$closeTime = $this->tmpData->Data->{$day};
				} else {
					$closeTime = '12:00';
				}
				
				 
				
				$result = $this->getTimeOnStoreSelection($storeID, $day,$closeTime, $inc , 0 , $isDelivery);
				
				 
			
			if (isset($result['store_state']) && $result['store_state'] == true)
				return $result;
		     }   
		
		   }

		return array(
				'run_counts' => $inc,
				'store_details' => $finalData->Data,
				'store_state' => $store_state,
				'day' => $selectedDay
		);
	}
	/**
	 * List cart item quantity
	 */
	public function getCartItemQtyCount($post)
	{
		$get_url   = API_URL . 'cart/getCartItemQtyCount/';
		$json      = $this->receive_data($get_url, $post);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	/**
	 * List all review content
	 */
	public function get_all_review_contents()
	{
		$get_url = API_URL . 'customer/get_all_review_contents/';
		$json    = $this->receive_data($get_url, array());
		$json    = json_decode($json);
		return $json->Data;
	}
	/**
	 * List user review
	 */
	public function get_user_review($post)
	{
		$get_url = API_URL . 'customer/get_user_reviews/';
		$json    = $this->receive_data($get_url, $post);
		$json    = json_decode($json);
		if (count($json->Data))
			return $json->Data[0];
		else
			return array();
	}
	/**
	 * List user details
	 */
	public function getUserDetails($uid)
	{
		$data['user_id'] = $uid;
		$get_url         = API_URL . 'user/get/';
		$json            = $this->receive_data($get_url, $data);
		$json            = json_decode($json);
		return $json->Data[0];

	}
	/**
	 * List order count
	 */
	public function get_order_count($uid)
	{
		$data['user_id'] = $uid;
		$get_url         = API_URL . 'sandwich/get_order_count/';
		$json            = $this->receive_data($get_url, $data);
		$json            = json_decode($json);
		return $json->Data;

	}
	/**
	 * Get current store time slot
	 */
	public function getCurrentStoreTimeSlot($data)
	{

		$get_url = API_URL . 'cart/getCurrentStoreTimeSlot/';
		$json    = $this->receive_data($get_url, $data);		
		$json    = json_decode($json);
		return $json->Data;

	}
	
	
	public function removeCard($post){
	    $get_url     = API_URL.'myaccount/cardRemoveEvent/';
		$json          = $this->receive_data($get_url,$post);
		print $json;
		exit(  );
	}
	
	public function deliveryTime($sub_total){
		$order_data_url      = API_URL.'sandwich/deliveryTime/';
		$data = $this->receive_data($order_data_url,array('subTotal'=>$sub_total));
		return json_decode($data);
	}
	public function getspecificday($store)
	{
		$store = $this->clean_for_sql( $store );
		
		$get_url     = API_URL.'cart/getspecificday/';
		$json          = $this->receive_data($get_url,array('storeId'=>$store));
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
	public function checkZip($store){
		$get_url     = API_URL.'cart/checkZip/';
		$json          = $this->receive_data($get_url,$store);
		$finalData = @json_decode($json);
		return $finalData->Data;
	}

	public function get_storeClosed_days( $post )
	{
		$get_url     = API_URL.'cart/get_storeClosed_days/';
		$json          = $this->receive_data( $get_url, $post );
		$finalData = @json_decode($json);
		return $finalData->Data;
	}

	public function getCardZIp( $post )
	{
		$get_url     = API_URL.'cart/getCardZIp/';
		$json          = $this->receive_data( $get_url, $post );
		$finalData = @json_decode($json);
		return $finalData->Data;
	}
	
}
