<?php
class HomeModel  extends Model
{

	public $db; //database connection object
	/**
	* invoke database connection object
	*/
	public function __construct()
	{
		$this->db = parent::__construct();
	}
	/**
	 * List web home information
	 */
	
	public function get_webpages_homepage_data($apiUrl, $data)
	{
		$data = ($data != '') ? $data : array();
		$url       = $apiUrl.'myaccount/get_webpages_data';
		$json      = $this->receive_data($url,$data);
		$finalData = json_decode($json);
		return $this->prep_wepage_data($finalData->Data);
	}
	/**
	 * List web page information
	 */
	public function prep_wepage_data($data){

		array_walk($data,function(&$val,$key){
				
			$val->image = ADMIN_URL.'upload/'.$val->image;
			if( !@file($val->image) ){
					
				$val = null;
			}
		});

			$data = array_filter($data);
			$data =  array_values($data);


			return $data;
	}
	/**
	 * List saved sandwich
	 */
	public function get_saved_sandwiches($uid,$limit){
		$url       = API_URL.'myaccount/get_saved_sandwiches';
		$json      = $this->receive_data($url,array('uid'=>$uid,'limit'=>$limit));
		
		$finalData = json_decode($json);

		return $finalData;
	}
	/**
	 * Insert post order
	 */
	public function pastOrders($uid){
		if(!$uid) return false;
		$url          = API_URL.'sandwich/getLastOrder/';
		$json         = $this->receive_data($url,array('uid'=>$uid));
		$data         = json_decode($json);
		return $data;
	}
	/**
	 * List Order Sandwich Banner Image
	 */
	public function OrderSandwichBannerImage($uid){
		if(!$uid) return false;
		$url          = API_URL.'sandwich/OrderSandwichBannerImage/';
		$json         = $this->receive_data($url,array('uid'=>$uid));
		$data         = json_decode($json);
		return $data;
	}
	/**
	 * List order details
	 */
	public function prepOrderDetail($ordDetobj,$uid){

		$order_url  = API_URL.'sandwich/banner_order_details';
		$order_json = $this->receive_data($order_url,array('id_string'=>$ordDetobj->order_item_id,'uid'=>$uid));
		$order_json = json_decode($order_json);
		
		if(isset($order_json->Data->is_delivery)){
			if($order_json->Data->is_delivery) $delivery = 1; else  $delivery = 0;
		} else $delivery = 0;
		

		$url        = API_URL.'sandwich/get_address_street';
		$json       = $this->receive_data($url,array('id'=>$ordDetobj->address_id,'is_delivery'=>$delivery));
		$outadd     = json_decode($json);
			
		if(isset($outadd->Data->address1)){
			$address   = $outadd->Data->address1;
		} else $address = null;
		$dt        =  date('D, M d, Y',strtotime($ordDetobj->date));
	  
	  
		if($order_json){
			$arr1 = array('street'=>$address,'date'=>$dt,'details'=>get_object_vars($order_json->Data));
			return $arr1;
		}

	}
	/**
	 * List past Order history
	 */
	public function get_past_order_history($uid,$limit){
		$url       = API_URL.'myaccount/get_past_order_history';
		$json      = $this->receive_data($url,array('uid'=>$uid,'limit'=>$limit));
	
		$finalData = json_decode($json);

		return $finalData;
	}
}