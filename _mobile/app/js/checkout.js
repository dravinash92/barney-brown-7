var checkout = {};
var currentDateTime = null;
var currentDateTimeNow = null;
var currentStoreTimeSlot = null;
var storeID_check = null;
if (window.location.href.indexOf("checkout") !== -1 )
{
    checkout = {
    check_time_and_change_if_needed:  function ( force_show ) // done
    {
        var self = this;
        var day_time_minumum = 0;
     
        self.getTimeAvilableSlots(  );
        var timeStateArray = self.pick_time_state( );
        var timeStateArray_now = self.pick_time_state_now();

        mode = timeStateArray['state'];
       
        mobile_ha.common.setTimeStateArray( timeStateArray );      // mobile_ha.common  saves our state array that we built.


        self.handle_user_time_pick_first_run( timeStateArray  );                // mobile_ha.common  is first run.  Sets appropriate stuff if it works.
        
        // So now we have the valid user info, we have the floor.  Now what?  
        // Get the actual time to use.
        timeStateArray = self.last_chance_specific_day( timeStateArray );
        
        var mesg;
        if (timeStateArray.msg) 
        {
            mesg = timeStateArray.msg;
        }
        while(timeStateArray.closedTodayOverride) // if the store is closed for next days, add days to it.
        {
            timeStateArray = self.last_chance_specific_day( timeStateArray );
        }
        self.limit_datepicker_min_date( mode, timeStateArray );                  // Sets datepicker minimum date - either 0 for now or 1 for tomorrow.
        self.limit_datepicker_on_closed_days(timeStateArray);
        timeStateArray.msg = mesg;
       
        set_date_time = self.pick_the_actual_time_to_set_in_datetime_picker( timeStateArray );
        

        if(self.getUserTimeValidated(  ) == 0 )
        {
            set_date_time = self.noon_or_greater( self.get_min_early_entry(  set_date_time ) );   //mobile_ha.common  makes sure our pick is not too early. 
        }
        else
        {
            timeStateArray['user_validated_time'] = self.getUserTimeValidated(  );
        }
      
        timeStateArray['set_date_time'] = set_date_time;
        timeStateArray_now['set_date_time'] = set_date_time;
        
        timeStateArray = self.figure_out_element_states( timeStateArray_now );
        timeStateArray = self.set_open_floor( timeStateArray );
        

        if( force_show == 1)
        {
            timeStateArray['nowHidden'] = true;
        }

        timeStateArray = self.no_now_on_specific_day( timeStateArray  );
        
        self.set_time_in_html_element( timeStateArray );
        self.set_date_in_html_element( timeStateArray );

        self.change_html_elements_for_time(  timeStateArray );

        mobile_ha.common.setTimeStateArray( timeStateArray );      // mobile_ha.common  saves our state array that we built.
        
        if( timeStateArray.state == 'None' ){
           self.limit_select_times_by_date(  mobile_ha.common.getOpenTime(  ) );
        }
        else{
            if(onPageLoad){
                self.limit_select_times_by_date(  timeStateArray.timeFloor, onLoad = true  );
                onPageLoad = false;
            }else{
                self.limit_select_times_by_date(  timeStateArray.timeFloor  );
            }
        }
        if (timeStateArray.msg) 
        {
            timeStateArray.msg = mesg;
        }
        
        mobile_ha.common.time_state_array = timeStateArray;
        self.set_front_initial_run( timeStateArray );

        
        setTimeout(function(){
            self.set_time_in_html_element( timeStateArray );
            self.set_date_in_html_element( timeStateArray );
        },500);
    
    },

    getTimeAvilableSlots  : function(oBj) //done
    {
        var time_list = this.get_all_possible_times(  );
        $(".timechange").html("");
        current_value = $(".timechange").val();
        $(".timechange").html(
            '<select class="timedropdown">' +
            time_list +
            "</select>"
            ); 
        this.set_time_html( current_value );

        return;         
    },

    // Figure out the base time. KEY FUNCTION.  This is the state manager function.  
    pick_time_state : function (  ) //done
    {
        var self = this;
        var store_closed = self.check_if_closed(  );
        var store_catered = self.checkBreadSimple(  );

        var currentTime =self.getCurrentDateTime(  );
        var storeOpenToday = self.getOpenCloseFromDate( currentTime );
        var storeID = storeOpenToday.store_id;

       if( store_closed  == 1 &&   store_catered == 1  )
        {
            timeFloor = self.get_hours_cater_and_closed(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'ClosedCater';
        }

        else if(  store_closed == 1 &&  store_catered  == 0  )
        {
            timeFloor = self.get_hours_closed(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'Closed';
        }

        else if( store_closed == 0 &&  store_catered == 1  )
        {
            timeFloor = self.get_hours_cater(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'Cater';
        }
        else
        {
            state = 'None';
            timeFloor = self.get_hours_regular(  );
            nowDisabled = false;
            nowHidden = false;
        }

        
        
        var $too_early = null;
        if( store_closed == 1 && store_catered == 0 )
        {
            // Too early!
            if( self.get_min_early_entry( ) == 1 )
            {
                timeFloor = self.get_hours_regular(  );
                $too_early = 1;
            }
        }

        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0, "tooEarly":$too_early, "store_id":storeID };
        
        return ret;
    },

    pick_time_state_now : function (  ) //done
    {
        var self = this;
        var store_closed = self.check_if_closed_now(  );
        var store_catered = self.checkBreadSimple(  );

        var currentTime = self.getCurrentDateTime(  );
        var storeOpenToday = self.getOpenCloseFromDate( currentTime );
        var storeID = storeOpenToday.store_id;
        console.log(store_closed+" "+store_catered)
        if( store_closed  == 1 &&   store_catered == 1  )
        {
            timeFloor = self.get_hours_cater_and_closed(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'ClosedCater';
        }

        else if(  store_closed == 1 &&  store_catered  == 0  )
        {
            timeFloor = self.get_hours_closed(  );
             console.log(timeFloor)
            nowDisabled = true;
            nowHidden = true;
            state = 'Closed';
        }

        else if( store_closed == 0 &&  store_catered == 1  )
        {
            timeFloor = self.get_hours_cater(  );
            nowDisabled = true;
            nowHidden = true;
            state = 'Cater';
        }
        else
        {
            state = 'None';
            timeFloor = self.get_hours_regular(  );
            console.log(timeFloor)
            nowDisabled = false;
            nowHidden = false;
        }

        console.log(timeFloor)
        
        var $too_early = null;
        if( store_closed == 1 && store_catered == 0 )
        {
            // Too early!
            
            if( self.get_min_early_entry( ) == 1 )
            {
                timeFloor = self.get_hours_regular(  );

                $too_early = 1;
            }
        }
        
        ret = {"state":state, "nowHidden":nowHidden, "nowDisabled":nowDisabled, "timeFloor":timeFloor, "user":0, "tooEarly":$too_early, "store_id":storeID };
        
        return ret;
    },

    check_if_closed : function (  ) //done
    {
        var self = this;
        openTime = mobile_ha.common.getOpenTime(  );
        closedTime = mobile_ha.common.getClosedTime(  );
        currentTime = self.getCurrentDateTime(  );
         
        var hour = currentTime.getHours();
        var newDate = currentTime.setHours(hour + 1);
        var storeOpenToday = self.getOpenCloseFromDate( currentTime );
        
        if(typeof openTime=="string"){
            if(openTime.indexOf("GMT ")){
            openTime=openTime.replace("GMT ", "GMT+");
            openTime=new Date(openTime);
            } 
        }
        if(typeof closedTime=="string"){
            if(closedTime.indexOf("GMT ")){
            closedTime=closedTime.replace("GMT ", "GMT+");
            closedTime=new Date(closedTime);
            } 
        }
         // IMPORTANT SET LOCAL TIME OVERRIDE
        //currentTime.setHours( 11 );
        if(storeOpenToday.store_closed_on_day == 1)
        {
            if( self.compareTimes( new Date(newDate), openTime ) == '<' )
            {
                return 1;
            }

            if( self.compareTimes( new Date(newDate), closedTime ) == '>' || self.compareTimes( new Date(newDate), closedTime ) == "=" )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }
    },

    check_if_closed_now : function (  ) //done
    {
        var self = this;
        openTime = mobile_ha.common.getOpenTime(  );
        closedTime = mobile_ha.common.getClosedTime(  );
        currentTime = self.getCurrentDateTime(  );

        var storeOpenToday = self.getOpenCloseFromDate( currentTime );

        if(typeof openTime=="string"){
            if(openTime.indexOf("GMT ")){
            openTime=openTime.replace("GMT ", "GMT+");
            openTime=new Date(openTime);
            } 
        }
        if(typeof closedTime=="string"){
            if(closedTime.indexOf("GMT ")){
            closedTime=closedTime.replace("GMT ", "GMT+");
            closedTime=new Date(closedTime);
            } 
        }
         // IMPORTANT SET LOCAL TIME OVERRIDE
        //currentTime.setHours( 11 );
        if(storeOpenToday.store_closed_on_day == 1)
        {
            if( self.compareTimes( currentTime, openTime ) == '<' )
            {
                return 1;
            }

            if( self.compareTimes( currentTime, closedTime ) == '>' || self.compareTimes( currentTime, closedTime ) == "=" )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
             return 1;
        }
    },

    getCurrentDateTime: function(  ) // done
    {
        var self = this;
        date = self.getCurrentDayHour(  );
        return date;
    },

    getOpenCloseFromDate: function( date ) // done
    {
        var self = this;
        date = new Date( date );

        var today_DayOfWeek = date.getDayName(  );

        var deliveryOrPickup = "delivery";
        var deliveryAddressId = $("input#address-id").val();
        var address_id = deliveryAddressId;
        // var store_timeslot_response = null;
        // if(storeID_check != address_id)
        // {
        //     var store_timeslot_response = self.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
        // }
        // else
        // {
        //     store_timeslot_response = currentStoreTimeSlot;
        // }
        
        var store_timeslot_response = self.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
        store_timeslot_response = JSON.parse( store_timeslot_response.responseText);


        var open_time =  store_timeslot_response['open'] ;
        var close_time =  store_timeslot_response['close'];

        var open_d = self.set_date_time(  date,  open_time );
        var close_d = self.set_date_time(  date,  close_time );
        
        if(  store_timeslot_response['delivery_active'] == 1 ){     // active 1 means it's off 
            store_delivery_off_message =  1;
        }
        else{
            store_delivery_off_message = 0;
        }

         ret = {
                "day_of_week" :store_timeslot_response['day'],
                "store_closed_on_day": store_timeslot_response['store_active'],
                "open_time": open_d,
                "close_time": close_d,
                "store_id":store_timeslot_response['store_id']
            };

        return ret;
    },

    get_hours_cater_and_closed :function (  ) // done
    {
        floor = this.getTimeFloor_closed_and_cater(  );
        return floor;
    },

    getTimeFloor_closed_and_cater : function (  ) //done
    {
        // ADD SAT SUN CLOSED HERE.
        var self = this;
        now = self.getCurrentDayHour(  );
        newDateMin  = now;
        
        if(  self.check_if_closed(  ) == 1 )
        {
            newDateMin = self.calcFloor(  );
        }
        
        var check_24_hours_too_late = self.test24HoursTooLate(  );
        if( check_24_hours_too_late == 1 )
        {
            newDateMin = self.date_plus_1( newDateMin );
        }
        else
        {
            // 24 hours from now is ok. since we are already tomorrow, set the time then for now, which would be 24 hours. 
            newDateMin = self.set_date_time( newDateMin, now );
        }
        
        newDateMin = self.roundMinutes( newDateMin  );
        return newDateMin;
    },

    get_hours_closed :function (  ) // done
    {
        floor = this.getTimeFloor_closed_only(  );
        return floor;
    },

    getTimeFloor_closed_only: function (  ) // done
    {
        // ADD SAT SUN CLOSED HERE.
        newDateMin = this.getCurrentDayHour(  );
        
        if(  this.check_if_closed(  ) == 1 )
        {
            newDateMin = this.calcFloor( 'closed_only' );
        }
           
        newDateMin = this.roundMinutes( newDateMin  );
        
        return newDateMin;
    },

    get_hours_cater :function (  ) // done
    {
        floor = this.getTimeFloor_cater_only(  );
        return floor;
    },

    getTimeFloor_cater_only: function (  ) // done
    {
        var self =this;
        newDateMin = self.getCurrentDayHour(  );
        
        if(  self.check_if_closed(  ) == 0 )
        {
            newDateMin = self.add24Hours( newDateMin );
        }
        else
        {
            return self.getTimeFloor_closed_and_cater(  )
        }
           
        newDateMin = self.roundMinutes( newDateMin  );
        return newDateMin;
    },

    get_hours_regular : function(  ) // done
    {
        return new Date(  );
    },

    get_min_early_entry: function(  date_time ) // done
    {
        var self = this;
        var open_time = mobile_ha.common.getOpenTime(  );
        var now = new Date(  date_time );

        if( self.is_empty( date_time ) )
            return open_time;
        else
        {
            vs_open = self.compare_times( now, open_time  );
            if( vs_open == "<" )
                return open_time;
            else
                return now;
        }
    },

    

    compareTimes: function( t1, t2 ) // done
    {
       if( this.is_empty( t1 ) == 1 )
            return undefined;

        if( this.is_empty( t2 ) == 1 )
            return undefined;

        date1 = new Date( t1 );
        date2 = new Date( t2 );

        date_comp_1 = new Date( date1 );
        date_comp_2 = new Date( date1 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2 = this.blank_time( date_comp_2 );

        date_comp_1.setHours( date1.getHours(  ) );
        date_comp_1.setMinutes( date1.getMinutes(  ) );

        date_comp_2.setHours( date2.getHours(  ) );
        date_comp_2.setMinutes( date2.getMinutes(  ) );

        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";

        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        
        else
            result = "?";

        return result;
    },

    getCurrentDayHour: function(  ) // done
    {
        var self = this;
        date = (currentDateTime != null)?currentDateTime:self.getCurrentDateFromServer(  );
        date = new Date( date );
        return date;
    },

    get_store_timeslot_ajax: function( dayOfWeekName, address_id, delivery_pickup ) // done
    {
        if(address_id != '')
        {
            currentStoreTimeSlot = $.ajax({
            type: "POST",
            url: SITE_URL + 'checkout/getCurrentStoreTimeSlot/',
            async: false,
            data: {
                deliveryOrPickup: delivery_pickup,
                id: address_id,
                day: dayOfWeekName
            },
            success: function( ajax_response ) {
                if(ajax_response == "false")
                {
                    //alert("Delivery not available in this area");
                    return false;

                }
                else
                {
                    store_timing = setTimeout( function( )
                    {
                        store_timing = JSON.parse(ajax_response);
                        return store_timing; 
                    } , 200);
                    
                    self.timeslot_information = store_timing ;
                    return   store_timing;
                }
                
            }
        });

        return currentStoreTimeSlot; 
        }
        else
        {
            return currentStoreTimeSlot; 
        }
              
    },

    set_date_time : function( date, time  ) // done
    {
        var day = new Date( date );
        var time_split = {};
        
        if( typeof( time ) == 'object' )
        {
            time_split[0] = time.getHours(  );
            time_split[1] = time.getMinutes(  );
        }
        else //string
        {
            time_split = time.split(/\:|\-/g);
        }
        
        day.setHours(time_split[0]);
        day.setMinutes(time_split[1]);
        
        return day;
    },

    calcFloor: function ( type_closed ) // done
    {
        var self = this;
        current = self.getCurrentDayHour(  );
        
        if( type_closed =='closed_only' )
        {
            if( self.compareTimes(  current, mobile_ha.common.getOpenTime( ) ) == "<" )
                    newDateMin =current ;
                else
                    newDateMin = self.date_plus_1( current );
        }
        else
        {
            newDateMin = self.date_plus_1( current );
        }
        
        newDateMin = self.set_date_time( newDateMin, self.format_time( mobile_ha.common.getOpenTime( ) ) );
        
        return newDateMin;
    },

    test24HoursTooLate: function (  ) // done
    {
        var self = this;
        closedTime = mobile_ha.common.getClosedTime(  );
        currentTime = self.getCurrentDateTime(  );
        if( self.compareTimes( currentTime, closedTime ) == '>' )
            return 1;
        else
            return 0;
    },

    date_plus_1 : function( d ) // done
    {
        var date = new Date( d );
        date.setDate( date.getDate() + 1 );
        return date;
    },

    roundMinutes: function( dateObject ) // done
    {
        now = dateObject;
        var mins = now.getMinutes();
        var quarterHours = Math.round(mins/15);
        if (quarterHours == 4)
        {
            now.setHours(now.getHours()+1);
        }   
        var rounded = (quarterHours*15)%60;
        now.setMinutes(rounded);
        return now;
    },

    add24Hours : function( date ) // done
    {
        twenty_four_hours_later = new Date( date.getTime() + 60 * 60 * 24 * 1000 );   
        twenty_four_hours_later = this.roundMinutes( twenty_four_hours_later  );
        return twenty_four_hours_later 
    },

    is_empty: function( thing ) // done
    {
        if( thing == 'undefined'    ||
            thing == undefined      ||
            thing == null           ||
            thing == ''             ||
            thing === false )
            return 1;
        else
            return 0;
    },

    compare_times: function( d1, d2 ) // done
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date1 = new Date( d1 );
        date2 = new Date( d2 );

        date_comp_1 = new Date( date1 );
        date_comp_2 = new Date( date1 );

        date_comp_1.setSeconds( 0 );
        date_comp_1.setMilliseconds( 0 );

        date_comp_2.setSeconds( 0 );
        date_comp_2.setMilliseconds( 0 );

        date_comp_1.setHours( date1.getHours(  ) );
        date_comp_1.setMinutes( date1.getMinutes(  ) );

        date_comp_2.setHours( date2.getHours(  ) );
        date_comp_2.setMinutes( date2.getMinutes(  ) );

        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

        return result;
    },

    checkTimingsRealtedConditions:  function( selectedDate  ) // done
    {
        var self = this;

        if( selectedDate == undefined )
        {
            selectedDate = $( ".delivery_date" ).val(  );
        }
        
        var today_Date;
        var nowDate = (currentDateTimeNow != null)?currentDateTimeNow:self.getCurrentDateFromServer_NOW(  );
        today_Date = new Date ( nowDate );

        var today_DayOfWeek = today_Date.getDayName(  );
      
        var is_delivery = -1;
        is_delivery = $("#order-type").val();      
        
        var address_id;
        var deliveryOrPickup;

        var is_delivery = 1;    // DEFAULT ONLY DELIVERY IN THIS VERSION
     
            if ( is_delivery == 0 )
            {
                deliveryOrPickup = "pickup";
                var pickupStoreId = $("#hidden_store_id").val()
                address_id = pickupStoreId;
            }
            else
            {
                deliveryOrPickup = "delivery";
                var deliveryAddressId =  self.get_active_address(  ); 
                address_id = deliveryAddressId;
            }
            
            var store_timeslot_response;
            if (currentStoreTimeSlot != null && storeID_check == address_id)
            {
                store_timeslot_response = currentStoreTimeSlot;
            }
            else if(storeID_check != address_id )
            {
                store_timeslot_response = self.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
            }
            //var store_timeslot_response = self.get_store_timeslot_ajax( today_DayOfWeek, address_id , deliveryOrPickup ) ;
            
            store_timeslot_response_text = JSON.parse( store_timeslot_response.responseText  );
              
            if( store_timeslot_response_text == null  ||  store_timeslot_response_text === false   ){
                return;
            }

            mobile_ha.common.setOpenTime( self.set_date_time(  today_Date,  store_timeslot_response_text['open'] ) );
            mobile_ha.common.setClosedTime( self.set_date_time(  today_Date,  store_timeslot_response_text['close'] ) );
    
            mobile_ha.common.setOpenHour( store_timeslot_response_text['open'] );
            mobile_ha.common.setCloseHour( store_timeslot_response_text['close'] );
            
            if( store_timeslot_response_text == false || store_timeslot_response_text == null ){
                return false;
            };

            $("#hidden_store_id").val( store_timeslot_response.store_id );
            mobile_ha.common.getSpecificDayForStore( $("#hidden_store_id").val(  ) );
    },

    blank_time: function( d ) // done
    {
        d = new Date(  d );
        d.setHours( 0 );
        d.setMinutes( 0 );
        d.setSeconds( 0 );
        d.setMilliseconds( 0 );
        return d;
    },

    compare_dates: function( d1, d2 ) // done
    {
        if( this.is_empty( d1 ) == 1 )
            return undefined;

        if( this.is_empty( d2 ) == 1 )
            return undefined;

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2  = this.blank_time( date_comp_2 );


        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";

        return result;
    },

    getCurrentDateFromServer: function(  ) // done
    {
        $.ajax({
                type: "POST",
                url: SITE_URL + 'checkout/getCurrentHour/',
                async: false,                   
                success: function(data) {
                    currentDayHour = JSON.parse(data);
                }
            });
                
        var currentHour = parseInt(currentDayHour.hour);
        var currentDay = parseInt(currentDayHour.day);
        var currentDate = currentDayHour.date_time_js;

        currentDate = currentDate.split(",");
        var current_date = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);
        currentDateTime = current_date;
        return current_date;
    }, 

    format_time: function( d ) // done
    {
        var self = this;
        if( self.is_empty( d ) == 1 )
            return undefined; 
        if(typeof d=="string")
        {
            if(d.indexOf("GMT ")){
                d=d.replace("GMT ", "GMT+");
                d=new Date(d);
            } 
        }
        date = new Date(  d  );
        display_hours = date.getHours();
    
        var time_string = self.pad(  date.getHours() , 2) + ":" + self.pad( date.getMinutes(), 2);
        return time_string;
    },

    getCurrentDateFromServer_NOW: function(  ) // done
    {
        $.ajax({
                type: "POST",
                url: SITE_URL + 'checkout/getCurrentHour/',
                async: false,                   
                success: function(data) {
                    currentDayHour = JSON.parse(data);
                }
            });
                
         
        var currentHour = parseInt(currentDayHour.hour);
        var currentDay = parseInt(currentDayHour.day);
        var currentDate = currentDayHour.date_time_js;
        
        currentDate = currentDate.split(",");
        var current_date = new Date(currentDate[0], currentDate[1] - 1, currentDate[2], currentDate[3], currentDate[4], currentDate[5]);
        currentDateTimeNow = current_date;
        return current_date;
    },

    get_active_address: function () // done
    {
        var self = this;
        if ( !$("#address-id").val() ) {
            return setTimeout(self.get_active_address, 1000);
        }
        return $("#address-id").val();
        // do work here
    },

   

    handle_user_time_pick_first_run: function( tsa ) // done
    {
        var self = this;
       
        if( mobile_ha.common.getTimepicked(  ) == 0 )
            return;

        valid_time_or_zero = self.get_valid_user_time_pick( tsa );

         mobile_ha.common.validation_to_cookies( valid_time_or_zero );        // clear cookies, etc
         mobile_ha.common.validation_to_global( valid_time_or_zero );
        return valid_time_or_zero;
    },

    get_valid_user_time_pick: function ( timeStateArray ) // done
    {
        var self = this;
        user_date = mobile_ha.common.getSpecificDate(  );
        user_time = mobile_ha.common.getSpecificTime(  );

        
        if( self.is_empty( user_date ) == 1 || self.is_empty( user_time ) == 1 )
        {
            validation = 0;
        }
        else
        {
            validation = self.validate_user_pick( user_date, user_time, timeStateArray );
        }

        return validation;
    },

    validate_user_pick: function( date, time, timeStateArray ) // done
    {
        var self = this;
        cookie_date = self.set_date_time( date, time );
        
        date_cookie     = cookie_date;
        date_cookie     = cookie_date;
        var now_date = self.getCurrentDateTime( );
        var date_open;
        var date_closed;
        if( self.compareDates( date_cookie, new Date(  ) ) == '=' )
        {
            date_open       = mobile_ha.common.getOpenTime(  );
            date_closed     = mobile_ha.common.getClosedTime(  );
        }
        else
        {
            times = self.getOpenCloseFromDate( date_cookie );
            date_open = times.open_time;
            date_closed = times.close_time;
        }
        date_current    = self.getCurrentDayHour(  );
        date_floor      = timeStateArray.timeFloor;
        
        if(  self.is_empty( date_floor ) == 1 )
            date_floor = mobile_ha.common.getOpenTime(  );
        
        vs_date_floor  =  self.compareDates( date_cookie, date_floor );
        vs_time_floor  =  self.compareTimes( date_cookie, date_floor );
        
        if( vs_date_floor == "<" )
            return 0;
        if( vs_date_floor == "=" )
        {
            if( vs_time_floor == "<" )
                return 0;
        }

        // first check if this is today or a future day...
        vs_current_date = self.compare_dates( date_cookie, date_current );
        vs_open_time = self.compareTimes( date_cookie, date_open );
        vs_closed_time = self.compareTimes( date_cookie, date_closed );

        if( vs_current_date == "<" )        // This is before today, so not works. 
            return 0;
        if( vs_open_time == "<"/* || vs_open_time == "="*/)
            return 0;
        if( vs_closed_time == ">"/* || vs_closed_time == "="*/)
            return 0;

        var vs_current_time = '';
        if( vs_current_date == "=" )
        {
            vs_current_time = self.compareTimes( date_cookie, date_current );
            if( vs_current_time == "<" ){   // same day but before current time, so invalid. 
                return 0;}
        }

        if( vs_current_date == "?" || vs_open_time == "?" || vs_closed_time == "?" || vs_current_time == "?")
        {
            return 0;
        }

        return cookie_date;
    },

    compareDates: function( d1, d2 ) // done
    {
        if( checkout.is_empty( d1 ) == 1 )
            return undefined;

        if( checkout.is_empty( d2 ) == 1 )
            return undefined;

        date_comp_1 = new Date( d1 );
        date_comp_2 = new Date( d2 );

        date_comp_1 = this.blank_time( date_comp_1 );
        date_comp_2  = this.blank_time( date_comp_2 );

        date_comp_1.setHours( 0 );
        date_comp_1.setMinutes( 0 );
        date_comp_1.setSeconds( 0 );
        date_comp_1.setMilliseconds( 0 );

        date_comp_2.setHours( 0 );
        date_comp_2.setMinutes( 0 );
        date_comp_2.setSeconds( 0 );
        date_comp_2.setMilliseconds( 0 );


        var result = "";
        if( date_comp_1.getTime(  ) > date_comp_2.getTime(  ) )
            result = ">";
        else if ( date_comp_1.getTime(  ) < date_comp_2.getTime(  ) ) 
            result = "<";
        else if ( date_comp_1.getTime(  ) === date_comp_2.getTime(  ) ) 
            result = "=";
        else
            result = "?";
            
            
        return result;
    },

    last_chance_specific_day : function( timeStateArray ) // done
    {
        var self = this;
        timeStateArray.closedTodayOverride = 0;
        
        picked_date_vs_bad_date = self.compareDates( timeStateArray.timeFloor, self.specific_day  );
        
        mobile_ha.common.getSpecificDayForStore( $("#hidden_default_store_id").val( ) );
        if( picked_date_vs_bad_date == "=" )
        {
            timeStateArray.closedTodayOverride = 1;
            timeStateArray.timeFloor = self.add24Hours( timeStateArray.timeFloor );
        }

        var times = self.getOpenCloseFromDate( timeStateArray.timeFloor );

        var open_time = times.open_time;
        var close_time = times.close_time;
        var store_open_today = times.store_closed_on_day;
        

        // This is all basically to test if the store is closed today, and if the user picked today or not picked at all therefore default to today.
        var user_time_validated =  self.getUserTimeValidated(  );
        var test_time_against = 0;
        if (  user_time_validated == 0 )
            test_time_against = 0;
        else
            test_time_against = times.open_time;

        // this is also changed to make the closed on specific day work.
        if(  store_open_today == 0 && 
        ( user_time_validated == 0 ? 1 :  ( self.compareDates( user_time_validated , test_time_against ) == "=" ) ) )
        {
            timeStateArray.closedTodayOverride = 1;
            timeStateArray.timeFloor = self.add24Hours( timeStateArray.timeFloor );
            self.set_checkout_message( "Sorry, we're currently closed. Please choose a future order time." );
            timeStateArray.day_closed = times.day_of_week;
            timeStateArray.state = "Closed";
            timeStateArray.msg = "Sorry, we're currently closed. Please choose a future order time.";
            timeStateArray.nowDisabled = true;
        }

        // and do one now.
        var times = self.getOpenCloseFromDate( new Date(  ) );
        store_open_today = times.store_closed_on_day;
        if(  store_open_today == 0 && 
        ( self.compareDates( self.getUserTimeValidated(  ) , times.open_time ) == "=" ))
        {
            timeStateArray.closedTodayOverride = 1;
            timeStateArray.msg = "Sorry, we're currently closed. Please choose a future order time.";
            timeStateArray.nowDisabled = true;
        }
        
        return timeStateArray;
    },

    no_now_on_specific_day: function( timeStateArray ) // done
    {
        var self = this;
        today_date_vs_bad_date = self.compareDates( new Date( ), self.specific_day  );
        
        if(  today_date_vs_bad_date == "=" )
        {
            timeStateArray.closedTodayOverride = 1;
            timeStateArray.nowDisabled = true;
            timeStateArray.nowHidden = true;
        }

        return timeStateArray;
    },

    limit_select_times_by_date: function( floor_date_time, onLoad = false ) // done
    {
        var self = this;
        //self.getTimeAvilableSlots(  );
        
        var picked_date = new Date( $( ".delivery_date" ).val(  ) );
       
        var changeToCurrentday = false;

        if(onLoad == true && self.compareDates( picked_date, new Date(  ) ) != '='){
            changeToCurrentday = true;
        }

        if( $('.timechange select').val()  == 'undefined' || $('.timechange select').val()  == undefined )
        {
            picked_date = self.set_date_time( picked_date, '08:00' );
        }
        else
        {
            picked_date = self.set_date_time( picked_date, $('.timechange select').val() );
        }
        
        var now_date = self.getCurrentDateTime( );
        var open_time;
        var close_time;

        var store_open_today = undefined;
        
        if( self.compareDates( picked_date, new Date(  ) ) == '=' ) 
        {
            open_time = mobile_ha.common.getOpenTime(  );
            close_time = mobile_ha.common.getClosedTime(  );

            // This is new line because picking smae date doesnt properly set floor date. 6-22-18
            if( self.compareTimes(  now_date, open_time ) == ">" )
            {
                var hour = now_date.getHours();
                var newDate = now_date.setHours(hour + 1);
                
                open_time = newDate;
            }

        }
        else
        {
            times = self.getOpenCloseFromDate( picked_date );
            open_time = times.open_time;
            close_time = times.close_time;
            store_open_today = times.store_closed_on_day;
        }
        
        if(  store_open_today == 0 )
        {
            var curr_dt = new Date();
            var weekdays = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

            if(changeToCurrentday == true)
            {
                times.day_of_week = weekdays[curr_dt.getDay()];
            }

            mobile_ha.common.set_validation_blocker( "Sorry, we're currently closed. Please choose a future order time.");
            self.set_checkout_message( "Sorry, we're currently closed. Please choose a future order time." );
            
        }
        else
        {
            mobile_ha.common.clear_validation_blocker(  );
        }
        

        //var timeStateArray  = self.getTimeStateArray( );
        var timeStateArray  = mobile_ha.common.time_state_array;
        
        if( floor_date_time == undefined ||  floor_date_time == 'undefined'  ||  floor_date_time == ''  )
        {
            if( self.compare_dates( picked_date, timeStateArray.timeFloor ) == "=" )
            {
                floor_date_time =  timeStateArray.timeFloor;   
            }
            else
            {
                floor_date_time = self.set_date_time( picked_date, self.format_time( open_time )  );
            }
        }
        else
        {
            if( timeStateArray.state == 'Cater' ||  timeStateArray.state == 'ClosedCater' )
            {
                // because if we are using catering, then we can't use the "getTimeStateArray" open time as a floor.  But we have already calc'd a floor so...
             
            }
            else{
                floor_date_time = self.set_date_time( floor_date_time, self.format_time( open_time )  );
            }
        }

        var floor_date_time = new Date( floor_date_time );
        
        time_state_array_datetime = floor_date_time;
        if( picked_date.getHours(  ) == 0 )
        {
            picked_date = self.set_date_time( picked_date, self.format_time( floor_date_time )  );
        }
       
        if(  checkout.is_empty(floor_date_time) == 1 || floor_date_time == 'Invalid Date' )
        {
            picked_date = self.set_date_time( picked_date, '08:00' );
        }
        

        if( self.compareDates( picked_date , floor_date_time ) == "<" )
        {
            // this.set_html_date( floor_date_time );
            self.limit_select_times_by_date( floor_date_time );
            picked_date = $( ".delivery_date" ).val(  );
        }
        else if( self.compareDates( picked_date , floor_date_time ) == "=" )
        {
            $(".timedropdown option").each(function()
            {
                var index_time = $(this).val();
                var index_date_time = self.set_date_time( picked_date, index_time );
                var vs_index_time_and_floor_time = self.compareTimes( index_date_time, floor_date_time );
                

                if( vs_index_time_and_floor_time == "<" )
                {
                    $( this ).remove(  );
                }

                var vs_index_time_and_closed_time = self.compareTimes( index_date_time, close_time );
                
                if( vs_index_time_and_closed_time == ">" )
                {
                    $( this ).remove(  );
                }


                var vs_index_time_and_open_time = self.compareTimes( index_date_time, open_time );              
                if( vs_index_time_and_open_time == "<" )
                {
                    $( this ).remove(  );
                }

            });
        }

        else if( self.compareDates( picked_date , floor_date_time ) == ">" )
        {
            $(".timedropdown option").each(function()
            {
                var index_time = $(this).val();
                var index_date_time = self.set_date_time( picked_date, index_time );
               
                vs_index_time_and_open_time = self.compareTimes( index_date_time, open_time );
                if( vs_index_time_and_open_time == "<" )
                {
                    $( this ).remove(  );
                }

                vs_index_time_and_closed_time = self.compareTimes( index_date_time, close_time );
                if( vs_index_time_and_closed_time == ">" )
                {
                    $( this ).remove(  );
                }
            });
            
        }
        
        if( self.compareTimes(  close_time, now_date  ) == ">" ||  self.compareTimes(  close_time, now_date ) == "=")
        {
            newDateMin = self.date_plus_1( now_date );
            self.set_date_time( newDateMin, self.format_time( mobile_ha.common.getOpenTime( ) ) );
        }
        $( 'select.timedropdown option' ).each( function(  )
        {
            if( $($(this)).val(  ) == "12:00" )
                $($(this)).prop( 'selected',true );
        } );
        
    },

    change_html_elements_for_time : function( time_state_array ) // done
    {
        var self = this;
        if( time_state_array.nowHidden == true )   // HIDE now.
        {
            this.show_date_radio(  );
            $('#front_page_delivery_time').text( self.format_date_time_for_display( time_state_array.set_date_time ) );
            if (navigator.userAgent.match(/(ipod|iPhone|iPad)/)) {
                $('#front_page_delivery_time').html( self.format_date_time_for_display( time_state_array.set_date_time ) );
            }
        }
        else        // SHOW now
        {
            this.show_now_radio(  );
            $("#enter_datetime_seelction").removeClass("first-time");
             $('#front_page_delivery_time').text( 'NOW' );
             if (navigator.userAgent.match(/(ipod|iPhone|iPad)/)) {
                $('#front_page_delivery_time').html( 'NOW' );
            }

        }

        if( time_state_array.nowDisabled == false )   // is active
        {
            $('#radio1').data('err-msg', '' ); //setter
            $('#radio1').data('data-off', 'ENABLE' ); //setter
        }
        else
        {
            $('#radio1').data('err-msg', time_state_array.msg ); //setter
            $('#radio1').data('data-off', 'DISABLE' ); //setter
        }
        if( !time_state_array.user_validated_time  && ( self.specific_message && self.compareDates( self.specific_day, new Date(  ) ) == "="  ))
        {
            self.set_checkout_message( self.specific_message );
            $("#radio2").data( 'err-msg', self.specific_message  );
        }
        else if( time_state_array.user_validated_time  && ( self.specific_message && self.compareDates( self.specific_day, time_state_array.user_validated_time ) == "="  ))
        {
            self.set_checkout_message( self.specific_message );
            $("#radio2").data( 'err-msg', self.specific_message  );
        }
        else
        {
            if ( time_state_array.msg )
            {
                // possible messages: closed today, deliveries off, specific day ( these are hit earlier ), closed cater.
                if( self.compareDates( time_state_array.user_validated_time, time_state_array.timeFloor ) == ">"    )
                {

                }
                else
                {
                    $("#radio2").data( 'err-msg', time_state_array.msg  ) ;
                    self.set_checkout_message( time_state_array.msg );
                 }
            }
            else
            {
                self.clear_checkout_message(  );
            }
        }
    },

    figure_out_element_states:function( time_state_array ) // done
    {
        var self = this;
        if(  time_state_array.state == "None" )
        {
            time_state_array.nowDisabled = false;
            if( self.getUserTimeValidated(  ) != 0 )
            {
                time_state_array.nowHidden = true;
            }
            else
            {
                time_state_array.nowHidden = false;
            }
        }
        else
        {
            time_state_array.nowDisabled = true;
            time_state_array.nowHidden = true;

            if( time_state_array.state == 'Cater' || time_state_array.state == 'ClosedCater'  )
                time_state_array.msg = 'Sorry! You have an item in your cart that requires 24-hour advance ordering.  Please select a future date and time.  Thank you! ';

            if( time_state_array.state == 'Closed'  )
            {
                var msg = '';
                if( time_state_array.msg ){
                    msg =  time_state_array.msg;
                }
                else{
                    var formated_open_time=self.format_time( mobile_ha.common.getOpenTime(  ) );
                    var formated_close_time=self.format_time( mobile_ha.common.getClosedTime(  ) );

                    if( self.compareTimes(  self.format_date_time( time_state_array.user_validated_time ), self.format_date_time(  mobile_ha.common.getOpenTime(  ) ) ) == ">" )
                    {
                        // validated its early and not open yet.
                    }
                    else
                    {
                        var msg = "Sorry, we&apos;re currently closed. <br/>Please choose a future order time.";
                        //var msg = "SORRY, WE&apos;RE CURRENTLY CLOSED. HOURS: " + this.tConvert(formated_open_time)  + " - " + this.tConvert(formated_close_time) + " <br/>SELECT A FUTURE ORDER TIME BELOW.</span>";
                    }
                }
                time_state_array.msg = msg;
            }

            if( self.specific_message && self.compareDates( self.specific_day, new Date(  ) ) == "=" )
            {
                   var msg = self.specific_message;
                   time_state_array.msg =  self.specific_message;
            }
        }
        return time_state_array;
    },

    set_checkout_message: function( txt ) // done
    {
        var msg = "<span class='checkout-msg-red'>" + txt + "</span>";
        $("div#enter_datetime_seelction").addClass("first-time");
        $("div#enter_datetime_seelction h2").html(msg);

        if (navigator.userAgent.match(/(ipod|iPhone|iPad)/)) {
            $('#front_page_delivery_time').text( msg );
            $('#front_page_delivery_time').html( msg );
        }
    },

    format_date_time_for_display: function( d ) // done
    {
        var self = this;
        if( self.is_empty( d ) == 1 )
            return undefined; 

        date_string = self.format_date( d );
        d = new Date( d );
        hours = d.getHours(  );
        min = d.getMinutes( );
        std_hours = self.military_hours_to_standard(  hours );
    
        ampm = self.get_am_pm_from_military_hour( hours );     
        time_string = std_hours + ":" + this.pad( min, 2 ) + ampm;

        return date_string + " " + time_string;
    },

    show_now_radio: function (  ) // done
    {
        $("#radio2").prop( "checked", false );
        $("#radio1").prop( "checked", true );
        $(".date_time_wrapper").hide(); 
    },

    clear_checkout_message: function(  ) // done
    {
        $('.checkout-msg-red').remove();
    },

    getUserTimeValidated: function(  ) // done
    {
        if( mobile_ha.common.getCookie("user_date_validated") ){
            return JSON.parse( mobile_ha.common.getCookie("user_date_validated") );
        }
        else
            return 0;
    },

    format_date_time: function( d ) // done
    {
        var self = this;
        if( self.is_empty( d ) == 1 )
            return undefined; 

        date_string = self.format_date( d );
        time_string = self.format_time( d );

        return date_string + " " + time_string;
    },

    format_date: function( d ) // done
    {
        var self = this;
        if( self.is_empty( d ) == 1 )
            return undefined; 

        date = new Date(  d  );
        var date_string = self.getAlphaDay(date.getDay())+", "+self.getAlphaMonth(date.getMonth())+" "+date.getDate()+", "+date.getFullYear();
        return date_string;
    },

    military_hours_to_standard:function( display_hours ) // done
    {
        display_hours = parseInt(  display_hours ) ;
        if( display_hours > 12 )
            display_hours = display_hours - 12;
        return display_hours;
    },

    get_am_pm_from_military_hour: function( hours ) // done
    {
        if( hours >= 12 )
            return "PM";
        else
            return "AM";
    },

    getAlphaDay : function(day) // done
    {
        var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        return days[day];
    },

    getAlphaMonth : function(month) // done
    {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return monthNames[month];
    },

    limit_datepicker_min_date: function ( time_state_mode, timeStateArray )
    {
        var floorDate = timeStateArray.timeFloor;
        self = this;
        if( time_state_mode =='Cater' || time_state_mode == 'ClosedCater' ||  time_state_mode == 'Closed') 
        {
            // if not too early and not overridden
            if( timeStateArray.tooEarly == 1 &&  timeStateArray.closedTodayOverride !== 1)
            {

            }
            else {
                $('.delivery_date').datepicker('option', {minDate: new Date(floorDate)});
            }
        }
        else
        {
            $(".delivery_date").datepicker('option', {minDate: 0});   
        }
    },

    limit_datepicker_on_closed_days: function(timeStateArray) // done
    {
        var self = this;
        var store_id = timeStateArray.store_id;
        mobile_ha.common.getSpecificDayForStore(store_id);
        $.ajax({
            type: "POST",
            data: {
                "store_id": store_id
            },
            dataType:'json',
            url: SITE_URL + 'checkout/get_storeClosed_days',
            beforeSend: function() {
                $("#ajax-loader").show();
            },
            complete: function() {
                $("#ajax-loader").hide();
            },
            success: function(e) 
            {
                if(e != false)
                {
                    $('.delivery_date').datepicker('option',
                    {
                        beforeShowDay: function(d) 
                        {
                            var is_specficHoliday = self.disable_specific_dates(d);
                            if(is_specficHoliday){
                                var day = d.getDay();
                                return [e.indexOf(day) == -1];
                            }else{
                                return [false];
                            }
                            
                        }
                    });
                }
            }
        });
    },

    disable_specific_dates: function( date ) // done
    {
        var self = this;
        if(  mobile_ha.common.specific_day  == "" )
        {
            return 1;
        }

        var bad_date = new Date(  mobile_ha.common.specific_day );
    
        var m_bad = bad_date.getMonth();
        var d_bad = bad_date.getDate();
        var y_bad = bad_date.getFullYear();
         
        var m = date.getMonth();
        var d = date.getDate();
        var y = date.getFullYear();
        
        var currentdate = (m + 1) + '-' + d + '-' + y ;
        var bad_date_formatted =  (m_bad + 1) + '-' + d_bad + '-' + y_bad ;
        if( currentdate == bad_date_formatted  )
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }, 

    pick_the_actual_time_to_set_in_datetime_picker: function( time_state_array ) // done
    {
        var self = this;
        floor_time = time_state_array.timeFloor;
        user_time = self.getUserTimeValidated(  );
        now_time = self.getCurrentDateTime(  );
        
        var hour = now_time.getHours();
        var newDate = now_time.setHours(hour + 1);      
        if( user_time != 0 )
        {
            return user_time;
        }
        else
        {
            if( floor_time != 0 )
            {
                return self.noon_or_greater( floor_time );
            }
            else 
            {
                return self.noon_or_greater( new Date(newDate) );
            }
        }
    },

    noon_or_greater:function ( day_time ) // done
    {
        var self = this;
        user_time = new Date( day_time );
        noon_time = new Date( day_time );
        return_time = new Date( day_time );

        noon_time = self.set_date_time( noon_time, "12:00" );

        user_vs_noon = self.compare_times( user_time, noon_time );
        
        if( user_vs_noon == ">" ){
            return_time = self.set_date_time( day_time, user_time );
        }
        else{
            return_time = self.set_date_time( day_time, "12:00" );
        }

        return return_time;
    },

    set_open_floor : function( time_state_array ) // done
    {
        if(  this.is_empty( time_state_array.timeFloor ) )
            time_state_array.timeFloor = mobile_ha.common.getOpenTime(  );
        return time_state_array;
    },

    set_time_in_html_element:function(  time_state_array ) // done
    {
        this.set_time_html( time_state_array.set_date_time );
    },

    set_time_html : function( newTime ) // done
    {
        var d = new Date( newTime );
        var new_time = this.get_time_from_date(d);
        
        $('.timechange >select option[value="' + new_time + '"]').prop('selected', true);
        
        if( $(".timedropdown").val(  )  == null || $(".timedropdown").val(  )  == "null"){
        }
    },

    set_date_in_html_element: function( timeStateArray ) // done
    {
        this.set_date_html(   timeStateArray.set_date_time );
    },

    set_date_html: function( newDate ) // done
    {
        d = new Date( newDate );
        $(".delivery_date").val($.datepicker.formatDate('D, M d, yy', d ));
    },

    set_front_initial_run: function ( time_state_array ) // done
    {
        var self = this;
        if( $('#time_select_front_page').text(  ) == 'NOW' )
        {
            if( self.is_empty( time_state_array.user_validated_time ) == 0 )
            {
                $('.checkout-msg-red').remove(); 
                $("div#enter_datetime_seelction h2").html( self.format_date_time_for_display( time_state_array.user_validated_time ) );
            }
            else
            {
                if( ( time_state_array.state == 'Closed' || time_state_array.state == 'ClosedCater' )  )
                {
                    $('.checkout-msg-red').remove(); 
                    var msg = "<span class='checkout-msg-red'>"+time_state_array.msg+"</span>";
                    
                    $("div#enter_datetime_seelction").addClass("first-time");
                    $("div#enter_datetime_seelction h2").html(msg);
                    setTimeout(function(){
                        $("div#enter_datetime_seelction").addClass("first-time")
                        $('.place-order-checkout').addClass('disabled');
                    }, 500); 
                }
                
                else
                {
                    $('.checkout-msg-red').remove(); 
                    $("div#enter_datetime_seelction").removeClass("first-time");
                }

            }
        
        }      
    },

    get_time_from_date: function( date_time ) // done
    {
        if( typeof( date_time ) == "object" )
        {
            result = ( this.pad( date_time.getHours(  ), 2 ) + ":" + this.pad(date_time.getMinutes(  ),2) );
            return result;
        }
        else
        {
            var timeRegex = /([01]\d|2[0-3]):([0-5]\d)/;
            var match = timeRegex.exec( date_time );
            time = match[0];
            return time;
        }
    },

    pad : function (num, size)  // done
    {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    },

    get_all_possible_times: function(  )
    {
        buffer = '';
        for( hour = 8; hour < 24; hour ++ )
        {
            for( minute = 0; minute <60; minute = minute + 15  )
            {
                time_text = this.pad( hour, 2 ) + ":" + this.pad( minute, 2);
                time_new = this.tConvert( time_text ); 
                buffer = buffer + '<option value="' + time_text  + '">' +  time_new +  '</option>';
            }
        }
        return buffer;
    },

    tConvert: function (time) // done
    {
       // Check correct time format and split into components
       time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) 
        {  
            time = time.slice (1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        //return time; // return adjusted time or original string
        return time[0] + ":" + time[2] + time[5];
    },

    //~checking if any 3-foot of 6-foot is existing in order. if there is then you get a TRUE else a FALSE.
    checkBreadSimple : function() // done
    {
        var self = this;
        var cateredBread = false

        $('.bread_type').each(function(){ 
        
            if( $(this).val() > 0) {  
                cateredBread = 1;
            } 
        });
        if( cateredBread == 1 )
            return 1;
        else
            return 0;
    },

    show_date_radio: function (  )
    {
        $("#radio1").prop( "checked", false );
        if(window.location.href.indexOf("createsandwich") == -1){ 
         $("#radio2").prop( "checked", true );
        }
        $(".date_time_wrapper").show(); 
    },

}
}
