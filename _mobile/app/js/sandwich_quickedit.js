$(document).ready(function () {
	
	var category_items;

	function renderCategoryItems(data){

		var category_items_html = '';
		// to display the first protein item by default
        if (data != null && data.length > 0) {

        	category_items = data;
            $.each(data, function(i, category_item){
                console.log("category_item"+i, category_item);
                category_items_html += '<option value="'+ category_item.id +'" data-category_name="'+ category_item.category_name +'" data-empty="false" data-priority = "'+ category_item.image_priority + '" data-id="'+ category_item.id + '" data-price="'+ category_item.item_price + '" data-image_trapezoid="'+ category_item.image_trapezoid +'" data-image_round="'+ category_item.image_round + '" data-image_long="'+ category_item.image_long + '"  data-itemname="'+ category_item.item_name +'" id="protein-check'+ i +'">'+ category_item.item_name + '</option>';
            })
        }

        renderItemOptions(data[0]);
 
        $('#add-item-select').html(category_items_html);
	}

	function renderItemOptions(data){
       
		 var options_html = ''; 

        // to display the first protein item options by default
        if(data.options_id != null && data.options_id.length > 0){
            
            $.each(data.options_id, function(i, option){
                var tImg = "", rImg = "", lImg = "";
                var option_id = option.id;
                if(i == 0){
                    var current = "true";
                    var display = "block";
                }else{
                    var current = "false";
                    var display = "none";
                }

                if(data.option_images.hasOwnProperty(option_id)){
                    if(typeof data.option_images[option_id]['tImg'] !== "undefined"){
                        tImg = data.option_images[option_id]['tImg'];
                    }
                    if(typeof data.option_images[option_id]['rImg'] !== "undefined"){
                        rImg = data.option_images[option_id]['rImg'];
                    }
                    if(typeof data.option_images[option_id]['lImg'] !== "undefined"){
                        lImg = data.option_images[option_id]['lImg'];
                    }
                }

                options_html += '<input rel="slide" readonly style="display:'+ display +'" data-unit="'+ option.option_unit +'" data-current="'+ current +'" data-image="" data-image_trapezoid="'+ tImg +'" data-image_round="'+ rImg +'" data-image_long="'+ lImg +'" data-price_mul="'+ option.price_mult +'"  type="text" name="" class="text-box" value="'+ option.option_name.toLowerCase() + '"/>';
            })
         }

         $('#sandwiches_quick_edit_add_item .text-box').remove();
         $('#sandwiches_quick_edit_add_item .new_popup-spiner__edit .left_spinner').after(options_html);
	}

	//open add item popup on quick edit popup
	$('#sandwiches_quick_edit a.open-add-item').click(function(e){
        e.preventDefault();

	    $.ajax({
	        type: "POST",
	        url: SITE_URL + 'createsandwich/getSandwichCategories',
	        dataType: 'json',
	        success: function(data) {
	            console.log("data", data);
	            var category_html = '';

	            $('#add-item-category-select').val('');
	            $('#add-item-select').val('');

	            // to display all the categories 
	            if (data.category != null && data.category.length > 0) {
	                $.each(data.category, function(i, category){
	                    console.log("Category"+i, category);
	                    if(category.category_identifier != "BREAD")
	                    category_html += '<option value="'+ category.id +'">'+ category.category_identifier+ '</option>';
	                })
	            }

	            // to display the protein items by default
	            renderCategoryItems(data.category_items);
	            
	            $('#add-item-category-select').html(category_html);

               	$("#sandwiches_quick_edit_add_item").show();
	        }
	    })

	    mobile_ha.sandwichcreator.user_selections_on_quick_edit();
	    
	 })

	//on change of category in dropdown
	$('#sandwiches_quick_edit_add_item #add-item-category-select').change(function(e){

		var category_id = $('#add-item-category-select option:selected').val();

		$.ajax({
	        type: "POST",
	        url: SITE_URL + 'createsandwich/getSandwichCategoryItems',
         	data: { 'category_id' : category_id},
	        dataType: 'json',
	        success: function(data) {
	        	console.log(data);

	        	// to display the category items list
	            renderCategoryItems(data);
	        }	
	    })
	})

	//on change of category items in dropdown
	$('#sandwiches_quick_edit_add_item #add-item-select').change(function(e){
		var index = $('#add-item-select option:selected').index();

		// to display the item options
		renderItemOptions(category_items[index]); 
	})

	//on change of bread item on dropdown
	$('#sandwiches_quick_edit #bread_drpdown').change(function(){
		var e = mobile_ha.sandwichcreator;
		console.log($(this).val());
		e.vars.ACTIVE_MENU = 'BREAD';
		data = $('#sandwiches_quick_edit #bread_drpdown option:selected').data();

		$num = e.ing_availaility_check();

		e.vars.ingredient_details["BREAD"].type = data.bread_type;
        e.vars.ingredient_details["BREAD"].shape = data.shape;

        if ($num > 0) {
            e.vars.ingredient_details["BREAD"].item_image[0] = data.item_image_sliced;
        }
        e.update_json_data('quick_edit');

        $image = e.select_image_by_shape(data, $num);

        image_url = e.vars.image_path + $image;
        $name = data.itemname;
        $price = data.price;
        $sliced_image = data.item_image_sliced;
        $id = data.id;
        $priority = data.priority;

		itm_price = parseFloat(data.price);
        e.vars.current_sliced_image = $sliced_image;
        className = e.Get_imageWrapperClassName();
        var  bread_img = $($(className + " .image-holder")[0]).find("img");
        
        $(bread_img).hide(0);
        $(bread_img).attr("src", image_url);
        
        e.on_complete_actions(true, $name, $price, $image, $id)
	});

	$('#sandwiches_quick_edit a#save-quickedit-sandwich').unbind("click").on("click", function(e) {
		e.stopPropagation();
		e.preventDefault();

		if ($(this).hasClass('disable')) {
            alert("Adding to cart is in progress please wait!");
            return false;
        }

        var active;

        $(this).addClass('disable');
     	active = 1;
        retchk = true;

        is_pub = 1 // by default
        name_cr = $('input[name="sandwich_name"]').val();
        name_cr = name_cr.toUpperCase();
        var is_saved = $(this).data('issaved');

        mobile_ha.sandwichcreator.sync_data_to_db(is_pub, name_cr, active, mobile_ha.sandwichcreator.temp_name, retchk, $(this), "quick_edit", is_saved );
	})
})

// End of Document ready function

//on click of item remove in edit sandwich popup
function removeItems_quickedit(ele){
	var data = $(ele).data();
	mobile_ha.sandwichcreator.vars.ACTIVE_MENU = data.category;
	$id = data.id;

	mobile_ha.sandwichcreator.remove_items($id);
	mobile_ha.sandwichcreator.reload_state_quick_edit();
	return
}

function editQuantity_quickEdit(ele){

	var e = mobile_ha.sandwichcreator;

    $parent = $(ele).parent();
    var category = $(ele).parent().next().data('category');

    e.vars.ACTIVE_MENU = category;
    e.vars.ingredient_details[category].manual_mode = true;

    $ptext = $parent.parent().parent().find('input[type="checkbox"]').data();
    $pele = $parent.parent().parent().find('input[type="checkbox"]');
    $p_priority = $ptext.priority;

    $total = $parent.find("input").length - 1;
    $inputs = $parent.find("input");
    $curr_index = $parent.find("input:visible").index();
    console.log("curr_index", $curr_index);
    $curr_index_t = $curr_index + 1;
    $curr_index = $curr_index - 1;
    if ($curr_index > 0) {
        $prv_index = $curr_index - 1
    } else {
        $prv_index = 0
    }
    if ($curr_index < $total) {
        $nxt_index = $curr_index + 1
    } else {
        $nxt_index = $total
    }

    if ($(ele).attr('class').indexOf("right") != -1) {
        $parent.find("input:visible").hide();
        $parent.find("input:eq("+ $nxt_index +")").show();
        $ind = $parent.find("input:visible").index();
    } else {
        $parent.find("input:visible").hide();
        $parent.find("input:eq("+ $prv_index +")").show();
        $ind = $parent.find("input:visible").index();
    }

    $arr = [];
    $value = $parent.find("input:visible").val();
    $data = $parent.find("input:visible").data();

    $num = e.ing_availaility_check();
    $image = e.select_image_by_shape($ptext, $num, $data);
    image_url = e.vars.image_path + $image;

    if(category == 'PROTEIN') $arr[0] = $value.split(' (')[0];
    else $arr[0] = $value;
    $arr[1] = $data.unit;
    $arr[2] = $data.price_mul;
    $arr[3] = $data.image;
     $arr[4] = $data.optionid;
   
   	e.vars.ingredient_details[category].item_qty[$ptext.itemname] = $arr;
	$itn_id = $ptext.id;
    new_price = parseFloat($ptext.price) * parseFloat($arr[2]);
    $p_index = $.inArray($itn_id, e.vars.ingredient_details[category].item_id);
    if ($p_index != -1) {
        e.vars.ingredient_details[category].item_price[$p_index] = new_price;
        e.calculate_price();
    }

    e.set_images_in_position($p_priority, image_url, $pele);
    e.on_complete_actions(false, $ptext.itemname, $ptext.price, $image, $ptext.id, $p_priority);
    e.auto_select_option_fix();
    

}