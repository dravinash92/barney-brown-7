<?php
session_start();
//FACEBOOK ACCESS CLASS

require_once 'fb_sdk/autoload.php';


use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\Entities\AccessToken;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;




class FB_API 
{
    
    
    public  $scope      =  array('email,publish_actions,user_friends');
    public  $redirURI   = FB_REDIR_URL;
    public  $helper;
    public  $appid      = FB_APP_ID; 
    public  $secret     = FB_APP_SECRET;
    public  $isValidTkn = false;
    public  $redirpaths = '';
    
    
    public function setSession(){
    	FacebookSession::setDefaultApplication($this->appid,$this->secret);
    	
    }
    
    
    public function FB_check_login()
    {
        
       
        
        FacebookSession::setDefaultApplication($this->appid,$this->secret);
        $this->helper = new FacebookRedirectLoginHelper($this->redirURI);
        $this->helper->disableSessionStatusCheck();
        
         if(isset($_SESSION['FBRLH_state']) && $_SESSION['FBRLH_state']  ){
   	      	$cookietime = time()+(86400 * 30 * 3);
   	      	setcookie("FBRLH_state", $_SESSION['FBRLH_state'], $cookietime, '/', NULL);
   	      }

     
        // login helper with redirect_uri
        
        try {
            $session = $this->helper->getSessionFromRedirect();
        }
        catch (FacebookRequestException $ex) {
            // When Facebook returns an error
        	print_r($ex->getMessage());
        }
        catch (Exception $ex) {
        // When validation fails or other local issues
        	print_r($ex->getMessage());
        }
        
       
        
        // see if we have a session
        if (!isset($session)) {
            
        	return false;
            
        } else {
            
            $_SESSION['access_token'] = $session->getToken();
            
            return true;
        }
        
        
    }
    
    function get_logout_URL(){
    
    	FacebookSession::setDefaultApplication($this->appid,$this->secret);
    	if(isset($_SESSION['access_token']) && $_SESSION['access_token']){
    		$session = new FacebookSession($_SESSION['access_token']);
    		$this->helper = new FacebookRedirectLoginHelper($this->redirURI);
    		$this->helper->disableSessionStatusCheck();
    		return $logoutUrl = $this->helper->getLogoutUrl($session, SITE_URL.'myaccount/signout');
    	} else {
    		return SITE_URL.'myaccount/signout';
    	}
    	 
    }
    
    
    function FB_get_url()
    {
        
        return $this->helper->getLoginUrl($this->scope);
        $this->helper->disableSessionStatusCheck();
        
    }
    
    
    function FB_create_accessToken()
    {
        
        $session = FacebookSession::newAppSession($this->appid, $this->secret);
         $session->getToken();
    }
    
    
    function FB_is_valid_token($token)
    {
        
        
        
        
        $accessToken = new AccessToken($token);
     
        try {
            $isvalid         = $accessToken->isValid($this->appid,$this->secret);
            $accessTokenInfo = $accessToken->getInfo();
        }
        catch (FacebookSDKException $e) {
            return false;
        }
        
        $access_data = $accessTokenInfo->asArray();
        return $access_data['is_valid'];
    }
    
    
    
    //for retriveing user date
    function FB_get_user($token)
    {
    	if($this->FB_is_valid_token($token) === true ){
        $session = new FacebookSession($token);
        
        $request     = new FacebookRequest($session, 'GET', '/me?fields=first_name,last_name,email,id,friends');
        $response    = $request->execute();
        $graphObject = $response->getGraphObject();
        
        $data = $graphObject->asArray();
        
        
        foreach ($data['friends'] as $friend) {
            
            
            $list['user_frnd_count'] = $data['friends']->summary->total_count;
            foreach ($friend as $fr) {
                if (is_object($fr)) {
                    if ($fr->name) {
                        $list['name'][] = $fr->name;
                    }
                    if ($fr->id) {
                        $list['id'][] = $fr->id;
                    }
                }
            }
            
        }
        
        $main['uid']               = $data['id'];
        $main['first_name']        = $data['first_name'];
        $main['last_name']         = $data['last_name'];
        $main['email']             = $data['email'];
        $main['friend_data'] = $list;
        
        return $main;
        
    	} else { return false; }
    }
    
    
   //for posting data to facebook 
   public  function FB_post_fb($token,$link,$message){
   	
   if($this->FB_is_valid_token($token) === true ){
  	    $session = new FacebookSession($token);
  	       if($session) {
   	             try {
   	                  $response = (new FacebookRequest(
   					   $session, 'POST', '/me/feed', array(
   							'link' => $link,
   							'message' => $message,
   					    )
   			            ))->execute()->getGraphObject();
   	               //echo "Posted with id: " . $response->getProperty('id');
   	                } catch(FacebookRequestException $e) {
   	
   			      echo "Exception occured, code: " . $e->getCode();
   			      echo " with message: " . $e->getMessage();
   	           }
             } 
           }
           
           if(isset($response)){
           	
           if(is_object($response)){	
         return $response->getProperty('id');

           	} else { return false; }
           	
           } else return false; 
    	
   }
    
    
    
    
    
}
