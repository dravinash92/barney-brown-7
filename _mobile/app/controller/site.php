<?php

class Site extends Base
{
	public $model;
	/**
	 *
	 * Load model
	 *
	 */
	public function __construct()
	{
		$this->model = $this->load_model('cmsModel');
		$this->sitemodel = $this->load_model('siteModel');
	}
	/**
	 *
	 * Load Mobile home page
	 *
	 */
	function index()
	{

		$this->title = TITLE;
		$this->smarty->assign('heading', TITLE);

		$this->smarty->assign('SITE_URL', SITE_URL);

	}
	/**
	 *
	 * Load location
	 *
	 */
	function location()
	{

		$this->title = TITLE;

		$this->smarty->assign('SITE_URL', SITE_URL);

		$this->smarty->assign('heading', 'LOCATION');

		$this->smarty->assign('content', "cms-location.tpl");

	}
	/**
	 *
	 * Load about us page
	 *
	 */
	function aboutus()
	{
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="ABOUT US";
		$model = $this->model;
		$webpageAboutUsData =  $model->get_webpagesdata_data(API_URL,$data);			
		$this->smarty->assign('webpagedata',$webpageAboutUsData);	
		$this->smarty->assign('content',"cms-about-us.tpl");

	}
	/**
	 *
	 * Load contact us page
	 *
	 */
	function contactus()
	{

		$this->title = TITLE;

		$this->smarty->assign('SITE_URL', SITE_URL);

		$this->smarty->assign('heading', 'CONTACT');

		$data['webpage_name'] = "CONTACT";

		$model = $this->model;

		$webpageCateringData = $model->get_webpagesdata_data(API_URL, $data);

		$this->smarty->assign('webpagedata', $webpageCateringData);

		$this->smarty->assign('content', "cms-contact.tpl");

	}
	/**
	 *
	 * Load term page
	 *
	 */
	function termsofuse()
	{

		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="TERMS OF USE";
		$model = $this->model;
		$webpageCateringData =  $model->get_webpagesdata_data(API_URL,$data);
		$this->smarty->assign('webpagedata',$webpageCateringData);
        $this->smarty->assign('content',"cms-terms-of-use.tpl");

	}
	/**
	 *
	 * Load privacypolicy page
	 *
	 */
	function privacypolicy()
	{

		 $this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="PRIVACY POLICY";
		$model = $this->model;
		$webpageCateringData =  $model->get_webpagesdata_data(API_URL,$data);
		$this->smarty->assign('webpagedata',$webpageCateringData);
        $this->smarty->assign('content',"cms-privacy-policy.tpl");

	}
	
	function testmode()
	{
		echo "<!DOCTYPE html>";
		echo "<html>";
		echo "<body style='margin: 0;padding: 0;background-color:#000;width:100%;height:100%;' >";
		echo "</body>";
		echo "</html>";
		exit;
	}
    
    
    /**
	 *
	 * Load Delivery page
	 *
	 */
	function deliverymap()
	{
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL',SITE_URL);
		$data['webpage_name'] ="Delivery Area";
		$model = $this->model;							
		$this->smarty->assign('content',"cms-delivery-map.tpl");
        

	}
	function covid19(){
			
		$this->smarty->assign('content',"cms-jobs.tpl");
	}
	
	

}
