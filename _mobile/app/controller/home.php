<?php

class Home extends Base
{
	public $model;
	/**
	 *
	 * Load model
	 *
	 */
	public function __construct()
	{

		$this->model = $this->load_model('homeModel');

	}
	/**
	 *
	 * Load home page
	 *
	 */
	function index()
	{
		//ini_set("display_errors", 1);
		$this->title = TITLE;
		$this->smarty->assign('SITE_URL', SITE_URL);
		$this->smarty->assign('ADMIN_URL', ADMIN_URL);
		$this->smarty->assign('FB_APP_ID', '');
		$this->smarty->assign('DATA_ID', '');

		$data        = "";
		$model       = $this->model;
		$webpagedata = $model->get_webpages_homepage_data(API_URL, $data);
		
		if(isset($webpagedata[0]) && $webpagedata[0]){
			$webpagedata = $webpagedata[0];
		} else {
			$webpagedata = "";
		}
		
		//For logged in user
		$uid = "";
		$uid = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];

		$order_detail = $this->pastOrders();
		//echo "<pre>";print_r($order_detail);exit;
		if (isset($order_detail->order_id)) {
			$dataOut = $this->model->prepOrderDetail($order_detail, $uid);
		
			 
			
			$this->smarty->assign('pastOrder', $dataOut);
			$this->smarty->assign('order_id', $order_detail->order_id);
			$this->smarty->assign('itemsAvailable',$order_detail->itemCheck->items);
		}

		$this->smarty->assign('uid', $uid);
	    if(isset($webpagedata->image) && $webpagedata->image){
	    	$this->smarty->assign('banner_image', $webpagedata->image);
	    }
		$this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
		$this->smarty->assign('content', "home.tpl");
	}
	/**
	 *
	 * List the past orders
	 *
	 */
	
	function get_last_order_detail(){
		$data = $this->pastOrders();
		$arrayElm = array('store_id'=>$data->store_id,'address_id'=>$data->address_id,'is_delivery'=>$data->delivery,'billing_id'=>$data->billinginfo_id);
		echo json_encode($arrayElm);
		exit;
	}
	
	function pastOrders()
	{
		$uid = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
		$out = $this->model->pastOrders($uid);
		if ($out)
			return $out->Data;
	}
	
	function check_session(){
		echo "<pre>";
		print_r($_SESSION);
		exit;
	}
	
	// For testing purpose only. (Displays date and time on the server).
	function showServerTime(){
		
		echo "date: "; echo date('M-d-Y');
		echo "<br>";  
		echo  "time: "; echo date('h:i:s A');
		exit;
		
	}

	function test(){
		unset($_SESSION['uid']);
	}


}
