<?php

class Menu extends Base
{
	public $model;
	/**
	 *
	 * Load model
	 *
	 */
	
	public $limit = 10;
	public function __construct(){
		$this->model = $this->load_model('menuModel');
	}
	/**
	 *
	 * Load menu items
	 *
	 */
	function index(){

		$uid  = @$_SESSION['uid']?@$_SESSION['uid']:@$_COOKIE['user_id'];
		
		if($uid){
			$totalMenuandwich  = $this->model->getTotalSanwichMenuCount($uid);
			if(isset($totalMenuandwich->Data) && $totalMenuandwich->Data) {
				$menuCount =	 $totalMenuandwich->Data->count;
			}
			else {
				$menuCount  = 0;
			}
		}
		else {
			$menuCount  = 0;
		}
		$this->smarty->assign('numMenu',$menuCount);
		$this->title = TITLE;
		$this->smarty->assign('heading','MY MENU');
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('ADMIN_URL',ADMIN_URL);
		$this->smarty->assign('CMS_URL',CMS_URL);
        $this->smarty->assign('SANDWICH',true);
        $model = $this->model;
        
        $stdCategoryItems = $model->getStandardCategories();
       
        foreach($stdCategoryItems as $row)
        	$row->categoryProducts = $model->getStandardCategoryProducts($row->id);
        
        
        $this->smarty->assign('stdCategoryItems',$stdCategoryItems);
        
        $menuData = array('uid'=>$uid, 'start' => 0, 'limit' => $this->limit );
        
        $data = $model->get_user_sanwich_data($menuData);
        $data = json_decode($data);
        
         if(count($data->Data)>0 && $uid!=0){
        	$gallery_data = $model->process_user_data($data->Data);
        } else {
        	$gallery_data = array();
        }
        
        $order_data  = $this->model->get_order_data(); 
        $this->smarty->assign('order_data',$order_data);
        $user_sandwich = 'user_sandwich';
        $this->smarty->assign('user_sandwich',$user_sandwich);
        $product='product';
        $this->smarty->assign('product',$product);
        $this->smarty->assign('uid',$uid); 
        $this->smarty->assign('usersandwich',$gallery_data);
        $this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
		$this->smarty->assign('content',"menu.tpl");
	}	
	/**
	 *
	 * Lazy load user menu sandwiches
	 *
	 */
	 
	public function lazyLoadMoreUserSandwich(){
		
		$model           = $this->model;		
		$count           = $_POST['count'];
		$page            = round($count / $this->limit);
		$data['limit']   = $this->limit;
		
		$uid  = @$_SESSION['uid']?@$_SESSION['uid']:@$_COOKIE['user_id'];

		if ($page == 0)
			$start = $count;
		else {
			$start = $this->limit * $page;
		}
        
        $menuData = array('uid'=>$uid, 'start' => $start, 'limit' => $this->limit );
        
        $data = $model->get_user_sanwich_data($menuData);
        $data = json_decode($data);
        
         if(count($data->Data)>0 && $uid!=0){
        	$gallery_data = $model->process_user_data($data->Data);
        } else {
        	$gallery_data = array();
        }

		$this->smarty->assign('uid',$uid); 
        $this->smarty->assign('usersandwich',$gallery_data);
        $this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
		$this->smarty->assign('SITE_URL', SITE_URL);
		$this->smarty->assign('ADMIN_URL', ADMIN_URL);
		echo $this->smarty->fetch("load_more_user_menu_sandwiches.tpl");
		exit;	
	
	}
	 
	/**
	 *
	 * Load friend menu items
	 *
	 */
	function friends_menu(){
	
		$uid = $this->UrlArray[2];
		$model = $this->model;
		
		$fname = $model->get_user_firstName($uid);
		if($fname) $fname = strtoupper($fname."'s");
		 
		
	    $this->title = TITLE;
		$this->smarty->assign('heading',$fname.' MENU');
		
		$this->smarty->assign('SITE_URL',SITE_URL);
		$this->smarty->assign('ADMIN_URL',ADMIN_URL);
		$this->smarty->assign('CMS_URL',CMS_URL);
        
        $this->smarty->assign('SANDWICH',true);
	
		$stdCategoryItems = $model->getStandardCategories();
	
	
		if($uid)
		{
			$data = $model->get_user_sanwich_data(array('uid'=>$uid));
			$data = json_decode($data);
		}
		else {
			$data=array();
		}
			
	
		if(isset($data->Data) && count($data->Data)>0){
			$gallery_data = $model->process_user_data($data->Data);
		} else {
			$gallery_data = array();
		}
	
		$order_data  = $this->model->get_order_data();
		$this->smarty->assign('order_data',$order_data);
		$user_sandwich = 'user_sandwich';
		$this->smarty->assign('user_sandwich',$user_sandwich);
		$product='product';
		$this->smarty->assign('product',$product);
		$this->smarty->assign('uid',$uid);
		$this->smarty->assign('usersandwich',$gallery_data);
		$this->smarty->assign('image_path',SANDWICH_IMAGE_PATH);
		$this->smarty->assign('content',"friends_menu.tpl");
	
	}

	function friendsMenu()
    {
        $f_uid         = $this->UrlArray[2];
        $uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
        $sandwichModel = $this->load_model('sandwichModel');
        $this->title = TITLE;
        $this->smarty->assign('SITE_URL', SITE_URL);
        $this->smarty->assign('ADMIN_URL', ADMIN_URL);
        $this->smarty->assign('SANDWICH', true);
        $model = $this->model;
        $stdCategoryItems = $model->getStandardCategories();

        $sandwichCount = $model->getfriendSandwichCount($f_uid);
        $this->smarty->assign('sandwichCount', $sandwichCount);
        $userdetails = $model->get_this_user($f_uid);
        $this->smarty->assign('fb_id', $userdetails->fb_id);
        
        $fname = ($userdetails->first_name != '')?$userdetails->first_name:'';
        $lname = ($userdetails->last_name != '')?$userdetails->last_name:'';
        $fname = $fname." ".$lname;
        $this->smarty->assign('fname', $fname);

        if ($uid) {
            $data = $model->get_user_sanwich_data(array('uid'=>$f_uid)); 
            $data = json_decode($data);
            $getSavedSandwiches = $sandwichModel->get_user_sandwich_ids($uid);
        } else {
            $data = array();
            $getSavedSandwiches = array();
        }
        
        $menu_added_from_array = array_column($getSavedSandwiches, 'menu_added_from');
        $id_array = array_column($getSavedSandwiches, 'id' );

        $is_public_array = array();
        foreach ($getSavedSandwiches as $key => $value) 
        {
            if($value['menu_added_from'] != 0)
                $is_public_array[$value['menu_added_from']] = $value['is_public'];
            else
                $is_public_array[$value['id']] = $value['is_public'];
        }

        $getSavedSandwiches = array_merge($menu_added_from_array, $id_array);


        if (isset($data->Data) && count($data->Data) > 0) {
            $featured_data = $model->process_user_data($data->Data);
        } else {
            $featured_data = array();
        }

        $order_data = $this->model->get_order_data();
        $this->smarty->assign('order_data', $order_data);
        $user_sandwich = 'user_sandwich';
        $this->smarty->assign('user_sandwich', $user_sandwich);
        $product = 'product';
        $this->smarty->assign('product', $product);
        //$this->smarty->assign('uid', $uid);
        $this->smarty->assign('featured_data',$featured_data);
        $this->smarty->assign('saved_data',$getSavedSandwiches);
        $this->smarty->assign('is_public_array',$is_public_array);
        $this->smarty->assign('image_path', SANDWICH_IMAGE_PATH);
        $this->smarty->assign('content', "friendsMenu.tpl");
    }
	/**
	 *
	 * Load facebook friend menu items
	 *
	 */
	function getFBfriendsMenu(){
		if(isset($_COOKIE['facebookOnlyUser']))
		{
			$data = $this->model->getFBfriendsMenu();
			if(isset($data->Data)){
				$data = $this->model->_process($data->Data);
				$this->smarty->assign('data',$data);
			}
			$this->smarty->assign('SITE_URL',SITE_URL);
			$res = $this->smarty->fetch("friends_menu_list.tpl");
		}
		else {
			$res= '<p class="nof">Sorry, you need to be logged in with <b/> Facebook to use this feature!</p>';
		}
		echo $res;
		exit;
	}

	function salads()
    {
        $uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
        $this->title = TITLE;       
        $this->smarty->assign('SITE_URL',SITE_URL);
        $this->smarty->assign('heading','SALADS');
        $product = 'product';
        $this->smarty->assign('product', $product);
        $this->smarty->assign('uid', $uid);
        $model = $this->model;
        $stdCategoryItems = $model->getStandardCategories();
        $saladsResult = array();

        foreach ($stdCategoryItems as $row)
        {
            if ($row->category_identifier == "SALADS") 
            {
                $saladsResult = $model->getStandardCategoryProducts($row->id);
            }
            
        }
        
        $salad_data = json_decode(json_encode($saladsResult), true);
        $sal = array();
        $modifier_options = array();
        foreach ($salad_data as $value)
        {
            $sal[] = $value;
        }
        foreach($sal as $key=>$val){
            $modifier_options[]=json_decode($val['modifier_options'],true);
        }
        //echo '<pre>';print_r($modifier_options);exit;
        $this->smarty->assign('salad_image_path',SALAD_IMAGE_PATH);
        $this->smarty->assign('modifier_options',$modifier_options);
        $this->smarty->assign('salads',$sal);
        $this->smarty->assign('content',"salads.tpl");
    }
    function snacks()
    {
        $uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
        $this->title = TITLE;       
        $this->smarty->assign('SITE_URL',SITE_URL);
        $this->smarty->assign('heading','SNACKS & THINGS');
        $product = 'product';
        $this->smarty->assign('product', $product);
        $this->smarty->assign('uid', $uid);
        $model = $this->model;
        $stdCategoryItems = $model->getStandardCategories();
        $saladsResult = array();

        foreach ($stdCategoryItems as $row)
        {
            if ($row->category_identifier == "SNACKS & THINGS") 
            {
                $saladsResult = $model->getStandardCategoryProducts($row->id);
            }
            
        }
        
        $salad_data = json_decode(json_encode($saladsResult), true);
        $sal = array();
        $modifier_options = array();
        foreach ($salad_data as $value)
        {
            $sal[] = $value;
        }
        foreach($sal as $key=>$val){
            $modifier_options[]=json_decode($val['modifier_options'],true);
        }
        $this->smarty->assign('salad_image_path',SALAD_IMAGE_PATH);
        $this->smarty->assign('modifier_options',$modifier_options);
        $this->smarty->assign('salads',$sal);
        $this->smarty->assign('content',"snacks.tpl");
    }
    function drinks()
    {
        $uid         = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
        $this->title = TITLE;       
        $this->smarty->assign('SITE_URL',SITE_URL);
        $this->smarty->assign('heading','DRINKS');
        $product = 'product';
        $this->smarty->assign('product', $product);
        $this->smarty->assign('uid', $uid);
        $model = $this->model;
        $stdCategoryItems = $model->getStandardCategories();
        $saladsResult = array();

        foreach ($stdCategoryItems as $row)
        {
            if ($row->category_identifier == "DRINKS") 
            {
                $saladsResult = $model->getStandardCategoryProducts($row->id);
            }
            
        }
        
        $salad_data = json_decode(json_encode($saladsResult), true);
        $sal = array();
        $modifier_options = array();
        foreach ($salad_data as $value)
        {
            $sal[] = $value;
        }
        foreach($sal as $key=>$val){
            $modifier_options[]=json_decode($val['modifier_options'],true);
        }
        $this->smarty->assign('salad_image_path',SALAD_IMAGE_PATH);
        $this->smarty->assign('modifier_options',$modifier_options);
        $this->smarty->assign('salads',$sal);
        $this->smarty->assign('content',"drinks.tpl");
    }

    function friendSandwiches()
    {
        if(isset($_SESSION['uid']))
        {
            $data = $this->model->getFBfriendsMenu();
            $userFBlogin = $this->model->checkFBLogin();
            $fbCheck = ($userFBlogin->Data->fb_id == "")?0:1;
        }
        else
        {
            $data = array();
        }
        
        if (isset($data->Data)) 
        {
            $data = $this->model->_process($data->Data);
            $this->smarty->assign('SITE_URL',SITE_URL);
            $this->smarty->assign('data', $data);
            $this->smarty->assign('fbCheck', $fbCheck);
            $this->smarty->assign('heading',"FRIEND'S SANDWICHES");
        }
       $this->smarty->assign('content', "friends_sandwiches.tpl");
    }
	

}
