<?php

class myaccount extends Base
{

	public $model;
	
	/**
	 *
	 * Load model
	 *
	 */
	
	public function __construct()
	{

		$this->model = $this->load_model("myAccountModel");
	}
	
	/**
	 *
	 * Load myaccount page
	 *
	 */
	
	function index()
	{


		if (isset($_GET['view'])) {
			$this->smarty->assign('orderview', 1);
		} else {
			$this->smarty->assign('orderview', 0);
		}
		if (isset($_SESSION['uid']))
			$uid = $_SESSION['uid'];
		else {
			echo "<script>window.location='" . SITE_URL . "';</script>";
			exit;
		}

		$model       = $this->model;
		$this->title = TITLE;

		$this->smarty->assign('SITE_URL', SITE_URL);

		$this->smarty->assign('LOGGED_IN', true);
		$user = $model->getUserDetails($_SESSION['uid']);
		$this->smarty->assign('user', $user);
		$review_content = $model->get_all_review_contents();
		$this->smarty->assign('review_content', $review_content);
		$data['uid'] = $_SESSION['uid'];
		$user_review = $model->get_user_review($data);
		$this->smarty->assign('user_review', $user_review);
		$count = array(
				1,
				2,
				3,
				4,
				5,
				6,
				7,
				8,
				9,
				10
		);
		$this->smarty->assign('count', $count);
		$data['user_id'] = $_SESSION['uid'];

		$orderhistory = $model->orderhistory($data);

		foreach ($orderhistory as $key => $order) {
			//Converting Each order Date to Timestamp and assigning to Order object.
			$orderDate            = '';
			$orderDate            = explode("-", $order->date);
			$order->timestampDate = $this->makeTimeStamp($orderDate[2], $orderDate[0], $orderDate[1]);

			//Getting OrderDetails
			$orderDetails           = '';
			$orderDetails           = $model->myOrderDetails($order->order_number);
			$order->orderDetails    = @$orderDetails->Data->order_item_detail;
			$order->billing_type    = @$orderDetails->Data->billing_type;
			$order->billing_card_no = @$orderDetails->Data->billing_card_no;

			$orderhistory[$key] = $order;
		}
		
		
	 

		$this->smarty->assign('orderhistory', $orderhistory);
		$this->smarty->assign('content', "myaccount-orders.tpl");

	}
	/**
	 *
	 * Load orders
	 *
	 */
	function orders()
	{

		$this->title = TITLE;

		$this->smarty->assign('heading', 'ORDER HISTORY');

		$this->smarty->assign('content', "myaccount-orders.tpl");
	}
	/**
	 *
	 * Load order history
	 *
	 */
	function orderhistory()
	{

		$model           = $this->model;
		$data['user_id'] = $_SESSION['uid'];

		$orderhistory = $model->orderhistory($data);
		$this->smarty->assign('orderhistory', $orderhistory);

		$this->title = TITLE;

		$this->smarty->assign('SITE_URL', SITE_URL);

		$this->smarty->assign('content', "myaccount-orders.tpl");


	}
	/**
	 *
	 * Check login
	 *
	 */
	function loginCheck()
	{
		$model = $this->model;
		$data  = $model->loginCheck($_POST);

		if (count($data) == 1) {
			$_SESSION['uid']        = $data[0]->uid;
			$_SESSION['start_time'] = strtotime("now");
			$_SESSION['uname']      = $data[0]->first_name;
			$_SESSION['lname']		= $data[0]->last_name;
			$result["state"]        = "true";
			$values                 = array();
			$mail                   = $data[0]->username;
			$fname                  = $data[0]->first_name;
			$lname                  = $data[0]->last_name;
			$uid                    = $data[0]->uid;
			$cookietime = time()+(86400 * 30 * 3); //90 days	
			setcookie("user_mail", $mail, $cookietime, '/', NULL);
			setcookie("user_fname", $fname, $cookietime, '/', NULL);
			setcookie("user_lname", $lname, $cookietime, '/', NULL);
			setcookie("user_id", $uid, $cookietime, '/', NULL);

		}

		else {

			$result["state"] = "false";
			$result["msg"]   = "Invalid Username or Password";
		}
		echo json_encode($result);

		exit;
	}
	/**
	 *
	 * Create new account
	 *
	 */
	function createAccount()
	{
		$model  = $this->model;
		$result = $model->createAccount($_POST);
		if ($result->state) {
			$_SESSION['uid']   = $result->msg->uid;
			$_SESSION['uname'] = $result->msg->first_name;
                        $_SESSION['lname']=$result->msg->last_name;
		}
		echo json_encode($result);
		exit;
	}
	
	/**
	 *
	 * Logout function
	 *
	 */
	function signout()
	{
	  session_destroy();
        if(count($_COOKIE) > 0){
			$past = time() - 3600;
			foreach ( $_COOKIE as $key => $value )
			{
			setcookie($key, $value, $past, '/',NULL);
			}
        }
        $random_num = rand();

	  header("Location:" . SITE_URL ."home/index/?pid=". $random_num);
           
        exit;
	}
	/**
	 *
	 * Generate forgotPassword
	 *
	 */
	public function forgotPassword()
	{
		$model = $this->model;

		$result = $model->forgotPassword($_POST);
		echo json_encode($result);
		exit;
	}
	/**
	 *
	 * Check session
	 *
	 */
	function is_session()
	{
		$data = new stdClass ();
		if (! isset ( $_SESSION ['uid'] ) && ! isset ( $_COOKIE ['user_id'] )){
			$state = false;
			// for fuxing unexpected cat item delete. Since we dont have any templ login here.
			 $this->model->deleteAll($_POST);
		} else
		{
			session_start();
			if (! isset ( $_SESSION ['uid'])  && isset($_COOKIE ['user_id']))
			{
				$_SESSION ['uid'] = $_COOKIE ['user_id'];
				$_SESSION['uname'] = $_COOKIE ['user_fname'];
				$_SESSION['lname'] = $_COOKIE ['user_lname'];
			}
			$state = true;
			$out = $this->model->checkUserAvailability ();
			$out = json_decode ( $out );
		
			if (isset ( $out ) && isset ( $out->Data )) {
				$data->userAvail = $out->Data [0]->count;
			}
		}
		
		

		if (isset ( $_COOKIE ['facebookOnlyUser'] ) && $_COOKIE ['facebookOnlyUser'])
			$fbonly = true;
		else
			$fbonly = false;
		
		$data->session_state = $state;
		$data->facebook_only_user = $fbonly;
		echo json_encode ( $data );
		exit ();

	}
	/**
	 *
	 * Add address
	 *
	 */
	function savingAddress()
	{
		$model        = $this->model;
		$_POST['uid'] = $_SESSION['uid'];
		$result       = $model->savingUserAddress($_POST);
		echo $result;
		exit;
	}
	/**
	 *
	 * Fetch store from zipcode
	 *
	 */
	public function getStoreFomZip()
	{
		$model = $this->model;
		echo json_encode($model->getStoreFomZip());
		exit;
	}
	/**
	 *
	 * Date to timestamp
	 *
	 */
	function makeTimeStamp($year = '', $month = '', $day = '')
	{
		if (empty($year)) {
			$year = strftime('%Y');
		}
		if (empty($month)) {
			$month = strftime('%m');
		}
		if (empty($day)) {
			$day = strftime('%d');
		}

		return mktime(0, 0, 0, $month, $day, $year);
	}
	/**
	 *
	 * Add reorder
	 *
	 */
	public function myOrderReorder()
	{
		$model    = $this->model;
		$response = $model->myOrderReorder($_POST);
		echo $response;
		exit;
	}
	/**
	 *
	 * Get user access token
	 *
	 */
	public function getUserAccessToken()
	{
		$out  = array();
		$data = $this->model->getUserAccessToken();
		$data = json_decode($data);
		if (isset($data->Data)) {
			if (isset($data->Data[0]->fb_auth_token)) {
				$toc = $data->Data[0]->fb_auth_token;
			} else
				$toc = null;
		} else
			$toc = null;

		if ($toc) { 
				$user_data      = $this->FB_get_user($toc);
				$out['fbstate'] = $this->input_FB_users($user_data);
			$out['state'] = true;

		} else {
			$out['state']   = false;
			$out['fbstate'] = false;
		}
		echo json_encode($out);
		exit;
	}
	/**
	 *
	 * Add facebook user data
	 *
	 */
	function input_FB_users($user_data)
	{
		$result      = true;
		$uid         = $user_data['uid'];
		$fname       = $user_data['first_name'];
		$lname       = $user_data['last_name'];
		$email       = $user_data['email'];
		$token       = @$_SESSION['access_token'];
		$friend_data = $user_data['friend_data'];
		if (isset($friend_data['name']))
			$count_friend = count($friend_data['name']);
		else
			$count_friend = 0;


		$num   = $this->db->Query("SELECT username,first_name,uid,fb_auth_token FROM users WHERE username = '$email' OR  fb_id = '$uid'");
		$count = $this->db->Rows();
		$data  = $this->db->FetchArray($num);


		if ($count == 0 && !@$_SESSION['uid']) {
			$result_usr        = $this->db->Query(" INSERT INTO users (username,fb_id,fb_auth_token,first_name,last_name) VALUES ('$email','$uid','$token','$fname','$lname') ");
			$insid             = $this->db->insertId();
			$_SESSION['uid']   = $insid;
			$_SESSION['uname'] = $fname;
                        $_SESSION['lname']=$lname;
			$uidz              = $insid;
		} else if ($count && !@$_SESSION['uid']) {
			if ((isset($_COOKIE['user_id']) && $_COOKIE['user_id']) && $_COOKIE['user_id'] != $data['uid']) {
				$result = false;
			} else if (!@$_COOKIE['user_id'] && !@$_SESSION['uid']) {
				$_SESSION['uid']   = $data['uid'];
				$_SESSION['uname'] = $data['first_name'];
                                $_SESSION['lname']=$data['last_name'];
				$cookietime        = time() + 60 * 60 * 24 * 60;
				setcookie("user_mail", $data['username'], $cookietime, '/', NULL);
				setcookie("user_fname", $data['first_name'], $cookietime, '/', NULL);
				setcookie("user_lname", $data['last_name'], $cookietime, '/', NULL);
				setcookie("user_id", $data['uid'], $cookietime, '/', NULL);
			}
			$uidz = $data['uid'];
			if ($data['fb_auth_token'] != $token) {
				$this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' ");
			}
		} else if (!$count && $_SESSION['uid']) {
			$uidz = $_SESSION['uid'];
			if ($data['fb_auth_token'] != $token) {
				$this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' ");
			}
		} else if ($count && $_SESSION['uid']) {
			$uidz = $data['uid'];
			if ($_SESSION['uid'] != $data['uid']) {
				$result = false;
			}
			if ($data['fb_auth_token'] != $token) {
				$this->db->Query("UPDATE users SET  fb_auth_token = '$token' , fb_id = '$uid' WHERE uid = '$uidz' ");
			}
		}


		for ($i = 0; $i < $count_friend; $i++) {
			$frnd_data = $this->db->Query("SELECT friend_uid FROM friend_data WHERE friend_uid = '" . $friend_data['id'][$i] . "' AND parent_uid = '$uidz' ");
			$count     = $this->db->Rows();
			if ($count == 0) {

				$this->db->Query("INSERT INTO friend_data (friend_name,friend_uid,parent_uid) VALUES ('" . $friend_data['name'][$i] . "','" . $friend_data['id'][$i] . "','$uidz') ");
			}
		}
		return $result;
	}
	
	public function getAllzipcodes(){
		$model = $this->model;
		$result = $model->getAllzipcodes($_POST);
		$result = json_decode($result);
		$arr = array();
		if(count($result->Data>0)){
			foreach($result->Data as $data){
				$arr[] = $data->zipcode;
			}
			$arr = array_unique($arr);
		}
		
		echo json_encode($arr);
		
		exit;
	}

}
