<?php

class Checkout extends Base
{
	public $model;
	/**
	 *
	 * Load model
	 *
	 */
	public function __construct()
	{
		$this->model = $this->load_model('checkoutModel');
	}
	/**
	 *
	 * Load checkout page
	 *
	 */
	function index()
	{
		
		 

		if (!@$_SESSION['uid']) {
			header('Location: ' . SITE_URL);
			exit;
		}

		$this->smarty->assign('heading', 'CHECKOUT');

		if (isset($_SESSION['uid'])) {
			$this->title = TITLE;

			$this->smarty->assign('SITE_URL', SITE_URL);

			$this->smarty->assign('LOGGED_IN', true);

			$model           = $this->model;
			$data            = $model->get_user_checkout_data(@$_SESSION['uid']);
			$billing_data    = $model->get_Billinginfo(array(
					'uid' => @$_SESSION['uid']
			));
			$billing_data    = $billing_data->Data;
                        if(count($billing_data) > 0)
                        {
                            foreach ($billing_data as $key => $value) {
                              $billing_data[$key]->card_number = "XXXX-XXXX-XXXX-".substr($value->card_number, -4);
                            }

                        }
			$user["user_id"] = $_SESSION['uid'];
			$shopitems = $model->cartItems($user);

			$shopitems = $this->getCartItemList($shopitems);
			$this->smarty->assign('cartItemList', $shopitems);
			$this->smarty->assign('billingInfo', $billing_data);

			// added for date
			$currenthour = date('H');
			$currentmin  = date('i');
			if ($currentmin < 15)
				$currentmin = 0;
			else if ($currentmin >= 15 && $currentmin < 30)
				$currentmin = 15;
			else if ($currentmin >= 30 && $currentmin < 45)
				$currentmin = 30;
			else if ($currentmin >= 45 && $currentmin < 60)
				$currentmin = 45;

			$currentdate = $currenthour . ":" . $currentmin;
			
			$from        = date('H:i:s', strtotime($currentdate));
			$todate      = date('H:i:s', strtotime('10pm'));


			sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
			$lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
			$lower = $lower + 900;

			sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
			$upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
			$upper = $upper - 900;

			$times = "<select class='timedropdown' >";
			if (empty($format)) {
				$format = 'g:i a';
			}
			$step = 900;
			foreach (range($lower, $upper, $step) as $increment) {
				$increment = gmdate('H:i', $increment);

				list($hour, $minutes) = explode(':', $increment);

				$date = new DateTime($hour . ':' . $minutes);
				$date = $date->format($format);

				$times .= "<option value=" . $hour . ':' . $minutes . ">" . $date . "</option>";
			}

			$times .= "</select>";

			$this->smarty->assign('times', $times);
			$this->smarty->assign('currenthour', $currenthour);

			$addresses = $model->getPickupStores(array("enabled"=>1));
			$this->smarty->assign('addresses', $addresses);

			$this->smarty->assign('content', "checkout.tpl");

		}

	}


	 function getSpecificDayForStore() {
        $model = $this->model;
        if( empty( $_POST  ) ){
            echo json_encode(array(  ));
            exit(  );
        }
        
        $store_info = $model->getspecificday($_POST['storeId']);

        $result = array(  );
        $result['specific_message'] = $store_info->specificday_message;
        $result['specific_day'] = $store_info->specificday;

        echo json_encode( $result );
        exit(  );
     }
     
	/**
	 *
	 * View cart page
	 *
	 */
	function cart()
	{

		$this->title = TITLE;

		$this->smarty->assign('SITE_URL', SITE_URL);

		$this->smarty->assign('content', "cart.tpl");

	}
	/**
	 *
	 * View cart list item
	 *
	 */
	public function getCartItemList($result, $deliveryOrPickup=1, $last_tip = 0, $whichArrow="")
	{
		
		$model = $this->model;

		if (count($result->sandwiches) > 0) {
			$sandwiches = $model->processSandwiches($result->sandwiches);

		} else {
			$sandwiches = array();
		}
		//echo "<pre>";print_r($sandwiches);exit;

		$total = 0;
		$o     = '';

		foreach ($sandwiches as $key => $sum) {
			$sum['item_total'] = number_format((float) $sum['item_price'] * $sum['item_qty'], 2, '.', '');
			$total             = $total + ($sum['item_price'] * $sum['item_qty']);
			$o .= $sum['order_item_id'] . ' ';
			$sandwiches[$key] = $sum;
		}

		$products = $result->products;
		foreach ($products as $key => $product) {
			$total += $product->product_price * $product->item_qty;

			$extra_price = $product->item_qty * $product->extra_cost;
			$total += $extra_price;

			$product->item_price = ($product->product_price * $product->item_qty) + ($product->item_qty * $product->extra_cost);

			$product->item_price = number_format((float) $product->item_price, 2, '.', '');

			$product->item_total = $product->item_price;
			$product->item_total = number_format((float) $product->item_total, 2, '.', '');

			$products[$key] = $product;

			$o .= $product->order_item_id . ' ';
		}

		$o         = trim($o);
		$ordString = str_replace(' ', ':', $o);
		
		$subtotalNumber = $totalNumber = $total;

		$subtotal = $total;
		$total    = 3 + $total;

		$subtotal = $subtotal;
		$total    = $total;
		
		//If deliveryOrPickup is pickup 0 then no tip
		if($deliveryOrPickup == 0){	
			$tips = 0;
		}else{	
			$tips     = ($subtotalNumber * 15) / 100;
			$tips     = round($tips);

			if ($tips < MIN_TIP)
				$tips = MIN_TIP;
				
			if ($tips > MAX_TIP)
				$tips = MAX_TIP;
		
		}
		
			if($last_tip > $tips && $whichArrow == "right")
				$tips = $last_tip;
		
				
		
		$this->smarty->assign('pickupOrDelivery', $deliveryOrPickup);
		
		if (count($sandwiches) + count($products) < 1) {
			$tips = 0;
		}
		
		$tax = $subtotal * TAX;
		$tax =number_format($tax, 2);

		$this->smarty->assign('tips', $tips);
		$total = $subtotalNumber + $tips + $tax;
		$totalNumber = $total;
		

		$this->smarty->assign('SITE_URL', SITE_URL);
		$this->smarty->assign('MAX_TIP', MAX_TIP);
		$this->smarty->assign('MIN_TIP', MIN_TIP);
		$this->smarty->assign('sandwiches', $sandwiches);
		$this->smarty->assign('products', $products);

		$this->smarty->assign('itemsCount', count($products) + count($sandwiches));
		$this->smarty->assign('order_string', $ordString);
		$this->smarty->assign('total', $subtotal);
		$this->smarty->assign('tax', $tax);
		 $this->smarty->assign('totalNumber', $subtotalNumber);
		$this->smarty->assign('grandtotal', $total);

		$output = $this->smarty->fetch("cart-list-items.tpl");

		return $output;
	}
	/**
	 *
	 * Update cart item
	 *
	 */
	function updateCartItems()
	{
		$this->title = TITLE;

		$response = new stdClass;
		if (!isset($_SESSION['uid'])) {
			$response->status  = false;
			$response->message = "Session is expired. Please login again.";
			echo json_encode($response);
			exit;
		} else {

			$model             = $this->model;
			$_POST['user_id']  = $_SESSION['uid'];
			$last_tip  = @$_POST['last_tip'];
			$whichArrow  = @$_POST['whichArrow'];
			$deliveryOrPickup = @$_POST['deliveryOrPickup'];
			$result            = $model->updateItemToCart($_POST);
			$output            = $this->getCartItemList($result, $deliveryOrPickup, $last_tip, $whichArrow);
			$response->status  = true;
			$response->message = $output;
			echo json_encode($response);
			exit;
		}
	}
	/**
	 *
	 * List cart item
	 *
	 */
	function getCartItemQtyCount()
	{
		$model            = $this->model;
		$_POST['user_id'] = $_SESSION['uid'];
		$result           = $model->getCartItemQtyCount($_POST);
		if ($result) {
			$result = $result->count;
		} else {
			$result = 0;
		}
		print_r($result);
		exit;
	}
	/**
	 *
	 * List delivery choose address
	 *
	 */
	function deliverychooseaddress()
	{
		$model = $this->model;

		$addresses = $model->listSavedAddress($_SESSION['uid']);

		$this->smarty->assign('addresses', $addresses);

		$this->title = TITLE;

		$this->smarty->assign('SITE_URL', SITE_URL);

		$output = $this->smarty->fetch("selected_address_list.tpl");

		print_r($output);
		exit;
	}
	/**
	 *
	 * List pickup store location
	 *
	 */
	function pickupstorelocation()
	{
		$this->title = TITLE;

		$model = $this->model;

		$addresses = $model->getPickupStores(array("enabled"=>1));
		$this->smarty->assign('addresses', $addresses);

		$this->smarty->assign('SITE_URL', SITE_URL);

		$output = $this->smarty->fetch("selected_pickupaddress_list.tpl");

		echo $output;
		exit;
	}
	/**
	 *
	 * Add billing information
	 *
	 */
	function saveBillinginfo()
	{
		$model          = $this->model;
		$checkout_items = $model->save_billing_info($_POST);
		exit;
	}
	/**
	 *
	 * Check cvv
	 *
	 */
	function chekcvvValid()
	{
		$model = $this->model;
		$chk   = $model->chekcvvValid($_POST);
		exit;
	}
	/**
	 *
	 * Change Time On Store Selection
	 *
	 */
	function changeTimeOnStoreSelection()
	{

		$storeID      = @$_POST['store_id'];
		$selectedDay  = @$_POST['selectedDay'];
		$Currenttime  = @$_POST['current_time'];
		$isCurrentDay = @$_POST['isCurrentDay'];
		$isDelivery   = @$_POST['isDelivery'];
		$model        = $this->model;

		if (!empty($_POST['current_day'])) {
			$datenow     = $_POST['current_day'];
			$selectedDay = date('l', strtotime($datenow));
		} else
			$datenow = date("Y-m-d");
			 $nowdate=  date("n/j/Y", strtotime($datenow));
			$specific = $model->getspecificday($storeID);
			$specificDate =  date("n/j/Y", strtotime($specific->specificday));
			
		$result = $model->getTimeOnStoreSelection($storeID, $selectedDay, $Currenttime, 0, $isCurrentDay,$isDelivery);
				
		if($_POST['isCurrentDay'] == 1) {
			$currenthour = date('H');
			$currentmin  = date('i');
			if ($currentmin < 15)
				$currentmin = 0;
			else if ($currentmin >= 15 && $currentmin < 30)
				$currentmin = 15;
			else if ($currentmin >= 30 && $currentmin < 45)
				$currentmin = 30;
			else if ($currentmin >= 45 && $currentmin < 60)
				$currentmin = 45;
			

			$currentdate = $currenthour . ":" . $currentmin;
			
		if( !isset($result['store_details']->open) || !isset($result['store_details']->close) ){
			
		}
		
			if(strtotime($currentdate) < strtotime($result['store_details']->close) && strtotime($currentdate) > strtotime($result['store_details']->open)){
					
				$from        = date('H:i:s', strtotime($currentdate));
			
			} else {
					
				$from = date('H:i:s', strtotime($result['store_details']->open ));
			}
			
		} else {
			
		if(isset($result['store_details']->open) && $result['store_details']->open)	{ 
			$from = date('H:i:s', strtotime($result['store_details']->open ));
		} else {
			exit("error");
		}
		}

		
		
		if(isset($result['store_details']->close) && $result['store_details']->close){
		
		$todate = date('H:i:s', strtotime($result['store_details']->close ));
		
		} else {
			exit("error");
		}
		 
		
		sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
		$lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		$lower = $lower + 900;

		sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
		$upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
		$upper = $upper - 900;

	
		$times = "<select>";
		if (empty($format)) {
			$format = 'g:i a';
		}
		$step = 900;
		foreach (range($lower, $upper, $step) as $increment) {
			$increment = gmdate('H:i', $increment);

			list($hour, $minutes) = explode(':', $increment);

			$date = new DateTime($hour . ':' . $minutes);
			$date = $date->format($format);

			if ( $hour == '12' && $minutes == '00')
				$s = 'selected="selected"';
			else
				$s = '';

			$times .= "<option " . $s . " value=" . $hour . ':' . $minutes . ">" . $date . "</option>";
		}
		$times .= "</select>";
		$dateHid = "<input type='hidden' name='updatedDate' value='" . date("n/j/Y", strtotime($datenow)) . "' />";
		if ($result['run_counts'] > 0) {

			if ($_POST['selectedDay'] == $result['day']) {

				$dateHid = "<input type='hidden' name='updatedDate' value='" . date("n/j/Y", strtotime($datenow)) . "' />";
			} else {
				$result_date = date("n/j/Y", strtotime($datenow . " +" . $result['run_counts'] . " day"));
                    
                  if ($result_date == $specificDate && $specific->specificday_active == 1) {  //echo "coming";
                        
                      
                       
                      $result_date = date("n/j/Y", strtotime($result_date . " + 1 day"));
                       
                      $this->changeTimeOnStoreSelectionOnSpecificDay($storeID, $result_date, $Currenttime, 0, $isDelivery);
                      exit;
                    }else{
                        
                    }
				$dateHid = "<input type='hidden' name='updatedDate' value='" . date("n/j/Y", strtotime($datenow . " +" . $result['run_counts'] . " day")) . "' />";
			}
		}
		$times .= $dateHid;
		$this->smarty->assign('times', $times);
		
		$output = $this->smarty->fetch("pickup-time-select.tpl");
		
		//	}
			echo $output;

		exit;
	}
	   function changeTimeOnStoreSelectionOnSpecificDay($storeID, $selectedDay, $Currenttime, $isCurrentDay, $isDelivery) {

        $storeID = $storeID;
        $selectedDay = $selectedDay;
        $Currenttime = $Currenttime;
        $isCurrentDay = $isCurrentDay;
        $isDelivery = $isDelivery;
        $model = $this->model;

      
        $selectedDay = date('l', strtotime($selectedDay));
            $datenow = $selectedDay;

      
        $nowdate = date("n/j/Y", strtotime($datenow));
     
            $result = $model->getTimeOnStoreSelection($storeID, $selectedDay, $Currenttime, 0, $isCurrentDay, $isDelivery);
                if (isset($result['store_details']->open) && $result['store_details']->open) {
                    $from = date('H:i:s', strtotime($result['store_details']->open));
                } else {
                    exit("error");
                }
         

            if (isset($result['store_details']->close) && $result['store_details']->close) {

                $todate = date('H:i:s', strtotime($result['store_details']->close));
            } else {
                exit("error");
            }



            sscanf($from, "%d:%d:%d", $hours, $minutes, $seconds);
            $lower = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $lower = $lower + 900;

            sscanf($todate, "%d:%d:%d", $hours, $minutes, $seconds);
            $upper = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $upper = $upper - 900;
            $times = "<select class='timedropdown'>";
            if (empty($format)) {
                $format = 'g:i a';
            }
            $step = 900;
            foreach (range($lower, $upper, $step) as $increment) {
                $increment = gmdate('H:i', $increment);

                list($hour, $minutes) = explode(':', $increment);

                $date = new DateTime($hour . ':' . $minutes);
                $date = $date->format($format);

                if ($hour == '12' && $minutes == '00')
                    $s = 'selected="selected"';
                else
                    $s = '';

                $times .= "<option " . $s . " value=" . $hour . ':' . $minutes . ">" . $date . "</option>";
            }
            $times .= "</select>";
            $dateHid = "<input type='hidden' name='updatedDate' value='" . date("n/j/Y", strtotime($datenow)) . "' />";
            if ($result['run_counts'] > 0) {
                
                if ($selectedDay == $result['day']) {

                    $result_date = date("n/j/Y", strtotime($datenow));
                } else {
                    $result_date = date("n/j/Y", strtotime($datenow . " +" . $result['run_counts'] . " day"));

                   
                    $dateHid = "<input type='hidden' name='updatedDate' value='" . $result_date . "' />";
              
                }
            }
            $times .= $dateHid;
			$this->smarty->assign('times', $times);
			
			$output = $this->smarty->fetch("pickup-time-select.tpl");
	        echo $output;

        exit;
    }
	public function getspecificMessage(){
		
		
		$storeID = $_POST['storeId'];
		$datenow = $_POST['date_text'];
		$nowdate=  date("n/j/Y", strtotime($datenow));
		$model        = $this->model;
		$specific = $model->getspecificday($storeID);
		
		$specificDate =  date("n/j/Y", strtotime($specific->specificday));
		if(strtotime($nowdate) == strtotime($specificDate) && $specific->specificday_active==1){
	
			$message =  $specific->specificday_message;
			$Smessage['message'] = $message;
			
		}
		else{
			$Smessage['message'] = '';
		}
		$output= json_encode($Smessage);
		echo $output;
		exit;
		
	}
	/**
	 *
	 * List CurrentHour
	 *
	 */
	public function getCurrentHour()
	{

		//$dt = new DateTime('now', new DateTimezone('America/New_York'));
		$dt = new DateTime('now', new DateTimezone('Asia/Kolkata'));
		$response                 = array();
		$response['hour']         = $dt->format("H:i");
		$response['day']          = $dt->format("w");
		$response['date_time']    = $dt->format("d-m-y H:i:s");
		$response['date_time_js'] = $dt->format("Y,m,d,H,i,s");
		$response['timezone']     = $dt->getTimezone();

		echo json_encode($response);
		exit;

	}
	/**
	 *
	 * List Current Store TimeSlot
	 *
	 */
	public function getCurrentStoreTimeSlot()
	{

		$response = $this->model->getCurrentStoreTimeSlot($_POST);
		echo json_encode($response);
		exit;
	}
	/**
	 *
	 * Apply Discount
	 *
	 */
	function applyDiscount()
	{
		$this->title = TITLE;
		$model       = $this->model;
		$result      = $model->geDiscountDetails($_POST);
		echo json_encode($result);
		exit;
	}
	/**
	 *
	 * Checkout confirm
	 *
	 */
	function checkout_confirm()
	{

		$this->title = TITLE;
		$model       = $this->model;
		
		$order_det   = $model->check_out_confirm($this->UrlArray[2]);
        $deliveryTime   = $model->deliveryTime($order_det->Data->sub_total);
        $time  = $deliveryTime->Data;
        $messege = $time->message;
		
		$this->smarty->assign('date', @$order_det->Data->date);
		$this->smarty->assign('check_now_specific', @$order_det->Data->check_now_specific);
		$this->smarty->assign('delivery_type', @$order_det->Data->delivery_type);
		$this->smarty->assign('time', @$order_det->Data->time);
		$this->smarty->assign('billing_type', ucfirst(@$order_det->Data->billing_type));
		$this->smarty->assign('billing_card_no', @$order_det->Data->billing_card_no);
		$this->smarty->assign('address', @$order_det->Data->address);
		$this->smarty->assign('delivery_instructions', @$order_det->Data->delivery_instructions);
		$this->smarty->assign('total', @$order_det->Data->total);
		$this->smarty->assign('sub_total', @$order_det->Data->sub_total);
		$this->smarty->assign('tip', @$order_det->Data->tip);
		$this->smarty->assign('tax', @$order_det->Data->tax);
		$this->smarty->assign('delivery_fee', @$order_det->Data->delivery_fee);
		$this->smarty->assign('order_item_detail', @$order_det->Data->order_item_detail);
	
		$this->smarty->assign('discount', @$order_det->Data->discount);
		$this->smarty->assign('orderid', $this->UrlArray[2]);

		$user = $model->getUserDetails(@$_SESSION['uid']);
		$this->smarty->assign('user', $user);
		$review_content = $model->get_all_review_contents();
		$this->smarty->assign('review_content', $review_content);
		$data['uid'] = @$_SESSION['uid'];
		$user_review = $model->get_user_review($data);
		$this->smarty->assign('user_review', $user_review);

		$count = array(
				1,
				2,
				3,
				4,
				5,
				6,
				7,
				8,
				9,
				10
		);
		$this->smarty->assign('count', $count);
		$this->smarty->assign('messege', $messege);
        
	
		$this->smarty->assign('content', "check-out-confirm.tpl");


	}
	/**
	 *
	 * List cart count
	 *
	 */
	public function getCartCount()
	{
		$count           = 0;
		if (isset($_SESSION['uid']) || isset($_COOKIE['user_id'])) {
            $uid = @$_SESSION['uid'] ? @$_SESSION['uid'] : @$_COOKIE['user_id'];
			$model           = $this->model;
			$data            = $model->get_user_checkout_data($uid);
			
			if(isset($data) && $data){
				
				
				$outdata = json_decode($data);
				foreach($outdata->Data as $counts){
					$count = $count+$counts->item_qty;
				}
				
			}
			 
		}  
		
		return $count;

	}
	/**
	 *
	 * Place order
	 *
	 */
	function place_order()
	{
		// echo "<pre>";print_r($_POST);exit;
		$model = $this->model;
		$model->confirm_cart();
		unset($_SESSION['orders']);
		exit;
	}
	
	public function removeCard(){
		echo $this->model->removeCard($_POST);
		exit;
	}
	
	
	
	public function updatePickupOrDelivery(){
		if (isset($_SESSION['uid'])) {
			$model           = $this->model;			
			$user["user_id"] = $_SESSION['uid'];
			
			$deliveryOrPickup = $_POST['deliveryOrPickup'];
			
			$shopitems = $model->cartItems($user);
			$output            = $this->getCartItemList($shopitems, $deliveryOrPickup);
			
			$response = new stdClass();
			$response->status  = true;
			$response->message = $output;
			echo json_encode($response);
			exit;
			
		}
		
		
		
	}
	
	public function saveTempUserSelectionData(){
		$_SESSION['tempUserSelectionData'] = $_POST['data'];
		echo "true";
		exit;
	}
	
	public function getTempUserSelectionData(){
		if(isset($_SESSION['tempUserSelectionData']) && $_SESSION['tempUserSelectionData']){
			echo $_SESSION['tempUserSelectionData'];
		} else {
			echo '{"orderType":null,"addressId":null,"cardId":null,"storeId":null,"isNotTimeNow":null,"specificTime":null}';
		}
		exit;
	}
	
	public function checkZip(){
		$model  = $this->model;
	
		$result = $model->checkZip($_POST);
		print_r(json_encode($result));
		//echo $result;
		exit;
	}
	public function get_storeClosed_days()
    {
       $model = $this->model;

        $result = $model->get_storeClosed_days($_POST);
        print_r(json_encode($result));
        exit;
    }

    public function getCardZIp()
    {
        $model = $this->model;
        $result = $model->getCardZIp($_POST);
        print_r(json_encode($result));
        exit;
    }

}
