<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:06:51
  from 'C:\wamp64\www\hashbury\_mobile\app\theme\templates\featured_sandwiches.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7cdbb2ea55_37735081',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '53113fbc4b5fe7c7010d7be1b9f822ae06ea6f1c' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile\\app\\theme\\templates\\featured_sandwiches.tpl',
      1 => 1616673980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7cdbb2ea55_37735081 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('random_number', rand(100000,999990));?>
<section class="sandwich new_sandwich_mobile">
            <div class="container">
                <div class="sandwich-wrapper sandwich-new-wrapp menu-listing">
                    <h3>Featured Sandwiches<span> <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="back_new"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/back_new.png">
                            <b>Back</b></a></span><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="back_new">
                        </a></h3>


                    <?php
$__section_sandwitch_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['featured_data']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_0_start = min(0, $__section_sandwitch_0_loop);
$__section_sandwitch_0_total = min(($__section_sandwitch_0_loop - $__section_sandwitch_0_start), $__section_sandwitch_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_0_total !== 0) {
for ($__section_sandwitch_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_0_start; $__section_sandwitch_0_iteration <= $__section_sandwitch_0_total; $__section_sandwitch_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>

            <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0]);?>
            <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
            <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
            <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
            <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>

            <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['bread_name']->value);?>

            <?php 
            $result = '';
            $d = $_smarty_tpl->get_template_vars('prot_data');
            if($d){
            foreach($d as $d){

            $d = trim($d);
            $d = str_replace(' ','##',$d);
            $result .= ' '.$d;
          }}
          ?>


          <?php 
          $result_1 = '';
          $c = $_smarty_tpl->get_template_vars('cheese_data');
          if($c){
          foreach($c as $c){

          $c = trim($c);
          $c = str_replace(' ','##',$c);

          $result_1 .= ' '.$c;
        }}
        ?>


        <?php 
        $result_2 = '';
        $t = $_smarty_tpl->get_template_vars('topping_data');
        if($t){
        foreach($t as $t){
        $t = trim($t);
        $t = str_replace(' ','##',$t);
        $result_2 .= ' '.$t;
      }}
      ?>

      <?php 
      if($o){       
      $result_3 = '';
      $o = trim($o);
      $o = str_replace(' ','##',$o);
      $o = $_smarty_tpl->get_template_vars('cond_data');
      foreach($o as $o){
      $result_3 .= ' '.$o;
      }}
      ?>

      <?php if (is_array($_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id'])) {?> 
      <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id']));?>
      <?php } else { ?>
      <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['by_admin'] == 1) {?>
      <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['ADMIN_URL']->value);?>
      <?php } else { ?>
      <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>
      <?php }?>

      <?php $_smarty_tpl->_assignInScope('sdesc', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc']);?>
      <?php $_smarty_tpl->_assignInScope('fname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['first_name']);?>
      <?php $_smarty_tpl->_assignInScope('lname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['last_name']);?>
      <?php $_smarty_tpl->_assignInScope('user_name', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name']);?>
      <?php $_smarty_tpl->_assignInScope('sprice', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price']);?>
      <?php $_smarty_tpl->_assignInScope('sname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name']);?>
      <?php $_smarty_tpl->_assignInScope('tname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name_trimmed']);?>
    <div class="save-sandwich--wrap" data-id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
">
        <input type="hidden" id="featured_sandwich_view_popup" value="1">
        <div class="save-sand--img">
          <span><img class="view_sandwich" title="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png"></span>
        </div>

        <div class="save-sand--content save-sand--content-new" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc_id'];?>
" data-flag="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['flag'];?>
" data-menuadds="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_add_count'];?>
" data-userid ="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
" <?php if ($_smarty_tpl->tpl_vars['items_id']->value == 1) {?> rel="ADD TO CART" <?php } else { ?> rel="ADD TO CART" <?php }?> data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-toast="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'];?>
" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-likeid="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_id'];?>
" data-likecount="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_count'];?>
" data-typeSandwich="FS">

            <input type="hidden" name="chkmenuactice<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active'];?>
" />
            
            <?php $_smarty_tpl->_assignInScope('jsonData', json_decode($_smarty_tpl->tpl_vars['sandwich']->value->sandwich_data,1));?>
          
            <span class="saved-sand--price"><?php echo $_smarty_tpl->tpl_vars['sname']->value;?>
</span>
            <p><?php echo $_smarty_tpl->tpl_vars['sdesc']->value;?>
</p>
            
            <img data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" id="sandwichimg_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png" hidden>
            <p>Created by: <?php echo $_smarty_tpl->tpl_vars['user_name']->value;?>
.</p>

            <div id="featured_new_mobile">
              
              <h2>$<?php echo $_smarty_tpl->tpl_vars['sprice']->value;?>
</h2>
              <div>
                <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                  <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                  <a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
                  <input id="sandwichQty<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" name="itemQty" type="text" class="text-box" value="01" readonly="">
                  <a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
                </h3>
              </div>
              <div class="saved-add-cartwrap">
                  <a class="saved-add-cart quickAddToCart" href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/cart.png"><span>Add</span></a>
              </div>

            </div>
              <div class="saved-item-btns saved-item-btns-new">
                <a href="#" class="view_sandwich" id="view-sandwich-popup">View</a>
                <!-- <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">Edit</a> -->
                <a href="javascript:void(0);" class="editSandwich" id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">Edit</a>
                <?php if (in_array($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['saved_data']->value)) {?>
                                  
                                  <a href="#" class="saved">SAVED</a>
                                  <input type="hidden" name="saved_tgl<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="1">
                                  <?php } else { ?>
                                  <a class="featured_item_btn" id="save_toggle<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" href="#">SAVE</a>
                                <?php }?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['saved_data']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
                                    <?php if ($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'] == $_smarty_tpl->tpl_vars['v']->value) {?>
                                       
                                        <?php if ($_smarty_tpl->tpl_vars['is_public_array']->value[$_smarty_tpl->tpl_vars['v']->value] == 0) {?>
                                        <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['is_public_array']->value[$_smarty_tpl->tpl_vars['v']->value];?>
" id="isPublic<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">
                                           <span class="locked-item" id="lockid_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/locked-itm.png"></span>
                                        <?php }?> 
                                        <?php } else { ?>
                                     <span class="locked-item"></span>
                                    <?php }?>

                                     
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    
            </div>
        </div>
  <input class="typeSandwich" type="hidden" value="FS">

  <input type="hidden" value="product" name="data_type">
 <!--  <div class="add-new no-items">
   

  </div> -->
</div>
<?php
}
}
?>

                 
                </div>

        </div></section><?php }
}
