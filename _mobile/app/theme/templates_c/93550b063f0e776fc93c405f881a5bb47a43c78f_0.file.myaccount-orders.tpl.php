<?php
/* Smarty version 3.1.39, created on 2021-03-30 01:29:19
  from 'C:\wamp64\www\hashbury\_mobile\app\theme\templates\myaccount-orders.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6062b72fbffbd5_21782498',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '93550b063f0e776fc93c405f881a5bb47a43c78f' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile\\app\\theme\\templates\\myaccount-orders.tpl',
      1 => 1602772819,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062b72fbffbd5_21782498 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_mobile\\app\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

<section class="sandwich create-sandwhich-menu-wrapper">
<div class="container">
  <!-- ORDER 1 -->
  <?php if (count($_smarty_tpl->tpl_vars['orderhistory']->value) > 0) {?> 
   <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orderhistory']->value, 'history', false, 'id');
$_smarty_tpl->tpl_vars['history']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['history']->value) {
$_smarty_tpl->tpl_vars['history']->do_else = false;
?>
  
  <div class="order-history">
   
  <div class="container">
  <div class="row">
  <div class="right-float">
     <?php if ($_smarty_tpl->tpl_vars['history']->value->itemCheck > 0) {?>
  <a href="#" data-order= "<?php echo $_smarty_tpl->tpl_vars['history']->value->order_id;?>
" data-from="order_history" class="recorder reorder-button">REORDER</a>
  <?php }?>
  </div>
  <div class="left-float">
  <h2><?php echo mb_strtoupper(smarty_modifier_date_format($_smarty_tpl->tpl_vars['history']->value->timestampDate,"%a, %b %e, %Y"), 'UTF-8');?>
</h2>


  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value->items, 'item', false, 'id');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>

  		<h3 class="order-product-name">(<?php echo $_smarty_tpl->tpl_vars['item']->value->qty;?>
) <?php if ($_smarty_tpl->tpl_vars['item']->value->type == 'user_sandwich') {?><span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['item']->value->sandwich_name, 'UTF-8');?>
</span><?php }?>
			<?php if ($_smarty_tpl->tpl_vars['item']->value->type == 'product') {?><span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['item']->value->product_name, 'UTF-8');?>
</span><?php }?>
  		</h3>  		
  		<p><?php echo $_smarty_tpl->tpl_vars['item']->value->desc;?>
</p>
      

        <span class="item_details" style="display: none;" data-bread="<?php echo $_smarty_tpl->tpl_vars['item']->value->bread;?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['item']->value->desc;?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['item']->value->desc_id;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
"></span>
      
  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  
  
  <div class="deliver">
  <p><span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['history']->value->delivery_type, 'UTF-8');?>
 : </span>&nbsp;<?php echo $_smarty_tpl->tpl_vars['history']->value->address->address1;?>
</p>
  <p><span>PAYMENT : </span>&nbsp;<?php echo mb_strtoupper($_smarty_tpl->tpl_vars['history']->value->billing_type, 'UTF-8');?>
 <?php echo $_smarty_tpl->tpl_vars['history']->value->billing_card_no;?>
</p>
  </div>
  </div>
  </div>
  </div>
  </div>
  
  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  <?php } else { ?>
  <span><h3 class="no_order_history">No Previous Orders.</h3></span>
  <?php }?>

</div>
</section>

<?php }
}
