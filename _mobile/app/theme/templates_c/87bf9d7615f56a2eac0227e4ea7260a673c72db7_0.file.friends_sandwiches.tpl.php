<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:07:45
  from 'C:\wamp64\www\hashbury\_mobile\app\theme\templates\friends_sandwiches.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7d11d9f7b7_78929675',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '87bf9d7615f56a2eac0227e4ea7260a673c72db7' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile\\app\\theme\\templates\\friends_sandwiches.tpl',
      1 => 1592287015,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7d11d9f7b7_78929675 (Smarty_Internal_Template $_smarty_tpl) {
?> <?php $_smarty_tpl->_assignInScope('random_number', rand(100000,999990));?>
 <input type="hidden" name="countMenuItems" value="<?php echo $_smarty_tpl->tpl_vars['numMenu']->value;?>
"/>
 <div id="frnDpopmenuz" class="sandwich-popup" style="display:none">
   <div class="summary-details-friends-menu">
     <a class="close-button" href="#"></a>
     <h2>Friends' Menus</h2>
     <section class="sandwich-menu" style="position:relative">

       <ul class="frndpopcont">
        <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/friends_menu.gif" style="margin: 0px auto; display:block; margin-top: 100px;" /> 
      </ul>
    </section>

  </div>  
</div>

<section class="sandwich new_sandwich_mobile">
  <div class="container">

    <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Friends’ Sandwiches<span> <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="back_new"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/back_new.png">
                                <b>Back</b></a></span><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="back_new">
                        </a></h3>
                <?php if ($_smarty_tpl->tpl_vars['fbCheck']->value == 1) {?>
                  <?php if (!empty($_smarty_tpl->tpl_vars['data']->value)) {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'item', false, 'key');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
                    <div class="friends-sandwich--block friends-sandwiches-all">
                        <div class="friends-sandwich ">
                            <div class="friends-dp"><img src="https://graph.facebook.com/<?php echo $_smarty_tpl->tpl_vars['item']->value->friend_uid;?>
/picture?width=107&height=107"></div>
                            <div class="friend-name friends-all">
                                <h2><?php echo $_smarty_tpl->tpl_vars['item']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['item']->value->last_name;?>
</h2>
                                <p><?php echo $_smarty_tpl->tpl_vars['item']->value->sandwich_count;?>
 Sandwiches</p>
                            </div>
                            <div class="friends-view-wrap">
                                <div class="friends-view--btn"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/friendsMenu/<?php echo $_smarty_tpl->tpl_vars['item']->value->uid;?>
" class="freinds-view">view</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  <?php } else { ?>
                    <div class="friends-share">
                      <p>LET YOUR FRIENDS KNOW ABOUT BARNEY BROWN!</p>
                      <a href="javascript:void(0);" id="Share-BarneyBrown" class="viewall-saved-sandwiches"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/view-all--border.png"><span>SHARE</span></a>
                    </div>
                  <?php }?>
                <?php } else { ?>
                <div class="friends-share">
                      <p>PLEASE CONNECT TO FACEBOOK TO SEE FRIENDS' SANDWICHES</p>
                      <a target="_blank" href="<?php  echo _URL; ?>" class="viewall-saved-sandwiches"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/view-all--border.png"><span>CONNECT</span></a>
                    </div>
                <?php }?>
                </div>
</div>

</section>
<?php }
}
