<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:06:39
  from 'C:\wamp64\www\hashbury\_mobile\app\theme\templates\sandwich_menu.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7ccf8ae0f3_74753598',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '17e7e41576b654f79a7a3462b84c753eb38abbc7' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile\\app\\theme\\templates\\sandwich_menu.tpl',
      1 => 1616673980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7ccf8ae0f3_74753598 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('random_number', rand(100000,999990));?>
<section class="sandwich new_sandwich_mobile">
            
            <div class="container">
              <div class="new_gallery_count">
              <p><?php echo $_smarty_tpl->tpl_vars['galleryCount']->value;?>
</p>
              <span>shared creations</span>
              <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/gallery/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">VIEW ALL</a>
            </div>
                <div id="sandwich_overview_mobile" class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Create a Sandwich</h3>
                    <ul class="user-menu-sandwiches">
                        <li class=" vertical-aligned">
                            <span class="new_create__sandwich">
                                 <div class="homebannerauto">
                                  <div class="homepageloader">
<img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/ellipsis.svg" class="homeAnimationLoaderImg" alt="">
</div>
        <span class="homepageAutoBanner">
        <span class="sandwich-1">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/overviewpage_animation/sandwich1/1.png" style="display:block" alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich1/2.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich1/3.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich1/4.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich1/5.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich1/6.png" style="display:none"  alt="">
        </span>
        
        <span class="sandwich-2">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich2/7.png"  style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich2/8.png"  style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich2/9.png"  style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich2/10.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich2/11.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich2/12.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich2/13.png" style="display:none"  alt="">
        </span>
        
        <span class="sandwich-3">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich3/14.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich3/15.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich3/16.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich3/17.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich3/18.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich3/19.png" style="display:none"  alt="">
          <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/overviewpage_animation/sandwich3/20.png" style="display:none"  alt="">
        </span>
        
        </span>
        
 <?php echo '<script'; ?>
 type="text/javascript">
 window.addEventListener('load',function(){ mobile_ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
 <?php echo '</script'; ?>
>

      </div> 
                            </span>
                        </li>
                        <li class="vertical-aligned">
                            <div class="right-a ">
                                <h5 class="create_own__sandwich">sandwich customizer</h5>
                                    <h2>Build Your Own in 5 Easy Steps</h2>
                                <a class="get-started__new" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">CUSTOMIZE</a>
                                
                            </div>
                        </li>


                    </ul>

                    <div class="loadMoreGallery" style="text-align:center"><img
                            src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/star.png" alt="Loading" /> </div>

                </div>
                <!-- <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Sandwich Gallery</h3>
                    <ul class="user-menu-sandwiches">
                        <li class="right-rgt vertical-aligned">
                            <span class="new_create__sandwich">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/Layer_6_copy.png">
                            </span>
                        </li>

                        <li class="right-rgt vertical-aligned">
                            <div class="right-a ">
                                <h5 class="create_own__sandwich shared-sandwich__head">
                                    <p class="big-count"><?php echo $_smarty_tpl->tpl_vars['galleryCount']->value;?>
</p>
                                    Shared Sandwiches
                                </h5>
                                <a class="get-started__new" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/gallery">VIEW GALLERY</a>
                                 <p>Use one of the options to the left to get started!</p>
                            </div>
                        </li>


                    </ul>

                    <div class="loadMoreGallery" style="text-align:center"><img
                            src="http://mobile.ha.allthingsmedia.com/app/images/star.png" alt="Loading" /> </div>

                </div> -->
                <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>My Saved Sandwiches <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/savedSandwiches/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="view-all-new"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a></h3>
                    <?php if (!empty($_smarty_tpl->tpl_vars['saved_sandwich_data']->value)) {?>
                              <ul>
                                 
                              <?php
$__section_sandwitch_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['saved_sandwich_data']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_0_start = min(0, $__section_sandwitch_0_loop);
$__section_sandwitch_0_total = min(($__section_sandwitch_0_loop - $__section_sandwitch_0_start), $__section_sandwitch_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_0_total !== 0) {
for ($__section_sandwitch_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_0_start; $__section_sandwitch_0_iteration <= $__section_sandwitch_0_total; $__section_sandwitch_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>
             
              <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0]);?>
              <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>
              
              <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['bread_name']->value);?>
              
              <?php 
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              ?>
      
      
              
                   
                   
                   <?php if (is_array($_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id'])) {?> 
                   <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id']));?>
                   <?php } else { ?>
                   <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
                   <?php }?>
              
              <?php if ($_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['by_admin'] == 1) {?>
              <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['ADMIN_URL']->value);?>
              <?php } else { ?>
              <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>
              <?php }?>
              
              <?php $_smarty_tpl->_assignInScope('sdesc', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc']);?>
              <?php $_smarty_tpl->_assignInScope('fname', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['first_name']);?>
              <?php $_smarty_tpl->_assignInScope('lname', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['last_name']);?>
              <?php $_smarty_tpl->_assignInScope('sprice', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price']);?>
              <?php $_smarty_tpl->_assignInScope('user_name', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name']);?>
              <?php $_smarty_tpl->_assignInScope('sname', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name']);?>
               <?php $_smarty_tpl->_assignInScope('tname', $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name_trimmed']);?>
                    <div class="save-sandwich--wrap" data-id="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
">
        <input type="hidden" id="featured_sandwich_view_popup" value="1">
        <div class="save-sand--img">
          <span><img title="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" class="view_sandwich" data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/toasted.png" class="toasted" style="<?php if ($_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'] == 1) {?> display:block; <?php }?>"></span>
        </div>

        <div class="save-sand--content save-sand--content-new save-sand_three_sec" id="toastID_<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc_id'];?>
" data-flag="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['flag'];?>
" data-menuadds="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_add_count'];?>
" data-userid ="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
" <?php if ($_smarty_tpl->tpl_vars['items_id']->value == 1) {?> rel="ADD TO CART" <?php } else { ?> rel="ADD TO CART" <?php }?> data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-toast="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'];?>
" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-likeid="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_id'];?>
" data-likecount="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_count'];?>
" data-typeSandwich="SS">
            <input type="hidden" name="chkmenuactice<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active'];?>
" />
            
            <?php $_smarty_tpl->_assignInScope('jsonData', json_decode($_smarty_tpl->tpl_vars['sandwich']->value->sandwich_data,1));?>
            <span class="saved-sand--price"><?php echo $_smarty_tpl->tpl_vars['sname']->value;?>
</span>
            <p><?php echo $_smarty_tpl->tpl_vars['sdesc']->value;?>
</p>
            <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['is_public'];?>
" id="isPublic<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">
                           
            <img data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" id="sandwichimg_<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png" hidden>
            <p>Created by: <?php echo $_smarty_tpl->tpl_vars['user_name']->value;?>
.</p>
            <div id="featured_new_mobile">
              <h2>$<?php echo $_smarty_tpl->tpl_vars['sprice']->value;?>
</h2>
              <div>
                 <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
                       <input id="sandwichQty<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" name="itemQuty" type="text" class="text-box" value="01" readonly="">
                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
                </h3>
              </div>
              <div class="saved-add-cartwrap">
                <a class="saved-add-cart quickAddToCart" href="javascript:void(0);"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/cart.png"><span>Add</span></a>
              </div>
            </div>
            
            <div class="saved-item-btns menu-saved_list">
              <a href="javascript:void(0);" class="view_sandwich" id="view-sandwich-popup">View</a>
              <!-- <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">Edit</a> -->
              <a href="javascript:void(0);" class="editSandwich" id="<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">Edit</a>
              <?php if ($_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active'] == 1) {?>
              
              <a class="saved" href="javascript:void(0);">SAVED</a>
              <input type="hidden" name="saved_tgl<?php echo $_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="1">
              <?php } else { ?>
              <a href="javascript:void(0);" class="featured_item_btn">SAVE</a>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['saved_sandwich_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['is_public'] == 0) {?>
              <span class="locked-item"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/locked-itm.png"></span>
              <?php }?>
          </div>
        </div>
  <input class="typeSandwich" type="hidden" value="SS">
  <input type="hidden" name="saved_tgl<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="1">
 <input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $_SESSION['uid'];?>
" >
  <input type="hidden" value="product" name="data_type">
 <!--  <div class="add-new no-items">
    

  </div> -->
</div>
                    <?php
}
}
?> 
                </div>
                <?php } else { ?>
                  <div class="no-saved-sandwiches">
                     <h3>
                     you currently have no saved sandwiches
                     </h3>
                     <p>EITHER CREATE YOUR OWN CUSTOM SANDWICH, CHOOSE FROM OUR USER-SUBMITTED GALLERY, OR TRY ONE OF YOUR FRIEND'S SANDWICHES.
                     </p>
                  </div>
               <?php }?>
                <div class="sandwich-wrapper sandwich-new-wrapp">
                  
                    <h3><span class="span-update">UPDATED MONTHLY!</span><span>Featured Sandwiches </span><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/featuredSandwiches/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="view-all-new"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a></h3>
                    <?php
$__section_sandwitch_1_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['featured_data']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_1_start = min(0, $__section_sandwitch_1_loop);
$__section_sandwitch_1_total = min(($__section_sandwitch_1_loop - $__section_sandwitch_1_start), $__section_sandwitch_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_1_total !== 0) {
for ($__section_sandwitch_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_1_start; $__section_sandwitch_1_iteration <= $__section_sandwitch_1_total; $__section_sandwitch_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>
             
              <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0]);?>
              <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>
              
              <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['bread_name']->value);?>
              
              <?php 
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              ?>
      
      
              
                   
                   
                   <?php if (is_array($_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id'])) {?> 
                   <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id']));?>
                   <?php } else { ?>
                   <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
                   <?php }?>
              
              <?php if ($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['by_admin'] == 1) {?>
              <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['ADMIN_URL']->value);?>
              <?php } else { ?>
              <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>
              <?php }?>
              
              <?php $_smarty_tpl->_assignInScope('sdesc', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc']);?>
              <?php $_smarty_tpl->_assignInScope('fname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['first_name']);?>
              <?php $_smarty_tpl->_assignInScope('lname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['last_name']);?>
              <?php $_smarty_tpl->_assignInScope('sprice', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price']);?>
              <?php $_smarty_tpl->_assignInScope('user_name', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name']);?>
              <?php $_smarty_tpl->_assignInScope('sname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name']);?>
               <?php $_smarty_tpl->_assignInScope('tname', $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name_trimmed']);?> 
                    <div class="save-sandwich--wrap" data-id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
">
        <input type="hidden" id="featured_sandwich_view_popup" value="1">
        <div class="save-sand--img">
          <span><img class="view_sandwich" title="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png"></span>
        </div>

        <div class="save-sand--content save-sand--content-new save-sand_three_sec" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc_id'];?>
" data-flag="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['flag'];?>
" data-menuadds="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_add_count'];?>
" data-userid ="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
" <?php if ($_smarty_tpl->tpl_vars['items_id']->value == 1) {?> rel="ADD TO CART" <?php } else { ?> rel="ADD TO CART" <?php }?> data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-toast="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'];?>
" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['current_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-likeid="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_id'];?>
" data-likecount="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_count'];?>
" data-typeSandwich="FS">

            <input type="hidden" name="chkmenuactice<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active'];?>
" />
            
            <?php $_smarty_tpl->_assignInScope('jsonData', json_decode($_smarty_tpl->tpl_vars['sandwich']->value->sandwich_data,1));?>
          
            <span class="saved-sand--price"><?php echo $_smarty_tpl->tpl_vars['sname']->value;?>
</span>
            <p><?php echo $_smarty_tpl->tpl_vars['sdesc']->value;?>
</p>
            
             <img data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" id="sandwichimg_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png" hidden>
            <p>Created by: <?php echo $_smarty_tpl->tpl_vars['user_name']->value;?>
.</p>

            <div id="featured_new_mobile">
              
              <h2>$<?php echo $_smarty_tpl->tpl_vars['sprice']->value;?>
</h2>
              <div>
                <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                                        <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                                        <a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
                                           <input id="sandwichQty<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" name="itemQuty" type="text" class="text-box" value="01" readonly="">
                                        <a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
                                    </h3>
              </div>
              <div class="saved-add-cartwrap">
                  <a class="saved-add-cart quickAddToCart" href="javascript:void(0);"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/cart.png"><span>Add</span></a>
              </div>

            </div>
              <div class="saved-item-btns saved-item-btns-new menu-saved_list">
                                <a href="javascript:void(0);" class="view_sandwich" id="view-sandwich-popup">View</a>
                                <!-- <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">Edit</a> -->
                                <a href="javascript:void(0);" class="editSandwich" id="<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">Edit</a>
                                <?php if (in_array($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['saved_sandwich_ids']->value)) {?>
                                
                                <a href="javascript:void(0);" class="saved ">SAVED</a>
                                <input type="hidden" name="saved_tgl<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="1">
                                <?php } else { ?>
                                <a class="featured_item_btn" href="javascript:void(0);">SAVE</a>
                                <?php }?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['saved_sandwich_ids']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
                                  <?php if ($_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'] == $_smarty_tpl->tpl_vars['v']->value) {?>
                                      <?php if ($_smarty_tpl->tpl_vars['is_public_array']->value[$_smarty_tpl->tpl_vars['v']->value] == 0) {?>
                                      <input type="hidden" value="0" id="isPublicFs<?php echo $_smarty_tpl->tpl_vars['featured_data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
"> 
                                          <span class="locked-item"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/locked-itm.png"></span>
                                      <?php }?>
                                  <?php }?>

                                     
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    
            </div>
        </div>
  <input class="typeSandwich" type="hidden" value="FS">
<input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $_SESSION['uid'];?>
" >
  <input type="hidden" value="product" name="data_type">
 <!--  <div class="add-new no-items">
   

  </div> -->
</div>
                    <?php
}
}
?>
                </div>
                <div class="sandwich-wrapper sandwich-new-wrapp bottom-blank">
                    <h3><span class="fb-left-head"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/facebook-round.png"></span>Friends’ Sandwiches<a
                            href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/friendSandwiches/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="view-all-new">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a>
                    </h3>
                    <?php if (!empty($_smarty_tpl->tpl_vars['fbdata']->value)) {?>
                    <div class="friends-sandwich--block">
                        <?php $i = 1;?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fbdata']->value, 'item', false, 'key');
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
?>
                            <?php  if ($i <=5){ ?>
                        <div class="friends-sandwich">
                            <div class="friends-dp"><img src="https://graph.facebook.com/<?php echo $_smarty_tpl->tpl_vars['item']->value->friend_uid;?>
/picture?width=107&height=107"></div>
                            <div class="friend-name">
                                <h2><?php echo $_smarty_tpl->tpl_vars['item']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['item']->value->last_name;?>
</h2>
                                <p><?php echo $_smarty_tpl->tpl_vars['item']->value->sandwich_count;?>
 saved sandwiches</p>
                            </div>
                            <div class="friends-view-wrap">
                                <div class="friends-view--btn"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/friendsMenu/<?php echo $_smarty_tpl->tpl_vars['item']->value->uid;?>
" class="freinds-view">view</a>
                                </div>
                            </div>
                        </div>
                        <?php $i++; }?>
                              
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                        <div>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/friendSandwiches" class="viewall-saved-sandwiches"><img
                                    src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/view-all--border.png"><span>view all
                                    <!--connect--></span></a>
                        </div>
                    </div>
                    <?php } else { ?>
                           <div class="friends-items-bottom not-connected-fb">
                              <p>CONNECT TO FACEBOOK TO SEE YOUR FRIENDS’ SANDWICHES!</p>
                              <div class="friends-items-view">
                                 <a href="#" class="viewall-saved-sandwiches"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/view-all--border.png"><span class="friends-items-view-h2 login-facebook">CONNECT</span></a>
                              </div>
                           </div>
                    <?php }?>
                </div>
            </div>
        </section><?php }
}
