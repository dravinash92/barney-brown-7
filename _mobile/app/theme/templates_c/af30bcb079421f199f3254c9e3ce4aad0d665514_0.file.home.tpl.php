<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:06:24
  from 'C:\wamp64\www\hashbury\_mobile\app\theme\templates\home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7cc09bb7e5_39710033',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'af30bcb079421f199f3254c9e3ce4aad0d665514' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile\\app\\theme\\templates\\home.tpl',
      1 => 1616673980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7cc09bb7e5_39710033 (Smarty_Internal_Template $_smarty_tpl) {
if (count($_smarty_tpl->tpl_vars['pastOrder']->value) == 0) {?>
<div class="homepageloader">
<img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/ellipsis.svg" class="homeAnimationLoaderImg" alt="">
</div>
<?php }?>

<div class="banner home-banner-div home-banner-div-new"> 
	<div class="banner-wrapper"> 
	 
	
	<?php if ($_smarty_tpl->tpl_vars['uid']->value != '' && count($_smarty_tpl->tpl_vars['pastOrder']->value) > 0) {?>  
			<?php if ($_smarty_tpl->tpl_vars['pastOrder']->value) {?>
			<?php if ($_smarty_tpl->tpl_vars['pastOrder']->value['details']['by_admin'] == 1) {?>
			  <?php $_smarty_tpl->_assignInScope('path', $_smarty_tpl->tpl_vars['ADMIN_URL']->value);?>		
			<?php } else { ?>
			  <?php $_smarty_tpl->_assignInScope('path', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>	
			<?php }?>
			
			<?php $_smarty_tpl->_assignInScope('image_exist', ($_smarty_tpl->tpl_vars['image_path']->value).($_smarty_tpl->tpl_vars['pastOrder']->value['details']['image']));?>	
			<?php 
				$d = $_smarty_tpl->get_template_vars('image_exist');		 
				$ch = curl_init($d);
				curl_setopt($ch, CURLOPT_NOBODY, true);
				curl_exec($ch);
				$retCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);		
			?>
			
	
			<?php  if($retCode==200){ ?>	
			<span class="new_banner-top">			
				<img class="banner-login" src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['pastOrder']->value['details']['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['title_text']->value;?>
">
				<img class="banner-img-top" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/banner-top.png" alt="banner-top">
			</span>
			<?php  } else { ?>
				<img class="banner-login" src='<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/BB_logo_placeholder.png' >
			<?php  } ?>
		
		<h2>Your Most Recent Order</h2>
		<h3><?php echo $_smarty_tpl->tpl_vars['pastOrder']->value['street'];?>
</h3>
		<p> <?php if ($_smarty_tpl->tpl_vars['pastOrder']->value['details']['is_delivery'] == 0) {?> pick-up <?php } else { ?> Delivery <?php }?> - <?php echo $_smarty_tpl->tpl_vars['pastOrder']->value['date'];?>
</p>
		<?php if ($_smarty_tpl->tpl_vars['pastOrder']->value['details']['bread'] != '') {?>
            <span class="item_details" data-bread="<?php echo $_smarty_tpl->tpl_vars['pastOrder']->value['details']['bread'];?>
" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['pastOrder']->value['details']['desc'];?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['pastOrder']->value['details']['desc_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['pastOrder']->value['details']['item_id'];?>
"></span>
        <?php }?>
		<div class="button-wrapper"> 
			<?php if ($_smarty_tpl->tpl_vars['itemsAvailable']->value > 0) {?>
			<a href="#" class="reorder reorder-item" data-order="<?php echo $_smarty_tpl->tpl_vars['order_id']->value;?>
" data-from="home_page">reorder</a>
			<?php }?>
			<a class="place-new-order" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/?pid=<?php echo rand(100000,999990);?>
">place new order</a>
		</div>
		<?php } else { ?>
		
		<?php }?>
	 
		<?php } else { ?>
		
		
	   <div class="homebannerauto">
	    
     	  <span class="homepageAutoBanner">
     	  <span class="sandwich-1">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/homepage_animation/sandwich1/1.png" style="display:block" alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich1/2.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich1/3.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich1/4.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich1/5.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich1/6.png" style="display:none"  alt="">
     	  </span>
     	  
     	  <span class="sandwich-2">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/7.png"  style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/8.png"  style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/9.png"  style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/10.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/11.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/12.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich2/13.png" style="display:none"  alt="">
     	  </span>
     	  
     	  <span class="sandwich-3">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/14.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/15.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/16.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/17.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/18.png" style="display:none"  alt="">
     	    <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
/app/images/homepage_animation/sandwich3/19.png" style="display:none"  alt="">
     	  </span>
     	  
     	  </span>
      </div> 	  
      
		  <h2>Sandwiches Your way!</h2>
		  <?php if ($_smarty_tpl->tpl_vars['uid']->value != '') {?>
		    <p>Build your own sandwich in 5 easy steps using a <br>curated selection of the finest local ingredients!</p>
		  	<?php } else { ?>
		  	<p>Welcome to Barney Brown, a custom-made social-sandwiching experience. Get started by building your own sandwich in just 5 easy steps.</p>
		  	<?php }?>

		  		  <a class="place-order" href="<?php if ($_SESSION['uid'] != '') {
echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/<?php } else {
echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/?pid=<?php echo rand(100000,999990);?>
/<?php }?>">get started</a>

		  
 
 <?php echo '<script'; ?>
 type="text/javascript">
 window.addEventListener('load',function(){ mobile_ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
 <?php echo '</script'; ?>
>

		
		 <!-- <img src="<?php echo $_smarty_tpl->tpl_vars['banner_image']->value;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['title_text']->value;?>
">
		  <h2>Sandwiches Your way!</h2>
		  <p>Build your own sandwich in 5 easy steps<br>using a curated selection of the best<br> local breads and other fine ingredients</p>
		  <a class="place-order" href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/">place order</a>
		  
		  -->
		
	<?php }?> 
	</div>
</div>
<section class="sandwich-menu">
<div class="sa-delivery" style="background:#695b45;font:bold 11px/16px 'Open Sans';letter-spacing: 0px;text-align:center;padding:5px 0;">
		<p class="timing"><span>Manhattan Delivery, </span>Mon - Fri 10:00AM - 4:00PM</p>
</div>
  <ul class="home_page_menu">
  	<li>
      <div class="left">		
			<h2>Sandwiches</h2>
			<p>A MENU OF ENDLESS POSSIBILITIES</p>        
      </div>
      <div class="right">  <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/index/?pid=<?php echo rand(100000,999990);?>
" class="view-button">VIEW</a> </div>
    </li>
    <li>		
		<div class="left">		
			<h2>Salads</h2>
			<p>A SELECTION OF OUR CLASSIC SALADS</p>
		
		</div>		
      <div class="right">  <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/salads/?pid=<?php echo rand(100000,999990);?>
" class="view-button">VIEW</a> </div>
    </li>
    
    <li>
      <div class="left">
        <h2>Snacks</h2>
        <p>A LITTLE SOMETHING ON THE SIDE</p>
      </div>
	   <div class="right"> <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/snacks/?pid=<?php echo rand(100000,999990);?>
" class="view-button">VIEW</a> </div>
	   </li>
	   <li>
	  <div class="left">
        <h2>Drinks</h2>
        <p>SOMETHING TO QUENCH YOUR THIRST</p>
      </div>
      <div class="right"> <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/drinks/?pid=<?php echo rand(100000,999990);?>
" class="view-button">VIEW</a> </div>
    </li>
  </ul>
</section>
<?php }
}
