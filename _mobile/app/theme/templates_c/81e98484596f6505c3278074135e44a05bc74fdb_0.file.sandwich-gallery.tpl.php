<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:07:39
  from 'C:\wamp64\www\hashbury\_mobile\app\theme\templates\sandwich-gallery.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7d0b951d78_30431823',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '81e98484596f6505c3278074135e44a05bc74fdb' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile\\app\\theme\\templates\\sandwich-gallery.tpl',
      1 => 1616674053,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7d0b951d78_30431823 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\hashbury\\_mobile\\app\\smarty\\libs\\plugins\\modifier.replace.php','function'=>'smarty_modifier_replace',),1=>array('file'=>'C:\\wamp64\\www\\hashbury\\_mobile\\app\\smarty\\libs\\plugins\\modifier.truncate.php','function'=>'smarty_modifier_truncate',),2=>array('file'=>'C:\\wamp64\\www\\hashbury\\_mobile\\app\\smarty\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<section class="sandwich create-sandwhich-menu-wrapper">
<div class="create-sandwhich-menu">
  	<div class="finalize-wrapper">
        <div class="counts-sandwich"><p class="sandwich_count" data-count="<?php echo $_smarty_tpl->tpl_vars['totalCount']->value;?>
"><?php echo number_format($_smarty_tpl->tpl_vars['totalCount']->value);?>
</p><span>CREATIONS</span></div>	 
      <a href="#" class="filter sandwich_filter_view sandwich_gallery_filter">filters</a>
	
      <div class="clearfix"></div>
 
    </div>
  </div>
  <div class="container sandwich-all-gallery">
    <div class="sandwich-wrapper">
      <form action="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/gallery" id="sandwich-gallery-search" method="post">
        <input class="sandwiches_search" type="text" name="search" value="<?php echo $_smarty_tpl->tpl_vars['serch_term']->value;?>
">
        <button type="submit" class="sandwiches_search_btn">search</button>
      </form>
      <input type="hidden" id="searchTerm" value="<?php echo $_smarty_tpl->tpl_vars['serch_term']->value;?>
">
  <div class="clearfix"></div>
    <div class="clearfix"></div>
       <ul class="menu-listing">
       <?php
$__section_sandwitch_0_loop = (is_array(@$_loop=count($_smarty_tpl->tpl_vars['GALLARY_DATA']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_sandwitch_0_start = min(0, $__section_sandwitch_0_loop);
$__section_sandwitch_0_total = min(($__section_sandwitch_0_loop - $__section_sandwitch_0_start), $__section_sandwitch_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_sandwitch'] = new Smarty_Variable(array());
if ($__section_sandwitch_0_total !== 0) {
for ($__section_sandwitch_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] = $__section_sandwitch_0_start; $__section_sandwitch_0_iteration <= $__section_sandwitch_0_total; $__section_sandwitch_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']++){
?>
             
              <?php $_smarty_tpl->_assignInScope('bread_name', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['BREAD']['item_name'][0]);?>
              <?php $_smarty_tpl->_assignInScope('prot_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['PROTEIN']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cheese_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CHEESE']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('topping_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['TOPPINGS']['item_name']);?>
              <?php $_smarty_tpl->_assignInScope('cond_data', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_data']['CONDIMENTS']['item_name']);?>
              
              <?php $_smarty_tpl->_assignInScope('bread_name', smarty_modifier_replace($_smarty_tpl->tpl_vars['bread_name']->value,' ','##'));?>
              
              <?php 
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              ?>
              
              
                     <?php 
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              ?>
              
              
                     <?php 
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              ?>
              
              
                     <?php 
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              ?>
      
      
              
                   
                   
                   <?php if (is_array($_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id'])) {?> 
                   <?php $_smarty_tpl->_assignInScope('items_id', in_array($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['order_data']->value['user_sandwich']['item_id']));?>
                   <?php } else { ?>
                   <?php $_smarty_tpl->_assignInScope('items_id', '0');?>
                   <?php }?>
              
              <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['by_admin'] == 1) {?>
              <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['ADMIN_URL']->value);?>
              <?php } else { ?>
              <?php $_smarty_tpl->_assignInScope('url', $_smarty_tpl->tpl_vars['SITE_URL']->value);?>
              <?php }?>
              
              <?php $_smarty_tpl->_assignInScope('sname', $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name']);?>
              <div class="p" class="right-rgt" data-sandwich_desc="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc'];?>
" data-sandwich_desc_id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_desc_id'];?>
" data-flag="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['flag'];?>
" data-menuadds="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_add_count'];?>
" data-userid ="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
" <?php if ($_smarty_tpl->tpl_vars['items_id']->value == 1) {?> rel="ADD TO CART" <?php } else { ?> rel="ADD TO CART" <?php }?> data-cheese="<?php  echo $result_1;?>" data-topping="<?php  echo $result_2;?>" data-cond="<?php  echo $result_3;?>" data-toast="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_toast'];?>
" data-formatdate="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['formated_date'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['user_name'];?>
" data-protien="<?php  echo trim($result); ?>" data-bread="<?php echo $_smarty_tpl->tpl_vars['bread_name']->value;?>
"  data-date="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['date_of_creation'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_price'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['sandwich_name'];?>
" data-likeid="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_id'];?>
" data-likecount="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['like_count'];?>
" data-typeSandwich="FS">
       			 <li>
                           <img data-href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" data-src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/thumbnails/sandwich_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png" alt="sandwitchimageview" class="sandwich_gallery_view">
<input type="hidden" id="sandwichGalleryImg_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;
echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
/sandwich_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['uid'];?>
.png">

                           <input type="hidden" name="chkmenuactice<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['menu_is_active'];?>
" />
           
           <h4 class="sandwich_gallery_view"><?php echo smarty_modifier_truncate(mb_strtoupper($_smarty_tpl->tpl_vars['sname']->value, 'UTF-8'),20,"..",true);?>
</h4>
           <a href="#" class="add-cart" id="view-sandwich-popup">VIEW</a>
           <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['saved_data']->value, 'v', false, 'k');
$_smarty_tpl->tpl_vars['v']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->do_else = false;
?>
              <?php if ($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'] == $_smarty_tpl->tpl_vars['v']->value) {?>
                  <?php if ($_smarty_tpl->tpl_vars['is_public_array']->value[$_smarty_tpl->tpl_vars['v']->value] == 0) {?>
                  <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['is_public_array']->value[$_smarty_tpl->tpl_vars['v']->value];?>
" id="isPublic<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
">
                  <?php }?> 
              <?php }?>
          <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          <?php if (in_array($_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'],$_smarty_tpl->tpl_vars['saved_data']->value)) {?>
                                <input type="hidden" name="saved_tgl<?php echo $_smarty_tpl->tpl_vars['GALLARY_DATA']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sandwitch']->value['index'] : null)]['id'];?>
" value="1">
                              <?php }?>
         </li>
           <input type="hidden" name="hidden_uid" id="hidden_uid" value="<?php echo $_SESSION['uid'];?>
" >
         </div>   
          <?php
}
}
?>  
       </ul>
    
    
     <div class="loadMoreGallery" style="text-align:center"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/star.png" alt="Loading" /> </div>
    
    
    
    
    
    </div>
  </div>
</section>
<div class="sandwich-popup" id="sandwich-popup-gallery" style="display:none;">
<div class="summary-details-add-menu">
<div class="banner"> <a href="#" class="close-button">&nbsp;</a> <img src="images/added-menu-sandwiches.png" alt="<?php echo $_smarty_tpl->tpl_vars['title_text']->value;?>
">
</div>
    <div class="final-list-wrapper">
	<h2 class="sandwich_name">JON'S MOZZARELLA MASTERPIECE</h2>
	<p class="view-popup-scroll">Corned Beef (1.0), Lettuce (N) Red Onion (L), Pickles (L), Tomato (N), Lettuce (L) Red Onion (L), Pickles (L), Lettuce (L) Red Onion (L), Pickles (L)  </p>
	<ul>
	<li>Created by:  <span class="user_name">Jon C</span>.</li>
	<li>Date Created:  <span class="created_date">10/14/14</span></li>

	</ul>
	
	<div class="toastit-wrapper">
    	<div class="checkbox-holder-final">
      	<input type="checkbox" class="savetousertoast toast" value="check1" name="check" id="check1">
        <label for="check1"> <span>Toast it!</span> <!-- <span>Yum!</span> --></label>
      </div>
    </div>
	
	<div class="add-menu">

	 <a class="share share-sandwich" href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/share-img.png" alt=""></img><span class="Share_sandwich">Share</span></a>  
	<a class="add-to-menu save-menu" href="#">Save to my menu</a>
	      <input type="hidden" name="hidden_sandwich_data" id="hidden_sandwich_data" value="user_sandwich">
        <input type="hidden" name="hidden_sandwich_id" id="hidden_sandwich_id" value="">
	</div>
	 
	<div class="add-cart">
	<h2 class="amount">$10.00</h2>
	<h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
	    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
		<a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
           <input id="resetQty" name="itemQuty" type="text" class="text-box"  value="01" readonly>
		<a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
	</h3>
	<a class="add-to-cart" href="#">Add to cart</a>
	</div>
	
    </div>	
  </div>
</div>

<!-- Late loading data -->
<input name="total_item_count" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['total_item_count']->value;?>
" />
<input name="sort_type" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['sort_id']->value;?>
" />

<div class="sandwich-popup" id="sandwich-popup-filter" style="display:none;">
  <div class="summary-details-login summary-details-discount"> <a href="#" class="close-button-new"></a>
    <div class="saved-creditcards-wrapper">
      <h2 class="filter-option">filter Options <a href="#" class="clear-all-filter">Clear Filters</a></h2>
 
      <ul class="from-holder">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category', false, 'i');
$_smarty_tpl->tpl_vars['category']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->do_else = false;
?>

		 <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['category']->value, 'data', false, 'k');
$_smarty_tpl->tpl_vars['data']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->do_else = false;
?>
		   <?php if ($_smarty_tpl->tpl_vars['data']->value->category_identifier) {?>

		 
        <li> <span class="text-box-holder">
			<div class="mobile-popup-filter">
				<p class="items"><?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['data']->value->category_name);?>
</p>
             <div class="list-options scroll-bar filterToggle" id="fit-<?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
" style="display:none;"> 
              <ul>
              <?php if ($_smarty_tpl->tpl_vars['data']->value->category_name != "bread") {?>
                <li>
                  <input id="No <?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
" type="checkbox" class="<?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
 isFiltered" name="<?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
" value="No <?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
">
                  <label for="No <?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
">No <?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
</label>
                </li>
            
                <?php }?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryItems']->value, 'categoryItem', false, 'j');
$_smarty_tpl->tpl_vars['categoryItem']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['categoryItem']->value) {
$_smarty_tpl->tpl_vars['categoryItem']->do_else = false;
?>
		            
		            	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryItem']->value, 'dataitems', false, 'j');
$_smarty_tpl->tpl_vars['dataitems']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['j']->value => $_smarty_tpl->tpl_vars['dataitems']->value) {
$_smarty_tpl->tpl_vars['dataitems']->do_else = false;
?>
                     <?php if ($_smarty_tpl->tpl_vars['data']->value->id == $_smarty_tpl->tpl_vars['dataitems']->value->category_id) {?>
            <li>
                  <input id="<?php echo $_smarty_tpl->tpl_vars['dataitems']->value->item_name;?>
" type="checkbox" class="<?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
 isFiltered" name="<?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
" value="<?php echo $_smarty_tpl->tpl_vars['dataitems']->value->item_name;?>
">
                  <label for="<?php echo $_smarty_tpl->tpl_vars['dataitems']->value->item_name;?>
"><?php echo $_smarty_tpl->tpl_vars['dataitems']->value->item_name;?>
</label>
                </li>
                
             <?php }?>
              
                  <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	               
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 
       
              </ul>
           </div>
               <span class="filter">
                 <div class="FilterLable" id="<?php echo $_smarty_tpl->tpl_vars['data']->value->category_name;?>
">Select</div>  </span>
           </div>
              </span> </li>
          
              <?php }?> 
              
             <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	                 
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

          <span class="border"></span>
          <a href="#" class="apply-filter">APPLY FILTERS</a>
          <div class="cancel-wrapper"><a href="#" class="cancel-button">CANCEL</a></div>
      </ul>
    </div>
  </div>
</div>
<?php }
}
