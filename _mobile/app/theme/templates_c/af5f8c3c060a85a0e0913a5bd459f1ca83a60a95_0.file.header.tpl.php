<?php
/* Smarty version 3.1.39, created on 2021-03-25 08:03:57
  from 'C:\wamp64\www\hashbury\_mobile\app\theme\templates\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_605c7c2da0f853_58976947',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'af5f8c3c060a85a0e0913a5bd459f1ca83a60a95' => 
    array (
      0 => 'C:\\wamp64\\www\\hashbury\\_mobile\\app\\theme\\templates\\header.tpl',
      1 => 1602577185,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605c7c2da0f853_58976947 (Smarty_Internal_Template $_smarty_tpl) {
$FB = new FB_API();
$access_token  = @$_SESSION['access_token'];

if(!isset($_SESSION['FBRLH_state']) || !$_SESSION['FBRLH_state']  ){

$FB->FB_check_login();
$url =  $FB->FB_get_url();
$_SESSION['tmpUrl'] = $url;
$cookietime        = time() + 60 * 60 * 24 * 60;
setcookie("fbloginurl",$url,$cookietime, '/', NULL);
define('_URL',$url);

} else {

define('_URL',$_SESSION['tmpUrl']);

}


  
?>
 <?php $_smarty_tpl->_assignInScope('random_number', rand(100000,999990));?>
<!DOCTYPE HTML>
<html lang="en">
<!-- <html lang="en" manifest="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
data.appcache"> -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo $_smarty_tpl->tpl_vars['title_text']->value;?>
</title>
<link href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/<?php echo $_smarty_tpl->tpl_vars['deviceSpecificCss']->value;?>
" rel="stylesheet">
<link href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/jquery-ui.css" rel="stylesheet">
<link href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/stylesheets/backgrounds.css" rel="stylesheet"> 


<style type="text/css">
  .slicknav_menu .slicknav_menutxt { display: none;}
</style>

 <?php echo '<script'; ?>
 type="text/javascript">
var SITE_URL   = "<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
";
var siteurl    = "<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
";
var CMS_URL    = "<?php echo $_smarty_tpl->tpl_vars['CMS_URL']->value;?>
";
var API_URL    = "<?php echo $_smarty_tpl->tpl_vars['API_URL']->value;?>
";
var ADMIN_URL  = "<?php echo $_smarty_tpl->tpl_vars['ADMIN_URL']->value;?>
";
var FB_APP_ID  = "<?php echo $_smarty_tpl->tpl_vars['FB_APP_ID']->value;?>
";
var SESSION_ID = "<?php echo $_SESSION['uid'];?>
";
var $sandwich_img_live_uri = "<?php echo $_smarty_tpl->tpl_vars['sandwich_img_live_uri']->value;?>
";
var DATA_ID = "<?php echo $_smarty_tpl->tpl_vars['DATA_ID']->value;?>
";
var BASE_FARE = "<?php echo $_smarty_tpl->tpl_vars['BASE_FARE']->value;?>
";
 <?php echo '</script'; ?>
>
</head>
<body <?php if ($_smarty_tpl->tpl_vars['cart']->value) {
echo $_smarty_tpl->tpl_vars['CLASS_NAME']->value;
}?>> 


<div class="pagelogo">
	<img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/loading_logo.png" width="196" height="88"  />  
	<div style="text-align: center; display:block;" class="loadMoreGallery spin-star"><img alt="Loading" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/star.png"> </div>
</div>		
<div class="page-contents" style="display:none" > 		
<div id="fb-root"></div> 

<?php echo '<script'; ?>
 src="https://connect.facebook.net/en_US/all.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
FB.init({
    appId: '557060384864429',
    status: true, 
    cookie: true, 
    xfbml: false
});    
<?php echo '</script'; ?>
>

<div class="header_wrapper">
	<header>
	
  		 <nav>
		  
      	<ul id="menu">
      	<!-- 
        	<li>
          	<span class="menu-logo">
            	<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></a>
            </span>
          </li>  -->
          <?php   if(isset($_SESSION['uid'])) { ?>
	          <li>
	          	<span class="button-wrapper">
	            	Hi, <?php echo $_SESSION['uname'];?>

              <a href="<?php echo $_smarty_tpl->tpl_vars['signOutURL']->value;?>
" class="signout-account">Sign Out</a> 
			  
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
myaccount/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
"  class="create-account">ORDERS</a>
					<!-- <div class="facebook-likes">
					 <div class="fb-like" data-href="http://cms.ha.allthingsmedia.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					  </div> --> 
	            </span>
	          </li>
	          <?php  } else if($_COOKIE['user_mail'] && $_COOKIE['user_fname'] && $_COOKIE['user_lname'] )  { ?>
	        <li>
	          	<span class="button-wrapper">
	            	Hi, <?php  echo $_COOKIE['user_fname']; ?>
              <a href="<?php echo $_smarty_tpl->tpl_vars['signOutURL']->value;?>
" class="signout-account">Sign Out</a> 
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
myaccount"  class="create-account">ORDERS</a>
	              <!-- <img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/facebook-like.png" alt=""> -->
	            </span>
	          </li>
         <?php  } else { ?>
	          <li>
	          	<span class="button-wrapper">
	            	<a href="<?php  echo _URL; ?>" class="login-facebook">LOGIN WITH FACEBOOK</a>
	              <a href="javascript:void(0);" class="login-button" id="logintowebcms">LOGIN</a>
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="javascript:void(0);"  class="create-account connect-register">Create account</a>
					<!-- <div class="facebook-likes">
					  <div class="fb-like" data-href="http://cms.ha.allthingsmedia.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					  </div>-->
	            </span>
	          </li>
         <?php   } ?>
         

		 <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
home/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">Home</a></li>
          <li class="sandwich_sub__menus"><a href="#">SANDWICHES</a>
            <ul>
                <li>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/sandwichMenu/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">MENU</a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
createsandwich/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">CUSTOMIZE</a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/savedSandwiches/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">SAVED</a>
                    
                    <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/featuredSandwiches/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">FEATURED</a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
sandwich/gallery/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">ALL</a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/friendSandwiches/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">FRIENDS</a>

                </li>
            </ul>
          </li>
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/salads/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">SALADS</a></li>
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/snacks/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">SNACKS</a></li>
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
menu/drinks/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">DRINKS</a></li>
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
catering/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">CATERING</a></li>
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
site/aboutus/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="nav-hide-img">About us</a></li>
          <!-- <li><a href="http://mobile.ha.allthingsmedia.com/site/location/index/?pid=863897" class="nav-hide-img">Location</a></li> -->
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
site/covid19/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="nav-hide-img">Food Safety</a></li>
          <li><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
site/contactus/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="nav-hide-img">Contact</a></li>
		 <!--  <li><a href="http://mobile.ha.allthingsmedia.com/site/testmode" class="nav-hide-img">Testmode</a></li> -->

      </ul>
      </nav>
			
      <figure class="logo"><a href="#" class="menu"><?php if ($_smarty_tpl->tpl_vars['heading']->value) {?> <?php echo $_smarty_tpl->tpl_vars['heading']->value;?>
 <?php } else { ?><img alt="logo" src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/logo.png"><?php }?><!--<?php echo $_smarty_tpl->tpl_vars['heading']->value;?>
--></a></figure>
      <div class="cart-top">
		<?php   if(isset($_SESSION['uid'])) { ?>  
			<a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
">
		<?php  } else { ?>	
			<a class="login-button-cart" href="#">
		<?php  } ?> 	
        	<img src="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
app/images/shopping-cart.png" alt="shopping-cart">
       	<h1><?php echo $_smarty_tpl->tpl_vars['cart']->value;?>
</h1>
        </a>
      </div>
      
		
  </header>
  
  		<div class="dynamic_updates" style="display: none;">
			<div class="left-block"></div>
			<div class="right-block"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="">CHECKOUT</a></div>
		</div>

	<!-- <div class="dynamic_updates_fixed" style="display: none;">
			<div class="left-block"></div>
			<div class="right-block"><a href="<?php echo $_smarty_tpl->tpl_vars['SITE_URL']->value;?>
checkout/index/?pid=<?php echo $_smarty_tpl->tpl_vars['random_number']->value;?>
" class="">CHECKOUT</a></div>
		</div> -->
  
</div>
<div class="menu-block-replace"></div>
		
<?php }
}
