
<section class="sandwich create-sandwhich-menu-wrapper">
<div class="container">
  <!-- ORDER 1 -->
  {if $orderhistory|@count gt 0} 
   {foreach from=$orderhistory key=id item=history}
  
  <div class="order-history">
   
  <div class="container">
  <div class="row">
  <div class="right-float">
     {if $history->itemCheck gt 0}
  <a href="#" data-order= "{$history->order_id}" data-from="order_history" class="recorder reorder-button">REORDER</a>
  {/if}
  </div>
  <div class="left-float">
  <h2>{$history->timestampDate|date_format:"%a, %b %e, %Y"|upper}</h2>


  {foreach from=$history->items key=id item=item}

  		<h3 class="order-product-name">({$item->qty}) {if $item->type eq 'user_sandwich'}<span>{$item->sandwich_name|upper}</span>{/if}
			{if $item->type eq 'product'}<span>{$item->product_name|upper}</span>{/if}
  		</h3>  		
  		<p>{$item->desc}</p>
      

        <span class="item_details" style="display: none;" data-bread="{$item->bread}" data-sandwich_desc="{$item->desc}" data-sandwich_desc_id="{$item->desc_id}" data-id="{$item->id}"></span>
      
  {/foreach}
  
  
  <div class="deliver">
  <p><span>{$history->delivery_type|upper} : </span>&nbsp;{$history->address->address1}</p>
  <p><span>PAYMENT : </span>&nbsp;{$history->billing_type|upper} {$history->billing_card_no}</p>
  </div>
  </div>
  </div>
  </div>
  </div>
  
  {/foreach}
  {else}
  <span><h3 class="no_order_history">No Previous Orders.</h3></span>
  {/if}

</div>
</section>

