

<section class="sandwich">
  <div class="container">
    <div class="sandwich-wrapper"> 
      <h3>Sandwiches</h3>
       <ul class="user-menu-sandwiches">         
         
          
         
         
         
        {if $usersandwich|@count gt 0 }
        
        
			 {section name=sandwitch start=0 loop=$usersandwich|@count step=1}
            
            
             {assign var = 'prot_data' value = $usersandwich[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name}
              {assign var = 'cheese_data' value = $usersandwich[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name}
              {assign var = 'topping_data' value = $usersandwich[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name}
              {assign var = 'cond_data' value = $usersandwich[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name}
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
              if($c){
              foreach($c as $c){
               $result_1 .= ' '.$c;
              } 
              }
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
            
                  {if $order_data.user_sandwich.item_id|@is_array} 
                   {assign var = 'items_id' value = $usersandwich[$smarty.section.sandwitch.index].id|@in_array:$order_data.user_sandwich.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
                   
                   
                {if $usersandwich[$smarty.section.sandwitch.index].by_admin eq 0}
                   {assign var="img_path" value=$CMS_URL}
                {else}
                   {assign var="img_path" value=$ADMIN_URL}
                {/if}   
  
  {if $usersandwich[$smarty.section.sandwitch.index].uid gt 0 }          
   <li id="item-{$usersandwich[$smarty.section.sandwitch.index].id}" class="right-rgt" rel="ADD TO CART" data-sandwich-name="{$usersandwich[$smarty.section.sandwitch.index].sandwich_name}" data-sandwich_desc="{$usersandwich[$smarty.section.sandwitch.index].sandwich_desc}"  data-userid ="{$usersandwich[$smarty.section.sandwitch.index].uid}"  data-toast="{$usersandwich[$smarty.section.sandwitch.index].menu_toast}" data-menuadds="{$usersandwich[$smarty.section.sandwitch.index].menu_add_count}" data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-formatdate="{$usersandwich[$smarty.section.sandwitch.index].formated_date}" data-username="{$usersandwich[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$usersandwich[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0]}"  data-date="{$usersandwich[$smarty.section.sandwitch.index].date_of_creation|date_format}" data-price="{$usersandwich[$smarty.section.sandwitch.index].sandwich_price}" data-id="{$usersandwich[$smarty.section.sandwitch.index].id}" data-name="{$usersandwich[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$usersandwich[$smarty.section.sandwitch.index].like_id}" data-likecount="{$usersandwich[$smarty.section.sandwitch.index].like_count}" data-flag="{$usersandwich[$smarty.section.sandwitch.index].flag}" data-image="{$image_path}{$usersandwich[$smarty.section.sandwitch.index].uid}/sandwich_{$usersandwich[$smarty.section.sandwitch.index].id}_{$usersandwich[$smarty.section.sandwitch.index].uid}.png"> 
		 
				
				        <img width="53" height="24"   class="retina toasted"  style=" {if $usersandwich[$smarty.section.sandwitch.index].menu_toast eq 1 } display:block; {else} display:none; {/if}" alt="toasted" src="{$SITE_URL}app/images/toasted.png">
				        
           <img class="sand-pop" title="{$usersandwich[$smarty.section.sandwitch.index].sandwich_name}" src="{$image_path}{$usersandwich[$smarty.section.sandwitch.index].uid}/sandwich_{$usersandwich[$smarty.section.sandwitch.index].id}_{$usersandwich[$smarty.section.sandwitch.index].uid}.png">
       
   
           
           <h4>{$usersandwich[$smarty.section.sandwitch.index].sandwich_name|upper|truncate:20:"..":true}</h4>
 
          
          
           <a class="common_add_item_cart add-cart" data-uid="{$smarty.session.uid}" data-sandwich_id="{$usersandwich[$smarty.section.sandwitch.index].id}" data-sandwich="user_sandwich" href="#">Add to Cart</a>
                </li>
                {/if}
         {/section}
         
        {else}         
        
         <li class="right-rgt">
           <div class="right-a salads-wrapper">
             <h5>YOU CURRENTLY HAVE NO SAVED SANDWICHES</h5>
   
           </div>
         </li>
         
        {/if} 
       </ul>
       
       <div class="loadMoreGallery" style="text-align:center"><img src="{$SITE_URL}app/images/star.png" alt="Loading" /> </div>
       
    </div>
	
	
	{foreach name=outer item=category from=$stdCategoryItems}      
		{if $category->categoryProducts|@count gt 0 }
			<div class="salads-wrapper">
			  <h3>{$category->standard_cat_name}</h3>
			   <ul>
				{foreach key=key item=item from=$category->categoryProducts}
          
          
					{if $order_data.product.item_id|@is_array} 
						{assign var = 'product_items_id' value = $item->id|@in_array:$order_data.product.item_id}
					{else}
						{assign var = 'product_items_id' value = '0' }
					{/if}
					{assign var = 'prdid' value=$item->id }   
				 <li id="item-{$prdid}" class="salads-menu-li">
				   <div class="left">
					 <h5>{$item->product_name}</h5>
                     <span class="price">${$item->product_price}</span>
					 <p>{$item->description}</p>
				   </div>
				   <div class="right">
					 <input type="hidden" class="amount" name="amount" value="{$item->product_price}"/>
					 <a id="product-{$item->id}" data-sandwich="{$product}" data-sandwich_id="{$item->id}" data-uid="{$smarty.session.uid}" href="#" class="add-cart common_add_item_cart">Add to cart</a>
				   </div>
				 </li>
				{/foreach} 
			   </ul>
			</div>
		{/if}	
	{/foreach}	
		
    
  </div>
</section>

	</ul>
	
	<div class="toastit-wrapper">
    	<div class="checkbox-holder-final">
      	<input type="checkbox" class="savetousertoast toast" id="check1" name="check" class="toast">
        <label for="check1"><span>Toast it!</span> </label>
      </div>
    </div>
	
	<div class="add-menu">
	

    
	<a class="share share-sandwich" href="#"><img src="{$SITE_URL}app/images/share-img.png" alt=""></img>
	<span class="Share_sandwich">Share</span>
	</a>
	
<a class="add-to-menu save-menu" href="#" >Save to my menu</a>
  
	
		 <input type="hidden" name="hidden_sandwich_data" id="hidden_sandwich_data" value="user_sandwich">
	       <input type="hidden" name="hidden_sandwich_id" id="hidden_sandwich_id" value="">
	</div>
	<div class="add-cart">
	<h2 class="amount">$10.00</h2>
    <h3 class="cart-items popup_spinner">
		<a href="javascript:void(0);" class="left_spinner"></a>
           <input name="itemQuty" type="text" class="text-box"  value="01" readonly>
		<a href="javascript:void(0);" class="right_spinner"></a>
	</h3>	
	<a class="add-to-cart" href="#">Add to cart</a>
	</div>
	
    </div>	
  </div>
</div>

<!-----------------Menu View Sandwich------------------------------->

