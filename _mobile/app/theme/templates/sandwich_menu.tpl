{assign var="random_number" value=100000|rand:999990}
<section class="sandwich new_sandwich_mobile">
            
            <div class="container">
              <div class="new_gallery_count">
              <p>{$galleryCount}</p>
              <span>shared creations</span>
              <a href="{$SITE_URL}sandwich/gallery/?pid={$random_number}">VIEW ALL</a>
            </div>
                <div id="sandwich_overview_mobile" class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Create a Sandwich</h3>
                    <ul class="user-menu-sandwiches">
                        <li class=" vertical-aligned">
                            <span class="new_create__sandwich">
                                 <div class="homebannerauto">
                                  <div class="homepageloader">
<img src="{$SITE_URL}app/images/ellipsis.svg" class="homeAnimationLoaderImg" alt="">
</div>
        <span class="homepageAutoBanner">
        <span class="sandwich-1">
          <img src="{$SITE_URL}app/images/overviewpage_animation/sandwich1/1.png" style="display:block" alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/2.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/3.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/4.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/5.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich1/6.png" style="display:none"  alt="">
        </span>
        
        <span class="sandwich-2">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/7.png"  style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/8.png"  style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/9.png"  style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/10.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/11.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/12.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich2/13.png" style="display:none"  alt="">
        </span>
        
        <span class="sandwich-3">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/14.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/15.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/16.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/17.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/18.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/19.png" style="display:none"  alt="">
          <img src="{$SITE_URL}/app/images/overviewpage_animation/sandwich3/20.png" style="display:none"  alt="">
        </span>
        
        </span>
        {literal}
 <script type="text/javascript">
 window.addEventListener('load',function(){ mobile_ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
 </script>
{/literal}
      </div> 
                            </span>
                        </li>
                        <li class="vertical-aligned">
                            <div class="right-a ">
                                <h5 class="create_own__sandwich">sandwich customizer</h5>
                                    <h2>Build Your Own in 5 Easy Steps</h2>
                                <a class="get-started__new" href="{$SITE_URL}createsandwich/index/?pid={$random_number}">CUSTOMIZE</a>
                                
                            </div>
                        </li>


                    </ul>

                    <div class="loadMoreGallery" style="text-align:center"><img
                            src="{$SITE_URL}/app/images/star.png" alt="Loading" /> </div>

                </div>
                <!-- <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>Sandwich Gallery</h3>
                    <ul class="user-menu-sandwiches">
                        <li class="right-rgt vertical-aligned">
                            <span class="new_create__sandwich">
                                <img src="{$SITE_URL}app/images/Layer_6_copy.png">
                            </span>
                        </li>

                        <li class="right-rgt vertical-aligned">
                            <div class="right-a ">
                                <h5 class="create_own__sandwich shared-sandwich__head">
                                    <p class="big-count">{$galleryCount}</p>
                                    Shared Sandwiches
                                </h5>
                                <a class="get-started__new" href="{$SITE_URL}sandwich/gallery">VIEW GALLERY</a>
                                 <p>Use one of the options to the left to get started!</p>
                            </div>
                        </li>


                    </ul>

                    <div class="loadMoreGallery" style="text-align:center"><img
                            src="http://mobile.ha.allthingsmedia.com/app/images/star.png" alt="Loading" /> </div>

                </div> -->
                <div class="sandwich-wrapper sandwich-new-wrapp">
                    <h3>My Saved Sandwiches <a href="{$SITE_URL}sandwich/savedSandwiches/?pid={$random_number}" class="view-all-new"><img src="{$SITE_URL}app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a></h3>
                    {if !empty($saved_sandwich_data)}
                              <ul>
                                 
                              {section name=sandwitch start=0 loop=$saved_sandwich_data|@count step=1 }
             
              {assign var = 'bread_name' value=$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0] }
              {assign var = 'prot_data' value = $saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {assign var =  'bread_name' value = $bread_name }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
              
                   
                   
                   {if $order_data.user_sandwich.item_id|@is_array} 
                   {assign var = 'items_id' value = $saved_sandwich_data[$smarty.section.sandwitch.index].id|@in_array:$order_data.user_sandwich.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
              
              {if $saved_sandwich_data[$smarty.section.sandwitch.index].by_admin eq 1}
              {assign var="url" value=$ADMIN_URL}
              {else}
              {assign var="url" value=$SITE_URL}
              {/if}
              
              {assign var="sdesc" value=$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_desc}
              {assign var="fname" value=$saved_sandwich_data[$smarty.section.sandwitch.index].first_name}
              {assign var="lname" value=$saved_sandwich_data[$smarty.section.sandwitch.index].last_name}
              {assign var="sprice" value=$saved_sandwich_data[$smarty.section.sandwitch.index].current_price}
              {assign var="user_name" value=$saved_sandwich_data[$smarty.section.sandwitch.index].user_name}
              {assign var="sname" value=$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_name}
               {assign var="tname" value=$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_name_trimmed}
                    <div class="save-sandwich--wrap" data-id="{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" data-sandwich_desc="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_desc}" data-bread="{$bread_name}">
        <input type="hidden" id="featured_sandwich_view_popup" value="1">
        <div class="save-sand--img">
          <span><img title="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_name}" class="view_sandwich" data-href="{$SITE_URL}createsandwich/index/{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}/sandwich_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}_{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}.png"><img src="{$SITE_URL}app/images/toasted.png" class="toasted" style="{if $saved_sandwich_data[$smarty.section.sandwitch.index].menu_toast eq 1 } display:block; {/if}"></span>
        </div>

        <div class="save-sand--content save-sand--content-new save-sand_three_sec" id="toastID_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" data-sandwich_desc="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_desc_id}" data-flag="{$saved_sandwich_data[$smarty.section.sandwitch.index].flag}" data-menuadds="{$saved_sandwich_data[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}" {if $items_id  eq 1} rel="ADD TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-toast="{$saved_sandwich_data[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$saved_sandwich_data[$smarty.section.sandwitch.index].formated_date}" data-username="{$saved_sandwich_data[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$bread_name}"  data-date="{$saved_sandwich_data[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$saved_sandwich_data[$smarty.section.sandwitch.index].current_price}" data-id="{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" data-name="{$saved_sandwich_data[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$saved_sandwich_data[$smarty.section.sandwitch.index].like_id}" data-likecount="{$saved_sandwich_data[$smarty.section.sandwitch.index].like_count}" data-typeSandwich="SS">
            <input type="hidden" name="chkmenuactice{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" value="{$saved_sandwich_data[$smarty.section.sandwitch.index].menu_is_active}" />
            
            {assign var=jsonData value=$sandwich->sandwich_data|json_decode:1}
            <span class="saved-sand--price">{$sname}</span>
            <p>{$sdesc}</p>
            <input type="hidden" value="{$saved_sandwich_data[$smarty.section.sandwitch.index].is_public}" id="isPublic{$saved_sandwich_data[$smarty.section.sandwitch.index].id}">
                           
            <img data-href="{$SITE_URL}createsandwich/index/{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" id="sandwichimg_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}/sandwich_{$saved_sandwich_data[$smarty.section.sandwitch.index].id}_{$saved_sandwich_data[$smarty.section.sandwitch.index].uid}.png" hidden>
            <p>Created by: {$user_name}.</p>
            <div id="featured_new_mobile">
              <h2>${$sprice}</h2>
              <div>
                 <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
                       <input id="sandwichQty{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" name="itemQuty" type="text" class="text-box" value="01" readonly="">
                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
                </h3>
              </div>
              <div class="saved-add-cartwrap">
                <a class="saved-add-cart quickAddToCart" href="javascript:void(0);"><img src="{$SITE_URL}app/images/cart.png"><span>Add</span></a>
              </div>
            </div>
            
            <div class="saved-item-btns menu-saved_list">
              <a href="javascript:void(0);" class="view_sandwich" id="view-sandwich-popup">View</a>
              <!-- <a href="{$SITE_URL}createsandwich/index/{$saved_sandwich_data[$smarty.section.sandwitch.index].id}">Edit</a> -->
              <a href="javascript:void(0);" class="editSandwich" id="{$saved_sandwich_data[$smarty.section.sandwitch.index].id}">Edit</a>
              {if $saved_sandwich_data[$smarty.section.sandwitch.index].menu_is_active eq 1}
              
              <a class="saved" href="javascript:void(0);">SAVED</a>
              <input type="hidden" name="saved_tgl{$saved_sandwich_data[$smarty.section.sandwitch.index].id}" value="1">
              {else}
              <a href="javascript:void(0);" class="featured_item_btn">SAVE</a>
              {/if}
              {if $saved_sandwich_data[$smarty.section.sandwitch.index].is_public eq 0}
              <span class="locked-item"><img src="{$SITE_URL}app/images/locked-itm.png"></span>
              {/if}
          </div>
        </div>
  <input class="typeSandwich" type="hidden" value="SS">
  <input type="hidden" name="saved_tgl{$featured_data[$smarty.section.sandwitch.index].id}" value="1">
 <input type="hidden" name="hidden_uid" id="hidden_uid" value="{$smarty.session.uid}" >
  <input type="hidden" value="product" name="data_type">
 <!--  <div class="add-new no-items">
    

  </div> -->
</div>
                    {/section} 
                </div>
                {else}
                  <div class="no-saved-sandwiches">
                     <h3>
                     you currently have no saved sandwiches
                     </h3>
                     <p>EITHER CREATE YOUR OWN CUSTOM SANDWICH, CHOOSE FROM OUR USER-SUBMITTED GALLERY, OR TRY ONE OF YOUR FRIEND'S SANDWICHES.
                     </p>
                  </div>
               {/if}
                <div class="sandwich-wrapper sandwich-new-wrapp">
                  
                    <h3><span class="span-update">UPDATED MONTHLY!</span><span>Featured Sandwiches </span><a href="{$SITE_URL}sandwich/featuredSandwiches/?pid={$random_number}" class="view-all-new"><img src="{$SITE_URL}app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a></h3>
                    {section name=sandwitch start=0 loop=$featured_data|@count step=1 }
             
              {assign var = 'bread_name' value=$featured_data[$smarty.section.sandwitch.index].sandwich_data.BREAD.item_name[0] }
              {assign var = 'prot_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.PROTEIN.item_name }
              {assign var = 'cheese_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.CHEESE.item_name }
              {assign var = 'topping_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.TOPPINGS.item_name }
              {assign var = 'cond_data' value = $featured_data[$smarty.section.sandwitch.index].sandwich_data.CONDIMENTS.item_name }
              
              {assign var =  'bread_name' value = $bread_name }
              
              {php}
              $result = '';
              $d = $_smarty_tpl->get_template_vars('prot_data');
              if($d){
              foreach($d as $d){
              
               $d = trim($d);
               $d = str_replace(' ','##',$d);
               $result .= ' '.$d;
              }}
              {/php}
              
              
                     {php}
              $result_1 = '';
              $c = $_smarty_tpl->get_template_vars('cheese_data');
               if($c){
              foreach($c as $c){
              
               $c = trim($c);
               $c = str_replace(' ','##',$c);
              
               $result_1 .= ' '.$c;
              }}
              {/php}
              
              
                     {php}
              $result_2 = '';
              $t = $_smarty_tpl->get_template_vars('topping_data');
              if($t){
              foreach($t as $t){
               $t = trim($t);
               $t = str_replace(' ','##',$t);
               $result_2 .= ' '.$t;
              }}
              {/php}
              
              
                     {php}
              if($o){       
              $result_3 = '';
              $o = trim($o);
              $o = str_replace(' ','##',$o);
              $o = $_smarty_tpl->get_template_vars('cond_data');
              foreach($o as $o){
               $result_3 .= ' '.$o;
              }}
              {/php}
      
      
              
                   
                   
                   {if $order_data.user_sandwich.item_id|@is_array} 
                   {assign var = 'items_id' value = $featured_data[$smarty.section.sandwitch.index].id|@in_array:$order_data.user_sandwich.item_id}
                   {else}
                   {assign var = 'items_id' value = '0' }
                   {/if}
              
              {if $featured_data[$smarty.section.sandwitch.index].by_admin eq 1}
              {assign var="url" value=$ADMIN_URL}
              {else}
              {assign var="url" value=$SITE_URL}
              {/if}
              
              {assign var="sdesc" value=$featured_data[$smarty.section.sandwitch.index].sandwich_desc}
              {assign var="fname" value=$featured_data[$smarty.section.sandwitch.index].first_name}
              {assign var="lname" value=$featured_data[$smarty.section.sandwitch.index].last_name}
              {assign var="sprice" value=$featured_data[$smarty.section.sandwitch.index].current_price}
              {assign var="user_name" value=$featured_data[$smarty.section.sandwitch.index].user_name}
              {assign var="sname" value=$featured_data[$smarty.section.sandwitch.index].sandwich_name}
               {assign var="tname" value=$featured_data[$smarty.section.sandwitch.index].sandwich_name_trimmed} 
                    <div class="save-sandwich--wrap" data-id="{$featured_data[$smarty.section.sandwitch.index].id}" data-sandwich_desc="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc}" data-bread="{$bread_name}">
        <input type="hidden" id="featured_sandwich_view_popup" value="1">
        <div class="save-sand--img">
          <span><img class="view_sandwich" title="{$featured_data[$smarty.section.sandwitch.index].sandwich_name}" data-href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png"></span>
        </div>

        <div class="save-sand--content save-sand--content-new save-sand_three_sec" data-sandwich_desc="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc}" data-sandwich_desc_id="{$featured_data[$smarty.section.sandwitch.index].sandwich_desc_id}" data-flag="{$featured_data[$smarty.section.sandwitch.index].flag}" data-menuadds="{$featured_data[$smarty.section.sandwitch.index].menu_add_count}" data-userid ="{$featured_data[$smarty.section.sandwitch.index].uid}" {if $items_id  eq 1} rel="ADD TO CART" {else} rel="ADD TO CART" {/if} data-cheese="{php} echo $result_1;{/php}" data-topping="{php} echo $result_2;{/php}" data-cond="{php} echo $result_3;{/php}" data-toast="{$featured_data[$smarty.section.sandwitch.index].menu_toast}" data-formatdate="{$featured_data[$smarty.section.sandwitch.index].formated_date}" data-username="{$featured_data[$smarty.section.sandwitch.index].user_name}" data-protien="{php} echo trim($result); {/php}" data-bread="{$bread_name}"  data-date="{$featured_data[$smarty.section.sandwitch.index].date_of_creation}" data-price="{$featured_data[$smarty.section.sandwitch.index].current_price}" data-id="{$featured_data[$smarty.section.sandwitch.index].id}" data-name="{$featured_data[$smarty.section.sandwitch.index].sandwich_name}" data-likeid="{$featured_data[$smarty.section.sandwitch.index].like_id}" data-likecount="{$featured_data[$smarty.section.sandwitch.index].like_count}" data-typeSandwich="FS">

            <input type="hidden" name="chkmenuactice{$featured_data[$smarty.section.sandwitch.index].id}" value="{$featured_data[$smarty.section.sandwitch.index].menu_is_active}" />
            
            {assign var=jsonData value=$sandwich->sandwich_data|json_decode:1}
          
            <span class="saved-sand--price">{$sname}</span>
            <p>{$sdesc}</p>
            
             <img data-href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}" id="sandwichimg_{$featured_data[$smarty.section.sandwitch.index].id}" src="{$image_path}{$featured_data[$smarty.section.sandwitch.index].uid}/sandwich_{$featured_data[$smarty.section.sandwitch.index].id}_{$featured_data[$smarty.section.sandwitch.index].uid}.png" hidden>
            <p>Created by: {$user_name}.</p>

            <div id="featured_new_mobile">
              
              <h2>${$sprice}</h2>
              <div>
                <h3 class="cart-items popup_spinner inputbox-disable-overlay-position">
                                        <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                                        <a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
                                           <input id="sandwichQty{$featured_data[$smarty.section.sandwitch.index].id}" name="itemQuty" type="text" class="text-box" value="01" readonly="">
                                        <a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
                                    </h3>
              </div>
              <div class="saved-add-cartwrap">
                  <a class="saved-add-cart quickAddToCart" href="javascript:void(0);"><img src="{$SITE_URL}app/images/cart.png"><span>Add</span></a>
              </div>

            </div>
              <div class="saved-item-btns saved-item-btns-new menu-saved_list">
                                <a href="javascript:void(0);" class="view_sandwich" id="view-sandwich-popup">View</a>
                                <!-- <a href="{$SITE_URL}createsandwich/index/{$featured_data[$smarty.section.sandwitch.index].id}">Edit</a> -->
                                <a href="javascript:void(0);" class="editSandwich" id="{$featured_data[$smarty.section.sandwitch.index].id}">Edit</a>
                                {if $featured_data[$smarty.section.sandwitch.index].id|in_array:$saved_sandwich_ids}
                                
                                <a href="javascript:void(0);" class="saved ">SAVED</a>
                                <input type="hidden" name="saved_tgl{$featured_data[$smarty.section.sandwitch.index].id}" value="1">
                                {else}
                                <a class="featured_item_btn" href="javascript:void(0);">SAVE</a>
                                {/if}
                                {foreach from=$saved_sandwich_ids key=k item=v}
                                  {if $featured_data[$smarty.section.sandwitch.index].id eq $v}
                                      {if $is_public_array.$v eq 0}
                                      <input type="hidden" value="0" id="isPublicFs{$featured_data[$smarty.section.sandwitch.index].id}"> 
                                          <span class="locked-item"><img src="{$SITE_URL}app/images/locked-itm.png"></span>
                                      {/if}
                                  {/if}

                                     
                                {/foreach}
                                    
            </div>
        </div>
  <input class="typeSandwich" type="hidden" value="FS">
<input type="hidden" name="hidden_uid" id="hidden_uid" value="{$smarty.session.uid}" >
  <input type="hidden" value="product" name="data_type">
 <!--  <div class="add-new no-items">
   

  </div> -->
</div>
                    {/section}
                </div>
                <div class="sandwich-wrapper sandwich-new-wrapp bottom-blank">
                    <h3><span class="fb-left-head"><img src="{$SITE_URL}app/images/facebook-round.png"></span>Friends’ Sandwiches<a
                            href="{$SITE_URL}menu/friendSandwiches/?pid={$random_number}" class="view-all-new">
                            <img src="{$SITE_URL}app/images/view-all--btn.png">
                            <h2>View All</h2>
                        </a>
                    </h3>
                    {if !empty($fbdata)}
                    <div class="friends-sandwich--block">
                        {php}$i = 1;{/php}
                            {foreach key=key item=item from=$fbdata}
                            {php} if ($i <=5){ {/php}
                        <div class="friends-sandwich">
                            <div class="friends-dp"><img src="https://graph.facebook.com/{$item->friend_uid}/picture?width=107&height=107"></div>
                            <div class="friend-name">
                                <h2>{$item->first_name} {$item->last_name}</h2>
                                <p>{$item->sandwich_count} saved sandwiches</p>
                            </div>
                            <div class="friends-view-wrap">
                                <div class="friends-view--btn"><a href="{$SITE_URL}menu/friendsMenu/{$item->uid}" class="freinds-view">view</a>
                                </div>
                            </div>
                        </div>
                        {php}$i++; }{/php}
                              
                        {/foreach}

                        <div>
                            <a href="{$SITE_URL}menu/friendSandwiches" class="viewall-saved-sandwiches"><img
                                    src="{$SITE_URL}app/images/view-all--border.png"><span>view all
                                    <!--connect--></span></a>
                        </div>
                    </div>
                    {else}
                           <div class="friends-items-bottom not-connected-fb">
                              <p>CONNECT TO FACEBOOK TO SEE YOUR FRIENDS’ SANDWICHES!</p>
                              <div class="friends-items-view">
                                 <a href="#" class="viewall-saved-sandwiches"><img src="{$SITE_URL}app/images/view-all--border.png"><span class="friends-items-view-h2 login-facebook">CONNECT</span></a>
                              </div>
                           </div>
                    {/if}
                </div>
            </div>
        </section>