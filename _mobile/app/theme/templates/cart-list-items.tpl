
{if $itemsCount gt 0}  
          <table class="cart_list_header">
					<thead>
						<tr>
							<th class="item">Items</th>
							<th class="qty">Qty</th>
							<th class="price">price</th>
							<th class="remove_header">&nbsp;</th>
						</tr>
					</thead>
					<tbody></tbody>
			</table>
                        <div class="hundred-min-red-bar first-time">{if $totalNumber < 10 && $pickupOrDelivery neq 0} <p>
					{math assign="reaminingNumber" equation=10.00-$totalNumber}
						$10.00 SUBTOTAL MINIMUM. ${$reaminingNumber|string_format:"%.2f"} TO GO!
                            </p>{/if}</div>
			<div class="cart_item_list_wrapper">
			<div class="cart_item_list_scroller common-scrollbar">	
			<table class="cart_list_items">		
					<tbody>
					{foreach from=$sandwiches key=myId item=sandwich}
						<tr class="cart_item_list">
							<td data-sandwich-ingredients='{$sandwich.sandwich_data}' data-toast = "{$sandwich.toast}" class="item cart-item-name">{$sandwich.sandwich_name}</td>
							<td class="qty inputbox-disable-overlay-position">
							<div class="inputbox-disable-overlay checkout-input-disable">&nbsp;</div> 
							   <a href="javascript:void(0);" class="left_spinner_checkout"><img class="qty-img-left" src="{$SITE_URL}app/images/left-arrow-hover.png"></a>							   
							   
                                                           <input name="itemQuty" type="hidden" class="qtybox"  value="{if $sandwich.item_qty lt 10}0{/if}{$sandwich.item_qty}">
                                                           <span class="qtybox">{if $sandwich.item_qty lt 10}0{/if}{$sandwich.item_qty}</span>
                                                           <a href="javascript:void(0);" class="right_spinner_checkout"><img class="qty-img-right" src="{$SITE_URL}app/images/right-arrow-hover.png"></a>
							   <input type="hidden" class="sandwich_id" value="{$sandwich.id}" />
							  <input type="hidden" class="order_item_id" value="{$sandwich.order_item_id}" />
							  <input type="hidden" class="data_type" name="data_type" value="user_sandwich"/>
							  
							</td>
							<td class="price">${$sandwich.item_total|number_format:2}</td>
							<td class="remove"><a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="{$sandwich.order_item_id}" data-item="{$sandwich.item_id}">&nbsp;</a> </td>
							  <input type="hidden" class="bread_type" name="bread_type" value="{$sandwich.bread_type}"/> 
						</tr>
					{/foreach}
					
					{foreach from=$products key=myId item=product}
					   <tr>
							<td class="item">{$product->product_name|truncate:19:"..":true}
								{if $product->extra_id ne ""} <p style="font-size: 10px;">Modifiers : <strong>{$product->extra_id}</strong></p> {/if}                 
        					{if $product->spcl_instructions ne ""} <p style="font-size: 10px;">Special Instructions : <strong>{$product->spcl_instructions}</strong></p> {/if} </td>
								
							<td class="qty">
							<div class="inputbox-disable-overlay checkout-input-disable">&nbsp;</div> 
								 <a href="javascript:void(0);" class="left_spinner_checkout"><img class="qty-img-left" src="{$SITE_URL}app/images/left-arrow-hover.png"></a>							   
							   
							   <input name="itemQuty" type="hidden" class="qtybox"  value="{if $product->item_qty lt 10}0{/if}{$product->item_qty}">
                                                           <span class="qtybox">{if $product->item_qty lt 10}0{/if}{$product->item_qty}</span>
                                                           
                                                           <a href="javascript:void(0);" class="right_spinner_checkout"><img class="qty-img-right" src="{$SITE_URL}app/images/right-arrow-hover.png"></a>
								<input type="hidden" class="sandwich_id" value="{$product->id}" />
								<input type="hidden" name="product_parent_type" value="{$product->product_parent_type}" />
								<input type="hidden" class="order_item_id" value="{$product->order_item_id}" />
								<input type="hidden" class="data_type" name="data_type" value="product">
							</td>
							<td class="price">${$product->item_total|number_format:2}</td>							
							<td class="remove"><a class="remove link remove_cart_item"  href="javascript:void(0)" data-target="{$product->order_item_id}" data-item="{$product->item_id}">&nbsp;</a> </td> 
						</tr>
					{/foreach}

					
  					</tbody>
			</table>
			</div>
			</div>
             
			
{else}
			<ul>
				<li>
				<h3>There are currently no items in your shopping cart.</h3>  
				</li>
			</ul>				
{/if}



			{if $itemsCount gt 0} 
			 <div class="clearfix sales-info">
			<div class="sales-tax"><p style="visibility:hidden;">Sales tax inclusive </p></div>
			 <div class="check-out">
                <div class="tax-wrapper">
                 <div class="apply-discount-code"><a href="#">Apply discount code</a></div>
				</div>
                <div class="tax-total-wrapper">
					<div class="total"><p>SUBTOTAL</p><p>TAX</p><p>TIP</p><p style="display:none" class="p-discount-title">DISCOUNT</p></div>
					<div class="total"><p class="p-subtotal">${$total|number_format:2}</p><p class="p-tax">${$tax}</p><p class="p-tips">${$tips|string_format:"%.2f"}</p><p style="display:none" class="p-discount">$0.00</p></div>
				</div>	
               </div>
                <div class="clearfix"></div><div class="clearfix"></div>
                 <div class="check-out">
                <div class="tax-2"><!-- visibilityHide -->
                    <p class="tip">TIP</p>
                		<div id="dd" class="wrapper-dropdown-tip" tabindex="1">	
                		<span class="tip-wrapper">					
						<select id="select_tip_amount">
							{if $pickupOrDelivery eq 0}
								<option value="0.00">$0.00</option>
								{section name=foo start=1 loop=$MAX_TIP step=1}
									<option {if $tips eq $smarty.section.foo.index || $smarty.section.foo.index lt $tips } selected {/if} value="{$smarty.section.foo.index}">${$smarty.section.foo.index}.00</option>
									{assign var="tipVal" value=$smarty.section.foo.index|cat:'.50'}
									{if $smarty.section.foo.index<10}  
									<option value="{$smarty.section.foo.index}.50">${$smarty.section.foo.index}.50</option>
									{/if}
								{/section} 
								<option {if $tips eq $MAX_TIP} selected {/if} value="{$MAX_TIP}.00">${$MAX_TIP}.00</option>
							{else}

								{section name=foo start=$MIN_TIP loop=$MAX_TIP step=1}
									<option {if $tips eq $smarty.section.foo.index || $smarty.section.foo.index lt $tips } selected {/if} value="{$smarty.section.foo.index}">${$smarty.section.foo.index}.00</option>
									{assign var="tipVal" value=$smarty.section.foo.index|cat:'.50'}
									{if $smarty.section.foo.index<10}  
									<option value="{$smarty.section.foo.index}.25">${$smarty.section.foo.index}.25</option>
									<option value="{$smarty.section.foo.index}.50">${$smarty.section.foo.index}.50</option>
									<option value="{$smarty.section.foo.index}.75">${$smarty.section.foo.index}.75</option>
									{/if}
								{/section}
								<option {if $tips eq $MAX_TIP} selected {/if} value="{$MAX_TIP}.00">${$MAX_TIP}.00</option>
							{/if}	
						</select>
						</span>
						
					</div>
                </div>
                <div class="grand-total"><h3><span>Total</span></h3></div>
                <div class="grand-total"><h2>${$grandtotal|number_format:2}</h2></div>
				{if $total>=0} <p class="est-delivery-time">EST. DELIVERY TIME:
	                {if $total>=0 && $total<100}30-60 mins
		                {elseif $total>=100 && $total<200}45-75 mins
		                {elseif $total>=200 && $total<300}60-90 mins
		                {elseif $total>=300 && $total<400}1-2 hrs
		                {elseif $total>=400 && $total<500}2-3 hrs
		                {elseif $total>=500}24 hours                
	                {/if}</p>
	            {/if}
               </div>
               </div>
               {/if}


<input type="hidden" value="{$order_string}" name="odstring" />
<input type="hidden" name="hidden_grand_total" id="hidden_grand_total" value="{$grandtotal}">
<input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="{$total}">
<input type="hidden" name="hidden_tip" id="hidden_tip" value="{$tips}">
<input type="hidden" name="hidden_tax" id="hidden_tax" value="{$tax}">
<input type="hidden" name="hidden_discount_id" id="hidden_discount_id" value="">
<input type="hidden" name="hidden_discount_amount" id="hidden_discount_amount" value="">
