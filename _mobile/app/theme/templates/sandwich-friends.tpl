<section class="sandwich-menu">
  <ul>
    <li>
      <div class="left">
        <h2>Menu</h2>
        <p>Lorem ipsum dolor sit amet</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
    <li>
      <div class="left">
        <h2>Create a Sandwich</h2>
        <p>build your own in 5 easy steps</p>
      </div>
      <div class="right">  <a href="#" class="view-button">Create</a> </div>
    </li>
    <li>
      <div class="left">
        <h2>Customer Creations</h2>
        <p>25,687 creations to choose from!</p>
      </div>
	   <div class="right"> <a href="#" class="view-button">view</a> </div>
	   </li>
	   <li>
	  <div class="left">
        <h2>Past Orders</h2>
        <p>Lorem ipsum dolore site amet</p>
      </div>
      <div class="right"> <a href="#" class="view-button">view</a> </div>
    </li>
  </ul>
</section>
<div class="sandwich-popup">
	<div class="summary-details-friends-menu">
  	<a href="#" class="close-button"></a>
  	<h2>Friends' Menus</h2>
		
	<section class="sandwich-menu">
  <ul>
    <li>
      <div class="left">
        <img src="{$SITE_URL}/app/images/sandwich-member1.png" alt=""></img>
        <h2>longname longlastname</h2>
        <p>10 Sandwiches</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
	<li>
      <div class="left">
        <img src="{$SITE_URL}/app/images/sandwich-member2.png" alt=""></img>
        <h2>longname longlastname</h2>
        <p>10 Sandwiches</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
	<li>
      <div class="left">
        <img src="{$SITE_URL}/app/images/sandwich-member3.png" alt=""></img>
        <h2>longname longlastname</h2>
        <p>10 Sandwiches</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
	<li>
      <div class="left">
        <img src="{$SITE_URL}/app/images/sandwich-member4.png" alt=""></img>
        <h2>longname longlastname</h2>
        <p>10 Sandwiches</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
	<li>
      <div class="left">
        <img src="{$SITE_URL}/app/images/sandwich-member1.png" alt=""></img>
        <h2>longname longlastname</h2>
        <p>10 Sandwiches</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
	<li>
      <div class="left">
        <img src="{$SITE_URL}/app/images/sandwich-member2.png" alt=""></img>
        <h2>longname longlastname</h2>
        <p>10 Sandwiches</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
	<li>
      <div class="left">
        <img src="{$SITE_URL}/app/images/sandwich-member3.png" alt=""></img>
        <h2>longname longlastname</h2>
        <p>10 Sandwiches</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
	<li>
      <div class="left">
        <img src="{$SITE_URL}/app/images/sandwich-member4.png" alt=""></img>
        <h2>longname longlastname</h2>
        <p>10 Sandwiches</p>
      </div>
      <div class="right">  <a href="#" class="view-button">view</a> </div>
    </li>
  </ul>
</section>
	
    </div>	
  </div>