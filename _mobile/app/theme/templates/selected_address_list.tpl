	{foreach from=$addresses key=k item=address}
	<div class="address">
	<address>
		<h2>{$address->address1}</h2>
		<input type="hidden" class="zip_hidden" value="{$address->zip}"/>
		<div class="left">
		 <p>{$address->name}<br/>{$address->company}<br/>
		{$address->address1}<br/>		

		{if $address->street ne ""}{$address->street}<br/>{/if}
		{if $address->cross_streets ne ""}({$address->cross_streets}) {/if} New York, NY {$address->zip} <br/> {$address->phone} {if $address->extn}EXT: {$address->extn}{/if}
		</p>
		</div>
		<div class="right address-right">
		{if $address->delivery_instructions ne ""}
		<p>
			DELIVERY INST:<br/> 
			{$address->delivery_instructions}
		</p>
		{/if}
		 <a href="#" data-address-id="{$address->address_id}" class="view-button select-address-button address-select">select</a>
		</div>
	</address>
	</div>
	{/foreach}
