{if $pastOrder|@count eq 0 }
<div class="homepageloader">
<img src="{$SITE_URL}app/images/ellipsis.svg" class="homeAnimationLoaderImg" alt="">
</div>
{/if}

<div class="banner home-banner-div home-banner-div-new"> 
	<div class="banner-wrapper"> 
	 
	
	{if  $uid neq ''  && $pastOrder|@count gt 0  }  
			{if $pastOrder}
			{if $pastOrder.details.by_admin eq 1}
			  {assign var="path" value=$ADMIN_URL}		
			{else}
			  {assign var="path" value=$SITE_URL}	
			{/if}
			
			{assign var = 'image_exist' value =$image_path|cat:$pastOrder.details.image}	
			{php}
				$d = $_smarty_tpl->get_template_vars('image_exist');		 
				$ch = curl_init($d);
				curl_setopt($ch, CURLOPT_NOBODY, true);
				curl_exec($ch);
				$retCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);		
			{/php}
			
	
			{php} if($retCode==200){ {/php}	
			<span class="new_banner-top">			
				<img class="banner-login" src="{$image_path}{$pastOrder.details.image}" alt="{$title_text}">
				<img class="banner-img-top" src="{$SITE_URL}app/images/banner-top.png" alt="banner-top">
			</span>
			{php} } else { {/php}
				<img class="banner-login" src='{$SITE_URL}app/images/BB_logo_placeholder.png' >
			{php} } {/php}
		
		<h2>Your Most Recent Order</h2>
		<h3>{$pastOrder.street}</h3>
		<p> {if $pastOrder.details.is_delivery eq 0} pick-up {else} Delivery {/if} - {$pastOrder.date}</p>
		{if $pastOrder.details.bread ne ""}
            <span class="item_details" data-bread="{$pastOrder.details.bread}" data-sandwich_desc="{$pastOrder.details.desc}" data-sandwich_desc_id="{$pastOrder.details.desc_id}" data-id="{$pastOrder.details.item_id}"></span>
        {/if}
		<div class="button-wrapper"> 
			{if $itemsAvailable gt 0}
			<a href="#" class="reorder reorder-item" data-order="{$order_id}" data-from="home_page">reorder</a>
			{/if}
			<a class="place-new-order" href="{$SITE_URL}sandwich/sandwichMenu/?pid={100000|rand:999990}">place new order</a>
		</div>
		{else}
		
		{/if}
	 
		{else}
		
		
	   <div class="homebannerauto">
	    
     	  <span class="homepageAutoBanner">
     	  <span class="sandwich-1">
     	    <img src="{$SITE_URL}app/images/homepage_animation/sandwich1/1.png" style="display:block" alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich1/2.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich1/3.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich1/4.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich1/5.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich1/6.png" style="display:none"  alt="">
     	  </span>
     	  
     	  <span class="sandwich-2">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/7.png"  style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/8.png"  style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/9.png"  style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/10.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/11.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/12.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich2/13.png" style="display:none"  alt="">
     	  </span>
     	  
     	  <span class="sandwich-3">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/14.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/15.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/16.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/17.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/18.png" style="display:none"  alt="">
     	    <img src="{$SITE_URL}/app/images/homepage_animation/sandwich3/19.png" style="display:none"  alt="">
     	  </span>
     	  
     	  </span>
      </div> 	  
      
		  <h2>Sandwiches Your way!</h2>
		  {if $uid neq ''}
		    <p>Build your own sandwich in 5 easy steps using a <br>curated selection of the finest local ingredients!</p>
		  	{else}
		  	<p>Welcome to Barney Brown, a custom-made social-sandwiching experience. Get started by building your own sandwich in just 5 easy steps.</p>
		  	{/if}

		  		  <a class="place-order" href="{if $smarty.session.uid neq ""}{$SITE_URL}menu/{else}{$SITE_URL}createsandwich/index/?pid={100000|rand:999990}/{/if}">get started</a>

		  
 {literal}
 <script type="text/javascript">
 window.addEventListener('load',function(){ mobile_ha.sandwichcreator.set_sandwich_maker_slider(); }, false );
 </script>
{/literal}
		
		 <!-- <img src="{$banner_image}" alt="{$title_text}">
		  <h2>Sandwiches Your way!</h2>
		  <p>Build your own sandwich in 5 easy steps<br>using a curated selection of the best<br> local breads and other fine ingredients</p>
		  <a class="place-order" href="{$SITE_URL}menu/">place order</a>
		  
		  -->
		
	{/if} 
	</div>
</div>
<section class="sandwich-menu">
<div class="sa-delivery" style="background:#695b45;font:bold 11px/16px 'Open Sans';letter-spacing: 0px;text-align:center;padding:5px 0;">
		<p class="timing"><span>Manhattan Delivery, </span>Mon - Fri 10:00AM - 4:00PM</p>
</div>
  <ul class="home_page_menu">
  	<li>
      <div class="left">		
			<h2>Sandwiches</h2>
			<p>A MENU OF ENDLESS POSSIBILITIES</p>        
      </div>
      <div class="right">  <a href="{$SITE_URL}sandwich/sandwichMenu/index/?pid={100000|rand:999990}" class="view-button">VIEW</a> </div>
    </li>
    <li>		
		<div class="left">		
			<h2>Salads</h2>
			<p>A SELECTION OF OUR CLASSIC SALADS</p>
		
		</div>		
      <div class="right">  <a href="{$SITE_URL}menu/salads/?pid={100000|rand:999990}" class="view-button">VIEW</a> </div>
    </li>
    
    <li>
      <div class="left">
        <h2>Snacks</h2>
        <p>A LITTLE SOMETHING ON THE SIDE</p>
      </div>
	   <div class="right"> <a href="{$SITE_URL}menu/snacks/?pid={100000|rand:999990}" class="view-button">VIEW</a> </div>
	   </li>
	   <li>
	  <div class="left">
        <h2>Drinks</h2>
        <p>SOMETHING TO QUENCH YOUR THIRST</p>
      </div>
      <div class="right"> <a href="{$SITE_URL}menu/drinks/?pid={100000|rand:999990}" class="view-button">VIEW</a> </div>
    </li>
  </ul>
</section>
