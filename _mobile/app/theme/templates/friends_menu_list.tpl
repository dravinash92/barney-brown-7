
{if $data|@count gt 0}

{foreach key=key item=item from=$data}

    <li>
      <div class="left">
        <img alt="" src="https://graph.facebook.com/{$item->friend_uid}/picture?width=57&height=57">
        <h2>{$item->first_name} {$item->last_name}</h2>
        <p>{$item->sandwich_count} Sandwiches</p>
      </div>
      <div class="right">  <a class="view-button" href="{$SITE_URL}menu/friends_menu/{$item->uid}">view</a> </div>
    </li>
 
{/foreach}
{else}

<ul class="frndpopcont"><li><p style="margin-top:18px;"> Looks like you're the first of your friends to join Barney Brown! Let them know what they're missing out on! </p>
<a href="{$SITE_URL}menu/" class="back-to-menu link">BACK TO MY MENU</a>
</li></ul>

{/if}



