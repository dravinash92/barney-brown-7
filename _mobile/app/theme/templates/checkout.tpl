<section class="sandwich create-sandwhich-menu-wrapper">
		<div class="container check-out-head-wrap">
        <div class="check-out-head profile">
		<h2>Almost done!</h2>
        <a href="{$SITE_URL}sandwich/sandwichMenu/?pid={100000|rand:999990}" class="friend-button">MY MENU</a>
        </div>
        </div>
        <div class="clearfix"></div> 
        <div class="check-out-left">  
             {$cartItemList}
        </div>      
		       
        <div class="selection-wrapper">    
			<div class="amount-done amount-done-checkout">
			
			    <div class="personal-detail">
			    <!-- style="display:none" for launch only -->
               <div style="display:none" class="details-1 first-time" id="select_delivery_or_pickup_div">
               <div class="total"><h3>TYPE</h3></div>
               <div class="tax" ><h2>CHOOSE DELIVERY OR PICK--UP</h2></div>
               <div class="total"><img class="total-img" src="{$SITE_URL}app/images/right-arrow-2.png"></div>
               </div>
               <div class="details-1 first-time" id="select_address_div">
               <div class="total"><h3>ADDRESS</h3></div>
               <div class="tax" ><h2>CHOOSE A LOCATION</h2></div>
               <div class="total"><img class="total-img" src="{$SITE_URL}app/images/right-arrow-2.png"></div>
               </div>
               <div class="details-1 first-time" id="enter_payment_div">
               <div class="total"><h3>PAYMENT</h3></div>
               <div class="tax" ><h2>CHOOSE PAYMENT METHOD</h2></div>
               <div class="total"><img class="total-img" src="{$SITE_URL}app/images/right-arrow-2.png"></div>
               </div>
               <div class="details-1" id="enter_datetime_seelction">
               <div class="total"><h3>WHEN</h3></div>
               <div  class="tax" ><h2 id='front_page_delivery_time'>NOW</h2></div>
               <div class="total"><img class="total-img" src="{$SITE_URL}app/images/right-arrow-2.png"></div>
               </div>
               </div>
			                     <input type = "hidden" name="_card_number_auth" value="" />
                              <input type = "hidden" name="_expiry_month_auth" value="" />
                              <input type = "hidden" name="_expiry_year_auth" value="" />
			
				<div class="amount-done-wrapper">
					<h2>Total</h2>
					<h3>${$grandtotal|string_format:"%.2f"}</h3>
					<a class="done place-order-checkout" href="javascript:void(0)">PLACE ORDER</a>
				</div>
			</div>
		</div>
		<input type="hidden" id="order-type" name="order_type" value="" />
		<input type="hidden" id="address-id" name="address_id" value="" />
		<input type="hidden" id="card-id" name="card_id" value="" />
		
		
</section>

<!--- Select Address --->
<input type="hidden" id="zip_hidden_selected" value=""/>
<div class="sandwich-popup" id="address_list" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
		<div class="add-address-wrapper">
			<h2>Select Address</h2>
		   <a href="javascript:void(0)" id="add-address" class="add-address">Add new address</a>
		   
					  
		   <div class="addresslist">		
			   <div class="address">		
				   <address>
						<h2>205 E 10TH STREET</h2>
						<div class="left">
						<p>Matthew Baer Company<br/> 205 E 10th Street<br/> Apt. 4D <br/> (Cross Streets Here) New York, NY 10003 212-555-5555</p>
						</div>
						<div class="right">
						<p>DELIVERY INST:<br/>Ring buzzer 4308</p>
						 <a href="#" class="view-button select-address-button address-select">select</a>
						</div>
					</address>
				</div>   
			</div>
		</div>  
	</div>
</div>

<!--- Select Address --->
<div class="sandwich-popup" id="warningcheckout" style="display:none;">
  <div class="added-menu-sandwiches-inner">
  <a href="#" class="close-button">Close</a>
  <div class="added-good-news">
  <div class="added-good-news-msg">
    <div class="title-holder">
      <h1>Good News!</h1>
    </div>
    <div class="title2-holder">
    <p>Although Barney Brown does not currently offer regular lunch service in this area, same-day orders of $100.00 or more are accepted to this address!</p>
    </div>
    </div>
    </div>
    <div class="button-holder"> 
      <a href="#" id="warningOk" class="add-to-cart">OK</a>
    </div>
  </div>
</div>  
<!--- Select Delivery or Pickup --->


<div class="sandwich-popup" id="select_delivery_or_pickup" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
	<div class="add-address-wrapper">
	<h2>Select Delivery or Pickup</h2>
	<div class="address">
  <div class=""> 
	<a href="javascript:void(0);" class="view-button order-type" data-order-type="1">Delivery</a>
	<span class="or">or</span>
	<a href="javascript:void(0);" class="view-button order-type" data-order-type="0">Pickup</a>
  </div>
    </div>
   </div>
</div>
</div>

<!--- Add new Address --->

<div class="sandwich-popup" id="add_new_address" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
	<div class="add-new-address">
		<h2>Add New Address</h2>
	<div class="popup-scroll-wrapper">
	<form method="post" class="add_address">
    <ul class="from-holder">
      <li> <span class="text-box-holder">
        <p>Recipient Name</p>
        <input name="recipient" type="text" class="text-box-control" autocorrect="off"  spellcheck="false" value="{$smarty.session.uname } {$smarty.session.lname }">
        </span> <span class="text-box-holder">
        <p>Company (optional)</p>
        <input name="company" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Street Address</p>
        <input name="address1" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span> <span class="text-box-holder">
      <!--  <p>Street Address 2 (optional)</p>
        <input name="address2" type="text" class="text-box-control" >
        </span> <span class="text-box-holder1">-->
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" autocorrect="off"  spellcheck="false" >
        </span> </li>
		<li><span class="text-box-holder phone-number">
        <p>Phone Number</p>
        <input name="phone1" id="phone1" type="text" class="text-box-phone" autocorrect="off" spellcheck="false" maxlength="3">
        <input name="phone2" id="phone2" type="text" class="text-box-phone" autocorrect="off"  spellcheck="false" maxlength="3">
        <input name="phone3" id="phone3" type="text" class="text-box-phone margin-none" autocorrect="off"  spellcheck="false" maxlength="4" >
        </span> <span class="text-box-holder1 text-box-control-ext" >
        <p>Ext.</p>
        <input name="extn" type="text" id="phoneext" class="text-box-control">
        </span> </li>
      <li> <span class="text-box-holder">
        <h3>New York, NY</h3>
        <span class="text-box-zip-holder1">
        <p>Zip</p>
        <input name="zip" type="text" class="text-box-zip enter_zip" autocorrect="off"  spellcheck="false">
        </span> </span>
      </li>
	  <li> <span class="text-box-holder">
        <p>Delivery Instructions (optional)</p>
        <input name="delivery_instructions" type="text" class="text-box-control enter_delivery" autocorrect="off"  spellcheck="false">
        </span> </li>
		<a href="#" class="add-address save_address">Add address</a>
	
    </ul>
    </form>
    </div>
	</div>
  
</div>
</div>

<!-- Credit card List -->
<div class="sandwich-popup" id="credit_card_list" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
		<div class="add-address-wrapper">
			<h2>Select Payment Method</h2>
		   <a href="javascript:void(0)" id="add-address" class="add-address add-credit-card">Add Credit Card</a>		  
		   <div class="addresslist">
					{foreach from=$billingInfo key=k item=card}
						<div class="address">	
{assign var="cardnos" value=$card->card_number|str_split:5}							
							<h2>{if $card->card_type eq "American Express"} AMEX {else} {$card->card_type|upper} {/if} ENDING WITH {$cardnos[3]}</h2>
							<div class="left">
							<p>	{$card->card_number}<br/>		
								{if $card->card_type ne ""}{$card->card_type|capitalize}<br/>{/if}
								{if $card->card_zip ne ""}{$address->card_zip}<br/>{/if}							
							</p>
							</div>
							<div class="right">	
							<a rel="{$card->id}" class="remove-card">Remove</a>							
							 <a href="#" data-card-id="{$card->id}" class="view-button select-card-button">select</a>
							</div>						
						</div>
					{/foreach}				
			</div>
		</div>  
	</div>
</div>	

<!-- Add new credit card -->

<div class="sandwich-popup" id="new_credit_card" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
 <div class="saved-creditcards-wrapper">
 <h2>Add New Credit Card</h2>
             <ul class="from-holder">
      <li> 
	  <span class="text-box-holder">
        <p>Credit Card</p>
        <span class="credit-card">
        <select name="cardType" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option>Visa</option>
          <option>MasterCard</option>
          <option>American Express</option>
          <option>Discover</option>          
        </select>
        </span> </span></li>
		<li><span class="text-box-holder">
        <p>Card Number</p>
        <input name="cardNo" type="text" class="text-box-control" placeholder="">
        </span></li> 
		<li><span class="text-box-holder1">
        <p>Sec. Code</p>
        <input name="cardCvv" type="text" class="text-box-control" >
        </span> </li>
      <li> <span class="text-box-holder">
        <p>Expiration Date</p>
        <span class="month">
        <select name="cardMonth" id="jumpMenu" onChange="MM_jumpMenu('parent',this,0)">
          <option value="01">01 - January</option>
          <option value="02">02 - February</option>
          <option value="03">03 - March</option>
          <option value="03">04 - April</option>
          <option value="05">05 - May</option>
          <option value="06">06 - June</option>
          <option value="07">07 - July</option>
          <option value="08">08 - August</option>
          <option value="09">09 - September</option>
          <option value="10">10 - October</option>
          <option value="11">11 - November</option>
          <option value="12">12 - December</option>
        </select>
        </span> <span class="year">
        <!--<select name="cardYear" id="jumpMenu" >
			{section name=foo start=2015 loop=2051 step=1}
			  <option value="{$smarty.section.foo.index}">{$smarty.section.foo.index}</option>
			{/section}
        </select>-->
	<select name="cardYear"  id="jumpMenu" >
				{assign var="currentyear" value=$smarty.now|date_format:"%Y" }
				{assign var="numyears" value=50}
				{assign var="totalyears" value=$currentyear+$numyears}
				{section name=loopyers  start=$currentyear loop=$totalyears step=1}
				<option value="{$smarty.section.loopyers.index}">{$smarty.section.loopyers.index}</option>
				{/section}
         </select>
        </span></li> <li></span> <span class="text-box-holder">
        <p>Billing Zip</p>
        <input name="cardZip" type="text" class="text-box-billing-zip" >
        </span> </li>
	  <!-- <li> <span class="text-box-holder">
        <p>Street Address</p>
        <input name="address1" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span>  <span class="text-box-holder1">
        <p>Ste/Fl/Apt</p>
        <input name="street" type="text" class="text-box-control" autocorrect="off"  spellcheck="false">
        </span> </li> -->
       <!-- <li> <span class="text-box-holder">
       <p>Street Address 2 (optional)</p>
        <input name="address2" type="text" class="text-box-control" >
        </span><span class="text-box-holder">
        <p>Cross Streets</p>
        <input name="cross_streets" type="text" class="text-box-control" autocorrect="off"  spellcheck="false" >
        </span> </li>-->
      <li> <span class="toastit-wrapper">
        <input id="check2" type="checkbox" checked="checked"  name="save_billing" value="check2">
        <label for="check2">Save Billing Info <!--<span>(Verisign Encryption)</span> --></label>
        </span> </li>
       <a href="#" class="add-address save-credit-card">Add card</a>
    </ul>
 </div>
  
</div>
</div>

<!---Select address --------->


<!---------------Select Date Popup------------------->
<div class="sandwich-popup" id="select_date_time" style="display:none;">
	<div class="summary-details-login">
  	<a href="#" class="close-button"></a>
		<div class="add-address-wrapper">
			<h2>Select Date / Time</h2>		   
		   <div class="addresslist">		
				<div class="address">							
					<input type="radio" class="css-checkbox switch_date" id="radio1" {if $currenthour lt 16} checked="checked" {/if} name="radiog_lite" value="now">
					<label class="css-label" for="radio1">Now</label>
          
          <p class="or">or</p>
					
					<input type="hidden" name="todays_date" value="{php} echo date(l); {/php}" />
					<input type="hidden" name="todays_time" value="{php} echo date('H:00'); {/php}" />					   
					<input type="hidden" name="delivery_store_id" value="" />
					<input type="radio" class="css-checkbox switch_date" id="radio2" {if $currenthour gt 16}checked="checked" {/if} name="radiog_lite" value="specific">
					<label class="css-label" for="radio2">Specific Date/Time</label>
					<div id="cal-holder" class="date_time_wrapper filter date_wrapper inputbox-disable-overlay-position" style="display:none;">
						 <div class="inputbox-disable-overlay calendar-input-disable">&nbsp;</div>
						<input name="" readonly="readonly" type="text" class="datepicker date delivery_date" placeholder="Select Date"/>				
					</div>
					<div class="date_time_wrapper filter" style="display:none; background:none;">
						<div class="timechange">  {$times}</div>
					</div>
					
					<input type="hidden" name="hidden_store_id" id="hidden_store_id" value=""/>
                    <input type="hidden" name="selected_day" id="selected_day" value=""/>
                    <input type="hidden" name="selected_date" id="selected_date" value="0"/>
						{foreach from=$addresses key=k item=v}
							{if $k==0}
							<input value="{$v->id}" type="hidden" name=="hidden_default_store_id" id="hidden_default_store_id" />
							{/if}
				        {/foreach}
				        
				       
				</div>   
         <div class="right">						
						<a href="#" class="view-button apply-time-selection done">select</a>
				 </div>
			</div>
		</div>  
	</div>
</div>


<!--Apply discount Popup--->
<div class="sandwich-popup" id="apply_discount_popup" style="display:none;">
	<div class="summary-details-discount">
		<a href="#" class="close-button"></a>
		<div class="apply-discount">
			<div class="discount-wrapper">
				<h2>APPLY DISCOUNT CODE</h2>
				<input class="text-lastname" name="discount_code" id="discount_code" type="text" placeholder="Enter Discount Code">
				<a href="#" class="add-address check-discount-code">apply discount</a>
			</div>
		</div>
	</div>
</div>
<input name="recipientName" type="hidden" value="{$smarty.session.uname } {$smarty.session.lname }">
{literal}
<style type="text/javascript">
.delivery_date{color:#dab37f;}
.date_time_wrapper .ui-datepicker-trigger{visibility:hidden;}
</style>
<script>
var inp1 = document.getElementById('phone1');
var inp2 = document.getElementById('phone2');
var inp3 = document.getElementById('phone3');
var count=0;
inp1.onkeyup = function() {
  
    if(inp1.value.length==3)
    {
        //$('#phone2').focus();
        
    }
   
}
inp2.onkeyup = function() {
  
    if(inp2.value.length==3)
    {
        //$('#phone3').focus();
    }
   
}
inp3.onkeyup = function() {
  
    if(inp3.value.length==4)
    {
        //$('#phoneext').focus();
    }
   
}


</script>
{/literal}
