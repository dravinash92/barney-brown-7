  <div class="container pages">
		<h1>DELIVERY MAP <a href="#" class="friend-button">Back</a></h1>
        
     <div id="delivery-map" style="color: black;">
		<div class="contact-wrapper">		
			
             <div class="delivery-time-map">
             <h2>MONDAY - SUNDAY, 10AM - 4PM</h2>
             <p>Barney Brown currently delivers in Midtown Manhattan, New York City from 14th Street to 42nd Street. </p>
             <div class="delivery-time-map-image">
                <img src="{$SITE_URL}app/images/delivery-map.png"  alt="map">
             </div>
             </div>   
		</div>	
     </div>         
       
      </div>
