{php} 

$FB = new FB_API();
$access_token  = @$_SESSION['access_token'];

if(!isset($_SESSION['FBRLH_state']) || !$_SESSION['FBRLH_state']  ){

$FB->FB_check_login();
$url =  $FB->FB_get_url();
$_SESSION['tmpUrl'] = $url;
$cookietime        = time() + 60 * 60 * 24 * 60;
setcookie("fbloginurl",$url,$cookietime, '/', NULL);
define('_URL',$url);

} else {

define('_URL',$_SESSION['tmpUrl']);

}


  
{/php}
 {assign var="random_number" value=100000|rand:999990}
<!DOCTYPE HTML>
<html lang="en">
<!-- <html lang="en" manifest="{$SITE_URL}data.appcache"> -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<title>{$title_text}</title>
<link href="{$SITE_URL}app/stylesheets/{$deviceSpecificCss}" rel="stylesheet">
<link href="{$SITE_URL}app/stylesheets/jquery-ui.css" rel="stylesheet">
<link href="{$SITE_URL}app/stylesheets/backgrounds.css" rel="stylesheet"> 

{literal}
<style type="text/css">
  .slicknav_menu .slicknav_menutxt { display: none;}
</style>
{/literal}
 <script type="text/javascript">
var SITE_URL   = "{$SITE_URL}";
var siteurl    = "{$SITE_URL}";
var CMS_URL    = "{$CMS_URL}";
var API_URL    = "{$API_URL}";
var ADMIN_URL  = "{$ADMIN_URL}";
var FB_APP_ID  = "{$FB_APP_ID}";
var SESSION_ID = "{$smarty.session.uid}";
var $sandwich_img_live_uri = "{$sandwich_img_live_uri}";
var DATA_ID = "{$DATA_ID}";
var BASE_FARE = "{$BASE_FARE}";
 </script>
</head>
<body {if $cart}{$CLASS_NAME}{/if}> 


<div class="pagelogo">
	<img src="{$SITE_URL}app/images/loading_logo.png" width="196" height="88"  />  
	<div style="text-align: center; display:block;" class="loadMoreGallery spin-star"><img alt="Loading" src="{$SITE_URL}app/images/star.png"> </div>
</div>		
<div class="page-contents" style="display:none" > 		
<div id="fb-root"></div> 
{literal}
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
FB.init({
    appId: '557060384864429',
    status: true, 
    cookie: true, 
    xfbml: false
});    
</script>
{/literal}
<div class="header_wrapper">
	<header>
	
  		 <nav>
		  
      	<ul id="menu">
      	<!-- 
        	<li>
          	<span class="menu-logo">
            	<a href="{$SITE_URL}"><img src="{$SITE_URL}app/images/logo1.png" width="196" height="88" class="retina" alt="logo"></a>
            </span>
          </li>  -->
          {php}  if(isset($_SESSION['uid'])) { {/php}
	          <li>
	          	<span class="button-wrapper">
	            	Hi, {$smarty.session.uname }
              <a href="{$signOutURL}" class="signout-account">Sign Out</a> 
			  
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="{$SITE_URL}myaccount/index/?pid={$random_number}"  class="create-account">ORDERS</a>
					<!-- <div class="facebook-likes">
					 <div class="fb-like" data-href="http://cms.ha.allthingsmedia.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					  </div> --> 
	            </span>
	          </li>
	          {php} } else if($_COOKIE['user_mail'] && $_COOKIE['user_fname'] && $_COOKIE['user_lname'] )  { {/php}
	        <li>
	          	<span class="button-wrapper">
	            	Hi, {php} echo $_COOKIE['user_fname']; {/php}
              <a href="{$signOutURL}" class="signout-account">Sign Out</a> 
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="{$SITE_URL}myaccount"  class="create-account">ORDERS</a>
	              <!-- <img src="{$SITE_URL}app/images/facebook-like.png" alt=""> -->
	            </span>
	          </li>
         {php} } else { {/php}
	          <li>
	          	<span class="button-wrapper">
	            	<a href="{php} echo _URL; {/php}" class="login-facebook">LOGIN WITH FACEBOOK</a>
	              <a href="javascript:void(0);" class="login-button" id="logintowebcms">LOGIN</a>
	            </span>
	            <span class="create-acount-wrapper">
	            	<a href="javascript:void(0);"  class="create-account connect-register">Create account</a>
					<!-- <div class="facebook-likes">
					  <div class="fb-like" data-href="http://cms.ha.allthingsmedia.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					  </div>-->
	            </span>
	          </li>
         {php}  } {/php}
         

		 <li><a href="{$SITE_URL}home/index/?pid={$random_number}">Home</a></li>
          <li class="sandwich_sub__menus"><a href="#">SANDWICHES</a>
            <ul>
                <li>
                    <a href="{$SITE_URL}sandwich/sandwichMenu/?pid={$random_number}">MENU</a>
                    <a href="{$SITE_URL}createsandwich/index/?pid={$random_number}">CUSTOMIZE</a>
                    <a href="{$SITE_URL}sandwich/savedSandwiches/?pid={$random_number}">SAVED</a>
                    
                    <a href="{$SITE_URL}sandwich/featuredSandwiches/?pid={$random_number}">FEATURED</a>
                    <a href="{$SITE_URL}sandwich/gallery/?pid={$random_number}">ALL</a>
                    <a href="{$SITE_URL}menu/friendSandwiches/?pid={$random_number}">FRIENDS</a>

                </li>
            </ul>
          </li>
          <li><a href="{$SITE_URL}menu/salads/?pid={$random_number}">SALADS</a></li>
          <li><a href="{$SITE_URL}menu/snacks/?pid={$random_number}">SNACKS</a></li>
          <li><a href="{$SITE_URL}menu/drinks/?pid={$random_number}">DRINKS</a></li>
          <li><a href="{$SITE_URL}catering/index/?pid={$random_number}">CATERING</a></li>
          <li><a href="{$SITE_URL}site/aboutus/index/?pid={$random_number}" class="nav-hide-img">About us</a></li>
          <!-- <li><a href="http://mobile.ha.allthingsmedia.com/site/location/index/?pid=863897" class="nav-hide-img">Location</a></li> -->
          <li><a href="{$SITE_URL}site/covid19/index/?pid={$random_number}" class="nav-hide-img">Food Safety</a></li>
          <li><a href="{$SITE_URL}site/contactus/index/?pid={$random_number}" class="nav-hide-img">Contact</a></li>
		 <!--  <li><a href="http://mobile.ha.allthingsmedia.com/site/testmode" class="nav-hide-img">Testmode</a></li> -->

      </ul>
      </nav>
			
      <figure class="logo"><a href="#" class="menu">{if $heading } {$heading} {else}<img alt="logo" src="{$SITE_URL}app/images/logo.png">{/if}<!--{$heading}--></a></figure>
      <div class="cart-top">
		{php}  if(isset($_SESSION['uid'])) { {/php}  
			<a href="{$SITE_URL}checkout/index/?pid={$random_number}">
		{php} } else { {/php}	
			<a class="login-button-cart" href="#">
		{php} } {/php} 	
        	<img src="{$SITE_URL}app/images/shopping-cart.png" alt="shopping-cart">
       	<h1>{$cart}</h1>
        </a>
      </div>
      
		
  </header>
  
  		<div class="dynamic_updates" style="display: none;">
			<div class="left-block"></div>
			<div class="right-block"><a href="{$SITE_URL}checkout/index/?pid={$random_number}" class="">CHECKOUT</a></div>
		</div>

	<!-- <div class="dynamic_updates_fixed" style="display: none;">
			<div class="left-block"></div>
			<div class="right-block"><a href="{$SITE_URL}checkout/index/?pid={$random_number}" class="">CHECKOUT</a></div>
		</div> -->
  
</div>
<div class="menu-block-replace"></div>
		
