<!--   <div class="menu-list-wrapper pages">
  <h1>JOBS <a href="#" class="friend-button">Back</a></h1>
       {foreach from=$webpagedata key=k item=v}
       
     <div id="about-us" style="color: black;">   
      <article>
      {$v->text}
      </article>
     </div>         
       {/foreach} 
      </div>
 -->

   <div class="menu-list-wrapper pages food-safety-menu-list-wrapper">
  <h1>FOOD SAFETY <a href="#" class="friend-button">Back</a></h1>
  <h3>Your health is our top priority.</h3>
  <div class="food-safety-wrapper">

          <div class="food-safety-item">
            <div class="food-safety-img">
             <img src="{$SITE_URL}app/images/facemaskicon.png" alt="">
            </div>
            <div class="food-safety-text">
              <h4>FACE MASKS </h4>
              <p>Staff members are required to be wearing face masks at all times during their shifts.</p>
            </div>
          </div>
          <div class="food-safety-item">
            <div class="food-safety-img">
             <img src="{$SITE_URL}app/images/handwashingicon.png" alt="">
            </div>
            <div class="food-safety-text">
              <h4>HANDWASHING & GLOVES </h4>
              <p>Staff members are required to frequently wash their hands during their shifts, and must be wearing gloves while performing all job tasks</p>
            </div>
          </div>

          <div class="food-safety-item">
            <div class="food-safety-img">
             <img src="{$SITE_URL}app/images/wellnessicon.png" alt="">
            </div>
            <div class="food-safety-text">
              <h4>WELLNESS CHECKS</h4>
              <p>At the beginning of each shift, staff members must complete a wellness check, which includes having their temperature taken.</p>
            </div>
          </div>

          <div class="food-safety-item">
            <div class="food-safety-img">
             <img src="{$SITE_URL}app/images/cleaningicon.png" alt="">
            </div>
            <div class="food-safety-text">
              <h4>ENHANCED CLEANING MEASURES </h4>
              <p>In addition to our standard daily cleaning protocols, additional measures have been put in place to exceed public health guidelines. </p>
            </div>
          </div>

          

          <div class="food-safety-item">
            <div class="food-safety-img">
             <img src="{$SITE_URL}app/images/tamperprooficon.png" alt="">
            </div>
            <div class="food-safety-text">
              <h4>TAMPER-PROOF PACKAGING</h4>
              <p>All packages include a tamper-proof safety seal to ensure your meal arrives undisturbed, and to verify that the order was prepared and packaged with care.</p>
            </div>
          </div>

          <div class="food-safety-item">
            <div class="food-safety-img">
             <img src="{$SITE_URL}app/images/contactfreeicon.png" alt="">
            </div>
            <div class="food-safety-text">
              <h4>CONTACT-FREE DELIVERY</h4>
              <p>Our delivery team is trained for contact-free delivery when requested. Please specify in your delivery instructions where you’d like your order dropped off. </p>
            </div>
          </div>
          <div class="safley-prepared">
            <img src="{$SITE_URL}app/images/safelyprepared.png" alt="">
          </div>
        </div>
      </div>
