 	{foreach from=$addresses key=k item=address}
	<div class="address">	
	<address>
		<h2>{$address->store_name}</h2>
		<div class="left">
		<p>{$address->store_name}<br/> 
		{$address->address1}<br/>		
		{if $address->address2 ne ""}{$address->address2}<br/>{/if}		
		New York, NY {$address->zip} <br/> {$address->phone}
		</p>
		</div>
		<div class="right">		
		 <a href="#" data-address-id="{$address->id}" class="view-button select-address-button">select</a>
		</div>
	</address>
	</div>
	{/foreach}
