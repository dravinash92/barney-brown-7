<section class="sandwich">
  <div class="container">


     
     {foreach from=$cateringdataList key=j  item=list}
     <div class="salads-wrapper">
      {if $list.data|@count gt 0 }
      <h3 class="catering_header">{$list.cat_name}</h3>
       <ul>
        {foreach from=$list.data key=k item=v}
         
         <div id="mobile_drinks" class="save-sandwich--wrap" data-id="{$v.id}" data-product_name="{$v.product_name}" data-description="{$v.description}" data-product_image="{$v.product_image}" data-product_price="{$v.product_price}" data-standard_category_id="{$v.standard_category_id}" data-image_path="{$salad_image_path}" data-uid={$uid} data-product={$product} data-spcl_instr="{$v.allow_spcl_instruction}" data-add_modifier="{$v.add_modifier}" data-modifier_desc="{$v.modifier_desc}" data-add_modifier="{$v.add_modifier}" data-modifier_isoptional="{$v.modifier_isoptional}" data-modifier_is_single="{$v.modifier_is_single}" data-modifier_options='{$v.modifier_options}'>
                        <div class="save-sand--img">
                            <span><img title="{$v.product_name}" class="view_sandwich" data-href="{$v.id}" src="{$salad_image_path}{$v.product_image}"></span>
                        </div>
                        <div class="save-sand--content">
                            <span class="saved-sand--price">{$v.product_name}</span>
                            <p>{$v.description}</p>
                            <div class="new-drink_flex">
                            <h2>${$v.product_price}</h2>
                            <div class="add-new no-items add_btn_item">
                            <div class="add-new-sub">
                                <h3 class="cart-items popup_spinner inputbox-disable-overlay-position catering_spinn">
                                    <div class="inputbox-disable-overlay gallery-input-disable">&nbsp;</div>
                                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup left_spinner"></a>
                                    <input id="resetQty" name="itemQuty" type="text" class="text-box" value="01"
                                        readonly="">
                                    <a href="javascript:void(0);" class="sandwich_gallery_view_popup right_spinner"></a>
                                </h3>
                            </div>
                            <div class="saved-add-cartwrap">
                                <a href="#" class="saved-add-cart {if $v.allow_spcl_instruction eq 1 or $v.add_modifier eq 1} salad-listing {else} common_add_item_cart {/if}" data-sandwich="{$product}" data-sandwich_id="{$v.id}" data-uid="{$uid}"><img src="{$SITE_URL}app/images/cart.png"><span>Add</span></a>
                            </div>

                        </div>
                            </div>
                            
                        </div>
                        
                    </div>
        {/foreach}
       </ul>
        {/if}
     </div>
      {/foreach}
    
    
  </div>
</section>
